package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.TuttiPersistenceNoDbImpl;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristic;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.Zones;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.persistence.entities.referential.ObjectType;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import fr.ifremer.tutti.persistence.service.UpdateSchemaContextSupport;
import fr.ifremer.tutti.service.cruise.CruiseCache;
import fr.ifremer.tutti.service.cruise.CruiseCacheLoader;
import fr.ifremer.tutti.service.sampling.CruiseSamplingCache;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs2.AllFileSelector;
import org.apache.commons.vfs2.FileName;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileType;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.util.TimeLog;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import static org.nuiton.i18n.I18n.t;

/**
 * Persistence service.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class PersistenceService extends AbstractTuttiService implements TuttiPersistence {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PersistenceService.class);

    protected TuttiPersistence driver;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);

        init();
    }

    public void setSampleCategoryModel(SampleCategoryModel sampleCategoryModel) {
        context.getDataContext().setSampleCategoryModel(sampleCategoryModel);
    }

    public List<Gear> retainTemporaryGearList(List<Gear> targetList) {

        return retainTemporaryList(Gears.IS_TEMPORARY, targetList);

    }

    public List<Person> retainTemporaryPersonList(List<Person> targetList) {

        return retainTemporaryList(Persons.IS_TEMPORARY, targetList);

    }

    public List<Species> retainTemporarySpeciesList(List<Species> targetList) {

        return retainTemporaryList(Speciess.IS_TEMPORARY, targetList);

    }

    public List<Vessel> retainTemporaryVesselList(List<Vessel> targetList) {

        return retainTemporaryList(Vessels.IS_TEMPORARY, targetList);

    }


    public <E extends TuttiEntity> List<E> retainTemporaryList(Predicate<E> isTemporaryPredicate, List<E> targetList) {
        List<E> sourceList = new ArrayList<>();
        Iterator<E> iterator = targetList.iterator();
        while (iterator.hasNext()) {
            E next = iterator.next();
            if (isTemporaryPredicate.apply(next)) {
                iterator.remove();
                sourceList.add(next);
            }
        }
        return sourceList;
    }

    public interface FrequencyFunction extends Function<SpeciesBatch, List<SpeciesBatchFrequency>> {
    }

    public FrequencyFunction newSpeciesFrequenciesFunction() {
        return input -> getAllSpeciesBatchFrequency(input.getIdAsInt());
    }

    public FrequencyFunction newBenthosFrequenciesFunction() {
        return input -> getAllBenthosBatchFrequency(input.getIdAsInt());
    }

    public Float countFrequenciesWeight(List<SpeciesBatchFrequency> frequencies, boolean stopIfNullFound) {
        Float result = null;
        for (SpeciesBatchFrequency frequency : frequencies) {
            Float w = frequency.getWeight();
            if (w == null) {

                if (stopIfNullFound) {
                    break;
                }
                continue;
            }
            if (result == null) {
                result = 0f;
            }
            result += w;
        }
        return result;
    }

    public Integer countFrequenciesNumber(List<SpeciesBatchFrequency> frequencies, boolean stopIfNullFound) {
        Integer result = null;
        for (SpeciesBatchFrequency frequency : frequencies) {
            Integer number = frequency.getNumber();
            if (number == null) {

                if (stopIfNullFound) {
                    break;
                }
                continue;
            }
            if (result == null) {
                result = 0;
            }
            result += number;
        }
        return result;
    }

    //------------------------------------------------------------------------//
    //-- Technical methods                                                  --//
    //------------------------------------------------------------------------//

    @Override
    public String getImplementationName() {
        return "Tutti Persistence Service";
    }

    @Override
    public void setSkipShutdownDbWhenClosing() {
        driver.setSkipShutdownDbWhenClosing();
    }

    @Override
    public ProgramDataModel loadProgram(String programId, boolean loadFishingOperation) {
        return driver.loadProgram(programId, loadFishingOperation);
    }

    @Override
    public ProgramDataModel loadCruises(String programId, boolean loadFishingOperation, Integer... cruiseIds) {
        return driver.loadCruises(programId, loadFishingOperation, cruiseIds);
    }

    @Override
    public ProgramDataModel loadCruise(String programId, Integer cruiseId, Integer... fishingOperationIds) {
        return driver.loadCruise(programId, cruiseId, fishingOperationIds);
    }

    @Override
    public void lazyInit() {
        // this service does not used lazy init
    }

    @Override
    public void clearAllCaches() {
        driver.clearAllCaches();
    }

    @Override
    public <V> V invoke(Callable<V> call) {
        return driver.invoke(call);
    }

    @Override
    public <U extends UpdateSchemaContextSupport> void prepareUpdateSchemaContext(U context) {
        driver.prepareUpdateSchemaContext(context);
    }

    @Override
    public Version getSchemaVersion() {
        return driver.getSchemaVersion();
    }

    @Override
    public Version getSchemaVersionIfUpdate() {
        return driver.getSchemaVersionIfUpdate();
    }

    @Override
    public void updateSchema() {
        driver.updateSchema();
    }

    @Override
    public void sanityDb() {
        driver.sanityDb();
    }

    public static final DateFormat EXPORT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final String EXPORT_DIRECTORY_FORMAT = "tutti-%s-%s";

    /**
     * Export db as a zip archive (including the attachments and protocols).
     *
     * @param file archive file where to store
     * @since 1.0.2
     */
    public void exportDb(File file) {

        // can not do this operation on a opnened bd
        Preconditions.checkState(!isDbLoaded());

        // need a file to export
        Preconditions.checkNotNull(file);

        // create zip structure

        TuttiConfiguration config = context.getConfig();

        String directoryName = String.format(
                EXPORT_DIRECTORY_FORMAT,
                config.getVersion(),
                EXPORT_DATE_FORMAT.format(context.currentDate()));

        File structureDirectory = new File(config.newTempFile("exportdb"),
                                           directoryName);

        try {
            ApplicationIOUtil.forceMkdir(structureDirectory,
                                         t("tutti.io.mkDir.error", structureDirectory));

            if (log.isInfoEnabled()) {
                log.info("Export directory: " + structureDirectory);
            }

            ApplicationIOUtil.copyDirectory(config.getDbDirectory(),
                                            new File(structureDirectory, "db"),
                                            t("tutti.service.persistence.copyDirectory.db.error"));

            ApplicationIOUtil.copyDirectory(config.getDbAttachmentDirectory(),
                                            new File(structureDirectory, "meas_files"),
                                            t("tutti.service.persistence.copyDirectory.attachment.error"));

            // create zip
            ApplicationIOUtil.zip(structureDirectory, file,
                                  t("tutti.service.persistence.exportDb.zip.error", file));

        } finally {

            // delete temp files
            ApplicationIOUtil.forceDeleteOnExit(
                    structureDirectory,
                    t("tutti.service.persistence.exportDb.deleteTempDir.error", structureDirectory));
        }
    }

    public enum ImportStructureType {
        NORMAL, // normal structure with a db directory + optional meas_file directory
        INLINE
    }

    public ImportStructureType checkImportStructure(File file) {

        if (!file.exists()) {
            throw new ApplicationBusinessException(t("tutti.service.persistence.checkImportstructure.fileNotExist", file));
        }

        // check zip structure
        FileObject fileObject = ApplicationIOUtil.resolveFile(
                "zip:" + file.getAbsolutePath(), t("tutti.service.persistence.getArchive.error", file));


        FileObject[] children = ApplicationIOUtil.getChildren(fileObject, t("tutti.service.persistence.openArchive.error", file));

        if (children.length != 1) {
            throw new ApplicationBusinessException(t("tutti.service.persistence.checkImportstructure.tooManyChildren", file));
        }

        ImportStructureType result;

        fileObject = children[0];
        children = ApplicationIOUtil.getChildren(fileObject, t("tutti.service.persistence.openArchive.error", file));

        // try to detect a db directory
        boolean dbDirectoyDetected = false;
        for (FileObject child : children) {
            FileName name = child.getName();
            if (name.getBaseName().equals("db")) {
                dbDirectoyDetected = true;
                break;
            }
        }

        if (dbDirectoyDetected) {

            // NORMAL type (two directories)
            result = ImportStructureType.NORMAL;

            checkArchiveDb(file, fileObject, "db", true);
            checkArchiveDb(file, fileObject, "meas_files", false);
        } else {

            // INLINE type
            result = ImportStructureType.INLINE;

            // should have no directory
            for (FileObject child : children) {
                FileType type = ApplicationIOUtil.getType(child, "Could not get type of " + child);
                if (FileType.FOLDER.equals(type)) {
                    throw new ApplicationBusinessException(t("tutti.service.persistence.checkImportstructure.inlineForbidDirectory"));
                }
            }
        }

        if (log.isInfoEnabled()) {
            log.info("Database import type: " + result);
        }

        return result;
    }

    protected void checkArchiveDb(File file,
                                  FileObject fileObject,
                                  String dir,
                                  boolean required) {
        FileObject directory = ApplicationIOUtil.getChild(fileObject, dir, t("tutti.service.persistence.getChild.error", dir));
        if (directory == null) {

            String message = t("tutti.service.persistence.checkArchiveDb.error", file, dir);
            if (required) {
                throw new ApplicationBusinessException(message);
            }

            if (log.isWarnEnabled()) {
                log.warn(message);
            }
        }
    }

    /**
     * Import a db from a zip archive (including the attachments and protocols).
     *
     * @param importStructureType type of structure of import
     * @param file                archive file where to store
     * @since 1.0.2
     */
    public void importDb(ImportStructureType importStructureType, File file) {

        // can not do this operation on a opened db
        Preconditions.checkState(!isDbLoaded());

        // need a file to export
        Preconditions.checkNotNull(file);

        TuttiConfiguration config = context.getConfig();

        File target = config.getDataDirectory();

        if (log.isInfoEnabled()) {
            log.info("Import db to " + target);
        }
        FileObject fileObject = ApplicationIOUtil.resolveFile(
                "zip:" + file.getAbsolutePath(),
                t("tutti.service.persistence.getArchive.error", file));

        FileObject[] children = ApplicationIOUtil.getChildren(
                fileObject,
                t("tutti.service.persistence.openArchive.error", file));

        fileObject = children[0];

        switch (importStructureType) {

            case NORMAL:
                // nothing special to do
                break;

            case INLINE:
                target = new File(target, "db");
                ApplicationIOUtil.forceMkdir(
                        target,
                        t("tutti.service.persistence.createDbDirectory.error", file));


                break;
        }

        ApplicationIOUtil.explode(fileObject,
                                  target,
                                  new AllFileSelector(),
                                  t("tutti.service.persistence.extractArchive.error", file));
    }

    public boolean isDbLoaded() {
        return !(driver instanceof TuttiPersistenceNoDbImpl);
    }

    public List<Caracteristic> getDefaultIndividualObservationCaracteristics() {

        TuttiProtocol protocol = getProtocol();

        List<Caracteristic> result;
        if (protocol != null) {
            List<String> individualObservationPmfmId = getProtocol().getIndividualObservationPmfmId();
            if (CollectionUtils.isEmpty(individualObservationPmfmId)) {

                // no conf
                result = Lists.newArrayList();

            } else {
                result = Lists.newArrayListWithCapacity(individualObservationPmfmId.size());
                for (String id : individualObservationPmfmId) {
                    Caracteristic caracteristic = getCaracteristic(Integer.valueOf(id));
                    result.add(caracteristic);
                }
            }

        } else {
            // no default caracteristics to use
            result = Lists.newArrayList();
        }

        return result;
    }

    public List<Caracteristic> getLengthStepCaracteristics(List<Caracteristic> caracteristics) {

        // get loaded protocol
        TuttiProtocol protocol = getProtocol();

        List<Caracteristic> result;
        if (protocol != null) {

            result = Lists.newArrayListWithCapacity(
                    protocol.sizeLengthClassesPmfmId());

            Map<String, Caracteristic> allCaractericsById =
                    TuttiEntities.splitById(caracteristics);

            if (!protocol.isLengthClassesPmfmIdEmpty()) {
                for (String id : protocol.getLengthClassesPmfmId()) {
                    result.add(allCaractericsById.get(id));
                }
            }
        } else {
            // no default caracteristics to use
            result = Lists.newArrayList();
        }


        result = Collections.unmodifiableList(result);
        return result;
    }

    public List<Caracteristic> getMaturityCaracteristics(List<Caracteristic> caracteristics) {

        // get loaded protocol
        TuttiProtocol protocol = getProtocol();

        List<Caracteristic> result;
        if (protocol != null) {

            result = Lists.newArrayListWithCapacity(
                    protocol.sizeMaturityCaracteristics());

            Map<String, Caracteristic> allCaractericsById =
                    TuttiEntities.splitById(caracteristics);

            if (!protocol.isMaturityCaracteristicsEmpty()) {
                for (MaturityCaracteristic caracteristic : protocol.getMaturityCaracteristics()) {
                    result.add(allCaractericsById.get(caracteristic.getId()));
                }
            }
        } else {
            // no default caracteristics to use
            result = Lists.newArrayList();
        }


        result = Collections.unmodifiableList(result);
        return result;
    }

//    /**
//     * Return the speciesProtocol corresponding to the species of the given protocol.
//     *
//     * @param species the species to filter
//     * @return the speciesProtocol corresponding to the species of the given protocol.
//     * @since 2.6
//     */
//    public SpeciesProtocol getSpeciesProtocol(Species species) {
//        return TuttiProtocols.getSpeciesProtocol(species, getProtocol().getSpecies());
//    }

//    /**
//     * Return the benthosProtocol corresponding to the species of the given protocol.
//     *
//     * @param species the species to filter
//     * @return the benthosProtocol corresponding to the species of the given protocol.
//     * @since 2.6
//     */
//    public SpeciesProtocol getBenthosProtocol(Species species) {
//        return TuttiProtocols.getSpeciesProtocol(species, getProtocol().getBenthos());
//    }

    public static final TimeLog TIME_LOG = new TimeLog(PersistenceService.class);

    @Override
    public void init() {

        long t0 = TimeLog.getTime();

        if (log.isDebugEnabled()) {
            log.debug("Opening persistence service...");
        }

        TuttiConfiguration config = context.getConfig();
        TuttiConfiguration.setInstance(config);
        if (config.isDbExists()) {

            String jdbcUrl = config.getJdbcUrl();
            if (log.isInfoEnabled()) {
                log.info("Using database at " + jdbcUrl);
            }
            // can use adagio driver
            driver = TuttiPersistenceServiceLocator.getPersistenceService();

            if (config.isSanityDb()) {

                driver.sanityDb();
            }

        } else {

            driver = new TuttiPersistenceNoDbImpl();

            driver.init();
        }

        TIME_LOG.log(t0, "Persistence driver *" + driver.getImplementationName() + "* opened.");
    }

    @Override
    public void close() throws IOException {

        long t0 = TimeLog.getTime();

        String driverName = driver.getImplementationName();

        if (log.isDebugEnabled()) {
            log.debug("Closing persistence Service *" + driverName + "*...");
        }

        IOUtils.closeQuietly(driver);

        TIME_LOG.log(t0, "Persistence driver *" + driverName + "* closed.");
    }

    //------------------------------------------------------------------------//
    //-- Referential methods                                                --//
    //------------------------------------------------------------------------//

    public List<Vessel> getAllVessel() {
        List<Vessel> targetList = Lists.newArrayList(getAllFishingVessel());
        targetList.addAll(getAllScientificVessel());
        return targetList;
    }

    @Override
    public List<Vessel> getAllScientificVessel() {
        return driver.getAllScientificVessel();
    }

    @Override
    public List<Vessel> getAllFishingVessel() {
        return driver.getAllFishingVessel();
    }

    @Override
    public List<Vessel> getAllVesselWithObsoletes() {
        return driver.getAllVesselWithObsoletes();
    }

    @Override
    public List<Species> getAllSpecies() {
        return driver.getAllSpecies();
    }

    @Override
    public List<Species> getAllReferentSpecies() {
        return driver.getAllReferentSpecies();
    }

    @Override
    public List<Species> getAllReferentSpeciesWithObsoletes() {
        return driver.getAllReferentSpeciesWithObsoletes();
    }

    @Override
    public Species getSpeciesByReferenceTaxonId(Integer referenceTaxonId) {
        return driver.getSpeciesByReferenceTaxonId(referenceTaxonId);
    }

    @Override
    public Species getSpeciesByReferenceTaxonIdWithVernacularCode(Integer referenceTaxonId) {
        return driver.getSpeciesByReferenceTaxonIdWithVernacularCode(referenceTaxonId);
    }

    @Override
    public Map<Integer, Integer> getAllObsoleteReferentTaxons() {
        return driver.getAllObsoleteReferentTaxons();
    }

    @Override
    public List<Caracteristic> getAllCaracteristic() {
        return driver.getAllCaracteristic();
    }

    @Override
    public List<Caracteristic> getAllCaracteristicWithProtected() {
        return driver.getAllCaracteristicWithProtected();
    }

    @Override
    public List<Caracteristic> getAllCaracteristicForSampleCategory() {
        return driver.getAllCaracteristicForSampleCategory();
    }

    @Override
    public List<Caracteristic> getAllNumericCaracteristic() {
        return driver.getAllNumericCaracteristic();
    }

    @Override
    public List<TuttiLocation> getAllProgramZone() {
        return driver.getAllProgramZone();
    }

    @Override
    public List<TuttiLocation> getAllCountry() {
        return driver.getAllCountry();
    }

    @Override
    public List<TuttiLocation> getAllHarbour() {
        return driver.getAllHarbour();
    }

    @Override
    public List<TuttiLocation> getAllHarbourWithObsoletes() {
        return driver.getAllHarbourWithObsoletes();
    }

    @Override
    public TuttiLocation getLocation(String id) {
        return driver.getLocation(id);
    }

    public List<Gear> getAllGear() {
        List<Gear> targetList = Lists.newArrayList(getAllScientificGear());
        targetList.addAll(getAllFishingGear());
        return targetList;
    }

    @Override
    public List<Gear> getAllScientificGear() {
        return driver.getAllScientificGear();
    }

    @Override
    public List<Gear> getAllFishingGear() {
        return driver.getAllFishingGear();
    }

    @Override
    public List<Gear> getAllGearWithObsoletes() {
        return driver.getAllGearWithObsoletes();
    }

    @Override
    public List<Person> getAllPerson() {
        return driver.getAllPerson();
    }

    @Override
    public List<Person> getAllPersonWithObsoletes() {
        return driver.getAllPersonWithObsoletes();
    }

    @Override
    public List<ObjectType> getAllObjectType() {
        return driver.getAllObjectType();
    }

    @Override
    public ImmutableSet<Integer> getAllFishingOperationStratasAndSubstratasIdsForProgram(String zoneId) {
        return driver.getAllFishingOperationStratasAndSubstratasIdsForProgram(zoneId);
    }

    @Override
    public Multimap<TuttiLocation, TuttiLocation> getAllFishingOperationStratasAndSubstratas(String zoneId) {
        return driver.getAllFishingOperationStratasAndSubstratas(zoneId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrata(String zoneId) {
        return driver.getAllFishingOperationStrata(zoneId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrataWithObsoletes(String zoneId) {
        return driver.getAllFishingOperationStrataWithObsoletes(zoneId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrata(String zoneId, String strataId) {
        return driver.getAllFishingOperationSubStrata(zoneId, strataId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrataWithObsoletes(String zoneId, String strataId) {
        return driver.getAllFishingOperationSubStrataWithObsoletes(zoneId, strataId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocation(String zoneId, String strataId, String subStrataId) {
        return driver.getAllFishingOperationLocation(zoneId, strataId, subStrataId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocationWithObsoletes(String zoneId, String strataId, String subStrataId) {
        return driver.getAllFishingOperationLocationWithObsoletes(zoneId, strataId, subStrataId);
    }

    @Override
    public Caracteristic getSizeCategoryCaracteristic() {
        return driver.getSizeCategoryCaracteristic();
    }

    @Override
    public Caracteristic getSexCaracteristic() {
        return driver.getSexCaracteristic();
    }

    @Override
    public Caracteristic getSortedUnsortedCaracteristic() {
        return driver.getSortedUnsortedCaracteristic();
    }

    @Override
    public Caracteristic getMaturityCaracteristic() {
        return driver.getMaturityCaracteristic();
    }

    @Override
    public Caracteristic getAgeCaracteristic() {
        return driver.getAgeCaracteristic();
    }

    @Override
    public Caracteristic getMarineLitterCategoryCaracteristic() {
        return driver.getMarineLitterCategoryCaracteristic();
    }

    @Override
    public Caracteristic getMarineLitterSizeCategoryCaracteristic() {
        return driver.getMarineLitterSizeCategoryCaracteristic();
    }

    @Override
    public Caracteristic getVerticalOpeningCaracteristic() {
        return driver.getVerticalOpeningCaracteristic();
    }

    @Override
    public Caracteristic getHorizontalOpeningWingsCaracteristic() {
        return driver.getHorizontalOpeningWingsCaracteristic();
    }

    @Override
    public Caracteristic getHorizontalOpeningDoorCaracteristic() {
        return driver.getHorizontalOpeningDoorCaracteristic();
    }

    @Override
    public Caracteristic getDeadOrAliveCaracteristic() {
        return driver.getDeadOrAliveCaracteristic();
    }

    @Override
    public Caracteristic getCalcifiedStructureCaracteristic() {
        return driver.getCalcifiedStructureCaracteristic();
    }

    @Override
    public Caracteristic getPmfmIdCaracteristic() {
        return driver.getPmfmIdCaracteristic();
    }

    @Override
    public Caracteristic getWeightMeasuredCaracteristic() {
        return driver.getWeightMeasuredCaracteristic();
    }

    @Override
    public Caracteristic getCopyIndividualObservationModeCaracteristic() {
        return driver.getCopyIndividualObservationModeCaracteristic();
    }

    @Override
    public Caracteristic getSampleCodeCaracteristic() {
        return driver.getSampleCodeCaracteristic();
    }

    @Override
    public Caracteristic getCaracteristic(Integer pmfmId) {
        return driver.getCaracteristic(pmfmId);
    }

    @Override
    public boolean isVracBatch(SpeciesBatch speciesBatch) {
        return driver.isVracBatch(speciesBatch);
    }

    @Override
    public boolean isHorsVracBatch(SpeciesBatch speciesBatch) {
        return driver.isHorsVracBatch(speciesBatch);
    }

    @Override
    public Predicate<SpeciesBatch> getVracBatchPredicate() {
        return driver.getVracBatchPredicate();
    }

    @Override
    public boolean isTemporary(TuttiReferentialEntity entity) {
        return driver.isTemporary(entity);
    }

    @Override
    public Vessel getVessel(String vesselCode) {
        return driver.getVessel(vesselCode);
    }

    @Override
    public Person getPerson(Integer personId) {
        return driver.getPerson(personId);
    }

    @Override
    public Gear getGear(Integer gearCode) {
        return driver.getGear(gearCode);
    }

    @Override
    public ObjectType getObjectType(String objectTypeCode) {
        return driver.getObjectType(objectTypeCode);
    }

    @Override
    public List<Gear> addTemporaryGears(List<Gear> gears) {
        return driver.addTemporaryGears(gears);
    }

    @Override
    public List<Person> addTemporaryPersons(List<Person> persons) {
        return driver.addTemporaryPersons(persons);
    }

    @Override
    public List<Species> addTemporarySpecies(List<Species> species) {
        return driver.addTemporarySpecies(species);
    }

    @Override
    public List<Vessel> addTemporaryVessels(List<Vessel> vessels) {
        return driver.addTemporaryVessels(vessels);
    }

    @Override
    public List<Gear> updateTemporaryGears(List<Gear> gears) {
        return driver.updateTemporaryGears(gears);
    }

    @Override
    public List<Person> updateTemporaryPersons(List<Person> persons) {
        return driver.updateTemporaryPersons(persons);
    }

    @Override
    public List<Species> updateTemporarySpecies(List<Species> species) {
        return driver.updateTemporarySpecies(species);
    }

    @Override
    public List<Vessel> updateTemporaryVessels(List<Vessel> vessels) {
        return driver.updateTemporaryVessels(vessels);
    }

    @Override
    public List<Gear> linkTemporaryGears(List<Gear> gears) {
        return driver.linkTemporaryGears(gears);
    }

    @Override
    public List<Person> linkTemporaryPersons(List<Person> persons) {
        return driver.linkTemporaryPersons(persons);
    }

    @Override
    public List<Species> linkTemporarySpecies(List<Species> specieses) {
        return driver.linkTemporarySpecies(specieses);
    }

    @Override
    public List<Vessel> linkTemporaryVessels(List<Vessel> vessels) {
        return driver.linkTemporaryVessels(vessels);
    }

    @Override
    public void replaceGear(Gear source, Gear target, boolean delete) {
        driver.replaceGear(source, target, delete);
    }

    @Override
    public void replacePerson(Person source, Person target, boolean delete) {
        driver.replacePerson(source, target, delete);
    }

    @Override
    public void replaceSpecies(Species source, Species target, boolean delete) {
        driver.replaceSpecies(source, target, delete);
    }

    @Override
    public void replaceVessel(Vessel source, Vessel target, boolean delete) {
        driver.replaceVessel(source, target, delete);
    }

    @Override
    public void deleteTemporaryGear(Integer id) {
        driver.deleteTemporaryGear(id);
    }

    @Override
    public void deleteTemporaryGears(Collection<Integer> id) {
        driver.deleteTemporaryGears(id);
    }

    @Override
    public void deleteTemporarySpecies(Integer referenceTaxonId) {
        driver.deleteTemporarySpecies(referenceTaxonId);
    }

    @Override
    public void deleteTemporarySpecies(Collection<Integer> referenceTaxonIds) {
        driver.deleteTemporarySpecies(referenceTaxonIds);
    }

    @Override
    public void deleteTemporaryPerson(Integer id) {
        driver.deleteTemporaryPerson(id);
    }

    @Override
    public void deleteTemporaryPersons(Collection<Integer> ids) {
        driver.deleteTemporaryPersons(ids);
    }

    @Override
    public void deleteTemporaryVessel(String code) {
        driver.deleteTemporaryVessel(code);
    }

    @Override
    public void deleteTemporaryVessels(Collection<String> codes) {
        driver.deleteTemporaryVessels(codes);
    }

    @Override
    public boolean isTemporaryPersonUsed(Integer id) {
        return driver.isTemporaryPersonUsed(id);
    }

    @Override
    public boolean isTemporarySpeciesUsed(Integer referenceTaxonId) {
        return driver.isTemporarySpeciesUsed(referenceTaxonId);
    }

    @Override
    public boolean isTemporaryGearUsed(Integer id) {
        return driver.isTemporaryGearUsed(id);
    }

    @Override
    public boolean isTemporaryVesselUsed(String code) {
        return driver.isTemporaryVesselUsed(code);
    }

    @Override
    public String getLocationLabelByLatLong(Float latitude, Float longitude) {
        return driver.getLocationLabelByLatLong(latitude, longitude);
    }

    @Override
    public Integer getLocationIdByLatLong(Float latitude, Float longitude) {
        return driver.getLocationIdByLatLong(latitude, longitude);
    }

    //------------------------------------------------------------------------//
    //-- Attachment methods                                                 --//
    //------------------------------------------------------------------------//

    @Override
    public List<Attachment> getAllAttachments(ObjectTypeCode objectType, Integer objectId) {
        return driver.getAllAttachments(objectType, objectId);
    }

    @Override
    public File getAttachmentFile(String attachmentId) {
        return driver.getAttachmentFile(attachmentId);
    }

    @Override
    public Attachment createAttachment(Attachment attachment, File file) {
        return driver.createAttachment(attachment, file);
    }

    @Override
    public Attachment saveAttachment(Attachment attachment) {
        return driver.saveAttachment(attachment);
    }

    @Override
    public void deleteAttachment(String attachmentId) {
        driver.deleteAttachment(attachmentId);
    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Set<Integer> objectIds) {
        driver.deleteAllAttachment(objectType, objectIds);
    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Integer objectId) {
        driver.deleteAllAttachment(objectType, objectId);
    }

    //------------------------------------------------------------------------//
    //-- Program methods                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public List<Program> getAllProgram() {
        return driver.getAllProgram();
    }

    @Override
    public Program getProgram(String id) {
        return driver.getProgram(id);
    }

    @Override
    public Program createProgram(Program bean) {
        return driver.createProgram(bean);
    }

    @Override
    public Program saveProgram(Program bean) {
        return driver.saveProgram(bean);
    }

    //------------------------------------------------------------------------//
    //-- Cruise methods                                                     --//
    //------------------------------------------------------------------------//

    @Override
    public List<Integer> getAllCruiseId(String programId) {
        return driver.getAllCruiseId(programId);
    }

    @Override
    public List<Cruise> getAllCruise(String programId) {
        return driver.getAllCruise(programId);
    }

    @Override
    public Cruise getCruise(Integer id) {
        return driver.getCruise(id);
    }

    @Override
    public Cruise createCruise(Cruise bean) {
        return driver.createCruise(bean);
    }

    @Override
    public Cruise saveCruise(Cruise bean, boolean updateVessel, boolean updateGear) {
        return driver.saveCruise(bean, updateVessel, updateGear);
    }

    @Override
    public void setCruiseReadyToSynch(Integer cruiseId) {
        driver.setCruiseReadyToSynch(cruiseId);
    }

    @Override
    public CaracteristicMap getGearCaracteristics(Integer cruiseId, Integer gearId, short rankOrder) {
        return driver.getGearCaracteristics(cruiseId, gearId, rankOrder);
    }

    @Override
    public boolean isOperationUseGears(Integer cruiseId, Collection<Gear> gears) {
        return driver.isOperationUseGears(cruiseId, gears);
    }

    @Override
    public void saveGearCaracteristics(Gear gear, Cruise cruise) {
        driver.saveGearCaracteristics(gear, cruise);
    }

    //------------------------------------------------------------------------//
    //-- Protocol methods                                                   --//
    //------------------------------------------------------------------------//


    @Override
    public TuttiProtocol getProtocol() {
        return driver.getProtocol();
    }

    @Override
    public void setProtocol(TuttiProtocol protocol) {
        driver.setProtocol(protocol);
    }

    @Override
    public boolean isProtocolExist(String id) {
        return driver.isProtocolExist(id);
    }

    @Override
    public List<String> getAllProtocolNames() {
        return driver.getAllProtocolNames();
    }

    @Override
    public String getFirstAvailableName(String protocolName) {
        return driver.getFirstAvailableName(protocolName);
    }

    @Override
    public List<TuttiProtocol> getAllProtocol() {
        return driver.getAllProtocol();
    }

    @Override
    public List<TuttiProtocol> getAllProtocol(String programId) {
        return driver.getAllProtocol(programId);
    }

    @Override
    public List<String> getAllProtocolId() {
        return driver.getAllProtocolId();
    }

    @Override
    public TuttiProtocol getProtocol(String id) {
        TuttiProtocol protocol = driver.getProtocol(id);

        if (protocol.isUseCalcifiedPieceSampling() && context.getDataContext().isProgramFilled()) {

            // on vérifie que les zones sont valident
            Program program = context.getDataContext().getProgram();

            String programZoneId = program.getZone().getId();

            ImmutableSet<Integer> availableLocationIds = getAllFishingOperationStratasAndSubstratasIdsForProgram(programZoneId);

            if (protocol.isUseCalcifiedPieceSampling() && !protocol.isZoneEmpty()) {

                protocol.getZone().forEach(zone -> {

                    ImmutableSet<Integer> allLocationIds = Zones.getAllLocationIds(zone);

                    for (Integer locationId : allLocationIds) {

                        if (!availableLocationIds.contains(locationId)) {

                            throw new ApplicationBusinessException("Le protocole utilise une strate d'identifiant " + locationId + " non reconnue dans le système.");

                        }
                    }
                });
            }

        }

        return protocol;
    }

    @Override
    public TuttiProtocol getProtocolByName(String protocolName) {
        return driver.getProtocolByName(protocolName);
    }

    @Override
    public TuttiProtocol createProtocol(TuttiProtocol bean) {
        return driver.createProtocol(bean);
    }

    @Override
    public TuttiProtocol saveProtocol(TuttiProtocol bean) {
        return driver.saveProtocol(bean);
    }

    @Override
    public void deleteProtocol(String id) {
        driver.deleteProtocol(id);
    }

    //------------------------------------------------------------------------//
    //-- FishingOperation methods                                           --//
    //------------------------------------------------------------------------//

    @Override
    public int getFishingOperationCount(Integer cruiseId) {
        return driver.getFishingOperationCount(cruiseId);
    }

    @Override
    public List<Integer> getAllFishingOperationIds(Integer cruiseId) {
        return driver.getAllFishingOperationIds(cruiseId);
    }

    @Override
    public List<FishingOperation> getAllFishingOperation(Integer cruiseId) {
        return driver.getAllFishingOperation(cruiseId);
    }

    @Override
    public FishingOperation getFishingOperation(Integer id) {
        FishingOperation bean = driver.getFishingOperation(id);
        // see http://forge.codelutin.com/issues/2014
        if (bean.getGearShootingEndDate() == null) {
            bean.setGearShootingEndDate(bean.getGearShootingStartDate());
        }
        return bean;
    }

    @Override
    public List<Vessel> getFishingOperationSecondaryVessel(Integer fishingOperationId) {
        return driver.getFishingOperationSecondaryVessel(fishingOperationId);
    }

    @Override
    public FishingOperation createFishingOperation(FishingOperation bean) {

        // see http://forge.codelutin.com/issues/2014
        if (Objects.equals(bean.getGearShootingStartDate(), bean.getGearShootingEndDate())) {
            bean.setGearShootingEndDate(null);
        }
        return driver.createFishingOperation(bean);
    }

    @Override
    public FishingOperation saveFishingOperation(FishingOperation bean) {
        // see http://forge.codelutin.com/issues/2014
        if (Objects.equals(bean.getGearShootingStartDate(), bean.getGearShootingEndDate())) {
            bean.setGearShootingEndDate(null);
        }

        Optional<CruiseCache> optionalCruiseCache = getOptionalCruiseCache();

        boolean reloadSamplingCache = false;

        if (optionalCruiseCache.isPresent()) {

            CruiseCache cruiseCache = optionalCruiseCache.get();

            if (optionalCruiseCache.isPresent()) {

                Optional<CruiseSamplingCache> optionalSamplingCruiseCache = cruiseCache.getSamplingCruiseCache();

                if (optionalSamplingCruiseCache.isPresent()) {

                    // on doit vérifier si l'opération n'a pas changée de zone
                    CruiseSamplingCache cruiseSamplingCache = optionalSamplingCruiseCache.get();

                    FishingOperation oldFishingOperation = getFishingOperation(bean.getIdAsInt());

                    boolean zoneChanged = cruiseSamplingCache.isZoneChanged(oldFishingOperation, bean);

                    if (zoneChanged) {

                        // les strates ou sous-strates ont changées, il faut recalculer le cache des échantillons
                        if (log.isInfoEnabled()) {
                            log.info("Zone has changed for fishingOperation: " + bean + ", remove fishing operation from cruise cache.");
                        }
                        reloadSamplingCache = true;

                        cruiseSamplingCache.printInfos("Before removing fishing operation");

                        List<IndividualObservationBatch> individualObservations = getAllIndividualObservationBatchsForFishingOperation(bean.getIdAsInt());
                        cruiseCache.removeFishingOperation(oldFishingOperation, individualObservations);

                    }

                }

            }

        }

        FishingOperation fishingOperation = driver.saveFishingOperation(bean);

        if (optionalCruiseCache.isPresent() && reloadSamplingCache) {

            if (log.isInfoEnabled()) {
                log.info("Zone has changed for fishingOperation: " + bean + ", recompute fishing operation from cruise cache.");
            }

            CruiseCache cruiseCache = optionalCruiseCache.get();

            CruiseSamplingCache cruiseSamplingCache = cruiseCache.getSamplingCruiseCache().orElseGet(null);

            cruiseSamplingCache.printInfos("Before loading " + fishingOperation);

            CruiseCacheLoader cruiseCacheLoader = CruiseCacheLoader.newCacheLoader(this, context.getService(DecoratorService.class), null, cruiseCache);
            cruiseCacheLoader.loadCruiseCacheForFishingOperation(fishingOperation);

            cruiseSamplingCache.printInfos("After  loading " + fishingOperation);

        }

        return fishingOperation;
    }

    //FIXME Remove this ? can't find where it is used...
    @Override
    public Collection<FishingOperation> saveFishingOperations(Collection<FishingOperation> beans) {
        return driver.saveFishingOperations(beans);
    }

    @Override
    public void deleteFishingOperation(Integer id) {
        Optional<CruiseCache> optionalCruiseCache = getOptionalCruiseCache();
        if (optionalCruiseCache.isPresent()) {

            CruiseCache cruiseCache = optionalCruiseCache.get();
            if (log.isInfoEnabled()) {
                log.info("Remove fishing operation: " + id + " from cruiseSamplingCache: " + cruiseCache);
            }
            FishingOperation fishingOperation = getFishingOperation(id);

            List<IndividualObservationBatch> individualObservations = getAllIndividualObservationBatchsForFishingOperation(id);

            optionalCruiseCache.get().removeFishingOperation(fishingOperation, individualObservations);
        }
        driver.deleteFishingOperation(id);
    }

    //------------------------------------------------------------------------//
    //-- CatchBatch methods                                                 --//
    //------------------------------------------------------------------------//

    @Override
    public boolean isFishingOperationWithCatchBatch(Integer operationId) {
        return driver.isFishingOperationWithCatchBatch(operationId);
    }

    @Override
    public CatchBatch getCatchBatchFromFishingOperation(Integer id) throws InvalidBatchModelException {
        return driver.getCatchBatchFromFishingOperation(id);

    }

    @Override
    public CatchBatch createCatchBatch(CatchBatch bean) {
        return driver.createCatchBatch(bean);
    }

    @Override
    public CatchBatch saveCatchBatch(CatchBatch bean) {
        return driver.saveCatchBatch(bean);
    }

    @Override
    public void deleteCatchBatch(Integer fishingOperationId) {
        driver.deleteCatchBatch(fishingOperationId);
    }

    @Override
    public void recomputeCatchBatchSampleRatios(Integer fishingOperationId) {
        driver.recomputeCatchBatchSampleRatios(fishingOperationId);
    }

    //------------------------------------------------------------------------//
    //-- Species Batch methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<SpeciesBatch> getRootSpeciesBatch(Integer fishingOperationId, boolean validateTree) {
        return driver.getRootSpeciesBatch(fishingOperationId, validateTree);
    }

    @Override
    public Set<Integer> getBatchChildIds(Integer id) {
        return driver.getBatchChildIds(id);
    }

    @Override
    public SpeciesBatch createSpeciesBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {
        return driver.createSpeciesBatch(bean, parentBatchId, computeRankOrder);
    }

    @Override
    public Collection<SpeciesBatch> createSpeciesBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {
        return driver.createSpeciesBatches(fishingOperationId, beans);
    }

    @Override
    public SpeciesBatch saveSpeciesBatch(SpeciesBatch bean) {
        return driver.saveSpeciesBatch(bean);
    }

    @Override
    public void deleteSpeciesBatch(Integer id) {
        beforeDeleteBatch(id, true);
        driver.deleteSpeciesBatch(id);
    }

    @Override
    public void deleteSpeciesSubBatch(Integer id) {
        beforeDeleteBatch(id, false);
        driver.deleteSpeciesSubBatch(id);
    }

    @Override
    public void changeSpeciesBatchSpecies(Integer batchId, Species species) {
        driver.changeSpeciesBatchSpecies(batchId, species);
    }

    @Override
    public List<SpeciesBatch> getAllSpeciesBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        return driver.getAllSpeciesBatchToConfirm(fishingOperationId);
    }

    @Override
    public List<SpeciesBatchFrequency> getAllSpeciesBatchFrequency(Integer speciesBatchId) {
        return driver.getAllSpeciesBatchFrequency(speciesBatchId);
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllSpeciesBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        return driver.getAllSpeciesBatchFrequencyForBatch(batchContainer);
    }

    @Override
    public List<SpeciesBatchFrequency> saveSpeciesBatchFrequency(Integer speciesBatchId,
                                                                 List<SpeciesBatchFrequency> frequencies) {
        return driver.saveSpeciesBatchFrequency(speciesBatchId, frequencies);
    }

    //------------------------------------------------------------------------//
    //-- Benthos Batch methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<SpeciesBatch> getRootBenthosBatch(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException {
        return driver.getRootBenthosBatch(fishingOperationId, validateTree);
    }

    @Override
    public SpeciesBatch createBenthosBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {
        return driver.createBenthosBatch(bean, parentBatchId, computeRankOrder);
    }

    @Override
    public Collection<SpeciesBatch> createBenthosBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {
        return driver.createBenthosBatches(fishingOperationId, beans);
    }

    @Override
    public SpeciesBatch saveBenthosBatch(SpeciesBatch bean) {
        return driver.saveBenthosBatch(bean);
    }

    @Override
    public void deleteBenthosBatch(Integer id) {
        beforeDeleteBatch(id, true);
        driver.deleteBenthosBatch(id);
    }

    @Override
    public void deleteBenthosSubBatch(Integer id) {
        beforeDeleteBatch(id, false);
        driver.deleteBenthosSubBatch(id);
    }

    @Override
    public void changeBenthosBatchSpecies(Integer batchId, Species species) {
        driver.changeBenthosBatchSpecies(batchId, species);
    }

    @Override
    public List<SpeciesBatch> getAllBenthosBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        return driver.getAllBenthosBatchToConfirm(fishingOperationId);
    }

    @Override
    public List<SpeciesBatchFrequency> getAllBenthosBatchFrequency(Integer benthosBatchId) {
        return driver.getAllBenthosBatchFrequency(benthosBatchId);
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllBenthosBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        return driver.getAllBenthosBatchFrequencyForBatch(batchContainer);
    }

    @Override
    public List<SpeciesBatchFrequency> saveBenthosBatchFrequency(Integer benthosBatchId, List<SpeciesBatchFrequency> frequencies) {
        return driver.saveBenthosBatchFrequency(benthosBatchId, frequencies);
    }

    //------------------------------------------------------------------------//
    //-- MarineLitter Batch methods                                         --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<MarineLitterBatch> getRootMarineLitterBatch(Integer fishingOperationId) {
        return driver.getRootMarineLitterBatch(fishingOperationId);
    }

    @Override
    public MarineLitterBatch createMarineLitterBatch(MarineLitterBatch bean) {
        return driver.createMarineLitterBatch(bean);
    }

    @Override
    public Collection<MarineLitterBatch> createMarineLitterBatches(Integer fishingOperationId, Collection<MarineLitterBatch> beans) {
        return driver.createMarineLitterBatches(fishingOperationId, beans);
    }

    @Override
    public MarineLitterBatch saveMarineLitterBatch(MarineLitterBatch bean) {
        return driver.saveMarineLitterBatch(bean);
    }

    @Override
    public void deleteMarineLitterBatch(Integer id) {
        driver.deleteMarineLitterBatch(id);
    }

    //------------------------------------------------------------------------//
    //-- Accidental Batch methods                                           --//
    //------------------------------------------------------------------------//

    @Override
    public List<AccidentalBatch> getAllAccidentalBatch(Integer fishingOperationId) {
        return driver.getAllAccidentalBatch(fishingOperationId);
    }

    @Override
    public Collection<AccidentalBatch> createAccidentalBatches(Collection<AccidentalBatch> beans) {
        return driver.createAccidentalBatches(beans);
    }

    @Override
    public AccidentalBatch createAccidentalBatch(AccidentalBatch bean) {
        return driver.createAccidentalBatch(bean);
    }

    @Override
    public AccidentalBatch saveAccidentalBatch(AccidentalBatch bean) {
        return driver.saveAccidentalBatch(bean);
    }

    @Override
    public void deleteAccidentalBatch(String id) {
        driver.deleteAccidentalBatch(id);
    }

    @Override
    public void deleteAccidentalBatchForFishingOperation(Integer fishingOperationId) {
        driver.deleteAccidentalBatchForFishingOperation(fishingOperationId);
    }

    //------------------------------------------------------------------------//
    //-- IndividualObservation Batch methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForBatch(Integer batchId) {
        return driver.getAllIndividualObservationBatchsForBatch(batchId);
    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForFishingOperation(Integer fishingOperationId) {
        return driver.getAllIndividualObservationBatchsForFishingOperation(fishingOperationId);
    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForCruise(Integer cruiseId) {
        return driver.getAllIndividualObservationBatchsForCruise(cruiseId);
    }

    @Override
    public boolean isSamplingCodeAvailable(Integer cruiseId, Integer referenceTaxonId, String samplingCodeSuffix) {
        return driver.isSamplingCodeAvailable(cruiseId, referenceTaxonId, samplingCodeSuffix);
    }

    @Override
    public List<IndividualObservationBatch> createIndividualObservationBatches(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Optional<CruiseCache> optionalCruiseCache = getOptionalCruiseCache();

        if (optionalCruiseCache.isPresent()) {

            // on supprime du cache les observation individuelles de l'opération
            CruiseCache cruiseCache = optionalCruiseCache.get();

            List<IndividualObservationBatch> individualObservationsToRemove = getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt());

            if (log.isInfoEnabled()) {
                log.info("Remove from cruise sampling cache: " + fishingOperation + " with " + individualObservationsToRemove.size() + " individual observations.");
            }
            cruiseCache.removeFishingOperation(fishingOperation, individualObservationsToRemove);

        }

        List<IndividualObservationBatch> individualObservationBatches = driver.createIndividualObservationBatches(fishingOperation, individualObservations);

        if (optionalCruiseCache.isPresent()) {

            // on ajoute les observation individuelles de l'opération du cache
            CruiseCache cruiseCache = optionalCruiseCache.get();

            List<IndividualObservationBatch> individualObservationsToAdd = getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt());
            if (log.isInfoEnabled()) {
                log.info("Add to cruise sampling cache: " + fishingOperation + " with " + individualObservationsToAdd.size() + " individual observations.");
            }
            cruiseCache.addFishingOperation(fishingOperation, individualObservationsToAdd);

        }

        return individualObservationBatches;
    }

    @Override
    public List<IndividualObservationBatch> saveBatchIndividualObservation(Integer batchId, List<IndividualObservationBatch> individualObservation) {
        return driver.saveBatchIndividualObservation(batchId, individualObservation);
    }

    @Override
    public void deleteAllIndividualObservationsForFishingOperation(Integer fishingOperationId) {
        driver.deleteAllIndividualObservationsForFishingOperation(fishingOperationId);
    }

    @Override
    public void deleteAllIndividualObservationsForBatch(Integer speciesBatchId) {
        driver.deleteAllIndividualObservationsForBatch(speciesBatchId);
    }

    protected void beforeDeleteBatch(Integer id, boolean addBatchId) {

        Optional<CruiseCache> optionalCruiseCache = getOptionalCruiseCache();
        if (optionalCruiseCache.isPresent()) {

            CruiseCache cruiseCache = optionalCruiseCache.get();

            FishingOperation fishingOperation = context.getDataContext().getFishingOperation();

            Set<Integer> batchIds = new LinkedHashSet<>();
            if (addBatchId) {
                batchIds.add(id);
            }
            batchIds.addAll(getBatchChildIds(id));

            batchIds.forEach(speciesBatchId -> {

                List<IndividualObservationBatch> individualObservations = getAllIndividualObservationBatchsForBatch(speciesBatchId);

                if (!individualObservations.isEmpty()) {

                    if (log.isInfoEnabled()) {
                        log.info("Remove for species or benthos batch: " + speciesBatchId + " - " + individualObservations.size() + " individual observations from cruiseSamplingCache: " + cruiseCache);
                    }

                    cruiseCache.removeIndividualObservations(fishingOperation, individualObservations);

                }

            });

        }

    }

    private Optional<CruiseCache> getOptionalCruiseCache() {
        return context.getDataContext().getOptionalCruiseCache();
    }

}
