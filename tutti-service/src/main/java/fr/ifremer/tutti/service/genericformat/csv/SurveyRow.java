package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;

import java.util.Date;
import java.util.List;

/**
 * A row in a survey export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class SurveyRow extends RowWithCruiseContextSupport {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_COUNTRY = "country";

    public static final String PROPERTY_ID_SISMER = "idSismer";

    public static final String PROPERTY_OBJECT_ID = "objectId";

    public static SurveyRow newEmptyInstance() {
        SurveyRow row = new SurveyRow();
        row.forImport();
        return row;
    }

    protected TuttiLocation country;

    private Integer objectId;

    public void setCountry(TuttiLocation country) {
        this.country = country;
    }

    public void setObjectId(Integer id) {
        this.objectId = id;
    }

    public void setDepartureLocation(TuttiLocation departureLocation) {
        getCruise().setDepartureLocation(departureLocation);
    }

    public void setReturnLocation(TuttiLocation returnLocation) {
        getCruise().setReturnLocation(returnLocation);
    }

    public void setName(String name) {
        getCruise().setName(name);
    }

    public void setHeadOfMission(List<Person> headOfMission) {
        getCruise().setHeadOfMission(headOfMission);
    }

    public void setMultirigNumber(Integer multirigNumber) {
        getCruise().setMultirigNumber(multirigNumber);
    }

    public void setHeadOfSortRoom(List<Person> headOfSortRoom) {
        getCruise().setHeadOfSortRoom(headOfSortRoom);
    }

    public void setEndDate(Date endDate) {
        getCruise().setEndDate(endDate);
    }

    public void setGear(List<GearWithOriginalRankOrder> gear) {
        getCruise().setGear(gear);
    }

    public void setComment(String comment) {
        getCruise().setComment(comment);
    }

    public void setVessel(Vessel vessel) {
        getCruise().setVessel(vessel);
    }

    public TuttiLocation getZone() {
        return getCruise().getProgram().getZone();
    }

    public Vessel getVessel() {
        return getCruise().getVessel();
    }

    public TuttiLocation getCountry() {
        return country;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public String getName() {
        return getCruise().getName();
    }

    public List<GearWithOriginalRankOrder> getGear() {
        return getCruise().getGear();
    }

    public TuttiLocation getDepartureLocation() {
        return getCruise().getDepartureLocation();
    }

    public Date getEndDate() {
        return getCruise().getEndDate();
    }

    public TuttiLocation getReturnLocation() {
        return getCruise().getReturnLocation();
    }

    public String getComment() {
        return getCruise().getComment();
    }

    public List<Person> getHeadOfMission() {
        return getCruise().getHeadOfMission();
    }

    public List<Person> getHeadOfSortRoom() {
        return getCruise().getHeadOfSortRoom();
    }

    public Integer getMultirigNumber() {
        return getCruise().getMultirigNumber();
    }

    // see http://forge.codelutin.com/issues/2877
    public String getIdSismer() {
        return "";
    }
}
