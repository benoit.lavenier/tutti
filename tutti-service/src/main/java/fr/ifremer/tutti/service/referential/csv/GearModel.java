package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import org.nuiton.csv.Common;

import static org.nuiton.i18n.I18n.t;

/**
 * Model to import / export {@link Gear} in csv format.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class GearModel extends AbstractTuttiImportExportModel<GearRow> {

    public static GearModel forExport(char separator) {

        GearModel exportModel = new GearModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static GearModel forImport(char separator) {

        GearModel importModel = new GearModel(separator);
        importModel.forImport();
        return importModel;

    }

    @Override
    public GearRow newEmptyInstance() {
        return new GearRow();
    }

    protected GearModel(char separator) {
        super(separator);
    }

    protected void forImport() {

        newMandatoryColumn(GearRow.PROPERTY_ID, new TemporaryReferentialEntityIdParser(
                t("tutti.service.referential.import.gear.error.idNotNegative")){

            @Override
            protected boolean isTemporaryId(String parse) {
                int id = Integer.parseInt(parse);
                return Gears.isTemporaryId(id);
            }
        });
        newMandatoryColumn(GearRow.PROPERTY_NAME);
        newMandatoryColumn(GearRow.PROPERTY_LABEL);
        newMandatoryColumn(GearRow.PROPERTY_SCIENTIFIC_GEAR, Common.PRIMITIVE_BOOLEAN);
        newMandatoryColumn(GearRow.PROPERTY_TO_DELETE, TuttiCsvUtil.BOOLEAN);

    }

    protected void forExport() {

        newColumnForExport(GearRow.PROPERTY_ID);
        newColumnForExport(GearRow.PROPERTY_NAME);
        newColumnForExport(GearRow.PROPERTY_LABEL);
        newColumnForExport(GearRow.PROPERTY_SCIENTIFIC_GEAR, Common.PRIMITIVE_BOOLEAN);
        newColumnForExport(GearRow.PROPERTY_TO_DELETE);

    }

}
