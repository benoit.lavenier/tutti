package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.BeanUtilsBean;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.csv.ValueFormatter;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class BeanPropertyFormatter<E> implements ValueFormatter<E> {

    protected final String defaultNullValue;

    protected String propertyName;

    public BeanPropertyFormatter(String propertyName, String defaultNullValue) {
        this.propertyName = propertyName;
        this.defaultNullValue = defaultNullValue;
    }

    @Override
    public String format(E value) {
        if (value == null) {
            return defaultNullValue;
        }
        try {
            return (String) BeanUtilsBean.getInstance().getPropertyUtils().getNestedProperty(value, propertyName);
        } catch (Exception e) {
            throw new ImportRuntimeException(t("tutti.service.cvs.format.error"), e);//"Could not format value"
        }
    }
}
