package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.3.1
 */
public abstract class AbstractFishingOperationRowModel<FO extends AbstractFishingOperationRow> extends AbstractTuttiImportExportModel<FO> {

    public AbstractFishingOperationRowModel() {
        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(FishingOperationRow.STATION_NUMBER);
        newColumnForImportExport(FishingOperationRow.OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newColumnForImportExport(FishingOperationRow.MULTIRIG_AGGREGATION);
        newColumnForImportExport(FishingOperationRow.DATE, TuttiCsvUtil.DAY_TIME);
    }
}
