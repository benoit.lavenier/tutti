package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;

import java.io.Serializable;

/**
 * Created on 3/26/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class AttachmentRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static AttachmentRow newEmptyInstance() {
        AttachmentRow row = new AttachmentRow();
        row.forImport();
        return row;
    }

    private Attachment attachment;

    public void forImport() {
        setAttachment(Attachments.newAttachment());
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public void setName(String name) {
        attachment.setName(name);
    }

    public void setObjectId(Integer objectId) {
        attachment.setObjectId(objectId);
    }

    public void setPath(String path) {
        attachment.setPath(path);
    }

    public void setComment(String comment) {
        attachment.setComment(comment);
    }

    public void setObjectType(ObjectTypeCode objectType) {
        attachment.setObjectType(objectType);
    }

    public String getName() {
        return attachment.getName();
    }

    public Integer getObjectId() {
        return attachment.getObjectId();
    }

    public String getPath() {
        return attachment.getPath();
    }

    public String getComment() {
        return attachment.getComment();
    }

    public ObjectTypeCode getObjectType() {
        return attachment.getObjectType();
    }

    public Attachment toAttachment(Integer objectId) {

        Attachment result = Attachments.newAttachment();
        result.setObjectId(objectId);
        result.setObjectType(getObjectType());
        result.setName(getName());
        result.setComment(getComment());
        result.setPath(getPath());
        return result;

    }
}
