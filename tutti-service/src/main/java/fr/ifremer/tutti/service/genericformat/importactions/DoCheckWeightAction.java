package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.genericformat.GenericFormatCheckDataService;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class DoCheckWeightAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DoCheckWeightAction.class);

    private final GenericFormatCheckDataService checkDataService;

    public DoCheckWeightAction(GenericFormatImportContext importContext, GenericFormatCheckDataService checkDataService) {
        super(importContext);
        this.checkDataService = checkDataService;
    }

    @Override
    protected boolean canExecute() {
        return importContext.getImportRequest().isCheckWeights()
               && importContext.isTechnicalFilesValid()
               && importContext.getOperationFileResult().isValid()
               && importContext.getCatchFileResult().isValid()
               && importContext.getMarineLitterFileResult().isValid();
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Will check weights.");
        }

        importContext.doActionOnCruiseContexts((cruiseContext, progressionModel) -> {

            for (GenericFormatImportOperationContext operationContext : cruiseContext) {

                progressionModel.increments(t("tutti.service.genericFormat.checkWeights.fishingOperation", cruiseContext.getCruiseLabel(), operationContext.getFishingOperationLabel()));

                Set<String> errors = checkDataService.checkFishingOperation(operationContext.getFishingOperation().getIdAsInt());
                operationContext.addCheckErrors(errors);

            }

        });

    }

}
