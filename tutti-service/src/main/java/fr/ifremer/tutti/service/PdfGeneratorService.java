package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class PdfGeneratorService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PdfGeneratorService.class);

    protected Configuration freemarkerConfiguration;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);

        freemarkerConfiguration = new Configuration();

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(getClass(), "/ftl");
        freemarkerConfiguration.setTemplateLoader(templateLoader);

        // pour les maps dans les template (entre autre)
        freemarkerConfiguration.setObjectWrapper(new BeansWrapper());

    }

    public void generatePdf(File targetFile, Locale locale, String templateName, Object model) {

        if (log.isInfoEnabled()) {
            log.info("Will generate pdf from template " + templateName + " at " + targetFile);
        }
        try {

            // Get freemarker template
            Template mapTemplate = freemarkerConfiguration.getTemplate(templateName, locale);

            Writer out = new StringWriter();
            mapTemplate.process(model, out);

            out.flush();

            // render template output as pdf
            try (OutputStream os = new FileOutputStream(targetFile)) {

                ITextRenderer renderer = new ITextRenderer();
                renderer.setDocumentFromString(out.toString());
                renderer.layout();
                renderer.createPDF(os);

                os.close();
            }

        } catch (Exception ex) {
            throw new ApplicationTechnicalException(t("tutti.service.exportPdf.error", targetFile), ex);
        }
    }
}
