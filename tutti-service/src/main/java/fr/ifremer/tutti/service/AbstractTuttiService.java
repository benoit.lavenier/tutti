package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

/**
 * Sammoa service support.
 *
 * This simple implementation of {@link TuttiService}, offer to keep the
 * {@link #context}.
 *
 * <strong>Note:</strong> should be used for any service! and never
 * instanciated by hand but via {@link TuttiServiceContext#getService(Class)} method.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public abstract class AbstractTuttiService implements TuttiService {

    protected TuttiServiceContext context;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        this.context = context;
    }

    public <S extends TuttiService> S getService(Class<S> serviceType) {
        return context.getService(serviceType);
    }

    @Override
    public void close() throws IOException {
        // by default nothing to close
    }

//    protected void compressZipFile(File zipFile, File directory) {
//
//        try {
//            FileUtils.forceMkdir(zipFile.getParentFile());
//
//            try {
//                ZipUtil.compress(zipFile, directory);
//            } finally {
//                FileUtils.deleteDirectory(directory);
//
//            }
//        } catch (IOException e) {
//            throw new ApplicationTechnicalException(t("tutti.service.compressZipFile.error", directory, zipFile), e);
//        }
//    }
}
