package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceNoDbImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Persistence service which always use the mock driver (means db is closed).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ClosedPersistenceService extends PersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ClosedPersistenceService.class);

    @Override
    public void init() {

        if (log.isInfoEnabled()) {
            log.info("Open persistence driver " + getImplementationName());
        }

        driver = new TuttiPersistenceNoDbImpl();

        if (log.isInfoEnabled()) {
            log.info("Will open persistence driver " +
                     driver.getImplementationName());
        }

        driver.init();
    }

    @Override
    public boolean isDbLoaded() {
        return false;
    }
}
