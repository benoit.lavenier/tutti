package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.service.cruise.CruiseCacheAble;
import org.apache.commons.lang3.mutable.MutableInt;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Contient le cache des codes de prélèvement disponibles.
 *
 * Created on 16/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class SamplingCodeCache implements CruiseCacheAble {

    /**
     * Le code prélèvement le plus grand pour chaque espèce.
     */
    private final Map<Integer, MutableInt> highestSamplingCodeBySpecies;

    public SamplingCodeCache() {
        this.highestSamplingCodeBySpecies = new TreeMap<>();
    }

    @Override
    public void addFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(individualObservations);
        addIndividualObservations(fishingOperation, individualObservations);

    }

    @Override
    public void removeFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(individualObservations);
        removeIndividualObservations(fishingOperation, individualObservations);

    }

    @Override
    public void addIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(individualObservations);

        individualObservations
                .stream()
                .filter(batch -> batch.getSamplingCode() != null)
                .forEach(batch -> addSamplingCode(batch.getSpecies().getReferenceTaxonId(), batch.getSamplingCode()));

    }

    @Override
    public void removeIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(individualObservations);

        individualObservations
                .stream()
                .filter(batch -> batch.getSamplingCode() != null)
                .forEach(batch -> removeSamplingCode(batch.getSpecies().getReferenceTaxonId(), batch.getSamplingCode()));

    }

    @Override
    public void close() throws IOException {
        highestSamplingCodeBySpecies.clear();
    }

    public int getNextSamplingCodeId(int speciesId) {
        MutableInt samplingCode = highestSamplingCodeBySpecies.get(speciesId);
        return (samplingCode == null ? 0 : samplingCode.intValue()) + 1;
    }

    public int addSamplingCode(int speciesId, String samplingCode) {
        int code = SamplingCodePrefix.extractSamplingCodeIdFromSamplingCode(samplingCode);

        return highestSamplingCodeBySpecies.compute(speciesId,
                                                    (key, highestSamplingCode) -> {
                                                        if (highestSamplingCode == null) {
                                                            highestSamplingCode = new MutableInt(code);
                                                        } else {
                                                            int nexValue = Math.max(highestSamplingCode.intValue(), code);
                                                            highestSamplingCode.setValue(nexValue);
                                                        }
                                                        return highestSamplingCode;
                                                    }).intValue();
    }

    public void removeSamplingCode(int speciesId, String samplingCode) {

        int code = SamplingCodePrefix.extractSamplingCodeIdFromSamplingCode(samplingCode);

        // decrement the highest sampling code if it is this code
        MutableInt samplingCodeFound = highestSamplingCodeBySpecies.get(speciesId);
        if (samplingCodeFound != null && code == samplingCodeFound.intValue()) {
            samplingCodeFound.decrement();
        }

    }

}
