package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

/**
 * Created on 11/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class CatchBatchRowModel extends AbstractFishingOperationRowModel<CatchBatchRow> {

    public CatchBatchRowModel() {

        newColumnForImportExport(CatchBatchRow.CATCH_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);
        newColumnForImportExport(CatchBatchRow.CATCH_TOTAL_REJECTED_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);
        newColumnForImportExport(CatchBatchRow.SPECIES_TOTAL_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);
        newColumnForImportExport(CatchBatchRow.BENTHOS_TOTAL_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);
        newColumnForImportExport(CatchBatchRow.MARINE_LITTER_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

    }

    @Override
    public CatchBatchRow newEmptyInstance() {
        return new CatchBatchRow();
    }
}
