package fr.ifremer.tutti.service.cruise;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.service.sampling.CruiseSamplingCache;
import fr.ifremer.tutti.service.sampling.SamplingCodeCache;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * Le cache de données pour une campagne. Ce cache contient tous les caches plus spécifiques
 * (codes de prélèvement, algorithme de prélèvement de pièces calcifiées,...).
 *
 * Created on 16/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class CruiseCache implements CruiseCacheAble {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CruiseCache.class);
    /**
     * L'identifiant de la campagne associée à ce cache.
     */
    private final int cruiseId;
    /**
     * L'identificant du protocol associé à ce cache.
     */
    private final String protocolId;
    /**
     * Le cache optioanel lié à l'algorithme de prélèvement des pièces calcifiées.
     */
    private final CruiseSamplingCache cruiseSamplingCache;
    /**
     * Le cache des codes de prélèvement disponibles.
     */
    private final SamplingCodeCache samplingCodeCache;

    public CruiseCache(Integer cruiseId, String protocolId, CruiseSamplingCache cruiseSamplingCache, SamplingCodeCache samplingCodeCache) {
        Objects.requireNonNull(cruiseId);
        Objects.requireNonNull(samplingCodeCache);
        this.cruiseId = cruiseId;
        this.protocolId = protocolId;
        this.cruiseSamplingCache = cruiseSamplingCache;
        this.samplingCodeCache = samplingCodeCache;
    }

    public boolean isCacheUpToDate(Integer cruiseId, String protocolId) {
        return Objects.equals(this.cruiseId, cruiseId) && Objects.equals(this.protocolId, protocolId);
    }

    public boolean useSamplingCache() {
        return cruiseSamplingCache != null;
    }

    public Integer getCruiseId() {
        return cruiseId;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public Optional<CruiseSamplingCache> getSamplingCruiseCache() {
        return Optional.ofNullable(cruiseSamplingCache);
    }

    public SamplingCodeCache getSamplingCodeCache() {
        return samplingCodeCache;
    }

    @Override
    public void addIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        if (useSamplingCache()) {
            cruiseSamplingCache.addIndividualObservations(fishingOperation, individualObservations);
        }

        samplingCodeCache.addIndividualObservations(fishingOperation, individualObservations);

    }

    @Override
    public void removeIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        if (useSamplingCache()) {
            cruiseSamplingCache.removeIndividualObservations(fishingOperation, individualObservations);
        }

        samplingCodeCache.removeIndividualObservations(fishingOperation, individualObservations);

    }

    @Override
    public void addFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        if (useSamplingCache()) {
            cruiseSamplingCache.addFishingOperation(fishingOperation, individualObservations);
        }

        samplingCodeCache.addFishingOperation(fishingOperation, individualObservations);

    }

    @Override
    public void removeFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        if (useSamplingCache()) {
            cruiseSamplingCache.removeFishingOperation(fishingOperation, individualObservations);
        }

        samplingCodeCache.removeFishingOperation(fishingOperation, individualObservations);

    }

    @Override
    public void close() {

        if (log.isInfoEnabled()) {
            log.info("Closing cruise cache: " + this);
        }
        if (useSamplingCache()) {
            IOUtils.closeQuietly(cruiseSamplingCache);
        }
        IOUtils.closeQuietly(samplingCodeCache);
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper toStringHelper = MoreObjects.toStringHelper(this);
        if (cruiseSamplingCache != null) {
            toStringHelper.add("cruiseSamplingCache", cruiseSamplingCache);
        }
        return toStringHelper.add("samplingCodeCache", samplingCodeCache).toString();
    }
}
