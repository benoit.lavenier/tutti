package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporaryPerson;
import fr.ifremer.tutti.service.referential.csv.PersonRow;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporaryPerson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class ReferentialTemporaryPersonService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ReferentialTemporaryPersonService.class);

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
    }

    public ReferentialImportRequest<Person, Integer> createReferentialImportRequest() {

        List<Person> allPersons = persistenceService.getAllPerson();
        return new ReferentialImportRequest<>(allPersons, TuttiEntities.<Person>newIdAstIntFunction(), Persons.GET_FULL_NAME);

    }

    public ReferentialImportResult<Person> importTemporaryPerson(File file) {

        if (log.isInfoEnabled()) {
            log.info("Will import persons from file: " + file);
        }

        ReferentialImportRequest<Person, Integer> requestResult = createReferentialImportRequest();

        try (CsvConsumerForTemporaryPerson consumer = new CsvConsumerForTemporaryPerson(file.toPath(), getCsvSeparator(), true)) {

            for (ImportRow<PersonRow> bean : consumer) {

                consumer.checkRow(bean, persistenceService, decoratorService, requestResult);

            }

        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.import.persons.error", file), e);
        }

        return executeImportRequest(requestResult);

    }

    public ReferentialImportResult<Person> executeImportRequest(ReferentialImportRequest<Person, Integer> requestResult) {

        ReferentialImportResult<Person> result = new ReferentialImportResult<>();

        if (requestResult.withEntitiesToDelete()) {

            List<Integer> idsToDelete = requestResult.getIdsToDelete();
            persistenceService.deleteTemporaryPersons(idsToDelete);
            result.setNbRefDeleted(idsToDelete.size());

        }

        if (requestResult.withEntitiesToAdd()) {

            List<Person> entitiesToAdd = requestResult.getEntitiesToAdd();
            List<Person> entitiesAdded = persistenceService.addTemporaryPersons(entitiesToAdd);
            result.addAllRefsAdded(entitiesAdded);

        }

        if (requestResult.withEntitiesToUpdate()) {

            List<Person> entitiesToUpdate = requestResult.getEntitiesToUpdate();
            List<Person> entitiesUpdated = persistenceService.updateTemporaryPersons(entitiesToUpdate);
            result.addAllRefsUpdated(entitiesUpdated);

        }

        if (requestResult.withEntitiesToLink()) {

            List<Person> entitiesToLink = requestResult.getEntitiesToLink();
            List<Person> entitiesLinked = persistenceService.linkTemporaryPersons(entitiesToLink);
            result.addAllRefsLinked(entitiesLinked);

        }

        return result;

    }

    public List<Person> getTemporaryPersons() {

        if (log.isInfoEnabled()) {
            log.info("Getting all persons from database");
        }
        List<Person> targetList = Lists.newArrayList(persistenceService.getAllPerson());
        if (log.isInfoEnabled()) {
            log.info("Got " + targetList.size() + " persons");
        }
        List<Person> toExport = persistenceService.retainTemporaryPersonList(targetList);
        if (log.isInfoEnabled()) {
            log.info("Got " + toExport.size() + " temporary persons");
        }
        return toExport;

    }

    public void exportExistingTemporaryPerson(File file) throws IOException {

        List<Person> toExport = getTemporaryPersons();
        exportTemporaryPerson(file, toExport);

    }

    public void exportTemporaryPersonExample(File file) throws IOException {

        List<Person> toExport = Lists.newArrayList();

        Person p;

        p = Persons.newPerson();
        p.setFirstName("First name 1");
        p.setLastName("Last name 1");
        toExport.add(p);

        p = Persons.newPerson();
        p.setFirstName("First name 2");
        p.setLastName("Last name 2");
        toExport.add(p);

        p = Persons.newPerson();
        p.setFirstName("First name 3");
        p.setLastName("Last name 3");
        toExport.add(p);

        exportTemporaryPerson(file, toExport);

    }

    public void exportTemporaryPerson(File file, List<Person> toExport) throws IOException {

        try (CsvProducerForTemporaryPerson producerForTemporarySpecies = new CsvProducerForTemporaryPerson(file.toPath(), getCsvSeparator())) {

            List<PersonRow> dataToExport = producerForTemporarySpecies.getDataToExport(toExport);
            producerForTemporarySpecies.write(dataToExport);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.export.person.error", file), e);
        }

    }

    protected char getCsvSeparator() {
        return ';';
    }
}
