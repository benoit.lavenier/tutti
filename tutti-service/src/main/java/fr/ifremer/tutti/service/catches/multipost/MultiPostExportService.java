package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.multipost.csv.AbstractFishingOperationRow;
import fr.ifremer.tutti.service.catches.multipost.csv.AccidentalCatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchBatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchBatchRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchWeightsRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchWeightsRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.FishingOperationRow;
import fr.ifremer.tutti.service.catches.multipost.csv.FishingOperationRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.IndividualObservationRow;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterRow;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterWeightRow;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterWeightRowModel;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;

/**
 * Service to export batches from a satellite post into a master post.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class MultiPostExportService extends AbstractTuttiService implements MultiPostConstants {

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    protected Map<String, CaracteristicQualitativeValue> sampleCategoryValueMap;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);

        SampleCategoryModel sampleCategoryModel = context.getSampleCategoryModel();

        sampleCategoryValueMap = sampleCategoryModel.toMap();

    }

    /**
     * Export catch batch.
     *
     * @param file      the file to export the batches into
     * @param operation the operation to export
     */
    public void exportCatchBatch(File file, FishingOperation operation) {

        Integer operationId = operation.getIdAsInt();
        if (persistenceService.isFishingOperationWithCatchBatch(operationId)) {

            try (MultiPostExportContext exportContext = new MultiPostExportContext(file, persistenceService)) {

                // Load data to export

                CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operationId);

                CatchBatchRow weights = new CatchBatchRow();
                weights.setCatchTotalWeight(catchBatch.getCatchTotalWeight());
                weights.setCatchTotalRejectedWeight(catchBatch.getCatchTotalRejectedWeight());
                weights.setSpeciesTotalSortedWeight(catchBatch.getSpeciesTotalSortedWeight());
                weights.setBenthosTotalSortedWeight(catchBatch.getBenthosTotalSortedWeight());
                weights.setMarineLitterTotalWeight(catchBatch.getMarineLitterTotalWeight());

                // export catch batch weights

                exportOperation(weights, operation);

                exportContext.addAttachments(catchBatch.getId(), catchBatch.getIdAsInt(), ObjectTypeCode.CATCH_BATCH);

                CatchBatchRowModel csvModel = new CatchBatchRowModel();

                exportContext.export(CATCH_BATCH_FILE,
                                     csvModel,
                                     Lists.newArrayList(weights),
                                     n("tutti.service.multipost.export.weights.error"));

            }

        }

    }

    /**
     * Export species batches.
     *
     * @param file      the file to export the batches into
     * @param operation the operation to export
     */
    public void exportSpecies(File file,
                              FishingOperation operation,
                              boolean exportFrequencies,
                              boolean exportIndividualObservations) {

        Integer operationId = operation.getIdAsInt();
        if (persistenceService.isFishingOperationWithCatchBatch(operationId)) {

            try (MultiPostExportContext exportContext = new MultiPostExportContext(file, persistenceService)) {

                // Export catch weights + attachments

                CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operationId);
                CatchWeightsRow weights = new CatchWeightsRow();
                weights.setTotalSortedWeight(catchBatch.getSpeciesTotalSortedWeight());
                weights.setInertWeight(catchBatch.getSpeciesTotalInertWeight());
                weights.setLivingNotItemizedWeight(catchBatch.getSpeciesTotalLivingNotItemizedWeight());
                exportOperation(weights, operation);

                exportContext.addAttachments(catchBatch.getId(), catchBatch.getIdAsInt(), ObjectTypeCode.CATCH_BATCH);

                exportContext.export(WEIGHTS_FILE,
                                     new CatchWeightsRowModel(),
                                     Lists.newArrayList(weights),
                                     n("tutti.service.multipost.export.weights.error"));

                // Export species catch, frequencies and individual observations

                BatchContainer<SpeciesBatch> speciesBatchContainer = persistenceService.getRootSpeciesBatch(operationId, false);

                for (SpeciesBatch batch : speciesBatchContainer.getChildren()) {
                    addBatch(exportContext, batch, null, exportFrequencies);
                }

                if (exportIndividualObservations) {
                    addIndividualObservations(exportContext, operation);
                }

                exportContext.storeSpeciesOrBenthosBatches(SPECIES_FILE, exportFrequencies, exportIndividualObservations);

            }

        }

    }

    /**
     * Export species batches.
     *
     * @param file      the file to export the batches into
     * @param operation the operation to export
     */
    public void exportBatch(File file,
                            FishingOperation operation,
                            SpeciesBatch speciesBatch,
                            boolean exportFrequencies,
                            boolean exportIndividualObservations) {

        Integer operationId = operation.getIdAsInt();
        if (persistenceService.isFishingOperationWithCatchBatch(operationId)) {

            try (MultiPostExportContext exportContext = new MultiPostExportContext(file, persistenceService)) {

                // Export frequencies
                if (exportFrequencies) {
                    addFrequencies(exportContext, speciesBatch);
                    exportContext.storeFrequencies();
                }

                // Export individual observations
                if (exportIndividualObservations) {
                    addIndividualObservations(exportContext, speciesBatch);
                    exportContext.storeIndividualObservations();
                }

            }

        }

    }

    /**
     * Export benthos batches.
     *
     * @param file      the file to export the batches into
     * @param operation the operation to export
     */
    public void exportBenthos(File file,
                              FishingOperation operation,
                              boolean exportFrequencies,
                              boolean exportIndividualObservations) {

        Integer operationId = operation.getIdAsInt();
        if (persistenceService.isFishingOperationWithCatchBatch(operationId)) {

            try (MultiPostExportContext exportContext = new MultiPostExportContext(file, persistenceService)) {

                // Export catch weights

                CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operationId);
                CatchWeightsRow weights = new CatchWeightsRow();
                weights.setTotalSortedWeight(catchBatch.getBenthosTotalSortedWeight());
                weights.setInertWeight(catchBatch.getBenthosTotalInertWeight());
                weights.setLivingNotItemizedWeight(catchBatch.getBenthosTotalLivingNotItemizedWeight());
                exportOperation(weights, operation);

                exportContext.addAttachments(catchBatch.getId(), catchBatch.getIdAsInt(), ObjectTypeCode.CATCH_BATCH);

                exportContext.export(WEIGHTS_FILE,
                                     new CatchWeightsRowModel(),
                                     Lists.newArrayList(weights),
                                     n("tutti.service.multipost.export.weights.error"));

                // Export benthos catch, frequencies and individual observations

                BatchContainer<SpeciesBatch> benthosBatchContainer = persistenceService.getRootBenthosBatch(operationId, false);

                for (SpeciesBatch batch : benthosBatchContainer.getChildren()) {
                    addBenthos(exportContext, batch, null, exportFrequencies);
                }

                if (exportIndividualObservations) {
                    addIndividualObservations(exportContext, operation);
                }

                exportContext.storeSpeciesOrBenthosBatches(BENTHOS_FILE, exportFrequencies, exportIndividualObservations);

            }

        }
    }

    /**
     * Export marine litter batches.
     *
     * @param file      the file to export the batches into
     * @param operation the operation to export
     */
    public void exportMarineLitter(File file, FishingOperation operation) {

        Integer operationId = operation.getIdAsInt();
        if (persistenceService.isFishingOperationWithCatchBatch(operationId)) {

            try (MultiPostExportContext exportContext = new MultiPostExportContext(file, persistenceService)) {

                // export weights

                CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operationId);
                MarineLitterWeightRow weight = new MarineLitterWeightRow();
                weight.setTotalWeight(catchBatch.getMarineLitterTotalWeight());
                exportOperation(weight, operation);

                exportContext.export(WEIGHTS_FILE,
                                     new MarineLitterWeightRowModel(),
                                     Lists.newArrayList(weight),
                                     n("tutti.service.multipost.export.weight.error"));

                // export marine litters

                BatchContainer<MarineLitterBatch> marineLitterBatchContainer =
                        persistenceService.getRootMarineLitterBatch(operationId);

                for (MarineLitterBatch batch : marineLitterBatchContainer.getChildren()) {

                    String id = context.generateId(MarineLitterRow.class);
                    exportContext.addMarineLitterBatch(id, batch);

                }

                exportContext.storeMarineLitterBatches();

            }

        }
    }

    /**
     * Export accidental catch batches.
     *
     * @param file      the file to export the batches into
     * @param operation the operation to export
     */
    public void exportAccidentalCatch(File file, FishingOperation operation) {

        try (MultiPostExportContext exportContext = new MultiPostExportContext(file, persistenceService)) {

            // export operation

            FishingOperationRow foRow = new FishingOperationRow();
            exportOperation(foRow, operation);

            FishingOperationRowModel foRowModel = new FishingOperationRowModel();

            exportContext.export(WEIGHTS_FILE,
                                 foRowModel,
                                 Lists.newArrayList(foRow),
                                 n("tutti.service.multipost.export.operation.error"));

            // export accidental catches

            List<AccidentalBatch> accidentalCatches =
                    persistenceService.getAllAccidentalBatch(operation.getIdAsInt());

            for (AccidentalBatch batch : accidentalCatches) {

                String id = context.generateId(AccidentalCatchRow.class);
                exportContext.addAccidentalCatch(id, batch);

            }

            exportContext.storeAccidentalCatches();


        }

    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void addBatch(MultiPostExportContext exportContext, SpeciesBatch batch, String parentId, boolean addFrequencies) {

        String id = context.generateId(CatchRow.class);

        exportContext.addSpeciesOrBenthosBatch(id, parentId, batch);

        if (addFrequencies) {
            List<SpeciesBatchFrequency> frequencies = getSpeciesFrequencies(batch);
            exportContext.addFrequencies(id, frequencies);
        }

        for (SpeciesBatch child : batch.getChildBatchs()) {
            addBatch(exportContext, child, id, addFrequencies);
        }

    }

    protected void addFrequencies(MultiPostExportContext exportContext, SpeciesBatch batch) {

        String id = context.generateId(CatchRow.class);

        List<SpeciesBatchFrequency> frequencies = getSpeciesFrequencies(batch);
        exportContext.addFrequencies(id, frequencies);

        for (SpeciesBatch child : batch.getChildBatchs()) {
            addFrequencies(exportContext, child);
        }

    }

    protected List<SpeciesBatchFrequency> getSpeciesFrequencies(SpeciesBatch batch) {

        List<SpeciesBatchFrequency> frequencies = persistenceService.getAllSpeciesBatchFrequency(batch.getIdAsInt());
        frequencies.forEach(speciesBatchFrequency -> speciesBatchFrequency.setBatch(batch));
        return frequencies;

    }

    protected void addBenthos(MultiPostExportContext exportContext, SpeciesBatch batch, String parentId, boolean addFrequencies) {

        String id = context.generateId(CatchRow.class);

        exportContext.addSpeciesOrBenthosBatch(id, parentId, batch);

        if (addFrequencies) {
            List<SpeciesBatchFrequency> frequencies = getBenthosFrequencies(batch);
            exportContext.addFrequencies(id, frequencies);
        }

        for (SpeciesBatch child : batch.getChildBatchs()) {
            addBenthos(exportContext, child, id, addFrequencies);
        }
    }

    protected List<SpeciesBatchFrequency> getBenthosFrequencies(SpeciesBatch batch) {

        List<SpeciesBatchFrequency> frequencies = persistenceService.getAllBenthosBatchFrequency(batch.getIdAsInt());
        frequencies.forEach(speciesBatchFrequency -> speciesBatchFrequency.setBatch(batch));
        return frequencies;

    }

    protected void addIndividualObservations(MultiPostExportContext exportContext, FishingOperation operation) {

        List<IndividualObservationBatch> individualObservations =
                persistenceService.getAllIndividualObservationBatchsForFishingOperation(operation.getIdAsInt());

        for (IndividualObservationBatch batch : individualObservations) {

            String id = context.generateId(IndividualObservationRow.class);
            exportContext.addIndividualObservations(id, batch);

        }

    }

    protected void addIndividualObservations(MultiPostExportContext exportContext, SpeciesBatch speciesOrBenthosBatch) {

        List<IndividualObservationBatch> individualObservations =
                persistenceService.getAllIndividualObservationBatchsForBatch(speciesOrBenthosBatch.getIdAsInt());

        for (IndividualObservationBatch batch : individualObservations) {

            String id = context.generateId(IndividualObservationRow.class);
            exportContext.addIndividualObservations(id, batch);

        }

    }

    protected void exportOperation(AbstractFishingOperationRow afoRow, FishingOperation operation) {
        afoRow.setStationNumber(operation.getStationNumber());
        afoRow.setOperationNumber(operation.getFishingOperationNumber());
        afoRow.setMultirigAggregation(operation.getMultirigAggregation());
        afoRow.setDate(operation.getGearShootingStartDate());
    }

}
