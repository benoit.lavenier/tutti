package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParser;

import java.text.ParseException;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public abstract class TemporaryReferentialEntityIdParser implements ValueParser<String> {

    protected String errorMessage;

    public TemporaryReferentialEntityIdParser(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String parse(String value) throws ParseException {

        value = StringUtils.trimToNull(value);

        if (value != null) {
            if (!isTemporaryId(value)) {
                String message = String.format("%s : %s", errorMessage, value);
                throw new IllegalArgumentException(message);
            }
        }

        return value;
    }

    protected abstract boolean isTemporaryId(String value);

}
