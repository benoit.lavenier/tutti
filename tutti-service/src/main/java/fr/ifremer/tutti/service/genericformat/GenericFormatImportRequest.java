package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created on 2/15/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private final GenericFormatImportConfiguration importConfiguration;

    private final GenericFormatArchive archive;

    private final char csvSeparator;

    private final SampleCategoryModel sampleCategoryModel;

    private final Set<Cruise> existingCruises;

    private final TuttiProtocol protocol;

    private final Program program;

    private final Date startingDate;

    private final ProgramDataModel existingData;

    public GenericFormatImportRequest(GenericFormatImportConfiguration importConfiguration,
                                      GenericFormatArchive archive,
                                      char csvSeparator,
                                      Program program,
                                      SampleCategoryModel sampleCategoryModel,
                                      Set<Cruise> cruises,
                                      ProgramDataModel existingData,
                                      TuttiProtocol protocol) {
        this.importConfiguration = importConfiguration;
        this.archive = archive;
        this.csvSeparator = csvSeparator;
        this.sampleCategoryModel = sampleCategoryModel;
        this.program = program;
        this.existingCruises = cruises;
        this.existingData = existingData;
        this.protocol = protocol;
        this.startingDate = new Date();

    }

    public GenericFormatArchive getArchive() {
        return archive;
    }

    public char getCsvSeparator() {
        return csvSeparator;
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return sampleCategoryModel;
    }

    public TuttiProtocol getProtocol() {
        return protocol;
    }

    public Program getProgram() {
        return program;
    }

    public Date getStartingDate() {
        return startingDate;
    }

    public GenericFormatImportConfiguration getImportConfiguration() {
        return importConfiguration;
    }

    public CruiseDataModel getExistingCruiseData(Cruise importRowCruise) {

        String cruiseId = getExistingCruiseId(importRowCruise);
        CruiseDataModel result = null;

        if (cruiseId != null) {

            for (CruiseDataModel cruiseDataModel : existingData) {
                if (cruiseId.equals(cruiseDataModel.getId())) {
                    result = cruiseDataModel;
                    break;
                }
            }
        }
        return result;

    }

    public boolean isCleanWeights() {
        return importConfiguration.isCleanWeights();
    }

    public boolean isCheckWeights() {
        return importConfiguration.isCheckWeights();
    }

    public boolean isOverrideProtocol() {
        return importConfiguration.isOverrideProtocol();
    }

    public boolean isUpdateCruises() {
        return importConfiguration.isUpdateCruises();
    }

    public boolean isUpdateOperations() {
        return importConfiguration.isUpdateOperations();
    }

    public boolean isImportSpecies() {
        return importConfiguration.isImportSpecies();
    }

    public boolean isImportBenthos() {
        return importConfiguration.isImportBenthos();
    }

    public boolean isImportMarineLitter() {
        return importConfiguration.isImportMarineLitter();
    }

    public boolean isImportAccidentalCatch() {
        return importConfiguration.isImportAccidentalCatch();
    }

    public boolean isImportIndividualObservation() {
        return importConfiguration.isImportIndividualObservation();
    }

    public boolean isImportAttachments() {
        return importConfiguration.isImportAttachments();
    }

    public File getReportFile() {
        return importConfiguration.getReportFile();
    }

    public int getMaximumRowsInErrorPerFile() {
        return importConfiguration.getMaximumRowsInErrorPerFile();
    }

    public boolean isAuthorizeObsoleteReferentials() {
        return importConfiguration.isAuthorizeObsoleteReferentials();
    }

    protected String getExistingCruiseId(Cruise importRowCruise) {

        String result = null;
        for (Cruise cruise : existingCruises) {

            boolean equals = Cruises.equals(importRowCruise, cruise);
            if (equals) {
                result = cruise.getId();
                break;
            }

        }
        return result;

    }

    public CruiseDataModel getSelectedCruise(Cruise importRowCruise) {

        CruiseDataModel result = null;

        ProgramDataModel dataToExport = importConfiguration.getDataToExport();
        for (CruiseDataModel selectedCruise : dataToExport) {
            boolean equals = Cruises.equalsNaturalId(importRowCruise, selectedCruise.getId());
            if (equals) {
                result = selectedCruise;
                break;
            }
        }

        return result;

    }

}
