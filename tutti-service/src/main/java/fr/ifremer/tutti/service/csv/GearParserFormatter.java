package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.service.PersistenceService;

import java.util.List;
import java.util.Map;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GearParserFormatter extends EntityParserFormatterSupport<Gear> {

    public static GearParserFormatter newFormatter() {
        return new GearParserFormatter(false, null, null);
    }

    public static GearParserFormatter newTechnicalFormatter() {
        return new GearParserFormatter(true, null, null);
    }

    public static GearParserFormatter newParser(PersistenceService persistenceService, Map<String, String> idTranslationMap) {
        return new GearParserFormatter(true, persistenceService, idTranslationMap);
    }

    private final PersistenceService persistenceService;

    private final Map<String, String> idTranslationMap;

    protected GearParserFormatter(boolean technical, PersistenceService persistenceService, Map<String, String> idTranslationMap) {
        super("", technical, Gear.class);
        this.persistenceService = persistenceService;
        this.idTranslationMap = idTranslationMap;
    }

    @Override
    protected List<Gear> getEntities() {
        return persistenceService.getAllGear();
    }

    @Override
    protected List<Gear> getEntitiesWithObsoletes() {
        return persistenceService.getAllGearWithObsoletes();
    }

    @Override
    protected String formatBusiness(Gear value) {

        return Gears.GET_NAME.apply(value);

    }

    @Override
    protected Gear parseNotBlankValue(String value) {

        if (idTranslationMap.containsKey(value)) {
            value = idTranslationMap.get(value);
        }
        return super.parseNotBlankValue(value);

    }

}
