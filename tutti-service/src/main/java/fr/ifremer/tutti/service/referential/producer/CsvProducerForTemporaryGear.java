package fr.ifremer.tutti.service.referential.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.referential.csv.GearModel;
import fr.ifremer.tutti.service.referential.csv.GearRow;

import java.nio.file.Path;
import java.util.List;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForTemporaryGear extends CsvProducer<GearRow, GearModel> {

    public CsvProducerForTemporaryGear(Path file, char separator) {
        super(file, GearModel.forExport(separator));
    }

    public List<GearRow> getDataToExport(List<Gear> toExport) {

        return Lists.transform(toExport, GearRow::new);

    }

}
