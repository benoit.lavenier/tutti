package fr.ifremer.tutti.service.export.sumatra;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.0
 */
public class CatchRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_STATION_NUMBER = FishingOperation.PROPERTY_STATION_NUMBER;

    public static final String PROPERTY_GEAR_SHOOTING_START_DATE = FishingOperation.PROPERTY_GEAR_SHOOTING_START_DATE;

    public static final String PROPERTY_GEAR_SHOOTING_END_DATE = FishingOperation.PROPERTY_GEAR_SHOOTING_END_DATE;

    public static final String PROPERTY_MULTIRIG_AGGREGATION = FishingOperation.PROPERTY_MULTIRIG_AGGREGATION;

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE = FishingOperation.PROPERTY_GEAR_SHOOTING_START_LATITUDE;

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE = FishingOperation.PROPERTY_GEAR_SHOOTING_END_LATITUDE;

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE = FishingOperation.PROPERTY_GEAR_SHOOTING_START_LONGITUDE;

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE = FishingOperation.PROPERTY_GEAR_SHOOTING_END_LONGITUDE;

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_SIGN = "sign";

    public static final String PROPERTY_TOTAL_WEIGHT = "totalWeight";

    public static final String PROPERTY_SORTED_WEIGHT = "sortedWeight";

    public static final String PROPERTY_AVERAGE_WEIGHT = "averageWeight";

    public static final String PROPERTY_AVERAGE_SIZE = "averageSize";

    public static final String PROPERTY_NUMBER = "number";

    public static final String PROPERTY_MOULE = "moule";

    protected FishingOperation fishingOperation;

    protected Species species;

    protected String sign;

    protected Float totalWeight;

    protected Float sortedWeight;

    protected Float averageWeight;

    protected Float averageSize;

    protected Integer number;

    protected Float moule;

    public String getStationNumber() {
        return fishingOperation.getStationNumber();
    }

    public Date getGearShootingStartDate() {
        return fishingOperation.getGearShootingStartDate();
    }

    public Date getGearShootingEndDate() {
        return fishingOperation.getGearShootingEndDate();
    }

    public String getMultirigAggregation() {
        return fishingOperation.getMultirigAggregation();
    }

    public Float getGearShootingStartLatitude() {
        return fishingOperation.getGearShootingStartLatitude();
    }

    public Float getGearShootingStartLongitude() {
        return fishingOperation.getGearShootingStartLongitude();
    }

    public Float getGearShootingEndLatitude() {
        return fishingOperation.getGearShootingEndLatitude();
    }

    public Float getGearShootingEndLongitude() {
        return fishingOperation.getGearShootingEndLongitude();
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        this.fishingOperation = fishingOperation;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Float getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Float getSortedWeight() {
        return sortedWeight;
    }

    public void setSortedWeight(Float sortedWeight) {
        this.sortedWeight = sortedWeight;
    }

    public Float getAverageWeight() {
        return averageWeight;
    }

    public void setAverageWeight(Float averageWeight) {
        this.averageWeight = averageWeight;
    }

    public Float getAverageSize() {
        return averageSize;
    }

    public void setAverageSize(Float averageSize) {
        this.averageSize = averageSize;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Float getMoule() {
        return moule;
    }

    public void setMoule(Float moule) {
        this.moule = moule;
    }
}
