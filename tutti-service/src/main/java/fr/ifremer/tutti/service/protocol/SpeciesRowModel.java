package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.protocol.Rtps;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.ForeignKeyParserFormatter;
import fr.ifremer.tutti.service.csv.StringParserFormatter;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import org.nuiton.csv.Common;
import org.nuiton.csv.ImportRuntimeException;

import java.text.ParseException;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Model to import / export {@link SpeciesRow}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class SpeciesRowModel extends AbstractTuttiImportExportModel<SpeciesRow> {

    public static SpeciesRowModel forExport(char separator) {

        SpeciesRowModel result = new SpeciesRowModel(separator);
        result.newColumnForExport(SpeciesRow.PROPERTY_SPECIES_REFERENCE_TAXON_ID, Common.INTEGER);
        result.newColumnForExport(SpeciesRow.PROPERTY_SPECIES_REF_TAX_CODE);
        result.newColumnForExport(SpeciesRow.PROPERTY_SPECIES_NAME);
        result.newColumnForExport(SpeciesRow.PROPERTY_SPECIES_SURVEY_CODE);
        result.newColumnForExport(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_ID);
        result.newColumnForExport(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_PARAMETER_NAME);
        result.newColumnForExport(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_MATRIX_NAME);
        result.newColumnForExport(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_FRACTION_NAME);
        result.newColumnForExport(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_METHOD_NAME);
        result.newColumnForExport(SpeciesRow.PROPERTY_MANDATORY_SAMPLE_CATEGORY_ID, TuttiCsvUtil.INTEGER_LIST_PARSER_FORMATTER);
        result.newColumnForExport(SpeciesRow.PROPERTY_WEIGHT_ENABLED, Common.PRIMITIVE_BOOLEAN);
        result.newColumnForExport(SpeciesRow.PROPERTY_RTP_MALE_A, Common.DOUBLE);
        result.newColumnForExport(SpeciesRow.PROPERTY_RTP_MALE_B, Common.FLOAT);
        result.newColumnForExport(SpeciesRow.PROPERTY_RTP_FEMALE_A, Common.DOUBLE);
        result.newColumnForExport(SpeciesRow.PROPERTY_RTP_FEMALE_B, Common.FLOAT);
        result.newColumnForExport(SpeciesRow.PROPERTY_RTP_UNDEFINED_A, Common.DOUBLE);
        result.newColumnForExport(SpeciesRow.PROPERTY_RTP_UNDEFINED_B, Common.FLOAT);
        return result;
    }

    public static SpeciesRowModel forImport(char separator,
                                            Map<String, Caracteristic> caracteristicMap,
                                            Map<String, Species> speciesMap) {

        SpeciesRowModel result = new SpeciesRowModel(separator);
        result.newMandatoryColumn(
                SpeciesRow.PROPERTY_SPECIES_REFERENCE_TAXON_ID,
                SpeciesRow.PROPERTY_SPECIES,
                new ForeignKeyParserFormatter<Species>(Species.class, Species.PROPERTY_REFERENCE_TAXON_ID, speciesMap) {

                    Set<Integer> taxonIds = Sets.newHashSet();

                    @Override
                    public Species parse(String value) throws ParseException {
                        Species species = super.parse(value);
                        Integer referenceTaxonId = species.getReferenceTaxonId();
                        if (taxonIds.contains(referenceTaxonId)) {

                            // duplicate reference taxon id used
                            throw new ImportRuntimeException(t("tutti.service.protocol.import.taxonUsed.error", referenceTaxonId));
                        }
                        taxonIds.add(referenceTaxonId);
                        return species;
                    }
                });

        result.newOptionalColumn(SpeciesRow.PROPERTY_SPECIES_REF_TAX_CODE);
        result.newOptionalColumn(SpeciesRow.PROPERTY_SPECIES_NAME);

        result.newForeignKeyColumn(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_ID,
                SpeciesRow.PROPERTY_LENGTH_STEP_PMFM,
                Caracteristic.class,
                Caracteristic.PROPERTY_ID,
                caracteristicMap);

        result.newIgnoredColumn(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_PARAMETER_NAME);
        result.newIgnoredColumn(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_MATRIX_NAME);
        result.newIgnoredColumn(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_FRACTION_NAME);
        result.newIgnoredColumn(SpeciesRow.PROPERTY_LENGTH_STEP_PMFM_METHOD_NAME);
        result.newIgnoredColumn(SpeciesRow.PROPERTY_CALCIFY_SAMPLE_ENABLED);
        result.newIgnoredColumn(SpeciesRow.PROPERTY_COUNT_IF_NO_FREQUENCY_ENABLED);

        //FIXME See if really needed, otherwise just use the default String parser
//        result.newMandatoryColumn(SpeciesRow.PROPERTY_SPECIES_SURVEY_CODE);
        result.newMandatoryColumn(SpeciesRow.PROPERTY_SPECIES_SURVEY_CODE, new StringParserFormatter(null, true));
        result.newMandatoryColumn(SpeciesRow.PROPERTY_MANDATORY_SAMPLE_CATEGORY_ID, TuttiCsvUtil.INTEGER_LIST_PARSER_FORMATTER);
        result.newMandatoryColumn(SpeciesRow.PROPERTY_WEIGHT_ENABLED, Common.PRIMITIVE_BOOLEAN);
        result.newOptionalColumn(SpeciesRow.PROPERTY_RTP_MALE_A, Common.DOUBLE);
        result.newOptionalColumn(SpeciesRow.PROPERTY_RTP_MALE_B, Common.FLOAT);
        result.newOptionalColumn(SpeciesRow.PROPERTY_RTP_FEMALE_A, Common.DOUBLE);
        result.newOptionalColumn(SpeciesRow.PROPERTY_RTP_FEMALE_B, Common.FLOAT);
        result.newOptionalColumn(SpeciesRow.PROPERTY_RTP_UNDEFINED_A, Common.DOUBLE);
        result.newOptionalColumn(SpeciesRow.PROPERTY_RTP_UNDEFINED_B, Common.FLOAT);
        return result;
    }

    public SpeciesRowModel(char separator) {
        super(separator);
    }

    @Override
    public SpeciesRow newEmptyInstance() {
        SpeciesRow speciesRow = new SpeciesRow();
        speciesRow.setRtpMale(Rtps.newRtp());
        speciesRow.setRtpFemale(Rtps.newRtp());
        speciesRow.setRtpUndefined(Rtps.newRtp());
        return speciesRow;
    }

}
