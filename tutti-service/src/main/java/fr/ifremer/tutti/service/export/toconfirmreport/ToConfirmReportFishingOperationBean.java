package fr.ifremer.tutti.service.export.toconfirmreport;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;

import java.util.Date;
import java.util.List;

/**
 * To store an fishing operation data and his species or benthos batch entries to report.
 *
 * Created on 2/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ToConfirmReportFishingOperationBean {

    private final FishingOperation fishingOperation;

    private final List<ToConfirmReportBatchEntryBean> speciesCatches;

    private final List<ToConfirmReportBatchEntryBean> benthosCatches;

    public ToConfirmReportFishingOperationBean(FishingOperation fishingOperation,
                                               List<ToConfirmReportBatchEntryBean> speciesCatches,
                                               List<ToConfirmReportBatchEntryBean> benthosCatches) {
        this.fishingOperation = fishingOperation;
        this.speciesCatches = speciesCatches;
        this.benthosCatches = benthosCatches;
    }

    public Integer getFishingOperationNumber() {
        return fishingOperation.getFishingOperationNumber();
    }

    public String getStationNumber() {
        return fishingOperation.getStationNumber();
    }

    public Date getGearShootingStartDate() {
        return fishingOperation.getGearShootingStartDate();
    }

    public Date getGearShootingEndDate() {
        return fishingOperation.getGearShootingEndDate();
    }

    public String getMultirigAggregation() {
        return fishingOperation.getMultirigAggregation();
    }

    public List<ToConfirmReportBatchEntryBean> getSpeciesCatches() {
        return speciesCatches;
    }

    public List<ToConfirmReportBatchEntryBean> getBenthosCatches() {
        return benthosCatches;
    }

}
