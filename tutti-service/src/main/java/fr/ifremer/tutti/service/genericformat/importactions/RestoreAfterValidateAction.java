package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportRequest;
import fr.ifremer.tutti.service.genericformat.GenericFormatReferentialImportResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 3/25/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class RestoreAfterValidateAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RestoreAfterValidateAction.class);

    private final PersistenceService persistenceService;

    public RestoreAfterValidateAction(GenericFormatContextSupport importContext, PersistenceService persistenceService) {
        super(importContext);
        this.persistenceService = persistenceService;
    }

    @Override
    protected boolean canExecute() {
        return true;
    }

    @Override
    protected void doExecute() {

        Set<Runnable> actions = new HashSet<>();
        actions.add(() -> rollbackSampleCategoryModel(importContext.getImportRequest()));
        actions.add(this::rollbackProtocol);
        actions.add(() -> {
            // push back if any previous protocol
            rollbackPreviousProtocol(importContext.getImportRequest());
        });
        actions.add(this::rollbackTemporaryGears);
        actions.add(this::rollbackTemporaryPersons);
        actions.add(this::rollbackTemporarySpecies);
        actions.add(this::rollbackTemporaryVessels);

        for (Runnable action : actions) {
            try {
                action.run();
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not execute rollback action", e);
                }
            }
        }

    }

    protected void rollbackSampleCategoryModel(GenericFormatImportRequest importRequest) {

        SampleCategoryModel sampleCategoryModel = importRequest.getSampleCategoryModel();

        if (log.isInfoEnabled()) {
            log.info("Rollback previous sample cateogry model: " + sampleCategoryModel);
        }
        persistenceService.setSampleCategoryModel(sampleCategoryModel);

    }

    protected void rollbackPreviousProtocol(GenericFormatImportRequest importRequest) {

        TuttiProtocol previousProtocol = importRequest.getProtocol();
        if (previousProtocol != null) {

            if (log.isInfoEnabled()) {
                log.info("Rollback previous protocol: " + previousProtocol);
            }
            persistenceService.setProtocol(previousProtocol);

        }

    }

    protected void rollbackProtocol() {

        TuttiProtocol importedProtocol = importContext.getImportedProtocol();
        if (importedProtocol != null) {

            if (log.isInfoEnabled()) {
                log.info("Delete imported protocol: " + importedProtocol);
            }
            persistenceService.setProtocol(null);
            persistenceService.deleteProtocol(importedProtocol.getId());

        }

    }

    protected void rollbackTemporaryGears() {

        GenericFormatReferentialImportResult<Gear, Integer> fileResult = importContext.getReferentialTemporaryGearFileResult();
        Set<Integer> addedEntriesIds = Sets.newHashSet(fileResult.getEntitiesAdded().stream().map(TuttiEntities.GET_ID_AS_INT::apply).collect(Collectors.toList()));
        if (!addedEntriesIds.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("Rollback previous imported temporary gears: " + addedEntriesIds);
            }
            persistenceService.deleteTemporaryGears(addedEntriesIds);
        }

    }

    protected void rollbackTemporaryPersons() {

        GenericFormatReferentialImportResult<Person, Integer> fileResult = importContext.getReferentialTemporaryPersonFileResult();
        Set<Integer> addedEntriesIds = Sets.newHashSet(fileResult.getEntitiesAdded().stream().map(TuttiEntities.GET_ID_AS_INT::apply).collect(Collectors.toList()));
        if (!addedEntriesIds.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("Rollback previous imported temporary persons: " + addedEntriesIds);
            }
            persistenceService.deleteTemporaryPersons(addedEntriesIds);
        }

    }

    protected void rollbackTemporarySpecies() {

        GenericFormatReferentialImportResult<Species, Integer> fileResult = importContext.getReferentialTemporarySpeciesFileResult();
        Set<Integer> addedEntriesIds = Sets.newHashSet(fileResult.getEntitiesAdded().stream().map(Speciess.GET_REFERECE_TAXON_ID_AS_INT::apply).collect(Collectors.toList()));
        if (!addedEntriesIds.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("Rollback previous imported temporary species: " + addedEntriesIds);
            }
            persistenceService.deleteTemporarySpecies(addedEntriesIds);
        }

    }

    protected void rollbackTemporaryVessels() {

        GenericFormatReferentialImportResult<Vessel, String> fileResult = importContext.getReferentialTemporaryVesselFileResult();
        Set<String> addedEntriesIds = Sets.newHashSet(fileResult.getEntitiesAdded().stream().map(TuttiEntities.GET_ID::apply).collect(Collectors.toList()));
        if (!addedEntriesIds.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("Rollback previous imported temporary vessels: " + addedEntriesIds);
            }
            persistenceService.deleteTemporaryVessels(addedEntriesIds);
        }

    }

}
