package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationModel;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationRow;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForIndividualObservation extends CsvProducer<IndividualObservationRow, IndividualObservationModel> {

    public CsvProducerForIndividualObservation(Path file, IndividualObservationModel model) {
        super(file, model);
    }

    public List<IndividualObservationRow> getDataToExport(GenericFormatExportOperationContext operationExportContext) {

        List<IndividualObservationRow> rows = new ArrayList<>();
        List<IndividualObservationBatch> individualObservations = operationExportContext.getIndividualObservations();

        if (CollectionUtils.isNotEmpty(individualObservations)) {
            for (IndividualObservationBatch child : individualObservations) {

                addIndividualObservationBatch(operationExportContext, rows, child);
            }
        }

        return rows;

    }

    protected void addIndividualObservationBatch(GenericFormatExportOperationContext operationExportContext,
                                                 List<IndividualObservationRow> rows,
                                                 IndividualObservationBatch child) {

        addCaracteristicRow(operationExportContext,
                            rows,
                            child,
                            operationExportContext.getWeightMeasuredCaracteristic(),
                            child.getWeight());

        addCaracteristicRow(operationExportContext,
                            rows,
                            child,
                            operationExportContext.getSampleCodeCaracteristic(),
                            child.getSamplingCode());

        Caracteristic copyIndividualObservationModeCaracteristic = operationExportContext.getCopyIndividualObservationModeCaracteristic();
        CopyIndividualObservationMode copyIndividualObservationMode = child.getCopyIndividualObservationMode();
        Objects.requireNonNull(copyIndividualObservationMode, "Mode de recopie null sur l'observation individuelle " + child.getId());
        addCaracteristicRow(operationExportContext,
                            rows,
                            child,
                            copyIndividualObservationModeCaracteristic,
                            copyIndividualObservationMode.getQualitativeValue(copyIndividualObservationModeCaracteristic));

        if (child.getLengthStepCaracteristic() != null) {
            addCaracteristicRow(operationExportContext,
                                rows,
                                child,
                                operationExportContext.getPmfmIdCaracteristic(),
                                child.getLengthStepCaracteristic().getIdAsInt());

            addCaracteristicRow(operationExportContext,
                                rows,
                                child,
                                child.getLengthStepCaracteristic(),
                                child.getSize());
        }

        CaracteristicMap caracteristics = child.getCaracteristics();
        if (MapUtils.isNotEmpty(caracteristics)) {
            for (Map.Entry<Caracteristic, Serializable> entry : caracteristics.entrySet()) {
                addCaracteristicRow(operationExportContext,
                                    rows,
                                    child,
                                    entry.getKey(),
                                    entry.getValue());
            }
        }
    }

    protected void addCaracteristicRow(GenericFormatExportOperationContext operationExportContext,
                                       List<IndividualObservationRow> rows,
                                       IndividualObservationBatch individualObservationBatch,
                                       Caracteristic caracteristic,
                                       Serializable caracteristicValue) {
        if (caracteristicValue != null) {

            IndividualObservationRow row = new IndividualObservationRow();
            row.setCruise(operationExportContext.getCruise());
            row.setFishingOperation(operationExportContext.getOperation());

            row.setComment(individualObservationBatch.getComment());
            row.setSpecies(individualObservationBatch.getSpecies());
            row.setId(individualObservationBatch.getIdAsInt());
            row.setBatchId(individualObservationBatch.getBatchId());
            row.setRankOrder(individualObservationBatch.getRankOrder());

            row.setCaracteristic(caracteristic);
            row.setCaracteristicValue(caracteristicValue);
            rows.add(row);
        }
    }

}
