package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A row in a catch export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class CatchRow extends RowWithOperationContextSupport {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_BENTHOS = "benthos";

    public static final String PROPERTY_SPECIES_TO_CONFIRM = "speciesToConfirm";

    public static final String FREQUENCY_LENGTH_STEP = "frequencyLengthStep";

    public static final String FREQUENCY_WEIGHT = "frequencyWeight";

    public static final String FREQUENCY_LENGTH_STEP_CARACTERISTIC = "frequencyLengthStepCaracteristic";

    public static final String FREQUENCY_RANK_ORDER = "frequencyRankOrder";

    public static final String SAMPLE_CATEGORY = "sampleCategory";

    public static final String REFERENCE_WEIGHT = "referenceWeight";

    public static final String RAISING_FACTOR = "raisingFactor";

    public static final String BATCH_NUMBER = "batchNumber";

    public static final String BATCH_NUMBER_COMPUTED = "batchNumberComputed";

    public static final String BATCH_WEIGHT_UNIT = "batchWeightUnit";

    public static final String FINAL_RAISING_FACTOR = "finalRaisingFactor";

    public static CatchRow newEmptyInstance() {
        CatchRow row = new CatchRow();
        row.forImport();
        return row;
    }

    protected final List<ExportSampleCategory> sampleCategory = new ArrayList<>();

    protected Species species;

    protected Float referenceWeight;

    protected float raisingFactor;

    protected Float finalRaisingFactor;

    protected Integer batchNumber;

    protected Boolean batchNumberComputed;

    protected String batchWeightUnit;

    protected boolean benthos;

    protected boolean vrac;

    protected boolean speciesToConfirm;

    protected Caracteristic frequencyLengthStepCaracteristic;

    protected Float frequencyLengthStep;

    protected Float frequencyWeight;

    protected Float computedFrequencyTotalWeight;

    protected Integer frequencyRankOrder;

    public void setBenthos(boolean benthos) {
        this.benthos = benthos;
    }

    public void setVrac(boolean vrac) {
        this.vrac = vrac;
    }

    public void setReferenceWeight(Float referenceWeight) {
        this.referenceWeight = referenceWeight;
    }

    public void setRaisingFactor(float raisingFactor) {
        this.raisingFactor = raisingFactor;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public void setBatchNumberComputed(Boolean batchNumberComputed) {
        this.batchNumberComputed = batchNumberComputed;
    }

    public void setComputedFrequencyTotalWeight(Float computedFrequencyTotalWeight) {
        this.computedFrequencyTotalWeight = computedFrequencyTotalWeight;
    }

    public void addSampleCategory(ExportSampleCategory sampleCategory) {
        int order = sampleCategory.getCategoryDef().getOrder();
        while (this.sampleCategory.size() <= order) {
            this.sampleCategory.add(null);
        }
        this.sampleCategory.set(order, sampleCategory);
    }

    public void setFrequency(SpeciesBatchFrequency frequency) {
        Preconditions.checkNotNull(frequency);
        setFrequencyLengthStepCaracteristic(frequency.getLengthStepCaracteristic());
        setFrequencyLengthStep(frequency.getLengthStep());
        setBatchNumber(frequency.getNumber());
        setBatchNumberComputed(false);
        setFrequencyRankOrder(frequency.getRankOrder());
        setFrequencyWeight(frequency.getWeight());
    }

    public void setFinalRaisingFactor(Float finalRaisingFactor) {
        this.finalRaisingFactor = finalRaisingFactor;
    }

    public void setFrequencyLengthStepCaracteristic(Caracteristic frequencyLengthStepCaracteristic) {
        this.frequencyLengthStepCaracteristic = frequencyLengthStepCaracteristic;
    }

    public void setFrequencyLengthStep(Float frequencyLengthStep) {
        this.frequencyLengthStep = frequencyLengthStep;
    }

    public void setFrequencyWeight(Float frequencyWeight) {
        this.frequencyWeight = frequencyWeight;
    }

    public void setFrequencyRankOrder(Integer frequencyRankOrder) {
        this.frequencyRankOrder = frequencyRankOrder;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setSpeciesToConfirm(boolean speciesToConfirm) {
        this.speciesToConfirm = speciesToConfirm;
    }

    public List<ExportSampleCategory> getSampleCategory() {
        return sampleCategory;
    }

    public ExportSampleCategory getLastSampleCategoryFilled() {
        List<ExportSampleCategory> list = sampleCategory.stream().filter(Objects::nonNull).collect(Collectors.toList());
        return list.isEmpty() ? null:list.get(list.size()-1);
    }

    public Float getFinalRaisingFactor() {
        return finalRaisingFactor;
    }

    public boolean isFrequencyWithWeight() {
        return frequencyLengthStepCaracteristic!=null && frequencyWeight!=null;
    }

    public Caracteristic getFrequencyLengthStepCaracteristic() {
        return frequencyLengthStepCaracteristic;
    }

    public Float getFrequencyLengthStep() {
        return frequencyLengthStep;
    }

    public Float getFrequencyWeight() {
        return frequencyWeight;
    }

    public Float getComputedFrequencyTotalWeight() {
        return computedFrequencyTotalWeight;
    }

    public Species getSpecies() {
        return species;
    }

    public Integer getFrequencyRankOrder() {
        return frequencyRankOrder;
    }

    public Float getReferenceWeight() {
        return referenceWeight;
    }

    public float getRaisingFactor() {
        return raisingFactor;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public Boolean getBatchNumberComputed() {
        return batchNumberComputed;
    }

    public String getBatchWeightUnit() {
        return batchWeightUnit;
    }

    public boolean isSpeciesToConfirm() {
        return speciesToConfirm;
    }

    public void setBatchWeightUnit(String batchWeightUnit) {
        this.batchWeightUnit = batchWeightUnit;
    }

    public boolean isVrac() {
        return vrac;
    }

    public boolean isHorsVrac() {
        return !isVrac();
    }

    public boolean isBenthos() {
        return benthos;
    }

    public CatchRow copy() {
        CatchRow result = new CatchRow();
        result.setVrac(vrac);
        result.setBenthos(benthos);
        result.setCruise(getCruise());
        result.setFishingOperation(getFishingOperation());
        result.sampleCategory.addAll(sampleCategory);
        result.setFrequencyLengthStep(frequencyLengthStep);
        result.setFrequencyLengthStepCaracteristic(frequencyLengthStepCaracteristic);
        result.setFrequencyRankOrder(frequencyRankOrder);
        result.setFrequencyWeight(frequencyWeight);

        result.setSpecies(species);
        result.setSpeciesToConfirm(speciesToConfirm);
        result.setRaisingFactor(raisingFactor);
        result.setReferenceWeight(referenceWeight);
        result.setBatchNumber(batchNumber);
        result.setBatchWeightUnit(batchWeightUnit);
        result.setFinalRaisingFactor(finalRaisingFactor);

        return result;
    }

    public boolean withFrequency() {
        return frequencyLengthStep != null;
    }

    public ExportSampleCategory getSampleCategory(SampleCategoryModelEntry sampleCategoryModelEntry) {

        int categoryOrder = sampleCategoryModelEntry.getOrder();
        if (sampleCategory.size() == categoryOrder) {
            ExportSampleCategory exportSampleCategory = new ExportSampleCategory();
            exportSampleCategory.setCategoryDef(sampleCategoryModelEntry);
            sampleCategory.add(categoryOrder, exportSampleCategory);
        }
        return sampleCategory.get(categoryOrder);

    }

    public List<ExportSampleCategory> getFilledSampleCategories() {

        List<ExportSampleCategory> result = new ArrayList<>();
        for (ExportSampleCategory exportSampleCategory : sampleCategory) {
            if (exportSampleCategory.isFilled()) {
                result.add(exportSampleCategory);
            }
        }
        return result;

    }

}
