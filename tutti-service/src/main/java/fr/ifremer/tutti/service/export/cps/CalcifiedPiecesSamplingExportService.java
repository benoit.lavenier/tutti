package fr.ifremer.tutti.service.export.cps;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.sampling.CacheExtractedKey;
import fr.ifremer.tutti.service.cruise.CruiseCache;
import fr.ifremer.tutti.service.sampling.CruiseSamplingCache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Export;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CalcifiedPiecesSamplingExportService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(CalcifiedPiecesSamplingExportService.class);

    protected PersistenceService persistenceService;
    protected DecoratorService decoratorService;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
    }

    /**
     * Export selected cruise with the csv sumatra format.
     *
     * @param file where to generate report
     * @since 2.0
     */
    public void exportCruiseCalcifiedPiecesSamplingsReport(File file, ProgressionModel progressionModel) {

        Preconditions.checkNotNull(file, "Cannot export to a null file");

        TuttiDataContext dataContext = context.getDataContext();
        Preconditions.checkState(dataContext.isCanUseCruiseSamplingCache() && dataContext.isCruiseCacheLoaded() && dataContext.isCruiseCacheUpToDate());

        Decorator<Species> decorator = decoratorService.getDecoratorByType(Species.class, DecoratorService.WITH_SURVEY_CODE_NO_NAME);
        SamplingNumberRowModel csvModel = new SamplingNumberRowModel(context.getConfig().getCsvSeparator(), decorator);

        Map<String, Species> referenceSpeciesByReferenceTaxonId = Speciess.splitReferenceSpeciesByReferenceTaxonId(dataContext.getReferentSpecies());

        Optional<CruiseCache> optionalCruiseCache = context.getDataContext().getOptionalCruiseCache();
        if (!optionalCruiseCache.isPresent()) {
            throw new IllegalStateException("No cruise cache found");
        }
        CruiseCache cruiseCache = optionalCruiseCache.get();

        Optional<CruiseSamplingCache> optionalSamplingCruiseCache = cruiseCache.getSamplingCruiseCache();
        if (!optionalSamplingCruiseCache.isPresent()) {
            throw new IllegalStateException("No sampling cruise cache found");
        }

        List<CacheExtractedKey> rows = optionalSamplingCruiseCache.get().getSamplingNumbers(referenceSpeciesByReferenceTaxonId);

        if (log.isInfoEnabled()) {
            log.info("Loaded " + rows.size() + " keys to export to " + file);
        }
        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), StandardCharsets.UTF_8)) {

            progressionModel.increments(t("tutti.service.cpsExport.step.toFile", file.getName()));

            Export export = Export.newExport(csvModel, rows);
            export.write(writer);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.cpsExport.error", file), e);
        }

    }

}
