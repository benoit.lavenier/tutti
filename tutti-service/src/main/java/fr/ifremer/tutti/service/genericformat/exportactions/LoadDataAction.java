package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.OperationDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class LoadDataAction extends ExportTechnicalActionSupport {

    private final PersistenceService persistenceService;

    public LoadDataAction(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public void execute(GenericFormatExportContext exportContext) {

        ProgramDataModel dataToExport = exportContext.getDataToExport();

        for (CruiseDataModel cruiseDataModel : dataToExport) {

            Cruise cruise = persistenceService.getCruise(cruiseDataModel.getIdAsInt());
            Preconditions.checkNotNull(cruise);

            Set<FishingOperation> operations = new LinkedHashSet<>();

            for (OperationDataModel operationDataModel : cruiseDataModel) {

                FishingOperation operation = persistenceService.getFishingOperation(operationDataModel.getIdAsInt());
                operations.add(operation);

            }

            exportContext.addCruiseContext(cruise, operations);

        }
    }
}
