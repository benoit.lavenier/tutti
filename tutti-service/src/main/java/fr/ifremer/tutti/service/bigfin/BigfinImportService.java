package fr.ifremer.tutti.service.bigfin;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.io.Files;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.bigfin.csv.BigfinDataRow;
import fr.ifremer.tutti.service.bigfin.csv.BigfinDataRowModel;
import fr.ifremer.tutti.service.bigfin.signs.Sex;
import fr.ifremer.tutti.service.bigfin.signs.Sign;
import fr.ifremer.tutti.service.bigfin.signs.Size;
import fr.ifremer.tutti.service.bigfin.signs.VracHorsVrac;
import fr.ifremer.tutti.type.WeightUnit;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class BigfinImportService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(BigfinImportService.class);

    protected PersistenceService persistenceService;

    protected Caracteristic sizeCaracteristic;

    protected Caracteristic sexCaracteristic;

    protected Map<Sign, CaracteristicQualitativeValue> signsToCaracteristicValue;

    @Override
    public void setServiceContext(TuttiServiceContext context) {

        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);

        signsToCaracteristicValue = new HashMap<>();

        // sex caracteristic
        sexCaracteristic = persistenceService.getSexCaracteristic();
        Sex.NONE.registerSign(sexCaracteristic, signsToCaracteristicValue);
        Sex.UNKNOWN.registerSign(sexCaracteristic, signsToCaracteristicValue);
        Sex.MALE.registerSign(sexCaracteristic, signsToCaracteristicValue);
        Sex.FEMALE.registerSign(sexCaracteristic, signsToCaracteristicValue);

        // size caracteristic
        sizeCaracteristic = persistenceService.getSizeCategoryCaracteristic();
        Size.NOT_SIZED.registerSign(sizeCaracteristic, signsToCaracteristicValue);
        Size.SMALL.registerSign(sizeCaracteristic, signsToCaracteristicValue);
        Size.BIG.registerSign(sizeCaracteristic, signsToCaracteristicValue);

        // sorted/unsorted caracteristic
        Caracteristic vracHorsVracCaracteristic = persistenceService.getSortedUnsortedCaracteristic();
        VracHorsVrac.VRAC.registerSign(vracHorsVracCaracteristic, signsToCaracteristicValue);
        VracHorsVrac.HORS_VRAC.registerSign(vracHorsVracCaracteristic, signsToCaracteristicValue);

    }

    private BigfinImportContext prepareImportContext(File importFile, FishingOperation operation, CatchBatch catchBatch) {

        TuttiProtocol protocol = persistenceService.getProtocol();

        if (protocol == null) {
            throw new ApplicationBusinessException(t("tutti.service.bigfinimport.error.no.protocol"));
        }

        List<Species> allReferentSpecies = persistenceService.getAllReferentSpecies();
        Map<String, Species> speciesBySurveyCode = Maps.newTreeMap();
        for (Species species : allReferentSpecies) {
            String surveyCode = species.getSurveyCode();
            if (StringUtils.isNotBlank(surveyCode)) {
                speciesBySurveyCode.put(surveyCode, species);

            }
            if (species.getRefTaxCode() != null) {
                speciesBySurveyCode.put(species.getRefTaxCode(), species);
            }
        }

        Map<String, SpeciesProtocol> speciesProtocolBySurveyCode = Maps.newTreeMap();
        for (SpeciesProtocol speciesProtocol : protocol.getSpecies()) {
            if (StringUtils.isNotBlank(speciesProtocol.getSpeciesSurveyCode())) {
                speciesProtocolBySurveyCode.put(speciesProtocol.getSpeciesSurveyCode(), speciesProtocol);

            } else {
                speciesProtocolBySurveyCode.put(speciesProtocol.getSpeciesReferenceTaxonId().toString(), speciesProtocol);
            }
        }

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);

        // make sure we use survey codes if possible in species batches (see https://forge.codelutin.com/issues/6848)
        if (!rootSpeciesBatch.isEmptyChildren()) {

            Map<String, Species> speciesByReferenceTaxonId = Speciess.splitReferenceSpeciesByReferenceTaxonId(allReferentSpecies);

            for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
                Species species = speciesBatch.getSpecies();
                Integer referenceTaxonId = species.getReferenceTaxonId();
                Preconditions.checkNotNull(referenceTaxonId, "Can't have a null referenceTaxonId for species: " + species);
                Species species1 = speciesByReferenceTaxonId.get(referenceTaxonId.toString());
                consolidateSpecies(speciesBatch, species1);

            }

        }

        return new BigfinImportContext(importFile, operation, catchBatch, signsToCaracteristicValue, speciesBySurveyCode, speciesProtocolBySurveyCode, rootSpeciesBatch);

    }

    protected void consolidateSpecies(SpeciesBatch speciesBatch, Species species) {

        speciesBatch.setSpecies(species);
        if (!speciesBatch.isChildBatchsEmpty()) {

            for (SpeciesBatch childBatch : speciesBatch.getChildBatchs()) {
                consolidateSpecies(childBatch, species);
            }
        }

    }

    public BigfinImportResult importFile(File importFile, FishingOperation operation, CatchBatch catchBatch) {

        Preconditions.checkNotNull(importFile);
        Preconditions.checkArgument(importFile.exists(), "Bigfin file " + importFile + " does not exist.");

        BigfinImportContext importContext = prepareImportContext(importFile, operation, catchBatch);

        BigfinDataRowModel importModel = new BigfinDataRowModel(importContext.speciesBySurveyCode, importContext.getSpeciesBatchesById());

        try (Reader reader = Files.newReader(importFile, Charsets.UTF_8)) {

            try (Import<BigfinDataRow> importer = Import.newImport(importModel, reader)) {

                for (BigfinDataRow bigfinDataRow : importer) {

                    if (log.isInfoEnabled()) {
                        log.info("Check row: " + bigfinDataRow.getRecordId());
                    }
                    boolean rowIsSafe = importContext.checkRow(bigfinDataRow);

                    if (rowIsSafe) {

                        importContext.addRowToProcess(bigfinDataRow);
                    }

                }

            }

        } catch (IOException e) {
            throw new ImportRuntimeException("Could not import bigfin data from file " + importFile, e);

        }

        if (importContext.isNoError()) {

            processSpeciesBatchRows(importContext);

            processSpeciesRows(importContext);

            addFileAsAttachment(importFile, importContext.catchBatch);

        }

        return importContext.getResult();

    }


    private void processSpeciesBatchRows(BigfinImportContext importContext) {

        BigfinImportResult result = importContext.getResult();

        Multimap<SpeciesBatch, BigfinDataRow> speciesBatchRowsBySpeciesBatch = importContext.getSpeciesBatchRowsBySpeciesBatch();

        for (SpeciesBatch batch : speciesBatchRowsBySpeciesBatch.keySet()) {

            List<SpeciesBatchFrequency> existingSpeciesFrequencies = persistenceService.getAllSpeciesBatchFrequency(batch.getIdAsInt());

            Integer deletedNb = persistenceService.countFrequenciesNumber(existingSpeciesFrequencies, false);

            Collection<BigfinDataRow> bigfinDataRows = speciesBatchRowsBySpeciesBatch.get(batch);

            Species species = importContext.getSpeciesWithSurveyCode(batch.getSpecies());
            Caracteristic lengthStepPmfm = importContext.getLengthStepPmfm(species, persistenceService);
            List<SpeciesBatchFrequency> frequencies = createFrequencies(batch, bigfinDataRows, lengthStepPmfm);

            persistenceService.saveSpeciesBatchFrequency(batch.getIdAsInt(), frequencies);

            result.incrementNbFrequenciesDeleted(deletedNb != null ? deletedNb : 0);

            Integer importedNb = persistenceService.countFrequenciesNumber(frequencies, false);
            result.incrementNbFrequenciesImported(importedNb != null ? importedNb : 0);

        }

    }

    private void processSpeciesRows(BigfinImportContext importContext) {

        BigfinImportResult result = importContext.getResult();

        SampleCategoryModel sampleCategoryModel = context.getSampleCategoryModel();
        List<Integer> samplingOrder = sampleCategoryModel.getSamplingOrder();

        List<Integer> pmfmIds = new ArrayList<>();
        pmfmIds.add(PmfmId.SORTED_UNSORTED.getValue());
        List<Function<BigfinDataRow, Sign>> functions = new ArrayList<>();

        // put the size and order in the right order
        for (Integer categoryId : samplingOrder) {
            if (PmfmId.SIZE_CATEGORY.getValue().equals(categoryId)) {
                pmfmIds.add(categoryId);
                functions.add(Size.newExtractValueFunction());

            } else if (PmfmId.SEX.getValue().equals(categoryId)) {
                pmfmIds.add(categoryId);
                functions.add(Sex.newExtractValueFunction());
            }
        }

        List<Category> categories = new ArrayList<>();
        for (int i = 0; i < pmfmIds.size(); i++) {
            Category category = new Category(pmfmIds.get(i), i < functions.size() ? functions.get(i) : null);
            categories.add(category);
        }

        // and separate them by species
        Multimap<Species, SpeciesBatch> batchesBySpecies = importContext.getRootSpeciesBatchBySpecies();

        // separate the imported rows by species
        Multimap<Species, BigfinDataRow> rowsBySpecies = importContext.getSpeciesRowsBySpecies();

        // for each species imported
        for (Species species : rowsBySpecies.keySet()) {

            // get the lengthstep pmfm associated with the species
            Caracteristic lengthStepPmfm = importContext.getLengthStepPmfm(species, persistenceService);

            // get the rows with the current species and separate them by vrac/hors varc
            Collection<BigfinDataRow> speciesRows = rowsBySpecies.get(species);
            Multimap<Sign, BigfinDataRow> rowsByVracHorsVrac = Multimaps.index(speciesRows, VracHorsVrac.newExtractValueFunction());

            // get the existing species batches for the current species and separate them by vrac/hors varc
            Collection<SpeciesBatch> speciesBatches = batchesBySpecies.get(species);
            Map<Serializable, SpeciesBatch> speciesBatchByVracHorsVrac = Maps.uniqueIndex(speciesBatches, SpeciesBatchs.GET_SAMPLE_CATEGORY_VALUE);

            BrowseBatchesParameter commonParameter = new BrowseBatchesParameter(importContext.operation, species, lengthStepPmfm, categories, result);
            browseBatchesToAddFrequencies(commonParameter, null, 0, speciesBatchByVracHorsVrac, rowsByVracHorsVrac);

        }

    }


    /**
     * Go deeper in the batches until it finds the last of gender or size class, then add the frequencies
     *
     * @param commonParameter        The parameter containing the parameters which do not change while browsing
     * @param parentBatch            The parent batch (null if root)
     * @param depth                  The depth in the batch children
     * @param batchesByCaracteristic a map containing the batches by caracteristic value
     * @param rowsByCaracteristic    a multimap containing the rows to import by caracteristic value
     */
    protected void browseBatchesToAddFrequencies(BrowseBatchesParameter commonParameter,
                                                 SpeciesBatch parentBatch,
                                                 int depth,
                                                 Map<Serializable, SpeciesBatch> batchesByCaracteristic,
                                                 Multimap<Sign, BigfinDataRow> rowsByCaracteristic) {

        Category category = commonParameter.getCategories().get(depth++);

        int parentSignChildrenNb = rowsByCaracteristic.keySet().size();

        for (Sign caracteristic : rowsByCaracteristic.keySet()) {
            Collection<BigfinDataRow> bigfinDataRows = rowsByCaracteristic.get(caracteristic);

            // get the batch with the caracteristic
            SpeciesBatch batch = batchesByCaracteristic.get(signsToCaracteristicValue.get(caracteristic));

            boolean batchHasFrequencies = false;

            // if it does not exists, create the batch
            if (batch == null) {
                if (caracteristic.isNullEquivalent(parentSignChildrenNb)) {
                    batch = parentBatch;

                } else {
                    batch = createSpeciesBatch(commonParameter.getSpecies(),
                                               commonParameter.getOperation(),
                                               category.getPmfmId(),
                                               caracteristic,
                                               parentBatch != null ? parentBatch.getIdAsInt() : null);
                    // useful to be able to save the batch later in the frequency creation
                    batch.setParentBatch(parentBatch);
                }

            } else {
                // useful to be able to save the batch later in the frequency creation
                batch.setFishingOperation(commonParameter.getOperation());
                List<SpeciesBatchFrequency> frequencies = persistenceService.getAllSpeciesBatchFrequency(batch.getIdAsInt());
                batchHasFrequencies = CollectionUtils.isNotEmpty(frequencies);
            }

            // if the function is null, do not go deeper in the batch children, add the frequencies in this batch
            if (category.getCategoryValueGetter() == null) {
                // if the batch is not the last one, error, we cannot add the frequencies to a more categorized batch
                if (CollectionUtils.isNotEmpty(batch.getChildBatchs())) {
                    commonParameter.getResult().addWarning(t("tutti.service.bigfinImport.warning.species.tooCategorized",
                                                             commonParameter.getSpeciesLabel(),
                                                             sizeCaracteristic.getParameterName(),
                                                             sexCaracteristic.getParameterName()));

                } else {
                    // create the frequencies
                    Integer deletedNb = persistenceService.countFrequenciesNumber(
                            persistenceService.getAllSpeciesBatchFrequency(batch.getIdAsInt()), false);

                    List<SpeciesBatchFrequency> frequencies = createFrequencies(batch, bigfinDataRows, commonParameter.getLengthStepPmfm());

                    persistenceService.saveSpeciesBatchFrequency(batch.getIdAsInt(), frequencies);

                    commonParameter.getResult().incrementNbFrequenciesDeleted(deletedNb != null ? deletedNb : 0);

                    Integer importedNb = persistenceService.countFrequenciesNumber(frequencies, false);
                    commonParameter.getResult().incrementNbFrequenciesImported(importedNb != null ? importedNb : 0);
                }

            } else if (batchHasFrequencies) { // if the batch is supposed to be categorized again, but already has frequencies
                CaracteristicQualitativeValue qualitativeValue = signsToCaracteristicValue.get(caracteristic);
                commonParameter.getResult().addWarning(t("tutti.service.bigfinImport.warning.species.batch.frequenciesOnHigherLevel",
                                                         commonParameter.getSpeciesLabel(), qualitativeValue.getName()));

            } else {
                List<SpeciesBatch> batchChildren = batch.getChildBatchs();

                Multimap<Sign, BigfinDataRow> rowsByNewCaracteristic = Multimaps.index(bigfinDataRows, category.getCategoryValueGetter());

                // get the children of the current batch and separate them by caracteristic
                Map<Serializable, SpeciesBatch> childrenByCaracteristic = new HashMap<>();
                if (CollectionUtils.isNotEmpty(batchChildren)) {

                    SpeciesBatch firstBatch = batchChildren.get(0);
                    Integer categoryId = firstBatch.getSampleCategoryId();
                    Category nextCategory = commonParameter.getCategories().get(depth);

                    // check the category is the right next one
                    if (!nextCategory.getPmfmId().equals(categoryId)) {
                        // if all the rows to import have a gender or size (the first category in the list) null equivalent
                        // then ok
                        // else error
                        Set<Sign> signsSet = rowsByNewCaracteristic.keySet();
                        if (signsSet.size() == 1 && signsSet.iterator().next().isNullEquivalent(parentSignChildrenNb)) {// we can go deeper
                            category = commonParameter.getCategories().get(depth++);
                            rowsByNewCaracteristic = Multimaps.index(bigfinDataRows, category.getCategoryValueGetter());

                            // check that this time, it is the right category. We can only skip one
                            nextCategory = commonParameter.getCategories().get(depth);
                            if (!nextCategory.getPmfmId().equals(categoryId)) {
                                commonParameter.getResult().addWarning(t("tutti.service.bigfinImport.warning.species.categoriesSkipped",
                                                                         commonParameter.getSpeciesLabel(),
                                                                         sizeCaracteristic.getParameterName(),
                                                                         sexCaracteristic.getParameterName()
                                ));
                                continue;
                            }

                        } else {
                            commonParameter.getResult().addWarning(t("tutti.service.bigfinImport.warning.species.categorySkipped",
                                                                     commonParameter.getSpeciesLabel(),
                                                                     persistenceService.getCaracteristic(nextCategory.getPmfmId()).getParameterName()
                            ));
                            continue;
                        }
                    }
                    childrenByCaracteristic.putAll(Maps.uniqueIndex(batchChildren, SpeciesBatchs.GET_SAMPLE_CATEGORY_VALUE));
                }

                // go deeper in the batch children
                browseBatchesToAddFrequencies(commonParameter, batch, depth, childrenByCaracteristic, rowsByNewCaracteristic);
            }

        }
    }

    protected SpeciesBatch createSpeciesBatch(Species species,
                                              FishingOperation operation,
                                              Integer categoryId,
                                              Sign signs,
                                              Integer parentBatchId) {

        Preconditions.checkArgument(signs.getCategory().equals(categoryId));

        SpeciesBatch batch = SpeciesBatchs.newSpeciesBatch();
        batch.setSpecies(species);
        batch.setFishingOperation(operation);

        batch.setSampleCategoryId(categoryId);
        batch.setSampleCategoryValue(signsToCaracteristicValue.get(signs));

        batch = persistenceService.createSpeciesBatch(batch, parentBatchId, true);
        return batch;
    }

    protected List<SpeciesBatchFrequency> createFrequencies(SpeciesBatch batch, Collection<BigfinDataRow> rows, Caracteristic lengthStepPmfm) {
        Preconditions.checkNotNull(lengthStepPmfm);
        String unit = lengthStepPmfm.getUnit();
        Float precision = lengthStepPmfm.getPrecision();
        // CHANGE poussin 2016, maintenant getPrecision retourne toujours quelque chose
//        if (precision == null) {
//            precision = 1f;
//        }

        // board measurements are in mm

        ListMultimap<Float, Float> weightsByLengthStep = ArrayListMultimap.create();
        for (BigfinDataRow row : rows) {
            Float weight = row.getWeight();
            float length = row.getLength();

            if ("cm".equals(unit)) {
                // measurement in cm asked
                length = length / 10;
            }

            int intValue = (int) (length * 10);
            int intStep = (int) (precision * 10);
            int correctIntStep = intValue - (intValue % intStep);
            float lengthStep = correctIntStep / 10f;

            weightsByLengthStep.put(lengthStep, weight);
        }

        // tous les poids du batch
        Collection<Float> weightValues = weightsByLengthStep.values();
        int weightValuesSize = weightValues.size();

        // poids non nuls du batch
        Collection<Float> notNullWeights = Collections2.filter(weightValues, input -> input != null);
        int notNullWeightsSize = notNullWeights.size();

        // si présence d'une seule valeur, mettre cette valeur dans le champ "Poids sous échantillon"
        if (notNullWeightsSize == 1) {
            float weight = IterableUtils.get(notNullWeights, 0);
            weight = WeightUnit.KG.round(weight / 1000);
            batch.setWeight(weight);
            persistenceService.saveSpeciesBatch(batch);
        }

        // si présence de plusieurs valeurs et aucune vide, renseigner les champs "Poids Observé" en face des classes de taille
        boolean importWeights = weightValuesSize == notNullWeightsSize;

        List<SpeciesBatchFrequency> frequencies = new ArrayList<>();
        for (Float lengthStep : weightsByLengthStep.keySet()) {
            SpeciesBatchFrequency frequency = SpeciesBatchFrequencys.newSpeciesBatchFrequency();
            frequencies.add(frequency);

            frequency.setBatch(batch);
            frequency.setLengthStep(lengthStep);
            frequency.setLengthStepCaracteristic(lengthStepPmfm);

            Collection<Float> weights = weightsByLengthStep.get(lengthStep);
            frequency.setNumber(weights.size());

            if (importWeights) {
                // all the weights are not null
                float totalWeight = 0f;
                for (float weight : weights) {
                    totalWeight += weight;
                }
                if (totalWeight > 0f) {
                    // convert grams to kilograms
                    totalWeight = WeightUnit.KG.round(totalWeight / 1000);
                    frequency.setWeight(totalWeight);
                }
            }
        }
        return frequencies;
    }

    protected void addFileAsAttachment(File f, CatchBatch catchBatch) {
        Attachment attachment = Attachments.newAttachment();
        attachment.setObjectType(ObjectTypeCode.CATCH_BATCH);
        attachment.setObjectId(Integer.valueOf(catchBatch.getId()));
        attachment.setName(f.getName());
        String date = DateFormat.getDateTimeInstance().format(context.currentDate());
        String comment = t("tutti.service.bigfin.import.attachment.comment", date);
        attachment.setComment(comment);
        persistenceService.createAttachment(attachment, f);
    }

    /**
     * Class describing the category of a batch level and the function to separate the rows to import by category value
     */
    private class Category {

        private Integer pmfmId;

        /** function to get the value of the caracteristic we want to group the batches by (eg size or gender) */
        private Function<BigfinDataRow, Sign> categoryValueGetter;

        public Category(Integer pmfmId, Function<BigfinDataRow, Sign> dataGetter) {
            this.pmfmId = pmfmId;
            this.categoryValueGetter = dataGetter;
        }

        public Integer getPmfmId() {
            return pmfmId;
        }

        public Function<BigfinDataRow, Sign> getCategoryValueGetter() {
            return categoryValueGetter;
        }
    }

    /**
     * Class containing the common parameters for the batch browsing
     * These parameter do not change when you go deeper.
     */
    private class BrowseBatchesParameter {
        /** the current fishing operation */
        private FishingOperation operation;

        /** the current species */
        private Species species;

        /** the lengthstep caracteristic found in the protocol */
        private Caracteristic lengthStepPmfm;

        /** the ordered categories */
        private List<Category> categories;

        /** the result of the import (to add the errors) */
        private BigfinImportResult result;

        /** label for the species in the errors */
        private String speciesLabel;

        public BrowseBatchesParameter(FishingOperation operation, Species species, Caracteristic lengthStepPmfm,
                                      List<Category> categories, BigfinImportResult result) {
            this.operation = operation;
            this.species = species;
            this.lengthStepPmfm = lengthStepPmfm;
            this.categories = categories;
            this.result = result;

            speciesLabel = species.getSurveyCode();
            if (StringUtils.isBlank(speciesLabel)) {
                speciesLabel = species.getRefTaxCode();
            }
        }

        public FishingOperation getOperation() {
            return operation;
        }

        public Species getSpecies() {
            return species;
        }

        public Caracteristic getLengthStepPmfm() {
            return lengthStepPmfm;
        }

        public List<Category> getCategories() {
            return categories;
        }

        public BigfinImportResult getResult() {
            return result;
        }

        public String getSpeciesLabel() {
            return speciesLabel;
        }
    }
}
