package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import java.util.List;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CaracteristicParserFormatter extends EntityParserFormatterSupport<Caracteristic> {

    public static CaracteristicParserFormatter newFormatter() {
        return new CaracteristicParserFormatter(false, null);
    }

    public static CaracteristicParserFormatter newTechnicalFormatter() {
        return new CaracteristicParserFormatter(true, null);
    }

    public static CaracteristicParserFormatter newParser(List<Caracteristic> entities) {
        return new CaracteristicParserFormatter(true, entities);
    }

    private final List<Caracteristic> entities;

    protected CaracteristicParserFormatter(boolean technical, List<Caracteristic> entities) {
        super("", technical, Caracteristic.class);
        this.entities = entities;
    }

    @Override
    protected List<Caracteristic> getEntities() {
        return entities;
    }

    @Override
    protected List<Caracteristic> getEntitiesWithObsoletes() {
        return entities;
    }

    @Override
    protected String formatBusiness(Caracteristic caracteristic) {
        return caracteristic.getParameterName()
               + " - " + caracteristic.getMatrixName()
               + " - " + caracteristic.getFractionName()
               + " - " + caracteristic.getMethodName();
    }
}
