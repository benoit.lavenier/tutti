package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.service.genericformat.csv.AccidentalCatchRow;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.CatchRow;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationRow;
import fr.ifremer.tutti.service.genericformat.csv.MarineLitterRow;
import fr.ifremer.tutti.service.genericformat.csv.OperationRow;
import fr.ifremer.tutti.service.genericformat.csv.ParameterRow;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class GenericFormatExportOperationContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatExportOperationContext.class);

    private final Cruise cruise;

    private final FishingOperation operation;

    private final CatchBatch catchBatch;

    private BatchContainer<MarineLitterBatch> rootMarineLitterBatch;

    private BatchContainer<SpeciesBatch> rootSpeciesBatch;

    private BatchContainer<SpeciesBatch> rootBenthosBatch;

    private List<IndividualObservationBatch> individualObservations;

    private List<AccidentalBatch> accidentalBatches;

    private boolean withCatchBatch;

    private final PersistenceService persistenceService;

    protected final Caracteristic weightMeasuredCaracteristic;

    protected final Caracteristic pmfmIdCaracteristic;

    protected final Caracteristic deadOrAliveCaracteristic;

    protected final Caracteristic genderCaracteristic;
    private final Caracteristic copyIndividualObservationModeCaracteristic;
    private final Caracteristic sampleCodeCaracteristic;

    private final SampleCategoryModel sampleCategoryModel;

    private final String operationLabel;

    private OperationRow operationRow;

    private List<ParameterRow> parameterRows;

    private List<MarineLitterRow> marineLitterRows;

    private List<IndividualObservationRow> individualObservationRows;

    private List<AccidentalCatchRow> accidentalCatchRows;

    private List<CatchRow> catchRows;

    private final List<AttachmentRow> attachmentRows;

    public GenericFormatExportOperationContext(Cruise cruise,
                                               FishingOperation operation,
                                               String operationLabel,
                                               PersistenceService persistenceService,
                                               WeightComputingService weightComputingService,
                                               SampleCategoryModel sampleCategoryModel,
                                               Caracteristic weightMeasuredCaracteristic,
                                               Caracteristic pmfmIdCaracteristic,
                                               Caracteristic deadOrAliveCaracteristic,
                                               Caracteristic genderCaracteristic,
                                               Caracteristic copyIndividualObservationModeCaracteristic,
                                               Caracteristic sampleCodeCaracteristic) {
        this.cruise = cruise;
        this.operation = operation;
        this.operationLabel = operationLabel;
        this.sampleCategoryModel = sampleCategoryModel;
        this.persistenceService = persistenceService;
        this.weightMeasuredCaracteristic = weightMeasuredCaracteristic;
        this.pmfmIdCaracteristic = pmfmIdCaracteristic;
        this.deadOrAliveCaracteristic = deadOrAliveCaracteristic;
        this.genderCaracteristic = genderCaracteristic;
        this.copyIndividualObservationModeCaracteristic = copyIndividualObservationModeCaracteristic;
        this.sampleCodeCaracteristic = sampleCodeCaracteristic;
        this.attachmentRows = new ArrayList<>();

        Integer operationId = operation.getIdAsInt();

        accidentalBatches = persistenceService.getAllAccidentalBatch(operationId);

        withCatchBatch = persistenceService.isFishingOperationWithCatchBatch(operationId);

        if (withCatchBatch) {


            // load batches

            catchBatch = persistenceService.getCatchBatchFromFishingOperation(operationId);

            boolean withError = false;

            try {
                rootSpeciesBatch = weightComputingService.getComputedSpeciesBatches(operationId);
            } catch (Exception e) {
                if (log.isWarnEnabled()) {
                    log.warn("Could not getComputedSpeciesBatches", e);
                }
                withError = true;
                rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operationId, false);
            }

            try {
                rootBenthosBatch = weightComputingService.getComputedBenthosBatches(operationId);
            } catch (Exception e) {
                if (log.isWarnEnabled()) {
                    log.warn("Could not getComputedBenthosBatches", e);
                }
                withError = true;
                rootBenthosBatch = persistenceService.getRootBenthosBatch(operationId, false);
            }

            try {
                rootMarineLitterBatch = weightComputingService.getComputedMarineLitterBatches(operationId, catchBatch.getMarineLitterTotalWeight());
            } catch (Exception e) {
                if (log.isWarnEnabled()) {
                    log.warn("Could not getComputedMarineLitterBatches", e);
                }
                withError = true;
                rootMarineLitterBatch = persistenceService.getRootMarineLitterBatch(operationId);
            }

            // apply compute weights

            if (!withError) {
                try {
                    weightComputingService.computeCatchBatchWeights(catchBatch, rootSpeciesBatch, rootBenthosBatch, rootMarineLitterBatch);
                } catch (Exception e) {
                    if (log.isWarnEnabled()) {
                        log.warn("Could not computeCatchBatchWeights", e);
                    }
                }
            }

            individualObservations = persistenceService.getAllIndividualObservationBatchsForFishingOperation(operation.getIdAsInt());

        } else {
            if (log.isWarnEnabled()) {
                log.warn("Skip fishing operation " + operationId + " since no catchBatch associated.");
            }
            catchBatch = null;
            rootSpeciesBatch = null;
            rootBenthosBatch = null;
            rootMarineLitterBatch = null;
        }

    }

    public Cruise getCruise() {
        return cruise;
    }

    public FishingOperation getOperation() {
        return operation;
    }

    public CatchBatch getCatchBatch() {
        return catchBatch;
    }

    public BatchContainer<MarineLitterBatch> getRootMarineLitterBatch() {
        return rootMarineLitterBatch;
    }

    public BatchContainer<SpeciesBatch> getRootSpeciesBatch() {
        return rootSpeciesBatch;
    }

    public BatchContainer<SpeciesBatch> getRootBenthosBatch() {
        return rootBenthosBatch;
    }

    public boolean isWithCatchBatch() {
        return withCatchBatch;
    }

    public List<IndividualObservationBatch> getIndividualObservations() {
        return individualObservations;
    }

    public Caracteristic getWeightMeasuredCaracteristic() {
        return weightMeasuredCaracteristic;
    }

    public Caracteristic getPmfmIdCaracteristic() {
        return pmfmIdCaracteristic;
    }

    public Caracteristic getDeadOrAliveCaracteristic() {
        return deadOrAliveCaracteristic;
    }

    public Caracteristic getGenderCaracteristic() {
        return genderCaracteristic;
    }

    public Caracteristic getCopyIndividualObservationModeCaracteristic() {
        return copyIndividualObservationModeCaracteristic;
    }

    public Caracteristic getSampleCodeCaracteristic() {
        return sampleCodeCaracteristic;
    }

    public List<AccidentalBatch> getAccidentalBatches() {
        return accidentalBatches;
    }

    public boolean isVracBatch(SpeciesBatch batch) {
        return persistenceService.isVracBatch(batch);
    }

    public List<SpeciesBatchFrequency> getAllBenthosBatchFrequency(Integer id) {
        return persistenceService.getAllBenthosBatchFrequency(id);
    }

    public List<SpeciesBatchFrequency> getAllSpeciesBatchFrequency(Integer id) {
        return persistenceService.getAllSpeciesBatchFrequency(id);
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return sampleCategoryModel;
    }

    public String getOperationLabel() {
        return operationLabel;
    }

    public void setOperationRow(OperationRow operationRow) {
        this.operationRow = operationRow;
    }

    public OperationRow getOperationRow() {
        return operationRow;
    }

    public void setParameterRows(List<ParameterRow> parameterRows) {
        this.parameterRows = parameterRows;
    }

    public List<ParameterRow> getParameterRows() {
        return parameterRows;
    }

    public void setMarineLitterRows(List<MarineLitterRow> marineLitterRows) {
        this.marineLitterRows = marineLitterRows;
    }

    public List<MarineLitterRow> getMarineLitterRows() {
        return marineLitterRows;
    }

    public void setIndividualObservationRows(List<IndividualObservationRow> individualObservationRows) {
        this.individualObservationRows = individualObservationRows;
    }

    public List<IndividualObservationRow> getIndividualObservationRows() {
        return individualObservationRows;
    }

    public void setAccidentalCatchRows(List<AccidentalCatchRow> accidentalCatchRows) {
        this.accidentalCatchRows = accidentalCatchRows;
    }

    public List<AccidentalCatchRow> getAccidentalCatchRows() {
        return accidentalCatchRows;
    }

    public void setCatchRows(List<CatchRow> catchRows) {
        this.catchRows = catchRows;
    }

    public List<CatchRow> getCatchRows() {
        return catchRows;
    }

    public List<AttachmentRow> getAttachmentRows() {
        return attachmentRows;
    }

    public void addAttachmentRows(List<AttachmentRow> attachmentRows) {

        if (CollectionUtils.isNotEmpty(attachmentRows)) {
            this.attachmentRows.addAll(attachmentRows);
        }

    }
}
