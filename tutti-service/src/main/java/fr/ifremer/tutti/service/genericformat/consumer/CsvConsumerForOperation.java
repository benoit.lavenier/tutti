package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportValidationHelper;
import fr.ifremer.tutti.service.genericformat.csv.OperationModel;
import fr.ifremer.tutti.service.genericformat.csv.OperationRow;
import org.apache.commons.lang3.BooleanUtils;
import org.nuiton.csv.ImportRow;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;

import java.nio.file.Path;
import java.util.Set;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForOperation extends CsvComsumer<OperationRow, OperationModel> {

    public CsvConsumerForOperation(Path file, char separator, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {
        super(file, OperationModel.forImport(separator, parserFactory), reportError);
    }

    public GenericFormatImportCruiseContext validateRow(ImportRow<OperationRow> row, GenericFormatContextSupport importContext) {

        GenericFormatImportValidationHelper validationHelper = importContext.getValidationHelper();

        GenericFormatImportCruiseContext cruiseContext = validationHelper.getCruise(this, row, importContext);

        if (cruiseContext != null) {

            OperationRow bean = row.getBean();

            FishingOperation fishingOperation = bean.getFishingOperation();

            if (cruiseContext.isFishingOperationAlreadyImported(bean)) {

                addCheckError(row, new FishingOperationAlreadyImportedException(fishingOperation));

            } else {

                Gear gear = bean.getGear();
                Short rankOrder = bean.getGearRankOrder();

                if (gear != null && rankOrder != null) {

                    Gear cruiseGear = validationHelper.getGear(this, importContext, row, gear, rankOrder);
                    if (cruiseGear != null) {
                        bean.setGear(cruiseGear);
                    }
                }
                NuitonValidatorResult validatorResult = validationHelper.validateFishingOperation(fishingOperation);

                if (validatorResult.hasFatalMessages()) {

                    Set<String> errorMessages = validationHelper.getMessages(validatorResult, NuitonValidatorScope.FATAL);
                    addCheckError(row, new FishingOperationNotValidException(fishingOperation, errorMessages));

                }

                //TODO other checks ?

            }

        }

        reportError(row);

        return cruiseContext;

    }

    public void prepareRowForPersist(ImportRow<OperationRow> row, boolean importSpecies, boolean importBenthos, boolean importMarineLitter) {

        OperationRow bean = row.getBean();

        CatchBatch catchBatch = bean.getCatchBatch();

        if (BooleanUtils.toBooleanDefaultIfNull(bean.getCatchTotalWeightComputed(), false)) {
            catchBatch.setCatchTotalWeight(null);
        }
        if (BooleanUtils.toBooleanDefaultIfNull(bean.getCatchTotalRejectedWeightComputed(), false)) {
            catchBatch.setCatchTotalRejectedWeight(null);
        }

        prepareSpecies(importSpecies, bean, catchBatch);

        prepareBenthos(importBenthos, bean, catchBatch);

        prepareMarineLitter(importMarineLitter, bean, catchBatch);

    }

    protected void prepareSpecies(boolean importSpecies, OperationRow bean, CatchBatch catchBatch) {

        if (!importSpecies || BooleanUtils.toBooleanDefaultIfNull(bean.getSpeciesTotalSortedWeightComputed(), false)) {
            catchBatch.setSpeciesTotalSortedWeight(null);
        }
        if (!importSpecies || BooleanUtils.toBooleanDefaultIfNull(bean.getSpeciesTotalInertWeightComputed(), false)) {
            catchBatch.setSpeciesTotalInertWeight(null);
        }
        if (!importSpecies || BooleanUtils.toBooleanDefaultIfNull(bean.getSpeciesTotalLivingNotItemizedWeightComputed(), false)) {
            catchBatch.setSpeciesTotalLivingNotItemizedWeight(null);
        }

    }

    protected void prepareBenthos(boolean importBenthos, OperationRow bean, CatchBatch catchBatch) {
        if (!importBenthos || BooleanUtils.toBooleanDefaultIfNull(bean.getBenthosTotalSortedWeightComputed(), false)) {
            catchBatch.setBenthosTotalSortedWeight(null);
        }
        if (!importBenthos || BooleanUtils.toBooleanDefaultIfNull(bean.getBenthosTotalInertWeightComputed(), false)) {
            catchBatch.setBenthosTotalInertWeight(null);
        }
        if (!importBenthos || BooleanUtils.toBooleanDefaultIfNull(bean.getBenthosTotalLivingNotItemizedWeightComputed(), false)) {
            catchBatch.setBenthosTotalLivingNotItemizedWeight(null);
        }
    }

    protected void prepareMarineLitter(boolean importMarineLitter, OperationRow bean, CatchBatch catchBatch) {
        if (!importMarineLitter || BooleanUtils.toBooleanDefaultIfNull(bean.getMarineLitterTotalWeightComputed(), false)) {
            catchBatch.setMarineLitterTotalWeight(null);
        }
    }

}