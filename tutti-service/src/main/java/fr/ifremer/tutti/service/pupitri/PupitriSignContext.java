package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * Pour aggreger toutes les lignes lors d'un import pupitri ayant la même tuple (espece - sorted - sign).
 *
 * Created on 11/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class PupitriSignContext implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Signs signs;

    private float weight;

    private int nbSmallBox;

    private int nbBigBox;

    public PupitriSignContext(Signs signs) {
        this.signs = signs;
    }

    public Signs getSigns() {
        return signs;
    }

    public float getWeight() {
        return weight;
    }

    public int getNbBigBox() {
        return nbBigBox;
    }

    public int getNbBox() {
        return nbSmallBox + nbBigBox;
    }

    public int getNbSmallBox() {
        return nbSmallBox;
    }

    void incrementNbSmallBox() {
        nbSmallBox++;
    }

    void incrementNbBigBox() {
        nbBigBox++;
    }

    void addWeight(float weight) {
        this.weight += weight;
    }

    public void addNbBoxs(PupitriSignContext otherSignContext) {
        nbSmallBox += otherSignContext.getNbSmallBox();
        nbBigBox += otherSignContext.getNbBigBox();
    }
}
