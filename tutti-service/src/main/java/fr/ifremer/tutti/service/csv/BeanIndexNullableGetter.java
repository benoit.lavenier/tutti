package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.nuiton.csv.ValueGetter;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class BeanIndexNullableGetter<E, T> implements ValueGetter<E, T> {

    protected String collectionName;

    protected int order;

    protected String propertyName;

    BeanIndexNullableGetter(String collectionName, int order, String suffix) {
        this.collectionName = collectionName;
        this.order = order;
        this.propertyName = collectionName + "[" + order + "]." + suffix;

    }

    @Override
    public T get(E object) throws Exception {
        T value = null;
        try {
            Collection c = (Collection) PropertyUtils.getProperty(object, collectionName);
            if (c != null && order < c.size()) {
                value = (T) PropertyUtils.getProperty(object, propertyName);
            }
        } catch (NestedNullException e) {
            value = null;
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof NullPointerException) {
                value = null;
            } else {
                throw e;
            }
        }
        return value;
    }
}
