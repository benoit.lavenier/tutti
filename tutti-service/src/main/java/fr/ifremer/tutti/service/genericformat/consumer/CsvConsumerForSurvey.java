package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportValidationHelper;
import fr.ifremer.tutti.service.genericformat.csv.SurveyModel;
import fr.ifremer.tutti.service.genericformat.csv.SurveyRow;
import org.nuiton.csv.ImportRow;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForSurvey extends CsvComsumer<SurveyRow, SurveyModel> {

    public CsvConsumerForSurvey(Path file, char separator, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {
        super(file, SurveyModel.forImport(separator, parserFactory), reportError);
    }

    public void validateRow(ImportRow<SurveyRow> row, GenericFormatContextSupport importContext) {

        if (row.isValid()) {

            SurveyRow bean = row.getBean();

            Cruise cruise = bean.getCruise();

            CruiseDataModel existingCruiseData = importContext.getImportRequest().getExistingCruiseData(cruise);

            boolean cruiseExist = existingCruiseData != null;

            if (importContext.isCruiseAlreadyImported(cruise)) {

                addCheckError(row, new CruiseAlreadyExistException(cruise));

            } else {

                if (cruiseExist) {

                    cruise.setId(existingCruiseData.getId());

                }

                GenericFormatImportValidationHelper validationHelper = importContext.getValidationHelper();
                NuitonValidatorResult nuitonValidatorResult = validationHelper.validateCruise(cruise);
                if (nuitonValidatorResult.hasErrorMessagess()) {

                    Set<String> errorMessages = validationHelper.getMessages(nuitonValidatorResult, NuitonValidatorScope.ERROR);
                    addCheckError(row, new CruiseNotValidException(cruise, errorMessages));

                }

                Program program = cruise.getProgram();
                if (program != null) {

                    Program expectedProgram = importContext.getImportRequest().getProgram();
                    if (!expectedProgram.equals(program)) {

                        addCheckError(row, new MismatchProgramException(cruise, expectedProgram));
                    }
                }

            }

        }

        reportError(row);

    }

    public void prepareRowForPersist(ImportRow<SurveyRow> row) {

        Cruise bean = row.getBean().getCruise();

        if (!bean.isGearEmpty()) {

            List gear = bean.getGear();
            List<GearWithOriginalRankOrder> gearWithOriginalRankOrders = GearWithOriginalRankOrders.toGearWithOriginalRankOrders(gear);
            bean.setGear(gearWithOriginalRankOrders);
        }

    }

}