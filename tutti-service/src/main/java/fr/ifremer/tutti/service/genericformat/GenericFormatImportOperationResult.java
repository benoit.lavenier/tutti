package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import java.io.Serializable;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;

/**
 * Created on 2/22/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportOperationResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private final FishingOperation fishingOperation;

    private final CatchBatch catchBatch;

    private final String label;

    private final Set<String> checkErrors;

    private final boolean weightsDeleted;

    private int nbGearFeatures;

    private int nbVesselFeatures;

    private int nbAccidentalCatches;

    private int nbMarineLitters;

    private int nbIndividualObservations;

    private int nbIndividualObservationsImported;

    private int nbSpeciesTaxon;

    private int nbBenthosTaxon;

    private final boolean override;

    public GenericFormatImportOperationResult(GenericFormatImportOperationContext operationContext) {
        this.fishingOperation = operationContext.getFishingOperation();
        this.label = operationContext.getFishingOperationLabel();
        this.override = operationContext.isOverride();
        this.catchBatch = operationContext.getCatchBatch();
        this.checkErrors = operationContext.getCheckErrors();
        this.weightsDeleted = operationContext.isWeightsDeleted();
        flushContext(operationContext);
    }

    public boolean isOverride() {
        return override;
    }

    public String getLabel() {
        return label;
    }

    public String getId() {
        return fishingOperation.getId();
    }

    public boolean isValid() {
        return BooleanUtils.isTrue(fishingOperation.getFishingOperationValid());
    }

    public boolean isWithInvalidWeights() {
        return CollectionUtils.isNotEmpty(checkErrors);
    }

    public boolean isWeightsDeleted() {
        return weightsDeleted;
    }
    public Set<String> getCheckErrors() {
        return checkErrors;
    }

    public Float getCatchTotalWeight() {
        return catchBatch.getCatchTotalWeight();
    }

    public Float getCatchTotalRejectedWeight() {
        return catchBatch.getCatchTotalRejectedWeight();
    }

    public Float getSpeciesTotalSortedWeight() {
        return catchBatch.getSpeciesTotalSortedWeight();
    }

    public Float getBenthosTotalSortedWeight() {
        return catchBatch.getBenthosTotalSortedWeight();
    }

    public boolean isWithAccidentalCatches() {
        return nbAccidentalCatches > 0;
    }

    public boolean isWithMarineLitter() {
        return nbMarineLitters > 0;
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public CatchBatch getCatchBatch() {
        return catchBatch;
    }

    public int getNbGearFeatures() {
        return nbGearFeatures;
    }

    public int getNbVesselFeatures() {
        return nbVesselFeatures;
    }

    public int getNbAccidentalCatches() {
        return nbAccidentalCatches;
    }

    public int getNbMarineLitters() {
        return nbMarineLitters;
    }

    public int getNbIndividualObservations() {
        return nbIndividualObservations;
    }

    public int getNbIndividualObservationsImported() {
        return nbIndividualObservationsImported;
    }

    public int getNbSpeciesTaxon() {
        return nbSpeciesTaxon;
    }

    public int getNbBenthosTaxon() {
        return nbBenthosTaxon;
    }

    protected void flushContext(GenericFormatImportOperationContext operationContext) {

        //TODO See what else to set here (catch stuff ?)
        if (operationContext.withGearFeatures()) {
            nbGearFeatures = operationContext.getGearUseFeatures().size();
        }
        if (operationContext.withVesselFeatures()) {
            nbVesselFeatures = operationContext.getVesselUseFeatures().size();
        }
        if (operationContext.withAccidentalBatches()) {
            nbAccidentalCatches = operationContext.getAccidentalBatches().size();
        }
        if (operationContext.withMarineLitterBatches()) {
            nbMarineLitters = operationContext.getMarineLitterBatches().size();
        }
        if (operationContext.withIndividualObservationBatches()) {
            nbIndividualObservations = operationContext.getIndividualObservationBatches().size();
            nbIndividualObservationsImported = operationContext.getIndividualObservationBatchImported().size();
        }

        if (operationContext.withSpeciesBatches(true)) {
            nbSpeciesTaxon = operationContext.getNbSpeciesTaxon();
        }
        if (operationContext.withBenthosBatches(true)) {
            nbBenthosTaxon = operationContext.getNbBenthosTaxon();
        }

    }

}
