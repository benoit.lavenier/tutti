package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.Weights;
import org.apache.commons.lang3.mutable.MutableFloat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Result of a pupitri import.
 *
 * Created on 12/17/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class PupitriImportResult {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PupitriImportResult.class);

    /**
     * Nombre de lignes importes depuis le fichier tremie.
     */
    private int nbTrunkImported;

    /**
     * Nombre de lignes rejetes depuis le fichier tremie.
     */
    private int nbTrunkNotImported;

    /**
     * Nombre d'espèces importées depuis le fichier carrousel.
     */
    private int nbCarrousselImported;

    /**
     * Poids total trié cumulé depuis le fichier tremie.
     */
    private Float sortedWeight;

    /**
     * Poids total rejeté cumulé depuis le fichier tremie.
     */
    private Float rejectedWeight;

    /**
     * Poids total du VRAC cumulé depuis le fichier carrousel.
     */
    private Float carrouselSortedWeight;

    /**
     * Poids total du HORS-VRAC cumulé depuis le fichier carrousel.
     */
    private Float carrouselUnsortedWeight;

    /**
     * Liste des lots lus depuis le fichier carrousel. Chaque lot est un tuple (espece,trie).
     */
    private final List<PupitriSpeciesContext> catches;

    /**
     * Ensemble de identifiants d'espèces non importés depuis el fichier carrousel.
     *
     * @since 3.10
     */
    private final Set<String> notImportedSpeciesIds;

    /**
     * Poids total des espèces du mélange cumulé depuis le fichier carrousel, i.e de toutes les lignes dont
     * l'espèce appartient à l'ensemble {@link PupitriSpeciesContext#MELAG_SPECIES}.
     *
     * @since 3.4.2
     */
    private MutableFloat melagTotalWeight;

    /**
     * Poids total des espèces du mélange cumulé depuis le fichier carrousel, i.e de toutes les lignes dont
     * l'espèce appartient à l'ensemble {@link PupitriSpeciesContext#MELAG_SPECIES}. Poids renseigné par l'utilisateur.
     *
     * @since 4.5
     */
    private Float editedMelagTotalWeight;

    /**
     * Poids total des lots echantillone cumulé depuis le fichier carrousel, i.e toutes les lignes dont le signe de
     * l'espèce est {@code T}.
     *
     * @since 3.4.2
     */
    private MutableFloat melagSortedWeight;

    /**
     * Commentaire à ajouter sur tous les lots importés en tant que MELAG.
     *
     * @since 3.10
     */
    private String melagComment;

    /**
     * L'identifiant de la pièce-jointe du rapport généré suite à l'import.
     *
     * @since 3.11
     */
    private String reportAttachmentId;

    /**
     * Le nom du rapport généré suite à l'import.
     *
     * @since 3.11
     */
    private String reportAttachmentFilename;

    public PupitriImportResult() {
        this.catches = new ArrayList<>();
        this.notImportedSpeciesIds = new LinkedHashSet<>();
    }

    public boolean isFishingOperationFound() {
        return nbTrunkImported + nbCarrousselImported > 0;
    }

    public int getNbTrunkImported() {
        return nbTrunkImported;
    }

    public int getNbTrunkNotImported() {
        return nbTrunkNotImported;
    }

    public int getNbCarrousselImported() {
        return nbCarrousselImported;
    }

    public int getNbCarrousselNotImported() {
        return notImportedSpeciesIds.size();
    }

    public Float getSortedWeight() {
        return sortedWeight;
    }

    public Float getRejectedWeight() {
        return rejectedWeight;
    }

    public Float getCarrouselSortedWeight() {
        return carrouselSortedWeight;
    }

    public Float getCarrouselUnsortedWeight() {
        return carrouselUnsortedWeight;
    }

    public List<PupitriSpeciesContext> getCatches() {
        return catches;
    }

    public Float getMelagTotalWeight() {
        return melagTotalWeight == null ? null : melagTotalWeight.floatValue();
    }

    public void setMelagTotalWeight(float melagTotalWeight) {
        this.melagTotalWeight.setValue(melagTotalWeight);
    }

    public Float getEditedMelagTotalWeight() {
        return editedMelagTotalWeight;
    }

    public void setEditedMelagTotalWeight(float editedMelagTotalWeight) {
        this.editedMelagTotalWeight = editedMelagTotalWeight;
    }

    public Float getMelagSortedWeight() {
        return melagSortedWeight == null ? null : melagSortedWeight.floatValue();
    }

    public String getMelagComment() {
        return melagComment;
    }

    public Set<String> getNotImportedSpeciesIds() {
        return ImmutableSet.copyOf(notImportedSpeciesIds);
    }

    public String getReportAttachmentId() {
        return reportAttachmentId;
    }

    public String getReportAttachmentFilename() {
        return reportAttachmentFilename;
    }

    /**
     * @return {@code true} if there is a total melag weight defined (at least one species MELA-GNE found).
     * @since 3.4.2
     */
    public boolean isFoundTotalMelag() {
        return melagTotalWeight != null;
    }

    /**
     * @return {@code true} if at least one species has a sorted melag weight.
     * @since 3.4.2
     */
    public boolean isFoundSortedMelag() {
        return melagSortedWeight != null;
    }

    void incrementNbTrunkImported() {
        this.nbTrunkImported++;
    }

    void incrementNbTrunkNotImported() {
        this.nbTrunkNotImported++;
    }

    void setSortedWeight(Float sortedWeight) {
        this.sortedWeight = sortedWeight;
    }

    void setRejectedWeight(Float rejectedWeight) {
        this.rejectedWeight = rejectedWeight;
    }

    void setReportAttachmentId(String reportAttachmentId) {
        this.reportAttachmentId = reportAttachmentId;
    }

    void setReportAttachmentFilename(String reportAttachmentFilename) {
        this.reportAttachmentFilename = reportAttachmentFilename;
    }

    void flushCarrouselResult(CarrouselImportRequestResult carrouselImportRequestResult) {

        // set carrousel sorted weight
        this.carrouselSortedWeight = WeightUnit.KG.round(carrouselImportRequestResult.getCarrouselSortedWeight());
        this.carrouselUnsortedWeight = WeightUnit.KG.round(carrouselImportRequestResult.getCarrouselUnsortedWeight());

        // set nb carrousel imported batches
        this.nbCarrousselImported = carrouselImportRequestResult.getNbCarrousselImported();
        this.notImportedSpeciesIds.addAll(carrouselImportRequestResult.getNotImportedSpeciesIds());

        List<PupitriSpeciesContext> incomingCatches = carrouselImportRequestResult.getCatches();

        for (PupitriSpeciesContext aCatch : incomingCatches) {
            addCatch(aCatch);
        }

    }

    void prepareMelag(Decorator<Species> speciesDecorator) {

        boolean useMelag = isFoundTotalMelag() && isFoundSortedMelag();

        float melagRatio = 1f;

        if (useMelag) {
            melagRatio = melagSortedWeight.floatValue() / editedMelagTotalWeight;

            if (log.isInfoEnabled()) {
                log.info("Use Melag (sorted weight: " + melagSortedWeight.floatValue() + " / total weight: " + editedMelagTotalWeight + ")");
            }
        }


        String unitLabel = WeightUnit.KG.getShortLabel();

        StringBuilder melagCommentBuilder = new StringBuilder(t("tutti.service.pupitri.import.createMelag.comment.part1") + "\n");

        Iterator<PupitriSpeciesContext> iterator = catches.iterator();
        while (iterator.hasNext()) {
            PupitriSpeciesContext aCatch = iterator.next();

            if (useMelag && aCatch.isMelagMetaSpecies()) {

                // remove the MELAG species from import
                iterator.remove();
                continue;

            }

            if (aCatch.isForMelag()) {

                if (useMelag) {

                    // compute the weight from melag

                    Float sampleWeight = aCatch.getWeight(Signs.MELAG);
                    Float weight = WeightUnit.KG.round(sampleWeight / melagRatio);

                    if (!aCatch.containsSign(Signs.BIG)) {

                        // No BIG batch associated with this melag species
                        // use default batch
                        // see https://forge.codelutin.com/issues/5742
                        aCatch.setMelagElevatedWeight(Signs.DEFAULT, weight);

                        if (log.isInfoEnabled()) {
                            log.info("Add melag batch for " + aCatch + " as default (no G found) : " + weight);
                        }

                    } else {

                        // also found a big batch associated with this melga species
                        // transform it as a small batch instead of default
                        // see https://forge.codelutin.com/issues/5742
                        aCatch.setMelagElevatedWeight(Signs.SMALL, weight);

                        if (log.isInfoEnabled()) {
                            log.info("Add melag batch for " + aCatch + " as small (G found) : " + weight);
                        }

                    }

                    melagCommentBuilder.append(t("tutti.service.pupitri.import.createMelag.comment.part2", sampleWeight, unitLabel, speciesDecorator.toString(aCatch.getSpecies()))).append("\n");

                } else {

                    // move the melag weight as a default weight

                    aCatch.moveMelagToDefaultSign();

                }
            }

        }

        if (useMelag) {

            melagCommentBuilder.append(t("tutti.service.pupitri.import.createMelag.comment.part3", editedMelagTotalWeight, unitLabel)).append("\n");

            if (WeightUnit.KG.isNotEquals(editedMelagTotalWeight, melagTotalWeight.floatValue())) {
                melagCommentBuilder.append(t("tutti.service.pupitri.import.createMelag.comment.part4", melagTotalWeight.floatValue(), unitLabel, editedMelagTotalWeight, unitLabel)).append("\n");
            }

            melagComment = melagCommentBuilder.toString();
            if (log.isInfoEnabled()) {
                log.info("Melag comment:\n" + melagComment);
            }

        }

    }

    private void addCatch(PupitriSpeciesContext aCatch) {

        catches.add(aCatch);

        if (aCatch.isMelagMetaSpecies()) {

            if (log.isInfoEnabled()) {
                log.info("Found a MELAG catch: " + aCatch);
            }
            // add weight to melag
            addMelagTotalWeight(aCatch.getWeight(Signs.DEFAULT));

        }

        if (aCatch.isForMelag()) {

            // add weight to sorted melag
            if (log.isInfoEnabled()) {
                log.info("Found a catch used in MELAG: " + aCatch);
            }
            addMelagSortedWeight(aCatch.getWeight(Signs.MELAG));

        }

    }

    private void addMelagTotalWeight(float weight) {

        if (melagTotalWeight == null) {
            melagTotalWeight = new MutableFloat();
        }
        melagTotalWeight.add(weight);

    }

    private void addMelagSortedWeight(float weight) {

        if (melagSortedWeight == null) {
            melagSortedWeight = new MutableFloat();
        }
        melagSortedWeight.add(weight);

    }
}
