package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.OperationDataModel;
import fr.ifremer.tutti.service.genericformat.csv.RowWithOperationContextSupport;

import java.io.Closeable;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 2/22/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportCruiseContext implements Closeable, Iterable<GenericFormatImportOperationContext> {

    /**
     * The imported cruise.
     */
    private final Cruise cruise;

    /**
     * The optional selection data.
     */
    private final CruiseDataModel selectiontCruiseData;

    /**
     * The optional existing cruise.
     */
    private final CruiseDataModel existingCruiseData;

    private final String cruiseLabel;

    private final Map<String, GenericFormatImportOperationContext> fishingOperationContexts;

    private final Set<Gear> gearsWithcaracteristics;

    private final Set<FishingOperation> existingOperations;

    private final Set<String> skippedFishingOperationsNaturalId;

    public GenericFormatImportCruiseContext(Cruise cruise, CruiseDataModel selectiontCruiseData, CruiseDataModel existingCruiseData, Set<FishingOperation> existingOperations, String cruiseLabel) {
        this.cruise = cruise;
        this.selectiontCruiseData = selectiontCruiseData;
        this.existingCruiseData = existingCruiseData;
        this.existingOperations = existingOperations == null ? Collections.<FishingOperation>emptySet() : existingOperations;
        this.cruiseLabel = cruiseLabel;
        this.fishingOperationContexts = new TreeMap<>();
        this.gearsWithcaracteristics = new HashSet<>();
        this.skippedFishingOperationsNaturalId = new LinkedHashSet<>();
    }

    public Iterable<GenericFormatImportOperationContext> orderedFishingOperationContexts() {

        return Ordering.from(FishingOperations.FISHING_OPERATION_COMPARATOR).onResultOf(new Function<GenericFormatImportOperationContext, FishingOperation>() {

            @Override
            public FishingOperation apply(GenericFormatImportOperationContext input) {
                return input.getFishingOperation();
            }
        }).sortedCopy(fishingOperationContexts.values());

    }

    @Override
    public void close() {

        fishingOperationContexts.values().forEach(GenericFormatImportOperationContext::close);
        fishingOperationContexts.clear();

    }

    @Override
    public Iterator<GenericFormatImportOperationContext> iterator() {
        return ImmutableList.copyOf(fishingOperationContexts.values()).iterator();
    }

    public Cruise getCruise() {
        return cruise;
    }

    public CruiseDataModel getExistingCruiseData() {
        return existingCruiseData;
    }

    public OperationDataModel getExistingFishingOperationData(FishingOperation importRowCruise) {

        OperationDataModel result = null;

        String fishingOperationId = getExistingFishingOperationId(importRowCruise);
        if (fishingOperationId != null) {

            for (OperationDataModel operationDataModel : existingCruiseData) {
                if (fishingOperationId.equals(operationDataModel.getId())) {
                    result = operationDataModel;
                    break;
                }
            }
        }
        return result;

    }


    public String getCruiseLabel() {
        return cruiseLabel;
    }

    public Gear getGear(Gear gear, int rankOrder) {

        Gear result = null;

        int index = rankOrder - 1;
        boolean gearExists = cruise.sizeGear() > index;
        if (gearExists) {
            Gear cruiseGear = cruise.getGear(index);
            gearExists = cruiseGear.getId().equals(gear.getId());
            if (gearExists) {
                result = cruiseGear;
            }
        }
        return result;

    }

    public void addFishingOperation(FishingOperation fishingOperation, CatchBatch catchBatch, String fishingOperationLabel) {

        OperationDataModel existingFishingOperationData = getExistingFishingOperationData(fishingOperation);
        fishingOperationContexts.put(fishingOperation.getId(), new GenericFormatImportOperationContext(fishingOperation, catchBatch, existingFishingOperationData, fishingOperationLabel));
    }

    public void addSkippedFishingOperation(FishingOperation fishingOperation) {

        String naturalId = FishingOperations.getNaturalId(fishingOperation);
        skippedFishingOperationsNaturalId.add(naturalId);

    }

    public boolean isFishingOperationSkipped(RowWithOperationContextSupport row) {

        FishingOperation fishingOperation = row.getFishingOperation();
        String naturalId = FishingOperations.getNaturalId(fishingOperation);
        return skippedFishingOperationsNaturalId.contains(naturalId);

    }

    public boolean isFishingOperationAlreadyImported(RowWithOperationContextSupport row) {

        GenericFormatImportOperationContext fishingOperationContext = getFishingOperationContext(row);
        return fishingOperationContext != null;

    }

    public OperationDataModel getSelectedFishingOperation(FishingOperation row) {

        OperationDataModel result = null;

        for (OperationDataModel selectedFishingOperation : selectiontCruiseData) {
            boolean equals = FishingOperations.equalsNaturalId(row, selectedFishingOperation.getId());
            if (equals) {
                result = selectedFishingOperation;
                break;
            }
        }

        return result;

    }

    public GenericFormatImportOperationContext getFishingOperationContext(RowWithOperationContextSupport row) {

        FishingOperation fishingOperation = row.getFishingOperation();
        GenericFormatImportOperationContext result = null;
        for (GenericFormatImportOperationContext importOperationContext : fishingOperationContexts.values()) {

            FishingOperation importedFishingOperation = importOperationContext.getFishingOperation();
            boolean equals = FishingOperations.equals(importedFishingOperation, fishingOperation);
            if (equals) {
                result = importOperationContext;
                break;
            }

        }

        return result;

    }

    public Iterable<FishingOperation> getFishingOperations() {
        return fishingOperationContexts.values().stream().map(GenericFormatImportOperationContext.TO_FISHING_OPERATION_FUNCTION::apply).collect(Collectors.toList());
    }

    public boolean withGearCaracteristics() {
        return !gearsWithcaracteristics.isEmpty();
    }

    public void addGearCaracteristic(Gear gear, Caracteristic caracteristic, Serializable caracteristicValue) {

        gearsWithcaracteristics.add(gear);

        CaracteristicMap caracteristics = gear.getCaracteristics();
        if (caracteristics == null) {
            caracteristics = new CaracteristicMap();
            gear.setCaracteristics(caracteristics);
        }
        caracteristics.put(caracteristic, caracteristicValue);

    }

    public Set<Gear> getGearsWithcaracteristics() {
        return gearsWithcaracteristics;
    }

    protected String getExistingFishingOperationId(FishingOperation importRowCruise) {

        String result = null;
        for (FishingOperation fishingOperation : existingOperations) {

            boolean equals = FishingOperations.equals(importRowCruise, fishingOperation);
            if (equals) {
                result = fishingOperation.getId();
                break;
            }

        }
        return result;

    }
}
