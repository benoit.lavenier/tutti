package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForCatch;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.CatchRow;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportCatchAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportCatchAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportCatchAction(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid()
                && importContext.getSurveyFileResult().isValid()
                && importContext.getOperationFileResult().isValid()
                && (importContext.getImportRequest().isImportSpecies() ||
                importContext.getImportRequest().isImportBenthos());
    }

    @Override
    protected void skipExecute() {

        importContext.increments(t("tutti.service.genericFormat.skip.import.catches"));

        if (!(importContext.getImportRequest().isImportSpecies() ||
                importContext.getImportRequest().isImportBenthos())) {
            GenericFormatCsvFileResult importFileResult = importContext.getCatchFileResult();
            importFileResult.setSkipped(true);
        }

    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Import catch.csv file.");
        }
        importContext.increments(t("tutti.service.genericFormat.import.catches"));
        GenericFormatCsvFileResult importFileResult = importContext.getCatchFileResult();
        try (CsvConsumerForCatch consumer = importContext.loadCatches(true)) {
            for (ImportRow<CatchRow> row : consumer) {

                GenericFormatImportOperationContext operationContext = consumer.validateRow(row, importContext);
                if (operationContext != null) {
                    consumer.prepareRowForPersist(operationContext, row);
                }

            }
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close catch.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        if (importContext.getImportRequest().isImportSpecies()) {
            persistSpeciesBatches();
        }

        if (importContext.getImportRequest().isImportBenthos()) {
            persistBenthosBatches();
        }

    }

    public void persistSpeciesBatches() {

        importContext.doActionOnCruiseContexts(new GenericFormatImportContext.CruiseContextAction() {

            @Override
            public void onCruise(GenericFormatImportCruiseContext cruiseContext, ProgressionModel progressionModel) {

                for (GenericFormatImportOperationContext fishingOperationContext : cruiseContext) {

//                    boolean override = fishingOperationContext.isOverride();
//
//                    if (override) {
//
//                        deleteSpeciesBatches(fishingOperationContext);
//
//                    }

                    deleteSpeciesBatches(fishingOperationContext);

                    String cruiseStr = cruiseContext.getCruiseLabel();
                    String operationStr = fishingOperationContext.getFishingOperationLabel();

                    importContext.increments(t("tutti.service.genericFormat.persist.operation.speciesBatches", cruiseStr, operationStr));

                    if (fishingOperationContext.withSpeciesBatches(true)) {

                        Collection<SpeciesBatch> batches = fishingOperationContext.getSpeciesBatches(true);
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + batches.size() + " VRAC root species batch(es) of " + operationStr + " for cruise: " + cruiseStr);
                        }
                        persistSpeciesBatches(fishingOperationContext, batches, null);

                    }

                    if (fishingOperationContext.withSpeciesBatches(false)) {

                        Collection<SpeciesBatch> batches = fishingOperationContext.getSpeciesBatches(false);
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + batches.size() + " HORS VRAC root species batch(es) of " + operationStr + " for cruise: " + cruiseStr);
                        }
                        persistSpeciesBatches(fishingOperationContext, batches, null);

                    }

                }

            }

            private void persistSpeciesBatches(GenericFormatImportOperationContext fishingOperationContext, Collection<SpeciesBatch> batches, Integer parentId) {

                int rankOrder = 1;

                for (SpeciesBatch batch : batches) {

                    batch.setRankOrder(rankOrder++);

                    Integer batchObjectId = fishingOperationContext.getBatchObjectId(batch.getIdAsInt());

                    List<SpeciesBatchFrequency> frequencies = fishingOperationContext.getSpeciesFrequencies(batch);

                    // reset temporary id to persist batch
                    batch.setId((Integer) null);
                    SpeciesBatch createdBatch = persistenceHelper.createSpeciesBatch(batch, parentId);
                    Collection<AttachmentRow> attachmentRows = importContext.popAttachmentRows(ObjectTypeCode.BATCH, batchObjectId);
                    persistenceHelper.persistAttachments(createdBatch.getIdAsInt(), attachmentRows);

                    // keep id translation (will be used by individual observations)
                    fishingOperationContext.registerPersistedSpeciesBatchId(batchObjectId, createdBatch.getIdAsInt());

                    if (CollectionUtils.isNotEmpty(frequencies)) {

                        Integer batchId = batch.getIdAsInt();
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + frequencies.size() + " frequency(ies) (species batch: " + batchId + ") of " + fishingOperationContext.getFishingOperationLabel() + " for cruise: " + importContext.decorate(fishingOperationContext.getFishingOperation().getCruise()));
                        }
                        persistenceHelper.saveSpeciesBatchFrequency(batchId, frequencies);

                    }

                    if (!batch.isChildBatchsEmpty()) {

                        persistSpeciesBatches(fishingOperationContext, batch.getChildBatchs(), createdBatch.getIdAsInt());

                    }

                }

            }

            private void deleteSpeciesBatches(GenericFormatImportOperationContext fishingOperationContext) {

                FishingOperation fishingOperation = fishingOperationContext.getFishingOperation();
                persistenceHelper.deleteSpeciesBatchForFishingOperation(fishingOperation.getIdAsInt());

            }

        });


    }

    public void persistBenthosBatches() {

        importContext.doActionOnCruiseContexts(new GenericFormatImportContext.CruiseContextAction() {

            @Override
            public void onCruise(GenericFormatImportCruiseContext cruiseContext, ProgressionModel progressionModel) {

                for (GenericFormatImportOperationContext fishingOperationContext : cruiseContext) {

//                    boolean override = fishingOperationContext.isOverride();
//
//                    if (override) {
//
//                        deleteBenthosBatches(fishingOperationContext);
//
//                    }

                    deleteBenthosBatches(fishingOperationContext);

                    String cruiseStr = cruiseContext.getCruiseLabel();
                    String operationStr = fishingOperationContext.getFishingOperationLabel();

                    importContext.increments(t("tutti.service.genericFormat.persist.operation.benthosBatches", cruiseStr, operationStr));

                    if (fishingOperationContext.withBenthosBatches(true)) {

                        Collection<SpeciesBatch> batches = fishingOperationContext.getBenthosBatches(true);
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + batches.size() + " VRAC benthos batch(es) of " + operationStr + " for cruise: " + cruiseStr);
                        }
                        persistBenthosBatches(fishingOperationContext, batches, null);

                    }

                    if (fishingOperationContext.withBenthosBatches(false)) {

                        Collection<SpeciesBatch> batches = fishingOperationContext.getBenthosBatches(false);
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + batches.size() + " HORS VRAC benthos batch(es) of " + operationStr + " for cruise: " + cruiseContext.getCruiseLabel());
                        }
                        persistBenthosBatches(fishingOperationContext, batches, null);

                    }

                }
            }

            private void persistBenthosBatches(GenericFormatImportOperationContext fishingOperationContext, Collection<SpeciesBatch> batches, Integer parentId) {

                int rankOrder = 1;

                for (SpeciesBatch batch : batches) {

                    batch.setRankOrder(rankOrder++);

                    Integer batchObjectId = fishingOperationContext.getBatchObjectId(batch.getIdAsInt());

                    List<SpeciesBatchFrequency> frequencies = fishingOperationContext.getBenthosFrequencies(batch);

                    // reset temporary id to persist batch
                    batch.setId((Integer) null);

                    SpeciesBatch createdBatch = persistenceHelper.createBenthosBatch(batch, parentId);

                    Collection<AttachmentRow> attachmentRows = importContext.popAttachmentRows(ObjectTypeCode.BATCH, batchObjectId);
                    persistenceHelper.persistAttachments(createdBatch.getIdAsInt(), attachmentRows);

                    if (CollectionUtils.isNotEmpty(frequencies)) {

                        Integer batchId = batch.getIdAsInt();
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + frequencies.size() + " frequency(ies) (benthos batch: " + batchId + ") of " + fishingOperationContext.getFishingOperationLabel() + " for cruise: " + importContext.decorate(fishingOperationContext.getFishingOperation().getCruise()));
                        }
                        persistenceHelper.saveBenthosBatchFrequency(batchId, frequencies);

                    }

                    if (!batch.isChildBatchsEmpty()) {

                        persistBenthosBatches(fishingOperationContext, batch.getChildBatchs(), createdBatch.getIdAsInt());

                    }
                }

            }

            private void deleteBenthosBatches(GenericFormatImportOperationContext fishingOperationContext) {

                FishingOperation fishingOperation = fishingOperationContext.getFishingOperation();
                persistenceHelper.deleteBenthosBatchForFishingOperation(fishingOperation.getIdAsInt());

            }

        });

    }

}
