package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created on 3/25/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class RestoreAfterImportAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RestoreAfterImportAction.class);

    private final PersistenceService persistenceService;

    public RestoreAfterImportAction(GenericFormatContextSupport importContext, PersistenceService persistenceService) {
        super(importContext);
        this.persistenceService = persistenceService;
    }

    @Override
    protected boolean canExecute() {
        return true;
    }

    @Override
    protected void doExecute() {
        
        Set<Runnable> actions = new HashSet<>();
        actions.add(() -> rollbackSampleCategoryModel(importContext.getImportRequest()));
        actions.add(() -> rollbackProtocol(importContext.getImportRequest()));

        for (Runnable action : actions) {
            try {
                action.run();
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not execute rollback action", e);
                }
            }
        }

    }

    protected void rollbackSampleCategoryModel(GenericFormatImportRequest importRequest) {

        SampleCategoryModel sampleCategoryModel = importRequest.getSampleCategoryModel();

        if (log.isInfoEnabled()) {
            log.info("Rollback previous sample cateogry model: " + sampleCategoryModel);
        }
        persistenceService.setSampleCategoryModel(sampleCategoryModel);

    }

    protected void rollbackProtocol(GenericFormatImportRequest importRequest) {

        if (importRequest.isOverrideProtocol()) {

            String protocolOriginalName = importContext.getProtocolOriginalName();

            TuttiProtocol toDelete = persistenceService.getProtocolByName(protocolOriginalName);

            if (log.isInfoEnabled()) {
                log.info("Delete previous protocol: " + toDelete.getId() + " - " + toDelete.getName());
            }

            // delete previous protocol
            persistenceService.deleteProtocol(toDelete.getId());

            TuttiProtocol importedProtocol = importContext.getImportedProtocol();
            importedProtocol.setName(protocolOriginalName);
            if (log.isInfoEnabled()) {
                log.info("Save imported protocol: " + importedProtocol.getId() + " - " + importedProtocol.getName());
            }

            persistenceService.saveProtocol(importedProtocol);

        }

    }

}
