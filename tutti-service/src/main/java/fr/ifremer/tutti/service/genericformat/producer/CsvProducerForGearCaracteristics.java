package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicModel;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicRow;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForGearCaracteristics extends CsvProducer<GearCaracteristicRow, GearCaracteristicModel> {

    public CsvProducerForGearCaracteristics(Path file, GearCaracteristicModel model) {
        super(file, model);
    }

    public List<GearCaracteristicRow> getDataToExport(Cruise cruise, Gear gear) {

        List<GearCaracteristicRow> rows = new ArrayList<>();
        CaracteristicMap caracteristics = gear.getCaracteristics();
        if (caracteristics != null) {
            for (Map.Entry<Caracteristic, Serializable> entry : caracteristics.entrySet()) {
                Caracteristic caracteristic = entry.getKey();
                Serializable value = entry.getValue();
                GearCaracteristicRow row = new GearCaracteristicRow();
                row.setCruise(cruise);
                row.setGear(gear);
                row.setRankOrder(gear.getRankOrder());
                row.setCaracteristic(caracteristic);
                row.setValue(value);
                rows.add(row);
            }
        }

        return rows;

    }

}
