package fr.ifremer.tutti.service.bigfin;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.bigfin.csv.BigfinDataRow;
import fr.ifremer.tutti.service.bigfin.csv.SpeciesOrSpeciesBatch;
import fr.ifremer.tutti.service.bigfin.signs.Sign;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class BigfinImportContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BigfinImportContext.class);

    protected final FishingOperation operation;

    protected final CatchBatch catchBatch;

    protected final Map<Sign, CaracteristicQualitativeValue> signsToCaracteristicValue;

    protected final Map<String, Species> speciesBySurveyCode;

    protected final Map<String, SpeciesProtocol> speciesProtocolBySurveyCode;

    // set of not found species already added in the errors
    protected final Set<Species> speciesNotRecognized = new HashSet<>();

    //set of species not in the protocol
    protected final Set<Species> speciesNotInProtocol = new HashSet<>();

    // set of species without lengthstep already added in the errors
    protected final Set<Species> speciesInProtocolButWithoutLengthStepPmfmId = new HashSet<>();

    private final List<BigfinDataRow> speciesRows = new ArrayList<>();

    private final List<BigfinDataRow> speciesBatchRows = new ArrayList<>();

    private final BigfinImportResult bigfinImportResult;

    private final BatchContainer<SpeciesBatch> rootSpeciesBatch;

    private final Map<Species, Caracteristic> lengthStepPmfmBySpecies;

    public BigfinImportContext(File importFile,
                               FishingOperation operation,
                               CatchBatch catchBatch,
                               Map<Sign, CaracteristicQualitativeValue> signsToCaracteristicValue,
                               Map<String, Species> speciesBySurveyCode,
                               Map<String, SpeciesProtocol> speciesProtocolBySurveyCode,
                               BatchContainer<SpeciesBatch> rootSpeciesBatch) {
        this.rootSpeciesBatch = rootSpeciesBatch;
        this.bigfinImportResult = new BigfinImportResult(importFile);
        this.operation = operation;
        this.catchBatch = catchBatch;
        this.signsToCaracteristicValue = signsToCaracteristicValue;
        this.speciesBySurveyCode = speciesBySurveyCode;
        this.speciesProtocolBySurveyCode = speciesProtocolBySurveyCode;
        this.lengthStepPmfmBySpecies = new HashMap<>();
    }

    public Caracteristic getLengthStepPmfm(Species species, PersistenceService persistenceService) {
        Caracteristic caracteristic = lengthStepPmfmBySpecies.get(species);
        if (caracteristic == null) {

            SpeciesProtocol speciesProtocol = getSpeciesProtocol(species);
            caracteristic = persistenceService.getCaracteristic(Integer.parseInt(speciesProtocol.getLengthStepPmfmId()));
            lengthStepPmfmBySpecies.put(species, caracteristic);

        }
        return caracteristic;
    }

    public boolean isNoError() {
        return bigfinImportResult.getErrors().isEmpty();
    }

    public void addRowToProcess(BigfinDataRow bigfinDataRow) {

        if (bigfinDataRow.getSpeciesOrSpeciesBatch().isSpecies()) {
            speciesRows.add(bigfinDataRow);
        } else {
            speciesBatchRows.add(bigfinDataRow);
        }

    }

    public Map<Integer, SpeciesBatch> getSpeciesBatchesById() {
        return SpeciesBatchs.getAllSpeciesBatchesById(this.rootSpeciesBatch);
    }

    public Multimap<Species, SpeciesBatch> getRootSpeciesBatchBySpecies() {
        return Multimaps.index(rootSpeciesBatch.getChildren(), SpeciesBatch::getSpecies);
    }

    public Multimap<Species, BigfinDataRow> getSpeciesRowsBySpecies() {

        // separate the imported rows by species

        return Multimaps.index(speciesRows, bigfinDataRow -> {
            return bigfinDataRow.getSpeciesOrSpeciesBatch().getSpecies();
        });
    }

    public Multimap<SpeciesBatch, BigfinDataRow> getSpeciesBatchRowsBySpeciesBatch() {

        return Multimaps.index(speciesBatchRows, input -> {
            return input.getSpeciesOrSpeciesBatch().getBatch();
        });

    }

    public boolean isStationFound(BigfinDataRow bean) {

        String station = bean.getStation();
        Date date = bean.getDate();
        return station != null
                         && station.equals(operation.getStationNumber())
                         && date != null
                         && DateUtils.isSameDay(date, operation.getGearShootingStartDate());

    }

    public SpeciesProtocol getSpeciesProtocol(Species species) {

        String code = species.getSurveyCode();
        if (StringUtils.isBlank(code)) {
            code = species.getReferenceTaxonId().toString();
        }
        return speciesProtocolBySurveyCode.get(code);

    }

    public boolean checkRow(BigfinDataRow bigfinDataRow) {

        // check if the station is the one of the operation
        // and do not check again a species that has not been recognized before
        boolean stationFound = isStationFound(bigfinDataRow);

        boolean canBeAdd = false;

        if (stationFound) {

            String recordId = bigfinDataRow.getRecordId();

            SpeciesOrSpeciesBatch speciesOrspeciesBatch = bigfinDataRow.getSpeciesOrSpeciesBatch();

            if (speciesOrspeciesBatch.isSpecies()) {

                // do some checks on the given species

                checkSizeIsDefined(bigfinDataRow);

                Species species = speciesOrspeciesBatch.getSpecies();

                boolean speciesIsKnown = checkSpeciesIsKnown(recordId, species);

                if (speciesIsKnown) {

                    boolean speciesProtocoleIsSafe = checkSpeciesProtocol(recordId, species);

                    if (speciesProtocoleIsSafe) {

                        canBeAdd = true;

                    }
                }

            } else {

                // do some checks on the given species batch

                SpeciesBatch speciesBatch = speciesOrspeciesBatch.getBatch();

                // Check Species batch is a leaf
                boolean speciesBatchIsLeaf = checkSpeciesBatchIsLeaf(recordId, speciesBatch);

                if (speciesBatchIsLeaf) {

                    // Check Species protocol is safe

                    boolean speciesProtocoleIsSafe = checkSpeciesProtocol(recordId, speciesBatch.getSpecies());

                    if (speciesProtocoleIsSafe) {

                        canBeAdd = true;

                    }

                }

            }

        } else {
            if (log.isInfoEnabled()) {
                log.info("Station is not matching for record: " + bigfinDataRow.getRecordId());
            }
        }

        return canBeAdd;
    }

    public boolean checkSpeciesBatchIsLeaf(String recordId, SpeciesBatch batch) {

        boolean speciesBatchIsLeaf = batch.isChildBatchsEmpty();

        if (!speciesBatchIsLeaf) {

            String warning = t("tutti.service.bigfinImport.warning.speciesBatch.tooCategorized", recordId, batch.getId());
            if (log.isWarnEnabled()) {
                log.warn(warning);
            }
            bigfinImportResult.addWarning(warning);
        }

        return speciesBatchIsLeaf;

    }

    public void checkSizeIsDefined(BigfinDataRow bigfinDataRow) {

        Preconditions.checkArgument(bigfinDataRow.getSpeciesOrSpeciesBatch().isSpecies());

        if (bigfinDataRow.getSize() == null) {
            String error = t("tutti.service.bigfinImport.error.szClass.unknwon", bigfinDataRow.getRecordId());
            if (log.isErrorEnabled()) {
                log.error(error);
            }
            bigfinImportResult.addError(error);
        }

    }

    public boolean checkSpeciesIsKnown(String recordId, Species species) {

        boolean speciesIsKnown = true;

        if (species.getId() == null) {

            // bloquer tout si un "species" ne match pas le référentiel de Tutti : lister dans ce cas les codes non reconnus
            if (speciesNotRecognized.add(species)) {
                String error = t("tutti.service.bigfinImport.error.species.not.found", recordId, species.getExternalCode());
                if (log.isErrorEnabled()) {
                    log.error(error);
                }
                bigfinImportResult.addError(error);
            }

            speciesIsKnown = false;

        }

        return speciesIsKnown;

    }

    public boolean checkSpeciesProtocol(String recordId, Species species) {

        boolean speciesProtocoleIsSafe = true;

        String speciesLabel = getSpeciesLabel(species);

        SpeciesProtocol speciesProtocol = getSpeciesProtocol(species);

        // On n'importe pas les espèces non présentes dans le protocole et
        // on liste les espèces/catégorisées non importées pour aider l'utilisateur
        // à identifier le problème et on fait l'import des autres
        if (speciesProtocol == null) {

            speciesProtocoleIsSafe = false;

            if (speciesNotInProtocol.add(species)) {

                String error = t("tutti.service.bigfinImport.warning.species.notInProtocol",  recordId, speciesLabel);
                if (log.isWarnEnabled()) {
                    log.warn(error);
                }
                bigfinImportResult.addWarning(error);
            }

        } else {

            if (speciesProtocol.getLengthStepPmfmId() == null) {

                speciesProtocoleIsSafe = false;

                if (speciesInProtocolButWithoutLengthStepPmfmId.add(species)) {
                    // bloquer toute espèce reconnue du protocole mais qui n'a pas de méthode de mesure
                    String error = t("tutti.service.bigfinImport.error.species.without.lengthstep", recordId, speciesLabel);
                    if (log.isErrorEnabled()) {
                        log.error(error);
                    }
                    bigfinImportResult.addError(error);
                }
            }
        }

        return speciesProtocoleIsSafe;

    }

    public BigfinImportResult getResult() {
        return bigfinImportResult;
    }

    protected String getSpeciesLabel(Species species) {
        String speciesLabel = species.getSurveyCode();
        if (StringUtils.isBlank(speciesLabel)) {
            speciesLabel = species.getRefTaxCode();
        }
        return speciesLabel;
    }

    public Species getSpeciesWithSurveyCode(Species species) {
        String refTaxCode = species.getRefTaxCode();
        return speciesBySurveyCode.get(refTaxCode);
    }
}