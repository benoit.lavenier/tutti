package fr.ifremer.tutti.service.operationimport;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportModel;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ValueSetter;
import org.nuiton.util.DateUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 12/11/14.
 *
 * @author Kevin Morin - morin@codelutin.com
 * @since 3.10
 */
public class ImportFromColumnFileModel extends AbstractTuttiImportModel<FishingOperation> {

    private final FishingOperation fishingOperation;

    public ImportFromColumnFileModel(char separator, Set<String> headers,
                                     PersistenceService persistenceService,
                                     TuttiDataContext dataContext,
                                     FishingOperation fishingOperation) throws ImportFromColumnFileMissingHeaderException {
        super(separator);
        this.fishingOperation = fishingOperation;

        TuttiProtocol protocol = dataContext.getProtocol();
        Collection<OperationFieldMappingRow> operationFieldMapping = protocol.getOperationFieldMapping();

        String zoneId = dataContext.getProgram().getZone().getId();

        for (OperationFieldMappingRow mappingRow : operationFieldMapping) {
            String importColumn = mappingRow.getImportColumn();
            boolean importColumnsInHeaders = headers.remove(importColumn);
            if (StringUtils.isNotBlank(importColumn) && importColumnsInHeaders) {

                String field = mappingRow.getField();
                ValueSetter valueSetter = null;
                ValueParser valueParser;
                switch (field) {
                    case FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER:
                    case FishingOperation.PROPERTY_TRAWL_DISTANCE:
                        valueParser = TuttiCsvUtil.INTEGER;
                        break;

                    case FishingOperation.PROPERTY_GEAR_SHOOTING_START_LATITUDE:
                    case FishingOperation.PROPERTY_GEAR_SHOOTING_START_LONGITUDE:
                    case FishingOperation.PROPERTY_GEAR_SHOOTING_END_LATITUDE:
                    case FishingOperation.PROPERTY_GEAR_SHOOTING_END_LONGITUDE:
                        valueParser = TuttiCsvUtil.FLOAT;
                        break;

                    case FishingOperations.PROPERTY_GEAR_SHOOTING_START_DAY:
                        valueParser = TuttiCsvUtil.DAY;
                        valueSetter = new ValueSetter<FishingOperation, Date>() {
                            @Override
                            public void set(FishingOperation bean, Date value) throws Exception {
                                Date date = bean.getGearShootingStartDate();
                                if (date == null) {
                                    date = value;

                                } else {
                                    date = DateUtil.getDateAndTime(value, date, false, false);
                                }
                                bean.setGearShootingStartDate(date);
                            }
                        };
                        break;
                    case FishingOperations.PROPERTY_GEAR_SHOOTING_END_DAY:
                        valueParser = TuttiCsvUtil.DAY;
                        valueSetter = new ValueSetter<FishingOperation, Date>() {
                            @Override
                            public void set(FishingOperation bean, Date value) throws Exception {
                                Date date = bean.getGearShootingEndDate();
                                if (date == null) {
                                    date = value;

                                } else {
                                    date = DateUtil.getDateAndTime(value, date, false, false);
                                }
                                bean.setGearShootingEndDate(date);
                            }
                        };
                        break;

                    case FishingOperations.PROPERTY_GEAR_SHOOTING_START_TIME:
                        valueParser = TuttiCsvUtil.TIME;
                        valueSetter = new ValueSetter<FishingOperation, Date>() {
                            @Override
                            public void set(FishingOperation bean, Date value) throws Exception {
                                Date date = bean.getGearShootingStartDate();
                                if (date == null) {
                                    date = value;

                                } else {
                                    date = DateUtil.getDateAndTime(date, value, false, false);
                                }
                                bean.setGearShootingStartDate(date);
                            }
                        };
                        break;
                    case FishingOperations.PROPERTY_GEAR_SHOOTING_END_TIME:
                        valueParser = TuttiCsvUtil.TIME;
                        valueSetter = new ValueSetter<FishingOperation, Date>() {
                            @Override
                            public void set(FishingOperation bean, Date value) throws Exception {
                                Date date = bean.getGearShootingEndDate();
                                if (date == null) {
                                    date = value;

                                } else {
                                    date = DateUtil.getDateAndTime(date, value, false, false);
                                }
                                bean.setGearShootingEndDate(date);
                            }
                        };
                        break;

                    case FishingOperation.PROPERTY_FISHING_OPERATION_RECTILIGNE:
                        valueParser = TuttiCsvUtil.PRIMITIVE_BOOLEAN;
                        break;

                    case FishingOperation.PROPERTY_FISHING_OPERATION_VALID:
                        valueParser = TuttiCsvUtil.BOOLEAN;
                        break;

                    case FishingOperation.PROPERTY_GEAR:
                        List<Gear> gears = new ArrayList<>(dataContext.getFishingGears());
                        gears.addAll(dataContext.getScientificGears());
                        Map<String, Gear> gearUniverse = TuttiEntities.splitById(gears);
                        valueParser = newForeignKeyParserFormatter(Gear.class, field, gearUniverse);
                        break;

                    case FishingOperation.PROPERTY_VESSEL:
                        List<Vessel> vessels = new ArrayList<>(dataContext.getFishingVessels());
                        vessels.addAll(dataContext.getScientificVessels());
                        Map<String, Vessel> vesselUniverse = TuttiEntities.splitById(vessels);
                        valueParser = newForeignKeyParserFormatter(Vessel.class, field, vesselUniverse);
                        break;

                    case FishingOperation.PROPERTY_SECONDARY_VESSEL:
                        vessels = new ArrayList<>(dataContext.getFishingVessels());
                        vessels.addAll(dataContext.getScientificVessels());
                        vesselUniverse = TuttiEntities.splitById(vessels);
                        valueParser = newForeignKeyListParserFormatter(Vessel.class, field, vesselUniverse);
                        break;

                    case FishingOperation.PROPERTY_STRATA:
                        Map<String, TuttiLocation> strataUniverse = TuttiEntities.splitById(
                                persistenceService.getAllFishingOperationStrata(zoneId));
                        valueParser = newForeignKeyParserFormatter(TuttiLocation.class, field, strataUniverse);
                        break;

                    case FishingOperation.PROPERTY_SUB_STRATA:
                        Map<String, TuttiLocation> subStrataUniverse = TuttiEntities.splitById(
                                persistenceService.getAllFishingOperationSubStrata(zoneId, null));
                        valueParser = newForeignKeyParserFormatter(TuttiLocation.class, field, subStrataUniverse);
                        break;

                    case FishingOperation.PROPERTY_LOCATION:
                        Map<String, TuttiLocation> locationUniverse = TuttiEntities.splitById(
                                persistenceService.getAllFishingOperationLocation(zoneId, null, null));
                        valueParser = newForeignKeyParserFormatter(TuttiLocation.class, field, locationUniverse);
                        break;

                    case FishingOperation.PROPERTY_RECORDER_PERSON:
                        Map<String, Person> personUniverse = TuttiEntities.splitById(dataContext.getPersons());
                        valueParser = newForeignKeyListParserFormatter(Person.class, field, personUniverse);
                        break;

                    default:
                        valueParser = TuttiCsvUtil.STRING;

                }

                if (valueSetter == null) {
                    newMandatoryColumn(importColumn, field, valueParser);

                } else {
                    newMandatoryColumn(importColumn, valueParser, valueSetter);
                }

            } else if (!importColumnsInHeaders) {
                throw new ImportFromColumnFileMissingHeaderException(importColumn);
            }
        }

        Collection<CaracteristicMappingRow> caracteristicMappingRows = protocol.getCaracteristicMapping();
        for (final CaracteristicMappingRow mappingRow : caracteristicMappingRows) {
            String importColumn = mappingRow.getImportColumn();
            if (StringUtils.isNotBlank(importColumn) && headers.remove(importColumn)) {

                int pmfmId = Integer.parseInt(mappingRow.getPmfmId());
                final Caracteristic caracteristic = persistenceService.getCaracteristic(pmfmId);

                ValueSetter<FishingOperation, String> setter = (fishingOperation1, value) -> {

                    Serializable realValue = null;
                    if (StringUtils.isNotBlank(value)) {
                        switch (caracteristic.getCaracteristicType()) {
                            case QUALITATIVE:
                                for (CaracteristicQualitativeValue cqv : caracteristic.getQualitativeValue()) {
                                    if (value.equals(cqv.getId())) {
                                        realValue = cqv;
                                        break;
                                    }
                                }
                                break;

                            case NUMBER:
                                realValue = Float.parseFloat(value);
                                break;

                            default:
                                realValue = value;
                        }
                    }

                    CaracteristicMap caracteristicMap;
                    if (CaracteristicType.GEAR_USE_FEATURE.toString().equals(mappingRow.getTab())) {
                        caracteristicMap = fishingOperation1.getGearUseFeatures();
                        if (caracteristicMap == null) {
                            caracteristicMap = new CaracteristicMap();
                            fishingOperation1.setGearUseFeatures(caracteristicMap);
                        }

                    } else {
                        caracteristicMap = fishingOperation1.getVesselUseFeatures();
                        if (caracteristicMap == null) {
                            caracteristicMap = new CaracteristicMap();
                            fishingOperation1.setVesselUseFeatures(caracteristicMap);
                        }
                    }
                    caracteristicMap.put(caracteristic, realValue);

                };
                newMandatoryColumn(importColumn, setter);

            }
        }


        for (String header : headers) {
            newIgnoredColumn(header);
        }
    }

    @Override
    public FishingOperation newEmptyInstance() {
//            FishingOperation fishingOperation = FishingOperations.newFishingOperation();
//            EditFishingOperationUIModel model = getModel();
//            toBeanBinder.copy(model, fishingOperation);
        return fishingOperation;
    }
}
