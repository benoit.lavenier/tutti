package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;

/**
 * Created on 2/20/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class RowWithOperationContextSupport extends RowWithCruiseContextSupport {

    private static final long serialVersionUID = 1L;

    private FishingOperation fishingOperation;

    public void forImport() {
        super.forImport();
        setFishingOperation(FishingOperations.newFishingOperation());
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        this.fishingOperation = fishingOperation;
    }

    public void setStationNumber(String stationNumber) {
        fishingOperation.setStationNumber(stationNumber);
    }

    public void setFishingOperationNumber(Integer fishingOperationNumber) {
        fishingOperation.setFishingOperationNumber(fishingOperationNumber);
    }

    public void setMultirigAggregation(String multirigAggregation) {
        fishingOperation.setMultirigAggregation(multirigAggregation);
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public String getStationNumber() {
        return fishingOperation.getStationNumber();
    }

    public Integer getFishingOperationNumber() {
        return fishingOperation.getFishingOperationNumber();
    }

    public String getMultirigAggregation() {
        return fishingOperation.getMultirigAggregation();
    }

}
