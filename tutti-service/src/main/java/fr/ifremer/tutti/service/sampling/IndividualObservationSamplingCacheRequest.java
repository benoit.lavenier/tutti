package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.util.Objects;

/**
 * Définit une requète sur le cache des échantillons.
 *
 * Created on 20/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class IndividualObservationSamplingCacheRequest {

    private final FishingOperation fishingOperation;
    private final Species species;
    private final Integer lengthClass;
    private final CaracteristicQualitativeValue maturity;
    private final CaracteristicQualitativeValue gender;
    private final String samplingCode;

    public IndividualObservationSamplingCacheRequest(FishingOperation fishingOperation,
                                                     Species species,
                                                     Integer lengthClass,
                                                     CaracteristicQualitativeValue maturity,
                                                     CaracteristicQualitativeValue gender,
                                                     String samplingCode) {
        this.fishingOperation = fishingOperation;
        Objects.requireNonNull(species);
        this.species = species;
        this.lengthClass = lengthClass;
        this.maturity = maturity;
        this.gender = gender;
        this.samplingCode = samplingCode;
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public Species getSpecies() {
        return species;
    }

    public Integer getLengthClass() {
        return lengthClass;
    }

    public CaracteristicQualitativeValue getMaturity() {
        return maturity;
    }

    public CaracteristicQualitativeValue getGender() {
        return gender;
    }

    public String getSamplingCode() {
        return samplingCode;
    }

    public boolean withMaturity() {
        return maturity != null;
    }

    public boolean withGender() {
        return gender != null;
    }

    public boolean withLengthClass() {
        return lengthClass != null;
    }

    public boolean withSamplingCode() {
        return samplingCode != null;
    }

    @Override
    public String toString() {

        MoreObjects.ToStringHelper toStringHelper = MoreObjects.toStringHelper(this)
                                                               .add("fishingOperation", fishingOperation)
                                                               .add("species", species);
        if (withLengthClass()) {
            toStringHelper.add("lengthClass", lengthClass);
        }
        if (withMaturity()) {
            toStringHelper.add("maturity", maturity);
        }
        if (withGender()) {
            toStringHelper.add("gender", gender);
        }
        if (withSamplingCode()) {
            toStringHelper.add("samplingCode", samplingCode);
        }

        return toStringHelper.toString();

    }
}
