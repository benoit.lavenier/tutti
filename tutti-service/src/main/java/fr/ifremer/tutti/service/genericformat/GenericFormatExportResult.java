package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatExportResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private final GenericFormatExportConfiguration exportConfiguration;

    private final GenericFormatArchive archive;

    private final List<String> errorsByCruise;

    public GenericFormatExportResult(GenericFormatExportRequest exportRequest,
                                     List<String> errorsByCruise) {
        this.errorsByCruise = errorsByCruise;
        this.archive = exportRequest.getArchive();
        this.exportConfiguration = exportRequest.getExportConfiguration();

    }

    public GenericFormatExportConfiguration getExportConfiguration() {
        return exportConfiguration;
    }

    public GenericFormatArchive getArchive() {
        return archive;
    }

    public List<String> getErrorsByCruise() {
        return errorsByCruise;
    }

    public boolean isSuccess() {
        return errorsByCruise.isEmpty();
    }
}
