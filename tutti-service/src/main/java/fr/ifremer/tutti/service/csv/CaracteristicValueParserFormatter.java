package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristics;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueParserFormatter;

import java.io.Serializable;
import java.text.ParseException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CaracteristicValueParserFormatter implements ValueParserFormatter<Serializable> {

    public static CaracteristicValueParserFormatter newFormatter() {
        return new CaracteristicValueParserFormatter(false, null);
    }

    public static CaracteristicValueParserFormatter newTechnicalFormatter() {
        return new CaracteristicValueParserFormatter(true, null);
    }

    public static CaracteristicValueParserFormatter newParser(Caracteristic caracteristic) {
        return new CaracteristicValueParserFormatter(true, caracteristic);
    }

    private final boolean technical;

    private final Caracteristic caracteristic;

    private final ValueParserFormatter<Integer> integerDelegate;

    private final ValueParserFormatter<Float> floatDelegate;

    private final ValueParserFormatter<String> textDelegate;

    protected CaracteristicValueParserFormatter(boolean technical, Caracteristic caracteristic) {
        this.technical = technical;
        this.caracteristic = caracteristic;
        this.integerDelegate = Common.INTEGER;
        this.floatDelegate = Common.FLOAT;
        this.textDelegate = Common.STRING;
    }

    @Override
    public Serializable parse(String value) throws ParseException {

        Serializable result;

        if ("NA".equals(value)) {

            result = null;

        } else if (Caracteristics.isNumberCaracteristic(caracteristic)) {

            result = floatDelegate.parse(value);

        } else if (Caracteristics.isTextCaracteristic(caracteristic)) {

            result = value;

        } else {

            result = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, value);
            if (result == null) {
                throw new ParseException(t("tutti.service.csv.caracteristic.qualitativeValue.notFound", caracteristic.getId(), value), 0);
            }

        }

        return result;

    }

    @Override
    public String format(Serializable e) {

        String value;

        if (e == null) {

            value = "NA";

        } else {

            if (e instanceof Float) {

                value = floatDelegate.format((Float) e);

            } else if (e instanceof Integer) {

                value = integerDelegate.format((Integer) e);

            } else if (e instanceof String) {

                value = textDelegate.format((String) e);

            } else {

                CaracteristicQualitativeValue qv = (CaracteristicQualitativeValue) e;
                value = technical ? qv.getId() : qv.getDescription();

            }

        }

        return value;

    }

}
