package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.Programs;
import fr.ifremer.tutti.service.PersistenceService;

import java.util.List;

/**
 * Created on 2/15/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ProgramParserFormatter extends EntityParserFormatterSupport<Program> {

    public static ProgramParserFormatter newFormatter() {
        return new ProgramParserFormatter(false, null);
    }

    public static ProgramParserFormatter newTechnicalFormatter() {
        return new ProgramParserFormatter(true, null);
    }

    public static ProgramParserFormatter newParser(PersistenceService persistenceService) {
        return new ProgramParserFormatter(true, persistenceService);
    }

    private final PersistenceService persistenceService;

    protected ProgramParserFormatter(boolean technical, PersistenceService persistenceService) {
        super("", technical, Program.class);
        this.persistenceService = persistenceService;
    }

    @Override
    protected List<Program> getEntities() {
        return persistenceService.getAllProgram();
    }

    @Override
    protected List<Program> getEntitiesWithObsoletes() {
        return getEntities();
    }

    @Override
    protected String formatBusiness(Program value) {
        return Programs.GET_NAME.apply(value);
    }

}