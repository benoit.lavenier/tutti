package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;

import java.io.File;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class MultiPostImportResult {

    private final List<SpeciesBatchFrequency> importedFrequencies;
    private final List<IndividualObservationBatch> importedObservations;
    private final Multimap<Integer, File> observationAttachments;

    public MultiPostImportResult(List<SpeciesBatchFrequency> importedFrequencies,
                                 List<IndividualObservationBatch> importedObservations,
                                 Multimap<Integer, File> observationAttachments) {
        this.importedFrequencies = importedFrequencies;
        this.importedObservations = importedObservations;
        this.observationAttachments = observationAttachments;
    }

    public List<SpeciesBatchFrequency> getImportedFrequencies() {
        return importedFrequencies;
    }

    public List<IndividualObservationBatch> getImportedObservations() {
        return importedObservations;
    }

}
