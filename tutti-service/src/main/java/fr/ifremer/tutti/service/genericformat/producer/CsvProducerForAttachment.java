package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentModel;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created on 3/26/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class CsvProducerForAttachment extends CsvProducer<AttachmentRow, AttachmentModel> {

    private final Path sourcePath;

    private final Path targetPath;

    public CsvProducerForAttachment(Path file, AttachmentModel model,
                                    Path sourcePath,
                                    Path targetPath) {
        super(file, model);
        this.sourcePath = sourcePath;
        this.targetPath = targetPath;
    }

    @Override
    public void write(List<AttachmentRow> rows) throws Exception {
        super.write(rows);
        for (AttachmentRow row : rows) {

            String path = row.getPath();

            Path source = sourcePath.resolve(path);
            Path target = targetPath.resolve(path);
            Files.createDirectories(target.getParent());
            Files.copy(source, target);

        }
    }

    public void addAttachments(List<Attachment> attachments, List<AttachmentRow> rows) {

        for (Attachment attachment : attachments) {

            AttachmentRow row = new AttachmentRow();
            row.setAttachment(attachment);
            rows.add(row);

        }

    }

}

