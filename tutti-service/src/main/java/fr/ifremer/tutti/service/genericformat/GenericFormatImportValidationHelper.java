package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.TuttiValidationDataContextSupport;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.consumer.CruiseNotFoundException;
import fr.ifremer.tutti.service.genericformat.consumer.FishingOperationNotFoundInCruiseException;
import fr.ifremer.tutti.service.genericformat.consumer.GearNotFoundInCruiseException;
import fr.ifremer.tutti.service.genericformat.csv.RowWithCruiseContextSupport;
import fr.ifremer.tutti.service.genericformat.csv.RowWithOperationContextSupport;
import org.apache.commons.io.IOUtils;
import org.nuiton.csv.ImportRow;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/19/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportValidationHelper implements Closeable {

    private final ValidationService validationService;

    private final TuttiValidationDataContextSupport originalValidationDataContextSupport;

    private final GenericFormatImportValidationDataContext validationDataContext;

    public GenericFormatImportValidationHelper(GenericFormatContextSupport importContext, ValidationService validationService) {
        this.validationService = validationService;
        this.validationDataContext = new GenericFormatImportValidationDataContext(importContext);
        this.originalValidationDataContextSupport = TuttiValidationDataContextSupport.setValidationContext(validationDataContext, true);
    }

    public static String getMessage(String prefix, Set<String> messages) {

        StringBuilder builder = new StringBuilder(prefix);
        builder.append("<ul>");
        for (String message : messages) {
            builder.append("<li>").append(message).append("</li>");

        }
        builder.append("</ul>");
        return builder.toString();

    }

    public Set<String> getMessages(NuitonValidatorResult validatorResult, NuitonValidatorScope scope) {

        Set<String> result = null;
        if (validatorResult.hasMessagesForScope(scope)) {

            result = new LinkedHashSet<>();

            List<String> messagesForScope = validatorResult.getMessagesForScope(scope);
            for (String message : messagesForScope) {

                String i18nError = getI18nError(message);
                result.add(i18nError);
            }

        }
        return result;

    }

    protected String getI18nError(String error) {
        String text;
        if (!error.contains("##")) {
            text = t(error);
        } else {
            StringTokenizer stk = new StringTokenizer(error, "##");
            String errorName = stk.nextToken();
            List<String> args = new ArrayList<>();
            while (stk.hasMoreTokens()) {
                args.add(stk.nextToken());
            }
            text = t(errorName, args.toArray());
        }
        return text;
    }

    @Override
    public void close() {

        IOUtils.closeQuietly(validationService);

        TuttiValidationDataContextSupport.setValidationContext(originalValidationDataContextSupport, true);

    }

//    public void useFishingOperationInValidation(FishingOperation fishingOperation) {
//        validationDataContext.setFishingOperation(fishingOperation);
//    }

    public NuitonValidatorResult validateCruise(Cruise cruise) {
        return validationService.validateEditCruise(cruise);
    }

    public NuitonValidatorResult validateFishingOperation(FishingOperation fishingOperation) {

        Cruise previousCruise = validationDataContext.getCruise();
        try {
            Cruise cruise = fishingOperation.getCruise();
            validationDataContext.setCruise(cruise);
            validationDataContext.setFishingOperation(fishingOperation);
            return validationService.validateEditFishingOperation(fishingOperation);
        } finally {
            validationDataContext.setCruise(previousCruise);
        }
    }

    public <R extends RowWithCruiseContextSupport> GenericFormatImportCruiseContext getCruise(CsvComsumer<R, ?> consumer, ImportRow<R> row, GenericFormatContextSupport importContext) {

        GenericFormatImportCruiseContext cruiseContext = null;

        if (row.isValid()) {

            R bean = row.getBean();

            boolean cruiseSkipped = importContext instanceof GenericFormatImportContext
                                    && importContext.isCruiseSkipped(bean);

            if (!cruiseSkipped) {

                cruiseContext = importContext.getCruiseContext(bean);

                if (cruiseContext == null) {

                    consumer.addCheckError(row, new CruiseNotFoundException(bean.getCruise()));

                } else {

                    Cruise cruise = cruiseContext.getCruise();
                    bean.setCruise(cruise);
                    if (bean instanceof RowWithOperationContextSupport) {
                        ((RowWithOperationContextSupport) bean).getFishingOperation().setCruise(cruise);

                    }

                }

            }

        }

        return cruiseContext;

    }

    public <R extends RowWithOperationContextSupport> GenericFormatImportOperationContext getFishingOperationContext(CsvComsumer<R, ?> consumer, ImportRow<R> row, GenericFormatContextSupport importContext) {

        GenericFormatImportOperationContext fishingOperationContext = null;

        GenericFormatImportCruiseContext cruiseContext = getCruise(consumer, row, importContext);

        if (cruiseContext != null) {

            R bean = row.getBean();

            boolean fishingOperationSkipped = importContext instanceof GenericFormatImportContext
                                              && cruiseContext.isFishingOperationSkipped(bean);

            if (!fishingOperationSkipped) {

                fishingOperationContext = cruiseContext.getFishingOperationContext(bean);

                if (fishingOperationContext == null) {

                    consumer.addCheckError(row, new FishingOperationNotFoundInCruiseException(bean.getFishingOperation()));

                } else {

                    bean.setFishingOperation(fishingOperationContext.getFishingOperation());

                }

            }

        }

        return fishingOperationContext;

    }

    public <R extends RowWithCruiseContextSupport> Gear getGear(CsvComsumer<R, ?> consumer, GenericFormatContextSupport importContext, ImportRow<R> row, Gear gear, short rankOrder) {

        Gear cruiseGear = null;

        Cruise cruise = row.getBean().getCruise();
        GenericFormatImportCruiseContext cruiseContext = importContext.getCruiseContext(cruise);
        if (cruiseContext != null) {
            cruiseGear = cruiseContext.getGear(gear, rankOrder);
            if (cruiseGear == null) {
                consumer.addCheckError(row, new GearNotFoundInCruiseException(cruise, gear, rankOrder));
            }
        }

        return cruiseGear;

    }

}
