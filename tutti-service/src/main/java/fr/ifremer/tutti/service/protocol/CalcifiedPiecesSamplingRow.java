package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinitions;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.io.Serializable;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CalcifiedPiecesSamplingRow implements Serializable {

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_REFTAX = "reftax";

    public static final String PROPERTY_SURVEY_CODE = "surveyCode";

    public static final String PROPERTY_SCIENTIFIC_NAME = "scientificName";

    public static final String PROPERTY_MIN_SIZE = "minSize";

    public static final String PROPERTY_MAX_SIZE = "maxSize";

    public static final String PROPERTY_MATURITY = "maturity";

    public static final String PROPERTY_SEX = "sex";

    public static final String PROPERTY_MAX_BY_LENGHT_STEP = "maxByLenghtStep";

    public static final String PROPERTY_SAMPLING_INTERVAL = "samplingInterval";

    public static final String PROPERTY_OPERATION_LIMITATION = "operationLimitation";

    public static final String PROPERTY_ZONE_LIMITATION = "zoneLimitation";

    protected Species species;

    protected CalcifiedPiecesSamplingDefinition delegate;

    public CalcifiedPiecesSamplingRow() {
        delegate = CalcifiedPiecesSamplingDefinitions.newCalcifiedPiecesSamplingDefinition();
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Boolean getMaturity() {
        return delegate.getMaturity();
    }

    public Integer getMaxByLenghtStep() {
        return delegate.getMaxByLenghtStep();
    }

    public Integer getMaxSize() {
        return delegate.getMaxSize();
    }

    public int getMinSize() {
        return delegate.getMinSize();
    }

    public int getSamplingInterval() {
        return delegate.getSamplingInterval();
    }

    public Integer getOperationLimitation() {
        return delegate.getOperationLimitation();
    }

    public Integer getZoneLimitation() {
        return delegate.getZoneLimitation();
    }

    public boolean isSex() {
        return delegate.isSex();
    }

    public void setMaturity(Boolean maturity) {
        delegate.setMaturity(maturity);
    }

    public void setMaxByLenghtStep(Integer maxByLenghtStep) {
        delegate.setMaxByLenghtStep(maxByLenghtStep);
    }

    public void setMaxSize(Integer maxSize) {
        delegate.setMaxSize(maxSize);
    }

    public void setMinSize(int minSize) {
        delegate.setMinSize(minSize);
    }

    public void setOperationLimitation(Integer operationLimitation) {
        delegate.setOperationLimitation(operationLimitation);
    }

    public void setSamplingInterval(int samplingInterval) {
        delegate.setSamplingInterval(samplingInterval);
    }

    public void setSex(boolean sex) {
        delegate.setSex(sex);
    }

    public void setZoneLimitation(Integer zoneLimitation) {
        delegate.setZoneLimitation(zoneLimitation);
    }
}
