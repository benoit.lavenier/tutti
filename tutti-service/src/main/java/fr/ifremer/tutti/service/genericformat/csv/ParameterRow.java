package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import java.io.Serializable;

/**
 * A row in a parameter export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ParameterRow extends RowWithOperationContextSupport {

    private static final long serialVersionUID = 1L;

    public static enum ParameterType {GEAR, VESSEL}

    public static final String PROPERTY_CARACTERISTIC = "caracteristic";

    public static final String PROPERTY_VALUE = "value";

    public static final String PROPERTY_PARAMETER_TYPE = "parameterType";

    public static ParameterRow newEmptyInstance() {

        ParameterRow row = new ParameterRow();
        row.forImport();
        return row;

    }

    private Caracteristic caracteristic;

    private Serializable value;

    private ParameterType parameterType;

    public void setParameterType(ParameterType parameterType) {
        this.parameterType = parameterType;
    }

    public void setCaracteristic(Caracteristic caracteristic) {
        this.caracteristic = caracteristic;
    }

    public void setValue(Serializable value) {
        this.value = value;
    }

    public ParameterType getParameterType() {
        return parameterType;
    }

    public Caracteristic getCaracteristic() {
        return caracteristic;
    }

    public Serializable getValue() {
        return value;
    }
}
