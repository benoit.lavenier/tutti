package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinitions;
import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.AbstractTuttiService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * To import / export {@link TuttiProtocol} to {@code Yaml} file formats.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ProtocolImportExportService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ProtocolImportExportService.class);

    public void exportProtocol(TuttiProtocol protocol, File file) {
        TuttiProtocols.toFile(protocol, file);
    }

    public TuttiProtocol importProtocol(File file) {
        return TuttiProtocols.fromFile(file);
    }

    public List<Species> importProtocolSpecies(File file,
                                               TuttiProtocol protocol,
                                               Map<String, Caracteristic> caracteristicMap,
                                               Map<String, Species> speciesMap) {

        if (log.isInfoEnabled()) {
            log.info("Will import protocol [" + protocol.getName() + "] species from file: " + file);
        }

        List<Species> result = new ArrayList<>();

        Map<Integer, SpeciesProtocol> ids = new LinkedHashMap<>();

        if (!protocol.isSpeciesEmpty()) {

            // get existing species (will be replaced if required)

            for (SpeciesProtocol speciesProtocol : protocol.getSpecies()) {
                ids.put(speciesProtocol.getSpeciesReferenceTaxonId(), speciesProtocol);
            }

        }

        Map<Integer, SpeciesProtocol> benthosIds = new LinkedHashMap<>();

        if (!protocol.isBenthosEmpty()) {

            // get existing species (will be not be imported)

            for (SpeciesProtocol speciesProtocol : protocol.getBenthos()) {
                benthosIds.put(speciesProtocol.getSpeciesReferenceTaxonId(), speciesProtocol);
            }
        }

        try (Reader reader = Files.newReader(file, StandardCharsets.UTF_8)) {

            SpeciesRowModel csvModel = SpeciesRowModel.forImport(getCsvSeparator(), caracteristicMap, speciesMap);
            try (Import<SpeciesRow> importer = Import.newImport(csvModel, reader)) {

                Binder<SpeciesRow, SpeciesProtocol> binder =
                        BinderFactory.newBinder(SpeciesRow.class, SpeciesProtocol.class);

                for (SpeciesRow bean : importer) {

                    Species species = bean.getSpecies();
                    Integer id = species.getReferenceTaxonId();

                    SpeciesProtocol sp = benthosIds.get(id);
                    if (sp != null) {
                        result.add(species);

                    } else {

                        sp = ids.get(id);
                        if (sp == null) {

                            // create a new species protocol
                            sp = SpeciesProtocols.newSpeciesProtocol();
                        }
                        binder.copy(bean, sp);
                        sp.setMandatorySampleCategoryId(new ArrayList<>(bean.getMandatorySampleCategoryId()));

                        cleanRptData(sp);

                        ids.put(id, sp);
                    }
                }
            }

            List<SpeciesProtocol> values = new ArrayList<>(ids.values());
            protocol.setSpecies(values);

            return result;

        } catch (ImportRuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.protocol.import.species.error", protocol.getName(), file), e);
        }

    }

    /**
     * @param file             file to import
     * @param protocol         existing protocol
     * @param caracteristicMap dictonnary of caracteristics
     * @param speciesMap       dictionnary of species
     * @return The list of the species is not imported because they are already in the species
     */
    public List<Species> importProtocolBenthos(File file,
                                               TuttiProtocol protocol,
                                               Map<String, Caracteristic> caracteristicMap,
                                               Map<String, Species> speciesMap) {

        if (log.isInfoEnabled()) {
            log.info("Will import protocol [" + protocol.getName() + "] species from file: " + file);
        }

        List<Species> result = new ArrayList<>();

        Map<Integer, SpeciesProtocol> ids = new LinkedHashMap<>();

        if (!protocol.isBenthosEmpty()) {

            // get existing species (will be replaced if required)

            for (SpeciesProtocol speciesProtocol : protocol.getBenthos()) {
                ids.put(speciesProtocol.getSpeciesReferenceTaxonId(), speciesProtocol);
            }
        }

        Map<Integer, SpeciesProtocol> speciesIds = new LinkedHashMap<>();

        if (!protocol.isSpeciesEmpty()) {

            // get existing species (will be not be imported)

            for (SpeciesProtocol speciesProtocol : protocol.getSpecies()) {
                speciesIds.put(speciesProtocol.getSpeciesReferenceTaxonId(), speciesProtocol);
            }
        }

        try (Reader reader = Files.newReader(file, StandardCharsets.UTF_8)) {

            SpeciesRowModel csvModel = SpeciesRowModel.forImport(getCsvSeparator(), caracteristicMap, speciesMap);
            try (Import<SpeciesRow> importer = Import.newImport(csvModel, reader)) {

                Binder<SpeciesRow, SpeciesProtocol> binder =
                        BinderFactory.newBinder(SpeciesRow.class, SpeciesProtocol.class);

                for (SpeciesRow bean : importer) {

                    Species species = bean.getSpecies();
                    Integer id = species.getReferenceTaxonId();

                    SpeciesProtocol sp = speciesIds.get(id);
                    if (sp != null) {
                        result.add(species);

                    } else {

                        sp = ids.get(id);
                        if (sp == null) {

                            // create a new species protocol
                            sp = SpeciesProtocols.newSpeciesProtocol();
                        }
                        binder.copy(bean, sp);

                        cleanRptData(sp);

                        ids.put(id, sp);
                    }
                }

                List<SpeciesProtocol> values = new ArrayList<>(ids.values());
                protocol.setBenthos(values);

            }

        } catch (ImportRuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.protocol.import.benthos.error", protocol.getName(), file), e);
        }

        return result;
    }

    public void exportProtocolSpecies(File file,
                                      List<SpeciesProtocol> protocol,
                                      Map<String, Caracteristic> caracteristicMap,
                                      Map<String, Species> speciesMap) {
        if (log.isInfoEnabled()) {
            log.info("Will export species to file: " + file);
        }

        List<SpeciesRow> rows;

        if (CollectionUtils.isEmpty(protocol)) {

            rows = null;

        } else {

            rows = protocol.stream().map(new SpeciesProtocolToSpeciesRowFunction(caracteristicMap, speciesMap)).collect(Collectors.toList());

        }

        try (BufferedWriter writer = Files.newWriter(file, StandardCharsets.UTF_8)) {

            SpeciesRowModel csvModel = SpeciesRowModel.forExport(getCsvSeparator());
            Export export = Export.newExport(csvModel, rows);
            export.write(writer);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.protocol.export.species.error", file), e);
        }
    }

    public void exportProtocolBenthos(File file,
                                      List<SpeciesProtocol> protocol,
                                      Map<String, Caracteristic> caracteristicMap,
                                      Map<String, Species> speciesMap) {
        if (log.isInfoEnabled()) {
            log.info("Will export benthos to file: " + file);
        }

        List<SpeciesRow> rows;

        if (CollectionUtils.isEmpty(protocol)) {

            rows = null;

        } else {

            rows = protocol.stream().map(new SpeciesProtocolToSpeciesRowFunction(caracteristicMap, speciesMap)).collect(Collectors.toList());

        }

        try (BufferedWriter writer = Files.newWriter(file, StandardCharsets.UTF_8)) {

            SpeciesRowModel csvModel = SpeciesRowModel.forExport(getCsvSeparator());
            Export export = Export.newExport(csvModel, rows);
            export.write(writer);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.protocol.export.benthos.error", file), e);
        }
    }

    /**
     * @param file       file to import
     * @param protocol   existing protocol
     * @param allSpecies dictionnary of species
     * @return The list of the species is not imported because they are already in the species
     * or not in the protocol species
     */
    public Set<Species> importCalcifiedPiecesSamplings(File file,
                                                       TuttiProtocol protocol,
                                                       Map<String, Species> allSpecies) {

        if (log.isInfoEnabled()) {
            log.info("Will import protocol [" + protocol.getName() + "] cps from file: " + file);
        }

        Set<Species> result = new HashSet<>();

        try (Reader reader = Files.newReader(file, StandardCharsets.UTF_8)) {

            CalcifiedPiecesSamplingRowModel csvModel = CalcifiedPiecesSamplingRowModel.forImport(getCsvSeparator(), allSpecies);
            try (Import<CalcifiedPiecesSamplingRow> importer = Import.newImport(csvModel, reader)) {

                Binder<CalcifiedPiecesSamplingRow, CalcifiedPiecesSamplingDefinition> binder =
                        BinderFactory.newBinder(CalcifiedPiecesSamplingRow.class, CalcifiedPiecesSamplingDefinition.class);

                Map<Integer, SpeciesProtocol> availableSpeciesProtocolMap = availableSpeciesProtocolMap(protocol);

                // contient les définitions sans maturité définie groupées par leur espèce du protocole
                ArrayListMultimap<SpeciesProtocol, CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinitionsWithoutMaturity = ArrayListMultimap.create();
                // contient les définitions avec une maturité définie à vrai groupées par leur espèce du protocole
                ArrayListMultimap<SpeciesProtocol, CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinitionsWithTrueMaturity = ArrayListMultimap.create();
                // contient les définitions avec une maturité définie à faux groupées par leur espèce du protocole
                ArrayListMultimap<SpeciesProtocol, CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinitionsWithFalseMaturity = ArrayListMultimap.create();

                // les espèces du protocole liés à des définitions avec ET sans maturité (ce qui est interdit)
                Set<SpeciesProtocol> speciesProtocolsWithAndWithoutMaturity = new LinkedHashSet<>();

                for (CalcifiedPiecesSamplingRow bean : importer) {

                    Species species = bean.getSpecies();

                    if (result.contains(species)) {

                        // already done
                        continue;
                    }

                    SpeciesProtocol speciesProtocol = availableSpeciesProtocolMap.get(species.getReferenceTaxonId());

                    if (speciesProtocol == null) {

                        if (log.isDebugEnabled()) {
                            log.debug("Skip species: " + species + ", not found in protocol.");
                        }
                        result.add(species);
                        continue;

                    }

                    if (speciesProtocol.getLengthStepPmfmId() == null) {

                        if (log.isDebugEnabled()) {
                            log.debug("Skip species: " + species + ", found in protocol, but with no length class.");
                        }
                        result.add(species);
                        continue;

                    }

                    Multimap<SpeciesProtocol, CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinitions;

                    CalcifiedPiecesSamplingDefinition definition = CalcifiedPiecesSamplingDefinitions.newCalcifiedPiecesSamplingDefinition(bean, binder);

                    Boolean maturity = definition.getMaturity();

                    if (maturity == null) {

                        if (log.isDebugEnabled()) {
                            log.debug("On species: " + species + ", add definition with no maturity defined (" + definition + ")");
                        }

                        calcifiedPiecesSamplingDefinitions = calcifiedPiecesSamplingDefinitionsWithoutMaturity;

                        if (calcifiedPiecesSamplingDefinitionsWithTrueMaturity.containsKey(speciesProtocol) ||
                                calcifiedPiecesSamplingDefinitionsWithFalseMaturity.containsKey(speciesProtocol)) {

                            // espèce déjà référencé comme avec maturité renseigné, on marque l'espèce
                            speciesProtocolsWithAndWithoutMaturity.add(speciesProtocol);
                        }

                    } else {

                        if (calcifiedPiecesSamplingDefinitionsWithoutMaturity.containsKey(speciesProtocol)) {

                            // espèce déjà référencé comme sans maturité renseignée, on marque l'espèce
                            speciesProtocolsWithAndWithoutMaturity.add(speciesProtocol);
                        }

                        if (maturity) {

                            if (log.isDebugEnabled()) {
                                log.debug("On species: " + species + ", add definition with maturity defined to True (" + definition + ")");
                            }

                            calcifiedPiecesSamplingDefinitions = calcifiedPiecesSamplingDefinitionsWithTrueMaturity;
                        } else {

                            if (log.isDebugEnabled()) {
                                log.debug("On species: " + species + ", add definition with maturity defined to False (" + definition + ")");
                            }
                            calcifiedPiecesSamplingDefinitions = calcifiedPiecesSamplingDefinitionsWithFalseMaturity;
                        }
                    }

                    calcifiedPiecesSamplingDefinitions.put(speciesProtocol, definition);

                }

                if (!speciesProtocolsWithAndWithoutMaturity.isEmpty()) {

                    // on a trouvé des espèces invalide vis-vis de la maturité
                    // i.e des espèces ayant des définitions avec et sans maturité
                    String badSpecies = speciesProtocolsWithAndWithoutMaturity.stream()
                                                                              .map(speciesProtocol -> {
                                                                                  String speciesToString = speciesProtocol.getSpeciesReferenceTaxonId() + "";
                                                                                  if (speciesProtocol.getSpeciesSurveyCode() != null) {
                                                                                      speciesToString += " (" + speciesProtocol.getSpeciesSurveyCode() + " )";
                                                                                  }
                                                                                  return speciesToString;
                                                                              })
                                                                              .collect(Collectors.joining("", "<li>", "</li>"));
                    throw new ImportRuntimeException(t("tutti.service.protocol.import.cps.speciesWithMaturityAndWithoutMaturity.error", badSpecies));

                }

                Comparator<CalcifiedPiecesSamplingDefinition> definitionsComparator = Comparator.comparing(CalcifiedPiecesSamplingDefinition::getMinSize);

                checkCpfDefinitionsValidity(definitionsComparator, calcifiedPiecesSamplingDefinitionsWithoutMaturity);
                checkCpfDefinitionsValidity(definitionsComparator, calcifiedPiecesSamplingDefinitionsWithTrueMaturity);
                checkCpfDefinitionsValidity(definitionsComparator, calcifiedPiecesSamplingDefinitionsWithFalseMaturity);

            }

        } catch (ImportRuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.protocol.import.cps.error", protocol.getName(), file), e);
        }

        return result;
    }

    public void exportCalcifiedPiecesSamplings(File file,
                                               Multimap<SpeciesProtocol, CalcifiedPiecesSamplingDefinition> cps,
                                               Map<String, Species> speciesByReferenceTaxonId) {

        if (log.isInfoEnabled()) {
            log.info("Will export cps to file: " + file);
        }

        List<CalcifiedPiecesSamplingRow> rows = new ArrayList<>();

        Binder<CalcifiedPiecesSamplingDefinition, CalcifiedPiecesSamplingRow> binder =
                BinderFactory.newBinder(CalcifiedPiecesSamplingDefinition.class, CalcifiedPiecesSamplingRow.class);

        cps.keySet().forEach(speciesProtocol -> {

            Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinitions = cps.get(speciesProtocol);

            calcifiedPiecesSamplingDefinitions.forEach(cpsDef -> {

                CalcifiedPiecesSamplingRow row = new CalcifiedPiecesSamplingRow();
                binder.copy(cpsDef, row);
                String refTaxId = String.valueOf(speciesProtocol.getSpeciesReferenceTaxonId());
                Species species = speciesByReferenceTaxonId.get(refTaxId);
                row.setSpecies(species);
                rows.add(row);

            });
        });

        try (BufferedWriter writer = Files.newWriter(file, StandardCharsets.UTF_8)) {

            CalcifiedPiecesSamplingRowModel csvModel = CalcifiedPiecesSamplingRowModel.forExport(getCsvSeparator());
            Export export = Export.newExport(csvModel, rows);
            export.write(writer);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.protocol.export.cps.error", file), e);
        }
    }

    protected char getCsvSeparator() {
        return context.getConfig().getCsvSeparator();
    }

    protected void cleanRptData(SpeciesProtocol sp) {
        if (sp.withRtpMale()) {
            Rtp rtpMale = sp.getRtpMale();
            if (rtpMale.getA() == null && rtpMale.getB() == null) {
                sp.setRtpMale(null);
            }
        }
        if (sp.withRtpFemale()) {
            Rtp rtpFemale = sp.getRtpFemale();
            if (rtpFemale.getA() == null && rtpFemale.getB() == null) {
                sp.setRtpFemale(null);
            }
        }
        if (sp.withRtpUndefined()) {
            Rtp rtpUndefined = sp.getRtpUndefined();
            if (rtpUndefined.getA() == null && rtpUndefined.getB() == null) {
                sp.setRtpUndefined(null);
            }
        }
    }

    protected void checkCpfDefinitionsValidity(Comparator<CalcifiedPiecesSamplingDefinition> definitionsComparator, ArrayListMultimap<SpeciesProtocol, CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinitions) {

        calcifiedPiecesSamplingDefinitions.asMap().forEach((speciesProtocol, unsortedDefinitions) -> {

            List<CalcifiedPiecesSamplingDefinition> sortedDefinitions = new ArrayList<>(unsortedDefinitions);
            sortedDefinitions.sort(definitionsComparator);

            Integer min = -1;
            for (CalcifiedPiecesSamplingDefinition definition : sortedDefinitions) {
                if (definition.getMinSize() != min + 1) {

                    throw new ImportRuntimeException(t("tutti.service.protocol.import.cps.interval.error",
                                                       speciesProtocol.getSpeciesReferenceTaxonId(),
                                                       definition.getMaturity(),
                                                       min,
                                                       definition.getMinSize()));
                }
                min = definition.getMaxSize();
            }

            speciesProtocol.setCalcifiedPiecesSamplingDefinition(sortedDefinitions);

        });

    }

    private Map<Integer, SpeciesProtocol> availableSpeciesProtocolMap(TuttiProtocol protocol) {

        Collection<SpeciesProtocol> availableSpeciesProtocol = new ArrayList<>();
        if (!protocol.isSpeciesEmpty()) {
            availableSpeciesProtocol.addAll(protocol.getSpecies());
        }
        if (!protocol.isBenthosEmpty()) {
            availableSpeciesProtocol.addAll(protocol.getBenthos());
        }

        return availableSpeciesProtocol.stream()
                                       .filter(SpeciesProtocol::isCalcifiedPiecesSamplingDefinitionEmpty)
                                       .collect(Collectors.toMap(SpeciesProtocol::getSpeciesReferenceTaxonId, Function.identity()));


    }

    private static class SpeciesProtocolToSpeciesRowFunction implements Function<SpeciesProtocol, SpeciesRow> {

        private final Map<String, Species> speciesMap;

        private final Map<String, Caracteristic> caracteristicMap;

        private final Binder<SpeciesProtocol, SpeciesRow> binder;

        public SpeciesProtocolToSpeciesRowFunction(Map<String, Caracteristic> caracteristicMap,
                                                   Map<String, Species> speciesMap) {
            this.speciesMap = speciesMap;
            this.caracteristicMap = caracteristicMap;
            this.binder = BinderFactory.newBinder(SpeciesProtocol.class, SpeciesRow.class);
        }

        @Override
        public SpeciesRow apply(SpeciesProtocol input) {
            Species species = speciesMap.get(String.valueOf(input.getSpeciesReferenceTaxonId()));
            Preconditions.checkNotNull(species, "Could not find a species with id: " + input);
            SpeciesRow result = new SpeciesRow();
            binder.copy(input, result);
            String pmfmId = input.getLengthStepPmfmId();
            if (pmfmId != null) {
                Caracteristic caracteristic = caracteristicMap.get(pmfmId);
                result.setLengthStepPmfm(caracteristic);
            }
            result.setSpecies(species);
            // always use a copy of list
            result.setMandatorySampleCategoryId(new ArrayList<>(input.getMandatorySampleCategoryId()));
            return result;
        }
    }
}
