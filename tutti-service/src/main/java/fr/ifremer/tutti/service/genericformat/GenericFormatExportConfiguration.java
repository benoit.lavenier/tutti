package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.model.ProgramDataModel;

import java.io.File;
import java.io.Serializable;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatExportConfiguration implements Serializable {

    private static final long serialVersionUID = -6500054741804757627L;

    /**
     * Data to export.
     */
    private ProgramDataModel dataToExport;

    /**
     * Archive file to produce.
     */
    private File exportFile;

    /**
     * Should we export attachments ?
     */
    private boolean exportAttachments;

    /**
     * Should we export species batches ?
     */
    private boolean exportSpecies;

    /**
     * Should we export benthos batches ?
     */
    private boolean exportBenthos;

    /**
     * Should we export marine litter batches ?
     */
    private boolean exportMarineLitter;

    /**
     * Should we export accidental catches ?
     */
    private boolean exportAccidentalCatch;

    /**
     * Should we export individual observations ?
     */
    private boolean exportIndividualObservation;

    public ProgramDataModel getDataToExport() {
        return dataToExport;
    }

    public void setDataToExport(ProgramDataModel dataToExport) {
        this.dataToExport = dataToExport;
    }

    public boolean isExportAttachments() {
        return exportAttachments;
    }

    public void setExportAttachments(boolean exportAttachments) {
        this.exportAttachments = exportAttachments;
    }

    public File getExportFile() {
        return exportFile;
    }

    public void setExportFile(File exportFile) {
        this.exportFile = exportFile;
    }

    public void setExportSpecies(boolean exportSpecies) {
        this.exportSpecies = exportSpecies;
    }

    public boolean isExportSpecies() {
        return exportSpecies;
    }

    public void setExportBenthos(boolean exportBenthos) {
        this.exportBenthos = exportBenthos;
    }

    public boolean isExportBenthos() {
        return exportBenthos;
    }

    public void setExportMarineLitter(boolean exportMarineLitter) {
        this.exportMarineLitter = exportMarineLitter;
    }

    public boolean isExportMarineLitter() {
        return exportMarineLitter;
    }

    public void setExportAccidentalCatch(boolean exportAccidentalCatch) {
        this.exportAccidentalCatch = exportAccidentalCatch;
    }

    public boolean isExportAccidentalCatch() {
        return exportAccidentalCatch;
    }

    public void setExportIndividualObservation(boolean exportIndividualObservation) {
        this.exportIndividualObservation = exportIndividualObservation;
    }

    public boolean isExportIndividualObservation() {
        return exportIndividualObservation;
    }

}
