package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/19/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CaracteristicValueParseException extends Exception {

    private static final long serialVersionUID = 1L;

    private final Caracteristic caracteristic;

    private final String value;

    private final String errorMessage;

    public CaracteristicValueParseException(Caracteristic caracteristic, String value, String errorMessage) {
        this.caracteristic = caracteristic;
        this.value = value;
        this.errorMessage = errorMessage;
    }

    public Caracteristic getCaracteristic() {
        return caracteristic;
    }

    public String getValue() {
        return value;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String getMessage() {
        return t("tutti.service.csv.parse.qualitativeValueError", value, caracteristic.getId(), errorMessage);
    }

}
