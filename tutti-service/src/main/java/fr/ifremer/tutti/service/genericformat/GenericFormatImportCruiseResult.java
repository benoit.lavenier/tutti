package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 2/22/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportCruiseResult implements Serializable, Iterable<GenericFormatImportOperationResult> {

    private static final long serialVersionUID = 1L;

    private final Map<String, GenericFormatImportOperationResult> fishingOperationResults;

    private final Cruise cruise;

    private final boolean override;

    private final String label;

    private int nbOperationsCreated;

    private int nbOperationsUpdated;

    private boolean withInvalidWeights;

    public GenericFormatImportCruiseResult(GenericFormatImportCruiseContext cruiseContext) {
        label = cruiseContext.getCruiseLabel();
        this.cruise = cruiseContext.getCruise();
        this.override = cruiseContext.getExistingCruiseData() != null;
        this.fishingOperationResults = new LinkedHashMap<>();

        this.withInvalidWeights = false;

        for (GenericFormatImportOperationContext operationContext : cruiseContext.orderedFishingOperationContexts()) {

            GenericFormatImportOperationResult operationResult = new GenericFormatImportOperationResult(operationContext);
            fishingOperationResults.put(operationContext.getFishingOperation().getId(), operationResult);

            if (operationContext.isOverride()) {
                nbOperationsUpdated++;
            } else {
                nbOperationsCreated++;
            }
            if (operationResult.isWithInvalidWeights()) {
                withInvalidWeights = true;
            }

        }

    }

    public String getLabel() {
        return label;
    }

    public String getId() {
        return cruise.getId();
    }

    public int getNbOperationsCreated() {
        return nbOperationsCreated;
    }

    public int getNbOperationsUpdated() {
        return nbOperationsUpdated;
    }

    public int getNbOperations() {
        return nbOperationsCreated + nbOperationsUpdated;
    }

    public boolean isWithInvalidWeights() {
        return withInvalidWeights;
    }

    public Cruise getCruise() {
        return cruise;
    }

    public boolean isOverride() {
        return override;
    }

    public String getVesselName() {
        Vessel vessel = cruise.getVessel();
        String result = vessel.getInternationalRegistrationCode();
        if (result == null) {
            result = vessel.getRegistrationCode();
        }
        return result;
    }

    @Override
    public Iterator<GenericFormatImportOperationResult> iterator() {
        return fishingOperationResults.values().iterator();
    }

}
