package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;

import java.util.Objects;

/**
 * Définit l'état en terme de prélèvement (de pièces calcifiées) d'une observation individuelle.
 *
 * L'utilisation de cet objet sous-entend que l'observation individuelle liée rentre bien dans l'algorithme.
 *
 * Created on 18/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationSamplingStatus {

    /**
     * Le context d'une observation individuelle (i.e ses caractéristiques).
     */
    private final IndividualObservationSamplingContext individualObservationSamplingContext;

    /**
     * Le nombre d'observations individuelles similaires effectués sur la campagne.
     */
    private final int individualObservationCountInCruise;

    /**
     * Le nombre de prélèvements similaires effectués sur la campagne.
     */
    private final int samplingCountInCruise;

    /**
     * Le nombre d'observations individuelles similaires effectués sur l'opération de pêche.
     */
    private final int individualObservationCountInFishingOperation;

    /**
     * Le nombre de prélèvements similaires effectués sur l'opération de pêche.
     */
    private final int samplingCountInFishingOperation;

    /**
     * Le nombre d'observations individuelles similaires effectués sur la zone.
     */
    private final int individualObservationCountInZone;

    /**
     * Le nombre de prélèvements similaires effectués sur la zone.
     */
    private final int samplingCountInZone;

    /**
     * Pour savoir si on demande un prélèvement.
     */
    private final boolean needSampling;

    public IndividualObservationSamplingStatus(IndividualObservationSamplingContext individualObservationSamplingContext,
                                               boolean computeSampling,
                                               CruiseSamplingInternalCache.SamplingData cruiseSamplingData,
                                               CruiseSamplingInternalCache.SamplingData zoneSamplingData,
                                               CruiseSamplingInternalCache.SamplingData fishingOperationSamplingData) {
        Objects.requireNonNull(individualObservationSamplingContext);
        Objects.requireNonNull(cruiseSamplingData);
        Objects.requireNonNull(zoneSamplingData);
        Objects.requireNonNull(fishingOperationSamplingData);
        this.individualObservationSamplingContext = individualObservationSamplingContext;
        this.samplingCountInCruise = cruiseSamplingData.getSamplingCount();
        this.individualObservationCountInCruise = cruiseSamplingData.getIndividualObservationCount();
        this.samplingCountInZone = zoneSamplingData.getSamplingCount();
        this.individualObservationCountInZone = zoneSamplingData.getIndividualObservationCount();
        this.samplingCountInFishingOperation = fishingOperationSamplingData.getSamplingCount();
        this.individualObservationCountInFishingOperation = fishingOperationSamplingData.getIndividualObservationCount();

        boolean needSampling;

        // on calcule needSampling si on nous le demande, que la définnition nous autorise (au moins un max n'est pas à 0) et qu'aucun max n'a été atteint
        if (computeSampling && !isNotUsingSampling() && !isOneTotalCountIsAttained()) {

            int samplingInterval = individualObservationSamplingContext.getCalcifiedPiecesSamplingDefinition().getSamplingInterval();
            needSampling = individualObservationCountInCruise== 1 || samplingInterval == 1 || individualObservationCountInCruise % samplingInterval == 1;

        } else {

            needSampling = false;

        }
        this.needSampling = needSampling;

    }

    public IndividualObservationSamplingContext getIndividualObservationSamplingContext() {
        return individualObservationSamplingContext;
    }

    public boolean isNeedSampling() {
        return needSampling;
    }

    public CalcifiedPiecesSamplingDefinition getCalcifiedPiecesSamplingDefinition() {
        return individualObservationSamplingContext.getCalcifiedPiecesSamplingDefinition();
    }

    public int getSamplingCountInCruise() {
        return samplingCountInCruise;
    }

    public int getSamplingCountInFishingOperation() {
        return samplingCountInFishingOperation;
    }

    public int getSamplingCountInZone() {
        return samplingCountInZone;
    }

    public int getIndividualObservationCountInCruise() {
        return individualObservationCountInCruise;
    }

    public int getIndividualObservationCountInFishingOperation() {
        return individualObservationCountInFishingOperation;
    }

    public int getIndividualObservationCountInZone() {
        return individualObservationCountInZone;
    }

    public Integer getTotalSamplingRequiredInCruise() {
        return getCalcifiedPiecesSamplingDefinition().getMaxByLenghtStep();
    }

    public Integer getTotalSamplingRequiredInFishingOperation() {
        return getCalcifiedPiecesSamplingDefinition().getOperationLimitation();
    }

    public Integer getTotalSamplingRequiredInZone() {
        return getCalcifiedPiecesSamplingDefinition().getZoneLimitation();
    }

    public boolean isTotalInCruiseAttained() {
        Integer totalSamplingRequiredInCruise = getTotalSamplingRequiredInCruise();
        return totalSamplingRequiredInCruise != null && totalSamplingRequiredInCruise > 0 && getSamplingCountInCruise() >= totalSamplingRequiredInCruise;
    }

    public boolean isTotalInFishingOperationAttained() {
        Integer totalSamplingRequiredInFishingOperation = getTotalSamplingRequiredInFishingOperation();
        return totalSamplingRequiredInFishingOperation != null && (totalSamplingRequiredInFishingOperation > 0 && getSamplingCountInFishingOperation() >= totalSamplingRequiredInFishingOperation);
    }

    public boolean isTotalInZoneAttained() {
        Integer totalSamplingRequiredInZone = getTotalSamplingRequiredInZone();
        return totalSamplingRequiredInZone != null && (totalSamplingRequiredInZone > 0 && getSamplingCountInZone() >= totalSamplingRequiredInZone);
    }

    public boolean isOneTotalCountIsAttained() {
        return isTotalInCruiseAttained() || isTotalInZoneAttained() || isTotalInFishingOperationAttained();
    }

    public boolean isTotalInCruiseNotUsed() {
        Integer totalSamplingRequiredInCruise = getTotalSamplingRequiredInCruise();
        return totalSamplingRequiredInCruise != null && totalSamplingRequiredInCruise == 0;
    }

    public boolean isTotalInFishingOperationNotUsed() {
        Integer totalSamplingRequiredInFishingOperation = getTotalSamplingRequiredInFishingOperation();
        return totalSamplingRequiredInFishingOperation != null && totalSamplingRequiredInFishingOperation == 0;
    }

    public boolean isTotalInZoneNotUsed() {
        Integer totalSamplingRequiredInZone = getTotalSamplingRequiredInZone();
        return totalSamplingRequiredInZone != null && totalSamplingRequiredInZone == 0;
    }

    public boolean isNotUsingSampling() {
        return isTotalInCruiseNotUsed() && isTotalInFishingOperationNotUsed() && isTotalInZoneNotUsed();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("individualObservationSamplingContext", individualObservationSamplingContext)
                          .add("individualObservationCountInCruise", getIndividualObservationCountInCruise())
                          .add("samplingCountInCruise", samplingCountInCruise)
                          .add("individualObservationCountInFishingOperation", getIndividualObservationCountInFishingOperation())
                          .add("samplingCountInFishingOperation", samplingCountInFishingOperation)
                          .add("individualObservationCountInZone", getIndividualObservationCountInZone())
                          .add("samplingCountInZone", samplingCountInZone)
                          .add("needSampling", needSampling)
                          .toString();
    }
}
