package fr.ifremer.tutti.service.cruise;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;

import java.io.Closeable;
import java.util.Collection;

/**
 * Created on 17/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface CruiseCacheAble extends Closeable {

    /**
     * Ajout de toutes les observations individuelles de l'opération de pêche dans le cache.
     *
     * @param fishingOperation l'opération de pêche concernée
     */
    void addFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations);

    /**
     * Suppression de toutes les observations individuelles du cache.
     *
     * @param fishingOperation l'opération de pêche concernée
     */
    void removeFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations);

    /**
     * Ajout des plusieurs échantillons qui sont sur la m$eme opération de pêche dans le cache.
     *
     * @param fishingOperation       l'opération de pêche concernée
     * @param individualObservations les observations à ajouter au cache
     */
    void addIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations);

    /**
     * Suppression de plusieurs échantillons qui sont sur la même opération de pêche du cache.
     *
     * @param fishingOperation       l'opération de pêche concernée
     * @param individualObservations les observations individuelles à supprimer du cache
     */
    void removeIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations);

}
