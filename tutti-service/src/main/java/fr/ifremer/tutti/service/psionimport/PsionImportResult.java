package fr.ifremer.tutti.service.psionimport;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;

import java.io.File;
import java.util.List;

/**
 * Created on 1/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class PsionImportResult {

    protected final File importFile;

    protected final List<String> errors;

    protected int nbSortedImported;

    protected int nbUnsortedImported;

    public PsionImportResult(File importFile, List<String> errors) {
        this.importFile = importFile;
        this.errors = Lists.newArrayList(errors);
    }

    public File getImportFile() {
        return importFile;
    }

    public int getNbSortedImported() {
        return nbSortedImported;
    }

    public int getNbUnsortedImported() {
        return nbUnsortedImported;
    }

    public List<String> getErrors() {
        return errors;
    }

    void incrementNbSortedImported() {
        this.nbSortedImported++;
    }

    void incrementNbUnsortedImported() {
        this.nbUnsortedImported++;
    }

    void addError(String error) {
        errors.add(error);
    }

    public boolean isDone() {
        return errors.isEmpty();
    }
}
