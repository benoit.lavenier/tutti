package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.genericformat.GenericFormatCheckDataService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportCruiseContext;

import java.util.Set;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class CheckDataAction extends ExportTechnicalActionSupport {

    private final GenericFormatCheckDataService checkDataService;

    public CheckDataAction(GenericFormatCheckDataService checkDataService) {

        this.checkDataService = checkDataService;

    }

    @Override
    public void execute(GenericFormatExportContext exportContext) {

        ProgressionModel progressionModel = exportContext.getProgressionModel();

        for (GenericFormatExportCruiseContext cruiseContext : exportContext) {

            Cruise cruise = cruiseContext.getCruise();
            Set<FishingOperation> operations = cruiseContext.getOperations();

            String checkErrors = checkDataService.getCruiseErrors(cruise, operations, progressionModel);

            cruiseContext.setCheckErrors(checkErrors);

        }

    }
}
