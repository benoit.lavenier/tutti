package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.protocol.Zones;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.service.cruise.CruiseCacheAble;
import fr.ifremer.tutti.util.Numbers;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static fr.ifremer.tutti.service.sampling.CruiseSamplingInternalCache.addPrefixKey;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CruiseSamplingCache implements CruiseCacheAble {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CruiseSamplingCache.class);

    /**
     * Le cache des échantillons au niveau de la campagne.
     */
    private final CruiseSamplingInternalCache cruiseCache = new CruiseSamplingInternalCache();
    /**
     * Le cache des échantillons au niveau de chaque zone.
     */
    private final CruiseSamplingInternalCache zoneCache = new CruiseSamplingInternalCache();
    /**
     * Le cache des échantillons au niveau de chaque opération de pêche.
     */
    private final CruiseSamplingInternalCache fishingOperationCache = new CruiseSamplingInternalCache();
    /**
     * Le dictionnaire des identifiants de strates ou sous-strates pour chaque zone.
     */
    private final Multimap<Zone, Integer> locationIdsPerZone;
    /**
     * Le dictionnaire des définitions de l'algorithme de prélèvement des piècess calcifiées indexés par identifiant de taxon.
     */
    private final Multimap<Integer, CalcifiedPiecesSamplingDefinition> cpsDefinitionsBySpecies = HashMultimap.create();
    /**
     * La caractéristique de maturité pour chaque espèce
     */
    private final Map<Integer, Caracteristic> maturityCaracteristicBySpecies = new HashMap<>();
    /**
     * Les états matures pour chaque caractéristique de maturité
     */
    private final Multimap<String, String> matureStatesByMaturityCracteristic = HashMultimap.create();
    /**
     * La caractéristique qui définie le sexe dans une observation individuelle.
     */
    private final Caracteristic sexCaracteristic;
    /**
     * Les valeurs qualitatives de la caractéristique de Sexe indexées par leur identifiant.
     */
    private final Map<Integer, CaracteristicQualitativeValue> sexQualitativeValues;
    /**
     * Un drapeau pour indiquer qu'on est en train de construire le cache et donc ne déclancher aucun listener.
     */
    private boolean loading;

    public CruiseSamplingCache(TuttiProtocol protocol, Caracteristic sexCaracteristic, Collection<Caracteristic> maturityCaracteristics) {
        this.sexCaracteristic = sexCaracteristic;
        this.sexQualitativeValues = TuttiEntities.splitByIdAsInt(sexCaracteristic.getQualitativeValue());

        locationIdsPerZone = HashMultimap.create();
        protocol.getZone().forEach(zone -> locationIdsPerZone.putAll(zone, Zones.getAllLocationIds(zone)));

        Map<String, Caracteristic> maturityCaracteristicsById = TuttiEntities.splitById(maturityCaracteristics);

        protocol.getSpecies().forEach(speciesProtocol -> {
            cpsDefinitionsBySpecies.putAll(speciesProtocol.getSpeciesReferenceTaxonId(),
                                           speciesProtocol.getCalcifiedPiecesSamplingDefinition());
            maturityCaracteristicBySpecies.put(speciesProtocol.getSpeciesReferenceTaxonId(),
                                               maturityCaracteristicsById.get(speciesProtocol.getMaturityPmfmId()));
        });

        protocol.getMaturityCaracteristics().forEach(mc -> matureStatesByMaturityCracteristic.putAll(mc.getId(), mc.getMatureStateIds()));
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    @Override
    public void addFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        addIndividualObservations(fishingOperation, individualObservations);

    }

    @Override
    public void addIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(individualObservations);

        Optional<Zone> optionalZone = tryFindZone(fishingOperation);
        if (!optionalZone.isPresent()) {

            // pas de zone définie pour cette opération de pêche, on n'enregistre rien ici.
            return;

        }

        Zone zone = optionalZone.get();

        setLoading(true);

        try {

            for (IndividualObservationBatch individualObservationBatch : individualObservations) {

                Optional<CalcifiedPiecesSamplingDefinition> optionalCalcifiedPiecesSamplingDefinition =
                        tryToFindCalcifiedPiecesSamplingDefinition(individualObservationBatch);

                if (!optionalCalcifiedPiecesSamplingDefinition.isPresent()) {

                    // pas dans l'algorithme, one ne tient pas compte de cette observation
                    continue;
                }

                Species species = individualObservationBatch.getSpecies();
                Objects.requireNonNull(species);

                // l'observation a forcement une taille
                Float lengthStep = individualObservationBatch.getSize();
                Objects.requireNonNull(lengthStep);

                Boolean maturity = getMaturity(individualObservationBatch);

                // don't forget to compute lengthStep with Caracteristic https://forge.codelutin.com/issues/8337
                int lengthStepInMm = individualObservationBatch.getLengthStepCaracteristic().getLengthStepInMm(lengthStep);

                CaracteristicQualitativeValue gender = individualObservationBatch.getCaracteristics().getQualitativeValue(sexCaracteristic);

                IndividualObservationSamplingContext individualObservationSamplingContext =
                        createContext(fishingOperation.getIdAsInt(),
                                      species,
                                      zone,
                                      optionalCalcifiedPiecesSamplingDefinition.get(),
                                      lengthStepInMm,
                                      maturity,
                                      gender);

                addIndividualObservation(individualObservationSamplingContext, individualObservationBatch.getSamplingCode() != null);

            }

        } finally {

            setLoading(false);

        }

    }

    @Override
    public void removeIndividualObservations(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(individualObservations);

        Optional<Zone> optionalZone = tryFindZone(fishingOperation);
        if (!optionalZone.isPresent()) {

            // pas de zone définie pour cette opération de pêche, rien à supprimer alors.
            return;

        }

        Zone zone = optionalZone.get();

        setLoading(true);

        try {

            for (IndividualObservationBatch individualObservationBatch : individualObservations) {

                Optional<CalcifiedPiecesSamplingDefinition> optionalCalcifiedPiecesSamplingDefinition =
                        tryToFindCalcifiedPiecesSamplingDefinition(individualObservationBatch);

                if (!optionalCalcifiedPiecesSamplingDefinition.isPresent()) {

                    // pas dans l'algorithme, on ne tient pas compte de cette observation
                    continue;
                }

                Species species = individualObservationBatch.getSpecies();
                Objects.requireNonNull(species);

                Boolean maturity = getMaturity(individualObservationBatch);

                Objects.requireNonNull(individualObservationBatch.getSize());
                float lengthStep = individualObservationBatch.getSize();
                int lengthStepInMm = Numbers.convertToMm(lengthStep, individualObservationBatch.getLengthStepCaracteristic().getUnit());

                CaracteristicQualitativeValue gender = individualObservationBatch.getCaracteristics().getQualitativeValue(sexCaracteristic);

                IndividualObservationSamplingContext individualObservationSamplingContext =
                        createContext(fishingOperation.getIdAsInt(),
                                      species,
                                      zone,
                                      optionalCalcifiedPiecesSamplingDefinition.get(),
                                      lengthStepInMm,
                                      maturity,
                                      gender);

                removeIndividualObservation(individualObservationSamplingContext, individualObservationBatch.getSamplingCode() != null);

            }

        } finally {

            setLoading(false);

        }

    }

    @Override
    public void removeFishingOperation(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(individualObservations);

        Optional<Zone> optionalZone = tryFindZone(fishingOperation);
        if (!optionalZone.isPresent()) {

            // pas de zone définie pour cette opération de pêche, rien à supprimer alors.
            return;

        }

        Zone zone = optionalZone.get();


        String fishingOperationId = fishingOperation.getId();

        if (log.isInfoEnabled()) {
            log.info("Removing fishing operation: " + fishingOperation + " from " + this);
        }

        // suppression de toutes les entrées du cache des opérations (et récupération des clefs)

        String keyPrefix = CruiseSamplingInternalCache.addPrefixKey(fishingOperationId, "");
        int keyPrefixLength = keyPrefix.length();

        String zoneId = zone.getId();

        fishingOperationCache.getKeys()
                             .stream()
                             .filter(key -> key.startsWith(keyPrefix))
                             .forEach(fishingOperationSamplingKey -> {

                                 CruiseSamplingInternalCache.SamplingData samplingData = fishingOperationCache.getSamplingData(fishingOperationSamplingKey);

                                 if (log.isInfoEnabled()) {
                                     log.info("Found " + fishingOperationSamplingKey + " to remove from fishing operation cache (" + samplingData + ").");
                                 }

                                 int individualObservationCount = samplingData.getIndividualObservationCount();
                                 int samplingCount = samplingData.getSamplingCount();

                                 String cruiseSamplingKey = fishingOperationSamplingKey.substring(keyPrefixLength);
                                 cruiseCache.remove(cruiseSamplingKey, individualObservationCount, samplingCount);

                                 String zoneSamplingKey = addPrefixKey(zoneId, cruiseSamplingKey);
                                 zoneCache.remove(zoneSamplingKey, individualObservationCount, samplingCount);

                                 fishingOperationCache.remove(fishingOperationSamplingKey, individualObservationCount, samplingCount);

                             });

        printInfos("After removing " + fishingOperation);

        cleanEmptyEntries();

        printInfos("After cleanEmptyEntries");

        if (log.isInfoEnabled()) {
            log.info(fishingOperation + " removed from fishingOperationCache: " + fishingOperationCache.size());
            log.info(fishingOperation + " removed from cruiseCache: " + cruiseCache.size());
            log.info(fishingOperation + " removed from zoneCache: " + zoneCache.size());
            log.info(fishingOperation + " removed from " + this);
        }

    }

    @Override
    public void close() {

        if (log.isInfoEnabled()) {
            log.info("Closing cruise sampling cache.");
        }
        cruiseCache.close();
        zoneCache.close();
        fishingOperationCache.close();
        locationIdsPerZone.clear();
        cpsDefinitionsBySpecies.clear();
        maturityCaracteristicBySpecies.clear();
        matureStatesByMaturityCracteristic.clear();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("cruiseCache", cruiseCache.size())
                          .add("zoneCache", zoneCache.size())
                          .add("fishingOperationCache", fishingOperationCache.size())
                          .toString();
    }

    public IndividualObservationSamplingStatus getIndividualObservationSamplingStatus(IndividualObservationSamplingCacheRequest request) throws SizeNotDefinedOnIndividualObservationException, ZoneNotDefinedOnFishingOperationException, CalcifiedPiecesSamplingAlgorithmEntryNotFoundException {

        Objects.requireNonNull(request);

        Species species = request.getSpecies();
        Objects.requireNonNull(species);

        Integer lengthStep = request.getLengthClass();
        if (lengthStep == null) {

            throw new SizeNotDefinedOnIndividualObservationException(request);
        }

        FishingOperation fishingOperation = request.getFishingOperation();
        Objects.requireNonNull(fishingOperation);

        Optional<Zone> optionalZone = tryFindZone(fishingOperation);
        if (!optionalZone.isPresent()) {

            throw new ZoneNotDefinedOnFishingOperationException(request);
        }

        CaracteristicQualitativeValue gender = request.getGender();
        Boolean maturity = getMaturity(request);

        Optional<CalcifiedPiecesSamplingDefinition> optionalCalcifiedPiecesSamplingDefinition =
                tryToFindCalcifiedPiecesSamplingDefinition(species, lengthStep, maturity, gender);

        if (!optionalCalcifiedPiecesSamplingDefinition.isPresent()) {

            throw new CalcifiedPiecesSamplingAlgorithmEntryNotFoundException(request);
        }

        IndividualObservationSamplingContext context = createContext(fishingOperation.getIdAsInt(),
                                                                     species,
                                                                     optionalZone.get(),
                                                                     optionalCalcifiedPiecesSamplingDefinition.get(),
                                                                     lengthStep,
                                                                     maturity,
                                                                     gender);

        String cruiseSamplingKey = context.getCruiseSamplingKey();
        CruiseSamplingInternalCache.SamplingData cruiseSamplingData = cruiseCache.getSamplingData(cruiseSamplingKey);

        String zoneSamplingKey = context.getZoneSamplingKey();
        CruiseSamplingInternalCache.SamplingData zoneSamplingData = zoneCache.getSamplingData(zoneSamplingKey);
        if (zoneSamplingData == null) {
            // poussin 20160608 fallback code to prevent UI error
            // fixes with https://forge.codelutin.com/issues/8337
            log.error(String.format("Can't find zoneSamplingData, create one"
                    + " (zoneSamplingKey: '%s'; request: '%s'; zone: '%s')",
                    zoneSamplingKey, request, optionalZone.get()));
            zoneSamplingData = zoneCache.getOrCreateSamplingData(zoneSamplingKey);
        }

        String fishingOperationSamplingKey = context.getFishingOperationSamplingKey();
        CruiseSamplingInternalCache.SamplingData fishingOperationSamplingData = fishingOperationCache.getSamplingData(fishingOperationSamplingKey);
        if (fishingOperationSamplingData == null) {
            // poussin 20160608 fallback code to prevent UI error
            // fixes with https://forge.codelutin.com/issues/8337
            log.error(String.format("Can't find fishingOperationSamplingData, create one"
                    + " (fishingOperationSamplingData: '%s'; request: '%s'; zone: '%s)",
                    zoneSamplingKey, request, optionalZone.get()));
            fishingOperationSamplingData = fishingOperationCache.getOrCreateSamplingData(fishingOperationSamplingKey);
        }

        // on demande à calculer needSampling uniquement s'il n'y a pas de code de prélèvement sur l'observation
        boolean computeSampling = !request.withSamplingCode();

        return new IndividualObservationSamplingStatus(context, computeSampling, cruiseSamplingData, zoneSamplingData, fishingOperationSamplingData);

    }

    public void addIndividualObservation(IndividualObservationSamplingCacheRequest request) {

        IndividualObservationSamplingContext individualObservationSamplingContext = getIndividualObservationSamplingContext(request);

        if (individualObservationSamplingContext != null) {

            addIndividualObservation(individualObservationSamplingContext, request.withSamplingCode());

        }

    }

    public void removeIndividualObservation(IndividualObservationSamplingCacheRequest request) {

        IndividualObservationSamplingContext individualObservationSamplingContext = getIndividualObservationSamplingContext(request);

        if (individualObservationSamplingContext != null) {

            removeIndividualObservation(individualObservationSamplingContext, request.withSamplingCode());

        }

    }

    public void addSampling(IndividualObservationSamplingCacheRequest request) {

        IndividualObservationSamplingContext individualObservationSamplingContext = getIndividualObservationSamplingContext(request);

        if (individualObservationSamplingContext != null) {

            addSampling(individualObservationSamplingContext);

        }

    }

    public void removeSampling(IndividualObservationSamplingCacheRequest request) {

        IndividualObservationSamplingContext individualObservationSamplingContext = getIndividualObservationSamplingContext(request);

        if (individualObservationSamplingContext != null) {

            removeSampling(individualObservationSamplingContext);

        }

    }

    public boolean isZoneChanged(FishingOperation operation1, FishingOperation operation2) {

        Optional<Zone> optionalZone1 = tryFindZone(operation1);
        Optional<Zone> optionalZone2 = tryFindZone(operation2);
        return !Objects.equals(optionalZone1, optionalZone2);

    }

    public List<CacheExtractedKey> getSamplingNumbers(Map<String, Species> speciesById) {
        List<CacheExtractedKey> result = cruiseCache.getSamplingNumbers(speciesById, sexQualitativeValues);
        result.forEach(key -> {
            Optional<CalcifiedPiecesSamplingDefinition> cpsDef = tryToFindCalcifiedPiecesSamplingDefinition(key.getSpecies(), key.getLengthStep(), key.getMaturity(), key.getSex());
            if (cpsDef.isPresent()) {
                key.setMaxByLengthStep(cpsDef.get().getMaxByLenghtStep());
            }
        });
        return result;
    }

    public Optional<Zone> tryFindZone(FishingOperation operation) {
        Optional<Zone> result;
        if (operation.getSubStrata() != null) {
            result = tryFindZone(operation.getSubStrata());
        } else if (operation.getStrata() != null) {
            result = tryFindZone(operation.getStrata());
        } else {
            result = Optional.empty();
        }
        return result;
    }

    public boolean isSpeciesDefined(Species species) {
        return cpsDefinitionsBySpecies.containsKey(species.getReferenceTaxonId());
    }

    private Optional<Zone> tryFindZone(TuttiLocation location) {

        // FIXME poussin 20160608 si on est obligé de parcourir pour chacun la
        // collection, c'est que ce n'est pas le bon format de stockage.
        // Modifier pour que la recherche soit faite sans parcours
        // il faudrait plutot Map[locationId -> Zone]
        // ce code est appeler de très nombreuse fois
        Integer locationId = location.getIdAsInt();
        return locationIdsPerZone.keySet()
                                 .stream()
                                 .filter(zone -> locationIdsPerZone.containsEntry(zone, locationId))
                                 .findFirst();

    }

    private Boolean getMaturity(IndividualObservationSamplingCacheRequest samplingCacheRequest) {
        Boolean maturity = null;
        if (samplingCacheRequest.withMaturity()) {
            Caracteristic maturityCaracteristic = maturityCaracteristicBySpecies.get(samplingCacheRequest.getSpecies().getReferenceTaxonId());
            if (maturityCaracteristic != null) {
                CaracteristicQualitativeValue maturityQualitativeValue = samplingCacheRequest.getMaturity();
                maturity = matureStatesByMaturityCracteristic.containsEntry(maturityCaracteristic.getId(), maturityQualitativeValue.getId());
            }
        }

        return maturity;
    }

    private Boolean getMaturity(IndividualObservationBatch individualObservationBatch) {
        Boolean maturity = null;
        Caracteristic maturityCaracteristic = maturityCaracteristicBySpecies.get(individualObservationBatch.getSpecies().getReferenceTaxonId());
        // if a maturity caracteristic is defined in the protocol for this species
        if (maturityCaracteristic != null) {
            CaracteristicQualitativeValue qualitativeValue = individualObservationBatch.getCaracteristics().getQualitativeValue(maturityCaracteristic);
            // it the maturity is set
            if (qualitativeValue != null) {
                maturity = matureStatesByMaturityCracteristic.containsEntry(maturityCaracteristic.getId(), qualitativeValue.getId());
            }
        }
        return maturity;
    }

    private String createCruiseSamplingKey(Species species,
                                           CaracteristicQualitativeValue gender,
                                           Boolean maturity,
                                           int lengthStep) {
        return CruiseSamplingInternalCache.createSamplingKey(species, gender, maturity, lengthStep);
    }

    private String createZoneSamplingKey(String cruiseSamplingKey, Zone zone) {
        return addPrefixKey(zone.getId(), cruiseSamplingKey);
    }

    private String createFishingOperationSamplingKey(String cruiseSamplingKey, int fishingOperationId) {
        return addPrefixKey(fishingOperationId, cruiseSamplingKey);
    }

    private void addIndividualObservation(IndividualObservationSamplingContext individualObservationSamplingContext, boolean addSampling) {

        Objects.requireNonNull(individualObservationSamplingContext);

        String cruiseSamplingKey = individualObservationSamplingContext.getCruiseSamplingKey();
        CruiseSamplingInternalCache.SamplingData cruiseSamplingData = cruiseCache.addOneIndividualObservation(cruiseSamplingKey);

        String zoneSamplingKey = individualObservationSamplingContext.getZoneSamplingKey();
        CruiseSamplingInternalCache.SamplingData zoneSamplingData = zoneCache.addOneIndividualObservation(zoneSamplingKey);

        String fishingOperationSamplingKey = individualObservationSamplingContext.getFishingOperationSamplingKey();
        CruiseSamplingInternalCache.SamplingData fishingOperationSamplingData = fishingOperationCache.addOneIndividualObservation(fishingOperationSamplingKey);

        if (log.isInfoEnabled()) {
            log.info(getLogMessage("add individual observation ",
                                   cruiseSamplingKey,
                                   zoneSamplingKey,
                                   fishingOperationSamplingKey,
                                   cruiseSamplingData,
                                   zoneSamplingData,
                                   fishingOperationSamplingData));
        }

        if (addSampling) {

            addSampling(individualObservationSamplingContext);

        }

    }

    private void removeIndividualObservation(IndividualObservationSamplingContext individualObservationSamplingContext, boolean removeSampling) {

        Objects.requireNonNull(individualObservationSamplingContext);

        String cruiseSamplingKey = individualObservationSamplingContext.getCruiseSamplingKey();
        CruiseSamplingInternalCache.SamplingData cruiseSamplingData = cruiseCache.removeOneIndividualObservation(cruiseSamplingKey);

        String zoneSamplingKey = individualObservationSamplingContext.getZoneSamplingKey();
        CruiseSamplingInternalCache.SamplingData zoneSamplingData = zoneCache.removeOneIndividualObservation(zoneSamplingKey);

        String fishingOperationSamplingKey = individualObservationSamplingContext.getFishingOperationSamplingKey();
        CruiseSamplingInternalCache.SamplingData fishingOperationSamplingData = fishingOperationCache.removeOneIndividualObservation(fishingOperationSamplingKey);

        if (log.isInfoEnabled()) {
            log.info(getLogMessage("remove individual observation ",
                                   cruiseSamplingKey,
                                   zoneSamplingKey,
                                   fishingOperationSamplingKey,
                                   cruiseSamplingData,
                                   zoneSamplingData,
                                   fishingOperationSamplingData));
        }

        if (removeSampling) {

            removeSampling(individualObservationSamplingContext);

        }

    }

    private void addSampling(IndividualObservationSamplingContext individualObservationSamplingContext) {

        Objects.requireNonNull(individualObservationSamplingContext);

        String cruiseSamplingKey = individualObservationSamplingContext.getCruiseSamplingKey();
        CruiseSamplingInternalCache.SamplingData cruiseSamplingData = cruiseCache.addOneSampling(cruiseSamplingKey);

        String zoneSamplingKey = individualObservationSamplingContext.getZoneSamplingKey();
        CruiseSamplingInternalCache.SamplingData zoneSamplingData = zoneCache.addOneSampling(zoneSamplingKey);

        String fishingOperationSamplingKey = individualObservationSamplingContext.getFishingOperationSamplingKey();
        CruiseSamplingInternalCache.SamplingData fishingOperationSamplingData = fishingOperationCache.addOneSampling(fishingOperationSamplingKey);

        if (log.isInfoEnabled()) {
            log.info(getLogMessage("add sampling ",
                                   cruiseSamplingKey,
                                   zoneSamplingKey,
                                   fishingOperationSamplingKey,
                                   cruiseSamplingData,
                                   zoneSamplingData,
                                   fishingOperationSamplingData));
        }

    }

    private void removeSampling(IndividualObservationSamplingContext individualObservationSamplingContext) {

        Objects.requireNonNull(individualObservationSamplingContext);

        String cruiseSamplingKey = individualObservationSamplingContext.getCruiseSamplingKey();
        CruiseSamplingInternalCache.SamplingData cruiseSamplingData = cruiseCache.removeOneSampling(cruiseSamplingKey);

        String zoneSamplingKey = individualObservationSamplingContext.getZoneSamplingKey();
        CruiseSamplingInternalCache.SamplingData zoneSamplingData = zoneCache.removeOneSampling(zoneSamplingKey);

        String fishingOperationSamplingKey = individualObservationSamplingContext.getFishingOperationSamplingKey();
        CruiseSamplingInternalCache.SamplingData fishingOperationSamplingData = fishingOperationCache.removeOneSampling(fishingOperationSamplingKey);

        if (log.isInfoEnabled()) {
            log.info(getLogMessage("remove sampling ",
                                   cruiseSamplingKey,
                                   zoneSamplingKey,
                                   fishingOperationSamplingKey,
                                   cruiseSamplingData,
                                   zoneSamplingData,
                                   fishingOperationSamplingData));
        }

    }

    private IndividualObservationSamplingContext getIndividualObservationSamplingContext(IndividualObservationSamplingCacheRequest request) {

        Objects.requireNonNull(request);

        FishingOperation fishingOperation = request.getFishingOperation();
        Objects.requireNonNull(fishingOperation);

        Optional<Zone> optionalZone = tryFindZone(fishingOperation);
        if (!optionalZone.isPresent()) {

            if (log.isInfoEnabled()) {
                log.info("Do not record sampling in cache, fishing operation has no matching zone.");
            }
            return null;
        }

        Integer lengthStep = request.getLengthClass();
        Objects.requireNonNull(lengthStep);

        Species species = request.getSpecies();
        Objects.requireNonNull(species);

        Boolean maturity = getMaturity(request);
        CaracteristicQualitativeValue gender = request.getGender();

        Optional<CalcifiedPiecesSamplingDefinition> optionalCalcifiedPiecesSamplingDefinition =
                tryToFindCalcifiedPiecesSamplingDefinition(species, lengthStep, maturity, gender);

        if (!optionalCalcifiedPiecesSamplingDefinition.isPresent()) {

            if (log.isInfoEnabled()) {
                log.info("Do not record sampling in cache, no definition matched.");
            }
            return null;
        }

        return createContext(fishingOperation.getIdAsInt(),
                             species,
                             optionalZone.get(),
                             optionalCalcifiedPiecesSamplingDefinition.get(),
                             lengthStep,
                             maturity,
                             gender);

    }

    private IndividualObservationSamplingContext createContext(int fishingOperationId,
                                                               Species species,
                                                               Zone zone,
                                                               CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition,
                                                               Integer lengthStep,
                                                               Boolean maturity,
                                                               CaracteristicQualitativeValue gender) {

        String cruiseSamplingKey = createCruiseSamplingKey(species, gender, maturity, lengthStep);
        String zoneSamplingKey = createZoneSamplingKey(cruiseSamplingKey, zone);
        String fishingOperationSamplingKey = createFishingOperationSamplingKey(cruiseSamplingKey, fishingOperationId);

        return new IndividualObservationSamplingContext(species,
                                                        lengthStep,
                                                        maturity,
                                                        gender,
                                                        calcifiedPiecesSamplingDefinition,
                                                        zone,
                                                        cruiseSamplingKey,
                                                        zoneSamplingKey,
                                                        fishingOperationSamplingKey);

    }

    private Optional<CalcifiedPiecesSamplingDefinition> tryToFindCalcifiedPiecesSamplingDefinition(IndividualObservationBatch individualObservationBatch) {

        Species species = individualObservationBatch.getSpecies();
        Objects.requireNonNull(species);

        Float lengthStep = individualObservationBatch.getSize();

        Optional<CalcifiedPiecesSamplingDefinition> result;
        if (lengthStep == null) {

            // on ne cherche pas sur une observation sans taille
            result = Optional.empty();

        } else {

            Boolean maturity = getMaturity(individualObservationBatch);

            int lengthStepInMm = Numbers.convertToMm(lengthStep, individualObservationBatch.getLengthStepCaracteristic().getUnit());

            CaracteristicQualitativeValue gender = individualObservationBatch.getCaracteristics().getQualitativeValue(sexCaracteristic);

            result = tryToFindCalcifiedPiecesSamplingDefinition(species, lengthStepInMm, maturity, gender);

        }

        return result;

    }

    private Optional<CalcifiedPiecesSamplingDefinition> tryToFindCalcifiedPiecesSamplingDefinition(Species species,
                                                                                                   int lengthStep,
                                                                                                   Boolean maturity,
                                                                                                   CaracteristicQualitativeValue gender) {

        Collection<CalcifiedPiecesSamplingDefinition> cpsDefinitions = cpsDefinitionsBySpecies.get(species.getReferenceTaxonId());

        CalcifiedPiecesSamplingDefinition result = null;

        if (cpsDefinitions == null) {

            if (log.isInfoEnabled()) {
                log.info(species + " not found in any calcified pieces sampling definitions");
            }

        } else {

            Optional<CalcifiedPiecesSamplingDefinition> optionalDefinition =
                    cpsDefinitions.stream()
                                  .filter(cpsDef -> Objects.equals(cpsDef.getMaturity(), maturity)
                                          && lengthStep >= cpsDef.getMinSize()
                                          && (cpsDef.getMaxSize() == null || lengthStep <= cpsDef.getMaxSize()))
                                  .findFirst();

            if (!optionalDefinition.isPresent()) {

                if (log.isInfoEnabled()) {
                    log.info(species + " - maturity " + maturity + " length step " + lengthStep + " not found in calcified pieces sampling definitions");
                }

            } else {

                result = optionalDefinition.get();

                if (result.getSamplingInterval() == 0) {

                    if (log.isInfoEnabled()) {
                        log.info("Can't use definition with no sampling interval: " + result);
                    }
                    result = null;

                } else if ((result.isSex() && gender == null)) {

                    if (log.isInfoEnabled()) {
                        log.info("Can't use definition (sex is required, but none was given): " + result);
                    }
                    result = null;

                } else {

                    if (log.isInfoEnabled()) {
                        log.info("Found matching definition: " + result);
                    }

                }

            }

        }

        return Optional.ofNullable(result);

    }

    private String getLogMessage(String prefix,
                                 String cruiseSamplingKey,
                                 String zoneSamplingKey,
                                 String fishingOperationSamplingKey,
                                 CruiseSamplingInternalCache.SamplingData cruiseSamplingData,
                                 CruiseSamplingInternalCache.SamplingData zoneSamplingData,
                                 CruiseSamplingInternalCache.SamplingData fishingOperationSamplingData) {

        int maxSize = IntStream
                .builder()
                .add(cruiseSamplingKey.length())
                .add(zoneSamplingKey.length())
                .add(fishingOperationSamplingKey.length())
                .build()
                .max()
                .orElseGet(() -> 0);

        return prefix
                + "\n[cruise            " + (String.format("%1$" + maxSize + "s", cruiseSamplingKey).replaceAll(" ", ".")) + "] → " + cruiseSamplingData
                + "\n[zone              " + (String.format("%1$" + maxSize + "s", zoneSamplingKey).replaceAll(" ", ".")) + "] → " + zoneSamplingData
                + "\n[fishing operation" + (String.format("%1$" + maxSize + "s", fishingOperationSamplingKey).replaceAll(" ", ".")) + "] → " + fishingOperationSamplingData;

    }

    private void cleanEmptyEntries() {
        cruiseCache.cleanEmptyEntries();
        zoneCache.cleanEmptyEntries();
        fishingOperationCache.cleanEmptyEntries();
    }

    public void printInfos(String message) {

        if (log.isInfoEnabled()) {

            String cruiseCacheInfos = cruiseCache.toStringVerbose();
            String zoneCacheInfos = zoneCache.toStringVerbose();
            String fishingOperationCacheInfos = fishingOperationCache.toStringVerbose();

            StringBuilder stringBuilder = new StringBuilder(message);
            stringBuilder.append("\nCruise            cache: ").append(cruiseCacheInfos);
            stringBuilder.append("\nZone              cache: ").append(zoneCacheInfos);
            stringBuilder.append("\nFishing operation cache: ").append(fishingOperationCacheInfos);

            if (log.isInfoEnabled()) {
                log.info(stringBuilder.toString());
            }
        }
    }

}
