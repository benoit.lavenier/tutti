package fr.ifremer.tutti.service.referential.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.referential.ReferentialImportRequest;
import fr.ifremer.tutti.service.referential.csv.SpeciesModel;
import fr.ifremer.tutti.service.referential.csv.SpeciesRow;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.nio.file.Path;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForTemporarySpecies extends CsvComsumer<SpeciesRow, SpeciesModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvConsumerForTemporarySpecies.class);

    public CsvConsumerForTemporarySpecies(Path file, char separator, boolean addReferenceTaxonId, boolean reportError) {
        super(file, SpeciesModel.forImport(separator, addReferenceTaxonId), reportError);
    }

    public void checkRow(ImportRow<SpeciesRow> row,
                         PersistenceService persistenceService,
                         DecoratorService decoratorService,
                         ReferentialImportRequest<Species, Integer> requestResult) {

        if (row.isValid()) {

            SpeciesRow bean = row.getBean();

            Integer id = bean.getIdAsInt();
            boolean delete = BooleanUtils.isTrue(bean.getToDelete());

            if (id == null) {

                // Ajout
                checkAdd(bean, requestResult);

            } else {

                // Mise à jour ou Suppression

                Species species = requestResult.getExistingEntityById(id);

                if (species == null) {
                    throw new ApplicationBusinessException(t("tutti.service.referential.import.species.error.notExistingId", id));
                }

                if (delete) {

                    // Suppression
                    checkDelete(bean, species, persistenceService, decoratorService, requestResult);

                } else {

                    // Mise à jour
                    checkUpdate(bean, species, requestResult);

                }
            }

        }

        reportError(row);

    }

    public void checkRowForGenericFormatImport(ImportRow<SpeciesRow> row, ReferentialImportRequest<Species, Integer> requestResult) {

        SpeciesRow bean = row.getBean();
        String name = bean.getName();

        if (row.isValid()) {

            Integer id = bean.getIdAsInt();

            if (id == null) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.species.error.noId")));

            } else if (!Speciess.isTemporaryId(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.species.error.idNotTemporary", id)));

            } else if (requestResult.isIdAlreadyAdded(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.species.error.id.alreaydAdded", id)));

            }

            if (StringUtils.isBlank(name)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.species.error.noName")));

            } else if (requestResult.isNaturalIdAlreadyAdded(name)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.species.error.name.alreaydAdded", name)));

            }

            if (bean.getReferenceTaxonId() == null) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.species.error.noReferencetTaxonId")));

            }

        }

        reportError(row);

        if (row.isValid()) {

            Species entity = bean.toEntity(null);
            boolean toAdd = requestResult.addExistingNaturalId(name);
            if (toAdd) {

                if (log.isInfoEnabled()) {
                    log.info("Will add species with name: " + name);
                }
                requestResult.addEntityToAdd(entity);

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Will link species with name: " + name);
                }
                requestResult.addEntityToLink(entity);

            }

        }

    }

    protected void checkAdd(SpeciesRow bean, ReferentialImportRequest<Species, Integer> requestResult) {

        String name = bean.getName();
        boolean delete = BooleanUtils.isTrue(bean.getToDelete());

        if (delete) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.species.error.cannotDeleteWithoutId"));
        }

        if (StringUtils.isBlank(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.species.error.noName"));
        }

        if (!requestResult.addExistingNaturalId(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.species.error.existingName", name));
        }

        if (log.isInfoEnabled()) {
            log.info("Will add species with name: " + name);
        }

        requestResult.addEntityToAdd(bean.toEntity(null));


    }

    protected void checkDelete(SpeciesRow bean, Species species, PersistenceService persistenceService,
                               DecoratorService decoratorService,
                               ReferentialImportRequest<Species, Integer> requestResult) {

        Integer id = bean.getIdAsInt();
        String name = bean.getName();
        Integer referenceTaxonId = species.getReferenceTaxonId();

        if (persistenceService.isTemporarySpeciesUsed(referenceTaxonId)) {

            String speciesRef = id + " : " + decoratorService.getDecoratorByType(Species.class, DecoratorService.WITH_SURVEY_CODE).toString(species);
            throw new ApplicationBusinessException(t("tutti.service.referential.import.species.error.used", speciesRef));
        }

        if (log.isInfoEnabled()) {
            log.info("Will delete species with referenceTaxonId: " + referenceTaxonId);
        }

        requestResult.addIdToDelete(referenceTaxonId);
        requestResult.removeExistingNaturalId(name);

    }

    protected void checkUpdate(SpeciesRow bean, Species species, ReferentialImportRequest<Species, Integer> requestResult) {

        Integer id = bean.getIdAsInt();
        String name = bean.getName();
        Integer referenceTaxonId = species.getReferenceTaxonId();

        if (StringUtils.isBlank(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.species.error.noName", id));
        }

        if (!species.getName().equals(name) && !requestResult.addExistingNaturalId(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.species.error.existingName", name));
        }

        if (log.isInfoEnabled()) {
            log.info("Will update species with referenceTaxonId: " + referenceTaxonId);
        }

        requestResult.addEntityToUpdate(bean.toEntity(referenceTaxonId));

    }


}