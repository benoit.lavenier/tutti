package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.TuttiValidationDataContextSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2/19/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportValidationDataContext extends TuttiValidationDataContextSupport {

    private Cruise cruise;

    private TuttiProtocol protocol;

    private FishingOperation fishingOperation;

    private final GenericFormatContextSupport importContext;

    public GenericFormatImportValidationDataContext(GenericFormatContextSupport importContext) {
        this.importContext = importContext;
    }

    // Not used
    @Override
    protected List<Program> loadExistingPrograms() {
        return Lists.newArrayList();
    }

    // Not used
    @Override
    protected List<TuttiProtocol> loadExistingProtocols() {
        return Lists.newArrayList();
    }

    @Override
    protected List<FishingOperation> loadExistingFishingOperations() {

        List<FishingOperation> result = new ArrayList<>();

        if (cruise != null) {

            GenericFormatImportCruiseContext cruiseContext = importContext.getCruiseContext(cruise);
            Iterable<FishingOperation> importedFishingOperations = cruiseContext.getFishingOperations();
            Iterables.addAll(result, importedFishingOperations);
            if (fishingOperation != null) {
                result.remove(fishingOperation);
            }

        }
        return result;
    }

    @Override
    protected Program getProgram() {
        return importContext.getImportRequest().getProgram();
    }

    @Override
    protected Cruise getCruise() {
        return cruise;
    }

    @Override
    protected TuttiProtocol getProtocol() {
        return protocol;
    }

    @Override
    protected FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public void setCruise(Cruise cruise) {
        this.cruise = cruise;
        resetExistingFishingOperations();
    }

    public void setProtocol(TuttiProtocol protocol) {
        this.protocol = protocol;
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        this.fishingOperation = fishingOperation;
        resetExistingFishingOperations();
    }
}
