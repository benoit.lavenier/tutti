package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class ReferentialImportResult<E extends TuttiReferentialEntity> {

    protected List<E> refAdded = new ArrayList<>();

    protected List<E> refUpdated = new ArrayList<>();

    protected List<E> refLinked = new ArrayList<>();

    protected int nbRefDeleted = 0;

    public List<E> getRefAdded() {
        return Collections.unmodifiableList(refAdded);
    }

    public void addAllRefsAdded(List<E> refs) {
        refAdded.addAll(refs);
    }

    public int getNbRefAdded() {
        return refAdded.size();
    }

    public List<E> getRefUpdated() {
        return Collections.unmodifiableList(refUpdated);
    }

    public void addAllRefsUpdated(List<E> refs) {
        refUpdated.addAll(refs);
    }

    public int getNbRefUpdated() {
        return refUpdated.size();
    }

    public List<E> getRefLinked() {
        return Collections.unmodifiableList(refLinked);
    }

    public void addAllRefsLinked(List<E> refs) {
        refLinked.addAll(refs);
    }

    public int getNbRefLinked() {
        return refLinked.size();
    }

    public int getNbRefDeleted() {
        return nbRefDeleted;
    }

    public void setNbRefDeleted(int nbRefDeleted) {
        this.nbRefDeleted = nbRefDeleted;
    }
}
