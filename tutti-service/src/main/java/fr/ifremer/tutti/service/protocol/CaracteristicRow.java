/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.tutti.service.protocol;

import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Bean used as a row for import of {@link Caracteristic}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class CaracteristicRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_PMFM_ID = "pmfmId";

    public static final String PROPERTY_PMFM_TYPE = "pmfmType";

    public static final String PROPERTY_PMFM_PARAMETER_NAME = "pmfmParameterName";

    public static final String PROPERTY_PMFM_MATRIX_NAME = "pmfmMatrixName";

    public static final String PROPERTY_PMFM_FRACTION_NAME = "pmfmFractionName";

    public static final String PROPERTY_PMFM_METHOD_NAME = "pmfmMethodName";

    public static final String PROPERTY_PMFM = "pmfm";

    public static final String PROPERTY_MATURE_STATE_IDS = "matureStateIds";;

    protected CaracteristicType pmfmType;

    protected Caracteristic pmfm;

    protected Set<String> matureStateIds = new LinkedHashSet<>();

    public Caracteristic getPmfm() {
        return pmfm;
    }

    public void setPmfm(Caracteristic pmfm) {
        this.pmfm = pmfm;
    }

    public CaracteristicType getPmfmType() {
        return pmfmType;
    }

    public void setPmfmType(CaracteristicType pmfmType) {
        this.pmfmType = pmfmType;
    }

    public String getPmfmId() {
        return getPmfm().getId();
    }

    public String getPmfmParameterName() {
        return getPmfm().getParameterName();
    }

    public String getPmfmMatrixName() {
        return getPmfm().getMatrixName();
    }

    public String getPmfmMethodName() {
        return getPmfm().getMethodName();
    }

    public String getPmfmFractionName() {
        return getPmfm().getFractionName();
    }

    public Set<String> getMatureStateIds() {
        return matureStateIds;
    }

    public void setMatureStateIds(Set<String> matureStateIds) {
        this.matureStateIds = matureStateIds;
    }
}
