package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatchs;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;

/**
 * A row in a marine litter export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.1
 */
public class MarineLitterRow extends RowWithOperationContextSupport {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_MARINE_LITTER_ID = "marineLitterId";

    protected MarineLitterBatch marineLitterBatch;

    public static MarineLitterRow newEmptyInstance() {
        MarineLitterRow row = new MarineLitterRow();
        row.forImport();
        row.setMarineLitterBatch(MarineLitterBatchs.newMarineLitterBatch());
        return row;
    }

    public void setMarineLitterBatch(MarineLitterBatch marineLitterBatch) {
        this.marineLitterBatch = marineLitterBatch;
    }

    public void setMarineLitterCategory(CaracteristicQualitativeValue marineLitterCategory) {
        marineLitterBatch.setMarineLitterCategory(marineLitterCategory);
    }

    public void setMarineLitterSizeCategory(CaracteristicQualitativeValue marineLitterSizeCategory) {
        marineLitterBatch.setMarineLitterSizeCategory(marineLitterSizeCategory);
    }

    public void setNumber(Integer number) {
        marineLitterBatch.setNumber(number);
    }

    public void setWeight(Float weight) {
        marineLitterBatch.setWeight(weight);
    }

    public void setComment(String comment) {
        marineLitterBatch.setComment(comment);
    }

    public void setMarineLitterId(Integer marineLitterId) {
        marineLitterBatch.setId(marineLitterId);
    }

    public MarineLitterBatch getMarineLitterBatch() {
        return marineLitterBatch;
    }

    public CaracteristicQualitativeValue getMarineLitterCategory() {
        return marineLitterBatch.getMarineLitterCategory();
    }

    public CaracteristicQualitativeValue getMarineLitterSizeCategory() {
        return marineLitterBatch.getMarineLitterSizeCategory();
    }

    public Integer getNumber() {
        return marineLitterBatch.getNumber();
    }

    public Float getWeight() {
        return marineLitterBatch.getWeight();
    }

    public String getComment() {
        return marineLitterBatch.getComment();
    }

    public Integer getMarineLitterId() {
        return marineLitterBatch.getIdAsInt();
    }

}
