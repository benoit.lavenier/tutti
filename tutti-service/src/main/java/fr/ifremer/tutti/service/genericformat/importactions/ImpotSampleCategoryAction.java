package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForSampleCategory;
import fr.ifremer.tutti.service.genericformat.csv.SampleCategoryRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImpotSampleCategoryAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImpotSampleCategoryAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImpotSampleCategoryAction(GenericFormatContextSupport importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return true;
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Validate sample category model.");
        }

        importContext.increments(t("tutti.service.genericFormat.import.sampleCategoryModel"));

        List<SampleCategoryModelEntry> categories = new ArrayList<>();

        GenericFormatCsvFileResult importFileResult = importContext.getSampleCategoryFileResult();
        try (CsvConsumerForSampleCategory consumer = importContext.loadSampleCategories(false)) {


            Set<String> existingCodes = new HashSet<>();
            Set<Integer> existingCaracteristicIds = new HashSet<>();

            for (ImportRow<SampleCategoryRow> row : consumer) {

                consumer.validateRow(row, existingCodes, existingCaracteristicIds);

                if (row.isValid()) {

                    SampleCategoryRow bean = row.getBean();
                    categories.add(bean.getSampleCategoryModelEntry());

                }

            }

            importFileResult.flushErrors(consumer);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close sampleCategory.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        if (importFileResult.isValid()) {

            SampleCategoryModel sampleCategoryModel = new SampleCategoryModel(categories);

            if (log.isInfoEnabled()) {
                log.info("Will use sample category model: " + sampleCategoryModel);
            }
            persistenceHelper.setSampleCategoryModel(sampleCategoryModel);
            importContext.setImportedSampleCategoryModel(sampleCategoryModel);

        }

    }

}
