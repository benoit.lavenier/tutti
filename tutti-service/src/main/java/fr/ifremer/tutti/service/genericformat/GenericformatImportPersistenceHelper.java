package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.service.referential.ReferentialImportRequest;
import fr.ifremer.tutti.service.referential.ReferentialImportResult;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryGearService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryPersonService;
import fr.ifremer.tutti.service.referential.ReferentialTemporarySpeciesService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryVesselService;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 2/19/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericformatImportPersistenceHelper {

    private final GenericFormatContextSupport importContext;

    private final PersistenceService persistenceService;

    private final ReferentialTemporaryGearService referentialTemporaryGearService;

    private final ReferentialTemporaryPersonService referentialTemporaryPersonService;

    private final ReferentialTemporarySpeciesService referentialTemporarySpeciesService;

    private final ReferentialTemporaryVesselService referentialTemporaryVesselService;

    private final ProtocolImportExportService protocolImportExportService;

    private final Caracteristic copyIndividualObservationModeCaracteristic;
    private final Caracteristic weightMeasuredCaracteristic;
    private final Caracteristic pmfmIdCaracteristic;
    private final Caracteristic sampleCodeCaracteristic;

    public GenericformatImportPersistenceHelper(TuttiServiceContext context, GenericFormatContextSupport importContext) {
        this.importContext = importContext;
        this.persistenceService = context.getService(PersistenceService.class);
        this.referentialTemporaryGearService = context.getService(ReferentialTemporaryGearService.class);
        this.referentialTemporaryPersonService = context.getService(ReferentialTemporaryPersonService.class);
        this.referentialTemporarySpeciesService = context.getService(ReferentialTemporarySpeciesService.class);
        this.referentialTemporaryVesselService = context.getService(ReferentialTemporaryVesselService.class);
        this.protocolImportExportService = context.getService(ProtocolImportExportService.class);
        this.weightMeasuredCaracteristic = persistenceService.getWeightMeasuredCaracteristic();
        this.copyIndividualObservationModeCaracteristic = persistenceService.getCopyIndividualObservationModeCaracteristic();
        this.pmfmIdCaracteristic = persistenceService.getPmfmIdCaracteristic();
        this.sampleCodeCaracteristic = persistenceService.getSampleCodeCaracteristic();
    }

    public Set<FishingOperation> getFishingOperations(Integer cruiseId) {

        List<FishingOperation> allFishingOperation = persistenceService.getAllFishingOperation(cruiseId);
        return Sets.newLinkedHashSet(allFishingOperation);

    }

    public void deleteAllAttachments(ObjectTypeCode objectTypeCode, Integer objectId) {

        persistenceService.deleteAllAttachment(objectTypeCode, objectId);

    }

    public void persistAttachments(Integer objectId, Collection<AttachmentRow> attachmentRows) {

        if (importContext.getImportRequest().isImportAttachments() && CollectionUtils.isNotEmpty(attachmentRows)) {

            for (AttachmentRow attachmentRow : attachmentRows) {

                Attachment attachment = attachmentRow.toAttachment(objectId);
                File attachmentFile = importContext.getImportRequest().getArchive().extractAttachment(attachment.getPath());
                persistenceService.createAttachment(attachment, attachmentFile);

            }

        }

    }

    public Cruise createCruise(Cruise cruise) {
        return persistenceService.createCruise(cruise);
    }

    public Cruise saveCruise(Cruise cruise) {
        return persistenceService.saveCruise(cruise, false, false);
    }

    public void saveGearCaracteristics(Gear gear, Cruise cruise) {
        persistenceService.saveGearCaracteristics(gear, cruise);
    }

    public FishingOperation createFishingOperation(FishingOperation fishingOperation) {
        return persistenceService.createFishingOperation(fishingOperation);
    }


    public FishingOperation saveFishingOperation(FishingOperation fishingOperation) {
        return persistenceService.saveFishingOperation(fishingOperation);
    }

    public CatchBatch getExistingCatchBatch(Integer operationId) {
        boolean withCatchBatch = isWithCatchBatch(operationId);
        CatchBatch catchBatch;
        if (withCatchBatch) {
            catchBatch = persistenceService.getCatchBatchFromFishingOperation(operationId);
        } else {
            catchBatch = null;
        }
        return catchBatch;
    }

    public boolean isWithCatchBatch(Integer operationId) {
        return persistenceService.isFishingOperationWithCatchBatch(operationId);
    }

    public CatchBatch createCatchBatch(CatchBatch catchBatch) {
        return persistenceService.createCatchBatch(catchBatch);
    }

    public CatchBatch saveCatchBatch(CatchBatch catchBatch) {
        return persistenceService.saveCatchBatch(catchBatch);
    }

    public SpeciesBatch createSpeciesBatch(SpeciesBatch batch, Integer parentId) {
        return persistenceService.createSpeciesBatch(batch, parentId, false);
    }

    public void saveSpeciesBatchFrequency(Integer batchId, List<SpeciesBatchFrequency> frequencies) {
        persistenceService.saveSpeciesBatchFrequency(batchId, frequencies);
    }

    public void deleteSpeciesBatchForFishingOperation(Integer fishingOperationId) {

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(fishingOperationId, false);
        for (SpeciesBatch batch : rootSpeciesBatch.getChildren()) {
            persistenceService.deleteSpeciesBatch(batch.getIdAsInt());
        }

    }

    public SpeciesBatch createBenthosBatch(SpeciesBatch batch, Integer parentId) {
        return persistenceService.createBenthosBatch(batch, parentId, false);
    }

    public void saveBenthosBatchFrequency(Integer batchId, List<SpeciesBatchFrequency> frequencies) {
        persistenceService.saveBenthosBatchFrequency(batchId, frequencies);
    }

    public void deleteBenthosBatchForFishingOperation(Integer fishingOperationId) {

        BatchContainer<SpeciesBatch> rootBenthosBatch = persistenceService.getRootBenthosBatch(fishingOperationId, false);
        for (SpeciesBatch batch : rootBenthosBatch.getChildren()) {
            persistenceService.deleteBenthosBatch(batch.getIdAsInt());
        }

    }

    public MarineLitterBatch createMarineLitterBatch(MarineLitterBatch marineLitterBatch) {
        return persistenceService.createMarineLitterBatch(marineLitterBatch);
    }

    public void deleteMarineLitterForFishingOperation(Integer fishingOperationId) {

        BatchContainer<MarineLitterBatch> rootMarineLitters = persistenceService.getRootMarineLitterBatch(fishingOperationId);
        for (MarineLitterBatch batch : rootMarineLitters.getChildren()) {
            persistenceService.deleteMarineLitterBatch(batch.getIdAsInt());
        }
    }

    public AccidentalBatch createAccidentalBatch(AccidentalBatch accidentalBatch) {
        return persistenceService.createAccidentalBatch(accidentalBatch);
    }

    public void deleteAccidentalBatchForFishingOperation(Integer fishingOperationId) {
        persistenceService.deleteAccidentalBatchForFishingOperation(fishingOperationId);
    }

    public void setSampleCategoryModel(SampleCategoryModel sampleCategoryModel) {
        persistenceService.setSampleCategoryModel(sampleCategoryModel);
    }

    public ReferentialImportRequest<Gear, Integer> createGearImportRequest() {
        return referentialTemporaryGearService.createReferentialImportRequest();
    }

    public ReferentialImportResult<Gear> importGears(ReferentialImportRequest<Gear, Integer> referentialImportRequest) {
        return referentialTemporaryGearService.executeImportRequest(referentialImportRequest);
    }

    public ReferentialImportRequest<Person, Integer> createPersonImportRequest() {
        return referentialTemporaryPersonService.createReferentialImportRequest();
    }

    public ReferentialImportResult<Person> importPersons(ReferentialImportRequest<Person, Integer> referentialImportRequest) {
        return referentialTemporaryPersonService.executeImportRequest(referentialImportRequest);
    }

    public ReferentialImportRequest<Vessel, String> createVesselsImportRequest() {
        return referentialTemporaryVesselService.createReferentialImportRequest();
    }

    public ReferentialImportResult<Vessel> importVessels(ReferentialImportRequest<Vessel, String> referentialImportRequest) {
        return referentialTemporaryVesselService.executeImportRequest(referentialImportRequest);
    }

    public ReferentialImportRequest<Species, Integer> createSpeciesImportRequest() {
        return referentialTemporarySpeciesService.createReferentialImportRequest();
    }

    public ReferentialImportResult<Species> importSpecies(ReferentialImportRequest<Species, Integer> referentialImportRequest) {
        return referentialTemporarySpeciesService.executeImportRequest(referentialImportRequest);
    }

    public TuttiProtocol importProtocol(File file) {
        return protocolImportExportService.importProtocol(file);
    }

    public List<Species> getAllReferentSpecies() {
        return persistenceService.getAllReferentSpecies();
    }

    public TuttiProtocol createProtocol(TuttiProtocol tuttiProtocol) {
        TuttiProtocol createdProtocol = persistenceService.createProtocol(tuttiProtocol);
        persistenceService.setProtocol(createdProtocol);
        return createdProtocol;
    }

    public String getProtocolFirstAvailableName(String protocolOriginalName) {
        return persistenceService.getFirstAvailableName(protocolOriginalName);
    }

    public Map<Integer, Integer> getAllObsoleteReferentTaxons() {
        return persistenceService.getAllObsoleteReferentTaxons();
    }

    public void deleteIndividualObservationBatchForFishingOperation(Integer fishingOperationId) {
        persistenceService.deleteAllIndividualObservationsForFishingOperation(fishingOperationId);
    }

    public List<IndividualObservationBatch> createIndividualObservationBatch(FishingOperation fishingOperation, List<IndividualObservationBatch> individualObservationBatch) {
        return persistenceService.createIndividualObservationBatches(fishingOperation, individualObservationBatch);
    }

    public Caracteristic getCaracteristic(Integer caracteristicId) {
        return persistenceService.getCaracteristic(caracteristicId);
    }

    public Caracteristic getCopyIndividualObservationModeCaracteristic() {
        return copyIndividualObservationModeCaracteristic;
    }

    public Caracteristic getPmfmIdCaracteristic() {
        return pmfmIdCaracteristic;
    }

    public Caracteristic getWeightMeasuredCaracteristic() {
        return weightMeasuredCaracteristic;
    }


    public Caracteristic getSampleCodeCaracteristic() {
        return sampleCodeCaracteristic;
    }

}
