package fr.ifremer.tutti.service.psionimport;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class PsionImportModel {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PsionImportModel.class);

    /**
     * All registred species in their registred order.
     */
    protected final LinkedHashSet<Species> speciesSet;

    /**
     * All sorted batch indexed by their species.
     *
     * @since 3.4.2
     */
    protected final Multimap<Species, PsionImportBatchModel> sortedBatchsBySpecies;

    /**
     * All unsorted batch indexed by their species.
     *
     * @since 3.4.2
     */
    protected final Multimap<Species, PsionImportBatchModel> unsortedBatchsBySpecies;

    protected final List<String> errors;

    public PsionImportModel() {
        speciesSet = new LinkedHashSet<>();
        sortedBatchsBySpecies = ArrayListMultimap.create();
        unsortedBatchsBySpecies = ArrayListMultimap.create();
        errors = Lists.newArrayList();
    }

    public boolean withBatchs() {
        return !speciesSet.isEmpty();
    }

    public Set<Species> getSpecies() {
        return ImmutableSet.copyOf(speciesSet);
    }

    public List<PsionImportBatchModel> getUnsortedBatches(Species species) {
        Collection<PsionImportBatchModel> batches = unsortedBatchsBySpecies.get(species);
        List<PsionImportBatchModel> result = null;

        if (batches != null) {
            result = ImmutableList.copyOf(batches);

        }
        return result;
    }

    public List<PsionImportBatchModel> getSortedBatches(Species species) {
        Collection<PsionImportBatchModel> batches = sortedBatchsBySpecies.get(species);
        List<PsionImportBatchModel> result = null;

        if (batches != null) {
            result = ImmutableList.copyOf(batches);
        }
        return result;
    }

    public Set<Integer> getSampleCategoryIdUsed() {
        Set<Integer> result = Sets.newHashSet();
        for (PsionImportBatchModel batch : sortedBatchsBySpecies.values()) {
            Iterator<PsionImportBatchModel.SampleCategory> categoryIterator = batch.getCategoryIterator();
            while (categoryIterator.hasNext()) {
                PsionImportBatchModel.SampleCategory next = categoryIterator.next();

                result.add(next.getCategoryId());
            }
        }
        for (PsionImportBatchModel batch : unsortedBatchsBySpecies.values()) {
            Iterator<PsionImportBatchModel.SampleCategory> categoryIterator = batch.getCategoryIterator();
            while (categoryIterator.hasNext()) {
                PsionImportBatchModel.SampleCategory next = categoryIterator.next();

                result.add(next.getCategoryId());
            }
        }
        return result;
    }

    public boolean withErrors() {
        return !errors.isEmpty();
    }

    public List<String> getErrors() {
        return errors;
    }

    void addBatch(PsionImportBatchModel batchModel) {

        Species species = batchModel.getSpecies();
        speciesSet.add(species);

        String categoryCode = batchModel.getCategoryCode();
        Float weight = batchModel.getWeight();
        Float sampleWeight = batchModel.getSampleWeight();

        Multimap<Species, PsionImportBatchModel> store;

        // --- Guess if sorted or unsorted batch --- //
        if (WeightUnit.KG.isGreaterThanZero(weight) && WeightUnit.KG.isEquals(weight, sampleWeight)) {

            store = unsortedBatchsBySpecies;
            if (log.isInfoEnabled()) {
                log.info(String.format("Found a unsorted batch [%s] %s - %s", species.getSurveyCode(), weight, sampleWeight));
            }
        } else {

            store = sortedBatchsBySpecies;
            if (log.isInfoEnabled()) {
                log.info(String.format("Found a sorted batch [%s] %s - %s", species.getSurveyCode(), weight, sampleWeight));
            }
        }

        // --- Get if exist the previous batch --- //

        Collection<PsionImportBatchModel> psionImportBatchModels = store.get(species);

        PsionImportBatchModel mergeBatch = null;

        if (CollectionUtils.isNotEmpty(psionImportBatchModels)) {

            for (PsionImportBatchModel importBatchModel : psionImportBatchModels) {
                if (categoryCode.equals(importBatchModel.getCategoryCode())) {
                    mergeBatch = importBatchModel;
                    break;
                }
            }
        }

        if (mergeBatch == null) {

            // new batch
            store.put(species, batchModel);

            if (log.isDebugEnabled()) {
                log.debug("Added " + batchModel);
            }
        } else {

            // merge batch
            mergeBatch.merge(batchModel);

            if (log.isDebugEnabled()) {
                log.debug("Merged " + batchModel + " to " + mergeBatch);
            }
        }
    }

    void addError(String error) {
        errors.add(error);
    }

    void cleanSortedBatches() {

        for (Species species : sortedBatchsBySpecies.keySet()) {

            for (PsionImportBatchModel batchModel : sortedBatchsBySpecies.get(species)) {

                Float weight = batchModel.getWeight();
                Float sampleWeight = batchModel.getSampleWeight();

                if (WeightUnit.KG.isZero(weight) && WeightUnit.KG.isGreaterThanZero(sampleWeight)) {

                    // POID = 0 et TAIL != POID : un seul poids à positionner
                    batchModel.setWeight(sampleWeight);
                    batchModel.setSampleWeight(null);

                }

            }

        }

    }

    void cleanUnsortedBatches() {

        for (Species species : unsortedBatchsBySpecies.keySet()) {

            for (PsionImportBatchModel batchModel : unsortedBatchsBySpecies.get(species)) {

                // POID = TAIL  un seul poids à positionner
                batchModel.setSampleWeight(null);
            }

        }
    }

    void checkSortedBatches(SampleCategoryModel sampleCategoryModel) throws IOException {

        Map<Integer, SampleCategoryModelEntry> categoriesById =
                sampleCategoryModel.getCategoryMap();

        for (Species species : sortedBatchsBySpecies.keySet()) {

            Collection<PsionImportBatchModel> speciesBatchesWithDoubleWeight = new ArrayList<>();
            Collection<PsionImportBatchModel> speciesBatchesWithDoubleWeightAndCat = new ArrayList<>();

            for (PsionImportBatchModel batchModel : sortedBatchsBySpecies.get(species)) {

                checkSampleCategoryModel(categoriesById, batchModel);

                Float weight = batchModel.getWeight();
                Float sampleWeight = batchModel.getSampleWeight();

                if (WeightUnit.KG.isZero(weight) && WeightUnit.KG.isGreaterThanZero(sampleWeight)) {

                    // POID = 0 et TAIL != POID : un seul poids à positionner
                    continue;
                }

                if (WeightUnit.KG.isGreaterThanZero(weight) && WeightUnit.KG.isGreaterThan(weight, sampleWeight)) {

                    // POID > 0 et POID > TAIL : deux poids à positionner
                    speciesBatchesWithDoubleWeight.add(batchModel);

                    if (batchModel.withCategories()) {

                        speciesBatchesWithDoubleWeightAndCat.add(batchModel);
                    }

                }

            }

            if (!speciesBatchesWithDoubleWeight.isEmpty()) {

                if (speciesBatchesWithDoubleWeightAndCat.size() == speciesBatchesWithDoubleWeight.size()) {

                    // tous les lots sont categories
                    // on marque les lots pour que weight soit sur la categorie et sampleWeight comme poids de sous echantillon

                    for (PsionImportBatchModel batchModel : speciesBatchesWithDoubleWeight) {
                        batchModel.setApplyBothWeightOnCategorizedBatch(true);
                    }
                } else if (speciesBatchesWithDoubleWeight.size() == 1 && speciesBatchesWithDoubleWeightAndCat.isEmpty()) {

                    // un seul lot non categorise
                } else {

                    // on bloque l'import
                    throw new IOException(
                            t("tutti.service.psionimport.error.inconsistentVracCategory.message", species.getSurveyCode()));
                }

            }

        }

    }

    protected void checkSampleCategoryModel(Map<Integer, SampleCategoryModelEntry> categoriesById,
                                            PsionImportBatchModel batchModel) throws IOException {

        SampleCategoryModelEntry lastSampleCategory = null;

        if (batchModel.withCategories()) {

            Iterator<PsionImportBatchModel.SampleCategory> categoryIterator = batchModel.getCategoryIterator();
            while (categoryIterator.hasNext()) {
                PsionImportBatchModel.SampleCategory nextCat = categoryIterator.next();
                SampleCategoryModelEntry actualCategory = categoriesById.get(nextCat.getCategoryId());

                if (lastSampleCategory != null) {

                    if (actualCategory.getOrder() < lastSampleCategory.getOrder()) {

                        throw new IOException(t("tutti.service.psionimport.error.mismatchSampleCategory.message", batchModel.getSpecies().getSurveyCode(), actualCategory, lastSampleCategory));

                    }
                }
                lastSampleCategory = actualCategory;

            }

        }

    }
}
