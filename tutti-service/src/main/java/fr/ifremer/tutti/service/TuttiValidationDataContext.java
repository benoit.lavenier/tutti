package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;

import java.util.List;

/**
 * Data to shared by validators.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class TuttiValidationDataContext extends TuttiValidationDataContextSupport {

    private final TuttiDataContext dataContext;

    public TuttiValidationDataContext(TuttiDataContext dataContext) {
        this.dataContext = dataContext;
    }

    @Override
    protected List<Program> loadExistingPrograms() {
        dataContext.checkOpened();
        List<Program> existingPrograms = Lists.newArrayList(dataContext.service.getAllProgram());

        if (dataContext.isProgramFilled()) {

            // remove current program
            existingPrograms.remove(getProgram());
        }

        return existingPrograms;
    }

    @Override
    protected List<TuttiProtocol> loadExistingProtocols() {
        dataContext.checkOpened();
        List<TuttiProtocol> existingProtocols = Lists.newArrayList(dataContext.service.getAllProtocol());

        if (dataContext.isProtocolFilled()) {

            // remove current protocol
            existingProtocols.remove(getProtocol());
        }

        return existingProtocols;
    }

    @Override
    protected List<FishingOperation> loadExistingFishingOperations() {
        dataContext.checkOpened();
        List<FishingOperation> existingFishingOperations = Lists.newArrayList(dataContext.service.getAllFishingOperation(dataContext.getCruiseId()));

        if (dataContext.isFishingOperationFilled()) {

            // remove current protocol
            existingFishingOperations.remove(getFishingOperation());
        }

        return existingFishingOperations;
    }

    @Override
    protected Program getProgram() {
        return dataContext.getProgram();
    }

    @Override
    protected Cruise getCruise() {
        return dataContext.getCruise();
    }

    @Override
    protected TuttiProtocol getProtocol() {
        return dataContext.getProtocol();
    }

    @Override
    protected FishingOperation getFishingOperation() {
        return dataContext.getFishingOperation();
    }

}
