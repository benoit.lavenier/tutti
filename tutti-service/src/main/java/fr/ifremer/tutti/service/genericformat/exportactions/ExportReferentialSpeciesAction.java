package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import fr.ifremer.tutti.service.referential.ReferentialTemporarySpeciesService;
import fr.ifremer.tutti.service.referential.csv.SpeciesRow;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporarySpecies;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class ExportReferentialSpeciesAction extends ExportTechnicalActionSupport {

    private final ReferentialTemporarySpeciesService referentialTemporarySpeciesService;

    public ExportReferentialSpeciesAction(ReferentialTemporarySpeciesService referentialTemporarySpeciesService) {
        this.referentialTemporarySpeciesService = referentialTemporarySpeciesService;
    }

    @Override
    public void execute(GenericFormatExportContext exportContext) {

        List<Species> temporarySpeciess = referentialTemporarySpeciesService.getTemporarySpeciess();
        exportContext.increments(t("tutti.service.genericFormat.export.exportTemporarySpecies", temporarySpeciess.size()));

        try {

            CsvProducerForTemporarySpecies producerForTemporarySpecies = exportContext.getProducerForTemporarySpecies();

            List<SpeciesRow> dataToExport = producerForTemporarySpecies.getDataToExport(temporarySpeciess);
            producerForTemporarySpecies.write(dataToExport);

        } catch (Exception e) {
            throw new ApplicationTechnicalException("Could not export temporary species", e);
        }

    }
}
