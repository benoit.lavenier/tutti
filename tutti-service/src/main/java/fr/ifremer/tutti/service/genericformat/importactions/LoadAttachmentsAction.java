package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForAttachment;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/26/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class LoadAttachmentsAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadAttachmentsAction.class);

    public LoadAttachmentsAction(GenericFormatContextSupport importContext) {
        super(importContext);
    }

    @Override
    protected boolean canExecute() {
        return importContext.getImportRequest().isImportAttachments();
    }

    @Override
    protected void skipExecute() {

        importContext.increments(t("tutti.service.genericFormat.skip.load.attachments"));

        if (!importContext.getImportRequest().isImportAttachments()) {
            GenericFormatCsvFileResult importFileResult = importContext.getAttachmentFileResult();
            importFileResult.setSkipped(true);
        }

    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Load attachments definitions.");
        }

        importContext.increments(t("tutti.service.genericFormat.load.attachments"));

        GenericFormatCsvFileResult importFileResult = importContext.getAttachmentFileResult();
        try (CsvConsumerForAttachment consumer = importContext.loadAttachments(false)) {

            Multimap<String, AttachmentRow> attachmentRowsByObjectId = ArrayListMultimap.create();

            for (ImportRow<AttachmentRow> row : consumer) {

                consumer.validateRow(row);

                if (row.isValid()) {

                    AttachmentRow bean = row.getBean();
                    attachmentRowsByObjectId.put(bean.getObjectType() + "_" + bean.getObjectId(), bean);

                }

            }

            importFileResult.flushErrors(consumer);

            importContext.setAttachmentRows(attachmentRowsByObjectId);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close attachments.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

    }

}