package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.model.OperationDataModel;
import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

/**
 * Created on 2/20/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportOperationContext implements Closeable {

    public static final Function<GenericFormatImportOperationContext, FishingOperation> TO_FISHING_OPERATION_FUNCTION = GenericFormatImportOperationContext::getFishingOperation;

    private final FishingOperation fishingOperation;

    private final CatchBatch catchBatch;

    private final OperationDataModel existingFishingOperationData;

    private final String fishingOperationLabel;

    private final Collection<MarineLitterBatch> marineLitterBatches;

    private final Map<Integer, AccidentalBatch> accidentalBatchesById;

    /** all observation available in imported file */
    private final Map<Integer, IndividualObservationBatch> individualObservationBatchesById;

    /** valid observation realy imported in database */
    private List<IndividualObservationBatch> individualObservationBatchImported;

    private final Map<Integer, SpeciesBatch> vracSpeciesBatches;

    private final Map<Integer, SpeciesBatch> horsVracSpeciesBatches;

    private final ArrayListMultimap<Integer, SpeciesBatchFrequency> speciesFrequencies;

    private final Map<Integer, SpeciesBatch> vracBenthosBatches;

    private final Map<Integer, SpeciesBatch> horsVracBenthosBatches;

    /**
     * Dictionnaire des ids de lot espèces (clef : l'id assigné pendant l'import, valeur l'id dans les fichiers d'import).
     */
    private final Map<Integer, Integer> batchesObjectIds;

    /**
     * Dictionnaire des ids des lots espèces importés (clef : id dans les fichiers d'import, valeur l'id persisté).
     */
    private final Map<Integer, Integer> speciesBatchIds;

    private final ArrayListMultimap<Integer, SpeciesBatchFrequency> benthosFrequencies;

    private final CaracteristicMap gearUseFeatures;

    private final CaracteristicMap vesselUseFeatures;

    private final Set<String> checkErrors;

    private boolean weightsDeleted;

    public GenericFormatImportOperationContext(FishingOperation fishingOperation,
                                               CatchBatch catchBatch,
                                               OperationDataModel existingFishingOperationData,
                                               String fishingOperationLabel) {

        this.fishingOperation = fishingOperation;
        this.catchBatch = catchBatch;
        this.existingFishingOperationData = existingFishingOperationData;
        this.fishingOperationLabel = fishingOperationLabel;
        this.marineLitterBatches = new ArrayList<>();
        this.accidentalBatchesById = new TreeMap<>();
        this.individualObservationBatchesById = new TreeMap<>();
        this.individualObservationBatchImported = Collections.emptyList();
        this.vracSpeciesBatches = new TreeMap<>();
        this.horsVracSpeciesBatches = new TreeMap<>();
        this.speciesFrequencies = ArrayListMultimap.create();
        this.vracBenthosBatches = new TreeMap<>();
        this.horsVracBenthosBatches = new TreeMap<>();
        this.benthosFrequencies = ArrayListMultimap.create();
        this.gearUseFeatures = new CaracteristicMap();
        this.vesselUseFeatures = new CaracteristicMap();
        this.checkErrors = new LinkedHashSet<>();
        this.batchesObjectIds = new TreeMap<>();
        this.speciesBatchIds = new TreeMap<>();

    }

    public boolean isOverride() {
        return existingFishingOperationData != null;
    }

    public OperationDataModel getExistingFishingOperationData() {
        return existingFishingOperationData;
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public CatchBatch getCatchBatch() {
        return catchBatch;
    }

    public String getFishingOperationLabel() {
        return fishingOperationLabel;
    }

    public boolean withGearFeatures() {
        return MapUtils.isNotEmpty(gearUseFeatures);
    }

    public boolean withVesselFeatures() {
        return MapUtils.isNotEmpty(vesselUseFeatures);
    }

    public boolean withMarineLitterBatches() {
        return CollectionUtils.isNotEmpty(marineLitterBatches);
    }

    public boolean withAccidentalBatches() {
        return MapUtils.isNotEmpty(accidentalBatchesById);
    }

    public boolean withIndividualObservationBatches() {
        return MapUtils.isNotEmpty(individualObservationBatchesById);
    }

    public boolean withSpeciesBatches(boolean vrac) {
        return MapUtils.isNotEmpty(getSpeciesBatchMap(vrac));
    }

    public boolean withBenthosBatches(boolean vrac) {
        return MapUtils.isNotEmpty(getBenthosBatchMap(vrac));
    }

    public AccidentalBatch getAccidentalBatchById(Integer accidentalBatchId) {
        return accidentalBatchesById.get(accidentalBatchId);
    }

    public IndividualObservationBatch getIndividualObservationBatchById(Integer individualObservationBatchId) {
        return individualObservationBatchesById.get(individualObservationBatchId);
    }

    public SpeciesBatch getSpeciesBatch(boolean vrac, Integer referenceTaxonId) {
        return getSpeciesBatchMap(vrac).get(referenceTaxonId);
    }

    public SpeciesBatch getBenthosBatch(boolean vrac, Integer referenceTaxonId) {
        return getBenthosBatchMap(vrac).get(referenceTaxonId);
    }

    public void addGearUseFeature(Caracteristic caracteristic, Serializable value) {
        gearUseFeatures.put(caracteristic, value);
    }

    public void addVesselUseFeature(Caracteristic caracteristic, Serializable value) {
        vesselUseFeatures.put(caracteristic, value);
    }

    public void addMarineLitterBatch(MarineLitterBatch marineLitterBatch) {
        marineLitterBatches.add(marineLitterBatch);
    }

    public void addAccidentalBatch(Integer batchId, AccidentalBatch accidentalBatch) {
        accidentalBatchesById.put(batchId, accidentalBatch);
    }

    public void addIndividualObservationBatch(Integer batchId, IndividualObservationBatch individualObservationBatch) {
        individualObservationBatchesById.put(batchId, individualObservationBatch);
    }

    public void addSpeciesBatch(boolean vrac, SpeciesBatch speciesBatch) {
        getSpeciesBatchMap(vrac).put(speciesBatch.getSpecies().getReferenceTaxonId(), speciesBatch);
    }

    public void addBenthosBatch(boolean vrac, SpeciesBatch benthosBatch) {
        getBenthosBatchMap(vrac).put(benthosBatch.getSpecies().getReferenceTaxonId(), benthosBatch);
    }

    public void addSpeciesFrequency(SpeciesBatch batch, SpeciesBatchFrequency frequency) {
        speciesFrequencies.put(batch.getIdAsInt(), frequency);
    }

    public void addBenthosFrequency(SpeciesBatch batch, SpeciesBatchFrequency frequency) {
        benthosFrequencies.put(batch.getIdAsInt(), frequency);
    }

    public void addCheckErrors(Set<String> checkErrors) {
        this.checkErrors.addAll(checkErrors);
    }

    public Collection<MarineLitterBatch> getMarineLitterBatches() {
        return ImmutableList.copyOf(marineLitterBatches);
    }

    public Collection<AccidentalBatch> getAccidentalBatches() {
        return ImmutableList.copyOf(accidentalBatchesById.values());
    }

    public ImmutableList<IndividualObservationBatch> getIndividualObservationBatches() {
        return ImmutableList.copyOf(individualObservationBatchesById.values());
    }

    public List<IndividualObservationBatch> getIndividualObservationBatchImported() {
        return individualObservationBatchImported;
    }

    public void setIndividualObservationBatchImported(List<IndividualObservationBatch> imported) {
        individualObservationBatchImported = imported;
    }

    public int getNbSpeciesTaxon() {
        Set<Species> speciesSet = new HashSet<>();
        SpeciesBatchs.grabSpeciesChildBatchs(vracSpeciesBatches.values(), speciesSet);
        SpeciesBatchs.grabSpeciesChildBatchs(horsVracSpeciesBatches.values(), speciesSet);
        return speciesSet.size();
    }

    public int getNbBenthosTaxon() {
        Set<Species> speciesSet = new HashSet<>();
        SpeciesBatchs.grabSpeciesChildBatchs(vracBenthosBatches.values(), speciesSet);
        SpeciesBatchs.grabSpeciesChildBatchs(horsVracBenthosBatches.values(), speciesSet);
        return speciesSet.size();
    }

    public Collection<SpeciesBatch> getSpeciesBatches(boolean vrac) {
        return ImmutableList.copyOf(getSpeciesBatchMap(vrac).values());
    }

    public Collection<SpeciesBatch> getBenthosBatches(boolean vrac) {
        return ImmutableList.copyOf(getBenthosBatchMap(vrac).values());
    }

    public List<SpeciesBatchFrequency> getBenthosFrequencies(SpeciesBatch benthosBatch) {
        return benthosFrequencies.get(benthosBatch.getIdAsInt());
    }

    public List<SpeciesBatchFrequency> getSpeciesFrequencies(SpeciesBatch speciesBatch) {
        return speciesFrequencies.get(speciesBatch.getIdAsInt());
    }

    public CaracteristicMap getGearUseFeatures() {
        return gearUseFeatures;
    }

    public CaracteristicMap getVesselUseFeatures() {
        return vesselUseFeatures;
    }

    private Map<Integer, SpeciesBatch> getSpeciesBatchMap(boolean vrac) {
        return vrac ? vracSpeciesBatches : horsVracSpeciesBatches;
    }

    private Map<Integer, SpeciesBatch> getBenthosBatchMap(boolean vrac) {
        return vrac ? vracBenthosBatches : horsVracBenthosBatches;
    }

    public Set<String> getCheckErrors() {
        return checkErrors;
    }

    @Override
    public void close() {

        gearUseFeatures.clear();
        vesselUseFeatures.clear();
        marineLitterBatches.clear();
        individualObservationBatchesById.clear();
        individualObservationBatchImported.clear();
        accidentalBatchesById.clear();
        benthosFrequencies.clear();
        vracBenthosBatches.clear();
        horsVracBenthosBatches.clear();
        speciesFrequencies.clear();
        vracSpeciesBatches.clear();
        horsVracSpeciesBatches.clear();
        batchesObjectIds.clear();
        speciesBatchIds.clear();

    }

    public void registerBatchObjectId(Integer batchId, Integer objectId) {
        if (objectId != null) {
            batchesObjectIds.put(batchId, objectId);
        }
    }

    public Integer getBatchObjectId(Integer batchId) {
        return batchesObjectIds.get(batchId);
    }


    public void registerPersistedSpeciesBatchId(Integer importBatchId, Integer persistBatchId) {
        speciesBatchIds.put(importBatchId, persistBatchId);
    }

    public Integer getSpeciesBatchId(Integer importBatchId) {
        return speciesBatchIds.get(importBatchId);
    }

    public void setWeightsDeleted(boolean weightsDeleted) {
        this.weightsDeleted = weightsDeleted;
    }

    public boolean isWeightsDeleted() {
        return weightsDeleted;
    }

}

