package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import org.nuiton.csv.ValueFormatter;

/**
 * Created on 2/8/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ProgramZoneFormatter implements ValueFormatter<TuttiLocation> {

    @Override
    public String format(TuttiLocation value) {
        String result = "";
        if (value != null) {
            result = TuttiLocations.GET_NAME.apply(value);
        }
        return result;
    }
}