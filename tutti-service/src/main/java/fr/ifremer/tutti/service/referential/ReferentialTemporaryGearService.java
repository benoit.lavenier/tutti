package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporaryGear;
import fr.ifremer.tutti.service.referential.csv.GearRow;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporaryGear;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class ReferentialTemporaryGearService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ReferentialTemporaryGearService.class);

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
    }

    public ReferentialImportRequest<Gear, Integer> createReferentialImportRequest() {

        List<Gear> allGears = Lists.newArrayList(persistenceService.getAllGear());
        return new ReferentialImportRequest<>(allGears, TuttiEntities.<Gear>newIdAstIntFunction(), Gears.GET_NAME);

    }

    public ReferentialImportResult<Gear> importTemporaryGear(File file) {

        if (log.isInfoEnabled()) {
            log.info("Will import gears from file: " + file);
        }

        ReferentialImportRequest<Gear, Integer> requestResult = createReferentialImportRequest();

        try (CsvConsumerForTemporaryGear consumer = new CsvConsumerForTemporaryGear(file.toPath(), getCsvSeparator(), true)) {

            for (ImportRow<GearRow> bean : consumer) {

                consumer.checkRow(bean, persistenceService, decoratorService, requestResult);

            }

        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.import.gears.error", file), e);
        }

        return executeImportRequest(requestResult);

    }


    public ReferentialImportResult<Gear> executeImportRequest(ReferentialImportRequest<Gear, Integer> requestResult) {

        ReferentialImportResult<Gear> result = new ReferentialImportResult<>();

        if (requestResult.withEntitiesToDelete()) {

            List<Integer> idsToDelete = requestResult.getIdsToDelete();
            persistenceService.deleteTemporaryGears(idsToDelete);
            result.setNbRefDeleted(idsToDelete.size());

        }

        if (requestResult.withEntitiesToAdd()) {

            List<Gear> entitiesToAdd = requestResult.getEntitiesToAdd();
            List<Gear> entitiesAdded = persistenceService.addTemporaryGears(entitiesToAdd);
            result.addAllRefsAdded(entitiesAdded);

        }

        if (requestResult.withEntitiesToUpdate()) {

            List<Gear> entitiesToUpdate = requestResult.getEntitiesToUpdate();
            List<Gear> entitiesUpdated = persistenceService.updateTemporaryGears(entitiesToUpdate);
            result.addAllRefsUpdated(entitiesUpdated);

        }

        if (requestResult.withEntitiesToLink()) {

            List<Gear> entitiesToLink = requestResult.getEntitiesToLink();
            List<Gear> entitiesLinked = persistenceService.linkTemporaryGears(entitiesToLink);
            result.addAllRefsLinked(entitiesLinked);

        }

        return result;

    }

    public void exportExistingTemporaryGear(File file) throws IOException {

        List<Gear> toExport = getTemporaryGears();
        exportTemporaryGear(file, toExport);

    }

    public List<Gear> getTemporaryGears() {
        if (log.isInfoEnabled()) {
            log.info("Getting all gears from database");
        }
        List<Gear> targetList = Lists.newArrayList(persistenceService.getAllGear());
        if (log.isInfoEnabled()) {
            log.info("Got " + targetList.size() + " gears");
        }
        List<Gear> toExport = persistenceService.retainTemporaryGearList(targetList);
        if (log.isInfoEnabled()) {
            log.info("Got " + toExport.size() + " temporary gears");
        }
        return toExport;
    }

    public void exportTemporaryGearExample(File file) throws IOException {

        List<Gear> toExport = Lists.newArrayList();

        {
            Gear g = Gears.newGear();
            g.setName("Gear fishing name 1");
            g.setLabel("Gear fishing label 1");
            toExport.add(g);
        }
        {
            Gear g = Gears.newGear();
            g.setName("Gear fishing name 2");
            g.setLabel("Gear fishing label 2");
            toExport.add(g);
        }
        {
            Gear g = Gears.newGear();
            g.setName("Gear scientific name 3");
            g.setLabel("Gear scientific label 3");
            g.setScientificGear(true);
            toExport.add(g);
        }
        {
            Gear g = Gears.newGear();
            g.setName("Gear scientific name 4");
            g.setLabel("Gear scientific label 4");
            g.setScientificGear(true);
            toExport.add(g);
        }
        exportTemporaryGear(file, toExport);

    }

    public void exportTemporaryGear(File file, List<Gear> toExport) throws IOException {

        try (CsvProducerForTemporaryGear producerForTemporarySpecies = new CsvProducerForTemporaryGear(file.toPath(), getCsvSeparator())) {

            List<GearRow> dataToExport = producerForTemporarySpecies.getDataToExport(toExport);
            producerForTemporarySpecies.write(dataToExport);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.export.gear.error", file), e);
        }

    }

    protected char getCsvSeparator() {
        return ';';
    }
}
