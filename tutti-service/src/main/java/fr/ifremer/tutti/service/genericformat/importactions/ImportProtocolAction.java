package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.genericformat.GenericFormatArchive;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatFileResult;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportProtocolAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportProtocolAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportProtocolAction(GenericFormatContextSupport importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.getReferentialTemporarySpeciesFileResult().isValid()
               && importContext.getProtocolFileResult().isFound();
    }

    @Override
    protected void skipExecute() {

        if (log.isInfoEnabled()) {
            log.info("Skip import protocol (no file found or archive is not valid).");
        }

        importContext.increments(t("tutti.service.genericFormat.skip.import.protocol"));

    }

    @Override
    protected void doExecute() {

        GenericFormatArchive archive = importContext.getImportRequest().getArchive();

        if (log.isInfoEnabled()) {
            log.info("Import protocol.");
        }
        GenericFormatFileResult fileResult = importContext.getProtocolFileResult();
        fileResult.setImported(true);

        try {

            TuttiProtocol tuttiProtocol = persistenceHelper.importProtocol(archive.getProtocolPath().toFile());

            // get id translations
            Map<Integer, Integer> referenceTaxonIdMap = importContext.getReferentialTemporarySpeciesFileResult().getReferenceTaxonIdTranslationMap();

            TuttiProtocols.translateReferenceTaxonIds(tuttiProtocol, referenceTaxonIdMap);

            String protocolOriginalName = tuttiProtocol.getName();

            String newName = persistenceHelper.getProtocolFirstAvailableName(protocolOriginalName);
            tuttiProtocol.setName(newName);

            List<Species> referentSpecies = persistenceHelper.getAllReferentSpecies();

            // Check missing species
            Map<Integer, String> missingSpecies = TuttiProtocols.detectMissingSpecies(tuttiProtocol, referentSpecies);
            if (!missingSpecies.isEmpty()) {

                String message = TuttiProtocols.getBadSpeciesMessage(missingSpecies);
                fileResult.addGlobalError(message);
            }

            // Check missing benthos
            Map<Integer, String> missingBenthos = TuttiProtocols.detectMissingBenthos(tuttiProtocol, referentSpecies);
            if (!missingBenthos.isEmpty()) {

                String message = TuttiProtocols.getBadBenthosMessage(missingBenthos);
                fileResult.addGlobalError(message);

            }

            // TODO Check lenghtClassPmfm ?

            if (fileResult.isValid()) {

                importContext.increments(t("tutti.service.genericFormat.import.protocol", protocolOriginalName));

                tuttiProtocol = persistenceHelper.createProtocol(tuttiProtocol);

                importContext.setImportedProtocol(tuttiProtocol);
                importContext.setProtocolOriginalName(protocolOriginalName);

            } else {

                importContext.increments(t("tutti.service.genericFormat.import.protocol.notValid", protocolOriginalName));

            }

        } catch (ApplicationTechnicalException e) {

            fileResult.addGlobalError(e.getCause().getMessage());

        }

    }
}
