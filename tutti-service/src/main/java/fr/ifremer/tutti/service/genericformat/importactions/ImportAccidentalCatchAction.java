package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForAccidentalCatch;
import fr.ifremer.tutti.service.genericformat.csv.AccidentalCatchRow;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportAccidentalCatchAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportAccidentalCatchAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportAccidentalCatchAction(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid()
               && importContext.getSurveyFileResult().isValid()
               && importContext.getOperationFileResult().isValid()
               && importContext.getImportRequest().isImportAccidentalCatch();
    }

    @Override
    protected void skipExecute() {

        importContext.increments(t("tutti.service.genericFormat.skip.import.accidentalCatches"));

        if (!importContext.getImportRequest().isImportAccidentalCatch()) {
            GenericFormatCsvFileResult importFileResult = importContext.getAccidentalCatchFileResult();
            importFileResult.setSkipped(true);
        }

    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Import accidentalCatch.csv file.");
        }
        importContext.increments(t("tutti.service.genericFormat.import.accidentalCatches"));
        GenericFormatCsvFileResult importFileResult = importContext.getAccidentalCatchFileResult();
        try (CsvConsumerForAccidentalCatch consumer = importContext.loadAccidentalCatches(true)) {
            for (ImportRow<AccidentalCatchRow> row : consumer) {

                GenericFormatImportOperationContext operationContext = consumer.validateRow(row, importContext);
                if (operationContext != null) {
                    consumer.prepareRowForPersist(operationContext, row);
                }

            }
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close accidentalCatch.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        persistAccidentalBatches();

    }

    private void persistAccidentalBatches() {

        importContext.doActionOnCruiseContexts(new GenericFormatImportContext.CruiseContextAction() {

            @Override
            public void onCruise(GenericFormatImportCruiseContext cruiseContext, ProgressionModel progressionModel) {

                for (GenericFormatImportOperationContext fishingOperationContext : cruiseContext) {

                    boolean override = fishingOperationContext.isOverride();

                    if (override) {

                        deleteAccidentalBatches(fishingOperationContext);

                    }

                    String cruiseStr = cruiseContext.getCruiseLabel();
                    String operationStr = fishingOperationContext.getFishingOperationLabel();

                    importContext.increments(t("tutti.service.genericFormat.persist.operation.accidentalBatches", cruiseStr, operationStr));

                    if (fishingOperationContext.withAccidentalBatches()) {

                        Collection<AccidentalBatch> accidentalBatches = fishingOperationContext.getAccidentalBatches();
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + accidentalBatches.size() + " accidental batch(es) of " + operationStr + " for cruise: " + cruiseStr);
                        }
                        persistAccidentalBatches(accidentalBatches);

                    }

                }

            }

            private void deleteAccidentalBatches(GenericFormatImportOperationContext fishingOperationContext) {

                FishingOperation fishingOperation = fishingOperationContext.getFishingOperation();
                persistenceHelper.deleteAccidentalBatchForFishingOperation(fishingOperation.getIdAsInt());

            }

            private void persistAccidentalBatches(Collection<AccidentalBatch> batches) {

                for (AccidentalBatch accidentalBatch : batches) {

                    Integer objectId = accidentalBatch.getIdAsInt();
                    Collection<AttachmentRow> attachmentRows = importContext.popAttachmentRows(ObjectTypeCode.SAMPLE, objectId);

                    // Id was only here to get attachments
                    accidentalBatch.setId((String) null);

                    AccidentalBatch createdAccidentalBatch = persistenceHelper.createAccidentalBatch(accidentalBatch);
                    persistenceHelper.persistAttachments(createdAccidentalBatch.getIdAsInt(), attachmentRows);

                }

            }

        });

    }

}
