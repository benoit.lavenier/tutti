package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.opensymphony.xwork2.util.ValueStack;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;
import org.nuiton.validator.xwork2.XWork2ValidatorUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Data to shared by validators.
 *
 * <strong>Be ware some of this methods are used in validation files, so do NOT remove them, even if your IDE tells you they are not used, they are!</strong>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class TuttiValidationDataContextSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiValidationDataContextSupport.class);

    private List<Program> existingPrograms = Lists.newArrayList();

    private List<TuttiProtocol> existingProtocols = Lists.newArrayList();

    private List<FishingOperation> existingFishingOperations = Lists.newArrayList();

    public static TuttiValidationDataContextSupport setValidationContext(TuttiValidationDataContextSupport validationDataContext, boolean removePrevious) {

        ValueStack sharedValueStack = XWork2ValidatorUtil.getSharedValueStack();

        TuttiValidationDataContextSupport previousValidationContext = null;
        if (removePrevious) {
            List toRepush = new ArrayList();
            do {

                Object o = sharedValueStack.pop();

                if (log.isDebugEnabled()) {
                    log.debug("Pop object: " + o);
                }
                if (o instanceof TuttiValidationDataContextSupport) {
                    previousValidationContext = (TuttiValidationDataContextSupport) o;
                } else {
                    toRepush.add(0, o);
                }

            } while (previousValidationContext == null || sharedValueStack.size() > 0);

            for (Object o : toRepush) {

                if (log.isDebugEnabled()) {
                    log.debug("Push object: " + o);
                }
                sharedValueStack.push(o);
            }

        }
        sharedValueStack.push(validationDataContext);

        return previousValidationContext;
    }

    protected abstract List<Program> loadExistingPrograms();

    protected abstract List<TuttiProtocol> loadExistingProtocols();

    protected abstract List<FishingOperation> loadExistingFishingOperations();

    protected abstract Program getProgram();

    protected abstract Cruise getCruise();

    protected abstract TuttiProtocol getProtocol();

    protected abstract FishingOperation getFishingOperation();

    public final List<Program> getExistingPrograms() {
        if (existingPrograms == null) {

            existingPrograms = loadExistingPrograms();
        }
        return existingPrograms;
    }

    public final List<TuttiProtocol> getExistingProtocols() {
        if (existingProtocols == null) {
            existingProtocols = loadExistingProtocols();
        }
        return existingProtocols;
    }

    public final List<FishingOperation> getExistingFishingOperations() {
        if (existingFishingOperations == null) {
            existingFishingOperations = loadExistingFishingOperations();
        }
        return existingFishingOperations;
    }

    public final boolean isDateInCruise(Date date) {
        return DateUtil.between(date, getCruise().getBeginDate(), getCruise().getEndDate());
    }

    public final boolean isNotWeightZeroValue(Float weight) {
        return WeightUnit.KG.isNotNullNorZero(weight);
    }

    public final boolean isMutiRegNumberValid(String numberAsString) {
        boolean result = false;
        if (numberAsString.matches("\\d+")) {
            int mutlirigNumber = Integer.valueOf(numberAsString);
            Integer cruiseMultirigNumber = getCruise().getMultirigNumber();
            result = mutlirigNumber <= cruiseMultirigNumber;
        }
        return result;
    }

    public final boolean isValidDuration(Date gearShootingStartDate, Date gearShootingEndDate) {
        boolean result = gearShootingStartDate == null || gearShootingEndDate == null;
        if (!result) {
            int minutes = DateUtil.getDifferenceInMinutes(gearShootingStartDate, gearShootingEndDate);
            result = minutes <= 45 && minutes >= 20;
        }
        return result;
    }

    public void reset() {
        resetExistingPrograms();
        resetExistingProtocols();
        resetExistingFishingOperations();
    }

    public void resetExistingPrograms() {
        existingPrograms = null;
    }

    public void resetExistingProtocols() {
        existingProtocols = null;
    }

    public void resetExistingFishingOperations() {
        existingFishingOperations = null;
    }
}
