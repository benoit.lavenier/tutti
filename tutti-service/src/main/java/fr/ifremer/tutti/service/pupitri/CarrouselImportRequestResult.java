package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.lang3.mutable.MutableFloat;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 11/21/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class CarrouselImportRequestResult {

    private final ListMultimap<String, Species> speciesByCode;

    /**
     * Nombre d'espèces importées depuis le fichier carrousel.
     */
    private int nbCarrousselImported;

    /**
     * Poids total du VRAC cumulé depuis le fichier carrousel.
     */
    private MutableFloat carrouselSortedWeight;

    /**
     * Poids total du HORS-VRAC cumulé depuis le fichier carrousel.
     */
    private MutableFloat carrouselUnsortedWeight;

    /**
     * Ensemble de identifiants d'espèces non importés depuis el fichier carrousel.
     */
    private final Set<String> notImportedSpeciesIds;

    /**
     * Liste des lots lus depuis le fichier carrousel. Chaque lot est un tuple (espece,trie).
     */
    private final List<PupitriSpeciesContext> catches;

    public CarrouselImportRequestResult(ListMultimap<String, Species> speciesByCode) {

        this.speciesByCode = speciesByCode;
        this.notImportedSpeciesIds = new LinkedHashSet<>();
        this.catches = Lists.newArrayList();
    }

    public void incrementNbCarrousselImported() {
        nbCarrousselImported++;
    }

    public void addCarrouselSortedWeight(Float beanWeight) {
        if (carrouselSortedWeight == null) {
            carrouselSortedWeight = new MutableFloat();
        }
        if (beanWeight != null) {

            carrouselSortedWeight.add(beanWeight);
        }
    }

    public void addCarrouselUnsortedWeight(Float beanWeight) {
        if (carrouselUnsortedWeight == null) {
            carrouselUnsortedWeight = new MutableFloat();
        }
        if (beanWeight != null) {

            carrouselUnsortedWeight.add(beanWeight);
        }
    }

    public List<Species> getSpecies(String speciesId) {
        return speciesByCode.get(speciesId);
    }

    public void addNotImportedSpeciesId(String speciesId) {
        notImportedSpeciesIds.add(speciesId);
    }

    public PupitriSpeciesContext getOrCreateCatch(List<Species> speciesList,
                                                  boolean createMissingSigns,
                                                  boolean sorted) {

        // on utilise la première espèce trouvée dans la liste des candidates
        //FIXME Bien s'assurer que cela est ok
        Species species = speciesList.get(0);
        PupitriSpeciesContext pupitriSpeciesContext = new PupitriSpeciesContext(species,
                                                                                createMissingSigns,
                                                                                sorted);
        int catchIndex = catches.indexOf(pupitriSpeciesContext);
        if (catchIndex >= 0) {
            pupitriSpeciesContext = catches.get(catchIndex);
        } else {
            catches.add(pupitriSpeciesContext);
        }
        return pupitriSpeciesContext;

    }

    public float getCarrouselSortedWeight() {
        return carrouselSortedWeight == null ? 0f : carrouselSortedWeight.floatValue();
    }

    public float getCarrouselUnsortedWeight() {
        return carrouselUnsortedWeight == null ? 0f : carrouselUnsortedWeight.floatValue();
    }

    public int getNbCarrousselImported() {
        return nbCarrousselImported;
    }

    public Set<String> getNotImportedSpeciesIds() {
        return notImportedSpeciesIds;
    }

    public List<PupitriSpeciesContext> getCatches() {
        return catches;
    }
}
