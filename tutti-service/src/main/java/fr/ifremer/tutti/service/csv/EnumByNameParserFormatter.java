package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParserFormatter;

import java.text.ParseException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class EnumByNameParserFormatter<E extends Enum<E>> implements ValueParserFormatter<E> {


    private final Class<E> enumType;

    private final boolean mandatory;

    public EnumByNameParserFormatter(Class<E> enumType, boolean mandatory) {
        this.enumType = enumType;
        this.mandatory = mandatory;
    }

    @Override
    public E parse(String value) throws ParseException {
        E result;
        if (StringUtils.isBlank(value)) {
            result = null;
        } else {
            result = Enum.valueOf(enumType, value);
        }
        if (mandatory && result == null) {
            throw new NullPointerException(t("tutti.service.cvs.mandatory.value"));
        }
        return result;
    }

    @Override
    public String format(E date) {
        String value = "";
        if (date != null) {
            value = date.name();
        }
        return value;
    }
}
