package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;

/**
 * Model of a marine litter export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.1
 */
public class MarineLitterModel extends AbstractTuttiImportExportModel<MarineLitterRow> {

    public static MarineLitterModel forExport(char separator) {

        MarineLitterModel exportModel = new MarineLitterModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static MarineLitterModel forImport(char separator, GenericFormatImportEntityParserFactory parserFactory) {

        MarineLitterModel importModel = new MarineLitterModel(separator);
        importModel.forImport(parserFactory);
        return importModel;

    }

    @Override
    public MarineLitterRow newEmptyInstance() {

        return MarineLitterRow.newEmptyInstance();

    }

    protected MarineLitterModel(char separator) {
        super(separator);
    }

    protected void forExport() {

        newColumnForExport("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newColumnForExport("Serie", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_FORMATTER);
        newColumnForExport("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newColumnForExport("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newColumnForExport("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newColumnForExport("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);

        newColumnForExport("Categorie", MarineLitterBatch.PROPERTY_MARINE_LITTER_CATEGORY, TuttiCsvUtil.CARACTERISTIC_VALUE_FORMATTER);
        newColumnForExport("Categorie_Taille", MarineLitterBatch.PROPERTY_MARINE_LITTER_SIZE_CATEGORY, TuttiCsvUtil.CARACTERISTIC_VALUE_FORMATTER);
        newColumnForExport("Nombre", MarineLitterBatch.PROPERTY_NUMBER, TuttiCsvUtil.INTEGER);
        newColumnForExport("Poids", MarineLitterBatch.PROPERTY_WEIGHT, TuttiCsvUtil.FLOAT);
        newColumnForExport("Commentaire", MarineLitterBatch.PROPERTY_COMMENT, TuttiCsvUtil.COMMENT_PARSER_FORMATTER);

        newColumnForExport("Serie_Id", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_TECHNICAL_FORMATTER);
        newColumnForExport("Lot_Id", MarineLitterRow.PROPERTY_MARINE_LITTER_ID, TuttiCsvUtil.INTEGER);
        newColumnForExport("Categorie_Id", MarineLitterBatch.PROPERTY_MARINE_LITTER_CATEGORY, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
        newColumnForExport("Categorie_Taille_Id", MarineLitterBatch.PROPERTY_MARINE_LITTER_SIZE_CATEGORY, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);

    }

    protected void forImport(GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newIgnoredColumn("Serie");
        newMandatoryColumn("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newMandatoryColumn("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newMandatoryColumn("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);

        newIgnoredColumn("Categorie");
        newIgnoredColumn("Categorie_Taille");
        newMandatoryColumn("Nombre", MarineLitterBatch.PROPERTY_NUMBER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Poids", MarineLitterBatch.PROPERTY_WEIGHT, TuttiCsvUtil.FLOAT);
        newMandatoryColumn("Commentaire", MarineLitterBatch.PROPERTY_COMMENT, TuttiCsvUtil.COMMENT_PARSER_FORMATTER);

        newMandatoryColumn("Serie_Id", Cruise.PROPERTY_PROGRAM, parserFactory.getProgramParser());
        newMandatoryColumn("Lot_Id", MarineLitterRow.PROPERTY_MARINE_LITTER_ID, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Categorie_Id", MarineLitterBatch.PROPERTY_MARINE_LITTER_CATEGORY, parserFactory.getMarineLitterCategoryValueParser());
        newMandatoryColumn("Categorie_Taille_Id", MarineLitterBatch.PROPERTY_MARINE_LITTER_SIZE_CATEGORY, parserFactory.getMarineLitterSizeCategoryValueParser());

    }

}
