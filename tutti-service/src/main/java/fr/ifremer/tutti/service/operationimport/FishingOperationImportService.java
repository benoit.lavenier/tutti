package fr.ifremer.tutti.service.operationimport;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.protocol.ProtocolCaracteristicsImportExportService;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportConf;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 12/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class FishingOperationImportService extends AbstractTuttiService {

    protected PersistenceService persistenceService;

    private TuttiDataContext dataContext;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        dataContext = context.getDataContext();
    }

    public void importCaracteristicsFromColumnFile(File columnsFile, FishingOperation fishingoperation) throws ImportFromColumnFileInvalidRowException, ImportFromColumnFileFishingOperationNotFoundException, ImportFromColumnFileMissingHeaderException, IOException {

        // Get import file headers
        List<String> headers =getService(ProtocolCaracteristicsImportExportService.class).loadProtocolCaracteristicsImportColumns(columnsFile);

        // Create import model
        ImportFromColumnFileModel importModel = new ImportFromColumnFileModel(';', new HashSet<>(headers), persistenceService, dataContext, fishingoperation);
        ImportConf conf = new ImportConf();
        conf.setStrictMode(false);

        // Create matching row predicate
        MatchingFishingOperationNaturalIdPredicate matchingRowPredicate = new MatchingFishingOperationNaturalIdPredicate(
                fishingoperation.getStationNumber(),
                fishingoperation.getFishingOperationNumber(),
                fishingoperation.getGearShootingStartDate(),
                dataContext.getProtocol());

        try (Import2<FishingOperation> operationUIModelImport = Import2.newImport(conf, importModel, new FileInputStream(columnsFile))) {

            doImport(operationUIModelImport, matchingRowPredicate);

        } catch (FileNotFoundException e) {
            throw new ApplicationTechnicalException("Could not find import file " + columnsFile);
        }

    }

    protected void doImport(Import2<FishingOperation> operationUIModelImport,
                            MatchingFishingOperationNaturalIdPredicate matchingRowPredicate) throws ImportFromColumnFileInvalidRowException, ImportFromColumnFileFishingOperationNotFoundException {


        boolean found = false;
        for (ImportRow<FishingOperation> next : operationUIModelImport) {

            FishingOperation fishingOperation = next.getBean();

            if (matchingRowPredicate.apply(fishingOperation)) {
                if (!next.isValid()) {

                    Set<AbstractImportErrorInfo<FishingOperation>> errors = next.getErrors();
                    throw new ImportFromColumnFileInvalidRowException(errors);

                }

                // valid row was found
                // can quit right now the import
                found = true;

                break;
            }

        }

        if (!found) {
            throw new ImportFromColumnFileFishingOperationNotFoundException();
        }

    }

    protected static class MatchingFishingOperationNaturalIdPredicate implements Predicate<FishingOperation> {

        protected final String stationNumber;

        protected final Integer fishingOperationNumber;

        protected final Date modelGearShootingStartOnlyDate;

        protected final Map<String, OperationFieldMappingRow> mappingRowsByField;

        MatchingFishingOperationNaturalIdPredicate(String stationNumber,
                                                   Integer fishingOperationNumber,
                                                   Date modelGearShootingStartDate,
                                                   TuttiProtocol protocol) {
            this.stationNumber = stationNumber;
            this.fishingOperationNumber = fishingOperationNumber;

            if (modelGearShootingStartDate != null) {
                this.modelGearShootingStartOnlyDate = DateUtil.getDay(modelGearShootingStartDate);
            } else {
                this.modelGearShootingStartOnlyDate = null;
            }

            this.mappingRowsByField = Maps.uniqueIndex(protocol.getOperationFieldMapping(), OperationFieldMappingRow::getField);
        }

        @Override
        public boolean apply(FishingOperation fishingOperation) {

            OperationFieldMappingRow operationNumberRow = mappingRowsByField.get(FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER);
            OperationFieldMappingRow stationNumberRow = mappingRowsByField.get(FishingOperation.PROPERTY_STATION_NUMBER);
            OperationFieldMappingRow startDateRow = mappingRowsByField.get(FishingOperation.PROPERTY_GEAR_SHOOTING_START_DATE);

            String operationNumberColumn = operationNumberRow != null ? operationNumberRow.getImportColumn() : null;
            String stationColumn = stationNumberRow != null ? stationNumberRow.getImportColumn() : null;
            String startDateColumn = startDateRow != null ? startDateRow.getImportColumn() : null;

            boolean sameStationNumber = Objects.equals(fishingOperation.getStationNumber(), stationNumber);
            boolean sameFishingOperationNumber = Objects.equals(fishingOperation.getFishingOperationNumber(), fishingOperationNumber);
            // check if the dates (not time) are equals, but push the time to the model
            Date gearShootingStartDate = fishingOperation.getGearShootingStartDate();
            if (gearShootingStartDate != null) {
                gearShootingStartDate = DateUtil.getDay(gearShootingStartDate);
            }
            boolean sameGearShootingStartDate = Objects.equals(modelGearShootingStartOnlyDate, gearShootingStartDate);

            return (StringUtils.isBlank(stationColumn) || sameStationNumber)
                   && (StringUtils.isBlank(operationNumberColumn) || sameFishingOperationNumber)
                   && (StringUtils.isBlank(startDateColumn) || sameGearShootingStartDate);
        }
    }

}
