package fr.ifremer.tutti.service.export;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import org.apache.commons.lang3.mutable.MutableInt;

import java.util.Map;
import java.util.TreeMap;

/**
 * To store a species or batch entry within his species informations,
 * his sorted weight, total weight and optional his total number.
 *
 * @since 3.0-rc-1
 */
public class ExportBatchEntry {

    protected final SpeciesBatch batch;

    protected float sortedWeight;

    protected float totalWeight;

    protected int totalNumber;

    /**
     * Frequencies (key = step, value = number).
     *
     * @since 3.3
     */
    protected Map<Float, MutableInt> frequencies;

    public ExportBatchEntry(SpeciesBatch batch) {
        this.batch = batch;
        this.frequencies = new TreeMap<>();
    }

    public SpeciesBatch getBatch() {
        return batch;
    }

    public void addSortedWeight(float weight) {
        sortedWeight += weight;
    }

    public void addTotalWeight(float weight) {
        totalWeight += weight;
    }

    public void addNumber(int number) {
        totalNumber += number;
    }

    public void addFrequency(float lengthStep, int number) {
        MutableInt totalNumber = frequencies.get(lengthStep);
        if (totalNumber == null) {
            totalNumber = new MutableInt();
            frequencies.put(lengthStep, totalNumber);
        }
        totalNumber.add(number);
    }

    public float getSortedWeight() {
        return sortedWeight;
    }

    public float getTotalWeight() {
        return totalWeight;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public Float getAverageFrequency() {
        int totNumber = 0;
        float totSize = 0;
        for (Map.Entry<Float, MutableInt> entry : frequencies.entrySet()) {
            float size = entry.getKey();
            int number = entry.getValue().intValue();
            totSize += (size * number);
            totNumber += number;
        }
        return totNumber == 0 ? null : totSize / (float) totNumber;
    }
}
