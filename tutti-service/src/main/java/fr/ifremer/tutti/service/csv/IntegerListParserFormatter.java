package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParserFormatter;

import java.text.ParseException;
import java.util.List;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class IntegerListParserFormatter implements ValueParserFormatter<List<Integer>> {

    @Override
    public String format(List<Integer> value) {
        List<String> decoratedValues = Lists.transform(value, Object::toString);
        return Joiner.on('|').join(decoratedValues);
    }

    @Override
    public List<Integer> parse(String value) throws ParseException {
        List<Integer> result = Lists.newArrayList();
        if (StringUtils.isNotBlank(value)) {
            String[] split = value.split("\\s*\\|\\s*");
            for (String string : split) {
                result.add(Integer.valueOf(string));
            }
        }
        return result;
    }
}
