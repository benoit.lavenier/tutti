package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatArchive implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatArchive.class);

    private enum ArchiveMode {
        IMPORT,
        EXPORT
    }

    private enum ArchiveFilePath {

        PROTOCOL(false, "protocol.tuttiProtocol"),
        REFERENTIAL_GEAR(false, "temporaryGears.csv"),
        REFERENTIAL_PERSON(false, "temporaryPersons.csv"),
        REFERENTIAL_SPECIES(false, "temporarySpecies.csv"),
        REFERENTIAL_VESSEL(false, "temporaryVessels.csv"),
        SAMPLE_CATEGORY(true, "sampleCategory.csv"),
        DATA_SURVEY(true, "survey.csv"),
        DATA_GEAR_CARACTERISTIC(true, "gearCaracteristics.csv"),
        DATA_OPERATION(true, "operation.csv"),
        DATA_PARAMETER(true, "parameter.csv"),
        DATA_CATCH(true, "catch.csv"),
        DATA_SPECIES(false, "species.csv"),
        DATA_MARINE_LITTER(true, "marineLitter.csv"),
        DATA_ACCIDENTAL_CATCH(true, "accidentalCatch.csv"),
        DATA_INDIVIDUAL_OBSERVATION(true, "individualObservation.csv"),
        DATA_ATTACHMENTS(true, "attachments.csv");

        private final boolean mandatory;

        private final String filename;

        ArchiveFilePath(boolean mandatory, String filename) {
            this.mandatory = mandatory;
            this.filename = filename;
        }

        public boolean isMandatory() {
            return mandatory;
        }

        public String getFilename() {
            return filename;
        }

    }

    private final File archiveFile;

    private final File workingDirectory;

    private final ArchiveMode archiveMode;

    private final EnumMap<ArchiveFilePath, Integer> countLines;

    private final EnumSet<ArchiveFilePath> missingPaths;

    public static GenericFormatArchive forImport(File archiveFile, File tempDirectory) {

        try {

            File workingDirectory = Files.createTempDirectory(tempDirectory.toPath(), "genericImport").toFile();
            return new GenericFormatArchive(ArchiveMode.IMPORT, archiveFile, workingDirectory);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not create generic format import archive", e);
        }

    }

    public static GenericFormatArchive forExport(File archiveFile, File tempDirectory) {

        try {

            File workingDirectory = Files.createTempDirectory(tempDirectory.toPath(), "genericExport").toFile();
            return new GenericFormatArchive(ArchiveMode.EXPORT, archiveFile, workingDirectory);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not create generic format export archive", e);
        }

    }

    public static GenericFormatArchive forExportFromWorkingDirectory(File archiveFile, File workingDirectory) {

        return new GenericFormatArchive(ArchiveMode.EXPORT, archiveFile, workingDirectory);

    }

    public File getWorkingDirectoryPath() {
        return workingDirectory;
    }

    public Path getSampleCategoryModelPath() {
        return getPath(ArchiveFilePath.SAMPLE_CATEGORY);
    }

    public Path getAttachmentFilePath() {
        return getPath(ArchiveFilePath.DATA_ATTACHMENTS);
    }

    public Path getAttachmentDataPath() {
        return workingDirectory.toPath().resolve("meas_files");
    }

    public boolean isProtocolExists() {
        return Files.exists(getProtocolPath());
    }

    public boolean isTemporaryReferentialGearsPathExists() {
        return Files.exists(getTemporaryReferentialGearsPath());
    }

    public boolean isTemporaryReferentialPersonsPathExists() {
        return Files.exists(getTemporaryReferentialPersonsPath());
    }

    public boolean isTemporaryReferentialSpeciesPathExists() {
        return Files.exists(getTemporaryReferentialSpeciesPath());
    }

    public boolean isTemporaryReferentialVesselsPathExists() {
        return Files.exists(getTemporaryReferentialVesselsPath());
    }

    public Path getProtocolPath() {
        return getPath(ArchiveFilePath.PROTOCOL);
    }

    public Path getTemporaryReferentialGearsPath() {
        return getPath(ArchiveFilePath.REFERENTIAL_GEAR);
    }

    public Path getTemporaryReferentialPersonsPath() {
        return getPath(ArchiveFilePath.REFERENTIAL_PERSON);
    }

    public Path getTemporaryReferentialSpeciesPath() {
        return getPath(ArchiveFilePath.REFERENTIAL_SPECIES);
    }

    public Path getTemporaryReferentialVesselsPath() {
        return getPath(ArchiveFilePath.REFERENTIAL_VESSEL);
    }

    public Path getSurveyPath() {
        return getPath(ArchiveFilePath.DATA_SURVEY);
    }

    public Path getGearCaracteristicsPath() {
        return getPath(ArchiveFilePath.DATA_GEAR_CARACTERISTIC);
    }

    public Path getOperationPath() {
        return getPath(ArchiveFilePath.DATA_OPERATION);
    }

    public Path getIndividualObservationPath() {
        return getPath(ArchiveFilePath.DATA_INDIVIDUAL_OBSERVATION);
    }

    public Path getSpeciesPath() {
        return getPath(ArchiveFilePath.DATA_SPECIES);
    }

    public Path getCatchPath() {
        return getPath(ArchiveFilePath.DATA_CATCH);
    }

    public Path getAccidentalCatchPath() {
        return getPath(ArchiveFilePath.DATA_ACCIDENTAL_CATCH);
    }

    public Path getParameterPath() {
        return getPath(ArchiveFilePath.DATA_PARAMETER);
    }

    public Path getMarineLitterPath() {
        return getPath(ArchiveFilePath.DATA_MARINE_LITTER);
    }

    public int getSampleCategoryLineCount() {
        return countImportLines(ArchiveFilePath.SAMPLE_CATEGORY);
    }

    public int getSurveyLineCount() {
        return countImportLines(ArchiveFilePath.DATA_SURVEY);
    }

    public int getGearCaracteristicsPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_GEAR_CARACTERISTIC);
    }

    public int getOperationPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_OPERATION);
    }

    public int getIndividualObservationPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_INDIVIDUAL_OBSERVATION);
    }

    public int getSpeciesPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_SPECIES);
    }

    public int getCatchPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_CATCH);
    }

    public int getAccidentalCatchPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_ACCIDENTAL_CATCH);
    }

    public int getAttachemntsPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_ATTACHMENTS);
    }

    public int getParameterPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_PARAMETER);
    }

    public int getMarineLitterPathLineCount() {
        return countImportLines(ArchiveFilePath.DATA_MARINE_LITTER);
    }

    public int getTemporaryReferentialGearLineCount() {
        return countImportLines(ArchiveFilePath.REFERENTIAL_GEAR);
    }

    public int getTemporaryReferentialPersonLineCount() {
        return countImportLines(ArchiveFilePath.REFERENTIAL_PERSON);
    }

    public int getTemporaryReferentialSpeciesLineCount() {
        return countImportLines(ArchiveFilePath.REFERENTIAL_SPECIES);
    }

    public int getTemporaryReferentialVesselLineCount() {
        return countImportLines(ArchiveFilePath.REFERENTIAL_VESSEL);
    }


    public void validateArchiveLayout() throws GenericFormatArchiveInvalidLayoutException {

        Set<String> errors = new LinkedHashSet<>();

        for (ArchiveFilePath archiveFilePath : ArchiveFilePath.values()) {

            if (archiveFilePath.isMandatory() && missingPaths.contains(archiveFilePath)) {

                // Missing a mandatory entry
                if (log.isErrorEnabled()) {
                    log.error("Mandatory entry " + archiveFilePath.getFilename() + " not found.");
                }
                errors.add(t("tutti.service.genericFormat.importError.missArchiveFile", archiveFilePath.getFilename()));

            }
        }

        if (!errors.isEmpty()) {

            throw new GenericFormatArchiveInvalidLayoutException(this, errors);

        }

    }

    public void createZip(ProgressionModel progressionModel) {

        if (progressionModel != null) {

            progressionModel.increments(t("tutti.service.genericFormat.export.buildZip", archiveFile));

        }

        ApplicationIOUtil.zip(workingDirectory, archiveFile, t("tutti.service.genericFormat.export.zip.error", archiveFile));

    }

    public File extractAttachment(String path) {

        Path resolve = getAttachmentDataPath().resolve(path);
        return resolve.toFile();

    }

    protected GenericFormatArchive(ArchiveMode archiveMode, File archiveFile, File workingDirectory) {

        this.archiveFile = archiveFile;
        this.archiveMode = archiveMode;
        this.countLines = new EnumMap<>(ArchiveFilePath.class);

        if (log.isInfoEnabled()) {
            log.info("Archive zip file: " + archiveFile);
            log.info("Archive working directory: " + workingDirectory);
            log.info("Archive mode: " + archiveMode);
        }
        if (isImport()) {

            this.workingDirectory = extractArchive(archiveFile, workingDirectory);
            this.missingPaths = computeMissingPaths();

        } else {

            this.workingDirectory = workingDirectory;
            this.missingPaths = null;

        }

    }

    protected File extractArchive(File archiveFile, File workingDirectory) {

        try {
            ZipUtil.uncompress(archiveFile, workingDirectory);
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not explode zipfile " + archiveFile, e);
        }
        File[] files = workingDirectory.listFiles();
        if (files == null || files.length != 1) {
            throw new ApplicationTechnicalException("Archive should contains onyl one directory");
        }
        return files[0];

    }

    protected boolean isImport() {
        return ArchiveMode.IMPORT == archiveMode;
    }

    protected boolean isExport() {
        return ArchiveMode.EXPORT == archiveMode;
    }

    protected Path getPath(ArchiveFilePath archiveFilePath) {

        String filename = archiveFilePath.getFilename();

        return workingDirectory.toPath().resolve(filename);

    }

    protected EnumSet<ArchiveFilePath> computeMissingPaths() {

        EnumSet<ArchiveFilePath> result = EnumSet.noneOf(ArchiveFilePath.class);

        for (ArchiveFilePath archiveFilePath : ArchiveFilePath.values()) {

            Path path = getPath(archiveFilePath);

            if (log.isDebugEnabled()) {
                log.debug("Check if entry " + archiveFilePath + " exists.");
            }

            if (!Files.exists(path)) {

                if (log.isInfoEnabled()) {
                    log.info("Entry " + archiveFilePath + " not found.");
                }

                result.add(archiveFilePath);

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Entry " + archiveFilePath + " found.");
                }

            }

        }

        return result;

    }

    protected int countImportLines(ArchiveFilePath archiveFilePath) {

        Integer result = countLines.get(archiveFilePath);
        if (result == null) {

            Path path = getPath(archiveFilePath);

            if (path == null) {

                result = 0;
                countLines.put(archiveFilePath, 0);

            } else {

                int nbLines = countLines(path);
                result = nbLines - 1;
                countLines.put(archiveFilePath, result);

            }

        }

        return result;

    }

    protected static int countLines(Path path) {

        try (LineNumberReader reader = new LineNumberReader(Files.newBufferedReader(path, Charset.forName("UTF-8")))) {
            String lastLine = null;
            String line;
            while ((line = reader.readLine()) != null) {
                lastLine = line;
            }
            int cnt = reader.getLineNumber();
            if (lastLine != null && lastLine.trim().isEmpty()) {
                cnt--;
            }
            return cnt;
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not read file " + path.toFile().getName(), e);
        }

    }

}
