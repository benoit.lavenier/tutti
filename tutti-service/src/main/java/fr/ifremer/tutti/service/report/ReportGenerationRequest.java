package fr.ifremer.tutti.service.report;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.Serializable;

/**
 * Request to generate a report.
 *
 * Created on 3/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class ReportGenerationRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Report to use. */
    protected File report;

    /**
     * Selected program id.
     */
    protected String programId;

    /**
     * Selected cruise id (of fishing operation).
     */
    protected Integer cruiseId;

    /**
     * Selected fishing operation to report.
     */
    protected Integer fishingOperationId;

    public File getReport() {
        return report;
    }

    public void setReport(File report) {
        this.report = report;
    }

    public String getProgramId() {
        return programId;
    }

    public void setProgramId(String programId) {
        this.programId = programId;
    }

    public Integer getCruiseId() {
        return cruiseId;
    }

    public void setCruiseId(Integer cruiseId) {
        this.cruiseId = cruiseId;
    }

    public Integer getFishingOperationId() {
        return fishingOperationId;
    }

    public void setFishingOperationId(Integer fishingOperationId) {
        this.fishingOperationId = fishingOperationId;
    }
}
