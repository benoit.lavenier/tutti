package fr.ifremer.tutti.service.psionimport;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCache;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCaches;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.DateTimes;
import fr.ifremer.tutti.util.Weights;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * To import some psion files.
 *
 * Created on 1/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class PsionImportService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(PsionImportService.class);

    protected static final ImmutableSet<String> NO_CATEGORY_VALUES = ImmutableSet.copyOf(new String[]{"N", "n"});

    protected static final ImmutableSet<String> SIZE_CATEGORY_VALUES = ImmutableSet.copyOf(new String[]{"G", "g", "P", "p"});

    protected static final ImmutableSet<String> SEX_CATEGORY_VALUES = ImmutableSet.copyOf(new String[]{"I", "i", "F", "f", "M", "m"});

    protected static final ImmutableSet<String> MATURITY_CATEGORY_VALUES = ImmutableSet.copyOf(new String[]{"1", "2", "3", "4", "5"});

    /**
     * All usables keywords in a psion import.
     *
     * Created on 1/20/14.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 3.0.1
     */
    public enum PsionImportKeyword {

        ESPE,
        POID,
        TAIL,
        CATE,
        LONG
    }

    protected PersistenceService persistenceService;

    protected CaracteristicQualitativeValue sortedCaracteristic;

    protected CaracteristicQualitativeValue unsortedCaracteristic;

    protected Map<String, CaracteristicQualitativeValue> sizeCaracteristicValues;

    protected Map<String, CaracteristicQualitativeValue> sexCaracteristicValues;

    protected Map<String, CaracteristicQualitativeValue> maturityCaracteristicValues;

    protected final DateFormat df = new SimpleDateFormat("MM-dd-yyyy");

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);

        { // sorted/unsorted caracteristic
            Caracteristic caracteristic =
                    persistenceService.getSortedUnsortedCaracteristic();

            sortedCaracteristic = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, QualitativeValueId.SORTED_VRAC.getValue());
            unsortedCaracteristic = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, QualitativeValueId.SORTED_HORS_VRAC.getValue());
        }

        { // size caracteristic

            sizeCaracteristicValues = Maps.newTreeMap();

            Caracteristic caracteristic = persistenceService.getSizeCategoryCaracteristic();

            List<CaracteristicQualitativeValue> qualitativeValues = caracteristic.getQualitativeValue();

            Map<Integer, CaracteristicQualitativeValue> sizeById = TuttiEntities.splitByIdAsInt(qualitativeValues);
            CaracteristicQualitativeValue smallCaracteristic = sizeById.get(QualitativeValueId.SIZE_SMALL.getValue());
            sizeCaracteristicValues.put("P", smallCaracteristic);
            sizeCaracteristicValues.put("p", smallCaracteristic);
            CaracteristicQualitativeValue bigCaracteristic = sizeById.get(QualitativeValueId.SIZE_BIG.getValue());
            sizeCaracteristicValues.put("G", bigCaracteristic);
            sizeCaracteristicValues.put("g", bigCaracteristic);
        }

        { // sex caracteristic

            sexCaracteristicValues = Maps.newTreeMap();

            Caracteristic caracteristic = persistenceService.getSexCaracteristic();

            List<CaracteristicQualitativeValue> qualitativeValues = caracteristic.getQualitativeValue();

            Map<Integer, CaracteristicQualitativeValue> sexById = TuttiEntities.splitByIdAsInt(qualitativeValues);
            CaracteristicQualitativeValue femaleCaracteristic = sexById.get(QualitativeValueId.SEX_FEMALE.getValue());
            sexCaracteristicValues.put("F", femaleCaracteristic);
            sexCaracteristicValues.put("f", femaleCaracteristic);
            CaracteristicQualitativeValue maleCaracteristic = sexById.get(QualitativeValueId.SEX_MALE.getValue());
            sexCaracteristicValues.put("M", maleCaracteristic);
            sexCaracteristicValues.put("m", maleCaracteristic);
            CaracteristicQualitativeValue unkownCaracteristic = sexById.get(QualitativeValueId.SEX_UNDEFINED.getValue());
            sexCaracteristicValues.put("I", unkownCaracteristic);
            sexCaracteristicValues.put("i", unkownCaracteristic);
        }

        { // maturity caracteristic

            maturityCaracteristicValues = Maps.newTreeMap();

            Caracteristic caracteristic = persistenceService.getMaturityCaracteristic();

            List<CaracteristicQualitativeValue> qualitativeValues = caracteristic.getQualitativeValue();

            Map<Integer, CaracteristicQualitativeValue> byIds = TuttiEntities.splitByIdAsInt(qualitativeValues);
            maturityCaracteristicValues.put("1", byIds.get(QualitativeValueId.MATURITY1.getValue()));
            maturityCaracteristicValues.put("2", byIds.get(QualitativeValueId.MATURITY2.getValue()));
            maturityCaracteristicValues.put("3", byIds.get(QualitativeValueId.MATURITY3.getValue()));
            maturityCaracteristicValues.put("4", byIds.get(QualitativeValueId.MATURITY4.getValue()));
            maturityCaracteristicValues.put("5", byIds.get(QualitativeValueId.MATURITY5.getValue()));
        }
    }

    public PsionImportResult importFile(File psionFile, FishingOperation operation, CatchBatch catchBatch) {

        Preconditions.checkNotNull(psionFile);
        Preconditions.checkArgument(psionFile.exists(), "Psion file " + psionFile + " does not exist.");

        TuttiProtocol protocol = persistenceService.getProtocol();

        if (protocol == null) {
            throw new ApplicationBusinessException(t("tutti.service.psionimport.error.no.protocol"));
        }

        List<Species> allSpeciesWithSurveyCode = new ArrayList<>(persistenceService.getAllReferentSpecies());

        TaxonCache speciesMap = TaxonCaches.createSpeciesCacheWithoutVernacularCode(persistenceService, protocol);
        speciesMap.load(allSpeciesWithSurveyCode);

        Map<String, Species> speciesBySurveyCode = Maps.newTreeMap();
        for (Species species : allSpeciesWithSurveyCode) {
            if (species.getSurveyCode() != null) {
                speciesBySurveyCode.put(species.getSurveyCode(), species);
            }
        }
        Map<String, SpeciesProtocol> speciesProtocolBySurveyCode = Maps.newTreeMap();

        for (SpeciesProtocol speciesProtocol : protocol.getSpecies()) {
            if (speciesProtocol.getSpeciesSurveyCode() == null) {
                continue;
            }

            speciesProtocolBySurveyCode.put(speciesProtocol.getSpeciesSurveyCode(), speciesProtocol);
        }

        BatchContainer<SpeciesBatch> rootSpeciesBatch =
                persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);

        Set<Species> alreadyUsedSpecies = Sets.newHashSet();
        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            alreadyUsedSpecies.add(speciesBatch.getSpecies());
        }

        // load model
        PsionImportModel importModel = new PsionImportModel();
        try {
            readImportFile(importModel,
                           psionFile,
                           operation,
                           speciesBySurveyCode,
                           speciesProtocolBySurveyCode,
                           alreadyUsedSpecies);
        } catch (IOException e) {
            importModel.addError(e.getMessage());
        }

        PsionImportResult result = new PsionImportResult(psionFile, importModel.getErrors());
        if (importModel.withErrors()) {
            if (log.isWarnEnabled()) {
                log.warn("Won't import psion file, errors detected.");
            }

            return result;
        }

        // --- Check sample category id used --- //
        SampleCategoryModel sampleCategoryModel = context.getSampleCategoryModel();
        Set<Integer> sampleCategoryIdUsed = importModel.getSampleCategoryIdUsed();
        List<String> missingCategories = Lists.newArrayList();
        for (Integer categoryId : sampleCategoryIdUsed) {
            if (!sampleCategoryModel.containsCategoryId(categoryId)) {
                missingCategories.add("<li>" + categoryId + "</li>");
            }
        }

        if (!missingCategories.isEmpty()) {

            result.addError(
                    t("tutti.service.psionimport.error.invalidSampleCategoryModel.message",
                      Joiner.on("").join(missingCategories))
            );

            return result;
        }

        // --- Check sorted batches --- //
        try {
            importModel.checkSortedBatches(context.getSampleCategoryModel());
        } catch (IOException e) {
            result.addError(e.getMessage());
            return result;
        }

        // --- clean sorted batches --- //
        importModel.cleanSortedBatches();

        // --- clean unsorted batches --- //
        importModel.cleanUnsortedBatches();

        // --- Ok no error, can persist --- //

        persist(result, importModel, operation, catchBatch);

        return result;
    }

    protected void readImportFile(PsionImportModel importModel,
                                  File importFile,
                                  FishingOperation operation,
                                  Map<String, Species> speciesBySurveyCode,
                                  Map<String, SpeciesProtocol> speciesProtocolBySurveyCode,
                                  Set<Species> alreadyUsedSpecies) throws IOException {

        String operationNumber = String.valueOf(operation.getFishingOperationNumber());
        Date operationStartDate = DateTimes.getDay(operation.getGearShootingStartDate());

        BufferedReader reader = Files.newReader(importFile, Charsets.UTF_8);

        try {

            reader.readLine(); // initiales saisisseurs
            String operationCode = reader.readLine(); // Id du trait
            String operationDateStr = reader.readLine(); // Date du trait

            Date operationDate;
            try {
                operationDate = df.parse(operationDateStr);
            } catch (ParseException e) {

                throw new IOException(t("tutti.service.psionimport.error.invalid.date.format"));
            }

            boolean correctOperation = Objects.equals(operationCode, operationNumber) &&
                                       Objects.equals(operationDate, operationStartDate);

            if (!correctOperation) {
                throw new IOException(t("tutti.service.psionimport.error.invalid.operation"));
            }

            reader.readLine(); // Heure de création du fichier
            reader.readLine(); // Ligne blanche

            int lineNumber = 5;

            PsionImportBatchModel batch = null;

            String line;
            String badSpecies = null;
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                if (!line.contains(":")) {
                    throw new IOException(t("tutti.service.psionimport.error.invalid.line.syntax", lineNumber, line));
                }
                int endIndex = line.indexOf(':');
                String commandStr = StringUtils.trim(line.substring(0, endIndex));


                PsionImportKeyword command;

                try {
                    command = PsionImportKeyword.valueOf(commandStr);
                } catch (IllegalArgumentException e) {
                    throw new IOException(t("tutti.service.psionimport.error.invalid.command.syntax", commandStr, lineNumber));
                }

                String value = StringUtils.trim(line.substring(endIndex + 1));

                if (PsionImportKeyword.ESPE.equals(command)) {

                    // start a new species

                    // register previous batch
                    if (batch != null) {
                        importModel.addBatch(batch);
                    }

                    Species species = speciesBySurveyCode.get(value);

                    if (species == null) {

                        // could not load this species

                        badSpecies = value;
                        batch = null;
                        String error = t("tutti.service.psionimport.error.species.not.found", lineNumber, value);
                        if (log.isWarnEnabled()) {
                            log.warn(error);
                        }
                        importModel.addError(error);
                        continue;
                    }

                    if (alreadyUsedSpecies.contains(species)) {

                        // can't use an already used species
                        badSpecies = value;
                        batch = null;
                        String error = t("tutti.service.psionimport.error.species.already.used", lineNumber, value);
                        if (log.isWarnEnabled()) {
                            log.warn(error);
                        }
                        importModel.addError(error);
                        continue;
                    }

                    badSpecies = null;

                    SpeciesProtocol speciesProtocol = speciesProtocolBySurveyCode.get(value);

                    String lengthStepCaracteristicId = speciesProtocol.getLengthStepPmfmId();

                    if (StringUtils.isBlank(lengthStepCaracteristicId)) {
                        badSpecies = value;
                        batch = null;
                        String error = t("tutti.service.psionimport.error.no.lengthClass.caracteristic", lineNumber, value);
                        if (log.isWarnEnabled()) {
                            log.warn(error);
                        }
                        importModel.addError(error);
                        continue;
                    }
                    batch = new PsionImportBatchModel(species, Integer.valueOf(lengthStepCaracteristicId));
                } else {

                    if (badSpecies != null) {
                        // ignore this line due to bad species
                        if (log.isDebugEnabled()) {
                            log.debug("Ligne " + lineNumber
                                      + " ignorée car l'espèce " + badSpecies + " n'était pas reconnue");
                        }
                        continue;
                    }

                    // check batch exists
                    if (batch == null) {
                        throw new IOException(
                                t("tutti.service.psionimport.error.invalid.firstLine", line, lineNumber));
                    }

                    switch (command) {

                        case POID:
                            // add weight
                            Float weight = toFloat(value, lineNumber);
                            batch.setWeight(weight);
                            break;

                        case TAIL:
                            // add sample weight
                            Float sampleWeight = toFloat(value, lineNumber);
                            batch.setSampleWeight(sampleWeight);
                            break;

                        case CATE:
                            // add category

                            if (StringUtils.isBlank(value)) {
                                badSpecies = batch.getSpecies().getSurveyCode();
                                batch = null;
                                String error = t("tutti.service.psionimport.error.invalid.category.syntax", lineNumber, value, badSpecies);

                                if (log.isWarnEnabled()) {
                                    log.warn(error);
                                }
                                importModel.addError(error);
                                continue;
                            }

                            if (NO_CATEGORY_VALUES.contains(value)) {

                                // special case, no category

                            } else {

                                // guess all categories

                                for (int i = 0, nbCategory = value.length(); i < nbCategory; i++) {
                                    String categoryCode = value.substring(i, i + 1);

                                    PsionImportBatchModel.SampleCategory category = guessCategory(categoryCode);

                                    if (category == null) {
                                        badSpecies = batch.getSpecies().getSurveyCode();
                                        batch = null;
                                        String error = t("tutti.service.psionimport.error.invalid.category.syntax", lineNumber, categoryCode, badSpecies);
                                        if (log.isWarnEnabled()) {
                                            log.warn(error);
                                        }
                                        importModel.addError(error);
                                        break;
                                    }

                                    batch.setCategory(category.getCategoryId(), category.getCategoryValue());
                                }

                                if (batch == null) {

                                    // at least one category was not ok
                                    continue;
                                }
                            }

                            batch.setCategoryCode(value);

                            break;

                        case LONG:
                            // add frequency
                            Float size = toFloat(value, lineNumber);
                            batch.addFrequency(size, 1);
                            break;
                    }
                }
            }

            if (batch != null) {

                // save it
                importModel.addBatch(batch);
            }

            reader.close();
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    protected void persist(PsionImportResult result,
                           PsionImportModel importModel,
                           FishingOperation operation,
                           CatchBatch catchBatch) {

        if (catchBatch != null) {
            addFileAsAttachment(result.getImportFile(), catchBatch);
        }

        // insert all imported species batches

        Set<Species> species = importModel.getSpecies();

        for (Species specy : species) {

            List<PsionImportBatchModel> sortedBatchs = importModel.getSortedBatches(specy);

            if (CollectionUtils.isNotEmpty(sortedBatchs)) {
                persistSortedBatches(operation, specy, sortedBatchs);

                result.incrementNbSortedImported();
            }

            List<PsionImportBatchModel> unsortedBatchs = importModel.getUnsortedBatches(specy);

            if (CollectionUtils.isNotEmpty(unsortedBatchs)) {
                persistUnsortedBatches(operation, specy, unsortedBatchs);

                result.incrementNbUnsortedImported();
            }

        }

        persistenceService.saveCatchBatch(catchBatch);
    }

    protected void persistSortedBatches(FishingOperation operation,
                                        Species specy,
                                        List<PsionImportBatchModel> batchs) {

        if (batchs.size() == 1 && !batchs.get(0).withCategories()) {

            PsionImportBatchModel batchModel = batchs.get(0);

            // simple batch with no category
            SpeciesBatch batch = createSpeciesBatch(operation,
                                                    batchModel.getSpecies(),
                                                    batchModel.getWeight(),
                                                    batchModel.getSampleWeight(),
                                                    PmfmId.SORTED_UNSORTED.getValue(),
                                                    sortedCaracteristic);


            batch = persistenceService.createSpeciesBatch(batch, null, true);

            persistFrequencies(batch, batchModel);

        } else {

            // batch with categories

            // Is there two weights ?
            // If so and applyOnLeaf is off then the weight has to be placed in the sorted batch
            // Otherwise apply both weight on categorized batch

            Float sortedBatchWeight = null;

            Float weight = batchs.get(0).getWeight();
            Float sampleWeight = batchs.get(0).getSampleWeight();
            boolean applyOnLeaf = batchs.get(0).isApplyBothWeightOnCategorizedBatch();

            if (sampleWeight != null) {

                // use the weight as sorted batch weight

                if (!applyOnLeaf) {

                    // use the weight as sorted batch weight
                    sortedBatchWeight = weight;

                }

            }

            SpeciesBatch rootBatch = createSpeciesBatch(operation,
                                                        specy,
                                                        sortedBatchWeight,
                                                        null,
                                                        PmfmId.SORTED_UNSORTED.getValue(),
                                                        sortedCaracteristic);

            rootBatch = persistenceService.createSpeciesBatch(rootBatch, null, true);

            createCategoryBatches(operation, specy, batchs, rootBatch, sortedBatchWeight != null);

        }

    }

    protected void persistUnsortedBatches(FishingOperation operation,
                                          Species specy,
                                          List<PsionImportBatchModel> batchs) {

        if (batchs.size() == 1 && !batchs.get(0).withCategories()) {

            PsionImportBatchModel batchModel = batchs.get(0);

            // simple batch with no category
            SpeciesBatch batch = createSpeciesBatch(operation,
                                                    batchModel.getSpecies(),
                                                    batchModel.getWeight(),
                                                    batchModel.getSampleWeight(),
                                                    PmfmId.SORTED_UNSORTED.getValue(),
                                                    unsortedCaracteristic);

            batch = persistenceService.createSpeciesBatch(batch, null, true);

            persistFrequencies(batch, batchModel);

        } else {

            // batch with categories

            SpeciesBatch rootBatch = createSpeciesBatch(operation,
                                                        specy,
                                                        null,
                                                        null,
                                                        PmfmId.SORTED_UNSORTED.getValue(),
                                                        unsortedCaracteristic);

            rootBatch = persistenceService.createSpeciesBatch(rootBatch, null, true);

            createCategoryBatches(operation, specy, batchs, rootBatch, false);

        }

    }

    protected void createCategoryBatches(FishingOperation operation,
                                         Species specy,
                                         List<PsionImportBatchModel> batchs,
                                         SpeciesBatch rootBatch,
                                         boolean applyOnlySampleWeight) {

        for (PsionImportBatchModel batchModel : batchs) {

            SpeciesBatch parentBatch = rootBatch;

            SpeciesBatch childBatch = null;

            Iterator<PsionImportBatchModel.SampleCategory> categoryIterator = batchModel.getCategoryIterator();

            while (categoryIterator.hasNext()) {
                PsionImportBatchModel.SampleCategory sampleCategory = categoryIterator.next();

                boolean lastCategory = !categoryIterator.hasNext();

                Integer categoryId = sampleCategory.getCategoryId();
                Serializable categoryValue = sampleCategory.getCategoryValue();

                if (lastCategory) {

                    // always create the leaf

                    Float weight;
                    Float sampleWeight;

                    if (applyOnlySampleWeight) {
                        weight = batchModel.getSampleWeight();
                        sampleWeight = null;
                    } else {
                        weight = batchModel.getWeight();
                        sampleWeight = batchModel.getSampleWeight();
                    }
                    childBatch = createSpeciesBatch(operation,
                                                    specy,
                                                    weight,
                                                    sampleWeight,
                                                    categoryId,
                                                    categoryValue);
                } else {

                    // try to find child in parent children

                    childBatch = null;
                    for (SpeciesBatch speciesBatch : parentBatch.getChildBatchs()) {

                        if (speciesBatch.getSampleCategoryId().equals(categoryId) &&
                            speciesBatch.getSampleCategoryValue().equals(categoryValue)) {
                            childBatch = speciesBatch;
                            break;
                        }
                    }

                    if (childBatch == null) {

                        // must create it
                        childBatch = createSpeciesBatch(operation,
                                                        specy,
                                                        null,
                                                        null,
                                                        categoryId,
                                                        categoryValue);
                    }
                }

                if (TuttiEntities.isNew(childBatch)) {

                    // persist it
                    childBatch = persistenceService.createSpeciesBatch(childBatch, parentBatch.getIdAsInt(), true);
                    parentBatch.addChildBatchs(childBatch);
                }

                parentBatch = childBatch;
            }

            persistFrequencies(childBatch, batchModel);
        }
    }

    protected PsionImportBatchModel.SampleCategory guessCategory(String categoryCode) {

        PsionImportBatchModel.SampleCategory result = null;

        Integer caracteristicId;
        CaracteristicQualitativeValue caracteristicQualitativeValue;

        if (SIZE_CATEGORY_VALUES.contains(categoryCode)) {

            // size caracteristic
            caracteristicId = PmfmId.SIZE_CATEGORY.getValue();
            caracteristicQualitativeValue = sizeCaracteristicValues.get(categoryCode);

            result = new PsionImportBatchModel.SampleCategory(caracteristicId, caracteristicQualitativeValue);

        } else if (SEX_CATEGORY_VALUES.contains(categoryCode)) {

            // sex caracteristic
            caracteristicId = PmfmId.SEX.getValue();
            caracteristicQualitativeValue = sexCaracteristicValues.get(categoryCode);

            result = new PsionImportBatchModel.SampleCategory(caracteristicId, caracteristicQualitativeValue);

        } else if (MATURITY_CATEGORY_VALUES.contains(categoryCode)) {

            // maturity caracteristic
            caracteristicId = PmfmId.MATURITY.getValue();
            caracteristicQualitativeValue = maturityCaracteristicValues.get(categoryCode);

            result = new PsionImportBatchModel.SampleCategory(caracteristicId, caracteristicQualitativeValue);
        }
        return result;
    }

    protected void persistFrequencies(SpeciesBatch batch, PsionImportBatchModel batchModel) {

        Integer lengthStepCaracteristicId = batchModel.getLengthStepCaracteristicId();
        Map<Float, MutableInt> frequencies = batchModel.getFrequencies();
        List<SpeciesBatchFrequency> toSave = Lists.newArrayList();

        Caracteristic lengthStepCaracteristic = persistenceService.getCaracteristic(lengthStepCaracteristicId);

        for (Map.Entry<Float, MutableInt> entry : frequencies.entrySet()) {
            Float size = entry.getKey();
            MutableInt number = entry.getValue();

            SpeciesBatchFrequency batchFrequency = SpeciesBatchFrequencys.newSpeciesBatchFrequency();
            batchFrequency.setBatch(batch);
            batchFrequency.setLengthStepCaracteristic(lengthStepCaracteristic);
            batchFrequency.setLengthStep(size);
            batchFrequency.setNumber(number.getValue());
            toSave.add(batchFrequency);
        }

        persistenceService.saveSpeciesBatchFrequency(batch.getIdAsInt(), toSave);
    }

    protected SpeciesBatch createSpeciesBatch(FishingOperation operation,
                                              Species species,
                                              Float catchWeight,
                                              Float sampleWeight,
                                              Integer categoryId,
                                              Serializable cqv) {
        SpeciesBatch batch = SpeciesBatchs.newSpeciesBatch();
        batch.setFishingOperation(operation);
        batch.setSampleCategoryId(categoryId);
        batch.setSampleCategoryValue(cqv);
        batch.setSpecies(species);
        if (catchWeight != null) {

            catchWeight = WeightUnit.KG.round(WeightUnit.G.toEntity(catchWeight));
            batch.setSampleCategoryWeight(catchWeight);
        }
        if (sampleWeight != null) {

            sampleWeight = WeightUnit.KG.round(WeightUnit.G.toEntity(sampleWeight));
            batch.setWeight(sampleWeight);
        }

        batch.setChildBatchs(Lists.<SpeciesBatch>newArrayList());
        return batch;
    }

    protected void addFileAsAttachment(File f, CatchBatch catchBatch) {
        Attachment attachment = Attachments.newAttachment();
        attachment.setObjectType(ObjectTypeCode.CATCH_BATCH);
        attachment.setObjectId(Integer.valueOf(catchBatch.getId()));
        attachment.setName(f.getName());
        String date = DateFormat.getDateTimeInstance().format(context.currentDate());
        String comment = t("tutti.service.psion.import.attachment.comment", date);
        attachment.setComment(comment);
        persistenceService.createAttachment(attachment, f);
    }

    protected Float toFloat(String cell, int lineNumber) throws IOException {
        Float result = null;
        if (!cell.isEmpty()) {
            try {
                result = Float.valueOf(cell);
            } catch (NumberFormatException e) {
                throw new IOException("Format de la valeur [" + lineNumber + "] : " + cell +
                                      " incorrect, devrait être un entier décimal");
            }
        }
        if (result == null) {
            throw new IOException("La valeur [" + lineNumber + "] est obligatoire mais n'est pas renseignée");
        }
        return result;
    }
}
