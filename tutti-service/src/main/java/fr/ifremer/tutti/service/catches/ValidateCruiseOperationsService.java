package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.ValidationService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Service to validate the operations of a cruise
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class ValidateCruiseOperationsService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(ValidateCruiseOperationsService.class);

    protected PersistenceService persistenceService;

    protected ValidationService validationService;

    protected WeightComputingService weightComputingService;

    protected DecoratorService decoratorService;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        validationService = getService(ValidationService.class);
        weightComputingService = getService(WeightComputingService.class);
        decoratorService = getService(DecoratorService.class);
    }

    /**
     * Validates the given cruise id.
     *
     * @return the validation results
     */
    public NuitonValidatorResult validateCruise(ProgressionModel progressionModel, Integer cruiseId) {

        progressionModel.increments(t("tutti.service.validateCruise.cruise.loading", cruiseId));
        Cruise cruise = persistenceService.getCruise(cruiseId);

        Decorator<Cruise> decorator = decoratorService.getDecoratorByType(Cruise.class);
        progressionModel.increments(t("tutti.service.validateCruise.cruise.check", cruiseId, decorator.toString(cruise)));

        return validationService.validateValidateCruise(cruise);

    }

    /**
     * Validates the operations of the given operation ids.
     *
     * @return a map containing the operations and the validation results
     */
    public LinkedHashMap<FishingOperation, NuitonValidatorResult> validateOperations(ProgressionModel progressionModel, List<Integer> operationIds) {

        LinkedHashMap<FishingOperation, NuitonValidatorResult> result = new LinkedHashMap<>();

        Decorator<FishingOperation> decorator = decoratorService.getDecoratorByType(FishingOperation.class);
        for (Integer operationId : operationIds) {

            progressionModel.increments(t("tutti.service.validateCruise.operations.loading", operationId));
            FishingOperation operation = persistenceService.getFishingOperation(operationId);

            progressionModel.increments(t("tutti.service.validateCruise.operations.check", operationId, decorator.toString(operation)));
            NuitonValidatorResult validator = validationService.validateValidateFishingOperation(operation);

            checkOperation(operation, validator);
            result.put(operation, validator);

        }

        return result;

    }

    /**
     * Validates the given cruise.
     *
     * @return the validation result
     */
    public NuitonValidatorResult validateCruiseCruise(Cruise cruise) {
        return validationService.validateValidateCruise(cruise);
    }

    /**
     * Validates the operation of the currently selected cruise whose id is the given id.
     *
     * @return the validation results
     */
    public NuitonValidatorResult validateCruiseOperation(FishingOperation operation) {
        NuitonValidatorResult validator = validationService.validateValidateFishingOperation(operation);
        checkOperation(operation, validator);
        return validator;
    }

    /**
     * Validates the operation of the currently selected cruise whose id is the given id.
     *
     * @return the validation results
     */
    public NuitonValidatorResult validateCruiseOperation(CatchBatch catches) {
        FishingOperation operation = persistenceService.getFishingOperation(catches.getFishingOperation().getIdAsInt());
        NuitonValidatorResult validator = validationService.validateValidateFishingOperation(operation);

        checkOperation(operation, catches, validator);
        return validator;
    }

    /**
     * @param file              where to write result
     * @param validationResults result of validation
     */
    public void exportValidationResults(File file, Map<FishingOperation, NuitonValidatorResult> validationResults) {
        try {
            List<String> lines = Lists.newArrayList();
            for (FishingOperation operation : validationResults.keySet()) {
                lines.addAll(getExportLines(operation, validationResults.get(operation)));
                lines.add("");
            }
            FileUtils.writeLines(file, lines);

        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.service.validateCruise.exportResult.error", file));
        }
    }

    /**
     * @param file             where to write result
     * @param operation        operation to check
     * @param validationResult result of validation
     */
    public void exportValidationResult(File file, FishingOperation operation, NuitonValidatorResult validationResult) {
        try {
            List<String> lines = getExportLines(operation, validationResult);

            FileUtils.writeLines(file, lines);

        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.service.validateCruise.exportResult.error", file));
        }
    }

    /*
     *  Internal methods
     */

    /**
     * Adds additional messages to the validation results
     *
     * @param fishingOperation the operation to validate
     * @param validator        the validatpr containing the messages.
     */
    protected void checkOperation(FishingOperation fishingOperation, NuitonValidatorResult validator) {

        Integer fishingOperationId = fishingOperation.getIdAsInt();

        boolean withCatchBatch =
                persistenceService.isFishingOperationWithCatchBatch(
                        fishingOperationId);

        if (!withCatchBatch) {
            if (log.isWarnEnabled()) {
                log.warn("Skip fishing operation " + fishingOperation +
                                 " since no catchBatch associated.");
            }
            Map<String, List<String>> errorMap = Maps.newHashMap();
            errorMap.put("catches", Lists.newArrayList(t("tutti.validator.warning.fishingOperation.batch.notFound")));
            validator.addMessagesForScope(NuitonValidatorScope.WARNING, errorMap);
        } else {
            try {
                CatchBatch catchBatch =
                        persistenceService.getCatchBatchFromFishingOperation(fishingOperationId);

                checkOperation(fishingOperation, catchBatch, validator);
            } catch (InvalidBatchModelException e) {

                // batch is not compatible with Tutti
                if (log.isDebugEnabled()) {
                    log.debug("Invalid batch model", e);
                }
                Map<String, List<String>> errorMap = Maps.newHashMap();
                errorMap.put("catches", Lists.newArrayList(t("tutti.validator.warning.fishingOperation.invalid.batch.model")));
                validator.addMessagesForScope(NuitonValidatorScope.WARNING, errorMap);
            }
        }
    }

    protected void transfertValidatorResult(NuitonValidatorResult validatorResult, List<String> errors) {
        if (validatorResult.hasFatalMessages()) {
            errors.addAll(validatorResult.getMessagesForScope(NuitonValidatorScope.FATAL));
        }

        if (validatorResult.hasErrorMessagess()) {
            errors.addAll(validatorResult.getMessagesForScope(NuitonValidatorScope.ERROR));
        }
    }

    /**
     * Adds additional messages to the validation results
     *
     * @param fishingOperation the operation to validate
     * @param catchBatch       the catchBatch to validate
     * @param validator        the validatpr containing the messages.
     */
    protected void checkOperation(FishingOperation fishingOperation,
                                  CatchBatch catchBatch,
                                  NuitonValidatorResult validator) {
        if (log.isDebugEnabled()) {
            log.debug("Will check fishingOperation: " + fishingOperation);
        }

        List<String> errors = Lists.newArrayList();
        Integer fishingOperationId = fishingOperation.getIdAsInt();

        NuitonValidatorResult fishingOperationValidationResult = validationService.validateValidateFishingOperation(fishingOperation);
        transfertValidatorResult(fishingOperationValidationResult, errors);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = null;
        boolean isCatchBatch = persistenceService.isFishingOperationWithCatchBatch(fishingOperationId);
        boolean error = !isCatchBatch;

        if (isCatchBatch) {

            NuitonValidatorResult catchBatchValidatorResult = validationService.validateValidateCatchBatch(catchBatch);
            transfertValidatorResult(catchBatchValidatorResult, errors);

            rootSpeciesBatch = persistenceService.getRootSpeciesBatch(fishingOperationId, true);

            if (rootSpeciesBatch != null) {
                List<SpeciesBatch> roots = rootSpeciesBatch.getChildren();

                for (SpeciesBatch batch : roots) {
                    NuitonValidatorResult speciesValidatorResult = validationService.validateValidateSpeciesBatch(batch);
                    transfertValidatorResult(speciesValidatorResult, errors);

                    try {
                        weightComputingService.computeSpeciesBatch(batch);

                    } catch (ApplicationBusinessException e) {
                        errors.add(e.getMessage());
                        error = true;
                    }
                }
            }
        }
        if (error) {
            rootSpeciesBatch = persistenceService.getRootSpeciesBatch(fishingOperationId, true);
        }

        BatchContainer<SpeciesBatch> rootBenthosBatch = null;
        error = false;
        if (isCatchBatch) {
            rootBenthosBatch = persistenceService.getRootBenthosBatch(fishingOperationId, true);

            if (rootBenthosBatch != null) {
                List<SpeciesBatch> roots = rootBenthosBatch.getChildren();

                for (SpeciesBatch batch : roots) {

                    NuitonValidatorResult benthosValidatorResult = validationService.validateValidateBenthosBatch(batch);
                    transfertValidatorResult(benthosValidatorResult, errors);

                    try {
                        weightComputingService.computeBenthosBatch(batch);

                    } catch (ApplicationBusinessException e) {
                        errors.add(e.getMessage());
                        error = true;
                    }
                }
            }
        }
        if (error) {
            rootBenthosBatch = persistenceService.getRootBenthosBatch(fishingOperationId, true);
        }
        if (isCatchBatch) {
            List<AccidentalBatch> accidentalBatchs = persistenceService.getAllAccidentalBatch(fishingOperationId);
            if (accidentalBatchs != null) {
                for (AccidentalBatch accidentalBatch : accidentalBatchs) {
                    NuitonValidatorResult accidentalBatchValidatorResult = validationService.validateValidateAccidentalBatch(accidentalBatch);
                    transfertValidatorResult(accidentalBatchValidatorResult, errors);
                }
            }
            List<IndividualObservationBatch> individualObservationBatchs = persistenceService.getAllIndividualObservationBatchsForFishingOperation(fishingOperationId);
            if (individualObservationBatchs != null) {
                for (IndividualObservationBatch individualObservationBatch : individualObservationBatchs) {
                    NuitonValidatorResult individualObservationBatchValidatorResult = validationService.validateValidateIndividualObservationBatch(individualObservationBatch);
                    transfertValidatorResult(individualObservationBatchValidatorResult, errors);
                }
            }
        }

        BatchContainer<MarineLitterBatch> rootMarineLitterBatch;
        try {
            Float weight = catchBatch == null ? null : catchBatch.getMarineLitterTotalWeight();
            rootMarineLitterBatch = weightComputingService.getComputedMarineLitterBatches(fishingOperationId, weight);

        } catch (ApplicationBusinessException e) {
            errors.add(e.getMessage());
            rootMarineLitterBatch = persistenceService.getRootMarineLitterBatch(fishingOperationId);
        }

        try {
            if (catchBatch != null) {
                weightComputingService.computeCatchBatchWeights(catchBatch,
                                                                rootSpeciesBatch,
                                                                rootBenthosBatch,
                                                                rootMarineLitterBatch);
            }
        } catch (ApplicationBusinessException e) {
            errors.add(e.getMessage());
        }

        if (CollectionUtils.isNotEmpty(errors)) {
            Map<String, List<String>> errorMap = Maps.newHashMap();
            errorMap.put("catches", errors);
            validator.addMessagesForScope(NuitonValidatorScope.ERROR, errorMap);
        }

        TuttiDataContext dataContext = context.getDataContext();
        if (dataContext.isProtocolFilled()) {
//            TuttiProtocol protocol = dataContext.getProtocol();
            Map<String, List<String>> warningMap = Maps.newHashMap();

            Decorator<Species> speciesDecorator = decoratorService.getDecoratorByType(Species.class);
            if (rootSpeciesBatch != null) {
                List<String> warnings = new ArrayList<>();
                for (SpeciesBatch batch : rootSpeciesBatch.getChildren()) {
                    if (isSpeciesBatchInvalid(batch)) {
                        String species = speciesDecorator.toString(batch.getSpecies());
                        String categoryValue = decoratorService.getDecorator(batch.getSampleCategoryValue())
                                                               .toString(batch.getSampleCategoryValue());
                        warnings.add(t("tutti.validator.warning.species.protocolNotRespected", species, categoryValue));
                    }
                }
                if (!warnings.isEmpty()) {
                    warningMap.put("species", warnings);
                }
            }

            if (rootBenthosBatch != null) {
                List<String> warnings = new ArrayList<>();
                for (SpeciesBatch batch : rootBenthosBatch.getChildren()) {
                    if (isBenthosBatchInvalid(batch)) {
                        String species = speciesDecorator.toString(batch.getSpecies());
                        String categoryValue = decoratorService.getDecorator(batch.getSampleCategoryValue())
                                                               .toString(batch.getSampleCategoryValue());
                        warnings.add(t("tutti.validator.warning.benthos.protocolNotRespected", species, categoryValue));
                    }
                }
                if (!warnings.isEmpty()) {
                    warningMap.put("benthos", warnings);
                }
            }

            if (MapUtils.isNotEmpty(warningMap)) {
                validator.addMessagesForScope(NuitonValidatorScope.WARNING, warningMap);
            }
        }
    }

    /**
     * Is the species batch respecting the protocol recommendations?
     *
     * @param batch       the batch to check (it should be a leaf)
     * @param frequencies the frequencies of the batch
     * @return true if the batch respects the protocol, false otherwise
     */
    public boolean isSpeciesBatchValid(SpeciesBatch batch, List<SpeciesBatchFrequency> frequencies) {
        boolean result = true;

        TuttiProtocol protocol = persistenceService.getProtocol();

        if (protocol != null) {
            Species species = batch.getSpecies();
            List<SpeciesProtocol> speciesProtocols = protocol.getSpecies();
            SpeciesProtocol speciesProtocol = TuttiProtocols.getSpeciesProtocol(species, speciesProtocols);

            if (speciesProtocol != null) {
                List<Integer> mandatoryCategories = speciesProtocol.getMandatorySampleCategoryId();

                SpeciesBatch browsingBatch = batch;
                while (browsingBatch.getParentBatch() != null) {
                    mandatoryCategories.remove(browsingBatch.getSampleCategoryId());
                    browsingBatch = browsingBatch.getParentBatch();
                }
                result = mandatoryCategories.isEmpty() &&
                        (speciesProtocol.getLengthStepPmfmId() != null
                                || CollectionUtils.isNotEmpty(frequencies)
                                || batch.getNumber() != null);
            }
        }
        return result;
    }

    /**
     * Is the species batch respecting the protocol recommendations?
     *
     * @param batch the batch to check
     * @return true if the batch or one of its children does not respect the protocol, false otherwise
     */
    protected boolean isSpeciesBatchInvalid(SpeciesBatch batch) {
        if (batch.isChildBatchsEmpty()) {
            List<SpeciesBatchFrequency> frequencies =
                    persistenceService.getAllSpeciesBatchFrequency(batch.getIdAsInt());
            return !isSpeciesBatchValid(batch, frequencies);
        }

        for (SpeciesBatch child : batch.getChildBatchs()) {
            boolean invalid = isSpeciesBatchInvalid(child);
            if (invalid) {
                return true;
            }
        }
        return false;
    }

    /**
     * Is the benthos batch respecting the protocol recommendations?
     *
     * @param batch the batch to check
     * @return true if the batch or one of its children does not respect the protocol, false otherwise
     */
    protected boolean isBenthosBatchInvalid(SpeciesBatch batch) {
        if (batch.isChildBatchsEmpty()) {
            List<SpeciesBatchFrequency> frequencies =
                    persistenceService.getAllBenthosBatchFrequency(batch.getIdAsInt());
            return !isBenthosBatchValid(
                    batch,
                    frequencies);
        }

        for (SpeciesBatch child : batch.getChildBatchs()) {
            boolean invalid = isBenthosBatchInvalid(child);
            if (invalid) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param operation        option to check
     * @param validationResult result of validation
     */
    protected List<String> getExportLines(FishingOperation operation, NuitonValidatorResult validationResult) {
        List<String> lines = Lists.newArrayList();

        lines.add(t("tutti.validator.export.operation",
                    decoratorService.getDecoratorByType(FishingOperation.class).toString(operation)));

        List<String> messages = validationResult.getMessagesForScope(NuitonValidatorScope.ERROR);
        lines.addAll(messages.stream().map(message -> t("tutti.validator.export.message.error", t(message))).collect(Collectors.toList()));
        messages = validationResult.getMessagesForScope(NuitonValidatorScope.WARNING);
        lines.addAll(messages.stream().map(message -> t("tutti.validator.export.message.warning", t(message))).collect(Collectors.toList()));

        return lines;
    }

    /**
     * Is the benthos batch respecting the protocol recommendations?
     *
     * @param batch       the batch to check (it should be a leaf)
     * @param frequencies the frequencies of the batch
     * @return true if the batch respects the protocol, false otherwise
     */
    public boolean isBenthosBatchValid(SpeciesBatch batch, List<SpeciesBatchFrequency> frequencies) {

        TuttiProtocol protocol = persistenceService.getProtocol();

        boolean result = true;

        if (protocol != null) {
            Species species = batch.getSpecies();
            List<SpeciesProtocol> speciesProtocols = protocol.getBenthos();
            SpeciesProtocol speciesProtocol = TuttiProtocols.getSpeciesProtocol(species, speciesProtocols);

            if (speciesProtocol != null) {
                // get the categories which should be set
                List<Integer> mandatoryCategories =
                        speciesProtocol.getMandatorySampleCategoryId();

                SpeciesBatch browsingBatch = batch;
                while (browsingBatch.getParentBatch() != null) {
                    mandatoryCategories.remove(browsingBatch.getSampleCategoryId());
                    browsingBatch = browsingBatch.getParentBatch();
                }
                result = mandatoryCategories.isEmpty() &&
                        (speciesProtocol.getLengthStepPmfmId() != null
                                || CollectionUtils.isNotEmpty(frequencies)
                                || batch.getNumber() != null);
            }
        }
        return result;
    }

}
