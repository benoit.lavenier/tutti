package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class SamplingCodePrefix {

    public static final String SEPARATOR = "#";
    public static final String NO_SURVEY_CODE_LABEL = "~";
    
    protected final String prefix;
    protected final String species;

    public SamplingCodePrefix(String prefix, String species) {
        this.prefix = prefix;
        if (SEPARATOR.equals(species)) {
            species = NO_SURVEY_CODE_LABEL;
        }
        this.species = species;
    }

    public SamplingCodePrefix(String samplingCode) {
        String[] codeParts = samplingCode.split(SEPARATOR);
        prefix = codeParts[0];
        species = codeParts[1];
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSpecies() {
        return species;
    }

    @Override
    public String toString() {
        return prefix + SEPARATOR + species + SEPARATOR;
    }

    public String toSamplingCode(int samplingCodeId) {
        return toString() + samplingCodeId;
    }

    public String toSpeciesOnlySamplingCode(int samplingCodeId) {
        return species + SEPARATOR + samplingCodeId;
    }

    /**
     * Extrait l'id du code (le nombre à la fin)
     * @param samplingCode
     * @return
     */
    public static int extractSamplingCodeIdFromSamplingCode(String samplingCode) {
        Objects.requireNonNull(samplingCode);
        String[] codeParts = samplingCode.split(SEPARATOR);
        return Integer.parseInt(codeParts[codeParts.length - 1]);
    }

}
