package fr.ifremer.tutti.service.bigfin.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.bigfin.signs.Sex;
import fr.ifremer.tutti.service.bigfin.signs.Size;
import fr.ifremer.tutti.service.bigfin.signs.VracHorsVrac;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportModel;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueParser;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class BigfinDataRowModel extends AbstractTuttiImportModel<BigfinDataRow> {

    public BigfinDataRowModel(Map<String, Species> speciesBySurveyCode, Map<Integer, SpeciesBatch> speciesBatchesById) {

        super(',');

        newIgnoredColumn("Study name");

        newMandatoryColumn("ID", BigfinDataRow.PROPERTY_RECORD_ID);

        // date et heure de l'enregistrement (non importé mais utile pour contrôle à l'import)
        newMandatoryColumn("Date", BigfinDataRow.PROPERTY_DATE, new Common.DateValue("MM/dd/yy HH:mm:ss"));

        newIgnoredColumn("LOC");

        // n° de la station (non importé mais utile pour contrôle à l'import)
        newMandatoryColumn("STA", BigfinDataRow.PROPERTY_STATION);

        newMandatoryColumn("COMMENT", BigfinDataRow.PROPERTY_VRAC_HORS_VRAC, new VracHorsVracValueParser());

        newIgnoredColumn("GPS X");

        newIgnoredColumn("GPS Y");

        newIgnoredColumn("SP CODE");

        // code espèce = code campagne (saisie libre donc risque fort de mauvaise saisie)
        newMandatoryColumn("SPEC", BigfinDataRow.PROPERTY_SPECIES_OR_SPECIES_BATCH, new SpeciesOrSpeciesBatchValueParser(speciesBySurveyCode, speciesBatchesById));

        newMandatoryColumn("LENGTH (mm)", BigfinDataRow.PROPERTY_LENGTH, Common.PRIMITIVE_FLOAT);

        newMandatoryColumn("WEIGHT (g)", BigfinDataRow.PROPERTY_WEIGHT, Common.FLOAT);

        newMandatoryColumn("SEX", BigfinDataRow.PROPERTY_SEX, new SexValueParser());

        // sz class : si code différents de 0 1 ou 2 alors tout bloquer et donner l'id des lignes en anomalies
        newMandatoryColumn("SIZE", BigfinDataRow.PROPERTY_SIZE, new SizeValueParser());

        newIgnoredColumn("MT");
        newIgnoredColumn("MS");

    }

    @Override
    public BigfinDataRow newEmptyInstance() {
        return new BigfinDataRow();
    }

    private static class SpeciesOrSpeciesBatchValueParser implements ValueParser<SpeciesOrSpeciesBatch> {

        final Map<String, Species> foundSpecies = new HashMap<>();

        private final Map<String, Species> speciesBySurveyCode;

        private final Map<Integer, SpeciesBatch> speciesBatches;

        public SpeciesOrSpeciesBatchValueParser(Map<String, Species> speciesBySurveyCode, Map<Integer, SpeciesBatch> speciesBatchesById) {
            this.speciesBySurveyCode = speciesBySurveyCode;
            this.speciesBatches = speciesBatchesById;
        }

        @Override
        public SpeciesOrSpeciesBatch parse(String value) throws ParseException {

            SpeciesBatch speciesBatch = null;
            Species species = null;

            if (StringUtils.isNotBlank(value)) {

                speciesBatch = getSpeciesBatch(value);

                if (speciesBatch == null) {

                    // try a species
                    species = getSpecies(value);
                }

            }

            SpeciesOrSpeciesBatch result;

            if (speciesBatch == null) {

                // Just a species
                result = new SpeciesOrSpeciesBatch(species);
            } else {

                // SpeciesBatch
                result = new SpeciesOrSpeciesBatch(speciesBatch);
            }

            return result;

        }

        protected SpeciesBatch getSpeciesBatch(String value) {

            SpeciesBatch speciesBatch = null;

            if (StringUtils.isNotBlank(value)) {
                try {
                    Integer batchId = Integer.valueOf(value);
                    speciesBatch = speciesBatches.get(batchId);
                } catch (NumberFormatException e) {
                    // Not a number
                    // Not a species batch
                }

            }

            return speciesBatch;

        }

        protected Species getSpecies(String value) {

            Species species;

            if (StringUtils.isBlank(value)) {

                species = Speciess.newSpecies();

            } else {

                // use upper case
                value = value.toUpperCase();

                // if code already found
                species = foundSpecies.get(value);

                //if not found, look for it in the survey codes
                if (species == null) {
                    species = speciesBySurveyCode.get(value);
                }

                // Si on ne trouve pas une espèce de code campagne XXXXXXX, on essaye alors avec le code XXXX-XXX
                if (species == null) {
                    String alternativeSpeciesCode = value.substring(0, 4) + '-' + value.substring(4);
                    species = speciesBySurveyCode.get(alternativeSpeciesCode);
                }

                if (species == null) {
                    species = Speciess.newSpecies();
                    species.setExternalCode(value);
                }

                // record the code in the found codes
                foundSpecies.put(value, species);

            }

            return species;

        }

    }

    private static class SizeValueParser implements ValueParser<Size> {
        @Override
        public Size parse(String value) throws ParseException {
            return Size.getValue(value.toUpperCase());
        }
    }

    private static class SexValueParser implements ValueParser<Sex> {
        @Override
        public Sex parse(String value) throws ParseException {
            Sex result = Sex.getValue(value.toUpperCase());
            if (result == null) {
                throw new ParseException("Could not parse Sex value: " + value, 0);
            }
            return result;
        }
    }

    private static class VracHorsVracValueParser implements ValueParser<VracHorsVrac> {
        @Override
        public VracHorsVrac parse(String value) throws ParseException {
            // On importe dans le Vrac par défaut, sauf si il y a le texte HV ou hv dans le champ text
            VracHorsVrac result = VracHorsVrac.getValue(value.toUpperCase());
            if (result == null) {
                result = VracHorsVrac.VRAC;
            }
            return result;
        }
    }
}
