package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForIndividualObservation;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationRow;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportIndividualObservationAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportIndividualObservationAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportIndividualObservationAction(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid()
                && importContext.getSurveyFileResult().isValid()
                && importContext.getOperationFileResult().isValid()
                && importContext.getImportRequest().isImportIndividualObservation();
    }

    @Override
    protected void skipExecute() {

        importContext.increments(t("tutti.service.genericFormat.skip.import.individualObservations"));

        if (!importContext.getImportRequest().isImportIndividualObservation()) {
            GenericFormatCsvFileResult importFileResult = importContext.getIndividualObservationFileResult();
            importFileResult.setSkipped(true);
        }

    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Import individualObservation.csv file.");
        }
        importContext.increments(t("tutti.service.genericFormat.import.individualObservations"));
        GenericFormatCsvFileResult importFileResult = importContext.getIndividualObservationFileResult();
        try (CsvConsumerForIndividualObservation consumer = importContext.loadIndividualObservations(true)) {

            for (ImportRow<IndividualObservationRow> row : consumer) {

                GenericFormatImportOperationContext operationContext = consumer.validateRow(row, importContext);
                if (operationContext != null) {
                    consumer.prepareRowForPersist(operationContext, row);
                }

            }

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close individualObservation.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        persistIndividualObservationBatches();

    }

    private void persistIndividualObservationBatches() {

        importContext.doActionOnCruiseContexts(new GenericFormatImportContext.CruiseContextAction() {

            @Override
            public void onCruise(GenericFormatImportCruiseContext cruiseContext, ProgressionModel progressionModel) {

                for (GenericFormatImportOperationContext fishingOperationContext : cruiseContext) {

                    boolean override = fishingOperationContext.isOverride();

                    if (override) {

                        deleteIndividualObservationBatches(fishingOperationContext);

                    }

                    String cruiseStr = cruiseContext.getCruiseLabel();
                    String operationStr = fishingOperationContext.getFishingOperationLabel();

                    importContext.increments(t("tutti.service.genericFormat.persist.operation.individualObservations", cruiseStr, operationStr));

                    if (fishingOperationContext.withIndividualObservationBatches()) {

                        ImmutableList<IndividualObservationBatch> individualObservationBatches = fishingOperationContext.getIndividualObservationBatches();

                        if (log.isInfoEnabled()) {
                            log.info("Check " + individualObservationBatches.size() + " individual observation(s) of " + operationStr + " for cruise: " + cruiseStr);
                        }
                        ValidateIndividualObservationAction.prepareIndividualObservationsForPersist(persistenceHelper, individualObservationBatches);

                        if (log.isInfoEnabled()) {
                            log.info("Persist " + individualObservationBatches.size() + " individual observation(s) of " + operationStr + " for cruise: " + cruiseStr);
                        }
                        List<IndividualObservationBatch> imported = persistIndividualObservationBatches(fishingOperationContext, individualObservationBatches);
                        fishingOperationContext.setIndividualObservationBatchImported(imported);
                    }

                }

            }

            private void deleteIndividualObservationBatches(GenericFormatImportOperationContext fishingOperationContext) {

                FishingOperation fishingOperation = fishingOperationContext.getFishingOperation();
                persistenceHelper.deleteIndividualObservationBatchForFishingOperation(fishingOperation.getIdAsInt());

            }

            private List<IndividualObservationBatch> persistIndividualObservationBatches(GenericFormatImportOperationContext fishingOperationContext, ImmutableList<IndividualObservationBatch> batches) {

                // https://forge.codelutin.com/issues/8335
                // Certain observation individuelle ne sont pas retrouvee dans
                // le fichier "catch" importer, cela est du a un autre bug
                // qui fait que certaine observation reste dans le system
                // au lieu d'etre effacee (?)
                // On ne peut donc pas les importer, on les ignores simplement
                // (car pour pouvoir les importer il faut absolument un speciesBatchId
                //  valide) au lieu de lever une exception.
                List<IndividualObservationBatch> goodBatches = new ArrayList<IndividualObservationBatch>(batches.size());

                for (IndividualObservationBatch individualObservation : batches) {

                    Integer batchId = individualObservation.getBatchId();
                    Integer speciesBatchId = fishingOperationContext.getSpeciesBatchId(batchId);
                    if (speciesBatchId == null) {
                        if (log.isErrorEnabled()) {
                            log.error("Persisted Species batch id not found for import species batch id: " + batchId);
                        }
                    } else {
                        // Objects.requireNonNull(speciesBatchId, "Persisted Species batch id not found for import species batch id: " + batchId);
                        individualObservation.setBatchId(speciesBatchId);
                        goodBatches.add(individualObservation);
                    }
                }


                // On conserve l'ordre des ids d'imports qui vont servir ensuite pour persister les pièces jointes
                ImmutableList.Builder<Integer> idsBuilder = ImmutableList.builder();
                goodBatches.forEach(batch -> idsBuilder.add(batch.getIdAsInt()));
                ImmutableList<Integer> importIds = idsBuilder.build();

                // on enlève les ids pour la persistence
                goodBatches.forEach(batch -> batch.setId((String) null));

                FishingOperation fishingOperation = fishingOperationContext.getFishingOperation();

                List<IndividualObservationBatch> savedBatches = persistenceHelper.createIndividualObservationBatch(fishingOperation, goodBatches);

                // Persistence des pièces-jointes associées aux observations individuelles
                int index = 0;
                for (IndividualObservationBatch savedBatch : savedBatches) {
                    Integer importId = importIds.get(index++);

                    Collection<AttachmentRow> attachmentRows = importContext.popAttachmentRows(ObjectTypeCode.SAMPLE, importId);
                    persistenceHelper.persistAttachments(savedBatch.getIdAsInt(), attachmentRows);
                }
                return goodBatches;
            }

        });

    }

}
