package fr.ifremer.tutti.service.export.toconfirmreport;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.PersistenceService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Contains a fishing operation data need to get species or benthos batch to report.
 *
 * Created on 2/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ToConfirmReportFishingOperationData {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ToConfirmReportFishingOperationData.class);

    public static ToConfirmReportFishingOperationData create(PersistenceService persistenceService,
                                                             Integer fishingOperationId) {

        boolean withCatchBatch = persistenceService.isFishingOperationWithCatchBatch(fishingOperationId);

        ToConfirmReportFishingOperationData result;

        if (!withCatchBatch) {

            if (log.isWarnEnabled()) {
                log.warn("Skip fishing operation " + fishingOperationId + " since no catchBatch associated.");
            }
            result = null;

        } else {

            FishingOperation fishingOperation = persistenceService.getFishingOperation(fishingOperationId);

            CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(fishingOperationId);

            List<SpeciesBatch> speciesBatchToConfirm = persistenceService.getAllSpeciesBatchToConfirm(fishingOperationId);

            List<SpeciesBatch> benthosBatchToConfirm = persistenceService.getAllBenthosBatchToConfirm(fishingOperationId);

            if (CollectionUtils.isEmpty(speciesBatchToConfirm) && CollectionUtils.isEmpty(benthosBatchToConfirm)) {

                if (log.isInfoEnabled()) {
                    log.info("No species nor benthos to confirm for fishing operation: " + fishingOperationId);
                }
                result = null;

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Found some species or benthos to confirm for fishing operation: " + fishingOperationId);
                }
                result = new ToConfirmReportFishingOperationData(fishingOperation,
                                                                 catchBatch,
                                                                 speciesBatchToConfirm,
                                                                 benthosBatchToConfirm);
            }

        }

        return result;

    }

    final FishingOperation fishingOperation;

    final CatchBatch catchBatch;

    final List<SpeciesBatch> speciesBatchToConfirm;

    final List<SpeciesBatch> benthosBatchToConfirm;

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public CatchBatch getCatchBatch() {
        return catchBatch;
    }

    public List<SpeciesBatch> getSpeciesBatchToConfirm() {
        return speciesBatchToConfirm;
    }

    public boolean isWithSpeciesBatchToConfirm() {
        return CollectionUtils.isNotEmpty(speciesBatchToConfirm);
    }

    public List<SpeciesBatch> getBenthosBatchToConfirm() {
        return benthosBatchToConfirm;
    }

    public boolean isWithBenthosBatchToConfirm() {
        return CollectionUtils.isNotEmpty(benthosBatchToConfirm);
    }

    private ToConfirmReportFishingOperationData(FishingOperation fishingOperation,
                                                CatchBatch catchBatch,
                                                List<SpeciesBatch> speciesBatchToConfirm,
                                                List<SpeciesBatch> benthosBatchToConfirm) {
        this.fishingOperation = fishingOperation;
        this.catchBatch = catchBatch;
        this.speciesBatchToConfirm = speciesBatchToConfirm;
        this.benthosBatchToConfirm = benthosBatchToConfirm;
    }

}
