package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;

/**
 * Model of a survey export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class SurveyModel extends AbstractTuttiImportExportModel<SurveyRow> {

    public static SurveyModel forExport(char separator) {

        SurveyModel exportModel = new SurveyModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static SurveyModel forImport(char separator, GenericFormatImportEntityParserFactory parserFactory) {

        SurveyModel importModel = new SurveyModel(separator);
        importModel.forImport(parserFactory);
        return importModel;

    }

    @Override
    public SurveyRow newEmptyInstance() {

        return SurveyRow.newEmptyInstance();

    }

    protected SurveyModel(char separator) {
        super(separator);
    }

    protected void forExport() {

        newColumnForExport("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newColumnForExport("Serie", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_FORMATTER);
        newColumnForExport("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);
        newColumnForExport("Navire", Cruise.PROPERTY_VESSEL, TuttiCsvUtil.VESSEL_FORMATTER);
        newColumnForExport("Nombre_Poches", Cruise.PROPERTY_MULTIRIG_NUMBER, TuttiCsvUtil.PRIMITIVE_INTEGER);
        newColumnForExport("Pays", SurveyRow.PROPERTY_COUNTRY, TuttiCsvUtil.COUNTRY_FORMATTER);
        newColumnForExport("Zone_Etude", Program.PROPERTY_ZONE, TuttiCsvUtil.PROGRAM_ZONE_FORMATTER);
        newColumnForExport("Campagne", Cruise.PROPERTY_NAME);
        newColumnForExport("Id_Sismer", SurveyRow.PROPERTY_ID_SISMER);
        newColumnForExport("Date_Deb_Campagne", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newColumnForExport("Port_Deb_Campagne", Cruise.PROPERTY_DEPARTURE_LOCATION, TuttiCsvUtil.HARBOUR_FORMATTER);
        newColumnForExport("Date_Fin_Campagne", Cruise.PROPERTY_END_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newColumnForExport("Port_Fin_Campagne", Cruise.PROPERTY_RETURN_LOCATION, TuttiCsvUtil.HARBOUR_FORMATTER);
        newColumnForExport("Chef_Mission", Cruise.PROPERTY_HEAD_OF_MISSION, TuttiCsvUtil.PERSON_LIST_FORMATTER);
        newColumnForExport("Resp_Salle_Tri", Cruise.PROPERTY_HEAD_OF_SORT_ROOM, TuttiCsvUtil.PERSON_LIST_FORMATTER);
        newColumnForExport("Commentaire", Cruise.PROPERTY_COMMENT, TuttiCsvUtil.COMMENT_PARSER_FORMATTER);

        newColumnForExport("Serie_Id", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_TECHNICAL_FORMATTER);
        newColumnForExport("Campagne_Id", SurveyRow.PROPERTY_OBJECT_ID, TuttiCsvUtil.INTEGER);
        newColumnForExport("Navire_Id", Cruise.PROPERTY_VESSEL, TuttiCsvUtil.VESSEL_TECHNICAL_FORMATTER);
        newColumnForExport("Engin_Id", Cruise.PROPERTY_GEAR, TuttiCsvUtil.GEAR_LIST_TECHNICAL_FORMATTER);
        newColumnForExport("Port_Deb_Campagne_Id", Cruise.PROPERTY_DEPARTURE_LOCATION, TuttiCsvUtil.HARBOUR_TECHNICAL_FORMATTER);
        newColumnForExport("Port_Fin_Campagne_Id", Cruise.PROPERTY_RETURN_LOCATION, TuttiCsvUtil.HARBOUR_TECHNICAL_FORMATTER);
        newColumnForExport("Chef_Mission_Id", Cruise.PROPERTY_HEAD_OF_MISSION, TuttiCsvUtil.PERSON_LIST_TECHNICAL_FORMATTER);
        newColumnForExport("Resp_Salle_Tri_Id", Cruise.PROPERTY_HEAD_OF_SORT_ROOM, TuttiCsvUtil.PERSON_LIST_TECHNICAL_FORMATTER);

    }

    protected void forImport(GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newIgnoredColumn("Serie");
        newMandatoryColumn("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);
        newIgnoredColumn("Navire");
        newMandatoryColumn("Nombre_Poches", Cruise.PROPERTY_MULTIRIG_NUMBER, TuttiCsvUtil.PRIMITIVE_INTEGER);
        newIgnoredColumn("Pays");
        newIgnoredColumn("Zone_Etude");
        newMandatoryColumn("Campagne", Cruise.PROPERTY_NAME);
        newIgnoredColumn("Id_Sismer");
        newMandatoryColumn("Date_Deb_Campagne", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newIgnoredColumn("Port_Deb_Campagne");
        newMandatoryColumn("Date_Fin_Campagne", Cruise.PROPERTY_END_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newIgnoredColumn("Port_Fin_Campagne");
        newIgnoredColumn("Chef_Mission");
        newIgnoredColumn("Resp_Salle_Tri");
        newMandatoryColumn("Commentaire", Cruise.PROPERTY_COMMENT, TuttiCsvUtil.COMMENT_PARSER_FORMATTER);

        newMandatoryColumn("Serie_Id", Cruise.PROPERTY_PROGRAM, parserFactory.getProgramParser());
        newMandatoryColumn("Campagne_Id", SurveyRow.PROPERTY_OBJECT_ID, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Navire_Id", Cruise.PROPERTY_VESSEL, parserFactory.getVesselParser());
        newMandatoryColumn("Engin_Id", Cruise.PROPERTY_GEAR, parserFactory.getGearListParser());
        newMandatoryColumn("Port_Deb_Campagne_Id", Cruise.PROPERTY_DEPARTURE_LOCATION, parserFactory.getHarbourParser());
        newMandatoryColumn("Port_Fin_Campagne_Id", Cruise.PROPERTY_RETURN_LOCATION, parserFactory.getHarbourParser());
        newMandatoryColumn("Chef_Mission_Id", Cruise.PROPERTY_HEAD_OF_MISSION, parserFactory.getPersonListParser());
        newMandatoryColumn("Resp_Salle_Tri_Id", Cruise.PROPERTY_HEAD_OF_SORT_ROOM, parserFactory.getPersonListParser());

    }

}
