<#--
 #%L
 Tutti :: Service
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2012 - 2014 Ifremer
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
  <head>
    <style type="text/css">

      <#assign blueColor="#000080">

      h1, h4 {
        color: ${blueColor};
        font-weight: bold;
        font-style: italic;
      }

      th {
        color: ${blueColor};
        font-weight: bold;
      }

      td, th {
        padding-right: 10pt;
      }

      td.number {
        text-align: right;
      }

      .operationInfo {
        font-weight: bold;
      }

      .label {
        color: ${blueColor};
      }

      .value {
        margin-right: 50pt;
      }

    </style>
  </head>
  <body>

    <h1>Rapport des traits validés</h1>

    <#assign orderedOperations = operations?sort_by("startDate")?reverse>
    <#list orderedOperations as operation>

      <p class='operationInfo'>
        <span class="label">Station :</span> <span class="value">${operation.station} - ${operation.number}</span>
        <span class="label">Poche :</span> <span class="value">${operation.rigNumber}</span>
        <span class="label">
          du ${operation.startDate?date?string.full} ${operation.startDate?time?string.short}
          <#if operation.endDate??>au ${operation.endDate?date?string.full} ${operation.endDate?time?string.short}</#if>
        </span>
      </p>

      <p class='operationInfo'>
        <span class="label">Poids total :</span> <span class="value"><#if operation.totalWeight??>${operation.totalWeight?string("0.00")}</#if></span>
        <span class="label">Poids total trié :</span> <#if operation.totalSortedWeight??>${operation.totalSortedWeight?string("0.00")}</#if>
      </p>

      <h4>Composition du trait par espèce :</h4>
      <table>
        <tr>
          <th>Espèce</th>
          <th>Nom scientifique</th>
          <th>Nom commun</th>
          <th>Trié (kg)</th>
          <th>Total (kg)</th>
          <th>%</th>
        </tr>

        <#list operation.catches?sort_by("sortedWeight")?reverse as catch>
          <tr>
            <td>${catch.code}</td>
            <td><em>${catch.scientificName}</em></td>
            <td><#if catch.vernacularCode??>${catch.vernacularCode}</#if></td>
            <td class="number">${catch.sortedWeight?string("0.00")}</td>
            <td class="number">${catch.totalWeight?string("0.00")}</td>
            <td class="number">${catch.percentage?string("0.0")}</td>
          </tr>
        </#list>
      </table>

      <#if operation != orderedOperations?last>
        <h2 style="page-break-after:always"/>
      </#if>

    </#list>

  </body>
</html>
