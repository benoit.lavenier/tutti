/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* ===================================================================
 * message.js
 * -------------------------------------------------------------------
 * Management messages  (internationalization)
 * -------------------------------------------------------------------       
 * @Author  : BODERE Erwan         
 * @Version : 1.0 - 19/03/2008                             
 * =================================================================== */
 

/**
 * Get a message with parameters
 * @param messageKey key of the message
 * @param params Array of values  
 */
getMessageWithParameters = function(messageKey, params) { 
	return reportContext.getMessage(messageKey, reportContext.getLocale(), params);
}
		
/**
 * Get a "simple" message
 * @param messageKey key of the message
 */
getMessage = function(messageKey) { 
	return reportContext.getMessage(messageKey, reportContext.getLocale()); 
}
		
/**
 * Get a message with 1 parameter
 * @param messageKey key of the message
 * @param param1 value of the parameter
 */
getMessage = function(messageKey, param1) { 
    var params = new Array(1);
	params[0]  = "" + param1;
	return getMessageWithParameters(messageKey, params);
}
	
/**
 * Get a message with 2 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 */
getMessage = function(messageKey, param1, param2) { 
    var params = new Array(2);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	return getMessageWithParameters(messageKey, params);
}
	
/**
 * Get a message with 3 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 * @param param3 value of the parameter
 */
getMessage = function(messageKey, param1, param2, param3) { 
    var params = new Array(3);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	params[2]  = "" + param3;
	return getMessageWithParameters(messageKey, params);
}
	
/**
 * Get a message with 4 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 * @param param3 value of the parameter
 * @param param4 value of the parameter
 */
getMessage = function(messageKey, param1, param2, param3, param4) { 
    var params = new Array(4);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	params[2]  = "" + param3;
	params[3]  = "" + param4;
	return getMessageWithParameters(messageKey, params);
}
	
/**
 * Get a message with 5 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 * @param param3 value of the parameter
 * @param param4 value of the parameter
 * @param param5 value of the parameter
 */
getMessage = function(messageKey, param1, param2, param3, param4, param5) { 
    var params = new Array(5);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	params[2]  = "" + param3;
	params[3]  = "" + param4;
	params[4]  = "" + param5;
	return getMessageWithParameters(messageKey, params);
}
	
/**
 * Get a message with 6 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 * @param param3 value of the parameter
 * @param param4 value of the parameter
 * @param param5 value of the parameter
 * @param param6 value of the parameter
 */
getMessage = function(messageKey, param1, param2, param3, param4, param5, param6) { 
    var params = new Array(6);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	params[2]  = "" + param3;
	params[3]  = "" + param4;
	params[4]  = "" + param5;
	params[5]  = "" + param6;
	return getMessageWithParameters(messageKey, params);
}
	
/**
 * Get a message with 7 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 * @param param3 value of the parameter
 * @param param4 value of the parameter
 * @param param5 value of the parameter
 * @param param6 value of the parameter
 * @param param7 value of the parameter
 */
getMessage = function(messageKey, param1, param2, param3, param4, param5, param6, param7) { 
    var params = new Array(7);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	params[2]  = "" + param3;
	params[3]  = "" + param4;
	params[4]  = "" + param5;
	params[5]  = "" + param6;
	params[6]  = "" + param7;
	return getMessageWithParameters(messageKey, params);
}
	
	
/**
 * Get a message with 8 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 * @param param3 value of the parameter
 * @param param4 value of the parameter
 * @param param5 value of the parameter
 * @param param6 value of the parameter
 * @param param7 value of the parameter
 * @param param8 value of the parameter
 */
getMessage = function(messageKey, param1, param2, param3, param4, param5, param6, param7, param8) { 
    var params = new Array(8);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	params[2]  = "" + param3;
	params[3]  = "" + param4;
	params[4]  = "" + param5;
	params[5]  = "" + param6;
	params[6]  = "" + param7;
	params[7]  = "" + param8;
	return getMessageWithParameters(messageKey, params);
}
	
/**
 * Get a message with 9 parameters
 * @param messageKey key of the message
 * @param param1 value of the parameter
 * @param param2 value of the parameter
 * @param param3 value of the parameter
 * @param param4 value of the parameter
 * @param param5 value of the parameter
 * @param param6 value of the parameter
 * @param param7 value of the parameter
 * @param param8 value of the parameter
 * @param param9 value of the parameter
 */
getMessage = function(messageKey, param1, param2, param3, param4, param5, param6, param7, param8, param9) { 
    var params = new Array(9);
	params[0]  = "" + param1;
	params[1]  = "" + param2;
	params[2]  = "" + param3;
	params[3]  = "" + param4;
	params[4]  = "" + param5;
	params[5]  = "" + param6;
	params[6]  = "" + param7;
	params[7]  = "" + param8;
	params[8]  = "" + param9;
	return getMessageWithParameters(messageKey, params);
}


/**
 * Convert a Boolean to a a local message
 * @param booleanObject the boolean object to convert
 * @param trueMessageKey message key for true value
 * @param falseMessageKey message key for false value
 */
translateBoolean = function(booleanObject, trueMessageKey, falseMessageKey) {
    var result = eval(booleanObject);
    if (result){
    	return getMessage(trueMessageKey);
    }
    return getMessage(falseMessageKey);
}
