package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.io.File;
import java.util.List;

public class ReferentialTemporaryVesselServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbCGFS");

    protected File dataDirectory;

    protected ReferentialTemporaryVesselService service;

    private static final String VESSEL_FILE_CONTENT =
            "id;name;internationalRegistrationCode;scientificVessel;toDelete\n" +
            ";Temporary fishing vessel name 1;International registration code F1;N;\n" +
            ";Temporary fishing vessel name 2;International registration code F2;N;\n" +
            ";Temporary scientific vessel name 3;International registration code S3;Y;\n" +
            ";Temporary scientific vessel name 4;International registration code S4;Y;";

    private static final String DUPLICATE_VESSEL_FILE_CONTENT =
            "id;name;internationalRegistrationCode;scientificVessel;toDelete\n" +
            ";Temporary fishing vessel name 1;International registration code F1;N;\n" +
            ";Temporary fishing vessel name 1;International registration code F1;N;\n" +
            ";Temporary fishing vessel name 2;International registration code F2;N;\n" +
            ";Temporary scientific vessel name 3;International registration code S3;Y;\n" +
            ";Temporary scientific vessel name 4;International registration code S4;Y;";


    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        serviceContext.getConfig().setCsvSeparator(';');

        service = serviceContext.getService(ReferentialTemporaryVesselService.class);

    }

    @Test
    public void importTemporaryVessel() throws Exception {
        File file = new File(dataDirectory, "importVessel.csv");

        Files.createParentDirs(file);

        Files.write(VESSEL_FILE_CONTENT, file, Charsets.UTF_8);

        ReferentialImportResult<Vessel> result = service.importTemporaryVessel(file);
        List<Vessel> addedVessels = result.getRefAdded();

        Assert.assertNotNull(result);
        Assert.assertEquals(4, addedVessels.size());
        for (int i = 1; i <= 2; i++) {
            Vessel actual = addedVessels.get(i - 1);
            Assert.assertNotNull(actual);
            Assert.assertEquals("#TEMP¿" + actual.getInternationalRegistrationCode(), actual.getId());
            Assert.assertEquals("Temporary fishing vessel name " + i, actual.getName());
            Assert.assertEquals("International registration code F" + i, actual.getInternationalRegistrationCode());
            Assert.assertFalse(actual.isScientificVessel());
        }
        for (int i = 3; i <= 4; i++) {
            Vessel actual = addedVessels.get(i - 1);
            Assert.assertNotNull(actual);
            Assert.assertEquals("#TEMP¿" + actual.getInternationalRegistrationCode(), actual.getId());
            Assert.assertEquals("Temporary scientific vessel name " + i, actual.getName());
            Assert.assertEquals("International registration code S" + i, actual.getInternationalRegistrationCode());
            Assert.assertTrue(actual.isScientificVessel());
        }

        // try to reimport them
        try {
            service.importTemporaryVessel(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }

        // TODO test to replace a used vessel
    }

    @Test
    public void importDuplicateTemporaryVessel() throws Exception {
        File file = new File(dataDirectory, "importVessel.csv");

        Files.createParentDirs(file);

        Files.write(DUPLICATE_VESSEL_FILE_CONTENT, file, Charsets.UTF_8);

        try {
            service.importTemporaryVessel(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            // good duplicate vessel
        }
    }


}