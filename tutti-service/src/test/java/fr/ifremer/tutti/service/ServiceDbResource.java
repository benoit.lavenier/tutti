package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.TuttiConfigurationOption;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.runner.Description;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.i18n.I18n;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class ServiceDbResource extends DatabaseResource {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ServiceDbResource.class);

    public void assertZipContent(String message,
                                 File zipFile,
                                 Pair<String, String>... entryAndExpectedContent) throws IOException {
        Assert.assertTrue(zipFile.exists());

        File explodeDir = ApplicationIOUtil.explodeZip(
                getConfig().getTmpDirectory(),
                zipFile,
                "could not explode archive zip");
        for (Pair<String, String> stringStringPair : entryAndExpectedContent) {
            String entry = stringStringPair.getKey();
            File explodeFile = new File(explodeDir, entry);
            Assert.assertTrue(zipFile.exists());

            String expectedContent = stringStringPair.getValue();

            assertFileContent(message + " - " + entry + "\n", explodeFile, expectedContent);
        }
    }

    public static void assertFileContent(String message,
                                         File actualFile,
                                         String expectedContent) throws IOException {
        Assert.assertTrue(actualFile.exists());
        String fileContent = Files.toString(actualFile,
                                            Charsets.UTF_8).trim();
        Assert.assertEquals(expectedContent, fileContent);

        if (log.isInfoEnabled()) {
            log.info(message + fileContent);
        }
    }

//    public static void assertFileContent(String message,
//                                         URL actualFile,
//                                         String expectedContent) throws IOException {
//
//        Assert.assertNotNull(actualFile);
//        InputStream inputStream = actualFile.openStream();
//        Assert.assertNotNull(inputStream);
//        String fileContent = IOUtils.toString(inputStream, Charsets.UTF_8).trim();
//        Assert.assertEquals(expectedContent, fileContent);
//
//        if (log.isInfoEnabled()) {
//            log.info(message + fileContent);
//        }
//    }

    //    protected TuttiServiceContext createServiceContext(RessourceClassLoader loader,
    protected TuttiServiceContext createServiceContext(TuttiConfiguration config) {
//        return new TuttiServiceContext(loader, config) {
        return new TuttiServiceContext(config) {

            Map<Class<?>, Integer> counts = Maps.newHashMap();

            @Override
            public String generateId(Class<?> type) {
                Integer count = counts.get(type);
                if (count == null) {
                    count = 0;
                }
                count++;
                counts.put(type, count);
                return type.getSimpleName() + "_" + count;
            }

            @Override
            public <S extends TuttiService> S getService(Class<S> serviceType) {
                S service = super.getService(serviceType);
                if (PersistenceService.class.equals(serviceType)) {

                    ((PersistenceService)service).setSkipShutdownDbWhenClosing();

//                    if (isUseLegacyPersonDepartement()) {
//
//                        if (log.isWarnEnabled()) {
//                            log.warn("Use legacy departement codes...");
//                        }
//                        DepartmentId.UNKNOWN_RECORDER_DEPARTMENT.setValue(181);
//                        DepartmentCode.INSIDE_PREFIX.setValue("PDG-");
//
//                    }
                }
                return service;
            }
        };
    }

    @Override
    public void prepareConfig(ApplicationConfig applicationConfig,
                              File resourceDirectory) {

        TuttiConfiguration.getDefaultApplicationConfig(applicationConfig);

        applicationConfig.setDefaultOption(
                TuttiConfigurationOption.BASEDIR.getKey(),
                resourceDirectory.getAbsolutePath());

        if (withDb() && !isWriteDb()) {

            // set tutti.persistence.db.directory
            File dbDirectory = FileUtil.getFileFromPaths(new File("src"), "test", "data", getDbName());

            applicationConfig.setDefaultOption(TuttiConfigurationOption.DB_DIRECTORY.getKey(),
                                               dbDirectory.getAbsolutePath());

            // set tutti.persistence.jdbc.url
            applicationConfig.setDefaultOption(TuttiConfigurationOption.JDBC_URL.getKey(),
                                               String.format("jdbc:hsqldb:file:%s/allegro", dbDirectory.getAbsolutePath()));

        }
    }

    protected TuttiServiceContext serviceContext;

//    public static ServiceDbResource readDb() {
//        return new ServiceDbResource("");
//    }

    public static ServiceDbResource writeDb() {
        return new ServiceDbResource("", true);
    }

    public static ServiceDbResource readDb(String dbName) {
        return new ServiceDbResource(dbName);
    }

    public static ServiceDbResource writeDb(String dbName) {
        return new ServiceDbResource(dbName, true);
    }

    public static ServiceDbResource noDb() {
        return new ServiceDbResource(
                null, "beanRefFactoryWitNoDb.xml", "TuttiBeanRefFactoryWithNoDb");
    }

    protected ServiceDbResource(String dbName) {
        super(dbName);
    }

    protected ServiceDbResource(String dbName, boolean writeDb) {
        super(dbName, writeDb);
    }

    protected ServiceDbResource(String dbName, String beanFactoryReferenceLocation, String beanRefFactoryReferenceId) {
        super(dbName, beanFactoryReferenceLocation, beanRefFactoryReferenceId);
    }

    protected ServiceDbResource(String dbName, String beanFactoryReferenceLocation, String beanRefFactoryReferenceId, boolean writeDb) {
        super(dbName, beanFactoryReferenceLocation, beanRefFactoryReferenceId, writeDb);
    }

    public TuttiServiceContext getServiceContext() {
        return serviceContext;
    }

    public void loadInternalProtocolFile() throws IOException {

        loadInternalProtocolFile(null, "tuttiProtocol");

    }

    public void loadInternalProtocolFile(String path, String protocolName) throws IOException {

        String protocolFilename = protocolName + ".tuttiProtocol";
        String classPath = "";
        if (path != null) {
            classPath = path;
            if (!path.endsWith("/")) {
                classPath += "/";
            }
        }
        classPath += protocolFilename;

        File protocol = copyClassPathResource(classPath, protocolFilename);
        getConfig().getApplicationConfig().setOption(TuttiConfigurationOption.DB_PROTOCOL_DIRECTORY.getKey(), protocol.getParentFile().getAbsolutePath());
        getServiceContext().getDataContext().setProtocolId(protocolName);
    }

    public TuttiProtocol loadProtocol(File protocolFile) {
        Assert.assertTrue(protocolFile.exists());

        ProtocolImportExportService protocolImportExportService =
                getServiceContext().getService(ProtocolImportExportService.class);

        return protocolImportExportService.importProtocol(protocolFile);
    }

    @Override
    protected void before(Description description) throws Throwable {

        super.before(description);

        serviceContext = createServiceContext(getConfig());

        I18n.init(null, Locale.FRANCE);
        serviceContext.getConfig().setCsvSeparator(';');

    }

    @Override
    protected void closeSpring() {
        IOUtils.closeQuietly(serviceContext);
        if (beanFactoryReferenceLocation != null) {

            // push back default tutti configuration
            TuttiPersistenceServiceLocator.initTuttiDefault();
        }
    }

    public DataContext loadContext(String programId, Integer cruiseId, int nbExpectedOperations, Integer... expectedOperationId) {

        DataContext dataContext = new DataContext();
        PersistenceService persistenceService = getServiceContext().getService(PersistenceService.class);

        Program program = persistenceService.getProgram(programId);
        Assert.assertNotNull(program);

        Cruise cruise = persistenceService.getCruise(cruiseId);
        Assert.assertNotNull(cruise);

        List<FishingOperation> operations = persistenceService.getAllFishingOperation(cruise.getIdAsInt());
        Assert.assertNotNull(operations);
        Assert.assertEquals(nbExpectedOperations, operations.size());
        if (expectedOperationId != null) {
            int index = 0;
            for (Integer id : expectedOperationId) {
                FishingOperation fishingOperation = operations.get(index);
                Assert.assertEquals("Fishign operation at index " + (index++) + " should have id " + id + " but was " + fishingOperation.getId(), id, fishingOperation.getIdAsInt());
            }
        }

        // load fully operations
        List<FishingOperation> loadedOperations =
                Lists.newArrayListWithCapacity(operations.size());
        for (FishingOperation operation : operations) {
            FishingOperation loadedOeration =
                    persistenceService.getFishingOperation(operation.getIdAsInt());
            loadedOperations.add(loadedOeration);
        }
        operations = loadedOperations;

        dataContext.program = program;
        dataContext.cruise = cruise;
        dataContext.operations = operations;

        return dataContext;
    }

    public DataContext loadContext(String programId) {

        DataContext dataContext = new DataContext();
        PersistenceService persistenceService = getServiceContext().getService(PersistenceService.class);

        Program program = persistenceService.getProgram(programId);
        Assert.assertNotNull(program);

        dataContext.program = program;

        return dataContext;
    }

    public void openDataContext() {

        TuttiServiceContext serviceContext = getServiceContext();

        PersistenceService persistenceService = serviceContext.getService(PersistenceService.class);
        serviceContext.getDataContext().setPersistenceService(persistenceService);
        serviceContext.getDataContext().open(serviceContext.getConfig());
    }

    public TuttiServiceContext setCountryInConfig(String countryCode) {
        TuttiServiceContext serviceContext = getServiceContext();

        PersistenceService persistenceService = serviceContext.getService(PersistenceService.class);

        // set export country id in configuration
        List<TuttiLocation> allCountry = persistenceService.getAllCountry();
        Assert.assertNotNull(allCountry);
        Assert.assertFalse(allCountry.isEmpty());
        TuttiLocation franceCountry = TuttiEntities.splitById(allCountry).get(countryCode);
        Assert.assertNotNull(franceCountry);
        getConfig().setExportCountry(franceCountry.getId());
        return serviceContext;
    }

    public static class DataContext {

        public Program program;

        public Cruise cruise;

        public List<FishingOperation> operations;

    }
}
