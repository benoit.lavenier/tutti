package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.io.File;
import java.util.List;

public class ReferentialTemporaryPersonServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbCGFS");

    protected File dataDirectory;

    protected ReferentialTemporaryPersonService service;

    private static final String PERSON_FILE_CONTENT =
            "id;firstName;lastName;toDelete\n" +
            ";First name 1;Last name 1;\n" +
            ";First name 2;Last name 2;\n" +
            ";First name 3;Last name 3;";

    private static final String PERSON_UPDATE_FILE_CONTENT =
            "id;firstName;lastName;toDelete\n" +
            "-1111;First name 11;Last name 11;N\n" +
            "-2222;;;Y";

    private static final String PERSON_DELETE_USED_FILE_CONTENT =
            "id;firstName;lastName;toDelete\n" +
            "-1;First name 11;Last name 11;Y";


    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        serviceContext.getConfig().setCsvSeparator(';');

        service = serviceContext.getService(ReferentialTemporaryPersonService.class);

    }

    @Test
    public void importTemporaryPerson() throws Exception {
        File file = new File(dataDirectory, "importPerson.csv");

        Files.createParentDirs(file);

        Files.write(PERSON_FILE_CONTENT, file, Charsets.UTF_8);

        // successful import
        ReferentialImportResult<Person> result = service.importTemporaryPerson(file);
        List<Person> addedPersons = result.getRefAdded();

        Assert.assertNotNull(result);
        Assert.assertEquals(3, addedPersons.size());
        for (int i = 1; i <= 3; i++) {
            Person actual = addedPersons.get(i - 1);
            Assert.assertNotNull(actual);
            Assert.assertEquals("-" + i, actual.getId());
            Assert.assertEquals("First name " + i, actual.getFirstName());
            Assert.assertEquals("Last name " + i, actual.getLastName());
        }

        // TODO test to replace a used person
    }

    @Test
    public void importNotExistingIdTemporaryPerson() throws Exception {

        File file = new File(dataDirectory, "importPerson.csv");

        Files.createParentDirs(file);

        // try to import not existing id
        Files.write(PERSON_UPDATE_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporaryPerson(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }
    }


}