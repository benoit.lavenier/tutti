package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0-rc-2
 */
public class PupitriImportServiceAno3898Test extends PupitryImportServiceTestSupport {

    @Test
    public void importPupitri() throws IOException {

        File trunk = dbResource.copyClassPathResource("pupitri/ano-3898.tnk", "pupitri.tnk");
        File carroussel = dbResource.copyClassPathResource("pupitri/ano-3898.car", "pupitri.car");
        dbResource.loadInternalProtocolFile("pupitri/", "ano-3898");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(0, rootSpeciesBatch.sizeChildren());

        PupitriImportResult pupitriImportResult = service.importPupitri(trunk, carroussel, operation, catchBatch);
        Assert.assertTrue(pupitriImportResult.isFishingOperationFound());
        int nbNotAdded = pupitriImportResult.getNbCarrousselNotImported();
        Assert.assertEquals(0, nbNotAdded);
        Set<String> notImportedSpeciesIds = pupitriImportResult.getNotImportedSpeciesIds();
        Assert.assertNotNull(notImportedSpeciesIds);
        Assert.assertTrue(notImportedSpeciesIds.isEmpty());

        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(31, rootSpeciesBatchAfter.sizeChildren());

        Decorator<Species> speciesDecorator = decoratorService.getDecoratorByType(Species.class);

        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            Integer nbChildren = null;
            // species 17003 (MELAAEG) has two childs (SMALL - BIG)
            if (17003 == speciesBatch.getSpecies().getIdAsInt()) {
                nbChildren = 2;
                // species 37312 (LOLIFOR) has no childs (in puttitri has some DEFAULT AND SMALL)
            } else if (37312 == speciesBatch.getSpecies().getIdAsInt()) {
                nbChildren = 0;
                // species 17005 (MERNMER) has no childs (in puttitri has some DEFAULT AND SMALL)
            } else if (17005 == speciesBatch.getSpecies().getIdAsInt()) {
                nbChildren = 0;
            }

            if (nbChildren != null) {
                Assert.assertEquals("SpeciesBatch with species " + speciesDecorator.toString(speciesBatch.getSpecies()) + " should have with " + nbChildren + " but had " + speciesBatch.sizeChildBatchs(), nbChildren.intValue(), speciesBatch.sizeChildBatchs());
            }

        }
    }
}
