package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.io.File;
import java.util.List;

public class ReferentialTemporarySpeciesServiceTest {

    private static final String SPECIES_FILE_CONTENT =
            "id;name;toDelete\n" +
            ";Temporary Species name 1;\n" +
            ";Temporary Species name 2;\n" +
            ";Temporary Species name 3;";

    private static final String SPECIES_DUPLICATE_NAME_FILE_CONTENT =
            "id;name;toDelete\n" +
            ";Temporary Species name 1;\n" +
            ";Temporary Species name 1;";

    private static final String SPECIES_BLANK_NAME_FILE_CONTENT =
            "id;name;toDelete\n" +
            "; ;";

    private static final String SPECIES_UPDATE_FILE_CONTENT =
            "id;name;toDelete\n" +
            "-1;Temporary Species name 11;N\n" +
            "-2;Temporary Species name 2;Y";

    private static final String SPECIES_DELETE_USED_FILE_CONTENT =
            "id;name;toDelete\n" +
            "-1;Temporary Species name 11;Y";

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbCGFS");

    protected File dataDirectory;


    protected ReferentialTemporarySpeciesService service;

    protected PersistenceService persistenceService;

    public static final Integer OPERATION_1_ID = 100108;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        serviceContext.getConfig().setCsvSeparator(';');

        service = serviceContext.getService(ReferentialTemporarySpeciesService.class);

        persistenceService = serviceContext.getService(PersistenceService.class);

    }

    @Test
    public void importTemporarySpecies() throws Exception {

        File file = new File(dataDirectory, "importSpecies.csv");

        Files.createParentDirs(file);

        // successful import
        Files.write(SPECIES_FILE_CONTENT, file, Charsets.UTF_8);

        ReferentialImportResult<Species> result = service.importTemporarySpecies(file);
        List<Species> addedSpecies = result.getRefAdded();

        Assert.assertNotNull(result);
        Assert.assertEquals(3, addedSpecies.size());
        for (int i = 1; i <= 3; i++) {
            Species actual = addedSpecies.get(i - 1);
            Assert.assertNotNull(actual);
            Assert.assertEquals("-" + i, actual.getId());
            Assert.assertEquals("Temporary Species name " + i, actual.getName());
        }
        // try to reimport them
        try {
            service.importTemporarySpecies(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }

        // try to update species with id -1 and remove id -2
        Assert.assertNotNull(persistenceService.getSpeciesByReferenceTaxonId(-1));
        Assert.assertNotNull(persistenceService.getSpeciesByReferenceTaxonId(-2));

        Files.write(SPECIES_UPDATE_FILE_CONTENT, file, Charsets.UTF_8);
        result = service.importTemporarySpecies(file);
        List<Species> updatedSpecies = result.getRefUpdated();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, updatedSpecies.size());
        Species actual = updatedSpecies.get(0);
        Assert.assertNotNull(actual);
        Assert.assertEquals("-1", actual.getId());
        Assert.assertEquals("Temporary Species name 11", actual.getName());

        Assert.assertNull(persistenceService.getSpeciesByReferenceTaxonId(-2));

        // try to delete used species
        SpeciesBatch batch = persistenceService.getRootSpeciesBatch(OPERATION_1_ID, false).getChildren().get(0);
        batch.setFishingOperation(persistenceService.getFishingOperation(OPERATION_1_ID));
        batch.setSpecies(persistenceService.getSpeciesByReferenceTaxonId(-1));
        persistenceService.saveSpeciesBatch(batch);

        Files.write(SPECIES_DELETE_USED_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporarySpecies(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }

    }

    @Test
    public void importNotExistingIdTemporarySpecies() throws Exception {

        File file = new File(dataDirectory, "importSpecies.csv");

        Files.createParentDirs(file);

        // try to import not existing id
        Files.write(SPECIES_UPDATE_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporarySpecies(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void importBlankNameTemporarySpecies() throws Exception {

        File file = new File(dataDirectory, "importSpecies.csv");

        Files.createParentDirs(file);
        // try to import blank name
        Files.write(SPECIES_BLANK_NAME_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporarySpecies(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void importDuplicateTemporarySpecies() throws Exception {

        File file = new File(dataDirectory, "importSpecies.csv");

        Files.createParentDirs(file);

        // try to import duplicate names
        Files.write(SPECIES_DUPLICATE_NAME_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporarySpecies(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }
    }

}