package fr.ifremer.tutti.service.bigfin;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class BigfinImportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbImportBigfin");

    /** Logger. */
    private static final Log log = LogFactory.getLog(BigfinImportServiceTest.class);

    public static final String PROGRAM_ID = "CAM-TEST_";

    public static final Integer CRUISE_ID = 0;

    public static final Integer OPERATION_1_ID = 0;

    protected BigfinImportService service;

    protected PersistenceService persistenceService;

    protected ServiceDbResource.DataContext dataContext;

    protected File dataDirectory;

    protected Predicate<SpeciesBatch> vracPredicate;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        dbResource.loadInternalProtocolFile();

        dbResource.openDataContext();

        persistenceService = serviceContext.getService(PersistenceService.class);
        service = serviceContext.getService(BigfinImportService.class);

        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_ID, 1, OPERATION_1_ID);

        vracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(PmfmId.SORTED_UNSORTED.getValue(), QualitativeValueId.VRAC.getValue());
    }

    @Test
    public void importValid() throws IOException {

        File importFile = dbResource.copyClassPathResource("bigfin/importbigfin-valid.csv", "importbigfin-valid.csv");

        FishingOperation operation = dataContext.operations.get(0);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BigfinImportResult importResult = service.importFile(importFile, operation, catchBatch);

        int nbFrequenciesAdded = importResult.getNbFrequenciesImported();
        List<String> errors = importResult.getErrors();
        List<String> warnings = importResult.getWarnings();

        if (log.isInfoEnabled()) {
            log.info("Frequencies Imported: " + nbFrequenciesAdded);
            log.info("Errors: " + errors.size());
            log.info("Warnings: " + warnings.size());
        }

        int nbNewFrequencies = 32;
        Assert.assertEquals(nbNewFrequencies, nbFrequenciesAdded);
        Assert.assertEquals(0, errors.size());
        Assert.assertEquals(0, warnings.size());

        // no batch imported
        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);

        int totalSortedBatchs = 0;
        int totalUnsortedBatchs = 0;
        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            boolean sorted = vracPredicate.apply(speciesBatch);

            if (sorted) {
                totalSortedBatchs++;
            } else {
                totalUnsortedBatchs++;
            }
        }

        Assert.assertEquals(4, totalSortedBatchs);
        Assert.assertEquals(3, totalUnsortedBatchs);
    }

    @Test
    public void importWithErrors() throws IOException {

        File importFile = dbResource.copyClassPathResource("bigfin/importbigfin-errors.csv", "importbigfin-invalid-errors.csv");

        FishingOperation operation = dataContext.operations.get(0);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BigfinImportResult importResult = service.importFile(importFile, operation, catchBatch);

        int nbFrequenciesAdded = importResult.getNbFrequenciesImported();
        List<String> errors = importResult.getErrors();
        List<String> warnings = importResult.getWarnings();


        if (log.isInfoEnabled()) {
            log.info("Frequencies Imported: " + nbFrequenciesAdded);
            log.info("Errors: " + errors.size());
            log.info("Warnings: " + warnings.size());
        }

        int nbNewFrequencies = 0;
        Assert.assertEquals(nbNewFrequencies, nbFrequenciesAdded);
        Assert.assertEquals(2, errors.size());
        Assert.assertEquals(0, warnings.size());

        TuttiServiceContext serviceContext = dbResource.getServiceContext();
        serviceContext.getDataContext().resetProtocol();

        boolean failIfNoProtocol = false;
        try {
            service.importFile(importFile, operation, catchBatch);
        } catch (ApplicationBusinessException e) {
            if (log.isErrorEnabled()) {
                log.error("Error during import: " + e.getMessage());
            }
            failIfNoProtocol = true;
        }
        Assert.assertTrue(failIfNoProtocol);
    }

    @Test
    public void importWithWarnings() throws IOException {

        File importFile = dbResource.copyClassPathResource("bigfin/importbigfin-warnings.csv", "importbigfin-invalid-warnings.csv");

        FishingOperation operation = dataContext.operations.get(0);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BigfinImportResult importResult = service.importFile(importFile, operation, catchBatch);

        int nbFrequenciesAdded = importResult.getNbFrequenciesImported();
        List<String> errors = importResult.getErrors();
        List<String> warnings = importResult.getWarnings();

        if (log.isInfoEnabled()) {
            log.info("Frequencies Imported: " + nbFrequenciesAdded);
            log.info("Errors: " + errors.size());
            log.info("Warnings: " + warnings.size());
        }

        int nbNewFrequencies = 1;
        Assert.assertEquals(nbNewFrequencies, nbFrequenciesAdded);
        Assert.assertEquals(0, errors.size());
        Assert.assertEquals(3, warnings.size());

    }
}
