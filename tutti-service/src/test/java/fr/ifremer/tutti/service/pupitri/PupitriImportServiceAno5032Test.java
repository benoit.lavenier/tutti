package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0-rc-2
 */
public class PupitriImportServiceAno5032Test extends PupitryImportServiceTestSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PupitriImportServiceAno5032Test.class);

    @Test
    public void importPupitri() throws IOException {

        File trunk = dbResource.copyClassPathResource("pupitri/ano-5032.tnk", "pupitri.tnk");
        File carroussel = dbResource.copyClassPathResource("pupitri/ano-5032.car", "pupitri.car");
        dbResource.loadInternalProtocolFile("pupitri/", "ano-5032");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(0, rootSpeciesBatch.sizeChildren());

        PupitriImportResult pupitriImportResult = service.importPupitri(trunk, carroussel, operation, catchBatch);
        Assert.assertTrue(pupitriImportResult.isFishingOperationFound());
        int nbNotAdded = pupitriImportResult.getNbCarrousselNotImported();
        Assert.assertEquals(4, nbNotAdded);
        Set<String> notImportedSpeciesIds = pupitriImportResult.getNotImportedSpeciesIds();
        Assert.assertNotNull(notImportedSpeciesIds);
        Assert.assertTrue(notImportedSpeciesIds.contains("MERL-MNG"));
        Assert.assertTrue(notImportedSpeciesIds.contains("ACANPEL"));
        Assert.assertTrue(notImportedSpeciesIds.contains("ACAN-PAL"));
        Assert.assertTrue(notImportedSpeciesIds.contains("ASRN-ATL"));

        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(4, rootSpeciesBatchAfter.sizeChildren());

        Set<Integer> unexpectedSpecies = Sets.newHashSet(
        );
        Set<Integer> expectedSpecies = Sets.newHashSet(
                17005, // MERNMER
                17186, // ACATPAL
                16328, // ACAPPEL
                20788  // ASRNATL
        );

        Decorator<Species> speciesDecorator = decoratorService.getDecoratorByType(Species.class);

        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            Species species = speciesBatch.getSpecies();
            Integer speciesId = species.getIdAsInt();
            boolean remove = expectedSpecies.remove(speciesId);
            if (log.isInfoEnabled()) {
                log.info("Species : " + speciesId + " : " + speciesDecorator.toString(species));
            }
            if (!remove) {
                unexpectedSpecies.add(speciesId);
                if (log.isWarnEnabled()) {
                    log.warn("Unexpected Species " + speciesId);
                }
            }
        }

        Assert.assertTrue("Expected species not found: " + expectedSpecies, expectedSpecies.isEmpty());
        Assert.assertTrue("Unexpected species found: " + unexpectedSpecies, unexpectedSpecies.isEmpty());
    }
}
