package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristics;
import fr.ifremer.tutti.service.ServiceDbResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.csv.ImportRuntimeException;

import java.io.File;
import java.util.Map;

import static fr.ifremer.tutti.service.protocol.ProtocolImportExportServiceTest.ALL_CARACTERISTIC_FILE_CONTENT;
import static fr.ifremer.tutti.service.protocol.ProtocolImportExportServiceTest.PROTOCOL_CARACTERISTIC_FILE_BAD_CONTENT;
import static fr.ifremer.tutti.service.protocol.ProtocolImportExportServiceTest.PROTOCOL_CARACTERISTIC_FILE_CONTENT;

/**
 * Created on 11/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ProtocolCaracteristicsImportExportServiceTest {


    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbCGFS");

    protected ProtocolCaracteristicsImportExportService service;

    protected File datadirectory;

    @Before
    public void setUp() throws Exception {

        dbResource.getConfig().setCsvSeparator(';');

        service = dbResource.getServiceContext().getService(ProtocolCaracteristicsImportExportService.class);

        datadirectory = dbResource.getConfig().getDataDirectory();
    }

    @Test
    public void importProtocolCaracteristic() throws Exception {

        File file = new File(datadirectory, "importProtocolCaracteristic.csv");

        Files.createParentDirs(file);

        Files.write(PROTOCOL_CARACTERISTIC_FILE_CONTENT, file, Charsets.UTF_8);

        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        Map<String, Caracteristic> caracteristicMap = createCaracteristics();

        service.importProtocolCaracteristic(file, protocol, caracteristicMap);

        Assert.assertEquals(Lists.newArrayList("1"),
                            protocol.getLengthClassesPmfmId());
        Assert.assertEquals(Lists.newArrayList(
                TuttiProtocols.caracteristicMappingRow("2", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("3", CaracteristicType.GEAR_USE_FEATURE)
                            ),
                            protocol.getCaracteristicMapping());
        Assert.assertEquals(Lists.newArrayList("4"),
                            protocol.getIndividualObservationPmfmId());
    }

    @Test(expected = ImportRuntimeException.class)
    public void importProtocolCaracteristicMissingCaracteristicType() throws Exception {

        File file = new File(datadirectory, "importProtocolCaracteristicMissingCaracteristicType.csv");

        Files.createParentDirs(file);

        Files.write(PROTOCOL_CARACTERISTIC_FILE_BAD_CONTENT, file, Charsets.UTF_8);

        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        Map<String, Caracteristic> caracteristicMap = createCaracteristics();

        service.importProtocolCaracteristic(file, protocol, caracteristicMap);
    }

    @Test
    public void exportProtocolCaracteristic() throws Exception {

        File file = new File(datadirectory, "exportProtocolCaracteristic.csv");

        Files.createParentDirs(file);

        Map<String, Caracteristic> caracteristicMap = createCaracteristics();
        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();

        protocol.setLengthClassesPmfmId(Lists.newArrayList("1"));
        protocol.setCaracteristicMapping(Lists.newArrayList(
                TuttiProtocols.caracteristicMappingRow("2", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("3", CaracteristicType.GEAR_USE_FEATURE)
        ));
        protocol.setIndividualObservationPmfmId(Lists.newArrayList("4"));

        Assert.assertFalse(file.exists());
        service.exportProtocolCaracteristic(file, protocol, caracteristicMap);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(PROTOCOL_CARACTERISTIC_FILE_CONTENT, exportFileToString);
    }

    @Test
    public void exportAllCaracteristic() throws Exception {

        File file = new File(datadirectory, "exportAllCaracteristic.csv");

        Files.createParentDirs(file);

        Map<String, Caracteristic> caracteristicMap = createCaracteristics();

        Assert.assertFalse(file.exists());
        service.exportAllCaracteristic(file, caracteristicMap);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(ALL_CARACTERISTIC_FILE_CONTENT, exportFileToString);
    }

    protected Map<String, Caracteristic> createCaracteristics() {
        Map<String, Caracteristic> result = Maps.newTreeMap();
        for (int i = 1; i < 6; i++) {
            Caracteristic c = Caracteristics.newCaracteristic();
            c.setId("" + i);
            c.setParameterName("parameterName" + i);
            c.setMatrixName("matrixName" + i);
            c.setFractionName("fractionName" + i);
            c.setMethodName("methodName" + i);
            result.put(i + "", c);
        }
        return result;
    }

}