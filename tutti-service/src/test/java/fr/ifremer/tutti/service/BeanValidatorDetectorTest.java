/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.tutti.service;

import fr.ifremer.tutti.persistence.entities.data.*;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.validator.AbstractValidatorDetectorTest;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

import java.io.File;
import java.util.SortedSet;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class BeanValidatorDetectorTest extends AbstractValidatorDetectorTest {

    private static final String EDIT_CONTEXT_NAME = "edit";

    private static final String VALIDATE_CONTEXT_NAME = "validate";

    SortedSet<NuitonValidator<?>> validators;

    public BeanValidatorDetectorTest() {
        super(XWork2NuitonValidatorProvider.PROVIDER_NAME);
    }

    static Class<?>[] ALL_TYPES;

    @BeforeClass
    public static void setUpClass() throws Exception {
        ALL_TYPES = new Class[]{
                AccidentalBatch.class,
                CatchBatch.class,
                Cruise.class,
                FishingOperation.class,
                IndividualObservationBatch.class,
                MarineLitterBatch.class,
                Program.class,
                SpeciesBatch.class,
                TuttiProtocol.class
        };
    }

    @Override
    protected File getRootDirectory(File basedir) {
        return new File(basedir,
                        "src" + File.separator + "main" + File.separator + "resources");
    }

    @Test
    public void detectAll() {

        SortedSet<NuitonValidator<?>> validators = detectValidators(ALL_TYPES);
        assertFalse(validators.isEmpty());
        assertEquals(15, validators.size());
    }

    @Test
    public void detectWithNoContext() {

        String contextName = "";

        validators = detectValidators(Pattern.compile(contextName), ALL_TYPES);

        assertValidatorSetWithSameContextName(validators, null,
                                              TuttiProtocol.class
        );
    }

    @Test
    public void detectWithEditContext() {

        String contextName = EDIT_CONTEXT_NAME;

        validators = detectValidators(Pattern.compile(contextName), ALL_TYPES);

        assertValidatorSetWithSameContextName(validators,
                                              contextName,
                                              AccidentalBatch.class,
                                              CatchBatch.class,
                                              Cruise.class,
                                              FishingOperation.class,
                                              IndividualObservationBatch.class,
                                              MarineLitterBatch.class,
                                              Program.class,
                                              SpeciesBatch.class
        );
    }

    @Test
    public void detectWithValidateContext() {
        String contextName = VALIDATE_CONTEXT_NAME;

        validators = detectValidators(Pattern.compile(contextName), ALL_TYPES);

        assertValidatorSetWithSameContextName(validators,
                                              contextName,
                                              AccidentalBatch.class,
                                              CatchBatch.class,
                                              Cruise.class,
                                              FishingOperation.class,
                                              IndividualObservationBatch.class,
                                              SpeciesBatch.class
        );

    }
}
