package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.Programs;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.validator.NuitonValidatorResult;

import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ValidationServiceTest extends AbstractServiceTest {

    private ValidationService service;

    private TuttiDataValidationContextFake validationContext;

    @Override
    protected TuttiServiceContext createServiceContext(TuttiConfiguration config) {

        TuttiDataContext dataContext = new TuttiDataContext();
        dataContext.setValidationContext(new TuttiDataValidationContextFake(dataContext));

        return new TuttiServiceContext(config, dataContext);
    }

    @Before
    public void setUp() throws Exception {

        super.setUp();

        service = serviceContext.getService(ValidationService.class);
        validationContext = (TuttiDataValidationContextFake) serviceContext.getDataContext().getValidationContext();

    }

    @Test
    public void validateCruise() throws Exception {

        Cruise c = Cruises.newCruise();

        assertIsNotValid(service.validateEditCruise(c));

        c.setName("cName");
        c.setMultirigNumber(1);
        c.setBeginDate(serviceContext.currentDate());
        c.setEndDate(DateUtils.addDays(serviceContext.currentDate(), 1));
        c.setDepartureLocation(TuttiLocations.newTuttiLocation());
        c.setReturnLocation(TuttiLocations.newTuttiLocation());
        c.setProgram(Programs.newProgram());
        c.setVessel(Vessels.newVessel());
        c.setGear(Lists.newArrayList(GearWithOriginalRankOrders.newGearWithOriginalRankOrder()));
        c.setHeadOfMission(Lists.newArrayList(Persons.newPerson()));
        c.setHeadOfSortRoom(Lists.newArrayList(Persons.newPerson()));
        assertIsValid(service.validateEditCruise(c));
    }

    @Test
    public void validateProgram() throws Exception {

        Program p = Programs.newProgram();

        // no name / no zone / no description
        assertIsNotValid(service.validateProgram(p));

        p.setName("pName");
        p.setDescription("pDescription");
        TuttiLocation z = TuttiLocations.newTuttiLocation();
        z.setId("z0");
        z.setLabel("z0Label");
        p.setZone(z);

        // valid
        assertIsValid(service.validateProgram(p));

        // adding two programs
        Program p2 = Programs.newProgram();
        p2.setId("p2");
        p2.setName("pName");
        p2.setZone(z);
        Program p3 = Programs.newProgram();
        p3.setId("p3");
        p3.setName("pName2");
        p3.setZone(z);

        // now program in data context
        validationContext.setExistingPrograms(Lists.newArrayList(p2, p3));

        // no more valid
        assertIsNotValid(service.validateProgram(p));

        // change name of p
        p.setName("pName3");

        // back to valid
        assertIsValid(service.validateProgram(p));

        // reput bad name and same id of an existant p (so should be ok)
        p.setName("pName");
        p.setId("p2");

        // still valid
        assertIsValid(service.validateProgram(p));
    }

    @Test
    public void validateProtocol() throws Exception {

        TuttiProtocol p = TuttiProtocols.newTuttiProtocol();

        // no name
        assertIsNotValid(service.validateProtocol(p));

        p.setName("pName");

        // valid
        assertIsValid(service.validateProtocol(p));

        // add two protocol in dataContext
        TuttiProtocol p2 = TuttiProtocols.newTuttiProtocol();
        p2.setId("p2");
        p2.setName("pName");
        TuttiProtocol p3 = TuttiProtocols.newTuttiProtocol();
        p3.setId("p3");
        p3.setName("pName2");

        // now program in data context
        validationContext.setExistingProtocols(Lists.newArrayList(p2, p3));

        // no more valid
        assertIsNotValid(service.validateProtocol(p));

        // change name of p
        p.setName("pName3");

        // back to valid
        assertIsValid(service.validateProtocol(p));

        // reput bad name and same id of an existant p (so should be ok)
        p.setName("pName");
        p.setId("p2");

        // still valid
        assertIsValid(service.validateProtocol(p));
    }

    @Test
    public void validateFishingOperation() throws Exception {

        //TODO
    }


    private void assertIsNotValid(NuitonValidatorResult validatorResult) {
        Assert.assertNotNull(validatorResult);
        Assert.assertTrue(validatorResult.hasErrorMessagess());
        Assert.assertFalse(validatorResult.isValid());
    }

    private void assertIsValid(NuitonValidatorResult validatorResult) {
        Assert.assertNotNull(validatorResult);
        Assert.assertFalse(validatorResult.hasErrorMessagess());
        Assert.assertTrue(validatorResult.isValid());
    }

    class TuttiDataValidationContextFake extends TuttiValidationDataContext {

        List<Program> existingPrograms;

        List<TuttiProtocol> existingProtocols;

        public TuttiDataValidationContextFake(TuttiDataContext dataContext) {
            super(dataContext);
        }

        public void setExistingPrograms(List<Program> existingPrograms) {
            this.existingPrograms = existingPrograms;
            resetExistingPrograms();
        }

        public void setExistingProtocols(List<TuttiProtocol> existingProtocols) {
            this.existingProtocols = existingProtocols;
            resetExistingProtocols();
        }

        @Override
        protected List<Program> loadExistingPrograms() {
            return existingPrograms;
        }

        @Override
        protected List<TuttiProtocol> loadExistingProtocols() {
            return existingProtocols;
        }

    }

}
