package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 5/14/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.4.2
 */
public class PupitriImportServiceEvo5062Test extends PupitryImportServiceTestSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PupitriImportServiceEvo5062Test.class);

    @Test
    public void importPupitri() throws IOException {

        File trunk = dbResource.copyClassPathResource("pupitri/evo-5062.tnk", "pupitri.tnk");
        File carroussel = dbResource.copyClassPathResource("pupitri/evo-5062.car", "pupitri.car");
        dbResource.loadInternalProtocolFile("pupitri/", "evo-5062");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(0, rootSpeciesBatch.sizeChildren());

        PupitriImportResult pupitriImportResult = service.importPupitri(trunk, carroussel, operation, catchBatch);
        Assert.assertTrue(pupitriImportResult.isFishingOperationFound());
        int nbNotAdded = pupitriImportResult.getNbCarrousselNotImported();
        Assert.assertEquals(0, nbNotAdded);
        Set<String> notImportedSpeciesIds = pupitriImportResult.getNotImportedSpeciesIds();
        Assert.assertNotNull(notImportedSpeciesIds);
        Assert.assertTrue(notImportedSpeciesIds.isEmpty());

        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(9, rootSpeciesBatchAfter.sizeChildren());

        Set<Integer> unexpectedSpecies = Sets.newHashSet(
        );
        Set<Integer> expectedSpecies = Sets.newHashSet(

                18237, // EUPH-AUX
                16994, // MERL-MCC
                17116, // TRAC-TRU
                16816, // ENGR-ENC
                17374, // EUTR-GUR
                17226, // SCOM-SCO
                17007, // MICR-POU
                16869 // MYCT-PUN
        );

        Set<Integer> alreadyFound = new HashSet<>();

        Decorator<Species> speciesDecorator = decoratorService.getDecoratorByType(Species.class);

        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            Species species = speciesBatch.getSpecies();
            Integer speciesId = species.getIdAsInt();
            if (alreadyFound.contains(speciesId)) {

                // already found
                continue;

            }

            boolean remove = expectedSpecies.remove(speciesId);
            if (log.isInfoEnabled()) {
                log.info("Species : " + speciesId + " : " + speciesDecorator.toString(species));
            }
            if (remove) {

                // ok mark it as safe specieId
                alreadyFound.add(speciesId);

            }

            if (!remove) {
                unexpectedSpecies.add(speciesId);
                if (log.isWarnEnabled()) {
                    log.warn("Unexpected Species " + speciesId);
                }
            }
        }

        Assert.assertTrue("Expected species not found: " + expectedSpecies, expectedSpecies.isEmpty());
        Assert.assertTrue("Unexpected species found: " + unexpectedSpecies, unexpectedSpecies.isEmpty());
    }
}
