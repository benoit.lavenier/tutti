package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

/**
 * Created on 7/8/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class PersistenceServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.writeDb("dbReplaceTemporary");

    protected PersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = dbResource.getServiceContext().getService(PersistenceService.class);
    }

    @Test
    public void replaceGear() {

        /**
         * We us 2 temporary gear:
         *
         *  -3 (cruise.gear)
         *  -4 (cruise.gear + fishingOperation.gear)
         */

        {
            Cruise cruise = service.getCruise(0);
            assertContainsIds(cruise.getGear(), "-3", "-4");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsId(fishingOperation.getGear(), "-4");

            assertContainsCaracteristics(service.getGearCaracteristics(0, -4, (short) 1));
            assertContainsNoCaracteristics(service.getGearCaracteristics(0, -3, (short) 2));
        }

        { // replace -3 by 377
            Gear source = service.getGear(-3);
            Gear target = service.getGear(377);

            service.replaceGear(source, target, false);
        }

        {
            Cruise cruise = service.getCruise(0);
            assertContainsIds(cruise.getGear(), "377", "-4");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsId(fishingOperation.getGear(), "-4");

            assertContainsCaracteristics(service.getGearCaracteristics(0, -4, (short) 1));
            assertContainsNoCaracteristics(service.getGearCaracteristics(0, -3, (short) 2));
            assertContainsNoCaracteristics(service.getGearCaracteristics(0, 377, (short) 2));
        }

        { // replace -4 by 378
            Gear source = service.getGear(-4);
            Gear target = service.getGear(378);

            service.replaceGear(source, target, false);
        }

        {
            Cruise cruise = service.getCruise(0);
            assertContainsIds(cruise.getGear(), "377", "378");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsId(fishingOperation.getGear(), "378");

            assertContainsNoCaracteristics(service.getGearCaracteristics(0, -4, (short) 1));
            assertContainsCaracteristics(service.getGearCaracteristics(0, 378, (short) 1));
            assertContainsNoCaracteristics(service.getGearCaracteristics(0, -3, (short) 2));
            assertContainsNoCaracteristics(service.getGearCaracteristics(0, 377, (short) 2));
        }

    }

    @Test
    public void replacePerson() {

        /**
         * We us 3 temporary person:
         *
         * -1 (cruise.managerPerson)
         * -2 (cruise.recorderPerson)
         * -3 (fishingOperation.dataInputor + fishingOperation.dataInputor)
         */

        {
            Cruise cruise = service.getCruise(0);
            assertContainsIds(cruise.getHeadOfMission(), "-1");
            assertContainsIds(cruise.getHeadOfSortRoom(), "-2");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsIds(fishingOperation.getRecorderPerson(), "-2", "-3");
        }

        { // replace -1 by 1
            Person source = service.getPerson(-1);
            Person target = service.getPerson(1);

            service.replacePerson(source, target, false);
        }
        {
            service = dbResource.getServiceContext().getService(PersistenceService.class);

            Cruise cruise = service.getCruise(0);
            assertContainsIds(cruise.getHeadOfMission(), "1");
            assertContainsIds(cruise.getHeadOfSortRoom(), "-2");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsIds(fishingOperation.getRecorderPerson(), "-2", "-3");
        }

        { // replace -2 by 2
            Person source = service.getPerson(-2);
            Person target = service.getPerson(2);

            service.replacePerson(source, target, false);
        }
        {
            Cruise cruise = service.getCruise(0);
            assertContainsIds(cruise.getHeadOfMission(), "1");
            assertContainsIds(cruise.getHeadOfSortRoom(), "2");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsIds(fishingOperation.getRecorderPerson(), "2", "-3");
        }

        { // replace -3 by 3
            Person source = service.getPerson(-3);
            Person target = service.getPerson(3);

            service.replacePerson(source, target, false);
        }
        {
            Cruise cruise = service.getCruise(0);
            assertContainsIds(cruise.getHeadOfMission(), "1");
            assertContainsIds(cruise.getHeadOfSortRoom(), "2");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsIds(fishingOperation.getRecorderPerson(), "2", "3");
        }
    }

    @Test
    public void replaceSpecies() {

        /**
         * We us 3 temporary species:
         *
         * -1 (speciesCatch)
         * -2 (benthosCatch + accidentalCatch)
         * -3 (individualObservation)
         */

        {
            BatchContainer<SpeciesBatch> rootSpeciesBatch = service.getRootSpeciesBatch(0, false);
            assertSpeciesBatch(rootSpeciesBatch, "-1");

            BatchContainer<SpeciesBatch> rootBenthosBatch = service.getRootBenthosBatch(0, false);
            assertBenthosBatch(rootBenthosBatch, "-2");

            List<IndividualObservationBatch> allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(0);
            assertIndividualObservationBatch(allIndividualObservationBatch, "-3");
            List<AccidentalBatch> allAccidentalBatch = service.getAllAccidentalBatch(0);
            assertAccidentalBatch(allAccidentalBatch, "-2");

        }

        { // replace -1 by 1 (id=15461)
            Species source = service.getSpeciesByReferenceTaxonId(-1);
            Species target = service.getSpeciesByReferenceTaxonId(1);

            service.replaceSpecies(source, target, false);
        }

        {
            BatchContainer<SpeciesBatch> rootSpeciesBatch = service.getRootSpeciesBatch(0, false);
            assertSpeciesBatch(rootSpeciesBatch, "15461");

            BatchContainer<SpeciesBatch> rootBenthosBatch = service.getRootBenthosBatch(0, false);
            assertBenthosBatch(rootBenthosBatch, "-2");

            List<IndividualObservationBatch> allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(0);
            assertIndividualObservationBatch(allIndividualObservationBatch, "-3");
            List<AccidentalBatch> allAccidentalBatch = service.getAllAccidentalBatch(0);
            assertAccidentalBatch(allAccidentalBatch, "-2");

        }

        { // replace -2 by 2 (id=15462)
            Species source = service.getSpeciesByReferenceTaxonId(-2);
            Species target = service.getSpeciesByReferenceTaxonId(2);

            service.replaceSpecies(source, target, false);
        }

        {
            BatchContainer<SpeciesBatch> rootSpeciesBatch = service.getRootSpeciesBatch(0, false);
            assertSpeciesBatch(rootSpeciesBatch, "15461");

            BatchContainer<SpeciesBatch> rootBenthosBatch = service.getRootBenthosBatch(0, false);
            assertBenthosBatch(rootBenthosBatch, "15462");

            List<IndividualObservationBatch> allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(0);
            assertIndividualObservationBatch(allIndividualObservationBatch, "-3");
            List<AccidentalBatch> allAccidentalBatch = service.getAllAccidentalBatch(0);
            assertAccidentalBatch(allAccidentalBatch, "15462");
        }

        { // replace -3 by 3 (id=15463)
            Species source = service.getSpeciesByReferenceTaxonId(-3);
            Species target = service.getSpeciesByReferenceTaxonId(3);

            service.replaceSpecies(source, target, false);
        }

        {
            BatchContainer<SpeciesBatch> rootSpeciesBatch = service.getRootSpeciesBatch(0, false);
            assertSpeciesBatch(rootSpeciesBatch, "15461");

            BatchContainer<SpeciesBatch> rootBenthosBatch = service.getRootBenthosBatch(0, false);
            assertBenthosBatch(rootBenthosBatch, "15462");

            List<IndividualObservationBatch> allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(0);
            assertIndividualObservationBatch(allIndividualObservationBatch, "15463");
            List<AccidentalBatch> allAccidentalBatch = service.getAllAccidentalBatch(0);
            assertAccidentalBatch(allAccidentalBatch, "15462");
        }

    }

    @Test
    public void replaceVessel() {

        /**
         * We us 2 temporary vessel:
         *
         * #TEMP¿International registration code S3 (cruise.vessel + fishingOperation.vessel)
         * #TEMP¿International registration code S4 (fishingOperation.secondaryVessel)
         */

        {
            Cruise cruise = service.getCruise(0);
            assertContainsId(cruise.getVessel(), "#TEMP¿International registration code S3");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsId(fishingOperation.getVessel(), "#TEMP¿International registration code S3");
            assertContainsIds(fishingOperation.getSecondaryVessel(), "#TEMP¿International registration code S4");
        }

        { // replace #TEMP¿International registration code S3 by 267206
            Vessel source = service.getVessel("#TEMP¿International registration code S3");
            Vessel target = service.getVessel("267206");

            service.replaceVessel(source, target, false);
        }
        {
            Cruise cruise = service.getCruise(0);
            assertContainsId(cruise.getVessel(), "267206");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsId(fishingOperation.getVessel(), "267206");
            assertContainsIds(fishingOperation.getSecondaryVessel(), "#TEMP¿International registration code S4");
        }

        { // replace #TEMP¿International registration code S4 by 278970
            Vessel source = service.getVessel("#TEMP¿International registration code S4");
            Vessel target = service.getVessel("278970");

            service.replaceVessel(source, target, false);
        }
        {
            Cruise cruise = service.getCruise(0);
            assertContainsId(cruise.getVessel(), "267206");

            FishingOperation fishingOperation = service.getFishingOperation(0);
            assertContainsId(fishingOperation.getVessel(), "267206");
            assertContainsIds(fishingOperation.getSecondaryVessel(), "278970");
        }

    }

    protected <R extends TuttiReferentialEntity> void assertContainsIds(List<R> entities, String... expectedIds) {
        Assert.assertEquals("Should contains " + expectedIds.length + " entities", expectedIds.length, entities.size());
        List<String> ids = TuttiEntities.collecIds(entities);

        for (String expectedId : expectedIds) {
            Assert.assertTrue("Should contains id:" + expectedId, ids.contains(expectedId));
        }
    }

    protected <R extends TuttiReferentialEntity> void assertContainsId(R entity, String expectedId) {
        Assert.assertNotNull(entity);
        Assert.assertEquals(expectedId, entity.getId());
    }

    protected void assertContainsCaracteristics(CaracteristicMap entity) {
        Assert.assertNotNull(entity);
        Assert.assertTrue(entity.size() > 0);
    }

    protected void assertContainsNoCaracteristics(CaracteristicMap entity) {
        Assert.assertNotNull(entity);
        Assert.assertTrue(entity.size() == 0);
    }

    protected void assertSpeciesBatch(BatchContainer<SpeciesBatch> rootSpeciesBatch, String expectedId) {
        Assert.assertNotNull(rootSpeciesBatch);
        Assert.assertNotNull(rootSpeciesBatch.getChildren());
        Assert.assertFalse(rootSpeciesBatch.isEmptyChildren());
        for (SpeciesBatch batch : rootSpeciesBatch.getChildren()) {
            assertContainsId(batch.getSpecies(), expectedId);

            List<SpeciesBatchFrequency> frequencies = service.getAllSpeciesBatchFrequency(batch.getIdAsInt());
            for (SpeciesBatchFrequency frequency : frequencies) {
                assertContainsId(frequency.getBatch().getSpecies(), expectedId);
            }
        }
    }

    protected void assertBenthosBatch(BatchContainer<SpeciesBatch> rootSpeciesBatch, String expectedId) {
        Assert.assertNotNull(rootSpeciesBatch);
        Assert.assertNotNull(rootSpeciesBatch.getChildren());
        Assert.assertFalse(rootSpeciesBatch.isEmptyChildren());
        for (SpeciesBatch batch : rootSpeciesBatch.getChildren()) {
            assertContainsId(batch.getSpecies(), expectedId);
            List<SpeciesBatchFrequency> frequencies = service.getAllBenthosBatchFrequency(batch.getIdAsInt());
            for (SpeciesBatchFrequency frequency : frequencies) {
                assertContainsId(frequency.getBatch().getSpecies(), expectedId);
            }
        }
    }

    protected void assertIndividualObservationBatch(List<IndividualObservationBatch> batches, String expectedId) {
        Assert.assertNotNull(batches);
        Assert.assertFalse(batches.isEmpty());
        for (IndividualObservationBatch batch : batches) {
            assertContainsId(batch.getSpecies(), expectedId);
        }
    }

    protected void assertAccidentalBatch(List<AccidentalBatch> batches, String expectedId) {
        Assert.assertNotNull(batches);
        Assert.assertFalse(batches.isEmpty());
        for (AccidentalBatch batch : batches) {
            assertContainsId(batch.getSpecies(), expectedId);
        }
    }
}
