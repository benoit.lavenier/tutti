package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.ServiceDbResource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 2/26/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportServiceValidTest extends GenericFormatImportServiceTestSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatImportServiceTest.class);

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbEmpty");

    private final static Set<String> builder = new HashSet<>();

    @Override
    protected ServiceDbResource getServiceDbResource() {
        return dbResource;
    }

    @AfterClass
    public static void afterClass() throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Report files:\n" + Joiner.on("\n").join(builder));
        }
        dbResource.setDestroyResources(true);

    }

    @Test
    public void testValidate() throws IOException {

        doValidate("testImport", PROGRAM_ID, "referentials", "sampleCategory", "protocol", "default");

    }

    @Test
    public void testImportWithObsoletes() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testImportWithObsoletes", PROGRAM_ID, "empty", "withObsoletes");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());

        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());

        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());
        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.isValid());
    }

    @Test
    public void testArchiveLayoutNotValid() throws IOException {

        GenericFormatValidateFileResult result = doValidate("ArchiveLayoutNotValid", PROGRAM_ID);
        Assert.assertFalse(result.isArchiveLayoutValid());
        Assert.assertFalse(result.isValid());
        Assert.assertFalse(result.getSampleCategoryFileResult().isImported());
        Assert.assertFalse(result.getSampleCategoryFileResult().isValid());
        Assert.assertFalse(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryVesselFileResult().isImported());

    }

    @Test
    public void testBadSampleCategoryFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("badSampleCategoryFormat", PROGRAM_ID,
                                                            "empty", "badSampleCategoryFormat");
        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertFalse(result.isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertFalse(result.getSampleCategoryFileResult().isValid());
        Assert.assertFalse(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryVesselFileResult().isImported());

    }

    @Test
    public void badTemporaryReferentialFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("badTemporaryReferentialFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory",
                                                            "badTemporaryReferentialGearFormat",
                                                            "badTemporaryReferentialPersonFormat",
                                                            "badTemporaryReferentialSpeciesFormat",
                                                            "badTemporaryReferentialVesselFormat");
        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertFalse(result.isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryVesselFileResult().isValid());

        Assert.assertFalse(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

    }

    @Test
    public void testBadTemporaryReferentialImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadTemporaryReferentialImport", PROGRAM_ID,
                                                            "empty", "sampleCategory",
                                                            "badTemporaryReferentialGearImport",
                                                            "badTemporaryReferentialPersonImport",
                                                            "badTemporaryReferentialSpeciesImport",
                                                            "badTemporaryReferentialVesselImport");
        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertFalse(result.isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());

        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertFalse(result.getReferentialTemporaryVesselFileResult().isValid());

        Assert.assertFalse(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

    }

    @Test
    public void testTemporaryReferentialImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadTemporaryReferentialImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());

        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());

        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

    }

    @Test
    public void testBadProtocolFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadProtocolFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "badProtocolFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());

        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertFalse(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

    }

    @Test
    public void testBadProtocolImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadProtocolImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "badProtocolImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());

        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertFalse(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

    }


    @Test
    public void testBadSurveyFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadSurveyFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol", "badSurveyFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertFalse(result.getSurveyFileResult().isValid());

    }

    @Test
    public void testBadSurveyImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadSurveyImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "badSurveyImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertFalse(result.getSurveyFileResult().isValid());

    }

    @Test
    public void testSurveyImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testSurveyImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());

    }

    @Test
    public void testBadGearCaracteristicFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadGearCaracteristicFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "badGearCaracteristicFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());

        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertFalse(result.getGearCaracteristicFileResult().isValid());

    }

    @Test
    public void testBadGearCaracteristicImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadGearCaracteristicImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "badGearCaracteristicImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());

        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertFalse(result.getGearCaracteristicFileResult().isValid());

    }

    @Test
    public void testGearCaracteristicImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testGearCaracteristicImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());

        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());

    }

    @Test
    public void testBadOperationFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadOperationFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "badOperationFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());

        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertFalse(result.getOperationFileResult().isValid());

    }

    @Test
    public void testBadOperationImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadOperationImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "badOperationImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());

        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertFalse(result.getOperationFileResult().isValid());

    }

    @Test
    public void testOperationImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testOperationImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());

        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());

    }

    @Test
    public void testOperationAndParameterImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testOperationAndParameterImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "parameters");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getParameterFileResult().isImported());
        Assert.assertTrue(result.getParameterFileResult().isValid());

    }

    @Test
    public void testBadCatchFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadCatchFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "badCatchFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());

        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertFalse(result.getCatchFileResult().isValid());

    }

    @Test
    public void testBadCatchImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadCatchImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "badCatchImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());

        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertFalse(result.getCatchFileResult().isValid());

    }

    @Test
    public void testCatchImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testCatchImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());

        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

    }

    @Test
    public void testBadMarineLitterFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadMarineLitterFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "badMarineLitterFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getMarineLitterFileResult().isImported());
        Assert.assertFalse(result.getMarineLitterFileResult().isValid());

    }

    @Test
    public void testBadMarineLitterImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadMarineLitterImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "badMarineLitterImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getMarineLitterFileResult().isImported());
        Assert.assertFalse(result.getMarineLitterFileResult().isValid());

    }

    @Test
    public void testMarineLitterImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testMarineLitterImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "marineLitter");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getMarineLitterFileResult().isImported());
        Assert.assertTrue(result.getMarineLitterFileResult().isValid());

    }

    @Test
    public void testBadAccidentalCatchFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadAccidentalCatchFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "badAccidentalCatchFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getAccidentalCatchFileResult().isImported());
        Assert.assertFalse(result.getAccidentalCatchFileResult().isValid());

    }

    @Test
    public void testBadAccidentalCatchImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadAccidentalCatchImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "badAccidentalCatchImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getAccidentalCatchFileResult().isImported());
        Assert.assertFalse(result.getAccidentalCatchFileResult().isValid());

    }

    @Test
    public void testAccidentalCatchImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testAccidentalCatchImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "accidentalCatch");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getAccidentalCatchFileResult().isImported());
        Assert.assertTrue(result.getAccidentalCatchFileResult().isValid());

    }

    @Test
    public void testBadIndividualObservationFormat() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testBadIndividualObservationFormat", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "badIndividualObservationFormat");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getIndividualObservationFileResult().isImported());
        Assert.assertFalse(result.getIndividualObservationFileResult().isValid());

    }

    @Test
    public void testBadIndividualObservationImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testSurveyImportLimitCases", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "badIndividualObservationImport");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertFalse(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getIndividualObservationFileResult().isImported());
        Assert.assertFalse(result.getIndividualObservationFileResult().isValid());

    }

    @Test
    public void testIndividualObservationImport() throws IOException {

        GenericFormatValidateFileResult result = doValidate("testIndividualObservationImport", PROGRAM_ID,
                                                            "empty", "sampleCategory", "referentials", "protocol",
                                                            "survey", "gearCaracteristic", "operation", "catch", "individualObservation");

        Assert.assertTrue(result.isArchiveLayoutValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getSampleCategoryFileResult().isImported());
        Assert.assertTrue(result.getSampleCategoryFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryGearFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryPersonFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporarySpeciesFileResult().isValid());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isImported());
        Assert.assertTrue(result.getReferentialTemporaryVesselFileResult().isValid());
        Assert.assertTrue(result.getProtocolFileResult().isImported());
        Assert.assertTrue(result.getProtocolFileResult().isValid());

        Assert.assertTrue(result.isValid());

        Assert.assertTrue(result.getSurveyFileResult().isImported());
        Assert.assertTrue(result.getSurveyFileResult().isValid());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isImported());
        Assert.assertTrue(result.getGearCaracteristicFileResult().isValid());
        Assert.assertTrue(result.getOperationFileResult().isImported());
        Assert.assertTrue(result.getOperationFileResult().isValid());
        Assert.assertTrue(result.getCatchFileResult().isImported());
        Assert.assertTrue(result.getCatchFileResult().isValid());

        Assert.assertTrue(result.getIndividualObservationFileResult().isImported());
        Assert.assertTrue(result.getIndividualObservationFileResult().isValid());

    }

    protected GenericFormatValidateFileResult doValidate(String archivName, String programId, String... directoryies) throws IOException {

        File archiveFile = createArchive(archivName + ".zip", directoryies);

        GenericFormatImportConfiguration importConfiguration = new GenericFormatImportConfiguration();
        importConfiguration.setImportFile(archiveFile);

        ProgramDataModel dataModel = persistenceService.loadProgram(programId, true);
        importConfiguration.setDataToExport(dataModel);
        importConfiguration.setImportSpecies(true);
        importConfiguration.setImportBenthos(true);
        importConfiguration.setImportMarineLitter(true);
        importConfiguration.setImportAccidentalCatch(true);
        importConfiguration.setImportIndividualObservation(true);
        importConfiguration.setImportAttachments(true);
        importConfiguration.setUpdateCruises(true);
        importConfiguration.setUpdateOperations(true);
        importConfiguration.setAuthorizeObsoleteReferentials(true);

        File reportFile = getServiceDbResource().getConfig().newTempFile(archivName, ".pdf");
        importConfiguration.setReportFile(reportFile);

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.setTotal(10000);

        GenericFormatValidateFileResult result = service.validateImportFile(importConfiguration, progressionModel);

        builder.add("evince " + reportFile);

        return result;

    }
}
