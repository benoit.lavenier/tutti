package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.protocol.Rtps;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristics;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.ServiceDbResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.util.Map;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ProtocolImportExportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.writeDb("dbCGFS");

    protected ProtocolImportExportService service;

    public static final int SIZE = 198;

    public static final int SEX = 196;

    public static final int MATURITY = 174;

    public static final int AGE = 1430;

    public static final String PROTOCOL_FILE_CONTENT =
            "id: 1\n" +
                    "name: protocolName\n" +
                    "benthos: \n" +
                    "- !SpeciesProtocol\n" +
                    "   id: 21\n" +
                    "   lengthStepPmfmId: 1394\n" +
                    "   mandatorySampleCategoryId: \n" +
                    "   - 174\n" +
                    "   - 196\n" +
                    "   speciesReferenceTaxonId: 11242\n" +
                    "   speciesSurveyCode: cruiseCode1\n" +
                    "   weightEnabled: true\n" +
                    "- !SpeciesProtocol\n" +
                    "   id: 22\n" +
                    "   lengthStepPmfmId: 323\n" +
                    "   mandatorySampleCategoryId: \n" +
                    "   - 1430\n" +
                    "   - 174\n" +
                    "   - 196\n" +
                    "   - 198\n" +
                    "   rtpFemale: !fr.ifremer.tutti.persistence.entities.protocol.RtpBean\n" +
                    "      a: 22.0\n" +
                    "      b: 33.0\n" +
                    "   speciesReferenceTaxonId: 3835\n" +
                    "   weightEnabled: true\n" +
                    "caracteristicMapping: \n" +
                    "- !CaracteristicMappingRow\n" +
                    "   pmfmId: 114\n" +
                    "   tab: VESSEL_USE_FEATURE\n" +
                    "- !CaracteristicMappingRow\n" +
                    "   pmfmId: 228\n" +
                    "   tab: VESSEL_USE_FEATURE\n" +
                    "- !CaracteristicMappingRow\n" +
                    "   pmfmId: 821\n" +
                    "   tab: VESSEL_USE_FEATURE\n" +
                    "- !CaracteristicMappingRow\n" +
                    "   pmfmId: 21\n" +
                    "   tab: GEAR_USE_FEATURE\n" +
                    "- !CaracteristicMappingRow\n" +
                    "   pmfmId: 262\n" +
                    "   tab: GEAR_USE_FEATURE\n" +
                    "comment: Commentaire\n" +
                    "individualObservationPmfmId: \n" +
                    "- 25\n" +
                    "- 26\n" +
                    "lengthClassesPmfmId: \n" +
                    "- 14\n" +
                    "- 18\n" +
                    "species: \n" +
                    "- !SpeciesProtocol\n" +
                    "   id: 1\n" +
                    "   lengthStepPmfmId: 1394\n" +
                    "   mandatorySampleCategoryId: \n" +
                    "   - 174\n" +
                    "   - 196\n" +
                    "   rtpFemale: !fr.ifremer.tutti.persistence.entities.protocol.RtpBean\n" +
                    "      a: 2.0\n" +
                    "      b: 3.0\n" +
                    "   rtpMale: !fr.ifremer.tutti.persistence.entities.protocol.RtpBean\n" +
                    "      a: 1.0\n" +
                    "      b: 2.0\n" +
                    "   rtpUndefined: !fr.ifremer.tutti.persistence.entities.protocol.RtpBean\n" +
                    "      a: 3.0\n" +
                    "      b: 4.0\n" +
                    "   speciesReferenceTaxonId: 11242\n" +
                    "   speciesSurveyCode: cruiseCode1\n" +
                    "   weightEnabled: true\n" +
                    "- !SpeciesProtocol\n" +
                    "   id: 2\n" +
                    "   lengthStepPmfmId: 323\n" +
                    "   mandatorySampleCategoryId: \n" +
                    "   - 1430\n" +
                    "   - 174\n" +
                    "   - 196\n" +
                    "   - 198\n" +
                    "   speciesReferenceTaxonId: 3835\n" +
                    "   weightEnabled: true\n" +
                    "version: " + TuttiProtocols.CURRENT_PROTOCOL_VERSION;

    public static final String PROTOCOL_CARACTERISTIC_FILE_CONTENT =
            "pmfmId;pmfmType;pmfmParameterName;pmfmMatrixName;pmfmFractionName;pmfmMethodName\n" +
            "1;LENGTH_STEP;parameterName1;matrixName1;fractionName1;methodName1\n" +
            "2;VESSEL_USE_FEATURE;parameterName2;matrixName2;fractionName2;methodName2\n" +
            "3;GEAR_USE_FEATURE;parameterName3;matrixName3;fractionName3;methodName3\n"+
            "4;INDIVIDUAL_OBSERVATION;parameterName4;matrixName4;fractionName4;methodName4";

    public static final String PROTOCOL_CARACTERISTIC_FILE_BAD_CONTENT =
            "pmfmId;pmfmType;pmfmParameterName;pmfmMatrixName;pmfmFractionName;pmfmMethodName\n" +
            "1;LENGTH_STEP;parameterName1;matrixName1;fractionName1;methodName1\n" +
            "2;;parameterName2;matrixName2;fractionName2;methodName2\n" +
            "3;GEAR_USE_FEATURE;parameterName3;matrixName3;fractionName3;methodName3";

    public static final String ALL_CARACTERISTIC_FILE_CONTENT =
            "pmfmId;pmfmType;pmfmParameterName;pmfmMatrixName;pmfmFractionName;pmfmMethodName\n" +
            "1;;parameterName1;matrixName1;fractionName1;methodName1\n" +
            "2;;parameterName2;matrixName2;fractionName2;methodName2\n" +
            "3;;parameterName3;matrixName3;fractionName3;methodName3\n" +
            "4;;parameterName4;matrixName4;fractionName4;methodName4\n" +
            "5;;parameterName5;matrixName5;fractionName5;methodName5";

    public static final String PROTOCOL_SPECIES_FILE_CONTENT =
            "speciesReferenceTaxonId;speciesRefTaxCode;speciesName;speciesSurveyCode;lengthStepPmfmId;lengthStepPmfmParameterName;lengthStepPmfmMatrixName;lengthStepPmfmFractionName;lengthStepPmfmMethodName;mandatorySampleCategoryId;weightEnabled;rtpMaleA;rtpMaleB;rtpFemaleA;rtpFemaleB;rtpUndefinedA;rtpUndefinedB\n" +
            "1;speciesRefTaxCode1;speciesName1;cruiseCode1;2;parameterName2;matrixName2;fractionName2;methodName2;1430|198|174|196;Y;1.0;2.0;2.0;3.0;3.0;4.0\n" +
            "2;speciesRefTaxCode2;speciesName2;;;;;;;1430|196;Y;;;;;;";

    public static final String PROTOCOL_BENTHOS_FILE_CONTENT =
            "speciesReferenceTaxonId;speciesRefTaxCode;speciesName;speciesSurveyCode;lengthStepPmfmId;lengthStepPmfmParameterName;lengthStepPmfmMatrixName;lengthStepPmfmFractionName;lengthStepPmfmMethodName;mandatorySampleCategoryId;weightEnabled;rtpMaleA;rtpMaleB;rtpFemaleA;rtpFemaleB;rtpUndefinedA;rtpUndefinedB\n" +
            "1;speciesRefTaxCode1;speciesName1;cruiseCode1;2;parameterName2;matrixName2;fractionName2;methodName2;1430|198|174|196;Y;;;;;;\n" +
            "2;speciesRefTaxCode2;speciesName2;;;;;;;1430|196;Y;;;22.0;33.0;;";

    File datadirectory;

    @Before
    public void setUp() throws Exception {

        dbResource.getConfig().setCsvSeparator(';');

        service = dbResource.getServiceContext().getService(
                ProtocolImportExportService.class);

        datadirectory = dbResource.getConfig().getDataDirectory();
    }

    @Test
    public void exportProtocol() throws Exception {

        File exportFile = new File(datadirectory, "exportProtocol.yaml");

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());
        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        protocol.setVersion(TuttiProtocols.CURRENT_PROTOCOL_VERSION);
        protocol.setId("1");
        protocol.setName("protocolName");
        protocol.setComment("Commentaire");
        protocol.setLengthClassesPmfmId(Lists.newArrayList("14", "18"));
        protocol.setCaracteristicMapping(Lists.newArrayList(
                TuttiProtocols.caracteristicMappingRow("114", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("228", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("821", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("21", CaracteristicType.GEAR_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("262", CaracteristicType.GEAR_USE_FEATURE)
        ));
        protocol.setIndividualObservationPmfmId(Lists.newArrayList("25", "26"));

        protocol.setSpecies(Lists.<SpeciesProtocol>newArrayList());
        SpeciesProtocol sp1 = SpeciesProtocols.newSpeciesProtocol();
        sp1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp1.setId(1);
        sp1.setSpeciesReferenceTaxonId(11242);
        sp1.setSpeciesSurveyCode("cruiseCode1");
        sp1.setLengthStepPmfmId("1394");
        sp1.addMandatorySampleCategoryId(MATURITY);
        sp1.addMandatorySampleCategoryId(SEX);
        sp1.setWeightEnabled(true);
        sp1.setRtpMale(Rtps.newRtp(1f, 2f));
        sp1.setRtpFemale(Rtps.newRtp(2f, 3f));
        sp1.setRtpUndefined(Rtps.newRtp(3f, 4f));
        protocol.addSpecies(sp1);

        SpeciesProtocol sp2 = SpeciesProtocols.newSpeciesProtocol();
        sp2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp2.setId(2);
        sp2.setSpeciesReferenceTaxonId(3835);
        sp2.setLengthStepPmfmId("323");
        sp2.addMandatorySampleCategoryId(AGE);
        sp2.addMandatorySampleCategoryId(MATURITY);
        sp2.addMandatorySampleCategoryId(SEX);
        sp2.addMandatorySampleCategoryId(SIZE);
        sp2.setWeightEnabled(true);
        protocol.addSpecies(sp2);

        protocol.setBenthos(Lists.<SpeciesProtocol>newArrayList());
        SpeciesProtocol bp1 = SpeciesProtocols.newSpeciesProtocol();
        bp1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        bp1.setId(21);
        bp1.setSpeciesSurveyCode("cruiseCode1");
        bp1.setSpeciesReferenceTaxonId(11242);
        bp1.setLengthStepPmfmId("1394");
        bp1.addMandatorySampleCategoryId(MATURITY);
        bp1.addMandatorySampleCategoryId(SEX);
        bp1.setWeightEnabled(true);
        protocol.addBenthos(bp1);

        SpeciesProtocol bp2 = SpeciesProtocols.newSpeciesProtocol();
        bp2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        bp2.setId(22);
        bp2.setSpeciesReferenceTaxonId(3835);
        bp2.setLengthStepPmfmId("323");
        bp2.addMandatorySampleCategoryId(AGE);
        bp2.addMandatorySampleCategoryId(MATURITY);
        bp2.addMandatorySampleCategoryId(SEX);
        bp2.addMandatorySampleCategoryId(SIZE);
        bp2.setWeightEnabled(true);
        bp2.setRtpFemale(Rtps.newRtp(22f, 33f));
        protocol.addBenthos(bp2);

        service.exportProtocol(protocol, exportFile);
        Assert.assertTrue(exportFile.exists());

        String exportFileToString = Files.toString(exportFile, Charsets.UTF_8).trim();
        Assert.assertEquals(PROTOCOL_FILE_CONTENT, exportFileToString);
    }

    @Test
    public void importProtocol() throws Exception {

        File importfile = new File(datadirectory, "importProtocol.yaml");

        Files.createParentDirs(importfile);

        Files.write(PROTOCOL_FILE_CONTENT, importfile, Charsets.UTF_8);

        TuttiProtocol protocol = service.importProtocol(importfile);

        Assert.assertNotNull(protocol);
        Assert.assertEquals("1", protocol.getId());
        Assert.assertEquals(TuttiProtocols.CURRENT_PROTOCOL_VERSION, protocol.getVersion(), 0);
        Assert.assertEquals("protocolName", protocol.getName());
        Assert.assertEquals("Commentaire", protocol.getComment());
        Assert.assertEquals(Lists.newArrayList("14", "18"), protocol.getLengthClassesPmfmId());
        Assert.assertEquals(Lists.newArrayList(
                                    TuttiProtocols.caracteristicMappingRow("114", CaracteristicType.VESSEL_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("228", CaracteristicType.VESSEL_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("821", CaracteristicType.VESSEL_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("21", CaracteristicType.GEAR_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("262", CaracteristicType.GEAR_USE_FEATURE)
                            ),
                            protocol.getCaracteristicMapping());
        Assert.assertEquals(Lists.newArrayList("25", "26"), protocol.getIndividualObservationPmfmId());

        Assert.assertNotNull(protocol.getSpecies());
        Assert.assertEquals(2, protocol.sizeSpecies());
        SpeciesProtocol sp1 = protocol.getSpecies(0);
        Assert.assertNotNull(sp1);
        Assert.assertEquals(1, sp1.getIdAsInt(), 0);
        Assert.assertEquals(11242, sp1.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("cruiseCode1", sp1.getSpeciesSurveyCode());
        Assert.assertEquals("1394", sp1.getLengthStepPmfmId());
        Assert.assertFalse(sp1.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(SEX));
        Assert.assertFalse(sp1.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp1.isWeightEnabled());
        Assert.assertTrue(sp1.withRtpMale());
        Assert.assertTrue(sp1.withRtpFemale());
        Assert.assertTrue(sp1.withRtpUndefined());
        Assert.assertEquals(1f, sp1.getRtpMale().getA(), 0.01);
        Assert.assertEquals(2f, sp1.getRtpMale().getB(), 0.01);
        Assert.assertEquals(2f, sp1.getRtpFemale().getA(), 0.01);
        Assert.assertEquals(3f, sp1.getRtpFemale().getB(), 0.01);
        Assert.assertEquals(3f, sp1.getRtpUndefined().getA(), 0.01);
        Assert.assertEquals(4f, sp1.getRtpUndefined().getB(), 0.01);

        SpeciesProtocol sp2 = protocol.getSpecies(1);
        Assert.assertNotNull(sp2);
        Assert.assertEquals(2, sp2.getIdAsInt(), 0);
        Assert.assertEquals(3835, sp2.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("323", sp2.getLengthStepPmfmId());
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(SEX));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp2.isWeightEnabled());
        Assert.assertFalse(sp2.withRtpMale());
        Assert.assertFalse(sp2.withRtpFemale());
        Assert.assertFalse(sp2.withRtpUndefined());

        Assert.assertNotNull(protocol.getBenthos());
        Assert.assertEquals(2, protocol.sizeBenthos());
        SpeciesProtocol b1 = protocol.getBenthos(0);
        Assert.assertNotNull(b1);
        Assert.assertEquals(21, b1.getIdAsInt(), 0);
        Assert.assertEquals(11242, b1.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("cruiseCode1", b1.getSpeciesSurveyCode());
        Assert.assertEquals("1394", b1.getLengthStepPmfmId());
        Assert.assertFalse(b1.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(b1.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(b1.containsMandatorySampleCategoryId(SEX));
        Assert.assertFalse(b1.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(b1.isWeightEnabled());
        Assert.assertFalse(b1.withRtpMale());
        Assert.assertFalse(b1.withRtpFemale());
        Assert.assertFalse(b1.withRtpUndefined());

        SpeciesProtocol b2 = protocol.getBenthos(1);
        Assert.assertNotNull(b2);
        Assert.assertEquals(22, b2.getIdAsInt(), 0);
        Assert.assertEquals(3835, b2.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("323", b2.getLengthStepPmfmId());
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(SEX));
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(b2.isWeightEnabled());
        Assert.assertFalse(b2.withRtpMale());
        Assert.assertFalse(b2.withRtpUndefined());
        Assert.assertTrue(b2.withRtpFemale());
        Assert.assertEquals(22f, b2.getRtpFemale().getA(), 0.01);
        Assert.assertEquals(33f, b2.getRtpFemale().getB(), 0.01);
    }

//    //FIXME
//    @Test
//    public void importProtocolCaracteristic() throws Exception {
//
//        File file = new File(datadirectory, "importProtocolCaracteristic.csv");
//
//        Files.createParentDirs(file);
//
//        Files.write(PROTOCOL_CARACTERISTIC_FILE_CONTENT, file, Charsets.UTF_8);
//
//        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
//        Map<String, Caracteristic> caracteristicMap = createCaracteristics();
//
//        service.importProtocolCaracteristic(file, protocol, caracteristicMap);
//
//        Assert.assertEquals(Lists.newArrayList("1"),
//                            protocol.getLengthClassesPmfmId());
//        Assert.assertEquals(Lists.newArrayList(
//                                TuttiProtocols.caracteristicMappingRow("2", CaracteristicType.VESSEL_USE_FEATURE),
//                                TuttiProtocols.caracteristicMappingRow("3", CaracteristicType.GEAR_USE_FEATURE)
//                            ),
//                            protocol.getCaracteristicMapping());
//        Assert.assertEquals(Lists.newArrayList("4"),
//                            protocol.getIndividualObservationPmfmId());
//    }
//
//    @Test(expected = ImportRuntimeException.class)
//    public void importProtocolCaracteristicMissingCaracteristicType() throws Exception {
//
//        File file = new File(datadirectory, "importProtocolCaracteristicMissingCaracteristicType.csv");
//
//        Files.createParentDirs(file);
//
//        Files.write(PROTOCOL_CARACTERISTIC_FILE_BAD_CONTENT, file, Charsets.UTF_8);
//
//        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
//        Map<String, Caracteristic> caracteristicMap = createCaracteristics();
//
//        service.importProtocolCaracteristic(file, protocol, caracteristicMap);
//    }
//    @Test
//    public void exportProtocolCaracteristic() throws Exception {
//
//        File file = new File(datadirectory, "exportProtocolCaracteristic.csv");
//
//        Files.createParentDirs(file);
//
//        Map<String, Caracteristic> caracteristicMap = createCaracteristics();
//        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
//
//        protocol.setLengthClassesPmfmId(Lists.newArrayList("1"));
//        protocol.setCaracteristicMapping(Lists.newArrayList(
//                TuttiProtocols.caracteristicMappingRow("2", CaracteristicType.VESSEL_USE_FEATURE),
//                TuttiProtocols.caracteristicMappingRow("3", CaracteristicType.GEAR_USE_FEATURE)
//        ));
//        protocol.setIndividualObservationPmfmId(Lists.newArrayList("4"));
//
//        Assert.assertFalse(file.exists());
//        service.exportProtocolCaracteristic(file, protocol, caracteristicMap);
//
//        Assert.assertTrue(file.exists());
//
//        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
//        Assert.assertEquals(PROTOCOL_CARACTERISTIC_FILE_CONTENT, exportFileToString);
//    }
//
//    @Test
//    public void exportAllCaracteristic() throws Exception {
//
//        File file = new File(datadirectory, "exportAllCaracteristic.csv");
//
//        Files.createParentDirs(file);
//
//        Map<String, Caracteristic> caracteristicMap = createCaracteristics();
//
//        Assert.assertFalse(file.exists());
//        service.exportAllCaracteristic(file, caracteristicMap);
//
//        Assert.assertTrue(file.exists());
//
//        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
//        Assert.assertEquals(ALL_CARACTERISTIC_FILE_CONTENT, exportFileToString);
//    }

    @Test
    public void importProtocolSpecies() throws Exception {

        File file = new File(datadirectory, "importProtocolSpecies.csv");

        Files.createParentDirs(file);

        Files.write(PROTOCOL_SPECIES_FILE_CONTENT, file, Charsets.UTF_8);

        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        Map<String, Species> speciesMap = createSpecies();
        Map<String, Caracteristic> caracteristicMap = createCaracteristics();

        service.importProtocolSpecies(file, protocol, caracteristicMap, speciesMap);

        Assert.assertEquals(2, protocol.sizeSpecies());

        SpeciesProtocol sp1 = protocol.getSpecies().get(0);
        Assert.assertNotNull(sp1);
        Assert.assertEquals(1, sp1.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("cruiseCode1", sp1.getSpeciesSurveyCode());
        Assert.assertEquals("2", sp1.getLengthStepPmfmId());
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(SEX));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp1.isWeightEnabled());
        Assert.assertTrue(sp1.isWeightEnabled());
        Assert.assertTrue(sp1.withRtpMale());
        Assert.assertTrue(sp1.withRtpFemale());
        Assert.assertTrue(sp1.withRtpUndefined());
        Assert.assertEquals(1f, sp1.getRtpMale().getA(), 0.01);
        Assert.assertEquals(2f, sp1.getRtpMale().getB(), 0.01);
        Assert.assertEquals(2f, sp1.getRtpFemale().getA(), 0.01);
        Assert.assertEquals(3f, sp1.getRtpFemale().getB(), 0.01);
        Assert.assertEquals(3f, sp1.getRtpUndefined().getA(), 0.01);
        Assert.assertEquals(4f, sp1.getRtpUndefined().getB(), 0.01);

        SpeciesProtocol sp2 = protocol.getSpecies().get(1);
        Assert.assertNotNull(sp2);
        Assert.assertEquals(2, sp2.getSpeciesReferenceTaxonId(), 0);
        Assert.assertNull(sp2.getSpeciesSurveyCode());
        Assert.assertNull(sp2.getLengthStepPmfmId());
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(AGE));
        Assert.assertFalse(sp2.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(SEX));
        Assert.assertFalse(sp2.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp2.isWeightEnabled());
        Assert.assertFalse(sp2.withRtpMale());
        Assert.assertFalse(sp2.withRtpFemale());
        Assert.assertFalse(sp2.withRtpUndefined());
    }

    @Test
    public void exportProtocolSpecies() throws Exception {

        File file = new File(datadirectory, "exportProtocolSpecies.csv");

        Files.createParentDirs(file);

        Map<String, Species> speciesMap = createSpecies();

        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        protocol.setSpecies(Lists.<SpeciesProtocol>newArrayList());

        SpeciesProtocol sp1 = SpeciesProtocols.newSpeciesProtocol();
        sp1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp1.setSpeciesReferenceTaxonId(1);
        sp1.setSpeciesSurveyCode("cruiseCode1");
        sp1.addMandatorySampleCategoryId(AGE);
        sp1.addMandatorySampleCategoryId(SIZE);
        sp1.addMandatorySampleCategoryId(MATURITY);
        sp1.addMandatorySampleCategoryId(SEX);
        sp1.setWeightEnabled(true);
        sp1.setLengthStepPmfmId("2");
        sp1.setRtpMale(Rtps.newRtp(1f, 2f));
        sp1.setRtpFemale(Rtps.newRtp(2f, 3f));
        sp1.setRtpUndefined(Rtps.newRtp(3f, 4f));

        protocol.addSpecies(sp1);

        SpeciesProtocol sp2 = SpeciesProtocols.newSpeciesProtocol();
        sp2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp2.setSpeciesReferenceTaxonId(2);
        sp2.addMandatorySampleCategoryId(AGE);
        sp2.removeMandatorySampleCategoryId(SIZE);
        sp2.removeMandatorySampleCategoryId(MATURITY);
        sp2.addMandatorySampleCategoryId(SEX);
        sp2.setWeightEnabled(true);
        sp2.setLengthStepPmfmId(null);

        protocol.addSpecies(sp2);

        Assert.assertFalse(file.exists());
        service.exportProtocolSpecies(file, protocol.getSpecies(), createCaracteristics(), speciesMap);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(PROTOCOL_SPECIES_FILE_CONTENT, exportFileToString);
    }

    @Test
    public void importProtocolBenthos() throws Exception {

        File file = new File(datadirectory, "importProtocolBenthos.csv");

        Files.createParentDirs(file);

        Files.write(PROTOCOL_BENTHOS_FILE_CONTENT, file, Charsets.UTF_8);

        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        Map<String, Species> speciesMap = createSpecies();
        Map<String, Caracteristic> caracteristicMap = createCaracteristics();

        service.importProtocolBenthos(file, protocol, caracteristicMap, speciesMap);

        Assert.assertEquals(2, protocol.sizeBenthos());

        SpeciesProtocol sp1 = protocol.getBenthos(0);
        Assert.assertNotNull(sp1);
        Assert.assertEquals(1, sp1.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("cruiseCode1", sp1.getSpeciesSurveyCode());
        Assert.assertEquals("2", sp1.getLengthStepPmfmId());
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(SEX));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp1.isWeightEnabled());
        Assert.assertFalse(sp1.withRtpMale());
        Assert.assertFalse(sp1.withRtpFemale());
        Assert.assertFalse(sp1.withRtpUndefined());

        SpeciesProtocol sp2 = protocol.getBenthos(1);
        Assert.assertNotNull(sp2);
        Assert.assertEquals(2, sp2.getSpeciesReferenceTaxonId(), 0);
        Assert.assertNull(sp2.getSpeciesSurveyCode());
        Assert.assertNull(sp2.getLengthStepPmfmId());
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(AGE));
        Assert.assertFalse(sp2.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(SEX));
        Assert.assertFalse(sp2.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp2.isWeightEnabled());
        Assert.assertFalse(sp2.withRtpMale());
        Assert.assertFalse(sp2.withRtpUndefined());
        Assert.assertTrue(sp2.withRtpFemale());
        Assert.assertEquals(22f, sp2.getRtpFemale().getA(), 0.01);
        Assert.assertEquals(33f, sp2.getRtpFemale().getB(), 0.01);

    }

    @Test
    public void exportProtocolBenthos() throws Exception {

        File file = new File(datadirectory, "exportProtocolBenthos.csv");

        Files.createParentDirs(file);

        Map<String, Species> speciesMap = createSpecies();

        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        protocol.setBenthos(Lists.<SpeciesProtocol>newArrayList());

        SpeciesProtocol sp1 = SpeciesProtocols.newSpeciesProtocol();
        sp1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp1.setId(21);
        sp1.setSpeciesReferenceTaxonId(1);
        sp1.setSpeciesSurveyCode("cruiseCode1");
        sp1.addMandatorySampleCategoryId(AGE);
        sp1.addMandatorySampleCategoryId(SIZE);
        sp1.addMandatorySampleCategoryId(MATURITY);
        sp1.addMandatorySampleCategoryId(SEX);
        sp1.setWeightEnabled(true);
        sp1.setLengthStepPmfmId("2");

        protocol.addBenthos(sp1);

        SpeciesProtocol sp2 = SpeciesProtocols.newSpeciesProtocol();
        sp2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp2.setId(22);
        sp2.setSpeciesReferenceTaxonId(2);
        sp2.addMandatorySampleCategoryId(AGE);
        sp2.removeMandatorySampleCategoryId(SIZE);
        sp2.removeMandatorySampleCategoryId(MATURITY);
        sp2.addMandatorySampleCategoryId(SEX);
        sp2.setWeightEnabled(true);
        sp2.setLengthStepPmfmId(null);
        sp2.setRtpFemale(Rtps.newRtp(22f, 33f));

        protocol.addBenthos(sp2);

        Assert.assertFalse(file.exists());
        service.exportProtocolBenthos(file, protocol.getBenthos(), createCaracteristics(), speciesMap);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(PROTOCOL_BENTHOS_FILE_CONTENT, exportFileToString);
    }

    protected Map<String, Caracteristic> createCaracteristics() {
        Map<String, Caracteristic> result = Maps.newTreeMap();
        for (int i = 1; i < 6; i++) {
            Caracteristic c = Caracteristics.newCaracteristic();
            c.setId("" + i);
            c.setParameterName("parameterName" + i);
            c.setMatrixName("matrixName" + i);
            c.setFractionName("fractionName" + i);
            c.setMethodName("methodName" + i);
            result.put(i + "", c);
        }
        return result;
    }

    protected Map<String, Species> createSpecies() {
        Map<String, Species> result = Maps.newTreeMap();
        for (int i = 1; i < 3; i++) {
            Species c = Speciess.newSpecies();
            c.setId("" + i);
            c.setReferenceTaxonId(i);
            c.setName("speciesName" + i);
            c.setRefTaxCode("speciesRefTaxCode" + i);
            result.put(i + "", c);
        }
        return result;
    }
}
