package fr.ifremer.tutti.service.psionimport;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.TuttiConfigurationOption;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created on 1/19/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class PsionImportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.writeDb("dbExport");

    /** Logger. */
    private static final Log log = LogFactory.getLog(PsionImportServiceTest.class);

    public static final String PROGRAM_ID = "CAM-TEST_ELEVATION";

    public static final Integer CRUISE_ID = 100003;

    public static final Integer OPERATION_1_ID = 100112;

    public static final Integer OPERATION_2_ID = 100113;

    public static final Integer OPERATION_3_ID = 100115;

    protected PsionImportService service;

    protected PersistenceService persistenceService;

    protected ServiceDbResource.DataContext dataContext;

    protected ProgressionModel progressionModel;

    protected File dataDirectory;

    protected Predicate<SpeciesBatch> vracPredicate;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        File protocol = dbResource.copyClassPathResource("psion/protocol.tuttiProtocol", "protocol.tuttiProtocol");
        dbResource.getConfig().getApplicationConfig().setOption(TuttiConfigurationOption.DB_PROTOCOL_DIRECTORY.getKey(), protocol.getParentFile().getAbsolutePath());
        serviceContext.getDataContext().setProtocolId("protocol");

        dbResource.openDataContext();

        persistenceService = serviceContext.getService(PersistenceService.class);
        service = serviceContext.getService(PsionImportService.class);

        progressionModel = new ProgressionModel();
        progressionModel.setTotal(9);

        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_ID, 3, OPERATION_2_ID, OPERATION_1_ID, OPERATION_3_ID);

        vracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(PmfmId.SORTED_UNSORTED.getValue(), QualitativeValueId.VRAC.getValue());
    }

    @Test
    public void importCC053() throws IOException {

        File importFile = dbResource.copyClassPathResource("psion/CC053.IWA", "CC053.IWA");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            persistenceService.deleteSpeciesBatch(speciesBatch.getIdAsInt());
        }

        PsionImportResult importResult = service.importFile(importFile, operation, catchBatch);

        int nbSortedAdded = importResult.getNbSortedImported();
        int nbUnsortedAdded = importResult.getNbUnsortedImported();
        List<String> errors = importResult.getErrors();

        if (log.isInfoEnabled()) {
            log.info("Sorted Imported: " + nbSortedAdded);
            log.info("Unsorted Imported: " + nbUnsortedAdded);
            log.info("Errors: " + errors.size());
        }

        int nbNewSortedBatchs = 10;
        int nbNewUnsortedBatchs = 9;
        Assert.assertEquals(nbNewSortedBatchs, nbSortedAdded);
        Assert.assertEquals(nbNewUnsortedBatchs, nbUnsortedAdded);
        Assert.assertEquals(0, errors.size());

        // no batch imported
        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);

        int totalSortedBatchs = 0;
        int totalUnsortedBatchs = 0;
        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            boolean sorted = vracPredicate.apply(speciesBatch);

            if (sorted) {
                totalSortedBatchs++;
            } else {
                totalUnsortedBatchs++;
            }
        }

        Assert.assertEquals(nbNewSortedBatchs, totalSortedBatchs);
        Assert.assertEquals(nbNewUnsortedBatchs, totalUnsortedBatchs);
    }

    @Test
    public void importFM001() throws IOException {

        File importFile = dbResource.copyClassPathResource("psion/FM001.IWA", "FM001.IWA");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            persistenceService.deleteSpeciesBatch(speciesBatch.getIdAsInt());
        }

        PsionImportResult importResult = service.importFile(importFile, operation, catchBatch);

        int nbSortedAdded = importResult.getNbSortedImported();
        int nbUnsortedAdded = importResult.getNbUnsortedImported();
        List<String> errors = importResult.getErrors();

        if (log.isInfoEnabled()) {
            log.info("Sorted Imported: " + nbSortedAdded);
            log.info("Unsorted Imported: " + nbUnsortedAdded);
            log.info("Errors: " + errors.size());
        }

        int nbNewSortedBatchs = 0;
        int nbNewUnsortedBatchs = 10;
        Assert.assertEquals(nbNewSortedBatchs, nbSortedAdded);
        Assert.assertEquals(nbNewUnsortedBatchs, nbUnsortedAdded);
        Assert.assertEquals(0, errors.size());

        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);

        int totalSortedBatchs = 0;
        int totalUnsortedBatchs = 0;
        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            boolean sorted = vracPredicate.apply(speciesBatch);

            if (sorted) {
                totalSortedBatchs++;
            } else {
                totalUnsortedBatchs++;
            }
        }

        Assert.assertEquals(nbNewSortedBatchs, totalSortedBatchs);
        Assert.assertEquals(nbNewUnsortedBatchs, totalUnsortedBatchs);
    }

    @Test
    public void importCFchephren() throws IOException {

        File importFile = dbResource.copyClassPathResource("psion/CFchephren 110612.IWA", "CFchephren 110612.IWA");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            persistenceService.deleteSpeciesBatch(speciesBatch.getIdAsInt());
        }

        PsionImportResult importResult = service.importFile(importFile, operation, catchBatch);

        int nbSortedAdded = importResult.getNbSortedImported();
        int nbUnsortedAdded = importResult.getNbUnsortedImported();
        List<String> errors = importResult.getErrors();

        if (log.isInfoEnabled()) {
            log.info("Sorted Imported: " + nbSortedAdded);
            log.info("Unsorted Imported: " + nbUnsortedAdded);
            log.info("Errors: " + errors.size());
        }

        Assert.assertEquals(0, nbSortedAdded);
        Assert.assertEquals(0, nbUnsortedAdded);
        Assert.assertEquals(1, errors.size());

        // no batch imported
        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(0, rootSpeciesBatchAfter.sizeChildren());
    }

    @Test
    public void importEvo5077() throws IOException {

        File importFile = dbResource.copyClassPathResource("psion/evo-5077.IWA", "evo-5077.IWA");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            persistenceService.deleteSpeciesBatch(speciesBatch.getIdAsInt());
        }

        PsionImportResult importResult = service.importFile(importFile, operation, catchBatch);

        int nbSortedAdded = importResult.getNbSortedImported();
        int nbUnsortedAdded = importResult.getNbUnsortedImported();
        List<String> errors = importResult.getErrors();

        if (log.isInfoEnabled()) {
            log.info("Sorted Imported: " + nbSortedAdded);
            log.info("Unsorted Imported: " + nbUnsortedAdded);
            log.info("Errors: " + errors.size());
        }

        int nbNewSortedBatchs = 3;
        int nbNewUnsortedBatchs = 1;
        Assert.assertEquals(nbNewSortedBatchs, nbSortedAdded);
        Assert.assertEquals(nbNewUnsortedBatchs, nbUnsortedAdded);
        Assert.assertEquals(0, errors.size());

        // no batch imported
        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);

        int totalSortedBatchs = 0;
        int totalUnsortedBatchs = 0;
        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            boolean sorted = vracPredicate.apply(speciesBatch);

            if (sorted) {
                totalSortedBatchs++;
            } else {
                totalUnsortedBatchs++;
            }
        }

        Assert.assertEquals(nbNewSortedBatchs, totalSortedBatchs);
        Assert.assertEquals(nbNewUnsortedBatchs, totalUnsortedBatchs);
    }
}
