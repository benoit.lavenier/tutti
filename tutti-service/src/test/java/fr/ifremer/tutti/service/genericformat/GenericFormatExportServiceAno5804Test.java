package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;

/**
 * Created on 9/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class GenericFormatExportServiceAno5804Test {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.readDb("dbAno5804");


    protected GenericFormatExportService service;

    protected PersistenceService persistenceService;

    protected ServiceDbResource.DataContext dataContext;

    protected ProgressionModel progressionModel;

    protected File dataDirectory;

    public static final String PROGRAM_ID = "CAM-CGFS";

    public static final Integer CRUISE_ID = 0;

    public static final Integer OPERATION_1_ID = 1;

    public static final Integer OPERATION_2_ID = 2;

    public static final Integer OPERATION_3_ID = 0;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        persistenceService = serviceContext.getService(PersistenceService.class);

        dbResource.loadInternalProtocolFile();

        dbResource.setCountryInConfig("12");
        dbResource.openDataContext();

        service = serviceContext.getService(GenericFormatExportService.class);

        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_ID, 3, OPERATION_1_ID, OPERATION_2_ID, OPERATION_3_ID);

        progressionModel = new ProgressionModel();

    }

    @Test
    public void exportCruise() throws Exception {

        File exportFile = new File(dataDirectory, "exportCruise.zip");

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        String programId = dataContext.program.getId();
        Integer cruiseId = dataContext.cruise.getIdAsInt();

        ProgramDataModel dataToExport = persistenceService.loadCruises(programId, true, cruiseId);

        GenericFormatExportConfiguration exportConfiguration = new GenericFormatExportConfiguration();
        exportConfiguration.setExportFile(exportFile);
        exportConfiguration.setExportAttachments(true);
        exportConfiguration.setExportSpecies(true);
        exportConfiguration.setExportBenthos(true);
        exportConfiguration.setExportMarineLitter(true);
        exportConfiguration.setExportAccidentalCatch(true);
        exportConfiguration.setExportIndividualObservation(true);
        exportConfiguration.setDataToExport(dataToExport);

        int nbSteps = service.getExportNbSteps(exportConfiguration);
        progressionModel.setTotal(nbSteps);

        service.export(exportConfiguration, progressionModel);
        Assert.assertTrue(exportFile.exists());
    }

}
