package fr.ifremer.tutti.caliper.feed;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.caliper.feed.event.CaliperFeedReaderEvent;
import fr.ifremer.tutti.caliper.feed.event.CaliperFeedReaderListener;
import fr.ifremer.tutti.caliper.feed.record.CaliperFeedReaderMeasureRecord;
import fr.ifremer.tutti.caliper.feed.record.CaliperFeedReaderRecordFactory;
import fr.ifremer.tutti.caliper.feed.record.CaliperFeedReaderRecordSupport;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.event.EventListenerList;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.TooManyListenersException;

/**
 * To read some records from a caliper in feed mode.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CaliperFeedReader implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CaliperFeedReader.class);

    private static final String SERIAL_PORT_PREFIX = "COM";

    /**
     * Name of the serial port
     */
    protected String serialPortName;

    /**
     * Serial port open in the {@code start} method.
     */
    protected SerialPort serialPort;

    /**
     * To keep list of {@link CaliperFeedReaderListener} listeners.
     */
    protected final EventListenerList listenerList;

    /**
     * To generate some feed record from the rax record send by the board.
     *
     * @since 3.10
     */
    protected final CaliperFeedReaderRecordFactory recordFactory;

    public CaliperFeedReader() {
        listenerList = new EventListenerList();
        recordFactory = new CaliperFeedReaderRecordFactory();
    }

    public void start(int portNumber) throws CaliperConnectionException {

        this.serialPortName = SERIAL_PORT_PREFIX + portNumber;

        try {
            CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(serialPortName);

            serialPort = portIdentifier.open(this.getClass().getName(), 2000);
            serialPort.setSerialPortParams(4800, SerialPort.DATABITS_7, SerialPort.STOPBITS_1, SerialPort.PARITY_EVEN);

            InputStream inputStream = serialPort.getInputStream();

            serialPort.addEventListener(new SerialReader(inputStream));
            serialPort.notifyOnDataAvailable(true);

            if (log.isDebugEnabled()) {
                log.debug("Ready to read remote device...");
            }

        } catch (IOException | TooManyListenersException | PortInUseException | NoSuchPortException | UnsupportedCommOperationException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while connecting to the serial port " + serialPortName, e);
            }
            throw new CaliperConnectionException(e);
        }
    }

    @Override
    public void close() throws IOException {

        try {
            serialPort.close();

        } finally {
            // do nothing if the close fails
        }
    }

    public void addFeedModeReaderListener(CaliperFeedReaderListener listener) {
        listenerList.add(CaliperFeedReaderListener.class, listener);
    }

    public void removeFeedModeReaderListener(CaliperFeedReaderListener listener) {
        listenerList.remove(CaliperFeedReaderListener.class, listener);
    }

    public void removeAllFeedModeReaderListeners() {
        for (CaliperFeedReaderListener listener : listenerList.getListeners(CaliperFeedReaderListener.class)) {
            listenerList.remove(CaliperFeedReaderListener.class, listener);
        }
    }

    public String getSerialPortName() {
        return serialPortName;
    }

    /**
     * Handles the input coming from the serial port. A new line character
     * is treated as the end of a block in this example.
     */
    public class SerialReader implements SerialPortEventListener {

        private InputStream in;
        private byte[] buffer = new byte[1024];

        public SerialReader(InputStream in)
        {
            this.in = in;
        }

        public void serialEvent(SerialPortEvent arg0) {

            try {
                CaliperFeedReaderRecordSupport readerRecord = readRecord();

                if (readerRecord != null && readerRecord instanceof CaliperFeedReaderMeasureRecord) {

                    // send new record to listeners
                    CaliperFeedReaderEvent e = new CaliperFeedReaderEvent(CaliperFeedReader.this, (CaliperFeedReaderMeasureRecord) readerRecord);
                    for (CaliperFeedReaderListener listener : listenerList.getListeners(CaliperFeedReaderListener.class)) {
                        listener.recordRead(e);
                    }

                }

            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not read record", e);
                }
            }
        }

        private CaliperFeedReaderRecordSupport readRecord() throws IOException {

            int len = 0;
            int data;

            while (( data = in.read()) > -1) {
                if (data == '\n') {
                    break;
                }
                buffer[len++] = (byte) data;
            }

            String result = new String(buffer,0,len);

            if (log.isInfoEnabled()) {
                log.info("New raw record: " + result);
            }
            CaliperFeedReaderRecordSupport readerRecord = recordFactory.newRecord(result);

            if (log.isDebugEnabled()) {
                log.debug("New feed record: " + readerRecord);
            }

            return readerRecord;
        }

    }

}
