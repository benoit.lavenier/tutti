package fr.ifremer.tutti.caliper.feed.record;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * TODO à mettre en commun entre l'ichtyo et le pied à coulisse ?
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public abstract class CaliperFeedReaderRecordSupport implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * Incoming raw record.
     */
    protected final String record;

    protected CaliperFeedReaderRecordSupport(String record) {
        this.record = record;
        Preconditions.checkState(isValid());
    }

    public abstract boolean isValid();

    public String getRecord() {
        return record;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("record", record)
                .toString();
    }
}
