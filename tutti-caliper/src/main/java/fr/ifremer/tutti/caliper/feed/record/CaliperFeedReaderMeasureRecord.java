package fr.ifremer.tutti.caliper.feed.record;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A measure record.
 *
 * Format is <pre>%l,v#</pre>.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CaliperFeedReaderMeasureRecord extends CaliperFeedReaderRecordSupport {

    private static final long serialVersionUID = 1L;

    /** Received values are like +12,345\n */
    private static final Pattern RECORD_PATTERN = Pattern.compile("\\+(\\w+),\\w+\\s*");

    public static boolean acceptRecord(String record) {

        Matcher matcher = RECORD_PATTERN.matcher(record);
        return matcher.matches();

    }

    protected final int measure;

    CaliperFeedReaderMeasureRecord(String record) {
        super(record);
        Matcher matcher = RECORD_PATTERN.matcher(record);
        matcher.find();
        String intPart = matcher.group(1);
        measure = Integer.valueOf(intPart);
    }

    public int getMeasure() {
        return measure;
    }

    @Override
    public boolean isValid() {
        return acceptRecord(record);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("record", measure)
                .append("measure", measure)
                .toString();
    }
}
