package fr.ifremer.tutti.caliper.feed.record;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CaliperFeedReaderRecordFactory {

    protected final Set<CaliperFeedReaderRecordAcceptor> acceptors;

    public CaliperFeedReaderRecordFactory() {
        acceptors = new LinkedHashSet<>();
        acceptors.add(new CaliperFeedReaderRecordAcceptor<CaliperFeedReaderMeasureRecord>() {
            @Override
            public boolean accept(String record) {
                return CaliperFeedReaderMeasureRecord.acceptRecord(record);
            }

            @Override
            public CaliperFeedReaderMeasureRecord newRecord(String record) {
                return new CaliperFeedReaderMeasureRecord(record);
            }
        });

    }

    public CaliperFeedReaderRecordSupport newRecord(String record) {

        CaliperFeedReaderRecordSupport feedRecord = null;

        for (CaliperFeedReaderRecordAcceptor acceptor : acceptors) {

            boolean accept = acceptor.accept(record);
            if (accept) {

                feedRecord = acceptor.newRecord(record);
                break;

            }

        }

        return feedRecord;

    }

    protected interface CaliperFeedReaderRecordAcceptor<F extends CaliperFeedReaderRecordSupport> {

        boolean accept(String record);

        F newRecord(String record);

    }

}
