.. -
.. * #%L
.. * Tutti
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2012 - 2014 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

==========================================================
Relation entre les versions de l'application et de la base
==========================================================

Présentation
------------

Depuis la version **2.4**, on est capable de faire des migrations de schémas, i.e
que lors de l'ouverture (ou import) d'une base, on vérifie la version du schéma
requit par l'applicatif (fait par adagio). (voir http://forge.codelutin.com/issues/2890).

Si la version du schéma de la base est inférieure à celle de l'applicatif, on
propose alors une migration de schéma.

Historique des versions de base
-------------------------------

Le tableau suivant résume les différents liens entre les versions :

+---------------------+------------+---------------------------+
| Application version | Db version | Allegro db schema version |
+=====================+============+===========================+
+ 2.3                 + 2013.06.04 + 3.2.1.01                  +
+---------------------+------------+---------------------------+
+ 2.4                 + 2013.08.22 + 3.2.3                     +
+---------------------+------------+---------------------------+
+ 2.8.1               + 2013.08.22 + 3.2.3                     +
+---------------------+------------+---------------------------+
+ 3.0-rc-2            + 2013.11.29 + 3.4.1                     +
+---------------------+------------+---------------------------+
+ 3.0                 + 2014.01.10 + 3.4.1                     +
+---------------------+------------+---------------------------+
+ 3.1                 + 2014.01.23 + 3.4.1                     +
+---------------------+------------+---------------------------+
+ 3.5                 + 2014.05.06 + 3.5.0                     +
+---------------------+------------+---------------------------+
+ 3.7                 + 2014.07.11 + 3.6.3                     +
+---------------------+------------+---------------------------+
+ 3.11.4              + ???        + 3.8.2                     +
+---------------------+------------+---------------------------+
+ 3.13                + 2015.02.04 + 3.8.3.1                   +
+---------------------+------------+---------------------------+
+ 3.13.6              + 2015.03.31 + 3.8.6.1                   +
+---------------------+------------+---------------------------+

Légende :

- *Application version* : Version de l'applicatif

- *Db version* : Version de la dernière base disponible sur le réseau en téléchargement ou pour mise à jour.

- *Allegro db schema version* : Version du schéma compatible avec l'applicatif (fourni par adagio).
