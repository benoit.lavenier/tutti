#!/bin/bash

BACKUP_DATE=~~BACKUP_DATE~~
if [ -d NEW/jre ]; then
  oldVersion=`cat jre/version.appup`
  newVersion=`cat NEW/jre/version.appup`
  echo "Update jre version $oldVersion to $newVersion"
  mkdir -p OLD
  backupdir=OLD/jre-$oldVersion-$BACKUP_DATE
  echo "Backup jre old version to $backupdir"
  mv jre $backupdir
  mv NEW/jre .
fi

chmod +x jre/bin/java
if [ ! -h java ]; then
  ln -sr jre/bin/java java
fi

if [ -d NEW/launcher ]; then
  oldVersion=`cat launcher/version.appup`
  newVersion=`cat NEW/launcher/version.appup`
  echo "Update launcher version $oldVersion to $newVersion"
  mkdir -p OLD
  backupdir=OLD/launcher-$oldVersion-$BACKUP_DATE
  echo "Backup launcher old version to $backupdir"
  mv *.sh launcher/
  mv launcher $backupdir

  mv NEW/launcher .
  rm launcher/*.exe
  rm launcher/*.bat

  mv launcher/*.sh .
  mv launcher/*.jar .
  mv launcher/README.txt .
  mv launcher/LICENSE.txt .
  chmod +x *.sh
fi

./tutti.sh