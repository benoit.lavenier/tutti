package fr.ifremer.tutti.ui.swing.updater;

/*
 * #%L
 * Tutti :: UI Updater
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * Created on 1/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.12.1
 */
public class UpdateModuleConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;

    public static UpdateModuleConfiguration applicationAndRuntime() {
        return new UpdateModuleConfiguration(true, true, true);
    }

    public static UpdateModuleConfiguration applicationAndNotRuntime() {
        return new UpdateModuleConfiguration(false, true, true);
    }

    public static UpdateModuleConfiguration data(boolean manageByUpdater) {
        return new UpdateModuleConfiguration(false, false, manageByUpdater);
    }

    private final boolean runtime;

    private final boolean application;

    private final boolean manageByUpdater;

    private UpdateModuleConfiguration(boolean runtime, boolean application, boolean manageByUpdater) {
        this.runtime = runtime;
        this.application = application;
        this.manageByUpdater = manageByUpdater;
    }

    public boolean isRuntime() {
        return runtime;
    }

    public boolean isApplication() {
        return application;
    }

    public boolean isData() {
        return !isApplication();
    }

    public boolean isManageByUpdater() {
        return manageByUpdater;
    }

}
