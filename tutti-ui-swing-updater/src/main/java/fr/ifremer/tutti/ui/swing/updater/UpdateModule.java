package fr.ifremer.tutti.ui.swing.updater;

/*
 * #%L
 * Tutti :: UI Updater
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 1/26/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.12
 */
public enum UpdateModule {

    // Application - runtime
    launcher(UpdateModuleConfiguration.applicationAndRuntime()),
    jre(UpdateModuleConfiguration.applicationAndRuntime()),

    // Application - not runtime
    tutti(UpdateModuleConfiguration.applicationAndNotRuntime()),
    i18n(UpdateModuleConfiguration.applicationAndNotRuntime()),
    help(UpdateModuleConfiguration.applicationAndNotRuntime()),
    ichtyometer(UpdateModuleConfiguration.applicationAndNotRuntime()),

    // Data
    report(UpdateModuleConfiguration.data(true)),
    db(UpdateModuleConfiguration.data(false));

    private final UpdateModuleConfiguration moduleConfiguration;

    UpdateModule(UpdateModuleConfiguration moduleConfiguration) {
        this.moduleConfiguration = moduleConfiguration;
    }

    public UpdateModuleConfiguration getModuleConfiguration() {
        return moduleConfiguration;
    }

    public String getModuleLoggerName() {
        return String.format("[Module %1$-20s]", name());
    }

}
