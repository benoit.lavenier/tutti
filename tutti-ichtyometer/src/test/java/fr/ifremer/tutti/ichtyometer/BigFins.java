package fr.ifremer.tutti.ichtyometer;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;

/**
 * Created on 12/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class BigFins {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BigFins.class);

    private static final String BIG_FIN_V2_NAME_PREFIX = "BigFin DFS/2-";

    public static void open(IchtyometerClient client) {

        RemoteDeviceChooser remoteDeviceChooser = remoteDeviceNames -> {
            String result = null;
            for (String remoteDeviceName : remoteDeviceNames) {
                if (remoteDeviceName.startsWith(BIG_FIN_V2_NAME_PREFIX) || remoteDeviceName.equals("Yo")) {
                    result = remoteDeviceName;
                    break;
                }
            }
            return result;
        };

        try {
            client.open(remoteDeviceChooser, true);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not connect to remote device",e);
            }
            Assume.assumeTrue("Could not connect to remote device", true);
        }

        Assume.assumeTrue("No device found!", client.isOpen());

    }
}
