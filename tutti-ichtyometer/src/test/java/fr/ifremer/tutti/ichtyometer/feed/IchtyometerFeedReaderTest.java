package fr.ifremer.tutti.ichtyometer.feed;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ichtyometer.BigFins;
import fr.ifremer.tutti.ichtyometer.IchtyometerClient;
import fr.ifremer.tutti.ichtyometer.feed.event.IchtyometerFeedReaderListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

/**
 * Created on 1/24/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
@Ignore
public class IchtyometerFeedReaderTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(IchtyometerFeedReaderTest.class);

    protected IchtyometerClient client;

    @Before
    public void setUp() throws Exception {

        client = new IchtyometerClient(2);
        BigFins.open(client);

    }

    @After
    public void tearDown() throws Exception {

        if (client != null) {
            client.close();
        }
    }

    @Test
    public void testFeedMode() throws IOException, InterruptedException {

        IchtyometerFeedReader reader = new IchtyometerFeedReader();

        IchtyometerFeedReaderListener listener = event -> log.info("Reader " + event.getSource() + " read : " + event.getRecord());
        reader.addFeedModeReaderListener(listener);

        reader.start(client);

        while (true) {
        }
    }
}
