package fr.ifremer.tutti.ichtyometer.tool;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.ichtyometer.IchtyometerClient;
import fr.ifremer.tutti.ichtyometer.RemoteDeviceChooser;
import fr.ifremer.tutti.ichtyometer.feed.IchtyometerFeedReader;
import fr.ifremer.tutti.ichtyometer.feed.event.IchtyometerFeedReaderListener;

import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created on 1/30/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.2
 */
public class FeedReaderTool {

    public static void main(String... args) throws IOException {

        final PrintWriter writer = System.console().writer();
        writer.println("FeedReaderTool: to display record in feed mode v1.0");

        RemoteDeviceChooser remoteDeviceChooser = remoteDeviceNames -> {
            List<String> remoteDeviceNameList = Lists.newArrayList(remoteDeviceNames);
            Console console = System.console();
            writer.println("Choose you device");
            int i = 0;
            for (String remoteDeviceName : remoteDeviceNameList) {
                writer.println(i++ + " for device " + remoteDeviceName);
            }
            writer.println("q (to quit)");
            writer.print("Your choice: ");
            writer.flush();

            String command = console.readLine();

            if ("q".equals(command)) {
                System.exit(0);
            }
            return remoteDeviceNameList.get(Integer.valueOf(command));
        };

        IchtyometerClient client = new IchtyometerClient(2);

        client.open(remoteDeviceChooser, true);

        IchtyometerFeedReader reader = new IchtyometerFeedReader();

        writer.println("client " + client + " is open and listen the board");

        IchtyometerFeedReaderListener listener = event -> writer.println("New record: " + event.getRecord());

        reader.addFeedModeReaderListener(listener);
        reader.start(client);

        while (true) {

        }
    }
}
