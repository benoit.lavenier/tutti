package fr.ifremer.tutti.ichtyometer.interactive;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created on 1/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class Command implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final String question;

    protected final String response;

    protected final String crc;

    public Command(String question, String response, String crc) {
        this.question = question;
        this.response = response;
        this.crc = crc;
    }

    public String getQuestion() {
        return question;
    }

    public String getResponse() {
        return response;
    }

    public String getCrc() {
        return crc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("question", question)
                .append("response", response)
                .append("crc", crc)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Command)) return false;

        Command command = (Command) o;

        return question.equals(command.question) && response.equals(command.response) &&
               !(crc != null ? !crc.equals(command.crc) : command.crc != null);

    }

    @Override
    public int hashCode() {
        int result = question.hashCode();
        result = 31 * result + response.hashCode();
        result = 31 * result + (crc != null ? crc.hashCode() : 0);
        return result;
    }
}
