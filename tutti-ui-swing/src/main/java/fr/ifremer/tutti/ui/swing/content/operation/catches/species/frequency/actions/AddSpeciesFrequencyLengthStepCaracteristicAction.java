package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class AddSpeciesFrequencyLengthStepCaracteristicAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    final SpeciesFrequencyUI ui;

    public AddSpeciesFrequencyLengthStepCaracteristicAction(SpeciesFrequencyUI ui) {
        this.ui = ui;

        putValue(NAME, t("tutti.editSpeciesFrequencies.action.addLengthStepCaracteristic"));
        putValue(SHORT_DESCRIPTION, t("tutti.editSpeciesFrequencies.action.addLengthStepCaracteristic.tip"));
        putValue(MNEMONIC_KEY, (int) SwingUtil.getFirstCharAt(t("tutti.editSpeciesFrequencies.action.addLengthStepCaracteristic.mnemonic"), 'Z'));

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // compute list of possible caracteristics (all but the one in the select box)
        List<Caracteristic> allNumericCaracteristic = ui.getHandler().getPersistenceService().getAllNumericCaracteristic();
        List<Caracteristic> toSelect = new ArrayList<>(allNumericCaracteristic);
        List<Caracteristic> knownCaracteristics = ui.getLengthStepCaracteristicComboBox().getData();
        toSelect.removeAll(knownCaracteristics);

        // open a dialog to select it

        BeanFilterableComboBox<Caracteristic> editor = new BeanFilterableComboBox<>();
        editor.setBeanType(Caracteristic.class);
        editor.setShowReset(true);

        ui.getHandler().initBeanFilterableComboBox(editor, toSelect, null);

        int response = JOptionPane.showConfirmDialog(
                ui.getHandler().getTopestUI(),
                editor,
                t("tutti.editSpeciesFrequencies.title.addLengthStepCaracteristic"),
                JOptionPane.OK_CANCEL_OPTION);

        Caracteristic selectedItem;
        if (response == JOptionPane.OK_OPTION) {
            selectedItem = (Caracteristic) editor.getSelectedItem();

//            // FIXME ? Should we add it to the combo box universe?
//            ui.getLengthStepCaracteristicComboBox().getData().add(selectedItem);
        } else {
            // user cancel selection
            selectedItem = null;
        }
        // set to model
        ui.getModel().setLengthStepCaracteristic(selectedItem);

    }

}
