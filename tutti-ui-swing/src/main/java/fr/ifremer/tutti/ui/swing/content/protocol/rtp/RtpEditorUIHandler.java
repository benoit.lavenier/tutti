package fr.ifremer.tutti.ui.swing.content.protocol.rtp;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.swing.ComponentMover;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.RowSorter;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class RtpEditorUIHandler extends AbstractTuttiUIHandler<RtpEditorUIModel, RtpEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RtpEditorUIHandler.class);

    @Override
    public void beforeInit(RtpEditorUI ui) {
        super.beforeInit(ui);

        RtpEditorUIModel model = new RtpEditorUIModel();

        this.ui.setContextValue(model);

    }

    @Override
    public void afterInit(RtpEditorUI ui) {

        super.initUI(ui);

        ui.pack();

//        ComponentResizer cr = new ComponentResizer();
//        cr.registerComponent(ui);
        ComponentMover cm = new ComponentMover();
//        cm.setDragInsets(cr.getDragInsets());
        cm.registerComponent(ui);

        ui.getBodyPanel().setRightDecoration(ui.getHeaderToolBar());

        RtpEditorUIModel model = getModel();
        listModelIsModify(model);
        listenValidatorValid(ui.getValidator(), model);

        model.addPropertyChangeListener(RtpEditorUIModel.PROPERTY_ROW, evt -> {
            EditProtocolSpeciesRowModel rowModel = getModel().getRowModel();

            Species species = rowModel.getSpecies();

            String decorate = decorate(species);

            if (log.isInfoEnabled()) {
                log.info("Edit RTP for species: " + decorate);
            }

            getUI().getBodyPanel().setTitle(t("tutti.rtpEdit.title", decorate));
        });
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getRtpMaleAField();
    }

    @Override
    public void onCloseUI() {

        boolean result = quitUI();

        if (result) {
            getModel().reset();
            ui.dispose();
        }
    }

    public boolean quitUI() {
        boolean result = true;

        RtpEditorUIModel model = getModel();

        if (model.isModify()) {
            if (model.isValid()) {

                result = quitUnsavedRtpForm();

            } else {

                // model is not valid, ask user to continue or not

                result = quitInvalidRtpForm();
            }
        }

        return result;
    }

    protected boolean quitUnsavedRtpForm() {
        // ask if user want to save, do not save or cancel action

        boolean result;

        int answer = askSaveBeforeLeaving(t("tutti.rtpEdit.askSaveBeforeLeaving"));
        switch (answer) {
            case JOptionPane.YES_OPTION:
                getContext().getActionEngine().runAction(ui.getSaveButton());
                result = true;
                break;

            case JOptionPane.NO_OPTION:
                result = true;
                break;

            default:
                // other case, use cancel action
                result = false;
        }
        return result;
    }

    protected boolean quitInvalidRtpForm() {
        return askCancelEditBeforeLeaving(t("tutti.rtpEdit.askCancelEditBeforeLeaving"));
    }

    @Override
    public SwingValidator<RtpEditorUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public Component getTopestUI() {
        return getUI();
    }

    public void setBean(EditProtocolSpeciesTableModel tableModel, RowSorter rowSorter, int row) {
        getModel().setRowModel(tableModel, rowSorter, row);
    }

    public void openEditor() {
        ui.setVisible(true);
    }

    public void closeEditor() {
        onCloseUI();
    }
}