package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.TuttiNumberTickUnitSource;
import fr.ifremer.tutti.util.Units;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnitSource;
import org.jfree.chart.axis.ValueAxis;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Closeable;

import static org.nuiton.i18n.I18n.t;

/**
 * To manage the frequencies histogram.
 *
 * Created on 14/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class FrequenciesHistogramHandler implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FrequenciesHistogramHandler.class);

    private final FrequenciesHistogramModel model;

    private final JFreeChart chart;
    private final JFrame popup;

    public FrequenciesHistogramHandler(SpeciesFrequencyUI ui) {

        SpeciesFrequencyUIModel uiModel = ui.getModel();
        SpeciesFrequencyUIHandler uiHandler = ui.getHandler();

        this.model = uiModel.getFrequenciesHistogramModel();

        chart = ChartFactory.createXYBarChart(null,
                                              t("tutti.editSpeciesFrequencies.table.header.lengthStep"),
                                              false,
                                              t("tutti.editSpeciesFrequencies.table.header.number"),
                                              model.getDataset());
        chart.clearSubtitles();

        ValueAxis rangeAxis = chart.getXYPlot().getRangeAxis();
        rangeAxis.setAutoRange(true);
        rangeAxis.setStandardTickUnits(new NumberTickUnitSource(true));

        ValueAxis domainAxis = chart.getXYPlot().getDomainAxis();
        domainAxis.setAutoRange(true);
        domainAxis.setStandardTickUnits(new TuttiNumberTickUnitSource(true));
        domainAxis.setMinorTickMarksVisible(true);

        chart.getXYPlot().getRenderer().setSeriesPaint(0, uiHandler.getConfig().getColorComputedWeights());

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setDomainZoomable(false);
        chartPanel.setMouseZoomable(false);
        chartPanel.setPopupMenu(null);

        ui.getHistogramPanel().add(chartPanel, BorderLayout.CENTER);

        popup = new JFrame();
        chartPanel.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getClickCount() > 1) {

                    popup.getContentPane().removeAll();
                    ChartPanel chartPanel = new ChartPanel(chart);
                    popup.getContentPane().add(chartPanel);
                    popup.pack();
                    popup.setVisible(true);

                }
            }
        });

        // when step has changed in model, update chart
        uiModel.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_STEP, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                Float step1 = (Float) evt.getNewValue();
                model.setStep(step1);

            }
        });

        // when lengthStepCaracteristicUnit changed, let's updates the label of some fields
        uiModel.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_LENGTH_STEP_CARACTERISTIC_UNIT, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                String unit = (String) evt.getNewValue();

                if (unit == null) {

                    unit = t("tutti.editSpeciesFrequencies.unkownStepUnit");
                }

                String lengthStepLabelWithUnit = Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.table.header.lengthStep"), unit);
                model.setLengthStepLabelWithUnit(lengthStepLabelWithUnit);

            }

        });

        model.addPropertyChangeListener(AverageWeightsHistogramModel.PROPERTY_TITLE, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                String title = (String) evt.getNewValue();
                if (log.isInfoEnabled()) {
                    log.info("Frequencies graph title changed to: " + title);
                }
                popup.setTitle(title);
            }
        });

        model.addPropertyChangeListener(AverageWeightsHistogramModel.PROPERTY_LENGTH_STEP_LABEL_WITH_UNIT, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                String lengthStepLabelWithUnit = (String) evt.getNewValue();
                if (log.isInfoEnabled()) {
                    log.info("Frequencies graph lengthStepLabelWithUnit changed to: " + lengthStepLabelWithUnit);
                }
                chart.getXYPlot().getDomainAxis().setLabel(lengthStepLabelWithUnit);
            }
        });

        model.addPropertyChangeListener(AverageWeightsHistogramModel.PROPERTY_STEP, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                Float step = (Float) evt.getNewValue();
                if (log.isInfoEnabled()) {
                    log.info("Frequencies graph step changed to: " + step);
                }

                chart.getXYPlot().getDomainAxis().setStandardTickUnits(new TuttiNumberTickUnitSource(step == 1f));
                model.getDataset().setIntervalWidth(step);

            }
        });

    }

    @Override
    public void close() {
        popup.dispose();
    }

}
