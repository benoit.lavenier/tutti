package fr.ifremer.tutti.ui.swing.content.protocol.rtp;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.persistence.entities.protocol.RtpBean;
import fr.ifremer.tutti.persistence.entities.protocol.Rtps;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.RowSorter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 14/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class RtpEditorUIModel extends AbstractTuttiBeanUIModel<EditProtocolSpeciesRowModel, RtpEditorUIModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RtpEditorUIModel.class);

    public static final String PROPERTY_RTP_MALE_A = "rtpMaleA";
    public static final String PROPERTY_RTP_MALE_B = "rtpMaleB";
    public static final String PROPERTY_RTP_FEMALE_A = "rtpFemaleA";
    public static final String PROPERTY_RTP_FEMALE_B = "rtpFemaleB";
    public static final String PROPERTY_RTP_UNDEFINED_A = "rtpUndefinedA";
    public static final String PROPERTY_RTP_UNDEFINED_B = "rtpUndefinedB";

    public static final String PROPERTY_FIRST_ROW = "firstRow";
    public static final String PROPERTY_LAST_ROW = "lastRow";
    public static final String PROPERTY_ROW = "row";

    protected final Rtp rtpMale = new RtpBean();

    protected final Rtp rtpFemale = new RtpBean();

    protected final Rtp rtpUndefined = new RtpBean();

    protected boolean firstRow;

    protected boolean lastRow;

    protected EditProtocolSpeciesTableModel tableModel;

    // list of the indexes of the rows in the table model
    // only the rows with lengthstep caracteristics are in the list
    protected List<Integer> rows = new ArrayList<>();

    protected int row = -1;

    public RtpEditorUIModel() {
        super(null, null);
    }

    @Override
    protected EditProtocolSpeciesRowModel newEntity() {
        return null;
    }

    public Rtp getRtpMale() {
        return rtpMale;
    }

    public Rtp getRtpUndefined() {
        return rtpUndefined;
    }

    public Rtp getRtpFemale() {
        return rtpFemale;
    }

    public Double getRtpMaleA() {
        return rtpMale.getA();
    }

    public void setRtpMaleA(Double a) {
        rtpMale.setA(a);
        firePropertyChanged(PROPERTY_RTP_MALE_A, null, a);
    }

    public Float getRtpMaleB() {
        return rtpMale.getB();
    }

    public void setRtpMaleB(Float b) {
        rtpMale.setB(b);
        firePropertyChanged(PROPERTY_RTP_MALE_B, null, b);
    }

    public Double getRtpFemaleA() {
        return rtpFemale.getA();
    }

    public void setRtpFemaleA(Double a) {
        rtpFemale.setA(a);
        firePropertyChanged(PROPERTY_RTP_FEMALE_A, null, a);
    }

    public Float getRtpFemaleB() {
        return rtpFemale.getB();
    }

    public void setRtpFemaleB(Float b) {
        rtpFemale.setB(b);
        firePropertyChanged(PROPERTY_RTP_FEMALE_B, null, b);
    }

    public Double getRtpUndefinedA() {
        return rtpUndefined.getA();
    }

    public void setRtpUndefinedA(Double a) {
        rtpUndefined.setA(a);
        firePropertyChanged(PROPERTY_RTP_UNDEFINED_A, null, a);
    }

    public Float getRtpUndefinedB() {
        return rtpUndefined.getB();
    }

    public void setRtpUndefinedB(Float b) {
        rtpUndefined.setB(b);
        firePropertyChanged(PROPERTY_RTP_UNDEFINED_B, null, b);
    }

    public boolean isFirstRow() {
        return firstRow;
    }

    public void setFirstRow(boolean firstRow) {
        Object oldValue = isFirstRow();
        this.firstRow = firstRow;
        firePropertyChange(PROPERTY_FIRST_ROW, oldValue, firstRow);
    }

    public boolean isLastRow() {
        return lastRow;
    }

    public void setLastRow(boolean lastRow) {
        Object oldValue = isLastRow();
        this.lastRow = lastRow;
        firePropertyChange(PROPERTY_LAST_ROW, oldValue, lastRow);
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        Object oldValue = getRow();
        this.row = row;
        firePropertyChange(PROPERTY_ROW, oldValue, row);
    }

    public void setRowModel(EditProtocolSpeciesTableModel tableModel, RowSorter rowSorter, int row) {

        this.tableModel = tableModel;

        rows.clear();
        int rowIndex = 0;

        for (int i = 0 ; i < tableModel.getRowCount() ; i++) {

            int rowIndexInModel = rowSorter.convertRowIndexToModel(i);
            EditProtocolSpeciesRowModel rowModel = tableModel.getEntry(rowIndexInModel);

            if (rowModel.withLengthStepPmfm()) {

                rows.add(rowIndexInModel);
                if (row == i) {
                    rowIndex = rows.size() - 1;
                }

            }

        }

        setRowModel(rowIndex);
    }

    public void setRowModel(int row) {

        if (log.isInfoEnabled()) {
            log.info("setRowModel " + row);
        }

        setRow(row);
        setFirstRow(row == 0);
        setLastRow(row == rows.size() - 1);

        EditProtocolSpeciesRowModel rowModel = getRowModel();

        Rtp rtpMale = rowModel.getRtpMale();
        setRtpMaleA(rtpMale == null ? null : rtpMale.getA());
        setRtpMaleB(rtpMale == null ? null : rtpMale.getB());

        Rtp rtpFemale = rowModel.getRtpFemale();
        setRtpFemaleA(rtpFemale == null ? null : rtpFemale.getA());
        setRtpFemaleB(rtpFemale == null ? null : rtpFemale.getB());

        Rtp rtpUndefined = rowModel.getRtpUndefined();
        setRtpUndefinedA(rtpUndefined == null ? null : rtpUndefined.getA());
        setRtpUndefinedB(rtpUndefined == null ? null : rtpUndefined.getB());

        setModify(false);
    }

    public EditProtocolSpeciesRowModel getRowModel() {
        int rowIndex = rows.get(row);
        return tableModel.getEntry(rowIndex);
    }

    public void reset() {
        if (row >= 0 && row < rows.size()) {
            setRowModel(row);
        }
    }

    public void updateRowRtp() {

        EditProtocolSpeciesRowModel rowModel = getRowModel();
        rowModel.setRtpMale(Rtps.newRtp(getRtpMale()));
        rowModel.setRtpFemale(Rtps.newRtp(getRtpFemale()));
        rowModel.setRtpUndefined(Rtps.newRtp(getRtpUndefined()));

        setModify(false);

        tableModel.fireTableRowsUpdated(row, row);

    }

}