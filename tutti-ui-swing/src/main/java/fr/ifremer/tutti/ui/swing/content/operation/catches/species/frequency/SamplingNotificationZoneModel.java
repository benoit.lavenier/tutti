package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created on 19/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class SamplingNotificationZoneModel extends AbstractSerializableBean {

    public static final String PROPERTY_SELECTED_ROW = "selectedRow";
    public static final String PROPERTY_UPDATED_ROW = "updatedRow";
    public static final String PROPERTY_SAMPLING_NOTIFICATION_ZONE_STATUS = "samplingNotificationZoneStatus";
    public static final String PROPERTY_SUMMARY_TEXT = "summaryText";

    /**
     * Pour bloquer le recalcul du modèle lors du changement de sélection (Pour le mode rafale, où l'on ajoute la
     * ligne puis ensuite on la sélectionne).
     */
    private boolean valueAdjusting;

    public void setSelectedRow(IndividualObservationBatchRowModel selectedRow) {
        firePropertyChange(PROPERTY_SELECTED_ROW, null /* On force la propagation de l'évènement! */, selectedRow);
    }

    public void setUpdatedRow(IndividualObservationBatchRowModel updatedRow) {
        firePropertyChange(PROPERTY_UPDATED_ROW, null /* On force la propagation de l'évènement! */, updatedRow);
    }

    public void setSamplingNotificationZoneStatus(SamplingNotificationZoneStatus samplingNotificationZoneStatus) {
        firePropertyChange(PROPERTY_SAMPLING_NOTIFICATION_ZONE_STATUS, null  /* On force la propagation de l'évènement! */, samplingNotificationZoneStatus);
    }

    public void setSummaryText(String summaryText) {
        firePropertyChange(PROPERTY_SUMMARY_TEXT, null /* On force la propagation de l'évènement! */, summaryText);
    }

    public void setValueAdjusting(boolean valueAdjusting) {
        this.valueAdjusting = valueAdjusting;
    }

    public boolean isValueAdjusting() {
        return valueAdjusting;
    }
}
