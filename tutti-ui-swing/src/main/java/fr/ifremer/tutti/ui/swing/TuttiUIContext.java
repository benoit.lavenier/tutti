package fr.ifremer.tutti.ui.swing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.caliper.feed.CaliperFeedReader;
import fr.ifremer.tutti.ichtyometer.feed.IchtyometerFeedReader;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.service.ClosedPersistenceService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.service.bigfin.BigfinImportService;
import fr.ifremer.tutti.service.catches.ValidateCruiseOperationsService;
import fr.ifremer.tutti.service.catches.WeightCleaningService;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.service.catches.multipost.MultiPostExportService;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportService;
import fr.ifremer.tutti.service.export.cps.CalcifiedPiecesSamplingExportService;
import fr.ifremer.tutti.service.export.pdf.CatchesPdfExportService;
import fr.ifremer.tutti.service.export.sumatra.CatchesSumatraExportService;
import fr.ifremer.tutti.service.export.toconfirmreport.ToConfirmReportService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportService;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportService;
import fr.ifremer.tutti.service.operationimport.FishingOperationImportService;
import fr.ifremer.tutti.service.protocol.ProtocolCaracteristicsImportExportService;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.service.psionimport.PsionImportService;
import fr.ifremer.tutti.service.pupitri.PupitriExportService;
import fr.ifremer.tutti.service.pupitri.PupitriImportService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryGearService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryPersonService;
import fr.ifremer.tutti.service.referential.ReferentialTemporarySpeciesService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryVesselService;
import fr.ifremer.tutti.service.referential.TuttiReferentialSynchronizeService;
import fr.ifremer.tutti.service.report.ReportGenerationService;
import fr.ifremer.tutti.service.cruise.CruiseCacheLoader;
import fr.ifremer.tutti.ui.swing.content.MainUI;
import fr.ifremer.tutti.ui.swing.updater.DeleteHelper;
import fr.ifremer.tutti.ui.swing.util.SoundEngine;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.UIMessageNotifier;
import fr.ifremer.tutti.ui.swing.util.auth.AuthenticationInfo;
import fr.ifremer.tutti.ui.swing.util.auth.LoginUI;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.editor.bean.BeanDoubleList;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.swing.help.JAXXHelpBroker;
import jaxx.runtime.swing.help.JAXXHelpUIHandler;
import jaxx.runtime.swing.session.BeanDoubleListState;
import jaxx.runtime.swing.session.BeanFilterableComboBoxState;
import jaxx.runtime.swing.session.State;
import jaxx.runtime.swing.session.SwingSession;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.converter.ConverterUtil;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.i18n.init.UserI18nInitializer;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.jaxx.application.listener.PropagatePropertyChangeListener;
import org.nuiton.jaxx.application.swing.ApplicationUIContext;
import org.nuiton.jaxx.application.swing.action.ApplicationActionEngine;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;
import org.nuiton.jaxx.application.swing.action.ApplicationUIAction;
import org.nuiton.jaxx.application.swing.util.ApplicationErrorHelper;

import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * UI application context.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class TuttiUIContext extends AbstractBean implements Closeable, UIMessageNotifier, JAXXHelpUIHandler, PropagatePropertyChangeListener.PropagatePropertyChange, ApplicationUIContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiUIContext.class);

    public static final String PROPERTY_PROGRAM_ID = "programId";

    public static final String PROPERTY_CRUISE_ID = "cruiseId";

    public static final String PROPERTY_PROTOCOL_ID = "protocolId";

    public static final String PROPERTY_SCREEN = "screen";

//    public static final String PROPERTY_PROGRAM_FILLED = "programFilled";
//
//    public static final String PROPERTY_CRUISE__FILLED = "cruiseFilled";
//
//    public static final String PROPERTY_PROTOCOL_FILLED = "protocolFilled";

    public static final String PROPERTY_VALIDATION_CONTEXT = "validationContext";

    public static final String PROPERTY_ICHTYOMETER_CONNECTED = "ichtyometerConnected";

    public static final String PROPERTY_CALIPER_CONNECTED = "caliperConnected";

    public static final String PROPERTY_BUSY = "busy";

    public static final String PROPERTY_HIDE_BODY = "hideBody";

    public static final String PROPERTY_LOCALE = "locale";

    public static final Set<String> PROPERTIES_TO_SAVE = Sets.newHashSet(
            PROPERTY_PROGRAM_ID,
            PROPERTY_CRUISE_ID,
            PROPERTY_PROTOCOL_ID,
            PROPERTY_LOCALE);

    public static final String PROPERTY_DB_EXIST = "dbExist";

    public static final String PROPERTY_DB_LOADED = "dbLoaded";

    /**
     * Application context (only one for all the application).
     *
     * @since 0.1
     */
    private static TuttiUIContext applicationContext;

    /**
     * Application global configuration.
     *
     * @since 0.1
     */
    protected final TuttiConfiguration config;

    /**
     * Service context used by any service.
     *
     * @since 0.1
     */
    protected final TuttiServiceContext serviceContext;

    /**
     * Swing session used to save ui states.
     *
     * @since 0.1
     */
    protected final SwingSession swingSession;

    /**
     * Erro helper.
     *
     * @since 1.0
     */
    protected final ApplicationErrorHelper errorHelper;

    /**
     * Shared data context.
     *
     * @since 1.0.2
     */
    protected TuttiDataContext dataContext;

    /**
     * Tutti help broker.
     *
     * @since 1.1
     */
    protected TuttiHelpBroker helpBroker;

    /**
     * Current screen displayed in ui.
     *
     * @since 0.1
     */
    protected TuttiScreen screen;

    /**
     * Current locale used in application.
     *
     * @since 1.0.3
     */
    protected Locale locale;

    /**
     * Busy state ({@code true} when a blocking action is running).
     *
     * @since 1.0.3
     */
    protected boolean busy;

    /**
     * Flag to hide (or not) the body of application.
     *
     * @since 1.1
     */
    protected boolean hideBody;

    /**
     * Message notifiers.
     *
     * @since 0.3
     */
    protected final Set<UIMessageNotifier> messageNotifiers;

    /**
     * Validation context (used by fishingOperation screens).
     *
     * @since 0.3
     */
    private String validationContext;

    private MainUI mainUI;

    private ApplicationActionUI actionUI;

    /**
     * Flag to know if there is an exsiting db.
     *
     * @since 1.0
     */
    private boolean dbExist;

    /**
     * Flag to know if there is a loaded db.
     *
     * @since 1.0
     */
    private boolean dbLoaded;

    private Properties helpMapping;

    private final TuttiActionFactory tuttiActionFactory;

    private final ApplicationActionEngine tuttiActionEngine;

    /**
     * To keep authentication credential in memory.
     *
     * @since 3.1
     */
    private final Map<String, AuthenticationInfo> updateAuthenticationStore;

    /**
     * Ichtyometer Reader.
     *
     * @since 3.1
     */
    private IchtyometerFeedReader ichtyometerReader;

    /**
     * Caliper Reader.
     *
     * @since 4.5
     */
    private CaliperFeedReader caliperFeedReader;

    /**
     * A file lock to prevent multiple instance. the lock is create in the {@link #init()} method and remove in the
     * {@link #close()} method.
     *
     * @since 3.2
     */
    private File lock;

    /**
     * Flag to know if context was already closed. Can happen when you close nicely the application, the shutdown hook
     * must then not close a second time this context.
     *
     * @since 3.2
     */
    private boolean closed;

    private final SoundEngine soundEngine;

    public static TuttiUIContext newContext(TuttiConfiguration config) {
        Preconditions.checkNotNull(config);
        Preconditions.checkState(applicationContext == null,
                                 "Application context was already opened!");
        applicationContext = new TuttiUIContext(config);
        return applicationContext;
    }

    public static TuttiUIContext getApplicationContext() {
        return applicationContext;
    }

    public ApplicationErrorHelper getErrorHelper() {
        return applicationContext.errorHelper;
    }

    @Override
    public String getI18nPrefix() {
        return "tutti.property.";
    }

    @Override
    public String getDateFormat() {
        return getConfig().getDateFormat();
    }

    @Override
    public boolean isActionInProgress(ApplicationUIAction action) {
        // TODO do something
        return false;
    }

    @Override
    public void setActionInProgress(ApplicationUIAction action, boolean actionInProgress) {
        // TODO do something
    }

    protected TuttiUIContext(TuttiConfiguration config) {
        this.config = config;
        this.serviceContext = new TuttiServiceContext(config);

        Map<Class, State> additionalStates = Maps.newHashMap();
        additionalStates.put(BeanFilterableComboBox.class, new BeanFilterableComboBoxState());
        additionalStates.put(BeanDoubleList.class, new BeanDoubleListState());
        this.swingSession = SwingSession.newSession(getConfig().getUIConfigFile(), false, additionalStates);

        //FIXME Push this to ifremer-shared
        this.errorHelper = new ApplicationErrorHelper(this) {

            @Override
            public void showWarningDialog(String message) {

                JOptionPane.showMessageDialog(context.getActionUI(), "<html><body>" + message + "</body></html>",
                                              t("application.error.ui.business.warning"),
                                              JOptionPane.WARNING_MESSAGE);
            }

            @Override
            public void showErrorDialog(String message, Throwable cause) {
                super.showErrorDialog(message, cause);
            }
        };
        this.dataContext = serviceContext.getDataContext();
        PropagatePropertyChangeListener.listenAndPropagateAll(dataContext, this);
        UIMessageNotifier logMessageNotifier = new UIMessageNotifier() {

            @Override
            public void showInformationMessage(String message) {
                if (StringUtils.isNotBlank(message)) {
                    message = message.replaceAll("\\<strong\\>", "");
                    message = message.replaceAll("\\<.strong\\>", "");
                    message = message.replaceAll("\\<li\\>", "");
                    message = message.replaceAll("\\<.li\\>", "");
                    message = message.replaceAll("\\<ul\\>", "");
                    message = message.replaceAll("\\<.ul\\>", "");
                    if (log.isInfoEnabled()) {
                        log.info(message);
                    }
                }
            }
        };
        this.messageNotifiers = Sets.newHashSet();
        addMessageNotifier(logMessageNotifier);
        tuttiActionFactory = new TuttiActionFactory();
        tuttiActionEngine = new ApplicationActionEngine(tuttiActionFactory);
        this.updateAuthenticationStore = Maps.newTreeMap();
        this.soundEngine = new SoundEngine(config);
    }

    @Override
    public TuttiConfiguration getConfiguration() {
        return config;
    }

    @Override
    public Component getBodyUI() {
        MainUI mainUI = getMainUI();
        return mainUI == null ? null : mainUI.getBody();
    }

    @Override
    public Component getStatusUI() {
        MainUI mainUI = getMainUI();
        return mainUI == null ? null : mainUI.getStatus();
    }

    //------------------------------------------------------------------------//
    //-- Open / close methods                                               --//
    //------------------------------------------------------------------------//

    public void init() {

        config.prepareDirectories();

        // converters are stored in current classloader, we need then to rescan them
        // each time we change current classloader
        ConverterUtil.deregister();
        ConverterUtil.initConverters();

        // Use shutdownHook to close context on System.exit
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (!closed) {
                close();
            }
        }));

        //--------------------------------------------------------------------//
        // init db configuration
        //--------------------------------------------------------------------//

        config.initConfig();

//        // clean db cache (avoid a lots of headache :(
//        File cacheDirectory = config.getServiceConfig().getPersistenceConfig().getCacheDirectory();
//        if (cacheDirectory.exists()) {
//            // clean cache directory (fix soem headaches...)
//            TuttiIOUtil.cleanDirectory(
//                    cacheDirectory,
//                    t("tutti.db.deleteCache.error", cacheDirectory));
//        }

        //--------------------------------------------------------------------//
        // init i18n
        //--------------------------------------------------------------------//
        File i18nDirectory = config.getI18nDirectory();
        if (!config.isFullLaunchMode()) {

            i18nDirectory = new File(config.getDataDirectory(), "i18n");

            if (i18nDirectory.exists()) {
                // clean i18n cache
                ApplicationIOUtil.cleanDirectory(
                        i18nDirectory,
                        t("tutti.i18n.deleteCache.error", i18nDirectory));
            }
        }

        ApplicationIOUtil.forceMkdir(i18nDirectory,
                                     t("tutti.i18n.mkDir.error", i18nDirectory));

        if (log.isDebugEnabled()) {
            log.debug("I18N directory: " + i18nDirectory);
        }

        Locale i18nLocale = config.getI18nLocale();

        if (log.isInfoEnabled()) {
            log.info(String.format("Starts i18n with locale [%s] at [%s]",
                                   i18nLocale, i18nDirectory));
        }
        I18n.init(new UserI18nInitializer(
                          i18nDirectory, new DefaultI18nInitializer("tutti-i18n")),
                  i18nLocale);


        //--------------------------------------------------------------------//
        // init lock
        //--------------------------------------------------------------------//

        lock = new File(config.getBasedir(), "tutti.lock");

        if (lock.exists()) {
            lock = null;
            throw new ApplicationBusinessException(t("tutti.error.application.already.started"));
        }

        ApplicationIOUtil.writeContent(lock, new Date().toString(), "Could not create lock file");
        if (log.isInfoEnabled()) {
            log.info("Create lock file: " + lock);
        }


        //--------------------------------------------------------------------//
        // init help
        //--------------------------------------------------------------------//

        File helpDirectory = config.getHelpDirectory();

        if (!config.isFullLaunchMode()) {

            if (!helpDirectory.exists()) {
                helpDirectory = new File(config.getDataDirectory(), "help");
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Help directory: " + helpDirectory);
        }
        ApplicationIOUtil.forceMkdir(
                helpDirectory,
                t("tutti.help.mkDir.error", helpDirectory));

        // load help mapping
        String mappingProperties = "/tutti-help-fr.properties";
        try {

            InputStream resourceAsStream =
                    getClass().getResourceAsStream(mappingProperties);
            helpMapping = new Properties();
            helpMapping.load(resourceAsStream);

        } catch (Exception eee) {
            log.error("Failed to load help mapping file at '" +
                              mappingProperties + "'", eee);
        }
        if (log.isInfoEnabled()) {
            log.info(String.format("Starts help with locale at [%s]",
                                   helpDirectory));
        }

        //--------------------------------------------------------------------//
        // init action UI
        //--------------------------------------------------------------------//
        setActionUI(new ApplicationActionUI(null, this));
    }

    public void open() {

        setLocale(config.getI18nLocale());

        if (getProgramId() == null) {

            // load it from config
            setProgramId(config.getProgramId());
        }

        if (getCruiseId() == null) {

            // load it from config
            setCruiseId(config.getCruiseId());
        }

        if (getProtocolId() == null) {

            // load it from config
            setProtocolId(config.getProtocolId());
        }

        boolean dbExists = config.isDbExists();

        setDbExist(dbExists);

        if (!dbExists) {

            setProtocolId(null);
            setProgramId(null);
            setCruiseId(null);
            setDbLoaded(false);
        }

        // save back to config
        saveContextToConfig();

        // list when programId or campaingId change to save the configuration
        addPropertyChangeListener(evt -> {

            if (PROPERTIES_TO_SAVE.contains(evt.getPropertyName())) {
                saveContextToConfig();
            }
        });

        soundEngine.open();

    }

    @Override
    public void close() {

        if (log.isInfoEnabled()) {
            log.info("Closing application context...");
        }
        Preconditions.checkState(!closed, "Application was already closed.");

        try {
            // Clear data references
            messageNotifiers.clear();
            validationContext = null;
            IOUtils.closeQuietly(dataContext);

            if (timer != null) {
                timer.purge();
                timer.cancel();
            }

            setScreen(null);

            IOUtils.closeQuietly(serviceContext);
            IOUtils.closeQuietly(ichtyometerReader);
            IOUtils.closeQuietly(caliperFeedReader);
            IOUtils.closeQuietly(soundEngine);

            // remove listeners
            PropertyChangeListener[] listeners = getPropertyChangeListeners();
            for (PropertyChangeListener listener : listeners) {
                if (log.isDebugEnabled()) {
                    log.debug("Remove listener: " + listener);
                }
                removePropertyChangeListener(listener);
            }
            setMainUI(null);
            if (actionUI != null) {

                // close action ui
                actionUI.getModel().clear();
            }
            setActionUI(null);

        } finally {
            closed = true;
            if (lock != null) {

                // destroy lock
                ApplicationIOUtil.deleteFile(lock, "Could not delete lock file");
                if (log.isInfoEnabled()) {
                    log.info("Delete lock file: " + lock);
                }
            }
        }
    }

    //------------------------------------------------------------------------//
    //-- Service methods                                                    --//
    //------------------------------------------------------------------------//

    public PersistenceService getPersistenceService() {

        PersistenceService service;

        if (useRealPersistenceService()) {
            service = dataContext.getService();
            if (service == null) {

                // use real service
                service = serviceContext.getService(PersistenceService.class);
                dataContext.setPersistenceService(service);
//                dataContext.open(config, service);
            }
        } else {
            service = serviceContext.getService(ClosedPersistenceService.class);
        }
        return service;
    }

    public DecoratorService getDecoratorService() {
        return serviceContext.getService(DecoratorService.class);
    }

    public TuttiReferentialSynchronizeService getTuttiReferentialSynchronizeService() {
        return serviceContext.getService(TuttiReferentialSynchronizeService.class);
    }

    public ProtocolImportExportService getTuttiProtocolImportExportService() {
        return serviceContext.getService(ProtocolImportExportService.class);
    }

    public ProtocolCaracteristicsImportExportService getProtocolCaracteristicsImportExportService() {
        return serviceContext.getService(ProtocolCaracteristicsImportExportService.class);
    }

    public PupitriImportService getPupitriImportService() {
        return serviceContext.getService(PupitriImportService.class);
    }

    public PupitriExportService getPupitriExportService() {
        return serviceContext.getService(PupitriExportService.class);
    }

    public PsionImportService getTuttiPsionImportService() {
        return serviceContext.getService(PsionImportService.class);
    }

    public BigfinImportService getTuttiBigfinImportService() {
        return serviceContext.getService(BigfinImportService.class);
    }

    public ReferentialTemporaryGearService getReferentialTemporaryGearService() {
        return serviceContext.getService(ReferentialTemporaryGearService.class);
    }

    public ReferentialTemporaryPersonService getReferentialTemporaryPersonService() {
        return serviceContext.getService(ReferentialTemporaryPersonService.class);
    }

    public ReferentialTemporarySpeciesService getReferentialTemporarySpeciesService() {
        return serviceContext.getService(ReferentialTemporarySpeciesService.class);
    }

    public ReferentialTemporaryVesselService getReferentialTemporaryVesselService() {
        return serviceContext.getService(ReferentialTemporaryVesselService.class);
    }

    public WeightComputingService getWeightComputingService() {
        return serviceContext.getService(WeightComputingService.class);
    }

    public WeightCleaningService getWeightCleaningService() {
        return serviceContext.getService(WeightCleaningService.class);
    }

    public ValidateCruiseOperationsService getValidateCruiseOperationsService() {
        return serviceContext.getService(ValidateCruiseOperationsService.class);
    }

    public ValidationService getValidationService() {
        return serviceContext.getService(ValidationService.class);
    }

    public CatchesPdfExportService getGeneratePDFService() {
        return serviceContext.getService(CatchesPdfExportService.class);
    }

    public ToConfirmReportService getToConfirmReportService() {
        return serviceContext.getService(ToConfirmReportService.class);
    }

    public GenericFormatExportService getGenericFormatExportService() {
        return serviceContext.getService(GenericFormatExportService.class);
    }

    public GenericFormatImportService getGenericFormatImportService() {
        return serviceContext.getService(GenericFormatImportService.class);
    }

    public CatchesSumatraExportService getCatchesSumatraExportService() {
        return serviceContext.getService(CatchesSumatraExportService.class);
    }

    public CalcifiedPiecesSamplingExportService getCalcifiedPiecesSamplingExportService() {
        return serviceContext.getService(CalcifiedPiecesSamplingExportService.class);
    }

    public MultiPostImportService getMultiPostImportService() {
        return serviceContext.getService(MultiPostImportService.class);
    }

    public MultiPostExportService getMultiPostExportService() {
        return serviceContext.getService(MultiPostExportService.class);
    }

    public FishingOperationImportService getFishingOperationImportService() {
        return serviceContext.getService(FishingOperationImportService.class);
    }

    public ReportGenerationService getReportGenerationService() {
        return serviceContext.getService(ReportGenerationService.class);
    }

    public SoundEngine getSoundEngine() {
        return soundEngine;
    }

    public boolean useRealPersistenceService() {
        return isDbExist() && isDbLoaded();
    }

    private PersistenceService reloadPersistenceService() {

        try {
            serviceContext.close();
        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.context.service.close.error"), e);
        }
        dataContext.close();

        return getPersistenceService();
    }

    public void reloadDecoratorService() {
        serviceContext.reloadService(DecoratorService.class);
    }

    //------------------------------------------------------------------------//
    //-- DataContext methods                                                --//
    //------------------------------------------------------------------------//

    public TuttiDataContext getDataContext() {
        return dataContext;
    }

    public boolean isCruiseFilled() {
        return dataContext.isCruiseFilled();
    }

    public String getProgramId() {
        return dataContext.getProgramId();
    }

    public boolean isProtocolFilled() {
        return dataContext.isProtocolFilled();
    }

    public String getProtocolId() {
        return dataContext.getProtocolId();
    }

    public Integer getCruiseId() {
        return dataContext.getCruiseId();
    }

    public boolean isProgramFilled() {
        return dataContext.isProgramFilled();
    }

    public void setProgramId(String programId) {
        dataContext.setProgramId(programId);
    }

    public void setCruiseId(Integer cruiseId) {
        dataContext.setCruiseId(cruiseId);
    }

    public void setProtocolId(String protocolId) {
        dataContext.setProtocolId(protocolId);
    }

    public void clearDbContext() {
        dataContext.clearContext();
    }

    public void checkDbContext() {

        // open data context
        dataContext.open(config);

        // ask data context to check his ids
        dataContext.checkDbContext();

        // save config
        saveContextToConfig();

    }

    public CruiseCacheLoader createCruiseCacheLoader(ProgressionModel progressionModel) {

        return CruiseCacheLoader.newCacheLoader(getPersistenceService(),
                                                getDecoratorService(),
                                                progressionModel,
                                                getDataContext().getProtocol(),
                                                getCruiseId());

    }

    //------------------------------------------------------------------------//
    //-- Db methods                                                         --//
    //------------------------------------------------------------------------//

    public boolean isDbExist() {
        return dbExist;
    }

    public void setDbExist(boolean dbExist) {
        this.dbExist = dbExist;
        firePropertyChange(PROPERTY_DB_EXIST, null, dbExist);
    }

    public boolean isDbLoaded() {
        return dbLoaded;
    }

    private void setDbLoaded(boolean dbLoaded) {
        this.dbLoaded = dbLoaded;
        firePropertyChange(PROPERTY_DB_LOADED, null, dbLoaded);
    }

    //------------------------------------------------------------------------//
    //-- Config methods                                                     --//
    //------------------------------------------------------------------------//

    public TuttiConfiguration getConfig() {
        return config;
    }

    protected void saveContextToConfig() {
        if (log.isInfoEnabled()) {
            log.info("Save config (programId: " + getProgramId() + ", cruiseId: " +
                             getCruiseId() + ", protocolId: " + getProtocolId() + ", locale: " +
                             getLocale() + ")");
        }
        config.setProgramId(getProgramId());
        config.setCruiseId(getCruiseId());
        config.setProtocolId(getProtocolId());
        config.setI18nLocale(getLocale());
        config.save();
    }


    //------------------------------------------------------------------------//
    //-- UI methods                                                         --//
    //------------------------------------------------------------------------//

    @Override
    public MainUI getMainUI() {
        return mainUI;
    }

    public void setMainUI(MainUI mainUI) {
        this.mainUI = mainUI;
    }

    @Override
    public ApplicationActionUI getActionUI() {
        return actionUI;
    }

    @Override
    public ApplicationActionUI getExistingActionUI() {
        while (actionUI == null || !actionUI.isShowing()) {

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                // ignore this one
            }
        }
        return actionUI;
    }

    public void setActionUI(ApplicationActionUI actionUI) {
        this.actionUI = actionUI;
    }

    public void addInSwingSession(Component c, boolean replace) {

        try {

            swingSession.add(c, replace);

            saveSwingSession();

        } catch (Exception e) {
            if (log.isWarnEnabled()) {
                log.warn("Could not add component " + c + "in swingSession file: " + swingSession.getFile(), e);
            }
        }

    }

    public void saveSwingSession() {

        try {
            swingSession.save();
        } catch (Exception e) {
            if (log.isWarnEnabled()) {
                log.warn("Could not save ui config file: " + swingSession.getFile(), e);
            }
        }

    }

    public TuttiScreen getScreen() {
        return screen;
    }

    public void setScreen(TuttiScreen screen) {
        Object oldValue = getScreen();
        this.screen = screen;
        firePropertyChange(PROPERTY_SCREEN, oldValue, screen);
    }

    @Override
    public boolean isBusy() {
        return busy;
    }

    @Override
    public void setBusy(boolean busy) {
        this.busy = busy;
        firePropertyChange(PROPERTY_BUSY, null, busy);
    }

    @Override
    public boolean isHideBody() {
        return hideBody;
    }

    @Override
    public void setHideBody(boolean hideBody) {
        this.hideBody = hideBody;
        firePropertyChange(PROPERTY_HIDE_BODY, null, hideBody);
    }

    @Override
    public Color getColorBlockingLayer() {
        return getConfig().getColorBlockingLayer();
    }

    @Override
    public TuttiActionFactory getActionFactory() {
        return tuttiActionFactory;
    }

    @Override
    public ApplicationActionEngine getActionEngine() {
        return tuttiActionEngine;
    }

    //------------------------------------------------------------------------//
    //-- UIMessageNotifier methods                                          --//
    //------------------------------------------------------------------------//

    public void addMessageNotifier(UIMessageNotifier messageNotifier) {
        this.messageNotifiers.add(messageNotifier);
    }

    public void removeMessageNotifier(UIMessageNotifier messageNotifier) {
        this.messageNotifiers.remove(messageNotifier);
    }

    @Override
    public void showInformationMessage(String message) {
        for (UIMessageNotifier messageNotifier : messageNotifiers) {
            messageNotifier.showInformationMessage(message);
        }
    }

    //------------------------------------------------------------------------//
    //-- Help methods                                                       --//
    //------------------------------------------------------------------------//

    public TuttiHelpBroker getHelpBroker() {
        return helpBroker;
    }

    public void setHelpBroker(TuttiHelpBroker helpBroker) {
        this.helpBroker = helpBroker;
    }

    @Override
    public void showHelp(JAXXContext context,
                         JAXXHelpBroker broker,
                         String helpId) {
        if (helpId == null) {
            helpId = broker.getDefaultID();
        }

        if (log.isInfoEnabled()) {
            log.info("show help " + helpId);
        }

        String value = (String) helpMapping.get(helpId);

        if (value == null) {
            throw new ApplicationTechnicalException(t("tutti.context.helpPage.notFound", helpId));
        }

        String helpDirectory = getConfig().getHelpResourceWithLocale(value);
        boolean withFragment = helpDirectory.contains("#");

        String fragment = null;
        if (withFragment) {
            fragment = StringUtils.substringAfter(helpDirectory, "#");
            helpDirectory = StringUtils.substringBefore(helpDirectory, "#");
        }

        URI resolvedUri = new File(helpDirectory).toURI();
        try {

            if (withFragment) {
                resolvedUri = new URI(resolvedUri.toString() + "#" + fragment);
            }
            if (log.isInfoEnabled()) {
                log.info("help uri = " + resolvedUri);
            }
            TuttiUIUtil.openLink(resolvedUri);
        } catch (URISyntaxException e) {
            throw new ApplicationTechnicalException(t("tutti.context.helpPage.notFound", resolvedUri));
        }

    }

    //------------------------------------------------------------------------//
    //-- Authentication methods                                             --//
    //------------------------------------------------------------------------//

    public AuthenticationInfo getAuthenticationInfo(String url) {

        // get existing info
        AuthenticationInfo authentication = updateAuthenticationStore.get(url);

        // ask user authentication
        authentication = new LoginUI(this).open(url, authentication).getAuthenticationInfo();

        if (authentication != null) {

            // store it back in authentication store
            updateAuthenticationStore.put(url, authentication);
        }

        return authentication;
    }

    //------------------------------------------------------------------------//
    //-- Ichtyometer methods                                                --//
    //------------------------------------------------------------------------//

    public IchtyometerFeedReader getIchtyometerReader() {
        return ichtyometerReader;
    }

    public void setIchtyometerReader(IchtyometerFeedReader ichtyometerReader) {
        this.ichtyometerReader = ichtyometerReader;
        firePropertyChange(PROPERTY_ICHTYOMETER_CONNECTED, null, isIchtyometerConnected());
    }

    public boolean isIchtyometerConnected() {
        return ichtyometerReader != null;
    }

    //------------------------------------------------------------------------//
    //-- Caliper methods                                                --//
    //------------------------------------------------------------------------//

    public CaliperFeedReader getCaliperReader() {
        return caliperFeedReader;
    }

    public void setCaliperReader(CaliperFeedReader caliperFeedReader) {
        this.caliperFeedReader = caliperFeedReader;
        firePropertyChange(PROPERTY_CALIPER_CONNECTED, null, isCaliperConnected());
    }

    public boolean isCaliperConnected() {
        return caliperFeedReader != null;
    }

    //------------------------------------------------------------------------//
    //-- Other methods                                                      --//
    //------------------------------------------------------------------------//

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
        firePropertyChange(PROPERTY_LOCALE, null, locale);
    }

    public void setValidationContext(String validationContext) {
        Object oldValue = getValidationContext();
        this.validationContext = validationContext;
        firePropertyChange(PROPERTY_VALIDATION_CONTEXT, oldValue, validationContext);
    }

    public String getValidationContext() {
        return validationContext;
    }

    @Override
    public void firePropertyChanged(String propertyName,
                                    Object oldValue,
                                    Object newValue) {
        firePropertyChange(propertyName, oldValue, newValue);
    }

    public void setFallBackScreen() {
        if (isDbLoaded()) {
            setScreen(TuttiScreen.SELECT_CRUISE);
        } else {
            setScreen(TuttiScreen.MANAGE_DB);
        }
    }

    public boolean checkUpdateApplicationReachable(boolean showErrorInPopup) {

        boolean result = true;
        String url = config.getUpdateApplicationUrl();

        try {
            TuttiUIUtil.tryToConnectToUpdateUrl(
                    url,
                    n("tutti.error.update.bad.url.syntax"),
                    n("tutti.error.update.could.not.reach.url"),
                    n("tutti.error.update.could.not.found.url")
            );
        } catch (ApplicationBusinessException e) {
            if (showErrorInPopup) {

                getErrorHelper().showWarningDialog(e.getMessage());
            } else {
                showInformationMessage(e.getMessage());
            }
            result = false;
        }
        return result;
    }

    public boolean checkUpdateDataReachable(boolean showErrorInPopup) {

        boolean result = true;
        String url = config.getUpdateDataUrl();

        try {
            TuttiUIUtil.tryToConnectToUpdateUrl(
                    url,
                    n("tutti.error.update.bad.url.syntax"),
                    n("tutti.error.update.could.not.reach.url"),
                    n("tutti.error.update.could.not.found.url")
            );
        } catch (ApplicationBusinessException e) {
            if (showErrorInPopup) {

                getErrorHelper().showWarningDialog(e.getMessage());
            } else {
                showInformationMessage(e.getMessage());
            }
            result = false;
        }
        return result;
    }

    public void closePersistenceService() {

        setDbLoaded(false);
        reloadPersistenceService();

    }

    public void openPersistenceService() {

        setDbLoaded(true);
        reloadPersistenceService();

    }

    public void deleteDbOnExit() {

        if (log.isInfoEnabled()) {
            log.info("Delete db files on exit.");
        }

        try {

            DeleteHelper.deleteDirectoryOnExit(config.getDbDirectory().toPath());
            DeleteHelper.deleteDirectoryOnExit(config.getCacheDirectory().toPath());
            DeleteHelper.deleteDirectoryOnExit(config.getDbAttachmentDirectory().toPath());

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not mark db files to be deleted on exit.", e);
        }

    }

    private Timer timer;

    public synchronized Timer getTimer() {
        if (timer == null) {
            timer = new Timer("ShowActions::");
        }
        return timer;
    }

    public synchronized Timer reloadTimer() {
        timer = null;
        return getTimer();
    }

}
