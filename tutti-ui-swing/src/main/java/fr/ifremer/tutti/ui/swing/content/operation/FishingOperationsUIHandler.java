package fr.ifremer.tutti.ui.swing.content.operation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.BenthosBatchUISupportImpl;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesBatchUISupportImpl;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.EditFishingOperationAction;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiTabContainerUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of UI {@link FishingOperationsUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class FishingOperationsUIHandler extends AbstractTuttiTabContainerUIHandler<FishingOperationsUIModel, FishingOperationsUI> implements CloseableUI {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(FishingOperationsUIHandler.class);

    protected EditFishingOperationAction editFishingOperationAction;

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(FishingOperationsUI ui) {

        super.beforeInit(ui);

        if (log.isDebugEnabled()) {
            log.debug("for " + this.ui);
        }
        FishingOperationsUIModel model = new FishingOperationsUIModel();

        EditCatchesUIModel catchesUIModel = new EditCatchesUIModel(
                getConfig().getSpeciesWeightUnit(),
                getConfig().getBenthosWeightUnit(),
                getConfig().getMarineLitterWeightUnit());

        SpeciesOrBenthosBatchUISupport speciesBatchUISupport = new SpeciesBatchUISupportImpl(
                getContext(),
                catchesUIModel,
                getConfig().getSpeciesWeightUnit());

        SpeciesOrBenthosBatchUISupport benthosBatchUISupport = new BenthosBatchUISupportImpl(
                getContext(),
                catchesUIModel,
                getConfig().getBenthosWeightUnit());


        // load existing cruise
        Cruise cruise = getDataContext().getCruise();

        PersistenceService persistenceService = getPersistenceService();

        TuttiProtocol protocol;
        if (getContext().isProtocolFilled()) {

            // load existing protocol

            protocol = persistenceService.getProtocol(
                    getContext().getProtocolId());

            if (log.isInfoEnabled()) {
                log.info("Loading existing protocol: " + protocol);
            }
        }

        List<FishingOperation> fishingOperations =
                Lists.newArrayList(persistenceService.getAllFishingOperation(cruise.getIdAsInt()));

        for (FishingOperation fishingOperation : fishingOperations) {
            fishingOperation.setCruise(cruise);
        }
        model.setFishingOperation(fishingOperations);

        if (log.isInfoEnabled()) {
            log.info("Loaded " + fishingOperations.size() +
                     " fishingOperation(s).");
        }

        this.ui.setContextValue(model);
        this.ui.setContextValue(catchesUIModel);
        this.ui.setContextValue(speciesBatchUISupport, SpeciesOrBenthosBatchUISupport.SPECIES);
        this.ui.setContextValue(benthosBatchUISupport, SpeciesOrBenthosBatchUISupport.BENTHOS);
    }

    @Override
    public void afterInit(FishingOperationsUI ui) {

        initUI(this.ui);

        editFishingOperationAction =
                getContext().getActionFactory().createLogicAction(this, EditFishingOperationAction.class);

        FishingOperationsUIModel model = getModel();

        List<FishingOperation> fishingOperations = model.getFishingOperation();

        initBeanFilterableComboBox(this.ui.getFishingOperationComboBox(),
                                   fishingOperations,
                                   model.getSelectedFishingOperation());

        model.addPropertyChangeListener(FishingOperationsUIModel.PROPERTY_SELECTED_FISHING_OPERATION, evt -> {
            if (log.isDebugEnabled()) {
                log.debug("propertyChange " + FishingOperationsUIModel.PROPERTY_SELECTED_FISHING_OPERATION);
            }

            // selected fishing operation is now the editing one
            FishingOperation newValue = (FishingOperation) evt.getNewValue();

            if (!getModel().isEditionAdjusting()) {

                FishingOperation operation;
                if (newValue == null) {
                    operation = null;
                } else {
                    operation = getPersistenceService().getFishingOperation(newValue.getIdAsInt());
                    Cruise cruise = getDataContext().getCruise();
                    operation.setCruise(cruise);
                }
                editFishingOperationAction.setFishingOperation(operation);
                if (SwingUtilities.isEventDispatchThread()) {

                    // launch a long action
                    getContext().getActionEngine().runAction(editFishingOperationAction);
                } else {

                    // run as an internal action (of embedded action)
                    getContext().getActionEngine().runInternalAction(editFishingOperationAction);
                }
            }

            // done here instead of in the action in order to update the headers
            // when the operation is saved
            String fishingOperationText = getFishingOperationTitle(newValue);

            EditFishingOperationUI efoUI = getUI().getFishingOperationTabContent();
            efoUI.getTraitGeneralTabPane().setTitle(fishingOperationText);
            efoUI.getVesselUseFeatureTabPane().setTitle(fishingOperationText);
            efoUI.getGearUseFeatureTabPane().setTitle(fishingOperationText);

            EditCatchesUI ecUI = getUI().getCatchesTabContent();
            ecUI.getCatchesCaracteristicsTabPane().setTitle(fishingOperationText);
            ecUI.getSpeciesTabPanel().getEditBatchesUIPanel().setTitle(fishingOperationText);
            ecUI.getBenthosTabPanel().getEditBatchesUIPanel().setTitle(fishingOperationText);
            ecUI.getMarineLitterTabFishingOperationReminderLabel().setTitle(fishingOperationText);
            ecUI.getAccidentalTabFishingOperationReminderLabel().setTitle(fishingOperationText);

        });

        model.addPropertyChangeListener(FishingOperationsUIModel.PROPERTY_FISHING_OPERATION, evt -> {
            if (log.isDebugEnabled()) {
                log.debug("propertyChange " + FishingOperationsUIModel.PROPERTY_FISHING_OPERATION);
            }
            FishingOperationsUIHandler.this.ui.getFishingOperationComboBox().setData(null);
            FishingOperationsUIHandler.this.ui.getFishingOperationComboBox().setData((List<FishingOperation>) evt.getNewValue());
        });

//        FishingOperation selectedOperation = null;
//        for (FishingOperation fishingOperation : fishingOperations) {
//            if (selectedOperation == null ||
//                fishingOperation.getGearShootingStartDate()
//                        .after(selectedOperation.getGearShootingStartDate())
//                && fishingOperation.getFishingOperationNumber()
//                   > selectedOperation.getFishingOperationNumber()
//                    ) {
//                selectedOperation = fishingOperation;
//            }
//        }

        getContext().addPropertyChangeListener(TuttiUIContext.PROPERTY_HIDE_BODY, evt -> {
            Boolean hideBody = (Boolean) evt.getNewValue();
            if (hideBody != null && hideBody) {
                if (getModel().getSelectedFishingOperation() == null) {
                    getUI().getFishingOperationComboBox().requestFocus();
                }
            }
        });
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void onCloseUI() {

        // ui will be saved so we do not want to keep selected tab indexes
        ui.getTabPane().setSelectedIndex(0);
        ui.getFishingOperationTabContent().getFishingOperationTabPane().setSelectedIndex(0);

        closeUI(ui.getFishingOperationTabContent());
        closeUI(ui.getCatchesTabContent());
    }

    @Override
    public boolean quitUI() {

        // reuse the editFishingOperationAction#prepareAction code
        FishingOperation editFishingOperation = getModel().getEditFishingOperation();
        editFishingOperationAction.setFishingOperation(editFishingOperation);
        editFishingOperationAction.setCheckPreviousEdit(true);

        try {
            return editFishingOperationAction.prepareAction();
        } finally {
            editFishingOperationAction.releaseAction();
        }
    }

    @Override
    public SwingValidator<FishingOperationsUIModel> getValidator() {
        return null;
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTabContainerUIHandler methods                         --//
    //------------------------------------------------------------------------//

    @Override
    public JTabbedPane getTabPanel() {
        return ui.getTabPane();
    }

    @Override
    public boolean removeTab(int i) {
        return false;
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public boolean isFishingOperationModified() {
        return getModel().getEditFishingOperation() != null &&
               getUI().getFishingOperationTabContent().getModel().isModify();
    }

    public boolean isFishingOperationValid() {
        return getModel().getEditFishingOperation() != null &&
               getUI().getFishingOperationTabContent().getModel().isValid();
    }

    public boolean isCatchBatchModified() {
        return getModel().getEditFishingOperation() != null &&
               getUI().getCatchesTabContent().getModel().isModify();
    }

    public boolean isCatchBatchValid() {
        return getModel().getEditFishingOperation() != null &&
               getUI().getCatchesTabContent().getModel().isValid();
    }

    public String getFishingOperationTitle(FishingOperation bean) {
        String fishingOperationText;

        if (bean == null) {
            fishingOperationText = null;
        } else if (TuttiEntities.isNew(bean)) {
            fishingOperationText = t("tutti.editFishingOperation.label.traitReminder",
                                     t("tutti.editFishingOperation.label.traitReminder.inCreation"));
        } else {
            fishingOperationText = t("tutti.editFishingOperation.label.traitReminder",
                                     decorate(bean));
        }
        return fishingOperationText;
    }
}
