package fr.ifremer.tutti.ui.swing.content.report.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.ui.swing.content.report.ReportUI;
import fr.ifremer.tutti.ui.swing.content.report.ReportUIHandler;
import fr.ifremer.tutti.ui.swing.content.report.ReportUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationIOUtil;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class SaveReportAction extends LongActionSupport<ReportUIModel, ReportUI, ReportUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SaveReportAction.class);

    protected File file;

    public SaveReportAction(ReportUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            FishingOperation fishingOperation = getModel().getFishingOperation();

            Preconditions.checkNotNull("Can't have a null fishing operation", fishingOperation);
            Preconditions.checkState(getModel().isReportDone(), "Result must be done");

            Decorator<FishingOperation> decorator = getDecorator(FishingOperation.class, null);

            String filename = "rapport-" + decorator.toString(fishingOperation) + ".pdf";
            filename = filename.replaceAll("/", "_").replaceAll("\\s", "");

            if (log.isInfoEnabled()) {
                log.info("Default save filename: " + filename);
            }

            file = saveFileWithStartDirectory(
                    getConfig().getReportBackupDirectory(),
                    false,
                    filename,
                    null,
                    t("tutti.report.title.choose.saveReportFile"),
                    t("tutti.report.action.save"),
                    "^.+\\.pdf$", t("tutti.common.file.pdf"));

            doAction = file != null;

        }

        return doAction;

    }

    @Override
    public void doAction() throws Exception {

        File outputFile = getModel().getOutputFile();
        ApplicationIOUtil.copyFile(outputFile, file, "Could not save file to " + file);

    }

    @Override
    public void postSuccessAction() {

        super.postSuccessAction();
        sendMessage(t("tutti.report.saved", file));

    }

    @Override
    protected void releaseAction() {

        super.releaseAction();
        file = null;

    }

}