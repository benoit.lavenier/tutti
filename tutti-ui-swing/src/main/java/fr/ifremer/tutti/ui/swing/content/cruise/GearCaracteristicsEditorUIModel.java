package fr.ifremer.tutti.ui.swing.content.cruise;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;

import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.1
 */
public class GearCaracteristicsEditorUIModel extends AbstractTuttiTableUIModel<Object, GearCaracteristicsEditorRowModel, GearCaracteristicsEditorUIModel> {

    public static final String PROPERTY_GEAR = "gear";

    public static final String PROPERTY_EDITABLE = "editable";

    public static final String PROPERTY_REMOVE_CARACTERISTIC_ENABLED = "removeCaracteristicEnabled";

    public static final String PROPERTY_AVAILABLE_CARACTERISTICS = "availableCaracteristics";

    private static final long serialVersionUID = 1L;

    protected List<Caracteristic> availableCaracteristics;

    protected Gear gear;

    protected boolean editable;

    protected CaracteristicMap caracteristicMap = new CaracteristicMap();

    /** Can user remove a selected caracteristic? */
    protected boolean removeCaracteristicEnabled;

    public GearCaracteristicsEditorUIModel() {
        super(Object.class, null, null);
    }

    public List<Caracteristic> getAvailableCaracteristics() {
        return availableCaracteristics;
    }

    public void setAvailableCaracteristics(List<Caracteristic> availableCaracteristics) {
        Object oldValue = getAvailableCaracteristics();
        this.availableCaracteristics = availableCaracteristics;
        firePropertyChange(PROPERTY_AVAILABLE_CARACTERISTICS, oldValue, availableCaracteristics);
    }

    public Gear getGear() {
        return gear;
    }

    public void setGear(Gear gear) {
        Object oldValue = getGear();
        this.gear = gear;
        firePropertyChange(PROPERTY_GEAR, oldValue, this.gear);
    }

    public boolean isRemoveCaracteristicEnabled() {
        return removeCaracteristicEnabled;
    }

    public void setRemoveCaracteristicEnabled(boolean removeCaracteristicEnabled) {
        Object oldValue = isRemoveCaracteristicEnabled();
        this.removeCaracteristicEnabled = removeCaracteristicEnabled;
        firePropertyChange(PROPERTY_REMOVE_CARACTERISTIC_ENABLED, oldValue, removeCaracteristicEnabled);
    }

    public CaracteristicMap getCaracteristicMap() {
        return caracteristicMap;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        Object oldValue = isEditable();
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    @Override
    protected Object newEntity() {
        return null;
    }
}
