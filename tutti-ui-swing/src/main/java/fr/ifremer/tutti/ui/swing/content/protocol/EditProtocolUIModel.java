package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristic;
import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristics;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.AvailableStratasTreeModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.ZonesTreeModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.SubStrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.ZoneNode;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class EditProtocolUIModel extends AbstractTuttiBeanUIModel<TuttiProtocol, EditProtocolUIModel> implements TuttiProtocol {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CARACTERISTIC_MAPPING_ROWS = "caracteristicMappingRows";

    public static final String PROPERTY_OPERATION_FIELD_MAPPING_ROWS = "operationFieldMappingRows";

    public static final String PROPERTY_SPECIES_ROW = "speciesRow";

    public static final String PROPERTY_BENTHOS_ROW = "benthosRow";

    public static final String PROPERTY_REMOVE_SPECIES_ENABLED = "removeSpeciesEnabled";

    public static final String PROPERTY_REMOVE_BENTHOS_ENABLED = "removeBenthosEnabled";

    public static final String PROPERTY_REMOVE_CARACTERISTIC_MAPPING_ENABLED = "removeCaracteristicMappingEnabled";

    public static final String PROPERTY_MOVE_UP_CARACTERISTIC_MAPPING_ENABLED = "moveUpCaracteristicMappingEnabled";

    public static final String PROPERTY_MOVE_DOWN_CARACTERISTIC_MAPPING_ENABLED = "moveDownCaracteristicMappingEnabled";

    public static final String PROPERTY_IMPORTED = "imported";

    public static final String PROPERTY_CLONED = "cloned";

    public static final String PROPERTY_CLEANED = "cleaned";

    public static final String PROPERTY_CPS_ROWS = "cpsRows";

    public static final String PROPERTY_MODIFYING_ZONES = "modifyingZones";

    public static final String PROPERTY_MATURITY_PMFM_ID = "maturityPmfmId";

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final TuttiProtocol editObject = TuttiProtocols.newTuttiProtocol();
    private final SampleCategoryModel sampleCategoryModel;

    /**
     * Flag when a incoming protocol is imported.
     *
     * @since 1.0
     */
    protected boolean imported;

    /**
     * Flag when a incoming protocol is cloned.
     *
     * @since 1.0
     */
    protected boolean cloned;

    /**
     * Flag when a incoming protocol is cleaned.
     *
     * @since 2.4
     */
    protected boolean cleaned;

    /**
     * Can user remove a selected species?
     *
     * @since 0.3
     */
    protected boolean removeSpeciesEnabled;

    /**
     * Can user remove a selected benthos?
     *
     * @since 0.3
     */
    protected boolean removeBenthosEnabled;

    protected boolean removeCaracteristicMappingEnabled;

    protected boolean moveUpCaracteristicMappingEnabled;

    protected boolean moveDownCaracteristicMappingEnabled;

    protected List<Species> allSpecies;

    protected List<Species> allSynonyms;

    protected Multimap<String, Species> allSpeciesByTaxonId;

    protected Map<String, Species> allReferentSpeciesByTaxonId;

    protected List<Caracteristic> caracteristics;

    protected Map<String, Caracteristic> allCaracteristic;

    /**
     * List of the maturity pmfm ids
     *
     * @since 4.5
     */
    protected List<String> maturityPmfmId;

    /**
     * Map of the maturity caracteristic by id. This is what is being edited.
     * We do not push the maturity caracteristics directly into the editobject, this is done only when the user saves the protocol
     *
     * @since 4.5
     */
    protected final LinkedHashMap<String, MaturityCaracteristic> maturityCaracteristicsById = new LinkedHashMap<>();

    protected LinkedHashMap<Caracteristic, EditProtocolCaracteristicsRowModel> caracteristicMappingRows =
            new LinkedHashMap<>();

    protected List<EditProtocolOperationFieldsRowModel> operationFieldMappingRows =
            new ArrayList<>();

    /**
     * Number of rows for each column
     *
     * @since 3.10
     */
    protected Map<String, MutableInt> numberOfRowsByColumn = new HashMap<>();

    protected List<EditProtocolSpeciesRowModel> speciesRow;

    protected List<EditProtocolSpeciesRowModel> benthosRow;

    protected List<CalcifiedPiecesSamplingEditorRowModel> cpsRows;

    protected final ZonesTreeModel zonesTreeModel = new ZonesTreeModel();

    protected final AvailableStratasTreeModel availableStratasTreeModel = new AvailableStratasTreeModel();

    protected static Binder<EditProtocolUIModel, TuttiProtocol> toBeanBinder =
            BinderFactory.newBinder(EditProtocolUIModel.class,
                                    TuttiProtocol.class);

    protected static Binder<TuttiProtocol, EditProtocolUIModel> fromBeanBinder =
            BinderFactory.newBinder(TuttiProtocol.class, EditProtocolUIModel.class);

    private boolean modifyingZones;

    /**
     * Le compteur de méthode de mensuration utilisées par les espèces et benthos.
     */
    private final CaracteristicsCount lengthStepPmfmUsed = new CaracteristicsCount();

    /**
     * Le compteur de méthode de maturité utilisées par les espèces et benthos.
     */
    private final CaracteristicsCount maturityPmfmUsed = new CaracteristicsCount();

    public EditProtocolUIModel(SampleCategoryModel sampleCategoryModel) {
        super(fromBeanBinder, toBeanBinder);
        this.sampleCategoryModel = sampleCategoryModel;

        // reset the list of maturity pmfm id when the list of maturity caracteristic changes
        addPropertyChangeListener(PROPERTY_MATURITY_CARACTERISTICS, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                List<MaturityCaracteristic> newValue = (List<MaturityCaracteristic>) evt.getNewValue();

                EditProtocolUIModel.this.setMaturityPmfmId(newValue.stream().map(MaturityCaracteristic::getId).collect(Collectors.toList()));

                LinkedHashMap<String, MaturityCaracteristic> newMap = new LinkedHashMap<>();
                newValue.forEach(maturityCaracteristic -> newMap.put(maturityCaracteristic.getId(), maturityCaracteristic));
                EditProtocolUIModel.this.setMaturityCaracteristicsById(newMap);
            }
        });

        // when the maturity psmf list changes, then remove the removed caracteristic or add the new caracteristic from the maturity caracteristics
        addPropertyChangeListener(PROPERTY_MATURITY_PMFM_ID, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                List<String> newValue = (List<String>) evt.getNewValue();
                LinkedHashMap<String, MaturityCaracteristic> newMap = new LinkedHashMap<>();
                newValue.forEach(maturityPmfmId -> newMap.put(maturityPmfmId,
                                                              maturityCaracteristicsById.getOrDefault(maturityPmfmId,
                                                                                                      MaturityCaracteristics.newMaturityCaracteristic(maturityPmfmId))));
                EditProtocolUIModel.this.setMaturityCaracteristicsById(newMap);
            }
        });
    }

    public CaracteristicsCount getLengthStepPmfmUsed() {
        return lengthStepPmfmUsed;
    }

    public CaracteristicsCount getMaturityPmfmUsed() {
        return maturityPmfmUsed;
    }

    public EditProtocolSpeciesRowModel newRow() {
        EditProtocolSpeciesRowModel result = new EditProtocolSpeciesRowModel();

        List<Integer> mandatoryIds = Lists.newArrayList(sampleCategoryModel.getSamplingOrder());
        // always remove the first category (V/HV)
        mandatoryIds.remove(0);
        result.setMandatorySampleCategoryId(mandatoryIds);

        result.setWeightEnabled(true);
        result.setValid(true);

        result.addPropertyChangeListener(EditProtocolSpeciesRowModel.PROPERTY_LENGTH_STEP_PMFM,
                                         evt -> {
                                             // update the used lengthstep caracteristic
                                             if (evt.getOldValue() != null) {
                                                 lengthStepPmfmUsed.removeCaracteristic((Caracteristic) evt.getOldValue());
                                             }
                                             if (evt.getNewValue() == null) {

                                                 EditProtocolSpeciesRowModel rowModel = (EditProtocolSpeciesRowModel) evt.getSource();
                                                 rowModel.setRtpMale(null);
                                                 rowModel.setRtpFemale(null);
                                                 rowModel.setRtpUndefined(null);

                                             } else {
                                                 lengthStepPmfmUsed.addCaracteristic((Caracteristic) evt.getNewValue());
                                             }

                                         });

        result.addPropertyChangeListener(EditProtocolSpeciesRowModel.PROPERTY_MATURITY_PMFM,
                                         evt -> {
                                             // update the used maturities
                                             if (evt.getOldValue() != null) {
                                                 maturityPmfmUsed.removeCaracteristic((Caracteristic) evt.getOldValue());
                                             }
                                             if (evt.getNewValue() != null) {
                                                 maturityPmfmUsed.addCaracteristic((Caracteristic) evt.getNewValue());
                                             }
                                         });

        return result;
    }

    @Override
    protected TuttiProtocol newEntity() {
        return TuttiProtocols.newTuttiProtocol();
    }

    protected List<Zone> incomingZones = new LinkedList<>();

    @Override
    public void fromEntity(TuttiProtocol entity) {
        fromBeanBinder.copyExcluding(entity, this, PROPERTY_ZONE);

        // on conserve les zones entrantes (elles seront ensuites transformées en modèle d'arbre)
        incomingZones.clear();
        if (!entity.isZoneEmpty()) {
            incomingZones.addAll(entity.getZone());
        }
    }

    public List<Zone> getIncomingZones() {
        return incomingZones;
    }

    @Override
    public TuttiProtocol toEntity() {
        TuttiProtocol result = TuttiProtocols.newTuttiProtocol();
        toBeanBinder.copyExcluding(this, result, PROPERTY_ZONE, PROPERTY_MATURITY_CARACTERISTICS);

        // tree to model
        Set<Zone> zones = zonesTreeModel.getZones();
        result.setZone(zones);

        Collection<EditProtocolCaracteristicsRowModel> protocolCaracteristicMappingRows = getCaracteristicMappingRows();
        List<CaracteristicMappingRow> caracteristicMappingRows = new ArrayList<>();
        for (EditProtocolCaracteristicsRowModel row : protocolCaracteristicMappingRows) {
            if (row.isValid()) {
                caracteristicMappingRows.add(row.toEntity());
            }
        }
        result.setCaracteristicMapping(caracteristicMappingRows);

        List<EditProtocolOperationFieldsRowModel> protocolOperationFieldMappingRows = getOperationFieldMappingRows();
        List<OperationFieldMappingRow> operationFieldMappingRows = new ArrayList<>();
        for (EditProtocolOperationFieldsRowModel row : protocolOperationFieldMappingRows) {
            if (StringUtils.isNotBlank(row.getField()) && StringUtils.isNotBlank(row.getImportColumn())
                    && row.isValid()) {
                operationFieldMappingRows.add(row.toEntity());
            }
        }
        result.setOperationFieldMapping(operationFieldMappingRows);

        // map the cps rows by species row
        Multimap<EditProtocolSpeciesRowModel, CalcifiedPiecesSamplingEditorRowModel> cpsRowsBySpecies =
                Multimaps.index(getCpsRows(), CalcifiedPiecesSamplingEditorRowModel::getProtocolSpecies);

        // get the species protocols from the table
        List<SpeciesProtocol> speciesProtocols = Lists.newArrayList();

        for (EditProtocolSpeciesRowModel row : getSpeciesRow()) {
            if (row.isValid()) {
                SpeciesProtocol protocol = row.toEntity();
                speciesProtocols.add(protocol);

                Collection<CalcifiedPiecesSamplingEditorRowModel> cpsRows = cpsRowsBySpecies.get(row);
                protocol.setCalcifiedPiecesSamplingDefinition(cpsRows.stream()
                                                                     .map(CalcifiedPiecesSamplingEditorRowModel::toEntity)
                                                                     .collect(Collectors.toList()));
            }
        }
        result.setSpecies(speciesProtocols);

        List<SpeciesProtocol> benthosProtocols = Lists.newArrayList();

        for (EditProtocolSpeciesRowModel row : getBenthosRow()) {
            if (row.isValid()) {
                SpeciesProtocol protocol = row.toEntity();
                benthosProtocols.add(protocol);

                Collection<CalcifiedPiecesSamplingEditorRowModel> cpsRows = cpsRowsBySpecies.get(row);
                protocol.setCalcifiedPiecesSamplingDefinition(cpsRows.stream()
                                                                     .map(CalcifiedPiecesSamplingEditorRowModel::toEntity)
                                                                     .collect(Collectors.toList()));
            }
        }
        result.setBenthos(benthosProtocols);

        result.setMaturityCaracteristics(new ArrayList<>(maturityCaracteristicsById.values()));

        return result;
    }

    public ZonesTreeModel getZonesTreeModel() {
        return zonesTreeModel;
    }

    public AvailableStratasTreeModel getAvailableStratasTreeModel() {
        return availableStratasTreeModel;
    }

    public void setLengthClassesPmfm(List<Caracteristic> lengthClassesPmfm) {
        List<String> ids = Lists.newArrayList(TuttiEntities.collecIds(lengthClassesPmfm));
        setLengthClassesPmfmId(ids);
    }

    public void setIndividualObservationPmfm(List<Caracteristic> individualObservationPmfm) {
        List<String> ids = TuttiEntities.collecIds(individualObservationPmfm);
        setIndividualObservationPmfmId(ids);
    }

    public void setMaturityPmfm(List<Caracteristic> maturityPmfm) {
        List<String> ids = Lists.newArrayList(TuttiEntities.collecIds(maturityPmfm));
        setMaturityPmfmId(ids);
    }

    public Collection<EditProtocolCaracteristicsRowModel> getCaracteristicMappingRows() {
        return caracteristicMappingRows.values();
    }

    public void addCaracteristicMappingRow(EditProtocolCaracteristicsRowModel newRow) {
        caracteristicMappingRows.put(newRow.getPsfm(), newRow);
        firePropertyChange(PROPERTY_CARACTERISTIC_MAPPING_ROWS, null, getCaracteristicMappingRows());
    }

    public void removeCaracteristicMappingRows(Collection<EditProtocolCaracteristicsRowModel> rowsToRemove) {
        for (EditProtocolCaracteristicsRowModel row : rowsToRemove) {
            caracteristicMappingRows.remove(row.getPsfm());
        }
        firePropertyChange(PROPERTY_CARACTERISTIC_MAPPING_ROWS, null, getCaracteristicMappingRows());
    }

    public void setCaracteristicMappingRows(List<EditProtocolCaracteristicsRowModel> caracteristicMappingRows) {
        this.caracteristicMappingRows = new LinkedHashMap<>(
                Maps.uniqueIndex(caracteristicMappingRows, EditProtocolCaracteristicsRowModel::getPsfm)
        );
        firePropertyChange(PROPERTY_CARACTERISTIC_MAPPING_ROWS, null, caracteristicMappingRows);
    }

    public boolean isCaracteristicUsedInMapping(Caracteristic caracteristic) {
        return caracteristicMappingRows.containsKey(caracteristic);
    }

    public Collection<Caracteristic> getUsedCaracteristics() {
        return caracteristicMappingRows.keySet();
    }

    public List<EditProtocolOperationFieldsRowModel> getOperationFieldMappingRows() {
        return operationFieldMappingRows;
    }

    public void setOperationFieldMappingRows(List<EditProtocolOperationFieldsRowModel> operationFieldMappingRows) {
        Object oldValue = getOperationFieldMappingRows();
        this.operationFieldMappingRows = operationFieldMappingRows;
        firePropertyChange(PROPERTY_OPERATION_FIELD_MAPPING_ROWS, oldValue, operationFieldMappingRows);
    }

    public List<EditProtocolSpeciesRowModel> getSpeciesRow() {
        return speciesRow;
    }

    public void setSpeciesRow(List<EditProtocolSpeciesRowModel> speciesRow) {
        Object oldValue = getSpeciesRow();
        this.speciesRow = speciesRow;
        firePropertyChange(PROPERTY_SPECIES_ROW, oldValue, speciesRow);
    }

    public List<EditProtocolSpeciesRowModel> getBenthosRow() {
        return benthosRow;
    }

    public void setBenthosRow(List<EditProtocolSpeciesRowModel> benthosRow) {
        Object oldValue = getBenthosRow();
        this.benthosRow = benthosRow;
        firePropertyChange(PROPERTY_BENTHOS_ROW, oldValue, benthosRow);
    }

    public Optional<EditProtocolSpeciesRowModel> getProtocolSpeciesRowForSpecies(Species species) {
        List<EditProtocolSpeciesRowModel> allRows = new ArrayList<>();
        if (speciesRow != null) {
            allRows.addAll(speciesRow);
        }
        if (benthosRow != null) {
            allRows.addAll(benthosRow);
        }
        return allRows.stream().filter(row -> row.getSpecies().equals(species)).findFirst();
    }

    public List<CalcifiedPiecesSamplingEditorRowModel> getCpsRows() {
        return cpsRows;
    }

    public void setCpsRows(List<CalcifiedPiecesSamplingEditorRowModel> cpsRows) {
        Object oldValue = getCpsRows();
        this.cpsRows = cpsRows;
        firePropertyChange(PROPERTY_CPS_ROWS, oldValue, cpsRows);
    }

    public boolean isRemoveSpeciesEnabled() {
        return removeSpeciesEnabled;
    }

    public void setRemoveSpeciesEnabled(boolean removeSpeciesEnabled) {
        this.removeSpeciesEnabled = removeSpeciesEnabled;
        firePropertyChange(PROPERTY_REMOVE_SPECIES_ENABLED, null, removeSpeciesEnabled);
    }

    public boolean isRemoveBenthosEnabled() {
        return removeBenthosEnabled;
    }

    public void setRemoveBenthosEnabled(boolean removeBenthosEnabled) {
        this.removeBenthosEnabled = removeBenthosEnabled;
        firePropertyChange(PROPERTY_REMOVE_BENTHOS_ENABLED, null, removeBenthosEnabled);
    }

    public boolean isRemoveCaracteristicMappingEnabled() {
        return removeCaracteristicMappingEnabled;
    }

    public void setRemoveCaracteristicMappingEnabled(boolean removeCaracteristicMappingEnabled) {
        this.removeCaracteristicMappingEnabled = removeCaracteristicMappingEnabled;
        firePropertyChange(PROPERTY_REMOVE_CARACTERISTIC_MAPPING_ENABLED, null, removeCaracteristicMappingEnabled);
    }

    public boolean isMoveUpCaracteristicMappingEnabled() {
        return moveUpCaracteristicMappingEnabled;
    }

    public void setMoveUpCaracteristicMappingEnabled(boolean moveUpCaracteristicMappingEnabled) {
        this.moveUpCaracteristicMappingEnabled = moveUpCaracteristicMappingEnabled;
        firePropertyChange(PROPERTY_MOVE_UP_CARACTERISTIC_MAPPING_ENABLED, null, moveUpCaracteristicMappingEnabled);
    }

    public boolean isMoveDownCaracteristicMappingEnabled() {
        return moveDownCaracteristicMappingEnabled;
    }

    public void setMoveDownCaracteristicMappingEnabled(boolean moveDownCaracteristicMappingEnabled) {
        this.moveDownCaracteristicMappingEnabled = moveDownCaracteristicMappingEnabled;
        firePropertyChange(PROPERTY_MOVE_DOWN_CARACTERISTIC_MAPPING_ENABLED, null, moveDownCaracteristicMappingEnabled);
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        Object oldValue = isImported();
        this.imported = imported;
        firePropertyChange(PROPERTY_IMPORTED, oldValue, imported);
    }

    public boolean isCloned() {
        return cloned;
    }

    public void setCloned(boolean cloned) {
        Object oldValue = isCloned();
        this.cloned = cloned;
        firePropertyChange(PROPERTY_CLONED, oldValue, cloned);
    }

    public boolean isCleaned() {
        return cleaned;
    }

    public void setCleaned(boolean cleaned) {
        Object oldValue = isCleaned();
        this.cleaned = cleaned;
        firePropertyChange(PROPERTY_CLEANED, oldValue, cleaned);
    }

    public List<Species> getAllSpecies() {
        return allSpecies;
    }

    public void setAllSpecies(List<Species> allSpecies) {
        this.allSpecies = allSpecies;
        if (allReferentSpeciesByTaxonId != null && allSpecies != null) {
            allSynonyms = Lists.newArrayList(allSpecies);
            allSynonyms.removeAll(allReferentSpeciesByTaxonId.values());
        }
    }

    public Multimap<String, Species> getAllSpeciesByTaxonId() {
        return allSpeciesByTaxonId;
    }

    public Collection<Species> getAllSynonyms(String taxonId) {
        return allSpeciesByTaxonId.get(taxonId);
    }

    public void setAllSpeciesByTaxonId(Multimap<String, Species> allSpeciesByTaxonId) {
        this.allSpeciesByTaxonId = allSpeciesByTaxonId;
    }

    public List<Species> getAllSynonyms() {
        return allSynonyms;
    }

    public Map<String, Species> getAllReferentSpeciesByTaxonId() {
        return allReferentSpeciesByTaxonId;
    }

    public void setAllReferentSpeciesByTaxonId(Map<String, Species> allReferentSpeciesByTaxonId) {
        this.allReferentSpeciesByTaxonId = allReferentSpeciesByTaxonId;
        if (allReferentSpeciesByTaxonId != null && allSpecies != null) {
            allSynonyms = Lists.newArrayList(allSpecies);
            // tchemit-2013-10-04 Do not do a removeAll (bad performance)
            for (Species species : allReferentSpeciesByTaxonId.values()) {
                allSynonyms.remove(species);
            }
        }
    }

    public List<Caracteristic> getCaracteristics() {
        return caracteristics;
    }

    public void setCaracteristics(List<Caracteristic> caracteristics) {
        this.caracteristics = caracteristics;
    }

    public Map<String, Caracteristic> getAllCaracteristic() {
        return allCaracteristic;
    }

    public void setAllCaracteristic(Map<String, Caracteristic> allCaracteristic) {
        this.allCaracteristic = allCaracteristic;
    }

    //------------------------------------------------------------------------//
    //-- TuttiProtocol methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    public String getName() {
        return editObject.getName();
    }

    @Override
    public void setName(String name) {
        Object oldValue = getName();
        editObject.setName(name);
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    @Override
    public String getComment() {
        return editObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public String getProgramId() {
        return editObject.getProgramId();
    }

    @Override
    public void setProgramId(String programId) {
        Object oldValue = getProgramId();
        editObject.setProgramId(programId);
        firePropertyChange(PROPERTY_PROGRAM_ID, oldValue, programId);
    }

    @Override
    public List<String> getLengthClassesPmfmId() {
        return editObject.getLengthClassesPmfmId();
    }

    @Override
    public void setLengthClassesPmfmId(List<String> lengthClassesPmfmId) {
        editObject.setLengthClassesPmfmId(lengthClassesPmfmId);
        // force to always propagates (need to recompte data of combobox in species table)
        firePropertyChange(PROPERTY_LENGTH_CLASSES_PMFM_ID, null, lengthClassesPmfmId);
    }

    @Override
    public List<String> getIndividualObservationPmfmId() {
        return editObject.getIndividualObservationPmfmId();
    }

    @Override
    public void setIndividualObservationPmfmId(List<String> individualObservationPmfmId) {
        editObject.setIndividualObservationPmfmId(individualObservationPmfmId);
        firePropertyChange(PROPERTY_INDIVIDUAL_OBSERVATION_PMFM_ID, null, individualObservationPmfmId);
    }

    public List<String> getMaturityPmfmId() {
        return maturityPmfmId;
    }

    public void setMaturityPmfmId(List<String> maturityPmfmId) {
        this.maturityPmfmId = maturityPmfmId;
        firePropertyChange(PROPERTY_MATURITY_PMFM_ID, null, maturityPmfmId);
    }

    @Override
    public List<CaracteristicMappingRow> getCaracteristicMapping() {
        return editObject.getCaracteristicMapping();
    }

    @Override
    public void setCaracteristicMapping(List<CaracteristicMappingRow> caracteristicMapping) {
        editObject.setCaracteristicMapping(caracteristicMapping);
        firePropertyChange(PROPERTY_CARACTERISTIC_MAPPING, null, caracteristicMapping);
    }

    @Override
    public CaracteristicMappingRow getCaracteristicMapping(int index) {
        return editObject.getCaracteristicMapping(index);
    }

    @Override
    public boolean isCaracteristicMappingEmpty() {
        return editObject.isCaracteristicMappingEmpty();
    }

    @Override
    public int sizeCaracteristicMapping() {
        return editObject.sizeCaracteristicMapping();
    }

    @Override
    public void addCaracteristicMapping(CaracteristicMappingRow caracteristicMapping) {
        editObject.addCaracteristicMapping(caracteristicMapping);
    }

    @Override
    public void addAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping) {
        editObject.addAllCaracteristicMapping(caracteristicMapping);
    }

    @Override
    public boolean removeCaracteristicMapping(CaracteristicMappingRow caracteristicMapping) {
        return editObject.removeCaracteristicMapping(caracteristicMapping);
    }

    @Override
    public boolean removeAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping) {
        return editObject.removeAllCaracteristicMapping(caracteristicMapping);
    }

    @Override
    public boolean containsCaracteristicMapping(CaracteristicMappingRow caracteristicMapping) {
        return editObject.containsCaracteristicMapping(caracteristicMapping);
    }

    @Override
    public boolean containsAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping) {
        return editObject.containsAllCaracteristicMapping(caracteristicMapping);
    }

    @Override
    public String getIndividualObservationPmfmId(int index) {
        return editObject.getIndividualObservationPmfmId(index);
    }

    @Override
    public boolean isIndividualObservationPmfmIdEmpty() {
        return editObject.isIndividualObservationPmfmIdEmpty();
    }

    @Override
    public int sizeIndividualObservationPmfmId() {
        return editObject.sizeIndividualObservationPmfmId();
    }

    @Override
    public void addIndividualObservationPmfmId(String individualObservationPmfmId) {
        editObject.addIndividualObservationPmfmId(individualObservationPmfmId);
    }

    @Override
    public void addAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        editObject.addAllIndividualObservationPmfmId(individualObservationPmfmId);
    }

    @Override
    public boolean removeIndividualObservationPmfmId(String individualObservationPmfmId) {
        return editObject.removeIndividualObservationPmfmId(individualObservationPmfmId);
    }

    @Override
    public boolean removeAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        return editObject.removeAllIndividualObservationPmfmId(individualObservationPmfmId);
    }

    @Override
    public boolean containsIndividualObservationPmfmId(String individualObservationPmfmId) {
        return editObject.containsIndividualObservationPmfmId(individualObservationPmfmId);
    }

    @Override
    public boolean containsAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        return editObject.containsAllIndividualObservationPmfmId(individualObservationPmfmId);
    }

    @Override
    public String getLengthClassesPmfmId(int index) {
        return editObject.getLengthClassesPmfmId(index);
    }

    @Override
    public boolean isLengthClassesPmfmIdEmpty() {
        return editObject.isLengthClassesPmfmIdEmpty();
    }

    @Override
    public int sizeLengthClassesPmfmId() {
        return editObject.sizeLengthClassesPmfmId();
    }

    @Override
    public void addLengthClassesPmfmId(String lengthClassesPmfmId) {
        editObject.addLengthClassesPmfmId(lengthClassesPmfmId);
        // force to always propagates (need to recompte data of combobox in species table)
        firePropertyChange(PROPERTY_LENGTH_CLASSES_PMFM_ID, null, getLengthClassesPmfmId());
    }

    @Override
    public void addAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        editObject.addAllLengthClassesPmfmId(lengthClassesPmfmId);
        // force to always propagates (need to recompte data of combobox in species table)
        firePropertyChange(PROPERTY_LENGTH_CLASSES_PMFM_ID, null, getLengthClassesPmfmId());
    }

    @Override
    public boolean removeLengthClassesPmfmId(String lengthClassesPmfmId) {
        boolean result = editObject.removeLengthClassesPmfmId(lengthClassesPmfmId);
        // force to always propagates (need to recompte data of combobox in species table)
        firePropertyChange(PROPERTY_LENGTH_CLASSES_PMFM_ID, null, getLengthClassesPmfmId());
        return result;
    }

    @Override
    public boolean removeAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        boolean result = editObject.removeAllLengthClassesPmfmId(lengthClassesPmfmId);
        // force to always propagates (need to recompte data of combobox in species table)
        firePropertyChange(PROPERTY_LENGTH_CLASSES_PMFM_ID, null, getLengthClassesPmfmId());
        return result;
    }

    @Override
    public boolean containsLengthClassesPmfmId(String lengthClassesPmfmId) {
        return editObject.containsLengthClassesPmfmId(lengthClassesPmfmId);
    }

    @Override
    public boolean containsAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        return editObject.containsAllLengthClassesPmfmId(lengthClassesPmfmId);
    }

    @Override
    public void addAllMaturityCaracteristics(Collection<MaturityCaracteristic> maturityCaracteristics) {
        editObject.addAllMaturityCaracteristics(maturityCaracteristics);
        firePropertyChanged(PROPERTY_MATURITY_CARACTERISTICS, null, getMaturityCaracteristics());
    }

    @Override
    public MaturityCaracteristic getMaturityCaracteristics(int index) {
        return editObject.getMaturityCaracteristics(index);
    }

    @Override
    public boolean isMaturityCaracteristicsEmpty() {
        return editObject.isMaturityCaracteristicsEmpty();
    }

    @Override
    public int sizeMaturityCaracteristics() {
        return editObject.sizeMaturityCaracteristics();
    }

    @Override
    public void addMaturityCaracteristics(MaturityCaracteristic maturityCaracteristics) {
        editObject.addMaturityCaracteristics(maturityCaracteristics);
        firePropertyChanged(PROPERTY_MATURITY_CARACTERISTICS, null, getMaturityCaracteristics());
    }

    @Override
    public boolean removeMaturityCaracteristics(MaturityCaracteristic maturityCaracteristics) {
        boolean result = editObject.removeMaturityCaracteristics(maturityCaracteristics);
        firePropertyChanged(PROPERTY_MATURITY_CARACTERISTICS, null, getMaturityCaracteristics());
        return result;
    }

    @Override
    public boolean removeAllMaturityCaracteristics(Collection<MaturityCaracteristic> maturityCaracteristics) {
        boolean result = editObject.removeAllMaturityCaracteristics(maturityCaracteristics);
        firePropertyChanged(PROPERTY_MATURITY_CARACTERISTICS, null, getMaturityCaracteristics());
        return result;
    }

    @Override
    public boolean containsMaturityCaracteristics(MaturityCaracteristic maturityCaracteristics) {
        return editObject.containsMaturityCaracteristics(maturityCaracteristics);
    }

    @Override
    public boolean containsAllMaturityCaracteristics(Collection<MaturityCaracteristic> maturityCaracteristics) {
        return editObject.containsAllMaturityCaracteristics(maturityCaracteristics);
    }

    @Override
    public List<MaturityCaracteristic> getMaturityCaracteristics() {
        return editObject.getMaturityCaracteristics();
    }

    @Override
    public void setMaturityCaracteristics(List<MaturityCaracteristic> maturityCaracteristics) {
        editObject.setMaturityCaracteristics(maturityCaracteristics);
        firePropertyChanged(PROPERTY_MATURITY_CARACTERISTICS, null, maturityCaracteristics);
    }

    @Override
    public SpeciesProtocol getSpecies(int index) {
        return editObject.getSpecies(index);
    }

    @Override
    public boolean isSpeciesEmpty() {
        return editObject.isSpeciesEmpty();
    }

    @Override
    public int sizeSpecies() {
        return editObject.sizeSpecies();
    }

    @Override
    public void addSpecies(SpeciesProtocol species) {
        editObject.addSpecies(species);
    }

    @Override
    public void addAllSpecies(Collection<SpeciesProtocol> species) {
        editObject.addAllSpecies(species);
    }

    @Override
    public boolean removeSpecies(SpeciesProtocol species) {
        return editObject.removeSpecies(species);
    }

    @Override
    public boolean removeAllSpecies(Collection<SpeciesProtocol> species) {
        return editObject.removeAllSpecies(species);
    }

    @Override
    public boolean containsSpecies(SpeciesProtocol species) {
        return editObject.containsSpecies(species);
    }

    @Override
    public boolean containsAllSpecies(Collection<SpeciesProtocol> species) {
        return editObject.containsAllSpecies(species);
    }

    @Override
    public List<SpeciesProtocol> getSpecies() {
        return editObject.getSpecies();
    }

    @Override
    public void setSpecies(List<SpeciesProtocol> species) {
        editObject.setSpecies(species);
    }

    @Override
    public SpeciesProtocol getBenthos(int index) {
        return editObject.getBenthos(index);
    }

    @Override
    public boolean isBenthosEmpty() {
        return editObject.isBenthosEmpty();
    }

    @Override
    public int sizeBenthos() {
        return editObject.sizeBenthos();
    }

    @Override
    public void addBenthos(SpeciesProtocol benthos) {
        editObject.addBenthos(benthos);
    }

    @Override
    public void addAllBenthos(Collection<SpeciesProtocol> benthos) {
        editObject.addAllBenthos(benthos);
    }

    @Override
    public boolean removeBenthos(SpeciesProtocol benthos) {
        return editObject.removeBenthos(benthos);
    }

    @Override
    public boolean removeAllBenthos(Collection<SpeciesProtocol> benthos) {
        return editObject.removeAllBenthos(benthos);
    }

    @Override
    public boolean containsBenthos(SpeciesProtocol benthos) {
        return editObject.containsBenthos(benthos);
    }

    @Override
    public boolean containsAllBenthos(Collection<SpeciesProtocol> benthos) {
        return editObject.containsAllBenthos(benthos);
    }

    @Override
    public List<SpeciesProtocol> getBenthos() {
        return editObject.getBenthos();
    }

    @Override
    public void setBenthos(List<SpeciesProtocol> benthos) {
        editObject.setBenthos(benthos);
    }

    @Override
    public Integer getVersion() {
        return editObject.getVersion();
    }

    @Override
    public void setVersion(Integer version) {
        editObject.setVersion(version);
    }

    @Override
    public String getImportColumns(int index) {
        return editObject.getImportColumns(index);
    }

    @Override
    public boolean isImportColumnsEmpty() {
        return editObject.isImportColumnsEmpty();
    }

    @Override
    public int sizeImportColumns() {
        return editObject.sizeImportColumns();
    }

    @Override
    public void addImportColumns(String importColumns) {
        editObject.addImportColumns(importColumns);
        firePropertyChanged(TuttiProtocol.PROPERTY_IMPORT_COLUMNS, null, getImportColumns());
    }

    @Override
    public void addAllImportColumns(Collection<String> importColumns) {
        editObject.addAllImportColumns(importColumns);
        firePropertyChanged(TuttiProtocol.PROPERTY_IMPORT_COLUMNS, null, getImportColumns());
    }

    @Override
    public boolean removeImportColumns(String importColumns) {
        boolean removeImportColumns = editObject.removeImportColumns(importColumns);
        firePropertyChanged(TuttiProtocol.PROPERTY_IMPORT_COLUMNS, null, getImportColumns());
        return removeImportColumns;
    }

    @Override
    public boolean removeAllImportColumns(Collection<String> importColumns) {
        boolean removeAllImportColumns = editObject.removeAllImportColumns(importColumns);
        firePropertyChanged(TuttiProtocol.PROPERTY_IMPORT_COLUMNS, null, getImportColumns());
        return removeAllImportColumns;
    }

    @Override
    public boolean containsImportColumns(String importColumns) {
        return editObject.containsImportColumns(importColumns);
    }

    @Override
    public boolean containsAllImportColumns(Collection<String> importColumns) {
        return editObject.containsAllImportColumns(importColumns);
    }

    @Override
    public Collection<String> getImportColumns() {
        return editObject.getImportColumns();
    }

    @Override
    public void setImportColumns(Collection<String> importColumns) {
        editObject.setImportColumns(importColumns);
        firePropertyChanged(TuttiProtocol.PROPERTY_IMPORT_COLUMNS, null, getImportColumns());
    }

    @Override
    public void setOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        editObject.setOperationFieldMapping(operationFieldMapping);
        firePropertyChanged(TuttiProtocol.PROPERTY_OPERATION_FIELD_MAPPING, null, getOperationFieldMapping());
    }

    public boolean isAvailableStratas() {
        return getAvailableStratasTreeModel().isAvailableStratas();
    }

    public void setAvailableStratas(boolean availableStratas) {
    }

    @Override
    public Collection<Zone> getZone() {
        // Not used
        return null;
    }

    @Override
    public void setZone(Collection<Zone> zones) {
        // Not used
    }

    @Override
    public Zone getZone(int index) {
        // Not used
        return null;
    }

    @Override
    public boolean isZoneEmpty() {

        // Not used
        return true;
    }

    @Override
    public int sizeZone() {
        // Not used
        return 0;
    }

    @Override
    public void addZone(Zone zone) {
        // Not used
    }

    @Override
    public void addAllZone(Collection<Zone> zones) {
        // Not used
    }

    @Override
    public boolean removeZone(Zone zone) {
        // Not used
        return false;
    }

    @Override
    public boolean removeAllZone(Collection<Zone> zones) {
        // Not used
        return false;
    }

    @Override
    public boolean containsZone(Zone zone) {
        // Not used
        return false;
    }

    @Override
    public boolean containsAllZone(Collection<Zone> zones) {
        // Not used
        return false;
    }

    @Override
    public Collection<OperationFieldMappingRow> getOperationFieldMapping() {
        return editObject.getOperationFieldMapping();
    }

    @Override
    public boolean containsAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        return editObject.containsAllOperationFieldMapping(operationFieldMapping);
    }

    @Override
    public boolean containsOperationFieldMapping(OperationFieldMappingRow operationFieldMapping) {
        return editObject.containsOperationFieldMapping(operationFieldMapping);
    }

    @Override
    public boolean removeAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        boolean removeAllOperationFieldMapping = editObject.removeAllOperationFieldMapping(operationFieldMapping);
        firePropertyChanged(TuttiProtocol.PROPERTY_OPERATION_FIELD_MAPPING, null, getOperationFieldMapping());
        return removeAllOperationFieldMapping;
    }

    @Override
    public boolean removeOperationFieldMapping(OperationFieldMappingRow operationFieldMapping) {
        boolean removeOperationFieldMapping = editObject.removeOperationFieldMapping(operationFieldMapping);
        firePropertyChanged(TuttiProtocol.PROPERTY_OPERATION_FIELD_MAPPING, null, getOperationFieldMapping());
        return removeOperationFieldMapping;
    }

    @Override
    public void addAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        editObject.addAllOperationFieldMapping(operationFieldMapping);
        firePropertyChanged(TuttiProtocol.PROPERTY_OPERATION_FIELD_MAPPING, null, getOperationFieldMapping());
    }

    @Override
    public void addOperationFieldMapping(OperationFieldMappingRow operationFieldMapping) {
        editObject.addOperationFieldMapping(operationFieldMapping);
        firePropertyChanged(TuttiProtocol.PROPERTY_OPERATION_FIELD_MAPPING, null, getOperationFieldMapping());
    }

    @Override
    public int sizeOperationFieldMapping() {
        return editObject.sizeOperationFieldMapping();
    }

    @Override
    public boolean isOperationFieldMappingEmpty() {
        return editObject.isOperationFieldMappingEmpty();
    }

    @Override
    public OperationFieldMappingRow getOperationFieldMapping(int index) {
        return editObject.getOperationFieldMapping(index);
    }

    @Override
    public boolean isUseCalcifiedPieceSampling() {
        return editObject.isUseCalcifiedPieceSampling();
    }

    @Override
    public void setUseCalcifiedPieceSampling(boolean useCalcifiedPieceSampling) {
        boolean oldValue = isUseCalcifiedPieceSampling();
        editObject.setUseCalcifiedPieceSampling(useCalcifiedPieceSampling);
        firePropertyChanged(TuttiProtocol.PROPERTY_USE_CALCIFIED_PIECE_SAMPLING, oldValue, useCalcifiedPieceSampling);
    }

    public int numberOfRows(String column) {
        int result = 0;
        MutableInt mutableInt = numberOfRowsByColumn.get(column);
        if (mutableInt != null) {
            result = mutableInt.intValue();
        }
        return result;
    }

    public void resetNumbersOfRows() {
        numberOfRowsByColumn = new HashMap<>();
    }

    public int incNumberOfRows(String column) {
        MutableInt mutableInt = numberOfRowsByColumn.get(column);
        if (mutableInt == null) {
            mutableInt = new MutableInt(1);
            numberOfRowsByColumn.put(column, mutableInt);
        } else {
            mutableInt.increment();
        }
        return mutableInt.intValue();

    }

    public int decNumberOfRows(String column) {
        MutableInt mutableInt = numberOfRowsByColumn.get(column);
        Preconditions.checkNotNull(mutableInt);
        Preconditions.checkArgument(mutableInt.intValue() > 0);
        mutableInt.decrement();
        return mutableInt.intValue();
    }


    public void selectStrataNodes(ZoneNode selectedZoneNode, StrataNode sourceStrataNode) {

        // suppression de l'arbre des disponibles
        availableStratasTreeModel.removeNodeFromParent(sourceStrataNode);

        String sourceStrataId = sourceStrataNode.getId();

        // récupération ou création du nœud de la strate dans la zone choisie
        StrataNode targetStrataNode = zonesTreeModel.getOrCreateStrataNode(selectedZoneNode, sourceStrataId);

        // déplacement des nœuds de toutes les sous-strates sur la strate cible
        zonesTreeModel.moveSubStratas(sourceStrataNode, targetStrataNode);

        setModify(true);

    }

    public void selectedSubStraNodes(ZoneNode selectedZoneNode, SubStrataNode sourceSubStrataNode) {

        StrataNode sourceStrataNode = sourceSubStrataNode.getParent();

        // suppression de l'arbre des disponibles
        availableStratasTreeModel.removeNodeFromParent(sourceSubStrataNode);

        if (sourceStrataNode.isLeaf()) {

            // la strate d'origine est vide, on peut la supprimer
            availableStratasTreeModel.removeNodeFromParent(sourceStrataNode);

        }

        // récupération ou création de la strate dans la zone choisie
        StrataNode targetStrataNode = zonesTreeModel.getOrCreateStrataNode(selectedZoneNode, sourceStrataNode.getId());

        // ajout de la sous-strate sur la strate cible
        zonesTreeModel.addNode(targetStrataNode, sourceSubStrataNode);

        setModify(true);

    }


    public void unselectStrataNode(StrataNode sourceStrataNode) {

        // Suppression de la strate de la zone
        zonesTreeModel.removeNodeFromParent(sourceStrataNode);

        // récupération ou création du nœud de la strate dans la zone choisie
        StrataNode targetStrataNode = availableStratasTreeModel.getOrCreateStrataNode(sourceStrataNode.getId());

        // déplacement des nœuds de toutes les sous-strates sur la strate cible
        availableStratasTreeModel.moveSubStratas(sourceStrataNode, targetStrataNode);

        setModify(true);

    }

    public void unselectSubStrataNode(SubStrataNode sourceSubStrataNode) {

        StrataNode sourceStrataNode = sourceSubStrataNode.getParent();

        // Suppression de la sous-strate
        zonesTreeModel.removeNodeFromParent(sourceSubStrataNode);

        if (sourceStrataNode.isLeaf()) {

            // plus de sous-strate, on peut supprimer la strate de la zone sélectionnée
            zonesTreeModel.removeNodeFromParent(sourceStrataNode);

        }

        // récupération ou création du nœud de la strate dans la zone choisie
        StrataNode targetStrataNode = availableStratasTreeModel.getOrCreateStrataNode(sourceStrataNode.getId());

        // ajout de la sous-strate
        availableStratasTreeModel.addNode(targetStrataNode, sourceSubStrataNode);

        setModify(true);

    }

    public boolean isModifyingZones() {
        return modifyingZones;
    }

    public void setModifyingZones(boolean modifyingZones) {
        Object oldValue = isModifyingZones();
        this.modifyingZones = modifyingZones;
        firePropertyChange(PROPERTY_MODIFYING_ZONES, oldValue, modifyingZones);
    }

    public void setLocationLabelCache(ImmutableMap<String, String> locationLabelCache) {
        this.zonesTreeModel.setLocationLabelCache(locationLabelCache);
        this.availableStratasTreeModel.setLocationLabelCache(locationLabelCache);
    }

    public Map<String, MaturityCaracteristic> getMaturityCaracteristicsById() {
        return maturityCaracteristicsById;
    }

    public void setMaturityCaracteristicsById(LinkedHashMap<String, MaturityCaracteristic> maturityCaracteristicsById) {
        this.maturityCaracteristicsById.clear();
        if (maturityCaracteristicsById != null) {
            this.maturityCaracteristicsById.putAll(maturityCaracteristicsById);
        }
    }

    public MaturityCaracteristic getMaturityCaracteristic(String id) {
        return maturityCaracteristicsById.get(id);
    }

    public boolean isMaturityValid(Caracteristic maturityCaracteristic) {
        Objects.requireNonNull(maturityCaracteristic);
        return isMaturityValid(maturityCaracteristic.getId());
    }

    public boolean isMaturityValid(String maturityCaracteristicId) {
        MaturityCaracteristic maturityCaracteristic = maturityCaracteristicsById.get(maturityCaracteristicId);
        return maturityCaracteristic != null && CollectionUtils.isNotEmpty(maturityCaracteristic.getMatureStateIds());
    }

    public boolean areAllMaturitiesValid() {
        return maturityPmfmId == null || maturityPmfmId.stream().allMatch(this::isMaturityValid);
    }
}
