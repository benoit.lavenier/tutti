package fr.ifremer.tutti.ui.swing.util.auth;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.JComponent;
import java.awt.Container;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/29/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class LoginUIHandler extends AbstractTuttiUIHandler<TuttiUIContext, LoginUI> {

    protected AuthenticationInfo authenticationInfo;

    @Override
    public void onCloseUI() {

    }

    @Override
    public SwingValidator<TuttiUIContext> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void beforeInit(LoginUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(TuttiUIUtil.getApplicationContext(ui));
    }

    @Override
    public void afterInit(LoginUI ui) {
        initUI(ui);
    }

    public LoginUI getUi() {
        return ui;
    }

    public void open(String url, AuthenticationInfo authenticationInfo) {

        ui.getInfoMessage().setText(t("tutti.login.infoMmessage", url));
        ui.getLoginField().setText(authenticationInfo == null ? null : authenticationInfo.getLogin());
        ui.getPasswordField().setText(authenticationInfo == null ? null : new String(authenticationInfo.getPassword()));
        ui.pack();

        Container parent = ui.getModel().getMainUI();
        if (parent != null) {
            SwingUtil.center(parent, ui);
        }
        ui.setVisible(true);
    }

    protected AuthenticationInfo getAuthenticationInfo() {
        return authenticationInfo;
    }

    public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
        this.authenticationInfo = authenticationInfo;
    }

}
