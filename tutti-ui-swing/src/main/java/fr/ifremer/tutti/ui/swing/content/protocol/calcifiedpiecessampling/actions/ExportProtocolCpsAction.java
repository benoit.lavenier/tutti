package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUIHandler;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * To export protocol cps.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class ExportProtocolCpsAction extends LongActionSupport<EditProtocolUIModel,
                                                               CalcifiedPiecesSamplingEditorUI,
                                                               CalcifiedPiecesSamplingEditorUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportProtocolCpsAction.class);

    private File file;

    public ExportProtocolCpsAction(CalcifiedPiecesSamplingEditorUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            // choose file to export
            file = saveFile(
                    getModel().getName() + "-cps",
                    "csv",
                    t("tutti.editProtocol.title.choose.cpsExportFile"),
                    t("tutti.editProtocol.action.exportProtocolCpsFile"),
                    "^.*\\.csv", t("tutti.common.file.csv")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will export protocol cps to file: " + file);
        }

        EditProtocolUIModel model = getModel();

        // build species protocol to export

        Multimap<SpeciesProtocol, CalcifiedPiecesSamplingDefinition> cps = ArrayListMultimap.create();
        for (CalcifiedPiecesSamplingEditorRowModel row : model.getCpsRows()) {
            cps.put(row.getProtocolSpecies().toEntity(), row.toEntity());
        }

        ProtocolImportExportService service = getContext().getTuttiProtocolImportExportService();

        service.exportCalcifiedPiecesSamplings(file, cps, getModel().getAllReferentSpeciesByTaxonId());

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.flash.info.cps.exported.from.protocol", file));
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }
}
