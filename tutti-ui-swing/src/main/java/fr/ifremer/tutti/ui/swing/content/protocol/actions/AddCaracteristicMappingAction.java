package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolCaracteristicsRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolCaracteristicsTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;

import static org.nuiton.i18n.I18n.t;

/**
 * To add a new species protocol.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class AddCaracteristicMappingAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    public AddCaracteristicMappingAction(EditProtocolUIHandler handler) {
        super(handler, false);
    }

    protected Caracteristic caracteristic;

    protected EditProtocolCaracteristicsRowModel newRow;

    @Override
    public void doAction() throws Exception {

        EditProtocolUI ui = getUI();

        BeanFilterableComboBox<Caracteristic> caracteristicMappingCombBox = ui.getCaracteristicMappingComboBox();

        caracteristic = (Caracteristic) caracteristicMappingCombBox.getSelectedItem();
        Preconditions.checkNotNull(caracteristic, "Can't add a speciesProtocol with a null species");

        // add new row to model (do it after combo stuff for ui best display)
        newRow = handler.createEditProtocolCaracteristicsRowModel();
        newRow.setPsfm(caracteristic);

        getModel().addCaracteristicMappingRow(newRow);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // update comboboxes
        getUI().getCaracteristicMappingComboBox().removeItem(caracteristic);

        // fire row was inserted in table model
        EditProtocolCaracteristicsTableModel tableModel = getHandler().getCaracteristicMappingTableModel();
        tableModel.addNewRow(newRow);

        // select this new row
        int rowIndex = tableModel.getRowIndex(newRow);
        SwingUtil.setSelectionInterval(handler.getCaracteristicsMappingTable(), rowIndex);

        // add notification
        String caracteristicStr = decorate(caracteristic);
        sendMessage(t("tutti.flash.info.caracteristic.add.to.protocol", caracteristicStr));
    }
}
