package fr.ifremer.tutti.ui.swing.content.protocol.rtp.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.rtp.RtpEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.rtp.RtpEditorUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.4
 */
public class CopyMaleRtpAction extends SimpleActionSupport<RtpEditorUI> {

    public CopyMaleRtpAction(RtpEditorUI rtpEditorUI) {
        super(rtpEditorUI);
    }

    @Override
    protected void onActionPerformed(RtpEditorUI rtpEditorUI) {
        RtpEditorUIModel model = rtpEditorUI.getModel();

        Double rtpMaleA = model.getRtpMaleA();
        model.setRtpFemaleA(rtpMaleA);
        model.setRtpUndefinedA(rtpMaleA);

        Float rtpMaleB = model.getRtpMaleB();
        model.setRtpFemaleB(rtpMaleB);
        model.setRtpUndefinedB(rtpMaleB);
    }
}
