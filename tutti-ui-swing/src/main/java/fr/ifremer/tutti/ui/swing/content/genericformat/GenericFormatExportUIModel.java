package fr.ifremer.tutti.ui.swing.content.genericformat;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportConfiguration;
import fr.ifremer.tutti.ui.swing.content.genericformat.tree.ProgramSelectTreeNode;
import org.jdesktop.beans.AbstractSerializableBean;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatExportUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_PROGRAM = "program";

    public static final String PROPERTY_EXPORT_ATTACHMENTS = "exportAttachments";

    public static final String PROPERTY_EXPORT_SPECIES = "exportSpecies";

    public static final String PROPERTY_EXPORT_BENTHOS = "exportBenthos";

    public static final String PROPERTY_EXPORT_MARINE_LITTER = "exportMarineLitter";

    public static final String PROPERTY_EXPORT_ACCIDENTAL_CATCH = "exportAccidentalCatch";

    public static final String PROPERTY_EXPORT_INDIVIDUAL_OBSERVATION = "exportIndividualObservation";

    public static final String PROPERTY_DATA_SELECTED= "dataSelected";

    public static final String PROPERTY_CAN_EXPORT = "canExport";

    private Program program;

    private boolean exportSpecies = true;

    private boolean exportBenthos = true;

    private boolean exportMarineLitter = true;

    private boolean exportAccidentalCatch = true;

    private boolean exportIndividualObservation = true;

    private boolean exportAttachments = true;

    private boolean canExport;

    private boolean dataSelected;

    private ProgramSelectTreeNode rootNode;

    public GenericFormatExportConfiguration toExportConfiguration() {

        GenericFormatExportConfiguration configuration = new GenericFormatExportConfiguration();

        configuration.setExportSpecies(exportSpecies);
        configuration.setExportBenthos(exportBenthos);
        configuration.setExportMarineLitter(exportMarineLitter);
        configuration.setExportAccidentalCatch(exportAccidentalCatch);
        configuration.setExportIndividualObservation(exportIndividualObservation);
        configuration.setExportAttachments(exportAttachments);

        ProgramDataModel selectedDataModel = rootNode.getSelectedDataModel();
        configuration.setDataToExport(selectedDataModel);

        return configuration;

    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        Object oldValue = getProgram();
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, oldValue, program);
    }

    public boolean isExportSpecies() {
        return exportSpecies;
    }

    public void setExportSpecies(boolean exportSpecies) {
        this.exportSpecies = exportSpecies;
        firePropertyChange(PROPERTY_EXPORT_SPECIES, null, exportSpecies);
    }

    public boolean isExportBenthos() {
        return exportBenthos;
    }

    public void setExportBenthos(boolean exportBenthos) {
        this.exportBenthos = exportBenthos;
        firePropertyChange(PROPERTY_EXPORT_BENTHOS, null, exportBenthos);
    }

    public boolean isExportMarineLitter() {
        return exportMarineLitter;
    }

    public void setExportMarineLitter(boolean exportMarineLitter) {
        this.exportMarineLitter = exportMarineLitter;
        firePropertyChange(PROPERTY_EXPORT_MARINE_LITTER, null, exportMarineLitter);
    }

    public boolean isExportAccidentalCatch() {
        return exportAccidentalCatch;
    }

    public void setExportAccidentalCatch(boolean exportAccidentalCatch) {
        this.exportAccidentalCatch = exportAccidentalCatch;
        firePropertyChange(PROPERTY_EXPORT_ACCIDENTAL_CATCH, null, exportAccidentalCatch);
    }

    public boolean isExportIndividualObservation() {
        return exportIndividualObservation;
    }

    public void setExportIndividualObservation(boolean exportIndividualObservation) {
        this.exportIndividualObservation = exportIndividualObservation;
        firePropertyChange(PROPERTY_EXPORT_INDIVIDUAL_OBSERVATION, null, exportIndividualObservation);
    }

    public boolean isExportAttachments() {
        return exportAttachments;
    }

    public void setExportAttachments(boolean exportAttachments) {
        this.exportAttachments = exportAttachments;
        firePropertyChange(PROPERTY_EXPORT_ATTACHMENTS, null, exportAttachments);
    }

    public boolean isCanExport() {
        return canExport;
    }

    public void setCanExport(boolean canExport) {
        this.canExport = canExport;
        firePropertyChange(PROPERTY_CAN_EXPORT, null, canExport);
    }

    public boolean isDataSelected() {
        return dataSelected;
    }

    public void setDataSelected(boolean dataSelected) {
        this.dataSelected = dataSelected;
        firePropertyChange(PROPERTY_DATA_SELECTED, null, dataSelected);
    }

    public boolean computeIsCanExport() {
        //TODO select data
        return getProgram() != null && isDataSelected();
    }

    public void setRootNode(ProgramSelectTreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public ProgramSelectTreeNode getRootNode() {
        return rootNode;
    }
}
