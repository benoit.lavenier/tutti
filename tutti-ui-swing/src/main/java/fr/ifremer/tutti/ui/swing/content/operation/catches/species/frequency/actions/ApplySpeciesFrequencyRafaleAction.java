package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyLogRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyLogsTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import fr.ifremer.tutti.ui.swing.util.SoundEngine;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Date;
import javax.swing.SwingUtilities;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class ApplySpeciesFrequencyRafaleAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ApplySpeciesFrequencyRafaleAction.class);

    protected final SpeciesFrequencyUI ui;
    protected final SoundEngine soundEngine;

    public ApplySpeciesFrequencyRafaleAction(SpeciesFrequencyUI ui) {
        this.ui = ui;
        this.soundEngine = ui.getHandler().getContext().getSoundEngine();
    }

    public void applyRafaleStep(Float step, boolean fromIchtyometer) {

        if (log.isDebugEnabled()) {
            log.debug("Will apply rafale step: " + step);
        }

        SpeciesFrequencyUIModel model = ui.getModel();
        SpeciesFrequencyUIHandler handler = ui.getHandler();

        float aroundLengthStep = model.getLengthStep(step);

        if (model.isCopyIndividualObservationNothing()) {

            SpeciesFrequencyTableModel tableModel = handler.getTableModel();

            SpeciesFrequencyRowModel row = tableModel.addRafaleRow(aroundLengthStep);
            int rowIndex = tableModel.getRowIndex(row);

            model.recomputeTotalNumber();

            SwingUtilities.invokeLater(() -> selectRow(ui.getTable(), rowIndex));

        }

        JXTable logsTable = ui.getLogsTable();
        SpeciesFrequencyLogsTableModel logsTableModel = (SpeciesFrequencyLogsTableModel) logsTable.getModel();
        SpeciesFrequencyLogRowModel newLogRow = logsTableModel.createNewRow();
        newLogRow.setDate(new Date());
        newLogRow.setLengthStep(step);
        logsTableModel.addNewRow(0, newLogRow);

        if (model.isAddIndividualObservationOnRafale()) {

            IndividualObservationBatchTableModel obsTableModel = (IndividualObservationBatchTableModel) ui.getObsTable().getModel();
            // need for https://forge.codelutin.com/issues/8329
            int oldHeight = ui.getObsTable().getHeight();
            if (log.isDebugEnabled()) {
                log.debug(String.format(
                        "DEBUG before **** rowCount: %s oldHeight: %s",
                        obsTableModel.getRowCount(), oldHeight));
            }


            IndividualObservationBatchRowModel obsRow = obsTableModel.addRafaleRow(step);

            newLogRow.setObsRow(obsRow);

            int rowIndex = obsTableModel.getRowIndex(obsRow);

            SwingUtilities.invokeLater(() -> {
                int newHeight = ui.getObsTable().getHeight();

                if (log.isDebugEnabled()) {
                    log.debug(String.format(
                            "DEBUG invokeLater **** rowCount: %s newHeight: %s",
                            obsTableModel.getRowCount(), newHeight));
                }

                // try to detect too early call (table not yet graphicaly modified)
                // see https://forge.codelutin.com/issues/8329
                if (oldHeight != newHeight) {
                    model.getIndividualObservationModel().getSamplingNotificationZoneModel().setValueAdjusting(true);

                    try {

                        selectRow(ui.getObsTable(), rowIndex);

                    } finally {

                        model.getIndividualObservationModel().getSamplingNotificationZoneModel().setValueAdjusting(false);

                    }
                } else {
                    log.error("Scroll to added line is not possible, try to use fallback method");
                    // fallback to early call, wait JComponent size change
                    selectRowWhenTableResized(ui.getObsTable(), rowIndex);
                }
            });
        }

        if (fromIchtyometer) {

            String unit = model.getLengthStepCaracteristicUnit();
            handler.showInformationMessage(t("tutti.editSpeciesFrequencies.addMeasure", step, aroundLengthStep, unit));

            soundEngine.beepOnExternalDeviceDataReception(unit, aroundLengthStep);

        }

    }

    protected void selectRow(JXTable table, int rowIndex) {

        table.setRowSelectionInterval(rowIndex, rowIndex);
        table.scrollRowToVisible(rowIndex);

    }

    protected void selectRowWhenTableResized(JXTable table, int rowIndex) {
        table.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                log.error(String.format("DEBUG ### remove listener: ", this.getClass().getName()));
                table.removeComponentListener(this);
                ui.getModel().getIndividualObservationModel().getSamplingNotificationZoneModel().setValueAdjusting(true);

                try {
                    table.setRowSelectionInterval(rowIndex, rowIndex);
                    table.scrollRowToVisible(rowIndex);
//                table.scrollRectToVisible(table.getCellRect(rowIndex, 0, true));
                    table.removeComponentListener(this);

                    int newHeight = ui.getObsTable().getHeight();
                    log.error(String.format(
                            "DEBUG in selectRowWhenTableResized resized ****"
                                    + " rowCount: %s"
                                    + " newHeight: %s",
                        table.getRowCount(),
                        newHeight)
                    );

                } finally {
                    ui.getModel().getIndividualObservationModel().getSamplingNotificationZoneModel().setValueAdjusting(false);
                }

            }
        });
    }

}
