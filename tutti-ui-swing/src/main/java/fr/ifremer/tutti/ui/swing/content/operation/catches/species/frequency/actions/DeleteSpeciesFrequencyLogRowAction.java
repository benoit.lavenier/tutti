package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyLogCellComponent;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyLogRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyLogsTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import jaxx.runtime.SwingUtil;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class DeleteSpeciesFrequencyLogRowAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    final SpeciesFrequencyUI ui;

    private final SpeciesFrequencyLogCellComponent.FrequencyLogCellEditor component;

    public DeleteSpeciesFrequencyLogRowAction(SpeciesFrequencyUI ui, SpeciesFrequencyLogCellComponent.FrequencyLogCellEditor component) {

        this.ui = ui;
        this.component = component;
        putValue(SMALL_ICON, SwingUtil.createActionIcon("delete"));

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        SpeciesFrequencyLogRowModel row = component.getRow();

        if (row != null) {

            SpeciesFrequencyUIHandler handler = ui.getHandler();

            int i = JOptionPane.showConfirmDialog(
                    handler.getTopestUI(),
                    t("tutti.editSpeciesFrequencies.logTable.removeRow.confirm.message", row.getLabel()),
                    t("tutti.editSpeciesFrequencies.logTable.removeRow.confirm.title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);

            if (i == JOptionPane.YES_OPTION) {


                Float lengthStep = row.getLengthStep();

                float roundLengthStep = ui.getModel().getLengthStep(lengthStep);

                SpeciesFrequencyTableModel tableModel = handler.getTableModel();
                tableModel.decrementNumberForLengthStep(roundLengthStep);

                SpeciesFrequencyLogsTableModel logsTableModel = (SpeciesFrequencyLogsTableModel) ui.getLogsTable().getModel();
                int index = logsTableModel.getRowIndex(row);
                logsTableModel.removeRow(index);

                if (row.getObsRow() != null) {
                    IndividualObservationBatchTableModel obsTableModel =
                            (IndividualObservationBatchTableModel) ui.getObsTable().getModel();
                    obsTableModel.removeRow(obsTableModel.getRowIndex(row.getObsRow()));
                }

            }

        }

    }

}