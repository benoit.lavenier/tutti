package fr.ifremer.tutti.ui.swing.content.genericformat.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created on 3/31/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class DataSelectTreeModel extends DefaultTreeModel {

    private static final long serialVersionUID = 1L;

    public static void installDataSelectionHandler(JTree tree) {
        DataSelectTreeNodeListener handler = new DataSelectTreeNodeListener();
        tree.addKeyListener(handler);
        tree.addMouseListener(handler);
    }

    public DataSelectTreeModel() {
        super(null);
    }

    @Override
    public ProgramSelectTreeNode getRoot() {
        return (ProgramSelectTreeNode) super.getRoot();
    }

    public boolean isDataSelected() {

        boolean dataSelected = false;

        if (getRoot() != null) {
            for (CruiseSelectTreeNode cruiseNode : getRoot()) {
                if (cruiseNode.isSelected() || cruiseNode.isPartialSelected()) {
                    dataSelected = true;
                    break;
                }
            }
        }
        return dataSelected;

    }

    public void setSelected(DataSelectTreeNodeSupport node, boolean selected) {

        node.setSelected(selected);

        nodeChanged(node);

        if (node instanceof CruiseSelectTreeNode) {
            nodeStructureChanged(node);
        } else if (node instanceof OperationSelectTreeNode) {
            nodeChanged(node.getParent());
        }

    }

    public void select(DataSelectTreeNodeSupport node) {

        boolean selected = !node.isSelected();
        setSelected(node, selected);

    }

    public void selectAll() {
        for (CruiseSelectTreeNode cruiseNode : getRoot()) {
            setSelected(cruiseNode, true);
        }
    }

    public void unselectAll() {
        for (CruiseSelectTreeNode cruiseNode : getRoot()) {
            setSelected(cruiseNode, false);
        }
    }

    /**
     * Created on 3/31/15.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 3.14.3
     */
    static class DataSelectTreeNodeListener implements KeyListener, MouseListener {

        @Override
        public void keyReleased(KeyEvent e) {

            JTree tree = (JTree) e.getSource();
            if (!tree.isSelectionEmpty()) {

                boolean doEdit = e.getModifiers() == 0
                                 && (e.getKeyCode() == KeyEvent.VK_ENTER ||
                                     e.getKeyCode() == KeyEvent.VK_SPACE);

                if (doEdit) {

                    TreePath selectionPath = tree.getSelectionPath();
                    doSelectNode(tree, selectionPath);

                }

            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {

            JTree tree = (JTree) e.getSource();
            if (!tree.isSelectionEmpty()) {

                TreePath selectionPath = tree.getSelectionPath();
                Rectangle bounds = tree.getPathBounds(selectionPath);

                int x = e.getX();
                boolean doEdit = (bounds != null && x >= (bounds.x) && (x - 12) <= (bounds.x));

                if (doEdit) {

                    doSelectNode(tree, selectionPath);

                }

            }
        }

        protected void doSelectNode(JTree tree, TreePath selectionPath) {
            DataSelectTreeNodeSupport lastPathComponent = (DataSelectTreeNodeSupport) selectionPath.getLastPathComponent();
            ((DataSelectTreeModel) tree.getModel()).select(lastPathComponent);
        }

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {

        }

        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
