package fr.ifremer.tutti.ui.swing.content.operation.catches.species;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

/**
 * Created on 18/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class EditSpeciesBatchPanelUIModel extends AbstractTuttiBeanUIModel<CatchBatch, EditSpeciesBatchPanelUIModel> {

    private final SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    public EditSpeciesBatchPanelUIModel(SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport) {
        super(null, null);
        this.speciesOrBenthosBatchUISupport = speciesOrBenthosBatchUISupport;
    }

    public SpeciesOrBenthosBatchUISupport getSpeciesOrBenthosBatchUISupport() {
        return speciesOrBenthosBatchUISupport;
    }

    @Override
    protected CatchBatch newEntity() {
        return null;
    }
}
