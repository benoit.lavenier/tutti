package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.split.SplitSpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class OpenSplitSpeciesBatchUIAction extends SimpleActionSupport<SpeciesBatchUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(OpenSplitSpeciesBatchUIAction.class);

    private static final long serialVersionUID = -6540241422935319461L;

    public OpenSplitSpeciesBatchUIAction(SpeciesBatchUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(SpeciesBatchUI ui) {

        JXTable table = ui.getTable();

        // get selected row
        int rowIndex = SwingUtil.getSelectedModelRow(table);

        Preconditions.checkState(rowIndex != -1, "Cant split batch if no batch selected");

        SpeciesBatchUIHandler handler = ui.getHandler();
        SpeciesBatchTableModel tableModel = handler.getTableModel();

        SpeciesBatchRowModel parentBatch = tableModel.getEntry(rowIndex);

        boolean split = true;
        if (parentBatch.getWeight() != null) {
            String htmlMessage = String.format(
                    SpeciesBatchUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.editSpeciesBatch.split.weightNotNull.message"),
                    t("tutti.editSpeciesBatch.split.weightNotNull.help"));
            int i = JOptionPane.showConfirmDialog(
                    handler.getTopestUI(),
                    htmlMessage,
                    t("tutti.editSpeciesBatch.split.weightNotNull.title"),
                    JOptionPane.OK_CANCEL_OPTION);

            if (i == JOptionPane.OK_OPTION) {
                parentBatch.setWeight(null);

            } else {
                split = false;
            }
        }

        if (split) {
            if (log.isDebugEnabled()) {
                log.debug("Open split batch ui for row [" + rowIndex + ']');
            }

            EditSpeciesBatchPanelUI parentUI = ui.getParentContainer(EditSpeciesBatchPanelUI.class);
            SplitSpeciesBatchUI splitBatchEditor = parentUI.getSplitBatchUI();

            splitBatchEditor.getHandler().editBatch(parentBatch);

            // open split editor
            parentUI.switchToSplitBatch();

            // update title
            String title = handler.buildReminderLabelTitle(parentBatch.getSpecies(),
                                                           parentBatch,
                                                           parentUI.getEditBatchesUIPanel().getTitle(),
                                                           t("tutti.splitSpeciesBatch.title"));
            parentUI.getSplitBatchUIPanel().setTitle(title);
        }

    }
}
