package fr.ifremer.tutti.ui.swing.content.operation.catches;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

import java.util.EventObject;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.4.2
 */
public class BatchSavedEvent extends EventObject {

    protected final AbstractTuttiBeanUIModel row;

    public BatchSavedEvent(AbstractTuttiBatchUIModel model, AbstractTuttiBeanUIModel row) {
        super(model);
        this.row = row;
    }

    @Override
    public AbstractTuttiBatchUIModel getSource() {
        return (AbstractTuttiBatchUIModel) super.getSource();
    }

    public AbstractTuttiBeanUIModel getRow() {
        return row;
    }
}
