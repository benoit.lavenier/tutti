package fr.ifremer.tutti.ui.swing.update.module;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.TuttiConfigurationOption;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class TuttiModuleUpdater extends ModuleUpdaterSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiModuleUpdater.class);

    public TuttiModuleUpdater() {
        super(UpdateModule.tutti);
    }

    @Override
    protected void onUpdateToDo(TuttiUIContext context, ApplicationInfo info) {

        if (info != null) {
            if (log.isInfoEnabled()) {
                log.info("Find a updatable module : " + updateModule);
            }
        }
    }

    @Override
    public void onUpdateDone(TuttiUIContext context, ApplicationInfo info) {

        if (log.isInfoEnabled()) {
            log.info(String.format(
                    "A tutti update was downloaded (oldVersion: %s, newVersion: %s), will restart application to use it",
                    info.oldVersion, info.newVersion));
        }

        TuttiConfiguration config = context.getConfig();

        // must remove db cache directory
        File cacheDirectory = config.getCacheDirectory();
        ApplicationIOUtil.forceDeleteOnExit(
                cacheDirectory,
                t("tutti.applicationUpdater.updateDone.deleteDirectory.caches.error", cacheDirectory)
        );

        Version oldVersion = Versions.valueOf(info.oldVersion);

        if (config.isFullLaunchMode() && oldVersion.before(Versions.valueOf("3.7.1"))) {

            // clean application data source (only if coming from before a 3.7.1 version)
            if (log.isInfoEnabled()) {
                log.info("Remove from configuration tutti.update.application.url: " + config.getUpdateApplicationUrl());
            }
            config.getApplicationConfig().setOption(TuttiConfigurationOption.UPDATE_APPLICATION_URL.getKey(), "");
            config.save();

        }

    }

    @Override
    public String getLabel() {
        return t("tutti.update.tutti");
    }

}
