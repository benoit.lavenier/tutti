package fr.ifremer.tutti.ui.swing.content.protocol.zones.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class AvailableStratasTreeModel extends ZoneEditorTreeModelSupport {

    public AvailableStratasTreeModel() {
        super(t("tutti.zoneEditor.availableStratas.root.label"));
    }

    public StrataNode getOrCreateStrataNode(String sourceStrataId) {
        return getOrCreateStrataNode(getRoot(), sourceStrataId);
    }

    public boolean isAvailableStratas() {
        return !getRoot().isLeaf();
    }

}
