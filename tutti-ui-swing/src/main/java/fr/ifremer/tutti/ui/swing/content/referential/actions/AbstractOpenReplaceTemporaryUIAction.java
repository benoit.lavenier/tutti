package fr.ifremer.tutti.ui.swing.content.referential.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.MainUI;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUI;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIHandler;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIModel;
import fr.ifremer.tutti.ui.swing.content.referential.replace.AbstractReplaceTemporaryUI;
import fr.ifremer.tutti.ui.swing.content.referential.replace.AbstractReplaceTemporaryUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.context.JAXXInitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JButton;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public abstract class AbstractOpenReplaceTemporaryUIAction<E extends TuttiReferentialEntity, M extends AbstractReplaceTemporaryUIModel<E>, UI extends AbstractReplaceTemporaryUI<E, M>> extends LongActionSupport<ManageTemporaryReferentialUIModel, ManageTemporaryReferentialUI, ManageTemporaryReferentialUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractOpenReplaceTemporaryUIAction.class);

    protected abstract String getEntityLabel();

    protected abstract M createNewModel();

    protected abstract UI createUI(JAXXInitialContext ctx);

    protected abstract List<E> getTargetList(PersistenceService persistenceService);

    protected abstract List<E> retainTemporaryList(PersistenceService persistenceService, List<E> targetList);

    protected E source;

    protected E target;

    protected List<E> targetList;

    protected List<E> sourceList;

    protected AbstractOpenReplaceTemporaryUIAction(ManageTemporaryReferentialUIHandler handler) {
        super(handler, true);
    }

    protected abstract JButton getButton();

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction() && getButton().isEnabled();

        if (doAction) {

            setProgressionModel(new ProgressionModel());
            getProgressionModel().setTotal(3);

        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        source = target = null;
        targetList = sourceList = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {

        PersistenceService persistenceService = getContext().getPersistenceService();

        String entityLabel = getEntityLabel();

        // Get target list
        getProgressionModel().increments(t("tutti.openReplaceTemporaryUI.loading.target", entityLabel));
        targetList = getTargetList(persistenceService);
        log.info("Loaded official referentials: " + targetList.size());

        // Get source list
        getProgressionModel().increments(t("tutti.openReplaceTemporaryUI.loading.source", entityLabel));
        sourceList = retainTemporaryList(persistenceService, targetList);
        log.info("Loaded temporary referentials: " + sourceList.size());

    }

    @Override
    public void postSuccessAction() {

        boolean showDialog = true;

        String entityLabel = getEntityLabel();

        M model = createNewModel();

        if (targetList.isEmpty()) {

            showDialog = false;
            displayWarningMessage(t("tutti.title.openReplaceTemporaryUI.noTarget", entityLabel),
                                  t("tutti.message.openReplaceTemporaryUI.noTarget", entityLabel));

        } else {

            model.setTargetList(targetList);

        }

        if (sourceList.isEmpty()) {

            showDialog = false;

            displayWarningMessage(t("tutti.title.openReplaceTemporaryUI.noSource", entityLabel),
                                  t("tutti.message.openReplaceTemporaryUI.noSource", entityLabel));

        } else {

            model.setSourceList(sourceList);

        }

        if (showDialog) {

            getProgressionModel().increments(t("tutti.openReplaceTemporaryUI.open.dialog"));

            JAXXInitialContext ctx = new JAXXInitialContext();
            ctx.add(getUI());
            ctx.add(model);

            UI dialog = createUI(ctx);
            MainUI mainUI = TuttiUIUtil.getApplicationContext(dialog).getMainUI();
            SwingUtil.setComponentHeight(dialog, 400);
            SwingUtil.setComponentWidth(dialog, mainUI.getWidth() - 100);
            SwingUtil.center(mainUI, dialog);

            dialog.setContextValue(getUI(), "owner");
            dialog.setVisible(true);
            ((TuttiUI) dialog).getHandler().onCloseUI();

        }

    }

}