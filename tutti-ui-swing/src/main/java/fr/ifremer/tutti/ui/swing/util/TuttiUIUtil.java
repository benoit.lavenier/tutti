package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.SwingUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.swing.util.ApplicationUIUtil;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static org.nuiton.i18n.I18n.t;

/**
 * Created: 14/06/12
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public final class TuttiUIUtil extends ApplicationUIUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiUIUtil.class);

    private TuttiUIUtil() {
        // never instanciate util class
    }

    public static final MouseListener GRAB_FOCUS_ON_ENTER_LISTENER = new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
            JComponent source = (JComponent) e.getSource();
            source.grabFocus();
        }
    };

    public static TuttiUIContext getApplicationContext(JAXXObject ui) {
        return (TuttiUIContext) ApplicationUIUtil.getApplicationContext(ui);
    }

    public static void setParentUI(JAXXObject ui, TuttiUI<?, ?> parentUI) {
        JAXXUtil.initContext(ui, parentUI);
        setApplicationContext(ui, parentUI.getHandler().getContext());
    }

    public static void tryToConnectToUpdateUrl(String urlAsString,
                                               String badUrlFormatI18nKey,
                                               String notReachI18nKey,
                                               String notFoundI18nKey) {
        URL url;
        // get url
        try {
            url = new URL(urlAsString);
        } catch (MalformedURLException e) {
            if (log.isDebugEnabled()) {
                log.debug("Bad url syntax at " + urlAsString, e);
            }
            throw new ApplicationBusinessException(t(badUrlFormatI18nKey, urlAsString));
        }

        URLConnection urlConnection;
        // try to connect (fail if network or remote host does not exists)
        try {
            urlConnection = url.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.connect();
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("Could not connect to " + urlAsString, e);
            }
            throw new ApplicationBusinessException(t(notReachI18nKey, urlAsString));
        }

        // try to open the resource (fail if resources does not exist)
        try {
            urlConnection.setReadTimeout(1000);
            InputStream inputStream = null;
            try {
                inputStream = urlConnection.getInputStream();
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("Could not found file at to " + urlAsString, e);
            }
            throw new ApplicationBusinessException(t(notFoundI18nKey, urlAsString));
        }
    }

    public static ImageIcon getCruiseIcon(Cruise cruise) {
        String iconName = "cruise";
        if (cruise != null) {

            if (Cruises.isDirty(cruise)) {
                iconName = "cruise-dirty";
            } else if (Cruises.isReadyToSynch(cruise)) {
                iconName = "cruise-ready-to-sync";
            } else if (Cruises.isSynch(cruise)) {
                iconName = "cruise-waiting";
            }
        }
        return SwingUtil.createActionIcon(iconName);
    }

    public static void openResource(File file) {

        Desktop desktop = getDesktopForOpen();

        try {

            desktop.open(file);
        } catch (Exception e) {

            throw new ApplicationBusinessException(t("swing.error.cannot.open.file"));

        }
    }

    /**
     * Computes the brightness of a color. This can be useful to determine the text color according to the backgound.
     * (e.g. if the backgound's brightness is over 125, the text could be written in black, otherwise in white)
     *
     * @param c the color
     * @return the brightness of the color: 0 the darkest, 255 the brightest
     */
    public static int getColorBrightness(Color c) {
        int red = c.getRed();
        int green = c.getGreen();
        int blue = c.getBlue();

        return (int) Math.sqrt(red * red * .241 +
                               green * green * .691 +
                               blue * blue * .068);
    }

    public static void prepareAction(AbstractButton button, Action action, String actionName) {

        action.putValue(Action.SMALL_ICON, button.getIcon());
        action.putValue(Action.LARGE_ICON_KEY, button.getIcon());
        action.putValue(Action.ACTION_COMMAND_KEY, actionName);
        action.putValue(Action.NAME, button.getText());
        action.putValue(Action.SHORT_DESCRIPTION, button.getToolTipText());
        action.putValue(Action.MNEMONIC_KEY, button.getMnemonic());

        button.setAction(action);

    }

    public static void initButton(TuttiUIContext context, JAXXObject ui, AbstractButton abstractButton) {

//        super.initButton(abstractButton);

        Class actionType = (Class) abstractButton.getClientProperty("simpleAction");
        if (actionType != null) {

            Action action = context.getActionFactory().createSimpleAction(ui, abstractButton, actionType);
            abstractButton.setAction(action);

        }

        String actionName = abstractButton.getName();
        Action action = abstractButton.getAction();
        Boolean skipAction = (Boolean) abstractButton.getClientProperty("skipAction");

        if (BooleanUtils.isNotTrue(skipAction)
            && abstractButton.isFocusable()
            && !(abstractButton instanceof JMenuItem)
            && !(abstractButton instanceof JCheckBox)
            && !(abstractButton instanceof JRadioButton)) {

            if (action == null) {
                throw new IllegalStateException("No action defined for button: " + actionName);
            }

            if (log.isDebugEnabled()) {
                log.debug("Register action: " + actionName);
            }

            abstractButton.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), actionName);
            abstractButton.getActionMap().put(actionName, action);

        }
    }

    //FIXME move to jaxx
    public static void collapseTree(final JTree tree) {
        SwingUtilities.invokeLater(() -> {
            int i = 0;

            while(i < tree.getRowCount()) {
                tree.collapseRow(i++);
            }

        });
    }

}
