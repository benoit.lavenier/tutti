package fr.ifremer.tutti.ui.swing.content.validation.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import jaxx.runtime.SwingUtil;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.ImageIcon;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class CruiseTreeNode extends TuttiMessageNodeSupport<Cruise> {

    private static final long serialVersionUID = 1L;

    private static final ImageIcon CRUISE_ICON = SwingUtil.createActionIcon("cruise");

    public CruiseTreeNode(Cruise cruise, NuitonValidatorResult validationResult) {
        super(cruise, CRUISE_ICON, null);

        setAllowsChildren(true);
        createChildren(validationResult);
    }

    public void createChildren(NuitonValidatorResult validationResult) {

        boolean withMessage = false;

        if (validationResult.hasFatalMessages()) {
            addMessages(NuitonValidatorScope.FATAL,
                        validationResult.getMessagesForScope(NuitonValidatorScope.FATAL));
            withMessage = true;
        }

        if (validationResult.hasErrorMessagess()) {
            addMessages(NuitonValidatorScope.ERROR,
                        validationResult.getMessagesForScope(NuitonValidatorScope.ERROR));
            withMessage = true;
        }

        if (validationResult.hasWarningMessages()) {
            addMessages(NuitonValidatorScope.WARNING,
                        validationResult.getMessagesForScope(NuitonValidatorScope.WARNING));
            withMessage = true;
        }

        if (!withMessage) {
            addMessages(NuitonValidatorScope.INFO,
                        Lists.newArrayList(t("tutti.validator.info.cruise.noError")));
        }
    }

    protected void addMessages(NuitonValidatorScope scope, List<String> messages) {
        // use a set to remove doublons
        Set<String> messageSet = Sets.newHashSet(messages);
        for (String message : messageSet) {
            MessageTreeNode child = new MessageTreeNode(scope, message, null);
            this.add(child);
        }
    }

}
