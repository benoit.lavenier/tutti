package fr.ifremer.tutti.ui.swing.util.attachment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Attachment;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.decorator.Decorator;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Color;
import java.awt.Font;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Renderer of a attachement editor in a table cell.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
public class AttachmentCellRenderer extends DefaultTableCellRenderer {

    public static final String TEXT_PATTERN = "<html><body>%s</body></html>";

    private static final long serialVersionUID = 1L;

    private final String noneText;

    private final Decorator<Attachment> decorator;

    private Font defaulfFont;

    private Font selectedFont;

    public static AttachmentCellRenderer newRender(Decorator<Attachment> decorator) {
        return new AttachmentCellRenderer(decorator);
    }

    protected AttachmentCellRenderer(Decorator<Attachment> decorator) {
        setHorizontalAlignment(CENTER);
        setIcon(SwingUtil.createActionIcon("edit-attachment"));
        this.noneText = n("tutti.attachmentEditor.none.tip");
        this.decorator = decorator;
    }

    @Override
    protected void setValue(Object value) {
        // do nothing
    }

    @Override
    public JComponent getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {

        if (defaulfFont == null) {
            defaulfFont = UIManager.getFont("Table.font");
            selectedFont = defaulfFont.deriveFont(Font.BOLD);
        }

        List<Attachment> attachments = (List<Attachment>) value;

        String toolTipTextValue;

        if (CollectionUtils.isEmpty(attachments)) {

            // use HTML to show the tooltip in italic
            toolTipTextValue = "<i>" + t(noneText) + "</i>";


        } else {

            StringBuilder sb = new StringBuilder();
            for (Attachment attachment : attachments) {
                sb.append("<br/>").append(decorator.toString(attachment));
            }
            // use html to display the tooltip on several lines
            toolTipTextValue = sb.substring(5);
        }
        String textValue = ButtonAttachment.getButtonText(attachments);
        boolean editable = table.isCellEditable(row, column);
        toolTipTextValue = String.format(TEXT_PATTERN, toolTipTextValue);
        setEnabled(editable);
        setText(textValue);
        setToolTipText(toolTipTextValue);
        setBackground(null);
        setForeground(Color.BLACK);

        if (isSelected) {
            setFont(selectedFont);
        } else {
            setFont(defaulfFont);
        }

        return this;
    }
}
