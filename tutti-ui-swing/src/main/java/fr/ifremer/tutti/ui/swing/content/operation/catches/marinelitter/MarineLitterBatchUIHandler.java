package fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.AbstractTuttiBatchTableUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.create.CreateMarineLitterBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellEditor;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellRenderer;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellEditor;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellRenderer;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.nuiton.jaxx.application.bean.JavaBeanObjectPropagateChangeListener;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.nuiton.validator.NuitonValidatorResult;

import javax.swing.JComponent;
import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class MarineLitterBatchUIHandler extends AbstractTuttiBatchTableUIHandler<MarineLitterBatchRowModel, MarineLitterBatchUIModel, MarineLitterBatchTableModel, MarineLitterBatchUI> {

    private static final Log log = LogFactory.getLog(MarineLitterBatchUIHandler.class);

    public MarineLitterBatchUIHandler() {
        super(
                MarineLitterBatchRowModel.PROPERTY_MARINE_LITTER_CATEGORY,
                MarineLitterBatchRowModel.PROPERTY_MARINE_LITTER_SIZE_CATEGORY,
                MarineLitterBatchRowModel.PROPERTY_WEIGHT,
                MarineLitterBatchRowModel.PROPERTY_NUMBER,
                MarineLitterBatchRowModel.PROPERTY_COMMENT);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBatchTableUIHandler methods                           --//
    //------------------------------------------------------------------------//

    @Override
    protected ColumnIdentifier<MarineLitterBatchRowModel> getCommentIdentifier() {
        return MarineLitterBatchTableModel.COMMENT;
    }

    @Override
    protected ColumnIdentifier<MarineLitterBatchRowModel> getAttachementIdentifier() {
        return MarineLitterBatchTableModel.ATTACHMENT;
    }

    @Override
    public void selectFishingOperation(FishingOperation bean) {

        boolean empty = bean == null;

        MarineLitterBatchUIModel model = getModel();

        List<MarineLitterBatchRowModel> rows;

        if (empty) {
            rows = null;
        } else {

            if (log.isDebugEnabled()) {
                log.debug("Get marineLitter batch for fishingOperation: " +
                                  bean.getId());
            }
            rows = Lists.newArrayList();

            if (!TuttiEntities.isNew(bean)) {

                // get all marine litter root
                BatchContainer<MarineLitterBatch> batchContainer =
                        getPersistenceService().getRootMarineLitterBatch(bean.getIdAsInt());

                for (MarineLitterBatch aBatch : batchContainer.getChildren()) {
                    MarineLitterBatchRowModel entry = loadBatch(aBatch);
                    rows.add(entry);
                }
            }
        }
        model.setRows(rows);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public MarineLitterBatchTableModel getTableModel() {
        return (MarineLitterBatchTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getTable();
    }

    @Override
    protected boolean isRowValid(MarineLitterBatchRowModel row) {
        MarineLitterBatch batch = row.toEntity();
        NuitonValidatorResult validator = getValidationService().validateEditMarineLitterBatch(batch);
        return !validator.hasErrorMessagess();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<MarineLitterBatchRowModel> rowMonitor,
                                             MarineLitterBatchRowModel row) {

        if (row != null && row.isValid() && rowMonitor.wasModified()) {

            // monitored bean was modified, save it
            if (log.isDebugEnabled()) {
                log.debug("Row " + row + " was modified, will save it");
            }

            String title = buildReminderLabelTitle(decorate(row.getMarineLitterCategory()) + " - " + decorate(row.getMarineLitterSizeCategory()),
                                                   null,
                                                   "Sauvegarde du lot Macro déchet : ",
                                                   "Ligne :" + (getTableModel().getRowIndex(row) + 1));

            showInformationMessage(title);

            rowMonitor.setBean(null);
            saveRow(row);
            rowMonitor.setBean(row);

            // clear modified flag on the monitor
            rowMonitor.clearModified();
        }
    }

    @Override
    protected void onModelRowsChanged(List<MarineLitterBatchRowModel> rows) {
        super.onModelRowsChanged(rows);

        for (MarineLitterBatchRowModel row : rows) {
            // update categoriesUsed
            addToMarineLitterCategoriesUsed(row);
        }
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public SwingValidator<MarineLitterBatchUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public void beforeInit(MarineLitterBatchUI ui) {

        super.beforeInit(ui);
        if (log.isDebugEnabled()) {
            log.debug("beforeInit: " + ui);
        }

        EditCatchesUIModel catchesUIModel = ui.getContextValue(EditCatchesUIModel.class);

        MarineLitterBatchUIModel model = new MarineLitterBatchUIModel(catchesUIModel, getConfig().getMarineLitterWeightUnit());
        ui.setContextValue(model);

        // propagate when value is changing
        JavaBeanObjectPropagateChangeListener.listenAndPropagate(
                catchesUIModel,
                model,
                EditCatchesUIModel.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT,
                EditCatchesUIModel.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT);
    }

    @Override
    public void afterInit(MarineLitterBatchUI ui) {

        if (log.isDebugEnabled()) {
            log.debug("afterInit: " + ui);
        }

        initUI(ui);

        JXTable table = getTable();

        // create table column model
        DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        {
            // Id column

            addIdColumnToModel(columnModel, MarineLitterBatchTableModel.ID, table);

        }

        {
            // MarineLitter Category column

            addColumnToModel(columnModel,
                             null,
                             newTableCellRender(CaracteristicQualitativeValue.class, null),
                             MarineLitterBatchTableModel.MACRO_WASTE_CATEGORY);
        }

        {
            // MarineLitter Size Category column

            addColumnToModel(columnModel,
                             null,
                             newTableCellRender(CaracteristicQualitativeValue.class, null),
                             MarineLitterBatchTableModel.MACRO_WASTE_SIZE_CATEGORY);
        }

        { // Number column

            addIntegerColumnToModel(columnModel,
                                    MarineLitterBatchTableModel.NUMBER,
                                    TuttiUI.INT_3_DIGITS_PATTERN,
                                    table);
        }

        WeightUnit weightUnit = getModel().getWeightUnit();

        { // Weight column

            addFloatColumnToModel(columnModel,
                                  MarineLitterBatchTableModel.WEIGHT,
                                  weightUnit,
                                  table);
        }

        { // Comment column

            addColumnToModel(columnModel,
                             CommentCellEditor.newEditor(ui),
                             CommentCellRenderer.newRender(),
                             MarineLitterBatchTableModel.COMMENT);
        }

        { // File column

            addColumnToModel(columnModel,
                             AttachmentCellEditor.newEditor(ui),
                             AttachmentCellRenderer.newRender(getDecorator(Attachment.class, null)),
                             MarineLitterBatchTableModel.ATTACHMENT);
        }

        // create table model
        MarineLitterBatchTableModel tableModel = new MarineLitterBatchTableModel(weightUnit, columnModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        initBatchTable(table, columnModel, tableModel);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getTable();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        ui.getMarineLitterBatchAttachmentsButton().onCloseUI();
    }

    @Override
    protected void beforeOpenPopup(int rowIndex, int columnIndex) {
        super.beforeOpenPopup(rowIndex, columnIndex);

        boolean enableRemove = false;

        if (rowIndex != -1) {

            // there is a selected row
            enableRemove = true;
        }
        MarineLitterBatchUIModel model = getModel();
        model.setRemoveBatchEnabled(enableRemove);
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void addBatch(CreateMarineLitterBatchUIModel model) {
        if (model.isValid()) {

            MarineLitterBatchTableModel tableModel = getTableModel();

            MarineLitterBatchRowModel newRow = tableModel.createNewRow();
            newRow.setMarineLitterCategory(model.getMarineLitterCategory());
            newRow.setMarineLitterSizeCategory(model.getMarineLitterSizeCategory());
            newRow.setNumber(model.getNumber());
            newRow.setWeight(model.getWeight());

            recomputeRowValidState(newRow);

            saveRow(newRow);

            tableModel.addNewRow(newRow);
            TuttiUIUtil.selectFirstCellOnLastRow(getTable());

            //update categories used
            addToMarineLitterCategoriesUsed(newRow);
        }
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected MarineLitterBatchRowModel loadBatch(MarineLitterBatch aBatch) {

        MarineLitterBatchRowModel newRow =
                new MarineLitterBatchRowModel(getModel().getWeightUnit(), aBatch);

        List<Attachment> attachments =
                getPersistenceService().getAllAttachments(newRow.getObjectType(), newRow.getObjectId());

        newRow.addAllAttachment(attachments);
        return newRow;
    }

    protected void saveRow(MarineLitterBatchRowModel row) {

        MarineLitterBatch entityToSave = row.toEntity();

        FishingOperation fishingOperation = getModel().getFishingOperation();
        entityToSave.setFishingOperation(fishingOperation);
        if (log.isInfoEnabled()) {
            log.info("Selected fishingOperation: " + fishingOperation.getId());
        }

        if (TuttiEntities.isNew(entityToSave)) {

            entityToSave = getPersistenceService().createMarineLitterBatch(entityToSave);
            row.setId(entityToSave.getId());
        } else {
            getPersistenceService().saveMarineLitterBatch(entityToSave);
        }

        fireBatchSaved(row);

    }

    public void removeFromMarineLitterCategoriesUsed(MarineLitterBatchRowModel row) {
        Preconditions.checkNotNull(row);
        Preconditions.checkNotNull(row.getMarineLitterCategory());
        Preconditions.checkNotNull(row.getMarineLitterSizeCategory());
        if (log.isInfoEnabled()) {
            log.info("Remove from speciesUsed: " +
                             decorate(row.getMarineLitterSizeCategory()) +
                             " - " + decorate(row.getMarineLitterCategory()));
        }
        MarineLitterBatchUIModel model = getModel();
        model.getMarineLitterCategoriesUsed().remove(row.getMarineLitterSizeCategory(),
                                                     row.getMarineLitterCategory());

    }

    protected void addToMarineLitterCategoriesUsed(MarineLitterBatchRowModel row) {
        Preconditions.checkNotNull(row);
        Preconditions.checkNotNull(row.getMarineLitterCategory());
        Preconditions.checkNotNull(row.getMarineLitterSizeCategory());
        if (log.isDebugEnabled()) {
            log.debug("Add to marineLitterCategoriesUsed: " +
                              decorate(row.getMarineLitterSizeCategory()) +
                              " - " + decorate(row.getMarineLitterCategory()));
        }
        MarineLitterBatchUIModel model = getModel();
        model.getMarineLitterCategoriesUsed().put(row.getMarineLitterSizeCategory(),
                                                  row.getMarineLitterCategory());

    }
}
