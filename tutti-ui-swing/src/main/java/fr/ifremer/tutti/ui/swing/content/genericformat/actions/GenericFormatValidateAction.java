package fr.ifremer.tutti.ui.swing.content.genericformat.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportConfiguration;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportService;
import fr.ifremer.tutti.service.genericformat.GenericFormatValidateFileResult;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUI;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUIHandler;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUIModel;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Icon;
import javax.swing.JLabel;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatValidateAction extends LongActionSupport<GenericFormatImportUIModel, GenericFormatImportUI, GenericFormatImportUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatValidateAction.class);

    private GenericFormatValidateFileResult validateFileResult;

    public GenericFormatValidateAction(GenericFormatImportUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            doAction = getModel().isCanValidate();

        }

        if (doAction) {

            // Remove any previous validate result
            updateResult(null);

        }

        return doAction;

    }

    @Override
    public void doAction() throws Exception {

        getModel().setValidateReportFile(getConfig().newTempFile("genericFormatValidateReport", ".pdf"));

        int maximumRowsInErrorsPerFile = getConfig().getGenericFormatImportMaximumRowsInErrorsPerFile();
        GenericFormatImportConfiguration configuration = getModel().toValidateImportFileConfiguration(maximumRowsInErrorsPerFile);

        GenericFormatImportService service = getContext().getGenericFormatImportService();

        int nbSteps = service.getValidateImportFileNbSteps(configuration);

        if (log.isInfoEnabled()) {
            log.info("validate import file nb steps: " + nbSteps);
        }
        createProgressionModelIfRequired(nbSteps);

        Program program = getModel().getProgram();

        if (log.isInfoEnabled()) {
            log.info("Validate generic format import file for program: " + program.getName() + " from file: " + configuration.getImportFile());
        }

        validateFileResult = service.validateImportFile(configuration, getProgressionModel());

    }

    @Override
    public void postSuccessAction() {

        updateResult(validateFileResult);

    }

    @Override
    protected void releaseAction() {
        validateFileResult = null;
        super.releaseAction();
    }

    protected void updateResult(GenericFormatValidateFileResult result) {

        getModel().setValidateResult(result);

        if (result != null) {

            Icon icon;
            String text;
            String tip;
            if (result.isValid()) {

                icon = SwingUtil.createActionIcon("accept");
                text = t("tutti.genericFormat.validate.success");
                tip = t("tutti.genericFormat.validate.success.tip");

            } else {

                icon = SwingUtil.createActionIcon("cancel");
                text = t("tutti.genericFormat.validate.error");
                tip = t("tutti.genericFormat.validate.error.tip");

            }

            JLabel resultText = getUI().getValidateResultText();
            resultText.setIcon(icon);
            resultText.setText(text);
            resultText.setToolTipText(tip);

        }

    }

}