package fr.ifremer.tutti.ui.swing.util.computable;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import jaxx.runtime.JAXXUtil;

import javax.swing.AbstractCellEditor;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.Serializable;

/**
 * Editor for TuttiComputedOrNotData
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class ComputableDataTableCell extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 1L;

    public static TableCellRenderer newRender(TableCellRenderer renderer, WeightUnit weightUnit, Color computedDataColor) {

        return new TuttiComputedOrNotDataTableCellRenderer(weightUnit, renderer, computedDataColor);
    }

    public static TableCellEditor newEditor(WeightUnit weightUnit, Color computedDataColor) {

        return new TuttiComputedOrNotDataTableCellEditor(weightUnit, computedDataColor);
    }

    public static class TuttiComputedOrNotDataTableCellEditor extends AbstractCellEditor implements TableCellEditor, FocusListener, AncestorListener {

        private static final long serialVersionUID = 1L;

        private final ComputableDataEditor<Float> numberEditor;

        private ComputableData<Float> data;

        /** constructor */
        public TuttiComputedOrNotDataTableCellEditor(WeightUnit weightUnit, Color computedDataColor) {

            numberEditor = new ComputableDataEditor<>();
            numberEditor.setComputedDataColor(computedDataColor);
            numberEditor.getTextField().setHorizontalAlignment(SwingConstants.RIGHT);
            numberEditor.getTextField().addFocusListener(this);
            numberEditor.getTextField().addAncestorListener(this);
            numberEditor.getTextField().setBorder(new LineBorder(Color.GRAY, 2));
            numberEditor.setSelectAllTextOnError(true);

            numberEditor.setNumberType(Float.class);
            numberEditor.setUseSign(false);
            numberEditor.init();
            numberEditor.setWeightUnit(weightUnit);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

            data = (ComputableData<Float>) value;
            numberEditor.setNumberValue(data.getData());

            // Check nullity and set the text that will be selected with the current value
            //FIXME NumberEditor
//            if (data.getData() != null) {
//                numberEditor.getModel().setTextValue(String.valueOf(data.getData()));
//            }
            return numberEditor;
        }

        public ComputableDataEditor getNumberEditor() {
            return numberEditor;
        }

        @Override
        public ComputableData<Float> getCellEditorValue() {
            return data;
        }

        @Override
        public void focusGained(FocusEvent e) {
            SwingUtilities.invokeLater(() -> {
                numberEditor.getTextField().requestFocus();
                numberEditor.getTextField().selectAll();
            });
        }

        @Override
        public void focusLost(FocusEvent e) {
        }

        @Override
        public void ancestorAdded(AncestorEvent event) {
            SwingUtilities.invokeLater(() -> {
                numberEditor.getTextField().requestFocus();
                numberEditor.getTextField().selectAll();
            });
        }

        @Override
        public void ancestorRemoved(AncestorEvent event) {
        }

        @Override
        public void ancestorMoved(AncestorEvent event) {
        }

        @Override
        public boolean stopCellEditing() {
            boolean result = super.stopCellEditing();
            // Reset previous data to avoid keeping it on other cell edition
            if (result) {
                data.setData((Float) numberEditor.getModel().getNumberValue());

                numberEditor.setBean((Serializable) null);

                data = null;
            }
            return result;
        }
    }

    private static class TuttiComputedOrNotDataTableCellRenderer implements TableCellRenderer {

        private final WeightUnit weightUnit;

        protected final TableCellRenderer delegate;

//        protected Integer decimalNumber;

        protected Color computedDataColor;

//        protected boolean useFloat;

        private TuttiComputedOrNotDataTableCellRenderer(WeightUnit weightUnit,
                                                        TableCellRenderer delegate,
//                                                       boolean useFloat,
//                                                       Integer decimalNumber,
                                                        Color computedDataColor) {
            this.weightUnit = weightUnit;
            this.delegate = delegate;
//            this.useFloat = useFloat;
//            this.decimalNumber = decimalNumber;
            this.computedDataColor = computedDataColor;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {

            ComputableData<Float> data = (ComputableData<Float>) value;
            Float dataValue = data.getData();
            Font font;
            Color foreground;
            String text;
            if (dataValue == null) {
                dataValue = data.getComputedData();
                font = TuttiUI.TEXTFIELD_COMPUTED_FONT;
                foreground = computedDataColor;

                text = weightUnit.renderWeight(dataValue);

//                if (useFloat && decimalNumber != null && dataValue != null) {
//                    DecimalFormat decimalFormat = Weights.getDecimalFormat(1, decimalNumber);
//                    text = JAXXUtil.getStringValue(decimalFormat.format(dataValue));
//                    text = weightUnit.renderWeight((Float) dataValue);
//                } else {
//                    text = JAXXUtil.getStringValue(dataValue);
//                }

            } else {
                font = TuttiUI.TEXTFIELD_NORMAL_FONT;
                foreground = Color.BLACK;
                text = JAXXUtil.getStringValue(dataValue);
            }

            Component component = delegate.getTableCellRendererComponent(table,
                                                                         text,
                                                                         isSelected,
                                                                         hasFocus,
                                                                         row,
                                                                         column);

            if (isSelected) {
                font = font.deriveFont(Font.BOLD);
            }
            component.setFont(font);
            component.setForeground(foreground);
            if (component instanceof JLabel) {
                JLabel jLabel = (JLabel) component;
                jLabel.setHorizontalAlignment(RIGHT);
            }

            return component;
        }

    }

}
