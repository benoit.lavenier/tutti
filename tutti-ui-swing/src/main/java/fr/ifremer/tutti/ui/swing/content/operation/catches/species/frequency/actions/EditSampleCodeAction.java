package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.sampling.SamplingCodePrefix;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation.SampleCodeEditionPopupUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation.SampleCodeEditionPopupUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class EditSampleCodeAction extends SimpleActionSupport<SpeciesFrequencyUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditSampleCodeAction.class);

    public EditSampleCodeAction(SpeciesFrequencyUI speciesFrequencyUI) {
        super(speciesFrequencyUI, false);
    }

    @Override
    protected void onActionPerformed(SpeciesFrequencyUI ui) {
        JXTable obsTable = ui.getObsTable();
        IndividualObservationBatchTableModel obsTableModel = (IndividualObservationBatchTableModel) obsTable.getModel();

        int selectedRowIndex = obsTable.getSelectedRow();
        IndividualObservationBatchRowModel selectedRow = obsTableModel.getRows().get(selectedRowIndex);
        SampleCodeEditionPopupUI sampleCodeEditionPopupUI = new SampleCodeEditionPopupUI(ui);

        String samplingCode;
        boolean samplingCodeAvailable = false;
        boolean modelValid;
        do {
            samplingCode = null;

            Integer samplingCodeId = selectedRow.getSamplingCodeId();

            SamplingCodePrefix samplingCodePrefix = ui.getModel().getIndividualObservationModel().getSamplingCodePrefix();
            sampleCodeEditionPopupUI.open(samplingCodePrefix, samplingCodeId);

            SampleCodeEditionPopupUIModel model = sampleCodeEditionPopupUI.getModel();
            modelValid = model.isValid();

            if (modelValid) {

                Integer sampleCode = model.getSampleCode();

                samplingCode = samplingCodePrefix.toSamplingCode(sampleCode);
                if (log.isDebugEnabled()) {
                    log.debug("Test if sampling code " + samplingCode + " is available.");
                }

                samplingCodeAvailable = samplingCodeId.equals(sampleCode) || ui.getModel().getSamplingCodeUICache().canUseSamplingCode(sampleCode);
                if (!samplingCodeAvailable) {

                    if (log.isDebugEnabled()) {
                        log.debug("Sampling code " + samplingCode + " is not available.");
                    }
                    TuttiUIContext.getApplicationContext().getErrorHelper().showErrorDialog(t("tutti.editSpeciesFrequencies.error.notAvailableSamplingCode", samplingCode));

                }
            }

        } while (!samplingCodeAvailable && modelValid);

        if (samplingCode != null) {

            if (log.isDebugEnabled()) {
                log.debug("Sampling code " + samplingCode + " is available, use it on selected row.");
            }

            selectedRow.setSamplingCode(samplingCode);
            obsTableModel.fireTableRowsUpdated(selectedRowIndex, selectedRowIndex);

        }
    }

}
