
NumberEditor {
  autoPopup: false;
  showPopupButton: false;
  bean: {model};
  showReset: true;
  _selectOnFocus: {true};
}

.aEditor {
  numberType: {Double.class};
  numberPattern: {DECIMAL9_PATTERN};
}

.bEditor {
  numberType: {Float.class};
  numberPattern: {DECIMAL4_PATTERN};
}

#rtpEditorDialog {
  undecorated: true;
  alwaysOnTop: true;
}

#headerToolBar {
  floatable: false;
  opaque: true;
  borderPainted: false;
}

#previousRowButton {
  actionIcon: previous;
  toolTipText: "tutti.editRtp.action.previous.tip";
  enabled: {!model.isFirstRow()};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.protocol.rtp.actions.EditPreviousRowAction.class};
}

#nextRowButton {
  actionIcon: next;
  toolTipText: "tutti.editRtp.action.next.tip";
  enabled: {!model.isLastRow()};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.protocol.rtp.actions.EditNextRowAction.class};
}

#rtpFormulaLabelContainer {
  background: {new java.awt.Color(245, 218, 88)};
}

#rtpFormulaLabel {
  text: "tutti.editRtp.formula";
  horizontalAlignment: "center";
  border: {BorderFactory.createEmptyBorder(5, 10, 5, 10)};
}

#rtpMaleALabel {
  text: "tutti.editRtp.field.rtpMaleA";
  toolTipText: "tutti.editRtp.field.rtpMaleA.tip";
  labelFor: {rtpMaleAField};
  _help: {"tutti.editRtp.field.rtpMaleA.help"};
}

#rtpMaleAField {
  property:{RtpEditorUIModel.PROPERTY_RTP_MALE_A};
  numberValue: {model.getRtpMaleA()};
  _help: {"tutti.editRtp.field.rtpMaleA.help"};
}

#rtpMaleBLabel {
  text: "tutti.editRtp.field.rtpMaleB";
  toolTipText: "tutti.editRtp.field.rtpMaleB.tip";
  labelFor: {rtpMaleBField};
  _help: {"tutti.editRtp.field.rtpMaleB.help"};
}

#rtpMaleBField {
  property:{RtpEditorUIModel.PROPERTY_RTP_MALE_B};
  numberValue: {model.getRtpMaleB()};
  _help: {"tutti.editRtp.field.rtpMaleB.help"};
}

#copyValuesButton {
  actionIcon: copy;
  text: "tutti.editRtp.action.copyValues";
  toolTipText: "tutti.editRtp.action.copyValues.tip";
  i18nMnemonic: "tutti.editRtp.action.copyValues.mnemonic";
  enabled: {model.getRtpMaleA() != null && model.getRtpMaleB() != null};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.protocol.rtp.actions.CopyMaleRtpAction.class};
  _help: {"tutti.editRtp.action.copyValues.help"};
}

#rtpFemaleALabel {
  text: "tutti.editRtp.field.rtpFemaleA";
  toolTipText: "tutti.editRtp.field.rtpFemaleA.tip";
  labelFor: {rtpFemaleAField};
  _help: {"tutti.editRtp.field.rtpFemaleA.help"};
}

#rtpFemaleAField {
  property:{RtpEditorUIModel.PROPERTY_RTP_FEMALE_A};
  numberValue: {model.getRtpFemaleA()};
  _help: {"tutti.editRtp.field.rtpFemaleA.help"};
}

#rtpFemaleBLabel {
  text: "tutti.editRtp.field.rtpFemaleB";
  toolTipText: "tutti.editRtp.field.rtpFemaleB.tip";
  labelFor: {rtpFemaleBField};
  _help: {"tutti.editRtp.field.rtpFemaleB.help"};
}

#rtpFemaleBField {
  property:{RtpEditorUIModel.PROPERTY_RTP_FEMALE_B};
  numberValue: {model.getRtpFemaleB()};
  _help: {"tutti.editRtp.field.rtpFemaleB.help"};
}

#rtpUndefinedALabel {
  text: "tutti.editRtp.field.rtpUndefinedA";
  toolTipText: "tutti.editRtp.field.rtpUndefinedA.tip";
  labelFor: {rtpUndefinedAField};
  _help: {"tutti.editRtp.field.rtpUndefinedA.help"};
}

#rtpUndefinedAField {
  property:{RtpEditorUIModel.PROPERTY_RTP_UNDEFINED_A};
  numberValue: {model.getRtpUndefinedA()};
  _help: {"tutti.editRtp.field.rtpUndefinedA.help"};
}

#rtpUndefinedBLabel {
  text: "tutti.editRtp.field.rtpUndefinedB";
  toolTipText: "tutti.editRtp.field.rtpUndefinedB.tip";
  labelFor: {rtpUndefinedBField};
  _help: {"tutti.editRtp.field.rtpUndefinedB.help"};
}

#rtpUndefinedBField {
  property:{RtpEditorUIModel.PROPERTY_RTP_UNDEFINED_B};
  numberValue: {model.getRtpUndefinedB()};
  _help: {"tutti.editRtp.field.rtpUndefinedB.help"};
}

#saveButton {
  actionIcon: save;
  text: "tutti.editRtp.action.save";
  toolTipText: "tutti.editRtp.action.save.tip";
  i18nMnemonic: "tutti.editRtp.action.save.mnemonic";
  enabled: {model.isModify() && model.isValid()};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.protocol.rtp.actions.SaveRtpAction.class};
  _help: {"tutti.editRtp.action.save.help"};
}

#closeButton {
  actionIcon: close;
  text: "tutti.editRtp.action.close";
  toolTipText: "tutti.editRtp.action.close.tip";
  i18nMnemonic: "tutti.editRtp.action.close.mnemonic";
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.protocol.rtp.actions.CloseRtpAction.class};
  _help: {"tutti.editRtp.action.close.help"};
}