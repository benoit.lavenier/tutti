package fr.ifremer.tutti.ui.swing.content.operation.catches.species;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportResult;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 17/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BenthosBatchUISupportImpl extends SpeciesOrBenthosBatchUISupport {

    protected static final Map<String, String> CATCHES_UI_MODEL_PROPERTIES_MAPPING =
            ImmutableMap.<String, String>builder()
                    .put(EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_COMPUTED_WEIGHT, PROPERTY_TOTAL_COMPUTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT, PROPERTY_TOTAL_SORTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_UNSORTED_COMPUTED_WEIGHT, PROPERTY_TOTAL_UNSORTED_COMPUTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT, PROPERTY_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT, PROPERTY_TOTAL_INERT_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, PROPERTY_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT)
                    .build();

    public BenthosBatchUISupportImpl(TuttiUIContext context, EditCatchesUIModel catchesUIModel, WeightUnit weightUnit) {
        super(context, catchesUIModel, CATCHES_UI_MODEL_PROPERTIES_MAPPING, weightUnit);
    }

    @Override
    public BatchContainer<SpeciesBatch> getRootSpeciesBatch(Integer batchId) {
        return getPersistenceService().getRootBenthosBatch(batchId, true);
    }

    @Override
    public String getTitle() {
        return n("tutti.label.tab.benthos");
    }

    @Override
    public boolean canPupitriImport() {
        return false;
    }

    @Override
    public boolean canPsionImport() {
        return false;
    }

    @Override
    public boolean canBigfinImport() {
        return false;
    }

    @Override
    public Float getTotalComputedWeight() {
        return catchesUIModel.getBenthosTotalComputedWeight();
    }

    @Override
    public void setTotalComputedWeight(Float totalComputedWeight) {
        catchesUIModel.setBenthosTotalComputedWeight(totalComputedWeight);
    }

    @Override
    public ComputableData<Float> getTotalSortedComputedOrNotWeight() {
        return catchesUIModel.getBenthosTotalSortedComputedOrNotWeight();
    }

    @Override
    public Float getTotalSortedWeight() {
        return catchesUIModel.getBenthosTotalSortedWeight();
    }

    @Override
    public void setTotalSortedWeight(Float totalSortedWeight) {
        catchesUIModel.setBenthosTotalSortedWeight(totalSortedWeight);
    }

    @Override
    public Float getTotalSortedComputedWeight() {
        return catchesUIModel.getBenthosTotalSortedComputedWeight();
    }

    @Override
    public void setTotalSortedComputedWeight(Float totalSortedComputedWeight) {
        catchesUIModel.setBenthosTotalSortedComputedWeight(totalSortedComputedWeight);
    }

    @Override
    public Float getTotalUnsortedComputedWeight() {
        return catchesUIModel.getBenthosTotalUnsortedComputedWeight();
    }

    @Override
    public void setTotalUnsortedComputedWeight(Float totalUnsortedComputedWeight) {
        catchesUIModel.setBenthosTotalUnsortedComputedWeight(totalUnsortedComputedWeight);
    }

    @Override
    public Float getTotalSampleSortedComputedWeight() {
        return catchesUIModel.getBenthosTotalSampleSortedComputedWeight();
    }

    @Override
    public void setTotalSampleSortedComputedWeight(Float totalSampleSortedComputedWeight) {
        catchesUIModel.setBenthosTotalSampleSortedComputedWeight(totalSampleSortedComputedWeight);
    }

    @Override
    public ComputableData<Float> getTotalInertComputedOrNotWeight() {
        return catchesUIModel.getBenthosTotalInertComputedOrNotWeight();
    }

    @Override
    public Float getTotalInertWeight() {
        return catchesUIModel.getBenthosTotalInertWeight();
    }

    @Override
    public void setTotalInertWeight(Float totalInertWeight) {
        catchesUIModel.setBenthosTotalInertWeight(totalInertWeight);
    }

    @Override
    public Float getTotalInertComputedWeight() {
        return catchesUIModel.getBenthosTotalInertComputedWeight();
    }

    @Override
    public void setTotalInertComputedWeight(Float totalInertComputedWeight) {
        catchesUIModel.setBenthosTotalInertComputedWeight(totalInertComputedWeight);
    }

    @Override
    public ComputableData<Float> getTotalLivingNotItemizedComputedOrNotWeight() {
        return catchesUIModel.getBenthosTotalLivingNotItemizedComputedOrNotWeight();
    }

    @Override
    public Float getTotalLivingNotItemizedWeight() {
        return catchesUIModel.getBenthosTotalLivingNotItemizedWeight();
    }

    @Override
    public void setTotalLivingNotItemizedWeight(Float totalLivingNotItemizedWeight) {
        catchesUIModel.setBenthosTotalLivingNotItemizedWeight(totalLivingNotItemizedWeight);
    }

    @Override
    public Float getTotalLivingNotItemizedComputedWeight() {
        return catchesUIModel.getBenthosTotalLivingNotItemizedComputedWeight();
    }

    @Override
    public void setTotalLivingNotItemizedComputedWeight(Float totalLivingNotItemizedComputedWeight) {
        catchesUIModel.setBenthosTotalLivingNotItemizedComputedWeight(totalLivingNotItemizedComputedWeight);
    }

    @Override
    public Integer getDistinctSortedSpeciesCount() {
        return catchesUIModel.getBenthosDistinctSortedSpeciesCount();
    }

    @Override
    public void setDistinctSortedSpeciesCount(Integer distinctSortedSpeciesCount) {
        catchesUIModel.setBenthosDistinctSortedSpeciesCount(distinctSortedSpeciesCount);
    }

    @Override
    public Integer getDistinctUnsortedSpeciesCount() {
        return catchesUIModel.getBenthosDistinctUnsortedSpeciesCount();
    }

    @Override
    public void setDistinctUnsortedSpeciesCount(Integer distinctUnsortedSpeciesCount) {
        catchesUIModel.setBenthosDistinctUnsortedSpeciesCount(distinctUnsortedSpeciesCount);
    }

    @Override
    public Map<String, Object> importMultiPost(File file, FishingOperation operation, boolean importFrequencies, boolean importIndivudalObservations) {
        return context.getMultiPostImportService().importBenthos(file, operation, importFrequencies, importIndivudalObservations);
    }

    @Override
    public MultiPostImportResult importMultiPost(File file, FishingOperation operation, SpeciesBatch speciesBatch, boolean importFrequencies, boolean importIndivudalObservations) {
        return context.getMultiPostImportService().importBenthosBatch(file, operation, speciesBatch, importFrequencies, importIndivudalObservations);
    }

    @Override
    public void exportMultiPost(File file, FishingOperation operation, boolean importFrequencies, boolean importIndivudalObservations) {
        context.getMultiPostExportService().exportBenthos(file, operation, importFrequencies, importIndivudalObservations);
    }

    @Override
    public void exportMultiPost(File file, FishingOperation operation, SpeciesBatch speciesBatch, boolean importFrequencies, boolean importIndivudalObservations) {
        context.getMultiPostExportService().exportBatch(file, operation, speciesBatch, importFrequencies, importIndivudalObservations);
    }

    @Override
    public SpeciesBatch createBatch(SpeciesBatch speciesBatch, Integer parentBatchId) {
        return getPersistenceService().createBenthosBatch(speciesBatch, parentBatchId, true);
    }

    @Override
    public SpeciesBatch saveBatch(SpeciesBatch speciesBatch) {
        return getPersistenceService().saveBenthosBatch(speciesBatch);
    }

    @Override
    public List<SpeciesBatchFrequency> saveBatchFrequencies(Integer speciesBatchId, List<SpeciesBatchFrequency> frequency) {
        return getPersistenceService().saveBenthosBatchFrequency(speciesBatchId, frequency);
    }

    @Override
    public List<Species> getReferentSpeciesWithSurveyCode(boolean restrictToProtocol) {
        return context.getDataContext().getReferentBenthosWithSurveyCode(restrictToProtocol);
    }

    @Override
    public List<Species> getReferentOtherSpeciesWithSurveyCode(boolean restrictToProtocol) {
        return context.getDataContext().getReferentSpeciesWithSurveyCode(restrictToProtocol);
    }

    @Override
    public List<SpeciesProtocol> getSpeciesFromProtocol() {
        return context.getDataContext().getProtocol().getBenthos();
    }

    @Override
    public void deleteSpeciesSubBatch(Integer speciesBatchId) {
        getPersistenceService().deleteBenthosSubBatch(speciesBatchId);
    }

    @Override
    public void deleteSpeciesBatch(Integer speciesBatchId) {
        getPersistenceService().deleteBenthosBatch(speciesBatchId);
    }

}
