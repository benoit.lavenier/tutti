package fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatchs;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.List;

/**
 * Define a MarineLitter batch row.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class MarineLitterBatchRowModel extends AbstractTuttiBeanUIModel<MarineLitterBatch, MarineLitterBatchRowModel> implements AttachmentModelAware, MarineLitterBatch {

    private static final long serialVersionUID = 1L;

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final MarineLitterBatch editObject =
            MarineLitterBatchs.newMarineLitterBatch();

    /**
     * Attachments (should never be null).
     *
     * @since 0.2
     */
    protected final List<Attachment> attachment = Lists.newArrayList();

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    protected static final Binder<MarineLitterBatch, MarineLitterBatchRowModel> fromBeanBinder =
            BinderFactory.newBinder(MarineLitterBatch.class,
                                    MarineLitterBatchRowModel.class);

    protected static final Binder<MarineLitterBatchRowModel, MarineLitterBatch> toBeanBinder =
            BinderFactory.newBinder(MarineLitterBatchRowModel.class,
                                    MarineLitterBatch.class);

    public MarineLitterBatchRowModel(WeightUnit weightUnit) {
        super(fromBeanBinder, toBeanBinder);
        this.weightUnit = weightUnit;
    }

    public MarineLitterBatchRowModel(WeightUnit weightUnit,
                                     MarineLitterBatch entity) {
        this(weightUnit);
        fromEntity(entity);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBeanUIModel                                           --//
    //------------------------------------------------------------------------//

    @Override
    protected MarineLitterBatch newEntity() {
        return MarineLitterBatchs.newMarineLitterBatch();
    }

    @Override
    public void fromEntity(MarineLitterBatch entity) {
        super.fromEntity(entity);

        // convert weight
        setWeight(weightUnit.fromEntity(getWeight()));
    }

    @Override
    public MarineLitterBatch toEntity() {
        MarineLitterBatch result = super.toEntity();

        // convert weight
        result.setWeight(weightUnit.toEntity(getWeight()));
        return result;
    }

    //------------------------------------------------------------------------//
    //-- MarineLitterBatch                                               --//
    //------------------------------------------------------------------------//

    @Override
    public CaracteristicQualitativeValue getMarineLitterCategory() {
        return editObject.getMarineLitterCategory();
    }

    @Override
    public void setMarineLitterCategory(CaracteristicQualitativeValue marineLitterCategory) {
        Object oldValue = getMarineLitterCategory();
        editObject.setMarineLitterCategory(marineLitterCategory);
        firePropertyChange(PROPERTY_MARINE_LITTER_CATEGORY, oldValue, marineLitterCategory);
    }

    @Override
    public CaracteristicQualitativeValue getMarineLitterSizeCategory() {
        return editObject.getMarineLitterSizeCategory();
    }

    @Override
    public void setMarineLitterSizeCategory(CaracteristicQualitativeValue marineLitterSizeCategory) {
        Object oldValue = getMarineLitterSizeCategory();
        editObject.setMarineLitterSizeCategory(marineLitterSizeCategory);
        firePropertyChange(PROPERTY_MARINE_LITTER_SIZE_CATEGORY, oldValue, marineLitterSizeCategory);
    }

    @Override
    public FishingOperation getFishingOperation() {
        return editObject.getFishingOperation();
    }

    @Override
    public void setFishingOperation(FishingOperation fishingOperation) {
        editObject.setFishingOperation(fishingOperation);
    }

    @Override
    public Float getWeight() {
        return editObject.getWeight();
    }

    @Override
    public void setWeight(Float weight) {
        Object oldValue = getWeight();
        editObject.setWeight(weight);
        firePropertyChange(PROPERTY_WEIGHT, oldValue, weight);
    }

    @Override
    public Integer getNumber() {
        return editObject.getNumber();
    }

    @Override
    public void setNumber(Integer number) {
        Object oldValue = getNumber();
        editObject.setNumber(number);
        firePropertyChange(PROPERTY_NUMBER, oldValue, number);
    }

    @Override
    public String getComment() {
        return editObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public Integer getRankOrder() {
        return editObject.getRankOrder();
    }

    @Override
    public void setRankOrder(Integer rankOrder) {
        editObject.setRankOrder(rankOrder);
    }

    //------------------------------------------------------------------------//
    //-- AttachmentModelAware                                               --//
    //------------------------------------------------------------------------//

    @Override
    public ObjectTypeCode getObjectType() {
        return ObjectTypeCode.BATCH;
    }

    @Override
    public Integer getObjectId() {
        return getIdAsInt();
    }

    @Override
    public List<Attachment> getAttachment() {
        return attachment;
    }

    @Override
    public void addAllAttachment(Collection<Attachment> attachments) {
        this.attachment.addAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        this.attachment.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAllAttachment(Collection<Attachment> attachments) {
        this.attachment.removeAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        this.attachment.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }
}
