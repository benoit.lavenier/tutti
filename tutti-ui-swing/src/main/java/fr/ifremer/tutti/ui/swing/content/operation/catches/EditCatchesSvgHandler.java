package fr.ifremer.tutti.ui.swing.content.operation.catches;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.catches.EnterWeightUI;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;
import fr.ifremer.tutti.util.Numbers;
import jaxx.runtime.JAXXUtil;
import org.apache.batik.bridge.UpdateManager;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.dom.svg.SVGOMRectElement;
import org.apache.batik.dom.svg.SVGOMTextElement;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.GVTTreeRendererAdapter;
import org.apache.batik.swing.gvt.GVTTreeRendererEvent;
import org.apache.batik.swing.svg.SVGUserAgentAdapter;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.bean.JavaBeanObjectUtil;
import org.nuiton.util.Resource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.svg.SVGRect;
import org.w3c.dom.svg.SVGStylable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/10/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class EditCatchesSvgHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditCatchesSvgHandler.class);

    protected Multimap<String, PropertyChangeListener> svgRelatedPropertyChangeListeners = HashMultimap.create();

    protected JSVGCanvas canvas;

    protected Document svgDocument;

    protected final TuttiUIContext context;

    protected final EditCatchesUI ui;

    protected final EditCatchesUIModel model;

    protected final SVGUserAgentAdapter ua = new SVGUserAgentAdapter() {
        @Override
        public void displayError(String message) {
            if (log.isErrorEnabled()) {
                log.error("Canvas error: " + message);
            }
        }

        @Override
        public void displayError(Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Canvas error", ex);
            }
        }
    };

    public EditCatchesSvgHandler(TuttiUIContext context, EditCatchesUI ui, EditCatchesUIModel model) {
        this.context = context;
        this.ui = ui;
        this.model = model;
    }


    public void initResumeSvg() {
        // remove all in case the previous canvas has not been removed
        ui.getSvgCanvasPanel().removeAll();

        try {
            String parser = XMLResourceDescriptor.getXMLParserClassName();
            SAXSVGDocumentFactory f = new SAXSVGDocumentFactory(parser);
            URL url = Resource.getURL("EcranResume.svg");
            svgDocument = f.createDocument(url.toString());

            // redefine user agent to avoid displaying errors about css
            canvas = new JSVGCanvas(ua, true, true);
            canvas.setBackground(Color.decode("#d6d9df"));
            canvas.setSize(new Dimension(1, 1));
            canvas.setMySize(new Dimension(1, 1));

            canvas.setRecenterOnResize(true);

            final TuttiConfiguration config = context.getConfig();

            final Color catchColor = config.getColorCatch();
            final Color speciesColor = config.getColorSpecies();
            final Color benthosColor = config.getColorBenthos();
            final Color speciesOrBenthosUnsortedComputedWeightInWarningColor = config.getColorSpeciesOrBenthosUnsortedComputedWeightInWarning();
            final Color marineLitterColor = config.getColorMarineLitter();

            canvas.addGVTTreeRendererListener(new GVTTreeRendererAdapter() {
                @Override
                public void gvtRenderingCompleted(GVTTreeRendererEvent gvtTreeRendererEvent) {
                    if (log.isDebugEnabled()) {
                        log.debug("gvtRenderingCompleted");
                    }
                    WeightUnit catchWeightUnit = model.getCatchWeightUnit();
                    WeightUnit speciesWeightUnit = config.getSpeciesWeightUnit();
                    WeightUnit benthosWeightUnit = config.getBenthosWeightUnit();
                    WeightUnit marineLitterWeightUnit = config.getMarineLitterWeightUnit();

                    initSvgField(n("tutti.editCatchBatch.field.catchTotalWeight"),
                                 CatchBatch.PROPERTY_CATCH_TOTAL_WEIGHT,
                                 CatchBatch.PROPERTY_CATCH_TOTAL_COMPUTED_WEIGHT,
                                 model.getCatchTotalComputedOrNotWeight(),
                                 catchWeightUnit,
                                 catchColor);

                    initSvgField(n("tutti.editCatchBatch.field.catchTotalSortedComputedWeight"),
                                 CatchBatch.PROPERTY_CATCH_TOTAL_SORTED_COMPUTED_WEIGHT, catchWeightUnit, catchColor);

                    initSvgField(n("tutti.editCatchBatch.field.catchTotalRejectedWeight"),
                                 CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT,
                                 CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_COMPUTED_WEIGHT,
                                 model.getCatchTotalRejectedComputedOrNotWeight(),
                                 catchWeightUnit,
                                 catchColor);
                    initSvgField(n("tutti.editCatchBatch.field.catchTotalSortedSortedComputedWeight"),
                                 CatchBatch.PROPERTY_CATCH_TOTAL_SORTED_SORTED_COMPUTED_WEIGHT, catchWeightUnit, catchColor);

                    initSvgField(n("tutti.editCatchBatch.field.speciesTotalSortedWeight"),
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT,
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_COMPUTED_WEIGHT,
                                 model.getSpeciesTotalSortedComputedOrNotWeight(),
                                 speciesWeightUnit,
                                 speciesColor);
                    initSvgField(n("tutti.editCatchBatch.field.speciesTotalSampleSortedComputedWeight"),
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT, speciesWeightUnit, speciesColor);

                    initSvgField(n("tutti.editCatchBatch.field.speciesTotalInertWeight"),
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT,
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_INERT_COMPUTED_WEIGHT,
                                 model.getSpeciesTotalInertComputedOrNotWeight(),
                                 speciesWeightUnit,
                                 null);
                    initSvgField(n("tutti.editCatchBatch.field.speciesTotalLivingNotItemizedWeight"),
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT,
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT,
                                 model.getSpeciesTotalLivingNotItemizedComputedOrNotWeight(),
                                 speciesWeightUnit,
                                 null);

                    initSvgField(n("tutti.editCatchBatch.field.benthosTotalSortedWeight"),
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT,
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_COMPUTED_WEIGHT,
                                 model.getBenthosTotalSortedComputedOrNotWeight(),
                                 benthosWeightUnit,
                                 benthosColor);

                    initSvgField(n("tutti.editCatchBatch.field.benthosTotalSampleSortedComputedWeight"),
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT, benthosWeightUnit, benthosColor);

                    initSvgField(n("tutti.editCatchBatch.field.benthosTotalInertWeight"),
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT,
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_INERT_COMPUTED_WEIGHT,
                                 model.getBenthosTotalInertComputedOrNotWeight(),
                                 benthosWeightUnit,
                                 null);

                    initSvgField(n("tutti.editCatchBatch.field.benthosTotalLivingNotItemizedWeight"),
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT,
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT,
                                 model.getBenthosTotalLivingNotItemizedComputedOrNotWeight(),
                                 benthosWeightUnit,
                                 null);

                    initSvgField(n("tutti.editCatchBatch.field.catchTotalUnsortedComputedWeight"),
                                 CatchBatch.PROPERTY_CATCH_TOTAL_UNSORTED_COMPUTED_WEIGHT, catchWeightUnit, catchColor);
                    initSvgField(n("tutti.editCatchBatch.field.speciesTotalUnsortedComputedWeight"),
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_UNSORTED_COMPUTED_WEIGHT, speciesWeightUnit, speciesColor);
                    initSvgField(n("tutti.editCatchBatch.field.benthosTotalUnsortedComputedWeight"),
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_UNSORTED_COMPUTED_WEIGHT, benthosWeightUnit, benthosColor);

                    addSvgRelatedPropertyChangeListener(null, new ChangeElementBackgroundColorPropertyChangeListener(
                            CatchBatch.PROPERTY_SPECIES_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                            Sets.newHashSet(CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT,
                                            CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_COMPUTED_WEIGHT,
                                            CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT,
                                            CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_COMPUTED_WEIGHT,
                                            CatchBatch.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT),
                            model1 -> {
                                boolean warning = model1.isSpeciesTotalUnsortedComputedWeightInWarning();
                                return warning ? speciesOrBenthosUnsortedComputedWeightInWarningColor : speciesColor;
                            }));

                    addSvgRelatedPropertyChangeListener(null, new ChangeElementBackgroundColorPropertyChangeListener(
                            CatchBatch.PROPERTY_BENTHOS_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                            Sets.newHashSet(CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT,
                                            CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_COMPUTED_WEIGHT,
                                            CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT,
                                            CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_COMPUTED_WEIGHT,
                                            CatchBatch.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT),
                            model1 -> {
                                boolean warning = model1.isBenthosTotalUnsortedComputedWeightInWarning();
                                return warning ? speciesOrBenthosUnsortedComputedWeightInWarningColor : benthosColor;
                            }));

                    initSvgField(n("tutti.editCatchBatch.field.speciesTotalComputedWeight"),
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT,
                                 speciesWeightUnit,
                                 speciesColor,
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_COMPUTED_WEIGHT,
                                 CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT);
                    initSvgField(n("tutti.editCatchBatch.field.benthosTotalComputedWeight"),
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_COMPUTED_WEIGHT,
                                 benthosWeightUnit,
                                 benthosColor,
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT,
                                 CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT);
                    initSvgField(n("tutti.editCatchBatch.field.marineLitterTotalWeight"),
                                 CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT,
                                 CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_COMPUTED_WEIGHT,
                                 model.getMarineLitterTotalComputedOrNotWeight(),
                                 marineLitterWeightUnit,
                                 marineLitterColor,
                                 CatchBatch.PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT,
                                 CatchBatch.PROPERTY_BENTHOS_TOTAL_COMPUTED_WEIGHT);

                    addSvgRelatedPropertyChangeListener(null, new RatioPropertyChangeListener("ratioSpeciesSampleSortedOverSpeciesSortedWeightLabel",
                                                                                              CatchBatch.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT,
                                                                                              CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT,
                                                                                              CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_COMPUTED_WEIGHT));
                    addSvgRelatedPropertyChangeListener(null, new RatioPropertyChangeListener("ratioBenthosSampleSortedOverBenthosSortedWeightLabel",
                                                                                              CatchBatch.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT,
                                                                                              CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT,
                                                                                              CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_COMPUTED_WEIGHT));
                    addSvgRelatedPropertyChangeListener(null, new RatioPropertyChangeListener("ratioSortedSortedOverSortedWeightLabel",
                                                                                              CatchBatch.PROPERTY_CATCH_TOTAL_SORTED_SORTED_COMPUTED_WEIGHT,
                                                                                              CatchBatch.PROPERTY_CATCH_TOTAL_SORTED_COMPUTED_WEIGHT,
                                                                                              CatchBatch.PROPERTY_CATCH_TOTAL_SORTED_COMPUTED_WEIGHT));

                    initSpeciesCount(n("tutti.editCatchBatch.field.speciesDistinctSortedSpeciesCount"),
                                     CatchBatch.PROPERTY_SPECIES_DISTINCT_SORTED_SPECIES_COUNT);
                    initSpeciesCount(n("tutti.editCatchBatch.field.benthosDistinctSortedSpeciesCount"),
                                     CatchBatch.PROPERTY_BENTHOS_DISTINCT_SORTED_SPECIES_COUNT);

                    initSpeciesCount(n("tutti.editCatchBatch.field.speciesDistinctUnsortedSpeciesCount"),
                                     CatchBatch.PROPERTY_SPECIES_DISTINCT_UNSORTED_SPECIES_COUNT);
                    initSpeciesCount(n("tutti.editCatchBatch.field.benthosDistinctUnsortedSpeciesCount"),
                                     CatchBatch.PROPERTY_BENTHOS_DISTINCT_UNSORTED_SPECIES_COUNT);

                    initTremieCarrouselField(n("tutti.editCatchBatch.field.catchTotalSortedCarousselWeight"),
                                             CatchBatch.PROPERTY_CATCH_TOTAL_SORTED_CAROUSSEL_WEIGHT, catchWeightUnit);
                    initTremieCarrouselField(n("tutti.editCatchBatch.field.catchTotalSortedTremisWeight"),
                                             CatchBatch.PROPERTY_CATCH_TOTAL_SORTED_TREMIS_WEIGHT, catchWeightUnit);

                    ui.getSvgCanvasPanel().add(canvas, BorderLayout.CENTER);
                }
            });

            canvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
            canvas.setDocument(svgDocument);

        } catch (IOException err) {
            if (log.isErrorEnabled()) {
                log.error("error while initializing the resume background", err);
            }
            context.getErrorHelper().showErrorDialog(t("tutti.editCatchBatch.svgLoading.error"), err);
        }
    }

    public void clearSVG() {

        for (String property : svgRelatedPropertyChangeListeners.keySet()) {
            for (PropertyChangeListener listener : svgRelatedPropertyChangeListeners.get(property)) {
                if (property != null) {
                    model.removePropertyChangeListener(property, listener);
                } else {
                    model.removePropertyChangeListener(listener);
                }
            }
        }
        svgRelatedPropertyChangeListeners.clear();

        if (canvas != null) {
            UpdateManager updateManager = canvas.getUpdateManager();
            if (updateManager != null ) {
                try {
                    updateManager.suspend();
                } catch (Exception e) {
                    // FIXME Should not come here, but still can happen...
                }
            }
            try {
                canvas.dispose();
            } catch (Exception e) {
                // FIXME Should not come here, but still can happen...
            }
            ui.getSvgCanvasPanel().remove(canvas);
        }
    }

    protected void initSvgField(String label, String computedProperty, WeightUnit weightUnit, Color bgColor, String... idsInGroup) {
        initSvgField(label, null, computedProperty, null, weightUnit, bgColor, idsInGroup);
    }

    protected void initSvgField(final String label,
                                String property,
                                String computedProperty,
                                final ComputableData<Float> computableData,
                                final WeightUnit weightUnit,
                                final Color bgColor,
                                final String... idsInGroup) {

        if (log.isDebugEnabled()) {
            log.debug("init " + property + " field");
        }

        final String notNullProperty = property != null ? property : computedProperty;

        OnDataOrComputedDataValueChangedListener listener = new OnDataOrComputedDataValueChangedListener(notNullProperty, computableData, weightUnit, idsInGroup);
        if (property != null) {
            addSvgRelatedPropertyChangeListener(property, listener);
        }
        if (computedProperty != null) {
            addSvgRelatedPropertyChangeListener(computedProperty, listener);
        }

        if (computableData != null) {
            // data or computed data value
            Element element = svgDocument.getElementById(property);
            EventTarget target = (EventTarget) element;
            target.addEventListener("click", new OnValueClickListener(computableData, property, weightUnit), false);
        }

        updateOnCanvas(() -> {

            SVGOMRectElement rectElement = (SVGOMRectElement) svgDocument.getElementById(notNullProperty + "Rect");
            SVGRect bbox = rectElement.getBBox();
            Float x = bbox.getX();

            SVGOMTextElement labelElement = (SVGOMTextElement) svgDocument.getElementById(notNullProperty + "Label");
            CSSStyleDeclaration style = labelElement.getStyle();
            if (computableData == null) {
                style.setProperty("font-style", "italic", null);
            }

            if (bgColor != null) {
                int colorBrightness = TuttiUIUtil.getColorBrightness(bgColor);
                String textColor = colorBrightness > 150 ? "#000000" : "#FFFFFF";
                style.setProperty("fill", textColor, null);
            }

            labelElement.setTextContent(weightUnit.decorateLabel(t(label)));
            bbox = labelElement.getBBox();
            float labelX = bbox.getX();
            float width = Math.abs(x - labelX) + 10;

            SVGOMRectElement labelRectElement = (SVGOMRectElement) svgDocument.getElementById(notNullProperty + "LabelRect");
            if (labelRectElement != null) {
                float actualWidth = Math.abs(labelRectElement.getBBox().getX() - labelX) + 10;

                if (actualWidth < width) {
                    labelRectElement.setAttribute("width", Float.toString(width));
                    labelRectElement.setAttribute("x", String.valueOf(labelX - 10));

                    for (String id : idsInGroup) {
                        Element el = svgDocument.getElementById(id + "LabelRect");
                        el.setAttribute("width", Float.toString(width));
                        el.setAttribute("x", String.valueOf(labelX - 10));
                    }
                }

                style = labelRectElement.getStyle();
                String hexaColor = "#" + Integer.toHexString(bgColor.getRGB()).substring(2);
                style.setProperty("fill", hexaColor, null);
            }

            Float value;
            boolean computed;
            if (computableData == null) {
                computed = true;
                value = getModelPropertyValue(notNullProperty);

            } else if (computableData.getData() == null) {
                computed = true;
                value = computableData.getComputedData();

            } else {
                computed = false;
                value = computableData.getData();
            }

            updateValue(notNullProperty, value, weightUnit, computed, idsInGroup);
        });

    }

    protected void updateOnCanvas(Runnable runnable) {
        try {
            canvas.getUpdateManager().getUpdateRunnableQueue().invokeLater(runnable);
        } catch (IllegalStateException e) {
            if (log.isErrorEnabled()) {
                log.error("error while updating canvas, reload it");
            }
            clearSVG();
            initResumeSvg();
        }
    }

    protected void addSvgRelatedPropertyChangeListener(String property, PropertyChangeListener listener) {
        svgRelatedPropertyChangeListeners.put(property, listener);
        if (property != null) {
            model.addPropertyChangeListener(property, listener);
        } else {
            model.addPropertyChangeListener(listener);
        }
    }

    protected void updateValue(final String property,
                               final Float value,
                               final WeightUnit weightUnit,
                               final boolean computed,
                               final String... idsInGroup) {

        updateOnCanvas(() -> {
            if (log.isDebugEnabled()) {
                log.debug("update " + property + " field");
            }

            SVGOMTextElement valueElement = (SVGOMTextElement) svgDocument.getElementById(property + "Value");
            if (valueElement == null) {
                return;
            }

            //TODO i18n ?
            String textContent;
            if (value != null) {
                textContent = weightUnit.renderWeight(value) + " " + weightUnit.getShortLabel();
            } else {
                textContent = null;
            }
            valueElement.setTextContent(textContent);

            CSSStyleDeclaration style = valueElement.getStyle();

            Color colorComputedWeights = context.getConfig().getColorComputedWeights();
            String computedColor = "#" + Integer.toHexString(colorComputedWeights.getRGB()).substring(2);
            style.setProperty("fill", computed ? computedColor : "#000000", null);
            style.setProperty("font-style", computed ? "italic" : "normal", null);

            SVGRect bbox = valueElement.getBBox();
            SVGOMRectElement rectElement = (SVGOMRectElement) svgDocument.getElementById(property + "Rect");
            if (bbox != null && rectElement != null) {
                float width = bbox.getWidth() + 15;
                float actualWidth = rectElement.getBBox().getWidth();
                if (actualWidth < width) {
                    rectElement.setAttribute("width", Float.toString(width));
                    for (String id : idsInGroup) {
                        Element el = svgDocument.getElementById(id + "Rect");
                        el.setAttribute("width", Float.toString(width));
                    }
                }
            }
        });

    }

    protected Float getModelPropertyValue(String property) {
        Float value;
        try {
            String sValue = BeanUtils.getProperty(model, property);
            if (sValue != null) {
                value = Float.parseFloat(sValue);
            } else {
                value = null;
            }

        } catch (ReflectiveOperationException e) {
            if (log.isErrorEnabled()) {
                log.error("error on reading model's property " + property, e);
            }
            value = null;
        }
        return value;
    }

    protected void initSpeciesCount(final String label, final String property) {

        if (log.isDebugEnabled()) {
            log.debug("init " + property + " field");
        }

        updateOnCanvas(() -> {
            Element labelElement = svgDocument.getElementById(property + "Label");
            labelElement.setTextContent(t(label));

            Element valueElement = svgDocument.getElementById(property + "Value");
            Integer value;
            try {
                String sValue = BeanUtils.getProperty(model, property);
                if (sValue != null) {
                    value = Integer.parseInt(sValue);
                } else {
                    value = null;
                }

            } catch (ReflectiveOperationException e) {
                if (log.isErrorEnabled()) {
                    log.error("error on reading model's property " + property, e);
                }
                value = null;
            }
            valueElement.setTextContent(JAXXUtil.getStringValue(value));
        });

        addSvgRelatedPropertyChangeListener(property, evt -> {
            final Integer value = (Integer) evt.getNewValue();
            updateOnCanvas(() -> {
                Element labelElement = svgDocument.getElementById(property + "Value");
                labelElement.setTextContent(JAXXUtil.getStringValue(value));
            });
        });
    }

    protected void initTremieCarrouselField(final String label, final String property, final WeightUnit weightUnit) {
        if (log.isDebugEnabled()) {
            log.debug("init " + property + " field");
        }

        updateOnCanvas(() -> {
            Element labelElement = svgDocument.getElementById(property + "Label");
            labelElement.setTextContent(weightUnit.decorateLabel(t(label)));
        });

        addSvgRelatedPropertyChangeListener(property, evt -> {
            Float value = (Float) evt.getNewValue();
            updateValue(property, value, weightUnit, true);
        });

        Float value = getModelPropertyValue(property);
        updateValue(property, value, weightUnit, true);

        addSvgRelatedPropertyChangeListener(EditCatchesUIModel.PROPERTY_FISHING_OPERATION, evt -> updateTremieCarrouselVisibility(property));
        updateTremieCarrouselVisibility(property);
    }


    protected void updateTremieCarrouselVisibility(final String property) {
        FishingOperation fishingOperation = model.getFishingOperation();
        if (fishingOperation != null) {
            final boolean tremieCarrouselFieldsVisisble = fishingOperation.getVessel() != null
                                                          && fishingOperation.getVessel().getId().equals(context.getConfig().getTremieCarousselVesselId());

            updateOnCanvas(() -> {
                Element labelElement = svgDocument.getElementById(property + "Label");
                labelElement.setAttribute("visibility", tremieCarrouselFieldsVisisble ? "visible" : "hidden");

                Element valueElement = svgDocument.getElementById(property + "Value");
                valueElement.setAttribute("visibility", tremieCarrouselFieldsVisisble ? "visible" : "hidden");
            });
        }
    }

    private class OnValueClickListener implements EventListener {

        private final ComputableData<Float> computableData;

        private final String property;

        private final WeightUnit weightUnit;

        public OnValueClickListener(ComputableData<Float> computableData, String property, WeightUnit weightUnit) {
            this.computableData = computableData;
            this.property = property;
            this.weightUnit = weightUnit;
        }

        public void handleEvent(org.w3c.dom.events.Event evt) {
            log.info("element clicked");
            EnterWeightUI dialog = new EnterWeightUI(context);
            Float originalWeight = computableData.getData();
            Float weight = dialog.openAndGetWeightValue(t("tutti.editCatchBatch.field." + property),
                                                        originalWeight,
                                                        weightUnit);

            if (!Objects.equals(originalWeight, weight)) {

                computableData.setData(weight);
            }
        }
    }

    private class OnDataOrComputedDataValueChangedListener implements PropertyChangeListener {

        private final String property;

        private final ComputableData<Float> computableData;

        private final WeightUnit weightUnit;

        private final String[] idsInGroup;

        public OnDataOrComputedDataValueChangedListener(String property,
                                                        ComputableData<Float> computableData,
                                                        WeightUnit weightUnit,
                                                        String... idsInGroup) {
            this.property = property;
            this.computableData = computableData;
            this.weightUnit = weightUnit;
            this.idsInGroup = idsInGroup;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            boolean computedData;
            Float newValue;

            if (computableData == null) {
                computedData = true;
                newValue = (Float) evt.getNewValue();

            } else {
                if (computableData.getData() == null) {
                    computedData = true;
                    newValue = computableData.getComputedData();

                } else {
                    computedData = false;
                    newValue = computableData.getData();
                }
            }
            updateValue(property, newValue, weightUnit, computedData, idsInGroup);
        }

    }

    private class ChangeElementBackgroundColorPropertyChangeListener implements PropertyChangeListener {

        private final String elementId;

        private Set<String> propertiesToListen;

        private Function<EditCatchesUIModel, Color> colorFunction;

        public ChangeElementBackgroundColorPropertyChangeListener(String elementId,
                                                                  Set<String> propertiesToListen,
                                                                  Function<EditCatchesUIModel, Color> colorFunction) {
            this.elementId = elementId;
            this.propertiesToListen = propertiesToListen;
            this.colorFunction = colorFunction;

            updateColor();
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (propertiesToListen.contains(evt.getPropertyName())) {
                updateColor();
            }
        }

        protected void updateColor() {
            updateOnCanvas(() -> {
                if (log.isDebugEnabled()) {
                    log.debug("update " + elementId + " field");
                }

                Element rectElement = svgDocument.getElementById(elementId + "LabelRect");
                SVGStylable field = (SVGStylable) rectElement;
                CSSStyleDeclaration style = field.getStyle();

                Color background = colorFunction.apply(model);
                String color = "#" + Integer.toHexString(background.getRGB()).substring(2);
                style.setProperty("fill", color, null);

                SVGOMTextElement labelElement = (SVGOMTextElement) svgDocument.getElementById(elementId + "Label");
                CSSStyleDeclaration labelStyle = labelElement.getStyle();

                int colorBrightness = TuttiUIUtil.getColorBrightness(background);
                String textColor = colorBrightness > 150 ? "#000000" : "#FFFFFF";
                labelStyle.setProperty("fill", textColor, null);
            });
        }
    }

    private class RatioPropertyChangeListener implements PropertyChangeListener {

        private final String elementId;

        private final String numeratorProperty;

        private final String denominatorProperty;

        private final String denominatorComputedProperty;

        private Integer ratio = null;

        private final Set<String> propertyNames;

        public RatioPropertyChangeListener(String elementId,
                                           String numeratorProperty,
                                           String denominatorProperty,
                                           String denominatorComputedProperty) {
            this.elementId = elementId;
            this.numeratorProperty = numeratorProperty;
            this.denominatorProperty = denominatorProperty;
            this.denominatorComputedProperty = denominatorComputedProperty;
            this.propertyNames = ImmutableSet.of(numeratorProperty, denominatorProperty, denominatorComputedProperty);
            updateRatioText();
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propertyName = evt.getPropertyName();

            if (propertyNames.contains(propertyName)) {
                updateRatioText();
            }
        }

        protected void updateRatioText() {

//            try {
//
//                String numerator = BeanUtils.getProperty(model, numeratorProperty);
//                String denominator = BeanUtils.getProperty(model, denominatorProperty);
//                if (denominator == null) {
//                    denominator = BeanUtils.getProperty(model, denominatorComputedProperty);
//                }
//
//                if (numerator != null && denominator != null) {
//                    Float numeratorValue = Float.valueOf(numerator);
//                    Float denominatorValue = Float.valueOf(denominator);
//
//                    if (denominatorValue != 0) {
//                        ratio = Numbers.roundToInt(100f * numeratorValue / denominatorValue);
//                    }
//                }
//
//            } catch (ReflectiveOperationException e) {
//                if (log.isErrorEnabled()) {
//                    log.error("Error while computing the ratio", e);
//                }
//            }

            Float  numerator = (Float) JavaBeanObjectUtil.getProperty(model, numeratorProperty);
            Float  denominator = (Float) JavaBeanObjectUtil.getProperty(model, denominatorProperty);
            if (denominator == null) {
                denominator = (Float) JavaBeanObjectUtil.getProperty(model, denominatorComputedProperty);
            }
            if (numerator == null || WeightUnit.KG.isNullOrZero(denominator)) {
                ratio = null;
            } else {
                ratio = Numbers.roundToInt(100f * numerator / denominator);
            }

            updateOnCanvas(() -> {
                if (log.isDebugEnabled()) {
                    log.debug("update " + elementId + " field");
                }

                Element ratioElement = svgDocument.getElementById(elementId);
                String textContent;
                if (ratio != null) {
                    textContent = ratio + "%";
                } else {
                    textContent = null;
                }
                ratioElement.setTextContent(textContent);
            });
        }
    }

}
