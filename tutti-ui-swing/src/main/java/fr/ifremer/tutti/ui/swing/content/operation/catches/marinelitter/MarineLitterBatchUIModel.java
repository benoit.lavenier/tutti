package fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.AbstractTuttiBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class MarineLitterBatchUIModel extends AbstractTuttiBatchUIModel<MarineLitterBatchRowModel, MarineLitterBatchUIModel>
        implements TabContentModel {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_REMOVE_BATCH_ENABLED = "removeBatchEnabled";

    /**
     * Can user remove a selected marineLitter batch?
     *
     * @since 1.3
     */
    protected boolean removeBatchEnabled;

    private final WeightUnit weightUnit;

    public MarineLitterBatchUIModel(EditCatchesUIModel catchesUIModel, WeightUnit weightUnit) {
        super(catchesUIModel, EditCatchesUIModel.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT);
        this.weightUnit = weightUnit;
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }

    public ComputableData<Float> getMarineLitterTotalComputedOrNotWeight() {
        return catchesUIModel.getMarineLitterTotalComputedOrNotWeight();
    }

    public Float getMarineLitterTotalWeight() {
        return catchesUIModel.getMarineLitterTotalWeight();
    }

    public void setMarineLitterTotalWeight(Float marineLitterTotalWeight) {
        catchesUIModel.setMarineLitterTotalWeight(marineLitterTotalWeight);
    }

    public Float getMarineLitterTotalComputedWeight() {
        return catchesUIModel.getMarineLitterTotalComputedWeight();
    }

    public void setMarineLitterTotalComputedWeight(Float marineLitterTotalWeight) {
        catchesUIModel.setMarineLitterTotalComputedWeight(marineLitterTotalWeight);
    }

    public boolean isRemoveBatchEnabled() {
        return removeBatchEnabled;
    }

    public void setRemoveBatchEnabled(boolean removeBatchEnabled) {
        this.removeBatchEnabled = removeBatchEnabled;
        firePropertyChange(PROPERTY_REMOVE_BATCH_ENABLED, null, removeBatchEnabled);
    }

    public Multimap<CaracteristicQualitativeValue, CaracteristicQualitativeValue> getMarineLitterCategoriesUsed() {
        return catchesUIModel.getMarineLitterCategoriesUsed();
    }

    //------------------------------------------------------------------------//
    //-- TabContentModel                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public boolean isEmpty() {
        boolean result = getMarineLitterTotalWeight() == null;
        if (result && CollectionUtils.isNotEmpty(getRows())) {

            // check if every line is not valid
            for (MarineLitterBatchRowModel row : rows) {
                if (row.isValid()) {

                    // found a valid row so not empty
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public String getTitle() {
        return n("tutti.label.tab.marineLitter");
    }

    @Override
    public String getIcon() {
        return null;
    }

    @Override
    public boolean isCloseable() {
        return false;
    }
}
