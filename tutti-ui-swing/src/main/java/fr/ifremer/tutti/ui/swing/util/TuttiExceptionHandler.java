package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.actions.AbstractChangeScreenAction;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.jaxx.application.swing.action.ApplicationActionException;
import org.nuiton.jaxx.application.swing.util.ApplicationErrorHelper;
import org.nuiton.jaxx.application.swing.util.ApplicationExceptionHandler;

import java.beans.PropertyVetoException;

/**
 * Tutti global exception handler.
 *
 * Catch all application uncaught and display it in a custom JoptionPane
 * or JXErrorPane.
 *
 * See http://stackoverflow.com/a/4448569/1165234 for details.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TuttiExceptionHandler extends ApplicationExceptionHandler {

    private static final Log log =
            LogFactory.getLog(TuttiExceptionHandler.class);

    public TuttiExceptionHandler(ApplicationErrorHelper errorHelper) {
        super(errorHelper);
    }

    @Override
    public void uncaughtException(Thread t, Throwable ex) {
        if (t.getName().startsWith("AWT-EventQueue-") || ex.getCause() instanceof PropertyVetoException) {

            // Swallow some ui execption we can't deal with
            // See https://forge.codelutin.com/issues/7489
            if (log.isErrorEnabled()) {
                log.error("Swallow Swing error: " + ex.getMessage(), ex);
            }

        } else {

            super.uncaughtException(t, ex);
        }
    }

    @Override
    protected void handleException(String tname, Throwable ex) {
        if (log.isErrorEnabled()) {
            log.error("Global application exception [" + tname + "]", ex);
        }

        Throwable cause = getCause(ex);

        boolean backToScreen = false;

        LongActionSupport action = null;

        if (cause instanceof ApplicationActionException) {

            ApplicationActionException actionException = (ApplicationActionException) cause;
            cause = cause.getCause();

            if (log.isDebugEnabled()) {
                log.debug("Action error cause:", cause);
            }

            action = (LongActionSupport) actionException.getAction();

            if (action instanceof AbstractChangeScreenAction) {
                backToScreen = true;
            }
        }

        showErrorDialog(cause.getMessage(), cause);

        if (backToScreen) {

            action.getContext().setFallBackScreen();
        }
    }

    @Override
    protected Throwable getCause(Throwable ex) {


        Throwable cause = super.getCause(ex);

        if (cause == null) {

            if (ex instanceof ApplicationTechnicalException) {
                cause = ex;
            }
        }

        return cause;

    }
}
