package fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.Strata;
import fr.ifremer.tutti.persistence.entities.protocol.Stratas;

import java.util.Enumeration;

/**
 * Pour définir une strate contenue dans une zone.
 *
 * Le parent d'un tel nœud est toujours une zone. Même dans l'arbre des disponibles le nœud root sera alors une zone fictive.
 *
 * @author Kevin Morin (Code Lutin)
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class StrataNode extends ZoneEditorNodeSupport {

    public StrataNode(String id, String label) {
        super(id, label, true);
    }

    @Override
    public ZoneNode getParent() {
        return (ZoneNode) super.getParent();
    }

    @Override
    public Enumeration<SubStrataNode> children() {
        return super.children();
    }

    public Strata toBean() {

        Strata strata = Stratas.newStrata();
        strata.setId(getId());
        Enumeration<SubStrataNode> children = children();
        while (children.hasMoreElements()) {
            SubStrataNode subStrataNode = children.nextElement();
            strata.addSubstrata(subStrataNode.toBean());
        }
        return strata;

    }
}
