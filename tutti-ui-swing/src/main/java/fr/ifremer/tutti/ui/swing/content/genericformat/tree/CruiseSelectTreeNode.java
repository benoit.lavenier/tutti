package fr.ifremer.tutti.ui.swing.content.genericformat.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterators;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.OperationDataModel;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class CruiseSelectTreeNode extends DataSelectTreeNodeSupport<CruiseDataModel> implements Iterable<OperationSelectTreeNode> {

    private static final long serialVersionUID = 1L;

    private boolean selected;

    private final int nbChilds;

    private int nbChildSelected;

    private boolean objectValueIsAdjusting;

    public CruiseSelectTreeNode(CruiseDataModel userObject) {
        super(userObject);

        for (OperationDataModel operation : userObject) {

            OperationSelectTreeNode operationNode = new OperationSelectTreeNode(operation);
            add(operationNode);

        }

        nbChilds = userObject.size();

    }

    public boolean isPartialSelected() {
        return !selected && nbChildSelected > 0;
    }

    public int getNbChilds() {
        return nbChilds;
    }

    public int getNbChildSelected() {
        return nbChildSelected;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {

        objectValueIsAdjusting = true;
        this.selected = selected;
        try {

            for (OperationSelectTreeNode o : this) {
                o.setSelected(selected);
            }

        } finally {

            objectValueIsAdjusting = false;

        }

        updateSelectedSate();

    }

    @Override
    public CruiseDataModel getSelectedDataModel() {

        CruiseDataModel result;

        if (isSelected() || isPartialSelected()) {

            Set<OperationDataModel> operations = new LinkedHashSet<>();

            for (OperationSelectTreeNode o : this) {
                OperationDataModel operation = o.getSelectedDataModel();
                if (operation != null) {
                    operations.add(operation);
                }
            }
            result = new CruiseDataModel(getId(), getLabel(), operations);

        } else {
            result = null;
        }

        return result;

    }

    @Override
    public Iterator<OperationSelectTreeNode> iterator() {
        return Iterators.forEnumeration(children());
    }

    public void updateSelectedSate() {

        if (!objectValueIsAdjusting && nbChilds > 0) {

            nbChildSelected = 0;
            for (OperationSelectTreeNode o : this) {
                if (o.isSelected()) {
                    nbChildSelected++;
                }
            }

            if (selected) {

                if (nbChildSelected < nbChilds) {
                    selected = false;
                }

            } else if (nbChildSelected == nbChilds) {
                selected = true;
            }

        }

    }

    public boolean isSelectedDataExists() {

        boolean result = false;
        if (isSelected() || isPartialSelected()) {
            result = getOptionalId() != null;
        }
        return result;

    }
}
