package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CalcifiedPiecesSamplingEditorTableModel extends AbstractApplicationTableModel<CalcifiedPiecesSamplingEditorRowModel> {

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> SPECIES = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_PROTOCOL_SPECIES,
            n("tutti.editCps.table.header.species.field"),
            n("tutti.editCps.table.header.species.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> MATURITY = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_MATURITY,
            n("tutti.editCps.table.header.maturity.field"),
            n("tutti.editCps.table.header.maturity.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> SEX = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_SEX,
            n("tutti.editCps.table.header.sex.field"),
            n("tutti.editCps.table.header.sex.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> MIN_SIZE = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_MIN_SIZE,
            n("tutti.editCps.table.header.minSize.field"),
            n("tutti.editCps.table.header.minSize.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> MAX_SIZE = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_MAX_SIZE,
            n("tutti.editCps.table.header.maxSize.field"),
            n("tutti.editCps.table.header.maxSize.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> MAX_BY_LENGHT_STEP = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_MAX_BY_LENGHT_STEP,
            n("tutti.editCps.table.header.maxByLengthStep.field"),
            n("tutti.editCps.table.header.maxByLengthStep.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> SAMPLING_INTERVAL = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_SAMPLING_INTERVAL,
            n("tutti.editCps.table.header.samplingInterval.field"),
            n("tutti.editCps.table.header.samplingInterval.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> OPERATION_LIMITATION = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_OPERATION_LIMITATION,
            n("tutti.editCps.table.header.operationLimitation.field"),
            n("tutti.editCps.table.header.operationLimitation.field.tip"));

    public static final ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> ZONE_LIMITATION = ColumnIdentifier.newId(
            CalcifiedPiecesSamplingEditorRowModel.PROPERTY_ZONE_LIMITATION,
            n("tutti.editCps.table.header.zoneLimitation.field"),
            n("tutti.editCps.table.header.zoneLimitation.field.tip"));


    public CalcifiedPiecesSamplingEditorTableModel(TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        setNoneEditableCols(SPECIES, MAX_SIZE, MATURITY);
    }

    @Override
    public CalcifiedPiecesSamplingEditorRowModel createNewRow() {
        return new CalcifiedPiecesSamplingEditorRowModel();
    }

    public CalcifiedPiecesSamplingEditorRowModel createNewRow(EditProtocolSpeciesRowModel species,
                                                              Boolean maturity,
                                                              boolean sex) {
        return createNewRow(species, maturity,sex, 0, null);
    }

    public CalcifiedPiecesSamplingEditorRowModel createNewRow(EditProtocolSpeciesRowModel species,
                                                              Boolean maturity,
                                                              boolean sex,
                                                              Integer minSize,
                                                              Integer maxSize) {

        CalcifiedPiecesSamplingEditorRowModel newRow = createNewRow();
        newRow.setValid(true);
        newRow.setProtocolSpecies(species);
        newRow.setMaturity(maturity);
        newRow.setSex(sex);
        newRow.setMinSize(minSize);
        newRow.setMaxSize(maxSize);
        newRow.setMaxByLenghtStep(0);
        newRow.setSamplingInterval(0);
        newRow.setOperationLimitation(0);
        newRow.setZoneLimitation(0);

        return newRow;
    }

    /**
     * Return the list of used protocolSpecies in the table (used to fill the
     * comparator cache for protocolSpecies sort)
     *
     * @return the list of used protocolSpecies in the table.
     * @since 2.8
     */
    public List<EditProtocolSpeciesRowModel> getSpeciesList() {
        List<EditProtocolSpeciesRowModel> result = Lists.newArrayList();
        for (CalcifiedPiecesSamplingEditorRowModel row : rows) {
            result.add(row.getProtocolSpecies());
        }
        return result;
    }

    @Override
    protected boolean isCellEditable(int rowIndex,
                                     int columnIndex,
                                     ColumnIdentifier<CalcifiedPiecesSamplingEditorRowModel> propertyName) {
        boolean result = super.isCellEditable(rowIndex, columnIndex, propertyName);

        if (result && MIN_SIZE.equals(propertyName)) {
            int minSize = (int) getValueAt(rowIndex, columnIndex);
            result = minSize > 0;
        }
        return result;
    }

    public boolean isSpeciesOrderEven(int rowIndex) {

        CalcifiedPiecesSamplingEditorRowModel selectedRow = getEntry(rowIndex);
        Integer selectedSpeciesReferenceTaxonId = selectedRow.getProtocolSpecies().getSpeciesReferenceTaxonId();

        Set<Integer> speciesReferenceTaxonIds = new LinkedHashSet<>();
        for (CalcifiedPiecesSamplingEditorRowModel row : rows) {
            Integer speciesReferenceTaxonId = row.getProtocolSpecies().getSpeciesReferenceTaxonId();
            speciesReferenceTaxonIds.add(speciesReferenceTaxonId);
            if (Objects.equals(selectedSpeciesReferenceTaxonId, speciesReferenceTaxonId)) {
                break;
            }
        }
        int nbSpecies = speciesReferenceTaxonIds.size();

        // Si on a trouvé des espèces, on doit alors incrémenter de 1 car l'espèce «0» est en fait la première trouvée ^^
        return nbSpecies == 0 || ((nbSpecies + 1) % 2 == 0);

    }

}
