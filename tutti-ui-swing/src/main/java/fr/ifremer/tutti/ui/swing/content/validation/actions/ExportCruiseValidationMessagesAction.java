package fr.ifremer.tutti.ui.swing.content.validation.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.catches.ValidateCruiseOperationsService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUI;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUIModel;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.0.1
 */
public class ExportCruiseValidationMessagesAction extends LongActionSupport<ValidateCruiseUIModel, ValidateCruiseUI, ValidateCruiseUIHandler> {

    protected File file;

    /** Validation service. */
    protected ValidateCruiseOperationsService validationService = getContext().getValidateCruiseOperationsService();

    public ExportCruiseValidationMessagesAction(ValidateCruiseUIHandler handler) {
        super(handler, false);
        setActionDescription(t("tutti.validateCruise.action.export.all.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {
            // choose file to export
            file = saveFile(
                    String.format("validation_%s", getDataContext().getCruise().getName()),
                    "txt",
                    t("tutti.validateCruise.action.export.all.chooseFile.title"),
                    t("tutti.validateCruise.action.export.all.chooseFile.label")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        ValidateCruiseUIModel uiModel = getModel();
        validationService.exportValidationResults(file, uiModel.getValidator());
    }

    @Override
    public void releaseAction() {
        super.releaseAction();
        file = null;
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.validateCruise.action.export.all.success", file));
    }
}
