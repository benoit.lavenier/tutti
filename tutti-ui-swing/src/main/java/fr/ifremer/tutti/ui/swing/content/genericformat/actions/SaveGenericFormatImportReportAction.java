package fr.ifremer.tutti.ui.swing.content.genericformat.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUI;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUIHandler;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUIModel;
import org.nuiton.jaxx.application.ApplicationIOUtil;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/25/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class SaveGenericFormatImportReportAction extends LongActionSupport<GenericFormatImportUIModel, GenericFormatImportUI, GenericFormatImportUIHandler> {

    private static final DateFormat DF = new SimpleDateFormat("yyy-MM-dd-hh-mm");

    private File targetFile;

    public SaveGenericFormatImportReportAction(GenericFormatImportUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            doAction = getModel().getImportReportFile() != null && getModel().getImportReportFile().exists();

        }

        if (doAction) {

            targetFile = saveFileWithStartDirectory(
                    getConfig().getGenericFormatReportBackupDirectory(),
                    false,
                    "import-report-" + getModel().getProgram().getId() + "-" + DF.format(new Date()),
                    "pdf",
                    t("tutti.genericFormat.title.choose.saveImportReportfile"),
                    t("tutti.genericFormat.action.chooseImportReportFile"),
                    "^.+\\.pdf", t("tutti.common.file.pdf"));

            doAction = targetFile != null;

        }

        return doAction;

    }

    @Override
    public void doAction() throws Exception {

        File reportFile = getModel().getImportReportFile();
        ApplicationIOUtil.copyFile(reportFile, targetFile, t("tutti.io.error.copyFile"));
        sendMessage(t("tutti.genericFormat.importReportFile.saved", targetFile));

    }

    @Override
    protected void releaseAction() {

        targetFile = null;
        super.releaseAction();

    }
}
