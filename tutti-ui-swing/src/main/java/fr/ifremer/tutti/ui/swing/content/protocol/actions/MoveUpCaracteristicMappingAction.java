package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;

/**
 * Created on 12/22/15.
 *
 * @author Kevin Morin - morin@codelutin.com
 * @since 4.2
 */
public class MoveUpCaracteristicMappingAction extends SimpleActionSupport<EditProtocolUI> {

    private static final long serialVersionUID = 1L;

    public MoveUpCaracteristicMappingAction(EditProtocolUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(EditProtocolUI ui) {

        int selectedRow = ui.getCaracteristicsMappingTable().getSelectedRow();
        Preconditions.checkState(selectedRow > -1);

        int newRow = selectedRow - 1;

        ui.getHandler().permuteCaracteristics(newRow, selectedRow);

//        ui.getHandler().getCaracteristicMappingTableModel().permuteEntry(newRow, selectedRow);
//        ui.getCaracteristicsMappingTable().getSelectionModel().setSelectionInterval(newRow, newRow);
//        ui.getModel().setModify(true);

    }

}