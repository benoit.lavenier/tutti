package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.TuttiDecorator;
import org.nuiton.decorator.JXPathDecorator;

import java.util.List;

/**
 * To decorate a {@link SpeciesBatch} as a species + keeping the logic of batchs childs.
 *
 * first sort on species, then always keep the row index order.
 *
 * Created on 10/11/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.8
 */
public class SpeciesBatchDecoratorComparator<R extends SpeciesBatch> extends TuttiDecorator.TuttiDecoratorComparator<R> {

    class RowComparatorContext implements Comparable<RowComparatorContext> {

        private final String speciesText;

        private final int rowIndex;

        RowComparatorContext(String speciesText,
                             int rowIndex) {
            this.speciesText = speciesText;
            this.rowIndex = rowIndex;
        }

        @Override
        public int compareTo(RowComparatorContext o) {
            // first compare on speciesText
            int result = sortSign * speciesText.compareTo(o.speciesText);
            if (result == 0) {

                // respect natural order
                result = rowIndex - o.rowIndex;
            }
            return result;
        }
    }

    private static final long serialVersionUID = 1L;

    SpeciesSortMode speciesSortMode;

    int sortSign;

    public void setSpeciesSortMode(SpeciesSortMode speciesSortMode) {
        this.speciesSortMode = speciesSortMode;
        if (speciesSortMode == SpeciesSortMode.DESC) {
            sortSign = -1;
        } else {
            sortSign = 1;
        }
    }

    public SpeciesBatchDecoratorComparator(String expression) {
        super(expression);
    }

    @Override
    public void init(JXPathDecorator<R> decorator, List<R> datas) {
        clear();

        int index = 0;
        for (R data : datas) {

            String speciesText = decorator.toString(data);
            Comparable value = new RowComparatorContext(speciesText, index++);
            valueCache.put(data, value);

        }
    }
}
