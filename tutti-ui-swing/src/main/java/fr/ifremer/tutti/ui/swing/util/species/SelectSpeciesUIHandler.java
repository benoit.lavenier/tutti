package fr.ifremer.tutti.ui.swing.util.species;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SelectSpeciesUIHandler extends AbstractTuttiUIHandler<SelectSpeciesUIModel, SelectSpeciesUI> {

    private static final Log log =
            LogFactory.getLog(SelectSpeciesUIHandler.class);

    @Override
    public void beforeInit(SelectSpeciesUI ui) {
        super.beforeInit(ui);
        SelectSpeciesUIModel model = new SelectSpeciesUIModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(SelectSpeciesUI ui) {
        initUI(ui);

        String context = ui.useSurveyCode ? DecoratorService.WITH_SURVEY_CODE : null;

        initBeanFilterableComboBox(ui.getSpeciesCombo(),
                                   Lists.<Species>newArrayList(),
                                   null,
                                   context);

//        initBeanFilterableComboBox(ui.getSpeciesCombo(),
//                                   Lists.<Species>newArrayList(),
//                                   null,
//                                   DecoratorService.WITH_SURVEY_CODE);

        getModel().addPropertyChangeListener(
                SelectSpeciesUIModel.PROPERTY_SPECIES,
                evt -> SelectSpeciesUIHandler.this.ui.getSpeciesCombo().getHandler().sortData());

    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getSpeciesCombo();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
    }

    @Override
    public SwingValidator<SelectSpeciesUIModel> getValidator() {
        return null;
    }

}
