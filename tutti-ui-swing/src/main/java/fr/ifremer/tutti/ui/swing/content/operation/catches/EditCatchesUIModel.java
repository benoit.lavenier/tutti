package fr.ifremer.tutti.ui.swing.content.operation.catches;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class EditCatchesUIModel extends AbstractTuttiBeanUIModel<CatchBatch, EditCatchesUIModel> implements AttachmentModelAware, TabContentModel, CatchBatch {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VALIDATION_CONTEXT = "validationContext";

    public static final String PROPERTY_CATCH_BATCH = "catchBatch";

    protected static final Binder<CatchBatch, EditCatchesUIModel> fromBeanBinder = BinderFactory.newBinder(CatchBatch.class, EditCatchesUIModel.class);

    protected static final Binder<EditCatchesUIModel, CatchBatch> toBeanBinder = BinderFactory.newBinder(EditCatchesUIModel.class, CatchBatch.class);

    protected Float speciesTotalComputedWeight;

    protected ComputableData<Float> speciesTotalSortedComputedOrNotWeight = new ComputableData<>();

    protected Float speciesTotalUnsortedComputedWeight;

    protected Float speciesTotalSampleSortedComputedWeight;

    protected ComputableData<Float> speciesTotalInertComputedOrNotWeight = new ComputableData<>();

    protected ComputableData<Float> speciesTotalLivingNotItemizedComputedOrNotWeight = new ComputableData<>();

    protected Float benthosTotalComputedWeight;

    protected ComputableData<Float> benthosTotalSortedComputedOrNotWeight = new ComputableData<>();

    protected Float benthosTotalUnsortedComputedWeight;

    protected Float benthosTotalSampleSortedComputedWeight;

    protected ComputableData<Float> benthosTotalInertComputedOrNotWeight = new ComputableData<>();

    protected ComputableData<Float> benthosTotalLivingNotItemizedComputedOrNotWeight = new ComputableData<>();

    protected ComputableData<Float> marineLitterTotalComputedOrNotWeight = new ComputableData<>();

    protected ComputableData<Float> catchTotalComputedOrNotWeight = new ComputableData<>();

    protected Float catchTotalSortedComputedWeight;

    protected Float catchTotalSortedSortedComputedWeight;

    protected Float catchTotalUnsortedComputedWeight;

    protected Float catchTotalSortedTremisWeight;

    protected Float catchTotalSortedCarousselWeight;

    protected ComputableData<Float> catchTotalRejectedComputedOrNotWeight = new ComputableData<>();

    protected FishingOperation fishingOperation;

    protected String validationContext;

    protected final CatchBatch editObject = CatchBatchs.newCatchBatch();

    protected final List<Attachment> attachment = Lists.newArrayList();

    /**
     * Species already used in some batches.
     *
     * @since 0.3
     */
    protected final Multimap<CaracteristicQualitativeValue, Species>
            speciesUsed = HashMultimap.create();

    /**
     * Categories already used in some batches.
     *
     * @since 1.4
     */
    protected final Multimap<CaracteristicQualitativeValue, CaracteristicQualitativeValue>
            marineLitterCategoriesUsed = ArrayListMultimap.create();

    protected boolean loadingData;

    /**
     * Catch weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit catchWeightUnit;

    /**
     * Species weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit speciesWeightUnit;

    /**
     * Benthos weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit benthosWeightUnit;

    /**
     * Marine Litter weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit marineLitterWeightUnit;

    /**
     * Pour ne pas vérifier si on peut quitter l'écran des mensurations.
     *
     * @since 4.5
     */
    private boolean doNotCheckLeavingFrequencyScreen;

    public EditCatchesUIModel(WeightUnit speciesWeightUnit,
                              WeightUnit benthosWeightUnit,
                              WeightUnit marineLitterWeightUnit) {
        super(fromBeanBinder, toBeanBinder);
        //FIXME See if this can be configurable or guess from other weightUnits
        if (WeightUnit.G == speciesWeightUnit &&
            WeightUnit.G == benthosWeightUnit) {
            this.catchWeightUnit = WeightUnit.G;
        } else {
            this.catchWeightUnit = WeightUnit.KG;
        }
        this.speciesWeightUnit = speciesWeightUnit;
        this.benthosWeightUnit = benthosWeightUnit;
        this.marineLitterWeightUnit = marineLitterWeightUnit;

        speciesTotalSortedComputedOrNotWeight.addPropagateListener(
                PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT, this);
        speciesTotalInertComputedOrNotWeight.addPropagateListener(
                PROPERTY_SPECIES_TOTAL_INERT_WEIGHT, this);
        speciesTotalLivingNotItemizedComputedOrNotWeight.addPropagateListener(
                PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, this);

        benthosTotalSortedComputedOrNotWeight.addPropagateListener(
                PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT, this);
        benthosTotalInertComputedOrNotWeight.addPropagateListener(
                PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT, this);
        benthosTotalLivingNotItemizedComputedOrNotWeight.addPropagateListener(
                PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, this);

        marineLitterTotalComputedOrNotWeight.addPropagateListener(
                PROPERTY_MARINE_LITTER_TOTAL_WEIGHT, this);

        catchTotalComputedOrNotWeight.addPropagateListener(
                PROPERTY_CATCH_TOTAL_WEIGHT, this);
        catchTotalRejectedComputedOrNotWeight.addPropagateListener(
                PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT, this);
    }

    public WeightUnit getCatchWeightUnit() {
        return catchWeightUnit;
    }

    public boolean isLoadingData() {
        return loadingData;
    }

    public void setLoadingData(boolean loadingData) {
        this.loadingData = loadingData;
    }

    @Override
    public String getTitle() {
        return t("tutti.label.tab.catchesCaracteristics");
    }

    @Override
    protected CatchBatch newEntity() {
//        return editObject;
        return CatchBatchs.newCatchBatch();
    }

    @Override
    public void fromEntity(CatchBatch entity) {

        Object oldObjectId = getObjectId();

        super.fromEntity(entity);

        // id was bind to #id but not to editObject#id
        editObject.setId(getId());

        firePropertyChange(PROPERTY_CATCH_BATCH, null, entity);
        firePropertyChange(PROPERTY_OBJECT_ID, oldObjectId, getObjectId());

        setFishingOperation(entity == null ? null : entity.getFishingOperation());

        if (entity != null) {

            // convert total weights

            setCatchTotalComputedWeight(catchWeightUnit.fromEntity(entity.getCatchTotalComputedWeight()));
            setCatchTotalRejectedComputedWeight(catchWeightUnit.fromEntity(entity.getCatchTotalRejectedComputedWeight()));
            setCatchTotalRejectedWeight(catchWeightUnit.fromEntity(entity.getCatchTotalRejectedWeight()));
            setCatchTotalSortedCarousselWeight(catchWeightUnit.fromEntity(entity.getCatchTotalSortedCarousselWeight()));
            setCatchTotalSortedComputedWeight(catchWeightUnit.fromEntity(entity.getCatchTotalSortedComputedWeight()));
            setCatchTotalSortedSortedComputedWeight(catchWeightUnit.fromEntity(entity.getCatchTotalSortedSortedComputedWeight()));
            setCatchTotalSortedTremisWeight(catchWeightUnit.fromEntity(entity.getCatchTotalSortedTremisWeight()));
            setCatchTotalUnsortedComputedWeight(catchWeightUnit.fromEntity(entity.getCatchTotalUnsortedComputedWeight()));
            setCatchTotalWeight(catchWeightUnit.fromEntity(entity.getCatchTotalWeight()));

            // convert species weights

            setSpeciesTotalComputedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalComputedWeight()));
            setSpeciesTotalInertComputedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalInertComputedWeight()));
            setSpeciesTotalInertWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalInertWeight()));
            setSpeciesTotalLivingNotItemizedComputedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalLivingNotItemizedComputedWeight()));
            setSpeciesTotalLivingNotItemizedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalLivingNotItemizedWeight()));
            setSpeciesTotalSampleSortedComputedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalSampleSortedComputedWeight()));
            setSpeciesTotalSortedComputedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalSortedComputedWeight()));
            setSpeciesTotalSortedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalSortedWeight()));
            setSpeciesTotalUnsortedComputedWeight(speciesWeightUnit.fromEntity(entity.getSpeciesTotalUnsortedComputedWeight()));

            // convert benthos weights

            setBenthosTotalComputedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalComputedWeight()));
            setBenthosTotalInertComputedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalInertComputedWeight()));
            setBenthosTotalInertWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalInertWeight()));
            setBenthosTotalLivingNotItemizedComputedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalLivingNotItemizedComputedWeight()));
            setBenthosTotalLivingNotItemizedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalLivingNotItemizedWeight()));
            setBenthosTotalSampleSortedComputedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalSampleSortedComputedWeight()));
            setBenthosTotalSortedComputedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalSortedComputedWeight()));
            setBenthosTotalSortedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalSortedWeight()));
            setBenthosTotalUnsortedComputedWeight(benthosWeightUnit.fromEntity(entity.getBenthosTotalUnsortedComputedWeight()));

            // convert marine litter weights

            setMarineLitterTotalComputedWeight(marineLitterWeightUnit.fromEntity(entity.getMarineLitterTotalComputedWeight()));
            setMarineLitterTotalWeight(marineLitterWeightUnit.fromEntity(entity.getMarineLitterTotalWeight()));
        }
    }

    @Override
    public CatchBatch toEntity() {
        CatchBatch result = super.toEntity();

        // convert total weights

        result.setCatchTotalComputedWeight(catchWeightUnit.toEntity(getCatchTotalComputedWeight()));
        result.setCatchTotalRejectedComputedWeight(catchWeightUnit.toEntity(getCatchTotalRejectedComputedWeight()));
        result.setCatchTotalRejectedWeight(catchWeightUnit.toEntity(getCatchTotalRejectedWeight()));
        result.setCatchTotalSortedCarousselWeight(catchWeightUnit.toEntity(getCatchTotalSortedCarousselWeight()));
        result.setCatchTotalSortedComputedWeight(catchWeightUnit.toEntity(getCatchTotalSortedComputedWeight()));
        result.setCatchTotalSortedSortedComputedWeight(catchWeightUnit.toEntity(getCatchTotalSortedSortedComputedWeight()));
        result.setCatchTotalSortedTremisWeight(catchWeightUnit.toEntity(getCatchTotalSortedTremisWeight()));
        result.setCatchTotalUnsortedComputedWeight(catchWeightUnit.toEntity(getCatchTotalUnsortedComputedWeight()));
        result.setCatchTotalWeight(catchWeightUnit.toEntity(getCatchTotalWeight()));

        // convert species weights

        result.setSpeciesTotalComputedWeight(speciesWeightUnit.toEntity(getSpeciesTotalComputedWeight()));
        result.setSpeciesTotalInertComputedWeight(speciesWeightUnit.toEntity(getSpeciesTotalInertComputedWeight()));
        result.setSpeciesTotalInertWeight(speciesWeightUnit.toEntity(getSpeciesTotalInertWeight()));
        result.setSpeciesTotalLivingNotItemizedComputedWeight(speciesWeightUnit.toEntity(getSpeciesTotalLivingNotItemizedComputedWeight()));
        result.setSpeciesTotalLivingNotItemizedWeight(speciesWeightUnit.toEntity(getSpeciesTotalLivingNotItemizedWeight()));
        result.setSpeciesTotalSampleSortedComputedWeight(speciesWeightUnit.toEntity(getSpeciesTotalSampleSortedComputedWeight()));
        result.setSpeciesTotalSortedComputedWeight(speciesWeightUnit.toEntity(getSpeciesTotalSortedComputedWeight()));
        result.setSpeciesTotalSortedWeight(speciesWeightUnit.toEntity(getSpeciesTotalSortedWeight()));
        result.setSpeciesTotalUnsortedComputedWeight(speciesWeightUnit.toEntity(getSpeciesTotalUnsortedComputedWeight()));

        // convert benthos weights

        result.setBenthosTotalComputedWeight(benthosWeightUnit.toEntity(getBenthosTotalComputedWeight()));
        result.setBenthosTotalInertComputedWeight(benthosWeightUnit.toEntity(getBenthosTotalInertComputedWeight()));
        result.setBenthosTotalInertWeight(benthosWeightUnit.toEntity(getBenthosTotalInertWeight()));
        result.setBenthosTotalLivingNotItemizedComputedWeight(benthosWeightUnit.toEntity(getBenthosTotalLivingNotItemizedComputedWeight()));
        result.setBenthosTotalLivingNotItemizedWeight(benthosWeightUnit.toEntity(getBenthosTotalLivingNotItemizedWeight()));
        result.setBenthosTotalSampleSortedComputedWeight(benthosWeightUnit.toEntity(getBenthosTotalSampleSortedComputedWeight()));
        result.setBenthosTotalSortedComputedWeight(benthosWeightUnit.toEntity(getBenthosTotalSortedComputedWeight()));
        result.setBenthosTotalSortedWeight(benthosWeightUnit.toEntity(getBenthosTotalSortedWeight()));
        result.setBenthosTotalUnsortedComputedWeight(benthosWeightUnit.toEntity(getBenthosTotalUnsortedComputedWeight()));

        // convert marine litter weights

        result.setMarineLitterTotalComputedWeight(marineLitterWeightUnit.toEntity(getMarineLitterTotalComputedWeight()));
        result.setMarineLitterTotalWeight(marineLitterWeightUnit.toEntity(getMarineLitterTotalWeight()));

        return result;
    }

    @Override
    public boolean isEmpty() {
        return getCatchTotalWeight() == null
               && getCatchTotalRejectedWeight() == null
               && CollectionUtils.isEmpty(getAttachment());
    }

    @Override
    public String getIcon() {
        return null;
    }

    @Override
    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    @Override
    public void setFishingOperation(FishingOperation fishingOperation) {
        Object oldValue = getFishingOperation();
        this.fishingOperation = fishingOperation;
        firePropertyChange(PROPERTY_FISHING_OPERATION, oldValue, fishingOperation);
    }

    @Override
    public boolean isCloseable() {
        return false;
    }

    public String getValidationContext() {
        return validationContext;
    }

    public void setValidationContext(String validationContext) {
        Object oldValue = getValidationContext();
        this.validationContext = validationContext;
        firePropertyChange(PROPERTY_VALIDATION_CONTEXT, oldValue, validationContext);
    }

    public Multimap<CaracteristicQualitativeValue, Species> getSpeciesUsed() {
        return speciesUsed;
    }

    public void replaceCaracteristicValue(Species species,
                                          CaracteristicQualitativeValue oldValue,
                                          CaracteristicQualitativeValue newValue) {

        speciesUsed.remove(oldValue, species);
        speciesUsed.put(newValue, species);

    }

    public Multimap<CaracteristicQualitativeValue, CaracteristicQualitativeValue> getMarineLitterCategoriesUsed() {
        return marineLitterCategoriesUsed;
    }

    //------------------------------------------------------------------------//
    //-- Total                                                              --//
    //------------------------------------------------------------------------//

    public ComputableData<Float> getCatchTotalComputedOrNotWeight() {
        return catchTotalComputedOrNotWeight;
    }

    @Override
    public Float getCatchTotalWeight() {
        return catchTotalComputedOrNotWeight.getData();
    }

    @Override
    public void setCatchTotalWeight(Float catchTotalWeight) {
        Object oldValue = getCatchTotalWeight();
        this.catchTotalComputedOrNotWeight.setData(catchTotalWeight);
        firePropertyChange(PROPERTY_CATCH_TOTAL_WEIGHT, oldValue, catchTotalWeight);
    }

    @Override
    public Float getCatchTotalComputedWeight() {
        return catchTotalComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setCatchTotalComputedWeight(Float catchTotalComputedWeight) {
        Object oldValue = getCatchTotalComputedWeight();
        this.catchTotalComputedOrNotWeight.setComputedData(catchTotalComputedWeight);
        firePropertyChange(PROPERTY_CATCH_TOTAL_COMPUTED_WEIGHT, oldValue, catchTotalComputedWeight);
    }

    @Override
    public Float getCatchTotalSortedComputedWeight() {
        return catchTotalSortedComputedWeight;
    }

    @Override
    public void setCatchTotalSortedComputedWeight(Float catchTotalSortedComputedWeight) {
        Object oldValue = getCatchTotalSortedComputedWeight();
        this.catchTotalSortedComputedWeight = catchTotalSortedComputedWeight;
        firePropertyChange(PROPERTY_CATCH_TOTAL_SORTED_COMPUTED_WEIGHT, oldValue, catchTotalSortedComputedWeight);
    }

    @Override
    public Float getCatchTotalUnsortedComputedWeight() {
        return catchTotalUnsortedComputedWeight;
    }

    @Override
    public void setCatchTotalUnsortedComputedWeight(Float catchTotalUnsortedComputedWeight) {
        Object oldValue = getCatchTotalUnsortedComputedWeight();
        this.catchTotalUnsortedComputedWeight = catchTotalUnsortedComputedWeight;
        firePropertyChange(PROPERTY_CATCH_TOTAL_UNSORTED_COMPUTED_WEIGHT, oldValue, catchTotalUnsortedComputedWeight);
    }

    @Override
    public Float getCatchTotalSortedTremisWeight() {
        return catchTotalSortedTremisWeight;
    }

    @Override
    public void setCatchTotalSortedTremisWeight(Float catchTotalSortedTremisWeight) {
        Object oldValue = getCatchTotalSortedTremisWeight();
        this.catchTotalSortedTremisWeight = catchTotalSortedTremisWeight;
        firePropertyChange(PROPERTY_CATCH_TOTAL_SORTED_TREMIS_WEIGHT, oldValue, catchTotalSortedTremisWeight);
    }

    @Override
    public Float getCatchTotalSortedCarousselWeight() {
        return catchTotalSortedCarousselWeight;
    }

    @Override
    public void setCatchTotalSortedCarousselWeight(Float catchTotalSortedCarousselWeight) {
        Object oldValue = getCatchTotalSortedCarousselWeight();
        this.catchTotalSortedCarousselWeight = catchTotalSortedCarousselWeight;
        firePropertyChange(PROPERTY_CATCH_TOTAL_SORTED_CAROUSSEL_WEIGHT, oldValue, catchTotalSortedCarousselWeight);
    }

    public ComputableData<Float> getCatchTotalRejectedComputedOrNotWeight() {
        return catchTotalRejectedComputedOrNotWeight;
    }

    @Override
    public Float getCatchTotalRejectedWeight() {
        return catchTotalRejectedComputedOrNotWeight.getData();
    }

    @Override
    public void setCatchTotalRejectedWeight(Float catchTotalRejectedWeight) {
        Object oldValue = getCatchTotalRejectedWeight();
        this.catchTotalRejectedComputedOrNotWeight.setData(catchTotalRejectedWeight);
        firePropertyChange(PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT, oldValue, catchTotalRejectedWeight);
    }

    @Override
    public Float getCatchTotalRejectedComputedWeight() {
        return catchTotalRejectedComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setCatchTotalRejectedComputedWeight(Float catchTotalRejectedComputedWeight) {
        Object oldValue = getCatchTotalRejectedComputedWeight();
        this.catchTotalRejectedComputedOrNotWeight.setComputedData(catchTotalRejectedComputedWeight);
        firePropertyChange(PROPERTY_CATCH_TOTAL_REJECTED_COMPUTED_WEIGHT, oldValue, catchTotalRejectedComputedWeight);
    }

    //------------------------------------------------------------------------//
    //-- Species                                                            --//
    //------------------------------------------------------------------------//

    @Override
    public Float getSpeciesTotalComputedWeight() {
        return speciesTotalComputedWeight;
    }

    @Override
    public void setSpeciesTotalComputedWeight(Float speciesTotalComputedWeight) {
        Object oldValue = getSpeciesTotalComputedWeight();
        this.speciesTotalComputedWeight = speciesTotalComputedWeight;
        firePropertyChange(PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT, oldValue, speciesTotalComputedWeight);
    }

    public ComputableData<Float> getSpeciesTotalSortedComputedOrNotWeight() {
        return speciesTotalSortedComputedOrNotWeight;
    }

    @Override
    public Float getSpeciesTotalSortedWeight() {
        return speciesTotalSortedComputedOrNotWeight.getData();
    }

    @Override
    public void setSpeciesTotalSortedWeight(Float speciesTotalSortedWeight) {
        Object oldValue = getSpeciesTotalSortedWeight();
        this.speciesTotalSortedComputedOrNotWeight.setData(speciesTotalSortedWeight);
        firePropertyChange(PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT, oldValue, speciesTotalSortedWeight);
    }

    @Override
    public Float getSpeciesTotalSortedComputedWeight() {
        return speciesTotalSortedComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setSpeciesTotalSortedComputedWeight(Float speciesTotalSortedComputedWeight) {
        Object oldValue = getSpeciesTotalSortedComputedWeight();
        this.speciesTotalSortedComputedOrNotWeight.setComputedData(speciesTotalSortedComputedWeight);
        firePropertyChange(PROPERTY_SPECIES_TOTAL_SORTED_COMPUTED_WEIGHT, oldValue, speciesTotalSortedComputedWeight);
    }

    @Override
    public Float getSpeciesTotalSampleSortedComputedWeight() {
        return speciesTotalSampleSortedComputedWeight;
    }

    @Override
    public void setSpeciesTotalSampleSortedComputedWeight(Float speciesTotalSampleSortedComputedWeight) {
        Object oldValue = getSpeciesTotalSampleSortedComputedWeight();
        this.speciesTotalSampleSortedComputedWeight = speciesTotalSampleSortedComputedWeight;
        firePropertyChange(PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT, oldValue, speciesTotalSampleSortedComputedWeight);
    }

    @Override
    public Float getSpeciesTotalUnsortedComputedWeight() {
        return speciesTotalUnsortedComputedWeight;
    }

    @Override
    public void setSpeciesTotalUnsortedComputedWeight(Float speciesTotalUnsortedComputedWeight) {
        Object oldValue = getSpeciesTotalUnsortedComputedWeight();
        this.speciesTotalUnsortedComputedWeight = speciesTotalUnsortedComputedWeight;
        firePropertyChange(PROPERTY_SPECIES_TOTAL_UNSORTED_COMPUTED_WEIGHT, oldValue, speciesTotalUnsortedComputedWeight);
    }

    public ComputableData<Float> getSpeciesTotalInertComputedOrNotWeight() {
        return speciesTotalInertComputedOrNotWeight;
    }

    @Override
    public Float getSpeciesTotalInertWeight() {
        return speciesTotalInertComputedOrNotWeight.getData();
    }

    @Override
    public void setSpeciesTotalInertWeight(Float speciesTotalInertWeight) {
        Object oldValue = getSpeciesTotalInertWeight();
        this.speciesTotalInertComputedOrNotWeight.setData(speciesTotalInertWeight);
        firePropertyChange(PROPERTY_SPECIES_TOTAL_INERT_WEIGHT, oldValue, speciesTotalInertWeight);
    }

    @Override
    public Float getSpeciesTotalInertComputedWeight() {
        return speciesTotalInertComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setSpeciesTotalInertComputedWeight(Float speciesTotalInertComputedWeight) {
        Object oldValue = getSpeciesTotalInertComputedWeight();
        this.speciesTotalInertComputedOrNotWeight.setComputedData(speciesTotalInertComputedWeight);
        firePropertyChange(PROPERTY_SPECIES_TOTAL_INERT_COMPUTED_WEIGHT, oldValue, speciesTotalInertComputedWeight);
    }

    public ComputableData<Float> getSpeciesTotalLivingNotItemizedComputedOrNotWeight() {
        return speciesTotalLivingNotItemizedComputedOrNotWeight;
    }

    @Override
    public Float getSpeciesTotalLivingNotItemizedWeight() {
        return speciesTotalLivingNotItemizedComputedOrNotWeight.getData();
    }

    @Override
    public void setSpeciesTotalLivingNotItemizedWeight(Float speciesTotalLivingNotItemizedWeight) {
        Object oldValue = getSpeciesTotalLivingNotItemizedComputedWeight();
        this.speciesTotalLivingNotItemizedComputedOrNotWeight.setData(speciesTotalLivingNotItemizedWeight);
        firePropertyChange(PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, oldValue, speciesTotalLivingNotItemizedWeight);
    }

    @Override
    public Float getSpeciesTotalLivingNotItemizedComputedWeight() {
        return speciesTotalLivingNotItemizedComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setSpeciesTotalLivingNotItemizedComputedWeight(Float speciesTotalLivingNotItemizedComputedWeight) {
        Object oldValue = getSpeciesTotalLivingNotItemizedComputedWeight();
        this.speciesTotalLivingNotItemizedComputedOrNotWeight.setComputedData(speciesTotalLivingNotItemizedComputedWeight);
        firePropertyChange(PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT, oldValue, speciesTotalLivingNotItemizedComputedWeight);
    }

    /**
     * Les espèces observées sont en warning lorsque :
     *
     * * pas de poids vrac non trié (ou = 0)
     * * poids espèce observé (hors vrac) présent et &gt; 0
     * * poids espèce observé (vrac) vaut poids espèce trié (vrac)
     *
     * @return {@code true} quand le les espèces observées sont en warning, {@code false} dans les autres cas.
     */
    public boolean isSpeciesTotalUnsortedComputedWeightInWarning() {
        Float catchTotalRejectedWeight = getCatchTotalRejectedComputedOrNotWeight().getDataOrComputedData();
        Float speciesTotalSortedWeight = getSpeciesTotalSortedComputedOrNotWeight().getDataOrComputedData();

        boolean noCatchTotalRejected = WeightUnit.KG.isNullOrZero(catchTotalRejectedWeight);
        boolean noTotalUnsortedWeight = speciesTotalUnsortedComputedWeight != null && WeightUnit.KG.isGreaterThanZero(speciesTotalUnsortedComputedWeight);
        boolean speciesWeightEquals = speciesTotalSortedWeight != null && speciesTotalSampleSortedComputedWeight !=null
                                      && WeightUnit.KG.isEquals(speciesTotalSortedWeight, speciesTotalSampleSortedComputedWeight);

        return noTotalUnsortedWeight && noCatchTotalRejected && speciesWeightEquals;
    }

    //------------------------------------------------------------------------//
    //-- Benthos                                                            --//
    //------------------------------------------------------------------------//

    @Override
    public Float getBenthosTotalComputedWeight() {
        return benthosTotalComputedWeight;
    }

    @Override
    public void setBenthosTotalComputedWeight(Float benthosTotalComputedWeight) {
        Object oldValue = getBenthosTotalComputedWeight();
        this.benthosTotalComputedWeight = benthosTotalComputedWeight;
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_COMPUTED_WEIGHT, oldValue, benthosTotalComputedWeight);
    }

    public ComputableData<Float> getBenthosTotalSortedComputedOrNotWeight() {
        return benthosTotalSortedComputedOrNotWeight;
    }

    @Override
    public Float getBenthosTotalSortedWeight() {
        return benthosTotalSortedComputedOrNotWeight.getData();
    }

    @Override
    public void setBenthosTotalSortedWeight(Float benthosTotalSortedWeight) {
        Object oldValue = getBenthosTotalSortedWeight();
        this.benthosTotalSortedComputedOrNotWeight.setData(benthosTotalSortedWeight);
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT, oldValue, benthosTotalSortedWeight);
    }

    @Override
    public Float getBenthosTotalSortedComputedWeight() {
        return benthosTotalSortedComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setBenthosTotalSortedComputedWeight(Float benthosTotalSortedComputedWeight) {
        Object oldValue = getBenthosTotalSortedComputedWeight();
        this.benthosTotalSortedComputedOrNotWeight.setComputedData(benthosTotalSortedComputedWeight);
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_SORTED_COMPUTED_WEIGHT, oldValue, benthosTotalSortedComputedWeight);
    }

    @Override
    public Float getBenthosTotalSampleSortedComputedWeight() {
        return benthosTotalSampleSortedComputedWeight;
    }

    @Override
    public void setBenthosTotalSampleSortedComputedWeight(Float benthosTotalSampleSortedComputedWeight) {
        Object oldValue = getBenthosTotalSampleSortedComputedWeight();
        this.benthosTotalSampleSortedComputedWeight = benthosTotalSampleSortedComputedWeight;
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT, oldValue, benthosTotalSampleSortedComputedWeight);
    }

    @Override
    public Float getBenthosTotalUnsortedComputedWeight() {
        return benthosTotalUnsortedComputedWeight;
    }

    @Override
    public void setBenthosTotalUnsortedComputedWeight(Float benthosTotalUnsortedComputedWeight) {
        Object oldValue = getBenthosTotalUnsortedComputedWeight();
        this.benthosTotalUnsortedComputedWeight = benthosTotalUnsortedComputedWeight;
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_UNSORTED_COMPUTED_WEIGHT, oldValue, benthosTotalUnsortedComputedWeight);
    }

    public ComputableData<Float> getBenthosTotalInertComputedOrNotWeight() {
        return benthosTotalInertComputedOrNotWeight;
    }

    @Override
    public Float getBenthosTotalInertWeight() {
        return benthosTotalInertComputedOrNotWeight.getData();
    }

    @Override
    public void setBenthosTotalInertWeight(Float benthosTotalInertWeight) {
        Object oldValue = getBenthosTotalInertWeight();
        this.benthosTotalInertComputedOrNotWeight.setData(benthosTotalInertWeight);
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT, oldValue, benthosTotalInertWeight);
    }

    @Override
    public Float getBenthosTotalInertComputedWeight() {
        return benthosTotalInertComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setBenthosTotalInertComputedWeight(Float benthosTotalInertComputedWeight) {
        Object oldValue = getBenthosTotalInertComputedWeight();
        this.benthosTotalInertComputedOrNotWeight.setComputedData(benthosTotalInertComputedWeight);
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_INERT_COMPUTED_WEIGHT, oldValue, benthosTotalInertComputedWeight);
    }

    public ComputableData<Float> getBenthosTotalLivingNotItemizedComputedOrNotWeight() {
        return benthosTotalLivingNotItemizedComputedOrNotWeight;
    }

    @Override
    public Float getBenthosTotalLivingNotItemizedWeight() {
        return benthosTotalLivingNotItemizedComputedOrNotWeight.getData();
    }

    @Override
    public void setBenthosTotalLivingNotItemizedWeight(Float benthosTotalLivingNotItemizedWeight) {
        Object oldValue = getBenthosTotalLivingNotItemizedComputedWeight();
        this.benthosTotalLivingNotItemizedComputedOrNotWeight.setData(benthosTotalLivingNotItemizedWeight);
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, oldValue, benthosTotalLivingNotItemizedWeight);
    }

    @Override
    public Float getBenthosTotalLivingNotItemizedComputedWeight() {
        return benthosTotalLivingNotItemizedComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setBenthosTotalLivingNotItemizedComputedWeight(Float benthosTotalLivingNotItemizedComputedWeight) {
        Object oldValue = getBenthosTotalLivingNotItemizedComputedWeight();
        this.benthosTotalLivingNotItemizedComputedOrNotWeight.setComputedData(benthosTotalLivingNotItemizedComputedWeight);
        firePropertyChange(PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT, oldValue, benthosTotalLivingNotItemizedComputedWeight);
    }

    /**
     * Le benthos observé est en warning lorsque :
     *
     * * pas de poids vrac non trié (ou = 0)
     * * poids benthos observé (hors vrac) présent et &gt; 0
     * * poids benthos observé (vrac) vaut poids benthos trié (vrac)
     *
     * @return {@code true} quand le le benthos est en warning, {@code false} dans les autres cas.
     */
    public boolean isBenthosTotalUnsortedComputedWeightInWarning() {
        Float catchTotalRejectedWeight = getCatchTotalRejectedComputedOrNotWeight().getDataOrComputedData();
        Float benthosTotalSortedWeight = getBenthosTotalSortedComputedOrNotWeight().getDataOrComputedData();

        boolean noCatchTotalRejected = WeightUnit.KG.isNullOrZero(catchTotalRejectedWeight);
        boolean noTotalUnsortedWeight = benthosTotalUnsortedComputedWeight != null && WeightUnit.KG.isGreaterThanZero(benthosTotalUnsortedComputedWeight);
        boolean benthosWeightEquals = benthosTotalSortedWeight != null && benthosTotalSampleSortedComputedWeight !=null
                                      && WeightUnit.KG.isEquals(benthosTotalSortedWeight, benthosTotalSampleSortedComputedWeight);

        return noTotalUnsortedWeight && noCatchTotalRejected && benthosWeightEquals;

    }

    //------------------------------------------------------------------------//
    //-- Marine Litter                                                      --//
    //------------------------------------------------------------------------//

    public ComputableData<Float> getMarineLitterTotalComputedOrNotWeight() {
        return marineLitterTotalComputedOrNotWeight;
    }

    @Override
    public Float getMarineLitterTotalWeight() {
        return marineLitterTotalComputedOrNotWeight.getData();
    }

    @Override
    public void setMarineLitterTotalWeight(Float marineLitterTotalWeight) {
        Object oldValue = getMarineLitterTotalWeight();
        this.marineLitterTotalComputedOrNotWeight.setData(marineLitterTotalWeight);
        firePropertyChange(PROPERTY_MARINE_LITTER_TOTAL_WEIGHT, oldValue, marineLitterTotalWeight);
    }

    @Override
    public Float getMarineLitterTotalComputedWeight() {
        return marineLitterTotalComputedOrNotWeight.getComputedData();
    }

    @Override
    public void setMarineLitterTotalComputedWeight(Float marineLitterTotalComputedWeight) {
        Object oldValue = getMarineLitterTotalComputedWeight();
        this.marineLitterTotalComputedOrNotWeight.setComputedData(marineLitterTotalComputedWeight);
        firePropertyChange(PROPERTY_MARINE_LITTER_TOTAL_COMPUTED_WEIGHT, oldValue, marineLitterTotalComputedWeight);
    }

    //------------------------------------------------------------------------//
    //-- Attachment                                                         --//
    //------------------------------------------------------------------------//

    @Override
    public ObjectTypeCode getObjectType() {
        return ObjectTypeCode.CATCH_BATCH;
    }

    @Override
    public Integer getObjectId() {
        return editObject == null ? null : editObject.getIdAsInt();
    }

    @Override
    public List<Attachment> getAttachment() {
        return attachment;
    }

    @Override
    public void addAllAttachment(Collection<Attachment> attachments) {
        this.attachment.addAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        this.attachment.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAllAttachment(Collection<Attachment> attachments) {
        this.attachment.removeAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        this.attachment.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public String getSynchronizationStatus() {
        return editObject.getSynchronizationStatus();
    }

    @Override
    public void setSynchronizationStatus(String synchronizationStatus) {
        String oldValue = getSynchronizationStatus();
        editObject.setSynchronizationStatus(synchronizationStatus);
        firePropertyChange(PROPERTY_SYNCHRONIZATION_STATUS, oldValue, synchronizationStatus);
    }

    @Override
    public Float getCatchTotalSortedSortedComputedWeight() {
        return catchTotalSortedSortedComputedWeight;
    }

    @Override
    public void setCatchTotalSortedSortedComputedWeight(Float catchTotalSortedSortedComputedWeight) {
        Object oldValue = getCatchTotalSortedSortedComputedWeight();
        this.catchTotalSortedSortedComputedWeight = catchTotalSortedSortedComputedWeight;
        firePropertyChange(PROPERTY_CATCH_TOTAL_SORTED_SORTED_COMPUTED_WEIGHT, oldValue, catchTotalSortedSortedComputedWeight);
    }

    @Override
    public Integer getSpeciesDistinctSortedSpeciesCount() {
        return editObject.getSpeciesDistinctSortedSpeciesCount();
    }

    @Override
    public void setSpeciesDistinctSortedSpeciesCount(Integer speciesDistinctSortedSpeciesCount) {
        Object oldValue = getSpeciesDistinctSortedSpeciesCount();
        this.editObject.setSpeciesDistinctSortedSpeciesCount(speciesDistinctSortedSpeciesCount);
        firePropertyChange(PROPERTY_SPECIES_DISTINCT_SORTED_SPECIES_COUNT, oldValue, speciesDistinctSortedSpeciesCount);
    }

    @Override
    public Integer getBenthosDistinctSortedSpeciesCount() {
        return editObject.getBenthosDistinctSortedSpeciesCount();
    }

    @Override
    public void setBenthosDistinctSortedSpeciesCount(Integer benthosDistinctSortedSpeciesCount) {
        Object oldValue = getBenthosDistinctSortedSpeciesCount();
        this.editObject.setBenthosDistinctSortedSpeciesCount(benthosDistinctSortedSpeciesCount);
        firePropertyChange(PROPERTY_BENTHOS_DISTINCT_SORTED_SPECIES_COUNT, oldValue, benthosDistinctSortedSpeciesCount);
    }

    @Override
    public Integer getBenthosDistinctUnsortedSpeciesCount() {
        return editObject.getBenthosDistinctUnsortedSpeciesCount();
    }

    @Override
    public Integer getSpeciesDistinctUnsortedSpeciesCount() {
        return editObject.getSpeciesDistinctUnsortedSpeciesCount();
    }

    @Override
    public void setSpeciesDistinctUnsortedSpeciesCount(Integer speciesDistinctUnsortedSpeciesCount) {
        Object oldValue = getSpeciesDistinctUnsortedSpeciesCount();
        this.editObject.setSpeciesDistinctUnsortedSpeciesCount(speciesDistinctUnsortedSpeciesCount);
        firePropertyChange(PROPERTY_SPECIES_DISTINCT_UNSORTED_SPECIES_COUNT, oldValue, speciesDistinctUnsortedSpeciesCount);
    }

    @Override
    public void setBenthosDistinctUnsortedSpeciesCount(Integer benthosDistinctUnsortedSpeciesCount) {
        Object oldValue = getBenthosDistinctUnsortedSpeciesCount();
        this.editObject.setBenthosDistinctUnsortedSpeciesCount(benthosDistinctUnsortedSpeciesCount);
        firePropertyChange(PROPERTY_BENTHOS_DISTINCT_UNSORTED_SPECIES_COUNT, oldValue, benthosDistinctUnsortedSpeciesCount);
    }

    public void reset() {
        setSpeciesTotalInertComputedWeight(null);
        setSpeciesTotalLivingNotItemizedComputedWeight(null);
        setSpeciesTotalSampleSortedComputedWeight(null);
        setSpeciesTotalSortedComputedWeight(null);
        setSpeciesTotalUnsortedComputedWeight(null);
        setSpeciesTotalComputedWeight(null);
        setBenthosTotalInertComputedWeight(null);
        setBenthosTotalLivingNotItemizedComputedWeight(null);
        setBenthosTotalSampleSortedComputedWeight(null);
        setBenthosTotalSortedComputedWeight(null);
        setBenthosTotalUnsortedComputedWeight(null);
        setBenthosTotalComputedWeight(null);
        setMarineLitterTotalComputedWeight(null);
        setCatchTotalRejectedComputedWeight(null);
        setCatchTotalSortedComputedWeight(null);
        setCatchTotalSortedSortedComputedWeight(null);
        setCatchTotalUnsortedComputedWeight(null);
        setCatchTotalComputedWeight(null);
        removeAllAttachment(getAttachment());
        getSpeciesUsed().clear();
        getMarineLitterCategoriesUsed().clear();
        setSpeciesDistinctSortedSpeciesCount(null);
        setBenthosDistinctSortedSpeciesCount(null);
        setSpeciesDistinctUnsortedSpeciesCount(null);
        setBenthosDistinctUnsortedSpeciesCount(null);
    }

    public boolean isDoNotCheckLeavingFrequencyScreen() {
        return doNotCheckLeavingFrequencyScreen;
    }

    public void setDoNotCheckLeavingFrequencyScreen(boolean doNotCheckLeavingFrequencyScreen) {
        this.doNotCheckLeavingFrequencyScreen = doNotCheckLeavingFrequencyScreen;
    }
}
