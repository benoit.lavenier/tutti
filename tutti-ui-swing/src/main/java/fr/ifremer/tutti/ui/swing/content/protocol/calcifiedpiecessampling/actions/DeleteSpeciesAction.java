package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.jdesktop.swingx.JXTable;
import org.nuiton.decorator.Decorator;

import javax.swing.JOptionPane;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class DeleteSpeciesAction extends SimpleActionSupport<CalcifiedPiecesSamplingEditorUI> {

    public DeleteSpeciesAction(CalcifiedPiecesSamplingEditorUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(CalcifiedPiecesSamplingEditorUI ui) {

        JXTable cpsTable = ui.getCpsTable();
        CalcifiedPiecesSamplingEditorTableModel tableModel = (CalcifiedPiecesSamplingEditorTableModel) cpsTable.getModel();

        int[] selectedRows = cpsTable.getSelectedRows();

        List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = ui.getModel().getCpsRows();

        Set<Species> speciesToDelete = new LinkedHashSet<>();
        for (int selectedRow : selectedRows) {

            CalcifiedPiecesSamplingEditorRowModel row = cpsRows.get(selectedRow);
            EditProtocolSpeciesRowModel speciesRowToDelete = row.getProtocolSpecies();
            speciesToDelete.add(speciesRowToDelete.getSpecies());

        }

        int confirmDeletion;

        Decorator<Species> speciesDecorator = ui.getHandler().getDecorator(Species.class, DecoratorService.WITH_SURVEY_CODE);

        if (speciesToDelete.size() == 1) {

            confirmDeletion = JOptionPane.showConfirmDialog(ui,
                                                            t("tutti.editCps.deleteOneSpecies.message", speciesDecorator.toString(speciesToDelete.iterator().next())),
                                                            t("tutti.editCps.deleteOneSpecies.title"),
                                                            JOptionPane.YES_NO_OPTION,
                                                            JOptionPane.QUESTION_MESSAGE);
        } else {

            StringBuilder builder = new StringBuilder();
            for (Species species : speciesToDelete) {
                builder.append("<li>").append(speciesDecorator.toString(species)).append("</li>");
            }
            confirmDeletion = JOptionPane.showConfirmDialog(ui,
                                                            t("tutti.editCps.deleteMoreThanOneSpecies.message", builder.toString()),
                                                            t("tutti.editCps.deleteMoreThanOneSpecies.title"),
                                                            JOptionPane.YES_NO_OPTION,
                                                            JOptionPane.QUESTION_MESSAGE);

        }

        if (confirmDeletion == JOptionPane.YES_OPTION) {

            for (Species species : speciesToDelete) {

                List<CalcifiedPiecesSamplingEditorRowModel> rowsToDelete
                        = cpsRows
                        .stream()
                        .filter(r -> r.getProtocolSpecies().getSpecies().equals(species))
                        .collect(Collectors.toList());

                TreeSet<Integer> indexesToDelete =
                        new TreeSet<>(rowsToDelete.stream().map(cpsRows::indexOf).collect(Collectors.toSet()));

                cpsRows.removeAll(rowsToDelete);

                tableModel.fireTableRowsDeleted(indexesToDelete.first(), indexesToDelete.last());

                ui.getSpeciesComboBox().addItem(species);

            }

        }

    }

}
