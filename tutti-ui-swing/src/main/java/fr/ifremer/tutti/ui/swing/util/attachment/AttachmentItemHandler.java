package fr.ifremer.tutti.ui.swing.util.attachment;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.JComponent;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class AttachmentItemHandler extends AbstractTuttiUIHandler<AttachmentItemModel, AttachmentItem> {

    @Override
    public void beforeInit(AttachmentItem ui) {
        super.beforeInit(ui);
    }

    @Override
    public void afterInit(AttachmentItem ui) {
//        initUI(ui);

        initButton(ui.getOpenAttachmentButton());
        initButton(ui.getRemoveAttachmentButton());
        initButton(ui.getSaveAttachmentButton());

    }

    @Override
    public SwingValidator<AttachmentItemModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void onCloseUI() {

    }
}
