package fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Le support pour un nœud des arbres de l'éditeur de zone.
 *
 * Chaque nœud possède au moins un id et le {@link #getUserObject()} est son label.
 *
 * Les nœuds sont ordonnées par leur label.
 *
 * @author Kevin Morin (Code Lutin)
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public abstract class ZoneEditorNodeSupport extends DefaultMutableTreeNode implements Comparable<ZoneEditorNodeSupport> {

    private final String id;

    public ZoneEditorNodeSupport(String id, String label, boolean allowchildren) {
        super(label, allowchildren);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getFutureNodePosition(ZoneEditorNodeSupport node) {
        if (children == null) {
            return 0;
        }

        List<ZoneEditorNodeSupport> orderedChildren = new ArrayList<>(children);
        orderedChildren.add(node);
        Collections.sort(orderedChildren);
        return orderedChildren.indexOf(node);
    }

    @Override
    public int compareTo(ZoneEditorNodeSupport o) {
        if (o == null) {
            return 1;
        }
        return getUserObject().compareTo(o.getUserObject());
    }

    @Override
    public ZoneEditorNodeSupport getParent() {
        return (ZoneEditorNodeSupport) super.getParent();
    }

    @Override
    public String getUserObject() {
        return (String) super.getUserObject();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("id", getId())
                          .addValue(getUserObject())
                          .toString();
    }
}
