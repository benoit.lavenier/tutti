package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.catches.WeightCleaningService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * To remove weight in double for species and benthos rows.
 *
 * Created on 9/27/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class CleanBatchWeightsAction extends LongActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CleanBatchWeightsAction.class);

    protected final WeightCleaningService cleaningService;

    protected int nbErrors;

    public CleanBatchWeightsAction(EditCatchesUIHandler handler) {
        super(handler, false);
        cleaningService = getContext().getWeightCleaningService();
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();
        if (doAction) {


            // do a check
            FishingOperation fishingOperation = getModel().getFishingOperation();
            Multimap<String, String> errors = cleaningService.checkFishingOperation(fishingOperation.getIdAsInt());

            nbErrors = errors.size();

            if (nbErrors == 0) {

                // no errors

                sendMessage(t("tutti.editCatchBatch.action.cleanWeights.no.double.weight.detected"));
                doAction = false;
            } else {

                // show errors to user and ask him to apply change

                String errorsStr = cleaningService.errorsToString(errors);
                String htmlMessage = String.format(
                        AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                        t("tutti.editCatchBatch.action.cleanWeights.double.weight.detected", errorsStr),
                        t("tutti.editCatchBatch.action.cleanWeights.help"));

                int answer = JOptionPane.showConfirmDialog(
                        getContext().getActionUI(),
                        htmlMessage,
                        t("tutti.editCatchBatch.action.cleanWieghts.resume.title"),
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE);

                doAction = (answer == JOptionPane.OK_OPTION);

            }
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {

        EditCatchesUIModel model = getModel();
        FishingOperation fishingOperation = getModel().getFishingOperation();
        model.setLoadingData(true);

        cleaningService.cleanFishingOperation(fishingOperation.getIdAsInt());

        try {

            getSpeciesBatchUI().getHandler().selectFishingOperation(fishingOperation);
        } catch (InvalidBatchModelException e) {

            // invalid sample category model for species batches
            if (log.isDebugEnabled()) {
                log.debug("Invalid sample category model for species batches", e);
            }
        }

        try {
            getBenthosBatchUI().getHandler().selectFishingOperation(fishingOperation);
        } catch (InvalidBatchModelException e) {

            // invalid sample category model for benthos batches
            if (log.isDebugEnabled()) {
                log.debug("Invalid sample category model for benthos batches", e);
            }
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        sendMessage(t("tutti.editCatchBatch.action.cleanWeights.done", nbErrors));
    }

    private SpeciesBatchUI getBenthosBatchUI() {
        return getUI().getBenthosTabPanel().getEditBatchesUI();
    }

    private SpeciesBatchUI getSpeciesBatchUI() {
        return getUI().getSpeciesTabPanel().getEditBatchesUI();
    }

}
