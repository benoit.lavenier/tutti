package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.AbstractTuttiBatchTableUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.create.CreateSpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyCellComponent;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.split.SplitSpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellEditor;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellRenderer;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellEditor;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellRenderer;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableDataTableCell;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JTables;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.nuiton.validator.NuitonValidatorResult;

import javax.swing.JComponent;
import javax.swing.RowFilter;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.Color;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class SpeciesBatchUIHandler extends AbstractTuttiBatchTableUIHandler<SpeciesBatchRowModel, SpeciesBatchUIModel, SpeciesBatchTableModel, SpeciesBatchUI> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SpeciesBatchUIHandler.class);

    private EnumMap<TableViewMode, RowFilter<SpeciesBatchTableModel, Integer>> tableFilters;

    /**
     * Sample categories model.
     *
     * @since 2.4
     */
    protected SampleCategoryModel sampleCategoryModel;

//    /**
//     * Weight unit.
//     *
//     * @since 2.5
//     */
//    protected WeightUnit weightUnit;

    /**
     * id of the unsorted qualitative value to remove of V/HV universe.
     *
     * @since 2.6
     */
    protected Integer qualitative_unsorted_id;

    protected SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    public SpeciesBatchUIHandler() {
        super(SpeciesBatchRowModel.PROPERTY_SPECIES_TO_CONFIRM,
              SpeciesBatchRowModel.PROPERTY_SPECIES,
              SpeciesBatchRowModel.PROPERTY_WEIGHT,
              SpeciesBatchRowModel.PROPERTY_NUMBER,
              SpeciesBatchRowModel.PROPERTY_COMMENT,
              SpeciesBatchRowModel.PROPERTY_ATTACHMENT,
              SpeciesBatchRowModel.PROPERTY_SAMPLE_CATEGORY_WEIGHT,
              SpeciesBatchRowModel.PROPERTY_FREQUENCY);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBatchTableUIHandler methods                           --//
    //------------------------------------------------------------------------//

    @Override
    protected ColumnIdentifier<SpeciesBatchRowModel> getCommentIdentifier() {
        return SpeciesBatchTableModel.COMMENT;
    }

    @Override
    protected ColumnIdentifier<SpeciesBatchRowModel> getAttachementIdentifier() {
        return SpeciesBatchTableModel.ATTACHMENT;
    }

    @Override
    public void selectFishingOperation(FishingOperation bean) {

        boolean empty = bean == null;

        SpeciesBatchUIModel model = getModel();

        List<SpeciesBatchRowModel> rows;

        if (empty) {
            rows = null;
        } else {

            if (log.isDebugEnabled()) {
                log.debug("Get species batch for fishingOperation: " +
                                  bean.getId());
            }
            rows = Lists.newArrayList();

            if (!TuttiEntities.isNew(bean)) {

                // get all batch species root (says the one with only a species sample category)
                BatchContainer<SpeciesBatch> rootSpeciesBatch = speciesOrBenthosBatchUISupport.getRootSpeciesBatch(bean.getIdAsInt());
//                        getPersistenceService().getRootSpeciesBatch(bean.getIdAsInt(), true);

                List<SpeciesBatch> catches = rootSpeciesBatch.getChildren();

                // use first category from configuration
                Integer firstCategoryId = sampleCategoryModel.getFirstCategoryId();

                for (SpeciesBatch aBatch : catches) {

                    // root batch sample category is species

                    Preconditions.checkState(
                            firstCategoryId.equals(aBatch.getSampleCategoryId()),
                            "Root species batch must be a sortedUnsorted sample " +
                                    "category but was:" + aBatch.getSampleCategoryId());

//                    SpeciesBatchRowModel rootRow =
                    loadBatch(aBatch, null, rows);

                    //FIXME kmorin 20140902 NPE decorator does not exist
//                    if (log.isDebugEnabled()) {
//                        log.debug("Loaded root batch " +
//                                  decorate(rootRow.getSpecies(), DecoratorService.FROM_PROTOCOL) + " - " +
//                                  decorate(rootRow.getSampleCategoryById(firstCategoryId)));
//                    }
                }
            }

            SpeciesBatchDecorator decorator = getSpeciesColumnDecorator();

            SpeciesSortMode speciesSortMode = model.getSpeciesSortMode();

            SpeciesBatchRowHelper.sortSpeciesRows(getTable(),
                                                  decorator,
                                                  rows,
                                                  speciesSortMode);
        }

        model.setRows(rows);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public SpeciesBatchTableModel getTableModel() {
        return (SpeciesBatchTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getTable();
    }

    @Override
    protected boolean isRowValid(SpeciesBatchRowModel row) {
        SpeciesBatch batch = convertRowToEntity(row, true);
        NuitonValidatorResult validator =
                getValidationService().validateEditSpeciesBatch(batch);
        boolean result = !validator.hasErrorMessagess();

        if (result
                && ValidationService.VALIDATION_CONTEXT_VALIDATE.equals(
                getContext().getValidationContext())
                && row.isBatchLeaf()) {

            List<SpeciesBatchFrequency> frequencies =
                    SpeciesFrequencyRowModel.toEntity(
                            row.getFrequency(),
                            batch);
            result = getValidateCruiseOperationsService().isSpeciesBatchValid(
                    batch,
                    frequencies);
        }

        return result;
    }

    @Override
    protected void onRowModified(int rowIndex,
                                 SpeciesBatchRowModel row,
                                 String propertyName,
                                 Object oldValue,
                                 Object newValue) {

        recomputeRowValidState(row);

        SpeciesBatchTableModel tableModel = getTableModel();

        if (SpeciesBatchRowModel.PROPERTY_SAMPLE_CATEGORY_WEIGHT.equals(propertyName)) {

            // sampling category weight has changed, must then save the top
            // ancestor row

            // new value is the sample category
            SampleCategory<?> sampleCategory = (SampleCategory<?>) newValue;
            Integer sampleCategoryId = sampleCategory.getCategoryId();

            SpeciesBatchRowModel firstAncestorRow = row.getFirstAncestor(sampleCategory);
            int firstAncestorIndex = tableModel.getRowIndex(firstAncestorRow);
            if (rowIndex != firstAncestorIndex) {

                // ancestor is not this row
                // then only save ancestor

                if (log.isDebugEnabled()) {
                    log.debug("Sample category " + sampleCategoryId +
                                      " weight was modified, First ancestor row: " +
                                      firstAncestorIndex + " will save it");
                }
                saveRow(firstAncestorRow);

                cleanrRowMonitor();

                return;
            }

            // modified sample weight is a leaf
            // will save it after
        } else if (SpeciesBatchRowModel.PROPERTY_SPECIES_TO_CONFIRM.equals(propertyName)) {

            // update his shell

            Set<SpeciesBatchRowModel> shell = Sets.newHashSet();
            row.collectShell(shell);

            boolean newVal = newValue == null ? false : (Boolean) newValue;

            for (SpeciesBatchRowModel rowToupdate : shell) {
                rowToupdate.setSpeciesToConfirm(newVal);
            }

            tableModel.fireTableRowUpdatedShell(shell);
        }

        saveSelectedRowIfNeeded();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<SpeciesBatchRowModel> rowMonitor,
                                             SpeciesBatchRowModel row) {
        // there is a valid bean attached to the monitor
        if (rowMonitor.wasModified()) {

            // monitored bean was modified, save it
            if (log.isDebugEnabled()) {
                log.debug("Row " + row + " was modified, will save it");
            }

            String title = buildReminderLabelTitle(row.getSpecies(),
                                                   row,
                                                   "Sauvegarde des modifications du lot Capture - Espèces : ",
                                                   "Ligne :" + (getTableModel().getRowIndex(row) + 1));

            showInformationMessage(title);

            rowMonitor.setBean(null);
            saveRow(row);
            rowMonitor.setBean(row);

            // clear modified flag on the monitor
            rowMonitor.clearModified();
        }
    }

    @Override
    protected void onModelRowsChanged(List<SpeciesBatchRowModel> rows) {
        super.onModelRowsChanged(rows);

        SpeciesBatchUIModel model = getModel();
        model.setRootNumber(0);
        model.setDistinctSortedSpeciesCount(0);
        model.setDistinctUnsortedSpeciesCount(0);

        for (SpeciesBatchRowModel row : rows) {
            updateTotalFromFrequencies(row);
            if (row.isBatchRoot()) {

                // update speciesUsed
                addToSpeciesUsed(row);
            }
        }

        getTable().clearSelection();
    }

    @Override
    protected void addHighlighters(JXTable table) {

        // use white color for not editable even row (to make sure white color is apply at least one)
        Highlighter evenHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.EVEN,
                        HighlightPredicate.EDITABLE),
                Color.WHITE);
        table.addHighlighter(evenHighlighter);

        super.addHighlighters(table);

        Color toConfirmColor = getConfig().getColorRowToConfirm();

        // paint the cell in orange if the row is to confirm
        Highlighter confirmHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.NotHighlightPredicate(HighlightPredicate.IS_SELECTED),
                        HighlightPredicate.EDITABLE,
                        (renderer, adapter) -> {
                            SpeciesBatchRowModel row = getTableModel().getEntry(adapter.convertRowIndexToModel(adapter.row));
                            return row.isSpeciesToConfirm();
                        }), toConfirmColor);
        table.addHighlighter(confirmHighlighter);

        // paint the cell in dark orange if the row is to confirm and the cell is not editable
        Highlighter confirmNotEditableHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.NotHighlightPredicate(HighlightPredicate.IS_SELECTED),
                        HighlightPredicate.READ_ONLY,
                        (renderer, adapter) -> {
                            SpeciesBatchRowModel row = getTableModel().getEntry(adapter.convertRowIndexToModel(adapter.row));
                            return row.isSpeciesToConfirm() && !adapter.isEditable();
                        }), toConfirmColor.darker());
        table.addHighlighter(confirmNotEditableHighlighter);
    }

    @Override
    protected void beforeOpenPopup(int rowIndex, int columnIndex) {
        super.beforeOpenPopup(rowIndex, columnIndex);

        boolean enableRename = false;
        boolean enableSplit = false;
        boolean enableChangeSampleCategory = false;
        boolean enableAddSampleCategory = false;
        boolean enableRemove = false;
        boolean enableRemoveSub = false;
        boolean enableCreateMelag = false;
        boolean enableEditFrequencies = false;

        if (rowIndex != -1) {

            // there is a selected row


            //TODO If there is some sub-batch, can remove them
            //TODO If there is no sub-batch, can split current batch

            SpeciesBatchTableModel tableModel = getTableModel();
            SpeciesBatchRowModel row = tableModel.getEntry(rowIndex);
            int selectedRowCount = getTable().getSelectedRowCount();

            // can edit frequencies on a single selected leaf row

            enableSplit = true;

            // action with single selection
            enableRemove = true;
            enableRemoveSub = selectedRowCount == 1;
            enableRename = selectedRowCount == 1;
            enableEditFrequencies = selectedRowCount == 1;
            enableChangeSampleCategory = selectedRowCount == 1 && tableModel.isCellEditable(rowIndex, columnIndex);
            enableAddSampleCategory = selectedRowCount == 1 && tableModel.isCellEditable(rowIndex, columnIndex);

            // action with multi-selection
            enableCreateMelag = selectedRowCount > 1;

            if (enableSplit) {

                // can split if selected batch is a leaf

                Integer lastSamplingId = sampleCategoryModel.getLastCategoryId();

                enableSplit = row.isBatchLeaf()
                        && selectedRowCount == 1
                        && ObjectUtils.notEqual(lastSamplingId, row.getFinestCategory().getCategoryId())
                        && row.getNumber() == null
                        && (row.getComputedNumber() == null
                        || row.getComputedNumber() == 0);
            }

            boolean firstCategory = false;
            Integer sampleCategoryId =
                    tableModel.getSampleCategoryId(columnIndex);

            SampleCategoryModelEntry category;

            List<CaracteristicQualitativeValue> available = null;

            if (sampleCategoryId != null) {

                // is first category ?
                firstCategory = sampleCategoryModel.getFirstCategoryId().equals(sampleCategoryId);

                // get category
                category = sampleCategoryModel.getCategoryById(sampleCategoryId);

                if (category.getCaracteristic().isNumericType()) {

                    // no category available
                    available = Collections.emptyList();
                } else {

                    // get the first ancestor row using this category
                    SpeciesBatchRowModel firstAncestorRow = row.getFirstAncestor(sampleCategoryId);

                    // get all used values for this category
                    Set<Serializable> used = getSampleUsedValues(
                            firstAncestorRow, sampleCategoryId);

                    // get all possible values
                    available = Lists.newArrayList(category.getCaracteristic().getQualitativeValue());
                    available.removeAll(used);

                    if (firstCategory) {

                        // remove the unsorted qualitative value
                        CaracteristicQualitativeValues.removeQualitativeValue(available, qualitative_unsorted_id);
                    }
                }
            }

            if (enableChangeSampleCategory) {

                // can change category if on a sample column and
                // there is still a brother category free

                if (sampleCategoryId == null) {

                    // not on a sample category column
                    enableChangeSampleCategory = false;
                } else {

                    // action possible only if there is still some available values
                    enableChangeSampleCategory = CollectionUtils.isNotEmpty(available);
                }
            }

            if (enableAddSampleCategory) {

                // can change category if on a sample column and
                // there is still a brother category free
                // and sample category is not the first

                if (sampleCategoryId == null || firstCategory) {

                    // not on a sample category column
                    // or using the first sample category (V/HV)
                    enableAddSampleCategory = false;
                } else {

                    // action possible only if there is still some available values
                    enableAddSampleCategory = CollectionUtils.isNotEmpty(available);
                }
            }

            if (enableEditFrequencies) {

                // can edit frequencies only on a leaf
                enableEditFrequencies = row.isBatchLeaf();
            }

            if (enableRename) {

                // can rename if selected batch is a parent and none of his shell has individual observations
                enableRename = row.isBatchRoot();

                if (enableRename) {

                    enableRename = !row.containsIndividualObservations();

                }
            }

            if (enableRemove) {

                // can always remove the batch

                // update the text of the remove batch menu
                String text, tip;

                if (selectedRowCount == 1) {
                    text = t("tutti.editSpeciesBatch.action.removeBatch");
                    tip = t("tutti.editBenthosBatch.action.removeBatch.tip");

                } else {
                    text = t("tutti.editSpeciesBatch.action.removeBatches");
                    tip = t("tutti.editSpeciesBatch.action.removeBatches.tip");
                }

                ui.getRemoveSpeciesBatchMenu().setText(text);
                ui.getRemoveSpeciesBatchMenu().setToolTipText(tip);
            }

            if (enableRemoveSub) {

                // can remove sub batch if selected batch is not a leaf
                enableRemoveSub = !row.isBatchLeaf();
            }

        }
        SpeciesBatchUIModel model = getModel();
        model.setSplitBatchEnabled(enableSplit);
        model.setChangeSampleCategoryEnabled(enableChangeSampleCategory);
        model.setAddSampleCategoryEnabled(enableAddSampleCategory);
        model.setRemoveBatchEnabled(enableRemove);
        model.setRemoveSubBatchEnabled(enableRemoveSub);
        model.setRenameBatchEnabled(enableRename);
        model.setCreateMelagEnabled(enableCreateMelag);
        model.setEditFrequenciesEnabled(enableEditFrequencies);

        if (log.isDebugEnabled()) {
            StringBuilder builder = new StringBuilder("actions for (" + rowIndex + "," + columnIndex + "):");
            builder.append("\nenableSplit:                ").append(enableSplit);
            builder.append("\nenableChangeSampleCategory: ").append(enableChangeSampleCategory);
            builder.append("\nenableAddSampleCategory:    ").append(enableAddSampleCategory);
            builder.append("\nenableRemove:               ").append(enableRemove);
            builder.append("\nenableRemoveSub:            ").append(enableRemoveSub);
            builder.append("\nenableRename:               ").append(enableRename);
            builder.append("\nenableCreateMelag:          ").append(enableCreateMelag);
            builder.append("\nenableEditFrequencies:      ").append(enableEditFrequencies);
            log.debug(builder.toString());
        }
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public SwingValidator<SpeciesBatchUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public void beforeInit(SpeciesBatchUI ui) {

        super.beforeInit(ui);
        if (log.isDebugEnabled()) {
            log.debug("beforeInit: " + ui);
        }

        qualitative_unsorted_id = QualitativeValueId.UNSORTED.getValue();

//        weightUnit = getConfig().getSpeciesWeightUnit();

        sampleCategoryModel = getDataContext().getSampleCategoryModel();

        tableFilters = new EnumMap<>(TableViewMode.class);

        tableFilters.put(TableViewMode.ALL, new RowFilter<SpeciesBatchTableModel, Integer>() {

            @Override
            public boolean include(Entry<? extends SpeciesBatchTableModel, ? extends Integer> entry) {
                return true;
            }
        });

        tableFilters.put(TableViewMode.ROOT, new RowFilter<SpeciesBatchTableModel, Integer>() {

            @Override
            public boolean include(Entry<? extends SpeciesBatchTableModel, ? extends Integer> entry) {
                boolean result = false;
                Integer rowIndex = entry.getIdentifier();
                if (rowIndex != null) {
                    SpeciesBatchTableModel model = entry.getModel();
                    SpeciesBatchRowModel row = model.getEntry(rowIndex);
                    result = row != null && row.isBatchRoot();
                }
                return result;
            }
        });

        tableFilters.put(TableViewMode.LEAF, new RowFilter<SpeciesBatchTableModel, Integer>() {

            @Override
            public boolean include(Entry<? extends SpeciesBatchTableModel, ? extends Integer> entry) {
                boolean result = false;
                Integer rowIndex = entry.getIdentifier();
                if (rowIndex != null) {
                    SpeciesBatchTableModel model = entry.getModel();
                    SpeciesBatchRowModel row = model.getEntry(rowIndex);
                    result = row != null && row.isBatchLeaf();
                }
                return result;
            }
        });

        speciesOrBenthosBatchUISupport = ui.getContextValue(SpeciesOrBenthosBatchUISupport.class, ui.getSpeciesOrBenthosContext());

        SpeciesBatchUIModel model = new SpeciesBatchUIModel(speciesOrBenthosBatchUISupport);
        model.setTableViewMode(TableViewMode.ALL);
        model.setSpeciesSortMode(SpeciesSortMode.NONE);
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(SpeciesBatchUI ui) {

        if (log.isDebugEnabled()) {
            log.debug("afterInit: " + ui);
        }

        WeightUnit weightUnit = getWeightUnit();

        initUI(ui);

        List<Integer> samplingOrder = sampleCategoryModel.getSamplingOrder();

        if (log.isDebugEnabled()) {
            log.debug("Will use sampling order: " + samplingOrder);
        }

        JXTable table = getTable();

        // can show / hide some columns in model
        table.setColumnControlVisible(true);

        // create table column model
        TableCellRenderer defaultRenderer =
                table.getDefaultRenderer(Object.class);

        DefaultTableColumnModelExt columnModel =
                new DefaultTableColumnModelExt();

        Decorator<CaracteristicQualitativeValue> caracteristicDecorator =
                getDecorator(CaracteristicQualitativeValue.class, null);

        Decorator<Number> numberDecorator =
                getDecorator(Number.class, null);

        Color computedDataColor = getConfig().getColorComputedWeights();

        { // Species to confirm column

            addBooleanColumnToModel(columnModel,
                                    SpeciesBatchTableModel.SPECIES_TO_CONFIRM,
                                    getTable());
        }

        {
            // Id column

            addIdColumnToModel(columnModel, SpeciesBatchTableModel.ID, table);

        }

        { // Species column

            TableColumnExt speciesColumn = addColumnToModel(
                    columnModel,
                    null,
                    null,
                    SpeciesBatchTableModel.SPECIES);
            speciesColumn.setSortable(true);
            SpeciesBatchDecorator<SpeciesBatchRowModel> speciesDecorator =
                    SpeciesBatchDecorator.newDecorator();
            speciesColumn.putClientProperty(SpeciesBatchRowHelper.SPECIES_DECORATOR, speciesDecorator);
            speciesColumn.setCellRenderer(newTableCellRender(speciesDecorator));
        }

        // Sample category columns

        for (SampleCategoryModelEntry sampleCategoryDef : sampleCategoryModel.getCategory()) {

            SampleCategoryColumnIdentifier columnIdentifier = SampleCategoryColumnIdentifier.newId(
                    sampleCategoryDef.getLabel(),
                    sampleCategoryDef.getCategoryId(),
                    n(sampleCategoryDef.getLabel()),
                    n(sampleCategoryDef.getLabel()));

            Decorator<? extends Serializable> decorator =
                    sampleCategoryDef.getCaracteristic().isNumericType() ?
                            numberDecorator : caracteristicDecorator;

            addSampleCategoryColumnToModel(columnModel,
                                           columnIdentifier,
                                           decorator,
                                           defaultRenderer,
                                           weightUnit);
        }

        { // Weight column

            addColumnToModel(columnModel,
                             ComputableDataTableCell.newEditor(weightUnit, computedDataColor),
                             ComputableDataTableCell.newRender(defaultRenderer, weightUnit, computedDataColor),
                             SpeciesBatchTableModel.WEIGHT,
                             weightUnit);
        }

        { // Number column (from frequencies)

            addColumnToModel(columnModel,
                             SpeciesFrequencyCellComponent.newEditor(ui, computedDataColor),
                             SpeciesFrequencyCellComponent.newRender(computedDataColor),
                             SpeciesBatchTableModel.COMPUTED_NUMBER);
        }

        { // Comment column

            addColumnToModel(columnModel,
                             CommentCellEditor.newEditor(ui),
                             CommentCellRenderer.newRender(),
                             SpeciesBatchTableModel.COMMENT);
        }

        { // File column

            addColumnToModel(columnModel,
                             AttachmentCellEditor.newEditor(ui),
                             AttachmentCellRenderer.newRender(getDecorator(Attachment.class, null)),
                             SpeciesBatchTableModel.ATTACHMENT);
        }

        // create table model
        SpeciesBatchTableModel tableModel =
                new SpeciesBatchTableModel(weightUnit,
                                           sampleCategoryModel,
                                           columnModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        initBatchTable(table, columnModel, tableModel);

        getModel().addPropertyChangeListener(SpeciesBatchUIModel.PROPERTY_TABLE_VIEW_MODE, evt -> {
            TableViewMode tableViewMode = (TableViewMode) evt.getNewValue();

            if (tableViewMode == null) {
                tableViewMode = TableViewMode.ALL;
            }

            if (log.isDebugEnabled()) {
                log.debug("Will use rowfilter for viewMode: " + tableViewMode);
            }
            RowFilter<SpeciesBatchTableModel, Integer> filter = tableFilters.get(tableViewMode);
            getTable().setRowFilter(filter);
        });

        // when species sort mode change, must reload the firshing operation
        // and applying the sort on model
        getModel().addPropertyChangeListener(SpeciesBatchUIModel.PROPERTY_SPECIES_SORT_MODE, evt -> {
            SpeciesSortMode newValue = (SpeciesSortMode) evt.getNewValue();
            if (log.isInfoEnabled()) {
                log.info("New species sort mode: " + newValue);
            }

            // must reload fishing operation
            selectFishingOperation(getModel().getFishingOperation());
        });

        // when species sort mode change, must reload the firshing operation
        // and applying the sort on model
        getModel().addPropertyChangeListener(SpeciesBatchUIModel.PROPERTY_SPECIES_DECORATOR_CONTEXT_INDEX, evt -> {
            int newValue = (int) evt.getNewValue();
            if (log.isInfoEnabled()) {
                log.info("New species decorator context index: " + newValue);
            }

            // update the decorator context index
            getSpeciesColumnDecorator().setContextIndex(newValue);

            // reload fishing operation
            selectFishingOperation(getModel().getFishingOperation());
        });
    }

    @Override
    protected void initBatchTable(final JXTable table,
                                  TableColumnModelExt columnModel,
                                  SpeciesBatchTableModel tableModel) {
        super.initBatchTable(table,
                             columnModel,
                             tableModel);

        // by default do not authorize to change column orders
        table.getTableHeader().setReorderingAllowed(false);

        // get the species column
        final TableColumnExt speciesColumn = table.getColumnExt(SpeciesBatchTableModel.SPECIES);

        // when model change, then rebuild the species comparator + set model as modified

        tableModel.addTableModelListener(e -> {

            SpeciesBatchTableModel tableModel1 =
                    (SpeciesBatchTableModel) e.getSource();
            int type = e.getType();
            if (type == TableModelEvent.DELETE ||
                    type == TableModelEvent.INSERT ||
                    e.getLastRow() == Integer.MAX_VALUE) {

                // get column comparator
                SpeciesBatchDecoratorComparator<SpeciesBatchRowModel> comparator =
                        getSpeciesRowComparator();

                // get column decorator
                SpeciesBatchDecorator<SpeciesBatchRowModel> decorator =
                        getSpeciesColumnDecorator();

                // init comparator with model species list
                comparator.init((SpeciesBatchDecorator) decorator, tableModel1.getRows());
            }
        });

        // create popup to change species decorator
        SpeciesBatchRowHelper.installSpeciesColumnComparatorPopup(
                table,
                speciesColumn,
                getModel(),
                t("tutti.species.surveyCode.tip"),
                t("tutti.species.name.tip")
        );
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getTable();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("Closing: " + ui);
        }
        ui.getSpeciesBatchAttachmentsButton().onCloseUI();
        clearValidators();
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public SampleCategoryModel getSampleCategoryModel() {
        return sampleCategoryModel;
    }

    public Integer getQualitative_unsorted_id() {
        return qualitative_unsorted_id;
    }

    public SpeciesBatchRowModel addBatch(CreateSpeciesBatchUIModel batchRootRowModel) {

        SpeciesBatchRowModel result = null;

        if (batchRootRowModel.isValid()) {

            SpeciesBatchTableModel tableModel = getTableModel();

            SpeciesBatchRowModel newRow = tableModel.createNewRow();
            Species species = batchRootRowModel.getSpecies();
            newRow.setSpecies(species);
            newRow.setNumber(batchRootRowModel.getBatchCount());
            CaracteristicQualitativeValue sampleCategory = batchRootRowModel.getSampleCategory();
            SampleCategory category = newRow.getFirstSampleCategory();
            category.setCategoryValue(sampleCategory);
            category.setCategoryWeight(batchRootRowModel.getBatchSampleCategoryWeight());
            newRow.setSampleCategory(category);
            newRow.setWeight(batchRootRowModel.getBatchWeight());

            recomputeRowValidState(newRow);

            saveRow(newRow);

            int insertIndex = SpeciesBatchRowHelper.getIndexToInsert(
                    tableModel.getRows(),
                    newRow,
                    getModel().getSpeciesSortMode(),
                    getSpeciesColumnDecorator());

            if (log.isDebugEnabled()) {
                log.debug("Will insert at index: " + insertIndex);
            }
            tableModel.addNewRow(insertIndex, newRow);
            JTables.doSelectCell(getTable(), insertIndex, 0);

            // update speciesUsed
            addToSpeciesUsed(newRow);

            if (batchRootRowModel.getBatchCount() == null &&
                    batchRootRowModel.getSelectedCategory() != null) {

                // add first category
                splitBatch(batchRootRowModel.getSelectedCategory(),
                           batchRootRowModel.getRows(),
                           batchRootRowModel.getSampleWeight()
                );
            }

            batchRootRowModel.setLastSampleCategoryUsed(batchRootRowModel.getSampleCategory());
            result = newRow;
        }

        return result;
    }

    public void splitBatch(SampleCategoryModelEntry sampleCategoryDef,
                           List<SplitSpeciesBatchRowModel> rows,
                           Float totalRowWeight) {
        JXTable table = getTable();

        // get selected row
        int insertRow = SwingUtil.getSelectedModelRow(table);

        SpeciesBatchTableModel tableModel = getTableModel();
        SpeciesBatchRowModel parentBatch = tableModel.getEntry(insertRow);

        // Create rows in batch table model

        //FIXME Weight check!!!
        Float parentWeight = parentBatch.getFinestCategory().getNotNullWeight();
        boolean subSample = parentWeight != null
                && totalRowWeight != null
                && getWeightUnit().isGreaterThan(parentWeight, totalRowWeight);
        List<SpeciesBatchRowModel> newBatches = Lists.newArrayList();
        for (SplitSpeciesBatchRowModel row : rows) {
            if (row.isValid()) {

                // can keep this row
                SpeciesBatchRowModel newBatch = tableModel.createNewRow();

                loadBatchRow(parentBatch,
                             newBatch,
                             sampleCategoryDef.getCategoryId(),
                             row.getCategoryValue(),
                             row.getWeight(),
                             null);

                newBatch.getFinestCategory().setSubSample(subSample);

                recomputeRowValidState(newBatch);
                newBatches.add(newBatch);

                tableModel.addNewRow(++insertRow, newBatch);
                JTables.selectFirstCellOnRow(getTable(), insertRow, false);
            }
        }

        // add new batches to his parent
        parentBatch.setChildBatch(newBatches);

        // save new batches
        saveRows(newBatches);

        SpeciesBatchUIModel model = getModel();
        model.setLeafNumber(model.getLeafNumber() + newBatches.size() - 1);
    }

    public void addSampleCategoryBatch(SpeciesBatchRowModel parentBatch,
                                       SampleCategoryModelEntry sampleCategoryDef,
                                       List<SplitSpeciesBatchRowModel> rows,
                                       Float totalRowWeight) {

        // get table model
        SpeciesBatchTableModel tableModel = getTableModel();

        // get insert row index
        int insertRow = tableModel.getNextChildRowIndex(parentBatch);

        // Create rows in batch table model
        List<SpeciesBatchRowModel> newBatches = Lists.newArrayList();
        for (SplitSpeciesBatchRowModel row : rows) {
            if (row.isEditable() && row.isValid()) {

                // can keep this row
                SpeciesBatchRowModel newBatch = tableModel.createNewRow();

                loadBatchRow(parentBatch,
                             newBatch,
                             sampleCategoryDef.getCategoryId(),
                             row.getCategoryValue(),
                             row.getWeight(),
                             null);

                recomputeRowValidState(newBatch);
                newBatches.add(newBatch);

                tableModel.addNewRow(insertRow++, newBatch);
            }
        }

        // add new batches to his parent
        List<SpeciesBatchRowModel> childBatch = parentBatch.getChildBatch();
        childBatch.addAll(newBatches);
        parentBatch.setChildBatch(childBatch);

        // re compute the sub sample property for all childs
        int categoryIndex = sampleCategoryModel.indexOf(sampleCategoryDef);

        //FIXME Weight check!!!
        Float parentWeight = parentBatch.getSampleCategoryByIndex(categoryIndex - 1).getNotNullWeight();
        boolean subSample = parentWeight != null && totalRowWeight != null
                && getWeightUnit().isGreaterThan(parentWeight, totalRowWeight);

        Set<SpeciesBatchRowModel> shell = Sets.newHashSet();
        parentBatch.collectShell(shell);

        for (SpeciesBatchRowModel rowModel : shell) {
            rowModel.getSampleCategoryByIndex(categoryIndex).setSubSample(subSample);
        }

        // save new batches
        saveRows(newBatches);

        // update model number of leaf
        SpeciesBatchUIModel model = getModel();
        model.setLeafNumber(model.getLeafNumber() + newBatches.size() - 1);

        // update columns for the parent shell
        tableModel.updateShell(shell, SwingUtil.getSelectedModelColumn(getTable()));
    }

    public void updateTotalFromFrequencies(SpeciesBatchRowModel row) {
        List<SpeciesFrequencyRowModel> frequency = row.getFrequency();

        Integer totalNumber = 0;
        boolean onlyOneFrequency = false;
        if (CollectionUtils.isNotEmpty(frequency)) {
            for (SpeciesFrequencyRowModel frequencyModel : frequency) {
                if (frequencyModel.getNumber() != null) {
                    totalNumber += frequencyModel.getNumber();
                }
            }
            onlyOneFrequency = frequency.size() == 1;
        }
        row.setComputedNumber(totalNumber);
        row.getFinestCategory().setOnlyOneFrequency(onlyOneFrequency);
    }

    public void saveRows(Iterable<SpeciesBatchRowModel> rows) {
        for (SpeciesBatchRowModel row : rows) {
            recomputeRowValidState(row);
            saveRow(row);
        }
    }

    public String getFilterSpeciesBatchRootButtonText(int rootNumber) {
        return t("tutti.editSpeciesBatch.filterBatch.mode.root", rootNumber);
    }

    public void collectChildren(SpeciesBatchRowModel row,
                                Set<SpeciesBatchRowModel> collectedRows) {

        if (!row.isBatchLeaf()) {

            for (SpeciesBatchRowModel batchChild : row.getChildBatch()) {
                collectedRows.add(batchChild);
                collectChildren(batchChild, collectedRows);
            }
        }
    }

    public SpeciesBatchRowModel loadBatch(SpeciesBatch aBatch,
                                          SpeciesBatchRowModel parentRow,
                                          List<SpeciesBatchRowModel> rows) {

        WeightUnit weightUnit = getWeightUnit();

        Integer id = aBatch.getIdAsInt();

        List<SpeciesBatchFrequency> frequencies = getPersistenceService().getAllSpeciesBatchFrequency(id);
        List<IndividualObservationBatch> individualObservations = getPersistenceService().getAllIndividualObservationBatchsForBatch(id);

        SpeciesBatchRowModel newRow =
                new SpeciesBatchRowModel(weightUnit,
                                         getConfig().getIndividualObservationWeightUnit(),
                                         sampleCategoryModel,
                                         aBatch,
                                         frequencies,
                                         individualObservations,
                                         getDataContext().getDefaultIndividualObservationCaracteristics());

        for (IndividualObservationBatchRowModel obsRow : newRow.getIndividualObservation()) {
            List<Attachment> attachments = getPersistenceService().getAllAttachments(obsRow.getObjectType(), obsRow.getObjectId());
            obsRow.addAllAttachment(attachments);
        }

        List<Attachment> attachments = getPersistenceService().getAllAttachments(newRow.getObjectType(), newRow.getObjectId());
        newRow.addAllAttachment(attachments);


        // set the surveycode, do it only on the parent,
        // the species of the parent is set to the children in loadBatchRow
        if (parentRow == null && getContext().isProtocolFilled()) {
            // get the surveycode from the species list of the model
            List<Species> speciesList = speciesOrBenthosBatchUISupport.getReferentSpeciesWithSurveyCode();
            int i = speciesList.indexOf(newRow.getSpecies());
            if (i > -1) {
                newRow.setSpecies(speciesList.get(i));
            }
        }

        Integer sampleCategoryId = aBatch.getSampleCategoryId();

        Preconditions.checkNotNull(
                sampleCategoryId,
                "Can't have a batch with no sample category, but was: " + aBatch);

        loadBatchRow(parentRow,
                     newRow,
                     sampleCategoryId,
                     aBatch.getSampleCategoryValue(),
                     weightUnit.fromEntity(aBatch.getSampleCategoryWeight()),
                     weightUnit.fromEntity(aBatch.getSampleCategoryComputedWeight()));

        rows.add(newRow);

        if (!aBatch.isChildBatchsEmpty()) {

            // create batch childs rows

            List<SpeciesBatchRowModel> batchChilds = Lists.newArrayListWithCapacity(aBatch.sizeChildBatchs());

            Float childrenWeights = 0f;
            for (SpeciesBatch childBatch : aBatch.getChildBatchs()) {
                SpeciesBatchRowModel childRow = loadBatch(childBatch, newRow, rows);
                if (childrenWeights != null) {
                    Float weight = childRow.getFinestCategory().getNotNullWeight();
                    if (weight == null) {
                        childrenWeights = null;
                    } else {
                        childrenWeights += weight;
                    }
                }

                batchChilds.add(childRow);
            }

            //FIXME Weight check!!!
            Float rowWeight = newRow.getFinestCategory().getNotNullWeight();
            boolean subSample = rowWeight != null
                    && childrenWeights != null
                    && weightUnit.isSmallerThan(childrenWeights, rowWeight);
            for (SpeciesBatchRowModel childRow : batchChilds) {
                childRow.getFinestCategory().setSubSample(subSample);
            }

            newRow.setChildBatch(batchChilds);
        }

        return newRow;
    }

    protected WeightUnit getWeightUnit() {
        return speciesOrBenthosBatchUISupport.getWeightUnit();
    }

    public void removeFromSpeciesUsed(SpeciesBatchRowModel row) {
        Preconditions.checkNotNull(row);
        Preconditions.checkNotNull(row.getSpecies());
        SampleCategory<?> firstSampleCategory = row.getFirstSampleCategory();
        CaracteristicQualitativeValue categoryValue = (CaracteristicQualitativeValue) firstSampleCategory.getCategoryValue();
        Preconditions.checkNotNull(firstSampleCategory);
        if (log.isDebugEnabled()) {
            log.debug("Remove from speciesUsed: " + decorate(categoryValue) + " - " + decorate(row.getSpecies()));
        }
        SpeciesBatchUIModel model = getModel();
        model.getSpeciesUsed().remove(categoryValue, row.getSpecies());

        if (row.isBatchRoot()) {
            model.setRootNumber(model.getRootNumber() - 1);

            if (QualitativeValueId.SORTED_VRAC.getValue().equals(categoryValue.getIdAsInt())) {
                model.decDistinctSortedSpeciesCount();
            } else if (QualitativeValueId.SORTED_HORS_VRAC.getValue().equals(categoryValue.getIdAsInt())) {
                model.decDistinctUnsortedSpeciesCount();
            }
        }
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    public void saveRow(SpeciesBatchRowModel row) {

        Preconditions.checkNotNull(row);
        Preconditions.checkNotNull(row.getSpecies());

        SpeciesBatch catchBean = row.toEntity();

        FishingOperation fishingOperation = getModel().getFishingOperation();
        Preconditions.checkNotNull(fishingOperation);
        catchBean.setFishingOperation(fishingOperation);

        SpeciesBatchRowModel parent = row.getParentBatch();
        if (parent != null) {
            catchBean.setParentBatch(parent.toEntity());
        }

        if (TuttiEntities.isNew(catchBean)) {

            Integer parentBatchId = null;
            if (parent != null) {
                parentBatchId = parent.getIdAsInt();
            }

            if (log.isDebugEnabled()) {
                log.debug("Persist new species batch with parentId: " + parentBatchId);
            }
            catchBean = speciesOrBenthosBatchUISupport.createBatch(catchBean, parentBatchId);
            row.setId(catchBean.getId());
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Persist existing species batch: " + catchBean.getId() + " (parent : " + catchBean.getParentBatch() + ")");
            }
            speciesOrBenthosBatchUISupport.saveBatch(catchBean);
        }

        fireBatchSaved(row);

        List<SpeciesFrequencyRowModel> frequencyRows = row.getFrequency();

        List<SpeciesBatchFrequency> frequency = SpeciesFrequencyRowModel.toEntity(frequencyRows, catchBean);

        if (log.isDebugEnabled()) {
            log.debug("Will save " + frequency.size() + " frequencies.");
        }
        frequency = speciesOrBenthosBatchUISupport.saveBatchFrequencies(catchBean.getIdAsInt(), frequency);

        // push it back to row model
        frequencyRows = SpeciesFrequencyRowModel.fromEntity(getWeightUnit(), frequency);
        row.setFrequency(frequencyRows);

        List<IndividualObservationBatchRowModel> obsRows = row.getIndividualObservation()
                                                              .stream()
                                                              .filter(obsRow -> !obsRow.isEmpty())
                                                              .collect(Collectors.toList());

        List<IndividualObservationBatch> obs = IndividualObservationBatchRowModel.toEntity(obsRows, catchBean);

        if (log.isDebugEnabled()) {
            log.debug("Will save " + obs.size() + " observations.");
        }
        obs = getPersistenceService().saveBatchIndividualObservation(catchBean.getIdAsInt(), obs);

        // push it back to row model
        List<IndividualObservationBatchRowModel> savedObsRows =
                IndividualObservationBatchRowModel.fromEntity(getConfig().getIndividualObservationWeightUnit(),
                                                              getDataContext().getDefaultIndividualObservationCaracteristics(),
                                                              obs);

        // save the not saved attachments
        for (int i = 0, n = obsRows.size(); i < n; i++) {

            IndividualObservationBatchRowModel obsRow = obsRows.get(i);
            IndividualObservationBatchRowModel savedObsRow = savedObsRows.get(i);

            List<Attachment> attachments = new ArrayList<>();

            for (Attachment attachment : obsRow.getAttachment()) {

                if (TuttiEntities.isNew(attachment)) {
                    File file = new File(attachment.getPath());
                    attachment.setObjectId(savedObsRow.getObjectId());
                    attachment = getPersistenceService().createAttachment(attachment, file);
                }

                attachments.add(attachment);

            }

            savedObsRow.addAllAttachment(attachments);
        }

        row.setIndividualObservation(savedObsRows);

    }

    protected void loadBatchRow(SpeciesBatchRowModel parentRow,
                                SpeciesBatchRowModel newRow,
                                Integer sampleCategoryId,
                                Serializable categoryValue,
                                Float categoryWeight,
                                Float categoryComputedWeight) {

        // get sample category from his type
        SampleCategory sampleCategory =
                newRow.getSampleCategoryById(sampleCategoryId);

        // fill it
        sampleCategory.setCategoryValue(categoryValue);
        sampleCategory.setCategoryWeight(categoryWeight);
        sampleCategory.setComputedWeight(categoryComputedWeight);

        // push it back to row as his *main* sample category
        newRow.setSampleCategory(sampleCategory);

        if (parentRow != null) {

            // copy back parent data (mainly other sample categories)

            newRow.setSpecies(parentRow.getSpecies());
            if (parentRow.isSpeciesToConfirm()) {

                // only set parent speciesToConfirm only if true in parent
                newRow.setSpeciesToConfirm(true);
            }
            newRow.setParentBatch(parentRow);

            newRow.setSpecies(parentRow.getSpecies());

            for (Integer id : sampleCategoryModel.getSamplingOrder()) {
                if (!id.equals(sampleCategoryId)) {
                    newRow.setSampleCategory(parentRow.getSampleCategoryById(id));
                }
            }
        }
    }

    protected <C extends Serializable> void addSampleCategoryColumnToModel(TableColumnModel columnModel,
                                                                           ColumnIdentifier<SpeciesBatchRowModel> columnIdentifier,
                                                                           Decorator<C> decorator,
                                                                           TableCellRenderer defaultRenderer,
                                                                           WeightUnit weightUnit) {
        addColumnToModel(
                columnModel,
                SampleCategoryComponent.newEditor(decorator, weightUnit),
                SampleCategoryComponent.newRender(defaultRenderer,
                                                  decorator,
                                                  getConfig().getColorComputedWeights(),
                                                  weightUnit),
                columnIdentifier,
                weightUnit);
    }

    protected void addToSpeciesUsed(SpeciesBatchRowModel row) {
        Preconditions.checkNotNull(row);
        Preconditions.checkNotNull(row.getSpecies());
        SampleCategory<?> firstSampleCategory = row.getFirstSampleCategory();
        Preconditions.checkNotNull(firstSampleCategory);
        CaracteristicQualitativeValue categoryValue = (CaracteristicQualitativeValue) firstSampleCategory.getCategoryValue();
        if (log.isDebugEnabled()) {
            log.debug("Add to speciesUsed: " +
                              decorate(categoryValue) +
                              " - " + decorate(row.getSpecies()));
        }
        SpeciesBatchUIModel model = getModel();
        model.getSpeciesUsed().put(categoryValue, row.getSpecies());

        if (QualitativeValueId.SORTED_VRAC.getValue().equals(categoryValue.getIdAsInt())) {
            model.incDistinctSortedSpeciesCount();
        } else if (QualitativeValueId.SORTED_HORS_VRAC.getValue().equals(categoryValue.getIdAsInt())) {
            model.incDistinctUnsortedSpeciesCount();
        }

        model.setRootNumber(model.getRootNumber() + 1);
    }

    protected SpeciesBatch convertRowToEntity(SpeciesBatchRowModel row,
                                              boolean convertParent) {
        Preconditions.checkNotNull(row.getFinestCategory());
        Preconditions.checkNotNull(row.getFinestCategory().getCategoryId());
        Preconditions.checkNotNull(row.getFinestCategory().getCategoryValue());

        SpeciesBatch catchBean = row.toEntity();

        if (convertParent && row.getParentBatch() != null) {
            SpeciesBatch parent = convertRowToEntity(row.getParentBatch(), true);
            catchBean.setParentBatch(parent);
        }

        return catchBean;
    }

    /**
     * Return all the sample category values (of the given
     * {@code sampleCategoryId}) for all brothers of the given {@code row}.
     *
     * @param row              the row
     * @param sampleCategoryId id of the sample category to seek in brothers of the given row
     * @return all the sample category values (of the given
     * {@code sampleCategoryId}) for all brothers of the given {@code row}.
     */
    public Set<Serializable> getSampleUsedValues(SpeciesBatchRowModel row, int sampleCategoryId) {

        Set<Serializable> usedValues = Sets.newHashSet();
        List<SpeciesBatchRowModel> childs;
        if (row.isBatchRoot()) {

            // on a root must take all his brothers (but have no common ancestor...)
            Species species = row.getSpecies();
            childs = Lists.newArrayList();
            for (SpeciesBatchRowModel rowToScan : getModel().getRows()) {
                if (rowToScan.isBatchRoot() && species.equals(rowToScan.getSpecies())) {
                    childs.add(rowToScan);
                }
            }
        } else {
            // on a son, must take all the brother directly from his father
            SpeciesBatchRowModel parentBatch = row.getParentBatch();
            childs = parentBatch.getChildBatch();
        }

        for (SpeciesBatchRowModel child : childs) {
            SampleCategory<?> category = child.getSampleCategoryById(sampleCategoryId);
            usedValues.add(category.getCategoryValue());
        }
        return usedValues;
    }

    protected SpeciesBatchDecoratorComparator<SpeciesBatchRowModel> getSpeciesRowComparator() {
        TableColumnExt speciesColumn = getTable().getColumnExt(SpeciesBatchTableModel.SPECIES);

        SpeciesBatchDecoratorComparator<SpeciesBatchRowModel> comparator =
                (SpeciesBatchDecoratorComparator<SpeciesBatchRowModel>) speciesColumn.getComparator();

        SpeciesBatchDecorator<SpeciesBatchRowModel> decorator = getSpeciesColumnDecorator();

        boolean comparatorNull = comparator == null;
        if (comparatorNull) {

            // first time coming here, add the comparator
            comparator = (SpeciesBatchDecoratorComparator) decorator.getCurrentComparator();
        }

        if (comparatorNull) {

            // affect it to colum
            speciesColumn.setComparator(comparator);
        }
        return comparator;
    }

    protected SpeciesBatchDecorator<SpeciesBatchRowModel> getSpeciesColumnDecorator() {
        TableColumnExt speciesColumn = getTable().getColumnExt(SpeciesBatchTableModel.SPECIES);
        return (SpeciesBatchDecorator<SpeciesBatchRowModel>) SpeciesBatchRowHelper.getSpeciesColumnDecorator(speciesColumn);
    }

}
