package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class EditProtocolOperationFieldsTableModel extends AbstractApplicationTableModel<EditProtocolOperationFieldsRowModel> {

    public static final ColumnIdentifier<EditProtocolCaracteristicsRowModel> FIELD = ColumnIdentifier.newId(
            EditProtocolOperationFieldsRowModel.PROPERTY_FIELD,
            n("tutti.editProtocol.table.header.operationFields.field"),
            n("tutti.editProtocol.table.header.operationFields.field.tip"));

    public static final ColumnIdentifier<EditProtocolCaracteristicsRowModel> IMPORT_FILE_COLUMN = ColumnIdentifier.newId(
            EditProtocolOperationFieldsRowModel.PROPERTY_IMPORT_COLUMN,
            n("tutti.editProtocol.table.header.operationFields.importFileColumn"),
            n("tutti.editProtocol.table.header.operationFields.importFileColumn.tip"));

    private static final long serialVersionUID = 1L;

    public EditProtocolOperationFieldsTableModel(TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        setNoneEditableCols(FIELD);
    }

    @Override
    public EditProtocolOperationFieldsRowModel createNewRow() {
        return new EditProtocolOperationFieldsRowModel();
    }
}
