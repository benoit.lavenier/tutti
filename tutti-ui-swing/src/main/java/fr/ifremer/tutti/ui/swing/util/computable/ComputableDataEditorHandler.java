package fr.ifremer.tutti.ui.swing.util.computable;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.widgets.number.NumberEditorHandler;
import org.nuiton.jaxx.widgets.number.NumberEditorModel;

import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeListener;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class ComputableDataEditorHandler extends NumberEditorHandler {

    private WeightUnit weightUnit;

    @Override
    public void init() {
        final PropertyChangeListener l = evt -> setComputedTextIfNullModel();

        ComputableData computableData = (ComputableData) ui.getModel().getBean();
        if (computableData != null) {
            computableData.addPropertyChangeListener(l);
        }
        ui.getModel().addPropertyChangeListener(NumberEditorModel.PROPERTY_BEAN, evt -> {

            ComputableData oldComputableData = (ComputableData) evt.getOldValue();
            if (oldComputableData != null) {
                oldComputableData.removePropertyChangeListener(ComputableData.PROPERTY_COMPUTED_DATA, l);
            }

            ComputableData newComputableData = (ComputableData) evt.getNewValue();
            if (newComputableData != null) {
                newComputableData.removePropertyChangeListener(ComputableData.PROPERTY_COMPUTED_DATA, l);
                newComputableData.addPropertyChangeListener(ComputableData.PROPERTY_COMPUTED_DATA, l);
            }
        });

        //FIXME NumberEditor
//        ui.addPropertyChangeListener(ComputableDataEditor.PROPERTY_MODEL, l);

        ui.getTextField().addFocusListener(new FocusListener() {

            public void focusGained(FocusEvent e) {
                JTextField tf = ui.getTextField();
                tf.setFont(TuttiUI.TEXTFIELD_NORMAL_FONT);
                tf.setForeground(Color.BLACK);
                if (ui.getModel().getNumberValue() == null) {
                    tf.setText("");
                }
            }

            public void focusLost(FocusEvent e) {
                setComputedTextIfNullModel();
            }
        });

        ui.addPropertyChangeListener(ComputableDataEditor.PROPERTY_WEIGHT_UNIT, evt -> {
            weightUnit = (WeightUnit) evt.getNewValue();
            ui.setNumberPattern(weightUnit.getNumberEditorPattern());
        });

        super.init();

    }

    /**
     * Pour afficher le 0.0 en bleu italique  si le moèdle est null.
     * On passera à faux lors d'un reset (voir http://forge.codelutin.com/issues/7088)
     *
     * @since 4.0.2
     */
    boolean displayNullComputedValue = true;

    @Override
    public void reset() {

        displayNullComputedValue = false;

        try {

            setTextValue("");

        } finally {

            displayNullComputedValue = true;

        }

    }

    protected void setComputedTextIfNullModel() {
        ComputableData bean = (ComputableData) ui.getModel().getBean();
        JTextField tf = ui.getTextField();
//        if (bean != null && ui.getModel() == null) {
        //FIXME NumberEditor
        if (bean != null && bean.getData() == null && displayNullComputedValue && !tf.isFocusOwner()) {
            tf.setFont(TuttiUI.TEXTFIELD_COMPUTED_FONT);
            tf.setForeground(((ComputableDataEditor) ui).getComputedDataColor());

            String modelText;
            Number computedData = bean.getComputedData();
            if (weightUnit != null) {
                modelText = weightUnit.renderWeight((Float) computedData);
            } else {
                modelText = JAXXUtil.getStringValue(computedData);
            }
//            if (ui.isUseFloat() && decimalNumber != null && computedData != null) {
//            if (decimalNumber != null && computedData != null) {
//                DecimalFormat decimalFormat = Weights.getDecimalFormat(1, decimalNumber);
//                modelText = decimalFormat.format(computedData);
//            } else {
//                modelText = JAXXUtil.getStringValue(computedData);
//            }
            tf.setText(modelText);

        } else {
            tf.setFont(TuttiUI.TEXTFIELD_NORMAL_FONT);
            tf.setForeground(Color.BLACK);
        }
    }

}
