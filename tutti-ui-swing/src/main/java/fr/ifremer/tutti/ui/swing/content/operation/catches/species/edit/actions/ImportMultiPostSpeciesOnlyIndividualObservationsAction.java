package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class ImportMultiPostSpeciesOnlyIndividualObservationsAction extends ImportMultiPostSpeciesSupportAction {

    public ImportMultiPostSpeciesOnlyIndividualObservationsAction(SpeciesBatchUIHandler handler) {
        super(handler);
    }

    @Override
    public boolean isImportFrequencies() {
        return false;
    }

    @Override
    public boolean isImportIndivudalObservations() {
        return true;
    }

    @Override
    protected String getFileExtension() {
        return "tuttiSpeciesOnlyIndividualObservations";
    }

    @Override
    protected String getFileExtensionDescription() {
        return t("tutti.common.file.tuttiSpeciesOnlyIndividualObservations");
    }

    @Override
    protected String getFileChooserTitle() {
        return t("tutti.editSpeciesBatch.action.importMultiPostOnlyIndividualObservations.sourceFile.title");
    }

    @Override
    protected String getFileChooserButton() {
        return t("tutti.editSpeciesBatch.action.importMultiPostOnlyIndividualObservations.sourceFile.button");
    }

    @Override
    protected String getSuccessMessage(File file) {
        return t("tutti.editSpeciesBatch.action.importMultiPostOnlyIndividualObservations.success", file);
    }

}
