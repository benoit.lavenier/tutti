package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.cruise.CruiseCache;
import fr.ifremer.tutti.service.sampling.CalcifiedPiecesSamplingAlgorithmEntryNotFoundException;
import fr.ifremer.tutti.service.sampling.CruiseSamplingCache;
import fr.ifremer.tutti.service.sampling.IndividualObservationSamplingCacheRequest;
import fr.ifremer.tutti.service.sampling.IndividualObservationSamplingStatus;
import fr.ifremer.tutti.service.sampling.SizeNotDefinedOnIndividualObservationException;
import fr.ifremer.tutti.service.sampling.ZoneNotDefinedOnFishingOperationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Closeable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Cache des observations individuelles.
 *
 * Gère notamment le cache de prélèvement des pièces calcifiés, mais aussi le cache des codes de prélèvements.
 *
 * Created on 19/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationUICache implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(IndividualObservationUICache.class);

    private final CruiseSamplingCache cruiseSamplingCache;
    private final boolean useCruiseSamplingCache;
    private final SpeciesFrequencyUIModel uiModel;

    private boolean speciesDefinedInCalcifiedPiecesSampling;
    private Zone fishingOperationZone;

    public IndividualObservationUICache(CruiseCache cruiseCache, SpeciesFrequencyUIModel uiModel) {
        this.useCruiseSamplingCache = cruiseCache.useSamplingCache();
        this.cruiseSamplingCache = cruiseCache.getSamplingCruiseCache().orElse(null);
        this.uiModel = uiModel;
    }

    public boolean useCruiseSamplingCache() {
        return useCruiseSamplingCache;
    }

    public boolean isSpeciesDefinedInCalcifiedPiecesSampling() {
        return speciesDefinedInCalcifiedPiecesSampling;
    }

    public boolean isFishingOperationWithZone() {
        return fishingOperationZone != null;
    }

    public void initFishingOperation(FishingOperation fishingOperation) {

        Objects.requireNonNull(fishingOperation);
        if (useCruiseSamplingCache) {

            // calcule de la zone associée à l'opération de pêche
            Optional<Zone> optionalZone = cruiseSamplingCache.tryFindZone(fishingOperation);

            if (log.isInfoEnabled()) {
                log.info("Init fishing operation zone: " + optionalZone);
            }

            fishingOperationZone = optionalZone.orElse(null);

        }

    }

    public void init(Species species, List<IndividualObservationBatchRowModel> individualObservations, boolean addToCache) {

        Objects.requireNonNull(species);
        Objects.requireNonNull(individualObservations);

        if (useCruiseSamplingCache) {

            // est-ce que l'espèce du lot existe dans l'algortihme des pièces calcifiées
            speciesDefinedInCalcifiedPiecesSampling = cruiseSamplingCache.isSpeciesDefined(species);

        }

        if (addToCache) {

            addIndividualObservations(individualObservations);

        }

    }

    @Override
    public void close() {
        this.fishingOperationZone = null;
    }

    public IndividualObservationSamplingStatus getIndividualObservationSamplingStatus(IndividualObservationBatchRowModel row) throws CalcifiedPiecesSamplingAlgorithmEntryNotFoundException, SizeNotDefinedOnIndividualObservationException, ZoneNotDefinedOnFishingOperationException {

        Objects.requireNonNull(row);

        IndividualObservationSamplingCacheRequest samplingCacheRequest = uiModel.toSamplingCacheRequest(row);
        return cruiseSamplingCache.getIndividualObservationSamplingStatus(samplingCacheRequest);

    }

    public void addIndividualObservations(Collection<IndividualObservationBatchRowModel> individualObservationRows) {

        Objects.requireNonNull(individualObservationRows);

        individualObservationRows.stream()
                                 .filter(IndividualObservationBatchRowModel::withSize)
                                 .forEach(row -> {

                                     IndividualObservationSamplingCacheRequest samplingCacheRequest = uiModel.toSamplingCacheRequest(row);
                                     addIndividualObservation(samplingCacheRequest);

                                 });

    }

    public void removeIndividualObservations(Collection<IndividualObservationBatchRowModel> individualObservationRows) {

        Objects.requireNonNull(individualObservationRows);

        individualObservationRows.stream()
                                 .filter(IndividualObservationBatchRowModel::withSize)
                                 .forEach(row -> {

                                     IndividualObservationSamplingCacheRequest samplingCacheRequest = uiModel.toSamplingCacheRequest(row);

                                     try {
                                         removeIndividualObservation(samplingCacheRequest);
                                     } catch (Exception e) {
                                         if (log.isErrorEnabled()) {
                                             log.error("Could not remove individual observation", e);
                                         }
                                     }

                                 });

    }

    public void addIndividualObservation(IndividualObservationSamplingCacheRequest samplingCacheRequest) {

        Objects.requireNonNull(samplingCacheRequest);

        if (useCruiseSamplingCache && samplingCacheRequest.withLengthClass()) {

            cruiseSamplingCache.addIndividualObservation(samplingCacheRequest);

        }

    }

    public void removeIndividualObservation(IndividualObservationSamplingCacheRequest samplingCacheRequest) {

        Objects.requireNonNull(samplingCacheRequest);

        if (useCruiseSamplingCache && samplingCacheRequest.withLengthClass()) {

            cruiseSamplingCache.removeIndividualObservation(samplingCacheRequest);

        }

    }

    public void addSampling(IndividualObservationSamplingCacheRequest samplingCacheRequest) {

        Objects.requireNonNull(samplingCacheRequest);

        String samplingCode = samplingCacheRequest.getSamplingCode();
        Objects.requireNonNull(samplingCode);

        if (useCruiseSamplingCache && samplingCacheRequest.withLengthClass()) {

            cruiseSamplingCache.addSampling(samplingCacheRequest);

        }

    }

    public void removeSampling(IndividualObservationSamplingCacheRequest samplingCacheRequest) {

        Objects.requireNonNull(samplingCacheRequest);

        String samplingCode = samplingCacheRequest.getSamplingCode();
        Objects.requireNonNull(samplingCode);

        if (useCruiseSamplingCache && samplingCacheRequest.withLengthClass()) {

            cruiseSamplingCache.removeSampling(samplingCacheRequest);

        }

    }

}
