package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractBean;
import org.nuiton.util.beans.BeanMonitor;
import org.nuiton.util.beans.PropertyDiff;

import java.util.Map;

/**
 * To monitor a bean and know when it changes.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class TuttiBeanMonitor<B> extends AbstractBean {

    public static final String PROPERTY_BEAN = "bean";

    BeanMonitor monitor;

    public TuttiBeanMonitor(String... properties) {
        this.monitor = new BeanMonitor(properties);
    }

    public void setProperties(String... properties) {
        monitor.setProperties(properties);
    }

    public B getBean() {
        return (B) monitor.getBean();
    }

    public void setBean(B bean) {
        Object oldValue = getBean();
        monitor.setBean(bean);
        firePropertyChange(PROPERTY_BEAN, oldValue, bean);
    }

    public BeanMonitor getMonitor() {
        return monitor;
    }

    public boolean wasModified() {
        return monitor.wasModified();
    }

    public String[] getModifiedProperties() {
        return monitor.getModifiedProperties();
    }

    public Map<String, Object> getOriginalValues() {
        return monitor.getOriginalValues();
    }

    public PropertyDiff[] getPropertyDiffs() {
        return monitor.getPropertyDiffs();
    }

    public void clearModified() {
        monitor.clearModified();
    }
}
