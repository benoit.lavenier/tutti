package fr.ifremer.tutti.ui.swing.util.catches;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/8/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.2
 */
public class EnterWeightUIHandler extends AbstractTuttiUIHandler<TuttiUIContext, EnterWeightUI> {

    public static final String CANCEL_ACTION = "cancelAction";

    public static final String VALIDATE_ACTION = "validateAction";

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<TuttiUIContext> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void afterInit(EnterWeightUI ui) {

        initUI(ui);

        JRootPane rootPane = ui.getRootPane();

        // add a auto-close action

        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), VALIDATE_ACTION);
        rootPane.getActionMap().put(VALIDATE_ACTION, ui.getValidateButton().getAction());

        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), CANCEL_ACTION);
        rootPane.getActionMap().put(CANCEL_ACTION, ui.getCancelButton().getAction());


        ui.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosed(WindowEvent e) {
                Component ui = (Component) e.getSource();
                JAXXUtil.destroy(ui);
            }
        });

        initUI(ui);


    }

    protected Float openAndGetWeightValue(String weightLabel, Float weight, WeightUnit weightUnit) {
        ui.setWeightLabel(weightLabel);
        ui.setOriginalWeight(weight);
        ui.setWeightUnit(weightUnit);
        SwingUtil.center(getContext().getMainUI(), ui);
        ui.pack();
        ui.getEditor().requestFocusInWindow();
        ui.setVisible(true);
        Number enteredWeight = ui.getEditor().getModel().getNumberValue();
        return enteredWeight == null ? null : enteredWeight.floatValue();

    }

    protected String getTitle(String weightLabel, WeightUnit weightUnit) {
        return weightUnit.decorateLabel(t("tutti.catches.enterWeight.title", weightLabel));
    }

}
