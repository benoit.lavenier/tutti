package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowHelper;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.util.species.EnterMelagWeightUI;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.StringUtils;

import javax.swing.JOptionPane;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0.2
 */
public class CreateSpeciesMelagAction extends LongActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    /**
     * Dictionnaire des lots du melag associé a leur poids d'echantillonnage (si un lot possède un poids de categorisation,
     * alors on lui associe la valeur {@code null} ici.
     */
    protected Map<SpeciesBatchRowModel, Float> selectedRows = Maps.newHashMap();

    /**
     * Poids du melag renseigne par l'utilisateur une fois les controles passés sur les lots du melag.
     */
    protected Float melagWeight;

    /**
     * Poids du melag moins la somme des poids de categorisations pour les lots du melag qui possede une poids de categorisation.
     *
     * Ce poids permet ensuite d'elever le poids d'echantillonage des lots du melag a leur poids de categorisation.
     */
    protected Float sampleMelagWeight;

    /**
     * Somme des poids d'echantillonage pour les lots du melag qui ne possède pas de poids de categorisation
     * mais un poids d'echantillonnage.
     *
     * Ce poids permet ensuite
     */
    protected Float sortedWeight;

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    public CreateSpeciesMelagAction(SpeciesBatchUIHandler handler) {
        super(handler, false);
        this.weightUnit = handler.getModel().getWeightUnit();
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean result = super.prepareAction();

        int[] selectedRowIndexes = SwingUtil.getSelectedModelRows(handler.getTable());

        SpeciesBatchTableModel tableModel = handler.getTableModel();

        // sum of the weights already entered by the user
        Float alreadyKnownWeights = 0f;
        sortedWeight = 0f;

        for (int selectedRowIndex : selectedRowIndexes) {

            SpeciesBatchRowModel selectedRow = tableModel.getEntry(selectedRowIndex);

            if (selectedRow.getFinestCategory().getNotNullWeight() != null) {

                // le lot possede un poid de categorisation, aucun traitement ne sera fait dessus
                // on conserve alors le poid connu
                alreadyKnownWeights += selectedRow.getFinestCategory().getNotNullWeight();
                // on conserve aussi le lot sans poids d'echantillonnage
                selectedRows.put(selectedRow, null);
                // on peut directement passer au lot suivant
                continue;

            }

            // on recherche le poids d'échantillonnage
            Float sampleWeight;

            if (selectedRow.getWeight() != null) {

                // le lot a un poids de sous echantillon
                sampleWeight = selectedRow.getWeight();

            } else {

                // on calcule la somme des individus peses s'il existent
                sampleWeight = SpeciesBatchRowHelper.getFrequenciesTotalWeight(selectedRow.getFrequency());

            }

            if (sampleWeight == null) {

                // pas de poids de sous-echantillon sur le lot
                // impossible de calculer le melag
                JOptionPane.showMessageDialog(
                        getDialogParentComponent(),
                        t("tutti.createSpeciesMelag.error.message", selectedRowIndex + 1),
                        t("tutti.createSpeciesMelag.error.title"),
                        JOptionPane.ERROR_MESSAGE);

                SwingUtil.setSelectionInterval(handler.getTable(), selectedRowIndex);
                result = false;
                // pas besoin de continuer a inspecter les autres lots
                break;

            }

            // le lot est utilisable dans le melag
            // on cumule le poids de sous echantillon
            sortedWeight += sampleWeight;

            // on conserve ce lot avec son poids de sous echantillon
            selectedRows.put(selectedRow, sampleWeight);

        }

        if (result) {

            // tous les lots indiques permettent le calcul du melag
            // on demande a l'utilisateur de saisir le poids total du melag
            EnterMelagWeightUI dialog = new EnterMelagWeightUI(getContext());
            melagWeight = dialog.openAndGetWeightValue(weightUnit);

            // on peut continuer l'action uniquement si l'utilisateur a saisi un poids
            result = melagWeight != null;

        }

        if (result) {

            // substract the weights that the user already entered
            // they must not be used to compute the other weights
            sampleMelagWeight = melagWeight - alreadyKnownWeights;

        }

        return result;

    }

    @Override
    public void doAction() throws Exception {

        String unitLabel = weightUnit.getShortLabel();
        String melagComment = t("tutti.createSpeciesMelag.comment.part1") + "\n";

        // ecriture du commentaire a positionner sur chaque lot qui fait parti du melag
        // et calcul du poids de categorisation sur les lots qui ont un poids de sous-echantillon precedemment renseigne
        for (SpeciesBatchRowModel batch : selectedRows.keySet()) {

            Float sampleWeight = selectedRows.get(batch);
            if (sampleWeight == null) {

                // on considere que le poids de sous echantillon est le poids de categorisation ?
                sampleWeight = batch.getFinestCategory().getNotNullWeight();

            } else {

                // ce lot possede un poids d'echantillonage mais pas de poids de categorisation
                // on calcul ce poids de categorisation et on lui affecte
                float categoryWeight = WeightUnit.KG.round(sampleMelagWeight * sampleWeight / sortedWeight);
                batch.getFinestCategory().setCategoryWeight(categoryWeight);

            }
            melagComment += t("tutti.createSpeciesMelag.comment.part2", sampleWeight, unitLabel, decorate(batch.getSpecies())) + "\n";

        }
        melagComment += t("tutti.createSpeciesMelag.comment.part3", melagWeight, unitLabel);

        // concatenation du commentaire sur chaque lot du melag

        for (SpeciesBatchRowModel batch : selectedRows.keySet()) {
            String comment = batch.getComment();
            if (StringUtils.isBlank(comment)) {
                comment = "";
            } else {
                comment += "\n";
            }
            comment += melagComment;
            batch.setComment(comment);
        }

        // sauvegarde des lots
        getHandler().saveRows(selectedRows.keySet());

    }

    @Override
    public void releaseAction() {
        selectedRows.clear();
        sampleMelagWeight = null;
        melagWeight = null;
        sortedWeight = null;
        super.releaseAction();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        getHandler().getTable().repaint();

    }
}
