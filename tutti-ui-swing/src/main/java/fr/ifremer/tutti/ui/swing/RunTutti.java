package fr.ifremer.tutti.ui.swing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.ui.swing.content.MainUI;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.actions.StartAction;
import fr.ifremer.tutti.ui.swing.update.actions.UpdateApplicationAction;
import fr.ifremer.tutti.ui.swing.update.actions.UpdateReportAction;
import fr.ifremer.tutti.ui.swing.util.TuttiExceptionHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.BorderUIResource;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;

/**
 * To start Tutti application.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class RunTutti {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RunTutti.class);

    public static final int RESTART_EXIT_CODE = 88;

    public static final int STOP_EXIT_CODE = 0;

    public static void main(String... args) {

        if (log.isInfoEnabled()) {
            log.info("Starting Tutti with arguments: " + Arrays.toString(args));
        }

        // Create configuration
        TuttiConfiguration config = new TuttiConfiguration("tutti.config", args);

        // Create application context
        final TuttiUIContext context = TuttiUIContext.newContext(config);

        // override default exception management (after config init)
        Thread.setDefaultUncaughtExceptionHandler(new TuttiExceptionHandler(context.getErrorHelper()));
        // See http://forge.codelutin.com/issues/2055
        //System.setProperty("sun.awt.exception.handler", TuttiExceptionHandler.class.getName());

        // prepare context (mainly init configs, i18n)
        context.init();

        // Prepare ui look&feel and load ui properties
        try {
            SwingUtil.initNimbusLoookAndFeel();
            UIManager.getLookAndFeelDefaults().put("ScrollBar.minimumThumbSize", new Dimension(30, 30));
        } catch (Exception e) {
            // could not find nimbus look-and-feel
            if (log.isWarnEnabled()) {
                log.warn("Failed to init nimbus look and feel", e);
            }
        }

        boolean reload = false;

        if (config.isFullLaunchMode()) {

            if (log.isInfoEnabled()) {
                log.info("Full launch mode, try to update.");
            }

            // check application url is reachable
            boolean canUpdateApplication = context.checkUpdateApplicationReachable(true);

            if (canUpdateApplication) {
                // try to update jre - i18n - application - help and exit if so
                UpdateApplicationAction logicAction = context.getActionFactory().createLogicAction(new MainUIHandler() {

                    @Override
                    public TuttiUIContext getContext() {
                        return context;
                    }
                }, UpdateApplicationAction.class);
                context.getActionEngine().runActionAndWait(logicAction);

                reload = logicAction.isReload();
            }

            // check data url is reachable
            boolean canUpdateData = context.checkUpdateDataReachable(true);

            if (canUpdateData) {
                // try to update report and exit if so
                UpdateReportAction logicAction = context.getActionFactory().createLogicAction(new MainUIHandler() {

                    @Override
                    public TuttiUIContext getContext() {
                        return context;
                    }
                }, UpdateReportAction.class);
                context.getActionEngine().runActionAndWait(logicAction);

                reload |= logicAction.isReload();
            }
        }

        if (!reload) {
            if (log.isInfoEnabled()) {
                log.info("Will start Tutti...");
            }
            startTutti(context, true);
        }
    }

    public static void startTutti(TuttiUIContext context, boolean openContext) {

        if (openContext) {
            context.open();
        }

        UIManager.put("Table.alternateRowColor", context.getConfig().getColorAlternateRow());
        UIManager.put("Table[Disabled+Selected].textBackground", context.getConfig().getColorSelectedRow());
        UIManager.put("Table[Enabled+Selected].textBackground", context.getConfig().getColorSelectedRow());
        UIManager.put("Table.focusCellHighlightBorder", new BorderUIResource.LineBorderUIResource(Color.BLACK));

        final MainUI mainUI = new MainUI(context);
        context.addMessageNotifier(mainUI.getHandler());

        SwingUtilities.invokeLater(() -> mainUI.setVisible(true));

        // launch start action (use the tutti-start-action file)
        StartAction uiAction = context.getActionFactory().createLogicAction(
                mainUI.getHandler(), StartAction.class);
        context.getActionEngine().runAction(uiAction);
    }

    public static void closeTutti(MainUIHandler handler, Integer exitCode) {

        TuttiUIContext context = handler.getContext();

        // close ui
        handler.onCloseUI();

        //close context
        context.saveSwingSession();
        context.close();

        if (exitCode != null) {
            System.exit(exitCode);
        }
    }
}
