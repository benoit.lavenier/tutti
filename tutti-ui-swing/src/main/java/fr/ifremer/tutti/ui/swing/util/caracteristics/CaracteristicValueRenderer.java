package fr.ifremer.tutti.ui.swing.util.caracteristics;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import jaxx.runtime.swing.renderer.DecoratorTableCellRenderer;
import org.nuiton.decorator.Decorator;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

/**
 * Renderer for the values of the caracteristics of the fishing operations.
 * The renderer depends on the caracteristic value type.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 0.3
 */
public class CaracteristicValueRenderer implements TableCellRenderer {

    protected int caracteristicColumn;

    protected Decorator<CaracteristicQualitativeValue> decorator;

    public CaracteristicValueRenderer(TuttiUIContext context) {
        this(0, context);
    }

    public CaracteristicValueRenderer(int caracteristicColumn, TuttiUIContext context) {
        super();
        this.caracteristicColumn = caracteristicColumn;
        DecoratorService decoratorService = context.getDecoratorService();
        decorator = decoratorService.getDecoratorByType(CaracteristicQualitativeValue.class);
    }

    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        TableCellRenderer renderer;

        Caracteristic caracteristic = (Caracteristic)
                table.getModel().getValueAt(row, caracteristicColumn);

        boolean numericType = false;
        if (caracteristic == null) {

            // should be render a null value ?
            renderer = table.getDefaultRenderer(Object.class);

        } else {
            switch (caracteristic.getCaracteristicType()) {

                case QUALITATIVE:
                    renderer = new DecoratorTableCellRenderer(decorator);
                    break;

                case NUMBER:

                    // use default text renderer with align at east
                    renderer = table.getDefaultRenderer(Object.class);
                    numericType = true;
                    break;
                default:
                case TEXT:
                    // use default text renderer
                    renderer = table.getDefaultRenderer(Object.class);
            }
        }

        Component result = renderer.getTableCellRendererComponent(
                table, value, isSelected, hasFocus, row, column);

        if (numericType) {

            if (result instanceof JLabel) {
                JLabel jLabel = (JLabel) result;
                jLabel.setHorizontalAlignment(SwingConstants.RIGHT);

            }
        } else {
            if (result instanceof JLabel) {
                JLabel jLabel = (JLabel) result;
                jLabel.setHorizontalAlignment(SwingConstants.LEFT);
            }
        }

        return result;
    }

}
