package fr.ifremer.tutti.ui.swing.content.referential.replace.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUI;
import fr.ifremer.tutti.ui.swing.content.referential.replace.AbstractReplaceTemporaryUIModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public abstract class AbstractReplaceTemporaryUIAction<E extends TuttiReferentialEntity, M extends AbstractReplaceTemporaryUIModel<E>, UI extends TuttiUI<M, ?>, H extends AbstractTuttiUIHandler<M, UI>> extends LongActionSupport<M, UI, H> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractReplaceTemporaryUIAction.class);

    protected E source;

    protected E target;

    protected Boolean delete;

    protected abstract String getEntityLabel();

    protected abstract void replaceReferentialEntity(PersistenceService persistenceService, E source, E target, boolean delete);

    protected abstract void updateNumberOfTemporaryEntities(ManageTemporaryReferentialUI mainUi);

    protected AbstractReplaceTemporaryUIAction(H handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {


        boolean doAction = super.prepareAction();

        if (doAction) {

            M model = getModel();
            doAction = model.isValid();

            if (doAction) {
                source = model.getSelectedSource();
                target = model.getSelectedTarget();
                delete = model.isDelete();
            }
        }

        getUI().getHandler().onCloseUI();

        return doAction;
    }

    @Override
    public void doAction() throws Exception {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(target);
        Preconditions.checkNotNull(delete);

        String entityLabel = getEntityLabel();

        if (log.isInfoEnabled()) {
            log.info(String.format("Will replace %s temporary: %s by: %s",
                                   entityLabel, source.getName(), target.getName()));
        }

        replaceReferentialEntity(getContext().getPersistenceService(), source, target, delete);

    }

    @Override
    public void releaseAction() {
        source = target = null;
        delete = null;
        super.releaseAction();
    }


    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (delete) {

            ManageTemporaryReferentialUI mainUi = getUI().getContextValue(ManageTemporaryReferentialUI.class, "owner");

            updateNumberOfTemporaryEntities(mainUi);
            sendMessage(t("tutti.replaceTemporaryAndDelete.done", getEntityLabel(), decorate(source), decorate(target)));
        } else {
            sendMessage(t("tutti.replaceTemporary.done", getEntityLabel(), decorate(source), decorate(target)));
        }

    }


    protected void reloadCruise() {
        if (getDataContext().isCruiseFilled()) {
            getDataContext().reloadCruise();
        }
    }

    protected void reloadFishingOperation() {
        if (getDataContext().isFishingOperationFilled()) {
            getDataContext().reloadFishingOperation();
        }
    }

}