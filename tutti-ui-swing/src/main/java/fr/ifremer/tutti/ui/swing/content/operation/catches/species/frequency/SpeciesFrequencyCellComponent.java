package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JTables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Component to render and edit frequency stuff from batch table.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class SpeciesFrequencyCellComponent extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    protected Color computedDataColor;

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesFrequencyCellComponent.class);

    private Font defaulfFont;

    private Font selectedFont;

    public SpeciesFrequencyCellComponent(Color computedDataColor) {
        setHorizontalAlignment(CENTER);
        setIcon(SwingUtil.createActionIcon("show-frequency"));
        this.computedDataColor = computedDataColor;
    }

    public void setComputedOrNotText(ComputableData<Integer> data) {
        String text;

        if (data != null && data.getData() != null) {
            text = String.valueOf(data.getData());

        } else if (data != null && data.getComputedData() != null && data.getComputedData() != 0) {

            String blue = Integer.toHexString(computedDataColor.getRGB()).substring(2);
            text = "<html><em style='color: #" + blue + "'>" + data.getComputedData() + "</em></html>";

        } else {
            text = " - ";
        }
        setText(text);
        setToolTipText(text);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {

        if (defaulfFont == null) {
            defaulfFont = UIManager.getFont("Table.font");
            selectedFont = defaulfFont.deriveFont(Font.BOLD);
        }

        Component result = super.getTableCellRendererComponent(table,
                                                               value,
                                                               isSelected,
                                                               hasFocus,
                                                               row,
                                                               column);
        if (isSelected) {
            result.setFont(selectedFont);
        } else {
            result.setFont(defaulfFont);
        }
        return result;
    }

    public static TableCellRenderer newRender(Color computedDataColor) {
        return new FrequencyCellRenderer(computedDataColor);
    }

    public static TableCellEditor newEditor(SpeciesBatchUI ui, Color computedDataColor) {
        return new FrequencyCellEditor(ui, computedDataColor);
    }

    public static class FrequencyCellEditor extends AbstractCellEditor implements TableCellEditor {

        private static final long serialVersionUID = 1L;

        protected final SpeciesFrequencyCellComponent component;

        protected final SpeciesBatchUI ui;

        protected JTable table;

        protected SpeciesBatchTableModel tableModel;

        protected ColumnIdentifier<SpeciesBatchRowModel> columnIdentifier;

        protected SpeciesBatchRowModel editRow;

        protected Integer rowIndex;

        protected Integer nextEditableRowIndex;

        protected Integer columnIndex;

        protected SpeciesBatchRowModel previousSiblingRow;

        public FrequencyCellEditor(SpeciesBatchUI ui, Color computedDataColor) {
            this.ui = ui;
            component = new SpeciesFrequencyCellComponent(computedDataColor);
            component.setBorder(new LineBorder(Color.BLACK));
            component.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE) {
                        e.consume();
                        startEdit();
                    }
                }
            });

            component.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    e.consume();
                    startEdit();
                }
            });
        }

        public SpeciesBatchRowModel getEditRow() {
            return editRow;
        }

        public Integer getNextEditableRowIndex() {
            return nextEditableRowIndex;
        }

        public SpeciesBatchRowModel getPreviousSiblingRow() {
            return previousSiblingRow;
        }

        public void initEditor(JTable table, int row, int column) {
            tableModel = (SpeciesBatchTableModel) table.getModel();
            this.table = table;
            columnIdentifier = SpeciesBatchTableModel.COMPUTED_NUMBER;
            rowIndex = row;
            columnIndex = column;
            editRow = tableModel.getEntry(row);
        }

        public void startEdit() {

            Objects.requireNonNull(tableModel, "No table model assigned.");
            Objects.requireNonNull(editRow, "No editRow found.");

            // compute the next editable row
            nextEditableRowIndex = tableModel.getNextEditableFrequencyRow(rowIndex + 1);

            // compute the previous sibling row
            previousSiblingRow = tableModel.getPreviousSibling(editRow);

            if (log.isDebugEnabled()) {
                log.debug("Will edit frequencies for row: " + rowIndex +
                                  ", nextEditableRow: " + nextEditableRowIndex +
                                  ", previous siblingRow: " + previousSiblingRow);
            }

            EditSpeciesBatchPanelUI parent = ui.getParentContainer(EditSpeciesBatchPanelUI.class);
            parent.getHandler().editSpeciesFrequencies(this);
        }

        public void save(SpeciesFrequencyUIModel frequencyModel) {

            if (frequencyModel.isValid()) {

                // at close, synch back frequencies

                // set the weigth
                editRow.setWeight(frequencyModel.getTotalWeight());

                if (frequencyModel.isSimpleCountingMode()) {

                    editRow.setNumber(frequencyModel.getSimpleCount());
                    // DO THIS BEFORE SETTING THE FREQUENCIES AS THE FREQUENCIES MODIFICATIONS TRIGGERS THE SAVE
                    editRow.setIndividualObservation(new ArrayList<>());
                    editRow.setFrequency(new ArrayList<>());

                } else {

                    // push back to batch
                    editRow.setNumber(null);

                    //
                    // transfert back individual observations
                    //

                    List<IndividualObservationBatchRowModel> individualObservation = new ArrayList<>();
//                    CopyIndividualObservationMode copyIndividualObservationMode = frequencyModel.getCopyIndividualObservationMode();
//
//                    row.setCopyIndividualObservationMode(copyIndividualObservationMode);
                    individualObservation.addAll(frequencyModel.getIndividualObservationModel().getRows());
                    if (log.isDebugEnabled()) {
                        log.debug("Push back " + individualObservation.size() + " observations to batch " + frequencyModel.getBatch());
                    }

                    // DO THIS BEFORE SETTING THE FREQUENCIES AS THE FREQUENCIES MODIFICATIONS TRIGGERS THE SAVE
                    // set individual observations to the incoming batch
                    editRow.setIndividualObservation(individualObservation);

                    //
                    // transfert back frequencies
                    //

                    // the row is valid even if there is no data (just for the highlighter)
                    // but we save it only if there is data
                    // can keep this row
                    List<SpeciesFrequencyRowModel> frequency =
                            frequencyModel.getRows()
                                          .stream()
                                          .filter(row -> row.isValid() && (row.withNumber() || row.withWeight()))
                                          .collect(Collectors.toList());

                    if (log.isDebugEnabled()) {
                        log.debug("Push back " + frequency.size() + " frequencies to batch " + frequencyModel.getBatch());
                    }

                    // set frequencies to the incoming batch
                    editRow.setFrequency(frequency);

                }

                // update frequencies total
                ui.getHandler().updateTotalFromFrequencies(editRow);
            }

            ui.getHandler().saveRow(editRow);

        }

        public void save(SpeciesFrequencyUIModel frequencyModel, boolean quit) {

            save(frequencyModel);

            if (quit) {
                int r = rowIndex;
                int c = columnIndex;

                // stop edition
                stopCellEditing();

                // reselect this cell
                JTables.doSelectCell(table, r, c);
                table.requestFocus();

            } else {

                // keep next cell to edit
                int nextR = nextEditableRowIndex;
                int c = columnIndex;

                // stop edition of this row
                stopCellEditing();

                // use now the next row data
                rowIndex = nextR;

                // load the row
                editRow = tableModel.getEntry(rowIndex);

                // will save the previous row in the species row
                JTables.doSelectCell(table, rowIndex, c);

                // start edit
                startEdit();
            }
        }

        @Override
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column) {

            initEditor(table, row, column);

            if (log.isDebugEnabled()) {
                log.debug("Set columnIdentifier (" + column + ") :: " + columnIdentifier.getPropertyName());
            }
            ComputableData<Integer> data = (ComputableData<Integer>) value;
            component.setComputedOrNotText(data);

            return component;
        }

        @Override
        public Object getCellEditorValue() {

            Objects.requireNonNull(editRow, "No editRow found in editor.");

            Object result = null;
            if (columnIdentifier == SpeciesBatchTableModel.COMPUTED_NUMBER) {
                result = editRow.getComputedOrNotNumber();
            }
            if (log.isDebugEnabled()) {
                log.debug("editor value (" + columnIdentifier + "): " + result);
            }

            return result;
        }

        @Override
        public void cancelCellEditing() {
            super.cancelCellEditing();
            rowIndex = null;
            nextEditableRowIndex = null;
            columnIndex = null;
            editRow = null;
        }
    }

    public static class FrequencyCellRenderer implements TableCellRenderer {

        protected final SpeciesFrequencyCellComponent component;

        public FrequencyCellRenderer(Color computedDataColor) {
            component = new SpeciesFrequencyCellComponent(computedDataColor);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            component.setForeground(Color.BLACK);
            ComputableData<Integer> data = (ComputableData<Integer>) value;
            SpeciesFrequencyCellComponent result =
                    (SpeciesFrequencyCellComponent) component.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            boolean editable = table.isCellEditable(row, column);
            result.setEnabled(editable);
            result.setComputedOrNotText(data);
            return result;
        }
    }
}
