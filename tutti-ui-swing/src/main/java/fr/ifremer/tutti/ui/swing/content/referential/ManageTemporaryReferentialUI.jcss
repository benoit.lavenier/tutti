/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#manageTemporaryReferentialTopPanel {
  _help: {"tutti.manageTemporaryReferential.help"};
}

#information {
  actionIcon: information;
  text: "tutti.manageTemporaryReferential.info.import.temporary.referential";
}

#speciesLabel {
  text: { t("tutti.manageTemporaryReferential.field.species", model.getNbTemporarySpecies()) };
  toolTipText: { t("tutti.manageTemporaryReferential.field.species.tip", model.getNbTemporarySpecies()) };
  icon: {SwingUtil.createImageIcon("action-species.gif")};
}

#speciesActionComboBox {
  _comboboxActions: {Arrays.asList(exportSpeciesExampleButton, exportExistingSpeciesButton, importSpeciesButton, replaceSpeciesButton)};
}

#exportSpeciesExampleButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportTemporarySpeciesExample";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportTemporarySpeciesExampleAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.exportTemporarySpeciesExample.tip";
  _help: {"tutti.manageTemporaryReferential.action.exportTemporarySpeciesExample.help"};
}

#exportExistingSpeciesButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportExistingTemporarySpecies";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportExistingTemporarySpeciesAction.class};
  enabled: { model.getNbTemporarySpecies() > 0 };
  toolTipText: "tutti.manageTemporaryReferential.action.exportExistingTemporarySpecies.tip";
  _help: {"tutti.manageTemporaryReferential.action.exportExistingTemporarySpecies.help"};
}

#importSpeciesButton {
  actionIcon: import;
  text: "tutti.manageTemporaryReferential.action.importTemporarySpecies";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ImportTemporarySpeciesAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.importTemporarySpecies.tip";
  _help: {"tutti.manageTemporaryReferential.action.importTemporarySpecies.help"};
}

#replaceSpeciesButton {
  actionIcon: replace;
  text: "tutti.manageTemporaryReferential.action.replaceTemporarySpecies";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.OpenReplaceTemporarySpeciesUIAction.class};
  enabled: { model.getNbTemporarySpecies() > 0 };
  toolTipText: "tutti.manageTemporaryReferential.action.replaceTemporarySpecies.tip";
  _help: {"tutti.manageTemporaryReferential.action.replaceTemporarySpecies.help"};
}

#vesselLabel {
  text: { t("tutti.manageTemporaryReferential.field.vessel", model.getNbTemporaryVessels()) };
  toolTipText: { t("tutti.manageTemporaryReferential.field.vessel.tip", model.getNbTemporaryVessels()) };
  actionIcon: vessel;
}

#vesselActionComboBox {
  _comboboxActions: {Arrays.asList(exportVesselExampleButton, exportExistingVesselButton, importVesselButton, replaceVesselButton)};
}

#exportVesselExampleButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportTemporaryVesselExample";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportTemporaryVesselExampleAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.exportTemporaryVesselExample.tip";
  _help: {"tutti.manageTemporaryReferential.action.exportTemporaryVesselExample.help"};
}

#exportExistingVesselButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportExistingTemporaryVessel";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportExistingTemporaryVesselAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.exportExistingTemporaryVessel.tip";
  enabled: { model.getNbTemporaryVessels() > 0 };
  _help: {"tutti.manageTemporaryReferential.action.exportExistingTemporaryVessel.help"};
}

#importVesselButton {
  actionIcon: import;
  text: "tutti.manageTemporaryReferential.action.importTemporaryVessel";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ImportTemporaryVesselAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.importTemporaryVessel.tip";
  _help: {"tutti.manageTemporaryReferential.action.importTemporaryVessel.help"};
}

#replaceVesselButton {
  actionIcon: replace;
  text: "tutti.manageTemporaryReferential.action.replaceTemporaryVessel";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.OpenReplaceTemporaryVesselUIAction.class};
  enabled: { model.getNbTemporaryVessels() > 0 };
  toolTipText: "tutti.manageTemporaryReferential.action.replaceTemporaryVessel.tip";
  _help: {"tutti.manageTemporaryReferential.action.replaceTemporaryVessel.help"};
}

#gearLabel {
  text: { t("tutti.manageTemporaryReferential.field.gear", model.getNbTemporaryGears()) };
  toolTipText: { t("tutti.manageTemporaryReferential.field.gear.tip", model.getNbTemporaryGears()) };
  actionIcon: gear;
}

#gearActionComboBox {
  _comboboxActions: {Arrays.asList(exportGearExampleButton, exportExistingGearButton, importGearButton, replaceGearButton)};
}

#exportGearExampleButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportTemporaryGearExample";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportTemporaryGearExampleAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.exportTemporaryGearExample.tip";
  _help: {"tutti.manageTemporaryReferential.action.exportTemporaryGearExample.help"};
}

#exportExistingGearButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportExistingTemporaryGear";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportExistingTemporaryGearAction.class};
  enabled: { model.getNbTemporaryGears() > 0 };
  toolTipText: "tutti.manageTemporaryReferential.action.exportExistingTemporaryGear.tip";
  _help: {"tutti.manageTemporaryReferential.action.exportExistingTemporaryGear.help"};
}

#importGearButton {
  actionIcon: import;
  text: "tutti.manageTemporaryReferential.action.importTemporaryGear";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ImportTemporaryGearAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.importTemporaryGear.tip";
  _help: {"tutti.manageTemporaryReferential.action.importTemporaryGear.help"};
}

#replaceGearButton {
  actionIcon: replace;
  text: "tutti.manageTemporaryReferential.action.replaceTemporaryGear";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.OpenReplaceTemporaryGearUIAction.class};
  enabled: { model.getNbTemporaryGears() > 0 };
  toolTipText: "tutti.manageTemporaryReferential.action.replaceTemporaryGear.tip";
  _help: {"tutti.manageTemporaryReferential.action.replaceTemporaryGear.help"};
}

#personLabel {
  text: { t("tutti.manageTemporaryReferential.field.person", model.getNbTemporaryPersons()) };
  toolTipText: { t("tutti.manageTemporaryReferential.field.person.tip", model.getNbTemporaryPersons()) };
  actionIcon: person;
}

#personActionComboBox {
  _comboboxActions: {Arrays.asList(exportPersonExampleButton, exportExistingPersonButton, importPersonButton, replacePersonButton)};
}

#exportExistingPersonButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportExistingTemporaryPerson";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportExistingTemporaryPersonAction.class};
  enabled: { model.getNbTemporaryPersons() > 0 };
  toolTipText: "tutti.manageTemporaryReferential.action.exportExistingTemporaryPerson.tip";
  _help: {"tutti.manageTemporaryReferential.action.exportExistingTemporaryPerson.help"};
}

#exportPersonExampleButton {
  actionIcon: export;
  text: "tutti.manageTemporaryReferential.action.exportTemporaryPersonExample";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ExportTemporaryPersonExampleAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.exportTemporaryPersonExample.tip";
  _help: {"tutti.manageTemporaryReferential.action.exportTemporaryPersonExample.help"};
}

#importPersonButton {
  actionIcon: import;
  text: "tutti.manageTemporaryReferential.action.importTemporaryPerson";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.ImportTemporaryPersonAction.class};
  toolTipText: "tutti.manageTemporaryReferential.action.importTemporaryPerson.tip";
  _help: {"tutti.manageTemporaryReferential.action.importTemporaryPerson.help"};
}

#replacePersonButton {
  actionIcon: replace;
  text: "tutti.manageTemporaryReferential.action.replaceTemporaryPerson";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.referential.actions.OpenReplaceTemporaryPersonUIAction.class};
  enabled: { model.getNbTemporaryPersons() > 0 };
  toolTipText: "tutti.manageTemporaryReferential.action.replaceTemporaryPerson.tip";
  _help: {"tutti.manageTemporaryReferential.action.replaceTemporaryPerson.help"};
}