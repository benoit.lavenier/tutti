package fr.ifremer.tutti.ui.swing.update;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.update.module.HelpModuleUpdater;
import fr.ifremer.tutti.ui.swing.update.module.I18NModuleUpdater;
import fr.ifremer.tutti.ui.swing.update.module.IchtyometerModuleUpdater;
import fr.ifremer.tutti.ui.swing.update.module.JreModuleUpdater;
import fr.ifremer.tutti.ui.swing.update.module.LauncherModuleUpdater;
import fr.ifremer.tutti.ui.swing.update.module.ModuleUpdaterSupport;
import fr.ifremer.tutti.ui.swing.update.module.TuttiModuleUpdater;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;

/**
 * CallBack to update all application modules.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TuttiApplicationUpdaterCallBack extends TuttiUpdaterCallBackSupport {

    public TuttiApplicationUpdaterCallBack(String url, LongActionSupport action, ProgressionModel progressionModel) {
        super(url,
              ImmutableMap.<UpdateModule, ModuleUpdaterSupport>builder()
                      .put(UpdateModule.launcher, new LauncherModuleUpdater())
                      .put(UpdateModule.jre, new JreModuleUpdater())
                      .put(UpdateModule.tutti, new TuttiModuleUpdater())
                      .put(UpdateModule.i18n, new I18NModuleUpdater())
                      .put(UpdateModule.help, new HelpModuleUpdater())
                      .put(UpdateModule.ichtyometer, new IchtyometerModuleUpdater())
                      .build(),
              action,
              progressionModel);
    }

}
