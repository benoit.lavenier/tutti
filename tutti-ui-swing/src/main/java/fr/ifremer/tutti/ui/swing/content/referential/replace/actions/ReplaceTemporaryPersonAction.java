package fr.ifremer.tutti.ui.swing.content.referential.replace.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUI;
import fr.ifremer.tutti.ui.swing.content.referential.replace.ReplaceTemporaryPersonUI;
import fr.ifremer.tutti.ui.swing.content.referential.replace.ReplaceTemporaryPersonUIHandler;
import fr.ifremer.tutti.ui.swing.content.referential.replace.ReplaceTemporaryPersonUIModel;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class ReplaceTemporaryPersonAction extends AbstractReplaceTemporaryUIAction<Person, ReplaceTemporaryPersonUIModel, ReplaceTemporaryPersonUI, ReplaceTemporaryPersonUIHandler> {

    public ReplaceTemporaryPersonAction(ReplaceTemporaryPersonUIHandler handler) {
        super(handler);
    }

    @Override
    protected void updateNumberOfTemporaryEntities(ManageTemporaryReferentialUI mainUi) {
        int nbRef = mainUi.getModel().getNbTemporaryPersons() - 1;
        mainUi.getModel().setNbTemporaryPersons(nbRef);
    }

    @Override
    protected String getEntityLabel() {
        return t("tutti.common.referential.person");
    }

    @Override
    protected void replaceReferentialEntity(PersistenceService persistenceService, Person source, Person target, boolean delete) {

        persistenceService.replacePerson(source, target, delete);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        reloadCruise();
        reloadFishingOperation();
    }

}