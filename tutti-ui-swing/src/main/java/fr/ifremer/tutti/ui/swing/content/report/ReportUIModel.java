package fr.ifremer.tutti.ui.swing.content.report;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.report.ReportGenerationRequest;
import fr.ifremer.tutti.service.report.ReportGenerationResult;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.File;
import java.util.List;

/**
 * Model of {@link ReportUI} screen.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class ReportUIModel extends AbstractTuttiBeanUIModel<ReportGenerationRequest, ReportUIModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_REPORTS = "reports";

    public static final String PROPERTY_REPORT = "report";

    public static final String PROPERTY_FISHING_OPERATION = "fishingOperation";

    public static final String PROPERTY_REPORT_DONE = "reportDone";

    protected static Binder<ReportUIModel, ReportGenerationRequest> toBeanBinder =
            BinderFactory.newBinder(ReportUIModel.class,
                                    ReportGenerationRequest.class);

    protected List<File> reports;

    protected List<FishingOperation> fishingOperations;

    protected ReportGenerationResult reportGenerationResult;

    /** Report to use. */
    protected File report;

    protected String programId;

    protected Integer cruiseId;

    protected FishingOperation fishingOperation;

    public ReportUIModel() {
        super(null, toBeanBinder);
    }

    public List<File> getReports() {
        return reports;
    }

    public void setReports(List<File> reports) {
        Object oldValue = getReports();
        this.reports = reports;
        firePropertyChange(PROPERTY_REPORTS, oldValue, reports);
    }

    public List<FishingOperation> getFishingOperations() {
        return fishingOperations;
    }

    public void setFishingOperations(List<FishingOperation> fishingOperations) {
        this.fishingOperations = fishingOperations;
    }

    public File getReport() {
        return report;
    }

    public void setReport(File report) {
        Object oldValue = getReport();
        this.report = report;
        firePropertyChange(PROPERTY_REPORT, oldValue, report);
    }

    public String getProgramId() {
        return programId;
    }

    public void setProgramId(String programId) {
        this.programId = programId;
    }

    public Integer getCruiseId() {
        return cruiseId;
    }

    public void setCruiseId(Integer cruiseId) {
        this.cruiseId = cruiseId;
    }

    public Integer getFishingOperationId() {
        return fishingOperation == null ? null : fishingOperation.getIdAsInt();
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        Object oldValue = this.fishingOperation;
        this.fishingOperation = fishingOperation;
        firePropertyChange(PROPERTY_FISHING_OPERATION, oldValue, fishingOperation);
    }

    public void setReportGenerationResult(ReportGenerationResult reportGenerationResult) {
        this.reportGenerationResult = reportGenerationResult;
        firePropertyChange(PROPERTY_REPORT_DONE, null, isReportDone());
    }

    public File getOutputFile() {
        return reportGenerationResult.getOutputFile();
    }

    public boolean isReportDone() {
        return reportGenerationResult != null;
    }

    @Override
    protected ReportGenerationRequest newEntity() {
        return new ReportGenerationRequest();
    }
}
