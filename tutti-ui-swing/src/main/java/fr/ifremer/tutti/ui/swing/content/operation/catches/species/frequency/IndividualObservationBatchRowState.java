package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;

import java.util.Objects;

/**
 * Contient l'état d'une observation individuelle utilisée par les modes de recopie pour savoir ce qu'il faut
 * déporter dans les mensurations.
 *
 * Created on 15/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationBatchRowState {

    private final Float size;
    private final Float weight;
    private final CaracteristicQualitativeValue maturity;
    private final CaracteristicQualitativeValue gender;
    private final String samplingCode;
    private final boolean valid;

    public IndividualObservationBatchRowState(Float size,
                                              Float weight,
                                              CaracteristicQualitativeValue maturity,
                                              CaracteristicQualitativeValue gender,
                                              String samplingCode,
                                              boolean valid) {
        this.size = size;
        this.weight = weight;
        this.maturity = maturity;
        this.gender = gender;
        this.samplingCode = samplingCode;
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }

    public Float getSize() {
        return size;
    }

    public Float getWeight() {
        return weight;
    }

    public CaracteristicQualitativeValue getMaturity() {
        return maturity;
    }

    public CaracteristicQualitativeValue getGender() {
        return gender;
    }

    public String getSamplingCode() {
        return samplingCode;
    }

    public boolean withSamplingCode() {
        return samplingCode != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndividualObservationBatchRowState that = (IndividualObservationBatchRowState) o;
        return Objects.equals(size, that.size) &&
                Objects.equals(weight, that.weight) &&
                Objects.equals(maturity, that.maturity) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(samplingCode, that.samplingCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, weight);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("size", size)
                          .add("weight", weight)
                          .add("maturity", maturity)
                          .add("gender", gender)
                          .add("samplingCode", samplingCode)
                          .add("valid", valid)
                          .toString();
    }
}
