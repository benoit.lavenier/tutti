package fr.ifremer.tutti.ui.swing.content.operation.fishing.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUI;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Cancels the edition of a fishing operation and potentially switch to another tab.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class CancelEditFishingOperationAction extends LongActionSupport<EditFishingOperationUIModel, EditFishingOperationUI, EditFishingOperationUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SaveFishingOperationAction.class);

    /**
     * Delegate edit action.
     *
     * @since 1.0
     */
    protected EditFishingOperationAction editAction;

    public CancelEditFishingOperationAction(EditFishingOperationUIHandler handler) {
        super(handler, true);
    }

    public EditFishingOperationAction getEditAction() {
        if (editAction == null) {
            editAction = getContext().getActionFactory().createLogicAction(
                    getHandler().getParentUi().getHandler(),
                    EditFishingOperationAction.class);
        }
        return editAction;
    }

    @Override
    public void doAction() throws Exception {

        EditFishingOperationAction action = getEditAction();

        if (getModel().isCreate()) {
            if (log.isInfoEnabled()) {
                log.info("Cancel creation for fishingOperation");
            }
            // cancel to create a new fishingOperation
            action.setFishingOperation(null);
            getActionEngine().runInternalAction(action);

        } else {

            if (log.isInfoEnabled()) {
                log.info("Can edition of fishingOperation");
            }

            // re-edit current fishing operation (but do not perform any check)
            action.setCheckPreviousEdit(false);
            action.setFishingOperation(getModel().getFishingOperation());
            action.setInternalAction(true);
            getActionEngine().runInternalAction(action);
        }

        //FIXME-TC Make sure this works again
//        // if called directly from the EditFishingOperationUIHandler:
//        // the user does not want to save the modifications before
//        // selecting another tab, we must reload the current tab before setting
//        // the new index of the tab pane
//        if (event.getSource() != null
//            && event.getSource().getClass().isAssignableFrom(EditFishingOperationUIHandler.class)) {
//
//            int newIndex = event.getID();
//            parentHandler.getTabPanel().setSelectedIndex(newIndex);
//        }
    }

//    @Override
//    public void postSuccessAction() {
//        super.postSuccessAction();
//        getEditAction().displayValidationErrors();
//    }
}
