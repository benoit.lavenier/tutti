package fr.ifremer.tutti.ui.swing.util.table;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.Binder;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @param <E> type of incoming bean to edit
 * @param <R> type of the row of the table model
 * @param <B> type of this model
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public abstract class AbstractTuttiTableUIModel<E, R extends AbstractTuttiBeanUIModel, B extends AbstractTuttiTableUIModel<E, R, B>> extends AbstractTuttiBeanUIModel<E, B> {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(AbstractTuttiTableUIModel.class);

    public static final String PROPERTY_ROWS = "rows";

    public static final String PROPERTY_ROWS_IN_ERROR = "rowsInError";

    protected List<R> rows;

    protected Set<R> rowsInError;

    protected AbstractTuttiTableUIModel(Class<E> entityType,
                                        Binder<E, B> fromBeanBinder,
                                        Binder<B, E> toBeanBinder) {
        super(fromBeanBinder, toBeanBinder);
        addPropertyChangeListener(PROPERTY_ROWS_IN_ERROR, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Set<R> rowsInErorr = (Set<R>) evt.getNewValue();
                if (log.isDebugEnabled()) {
                    log.debug(PROPERTY_ROWS_IN_ERROR + " changed " + rowsInErorr.size());
                }
                setValid(CollectionUtils.isEmpty(rowsInErorr));
            }
        });
        setRowsInError(new HashSet<>());
    }

    public List<R> getRows() {
        return rows;
    }

    public void setRows(List<R> rows) {
        if (rows == null) {
            rows = new ArrayList<>();
        }
        this.rows = rows;

        // always propagates (since empty list will not fire and we want it)
        firePropertyChange(PROPERTY_ROWS, null, rows);

        rowsInError.clear();
        for (R row : rows) {
            if (!row.isValid()) {
                rowsInError.add(row);
            }
        }
        setRowsInError(rowsInError);
    }

    public int getRowCount() {
        return rows == null ? 0 : rows.size();
    }

    public Set<R> getRowsInError() {
        return rowsInError;
    }

    public void setRowsInError(Set<R> rowsInError) {
        this.rowsInError = rowsInError;
        firePropertyChange(PROPERTY_ROWS_IN_ERROR, null, rowsInError);
    }

    public void addRowInError(R row) {
        rowsInError.add(row);
        firePropertyChange(PROPERTY_ROWS_IN_ERROR, null, rowsInError);
    }

    public void removeRowInError(R row) {
        rowsInError.remove(row);
        firePropertyChange(PROPERTY_ROWS_IN_ERROR, null, rowsInError);
    }

    public void removeRowsInError(Collection<R> rows) {
        rowsInError.removeAll(rows);
        firePropertyChange(PROPERTY_ROWS_IN_ERROR, null, rowsInError);
    }
}
