package fr.ifremer.tutti.ui.swing.update.module;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.update.ApplicationUpdateException;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;
import fr.ifremer.tutti.ui.swing.util.auth.AuthenticationInfo;
import org.nuiton.updater.ApplicationInfo;

import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public abstract class ModuleUpdaterSupport {

    protected final UpdateModule updateModule;

    public ModuleUpdaterSupport(UpdateModule updateModule) {
        this.updateModule = updateModule;
    }

    public UpdateModule getUpdateModule() {
        return updateModule;
    }

    public boolean matchUpdate(ApplicationInfo info) {
        return updateModule.name().toLowerCase().equals(info.name);
    }

    public ApplicationInfo updateToDo(TuttiUIContext context, Map<String, ApplicationInfo> appToUpdate) {

        ApplicationInfo info = getInfo(appToUpdate);

        if (info != null) {

            if (info.needAuthentication) {
                // ask auth
                AuthenticationInfo authenticationInfo = context.getAuthenticationInfo(info.url);
                if (authenticationInfo != null) {
                    info.setAuthentication(authenticationInfo.getLogin(), authenticationInfo.getPassword());
                }
            }
        }

        onUpdateToDo(context, info);

        return info;

    }

    public boolean updateDone(TuttiUIContext context,
                              Map<String, ApplicationInfo> appToUpdate,
                              Map<String, Exception> appUpdateError) throws ApplicationUpdateException {

        ApplicationInfo info = getInfo(appToUpdate);

        Exception error = getError(appUpdateError);

        if (error != null) {

            String errorMessage;
            if (info != null && info.needAuthentication) {
                errorMessage = t("tutti.update.error.with.auth", getLabel());
            } else {
                errorMessage = t("tutti.update.error.with.noauth", getLabel());
            }

            // something bad while updating application
            throw new ApplicationUpdateException(updateModule, errorMessage, error);

        }

        boolean doRestart = false;
        if (info != null) {

            doRestart = true;

            onUpdateDone(context, info);

        }
        return doRestart;
    }

    protected abstract void onUpdateToDo(TuttiUIContext context, ApplicationInfo info) ;

    protected abstract void onUpdateDone(TuttiUIContext context, ApplicationInfo info);

    public abstract String getLabel();

    protected ApplicationInfo getInfo(Map<String, ApplicationInfo> appToUpdate) {
        return appToUpdate.get(updateModule.name().toLowerCase());
    }

    protected Exception getError(Map<String, Exception> appUpdateError) {
        return appUpdateError.get(updateModule.name().toLowerCase());
    }
}
