package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class SaveAndCloseSpeciesFrequencyAction extends SaveSupportAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SaveAndCloseSpeciesFrequencyAction.class);

    private static final long serialVersionUID = 1L;

    public SaveAndCloseSpeciesFrequencyAction(SpeciesFrequencyUI ui) {
        super(ui);

        putValue(NAME, t("tutti.editSpeciesFrequencies.action.saveAndClose"));
        putValue(SHORT_DESCRIPTION, t("tutti.editSpeciesFrequencies.action.saveAndClose.tip"));
        putValue(MNEMONIC_KEY, (int) SwingUtil.getFirstCharAt(t("tutti.editSpeciesFrequencies.action.saveAndClose.mnemonic"), 'Z'));
        putValue(SMALL_ICON, SwingUtil.createActionIcon("save"));

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (log.isDebugEnabled()) {
            log.debug("Save And Close UI " + ui);
        }

        boolean doSave = canSaveFrequencies();

        if (doSave) {

            SpeciesFrequencyUIHandler handler = ui.getHandler();
            handler.getFrequencyEditor().save(ui.getModel(), true);
            handler.onCloseUI();

        }

    }

}