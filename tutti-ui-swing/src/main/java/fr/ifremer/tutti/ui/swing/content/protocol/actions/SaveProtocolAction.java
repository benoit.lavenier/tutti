package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * Saves a protocol
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SaveProtocolAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SaveProtocolAction.class);

    public SaveProtocolAction(EditProtocolUIHandler handler) {
        super(handler, true);
    }

    /**
     * If the event source is a TuttiScreen, then the screen changes to the source.
     * Otherwise, the screen changes to the home.
     */
    @Override
    public void doAction() throws Exception {
        EditProtocolUIModel model = getModel();
        TuttiUIContext context = getContext();

        PersistenceService persistenceService = context.getPersistenceService();

        TuttiProtocol bean = model.toEntity();

        if (log.isInfoEnabled()) {
            log.info("bean zones " + bean.getZone());
        }

        if (log.isDebugEnabled()) {
            log.debug("protocol id to save: " + bean.getId());
        }

        TuttiProtocol saved;

        if (TuttiEntities.isNew(bean)) {

            saved = persistenceService.createProtocol(bean);
            model.setId(saved.getId());
            sendMessage(t("tutti.flash.info.protocolCreated", bean.getName()));
        } else {
            saved = persistenceService.saveProtocol(bean);
            sendMessage(t("tutti.flash.info.protocolSaved", bean.getName()));
        }

        context.setProtocolId(saved.getId());

        getDataContext().closeCruiseCache();

        model.setModify(false);
    }

    @Override
    public void postSuccessAction() {
        getContext().getMainUI().getHandler().setBodyTitle(EditProtocolUIHandler.getTitle(true));
        getUI().getSaveWarningContainer().setVisible(false);

    }
}
