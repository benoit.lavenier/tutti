package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class DeleteSampleCodeAction extends SimpleActionSupport<SpeciesFrequencyUI> {

    public DeleteSampleCodeAction(SpeciesFrequencyUI speciesFrequencyUI) {
        super(speciesFrequencyUI, false);
    }

    @Override
    protected void onActionPerformed(SpeciesFrequencyUI ui) {
        JXTable obsTable = ui.getObsTable();
        IndividualObservationBatchTableModel obsTableModel =
                (IndividualObservationBatchTableModel) obsTable.getModel();

        int selectedRowIndex = obsTable.getSelectedRow();
        IndividualObservationBatchRowModel selectedRow = obsTableModel.getRows().get(selectedRowIndex);

        int answer = JOptionPane.showConfirmDialog(ui,
                                                   t("tutti.editSpeciesFrequencies.action.deleteSampleCode.confirm.message", selectedRow.getSamplingCode()),
                                                   t("tutti.editSpeciesFrequencies.action.deleteSampleCode.confirm.title"),
                                                   JOptionPane.YES_NO_OPTION);

        if (answer == JOptionPane.YES_OPTION) {
            selectedRow.setSamplingCode(null);
            obsTableModel.fireTableRowsUpdated(selectedRowIndex, selectedRowIndex);
        }
    }
}