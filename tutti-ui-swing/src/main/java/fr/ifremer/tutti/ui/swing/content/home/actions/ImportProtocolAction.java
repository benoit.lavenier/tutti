package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractChangeScreenAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.context.JAXXContextEntryDef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Opens a file chooser and imports the protocol from the selected file.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ImportProtocolAction extends AbstractChangeScreenAction {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ImportProtocolAction.class);

    public static final JAXXContextEntryDef<TuttiProtocol> IMPORT_PROTOCOL_ENTRY =
            JAXXUtil.newContextEntryDef("importProtocol", TuttiProtocol.class);

    protected TuttiProtocol protocol;

    public ImportProtocolAction(MainUIHandler handler) {
        super(handler, true, TuttiScreen.EDIT_PROTOCOL);
    }

    @Override
    public boolean prepareAction() throws Exception {

        IMPORT_PROTOCOL_ENTRY.removeContextValue(getContext().getMainUI());

        boolean doAction = super.prepareAction();

        File file = null;

        if (doAction) {
            // choose file to import
            file = chooseFile(
                    t("tutti.selectCruise.title.choose.importProtocolFile"),
                    t("tutti.selectCruise.action.importProtocol"),
                    "^.+\\.tuttiProtocol$", t("tutti.common.file.protocol")
            );

            doAction = file != null;
        }

        if (doAction) {

            // import protocol
            if (log.isInfoEnabled()) {
                log.info("Will import protocol file: " + file);
            }

            ProtocolImportExportService service =
                    getContext().getTuttiProtocolImportExportService();

            protocol = service.importProtocol(file);

            // remove id
            protocol.setId((String) null);

            sendMessage(t("tutti.importProtocol.action.success", protocol.getName()));
        }

        if (doAction) {

            // check that protocol is compatible with sample category model
            doAction = cleanCategories(protocol);

        }

        if (doAction) {

            // replace obsolete referent taxons
            TuttiProtocols.translateReferenceTaxonIds(protocol, getContext().getPersistenceService().getAllObsoleteReferentTaxons());

            List<Species> allReferentSpecies = getContext().getPersistenceService().getAllReferentSpecies();

            // clean species
            doAction = cleanSpecies(allReferentSpecies, protocol);

            if (doAction) {

                // clean benthos
                doAction = cleanBenthos(allReferentSpecies, protocol);
            }
        }

        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(protocol);

        // store protocol in context
        IMPORT_PROTOCOL_ENTRY.setContextValue(getContext().getMainUI(), protocol);
        protocol = null;
        createProgressionModelIfRequired(4);

        // removed selected protocol
        getContext().setProtocolId(null);
        super.doAction();
    }

    protected boolean cleanCategories(TuttiProtocol protocol) {

        boolean doAction = true;
        SampleCategoryModel sampleCategoryModel =
                getDataContext().getSampleCategoryModel();

        Set<Integer> badCategories = Sets.newHashSet();

        TuttiProtocols.checkSampleCategories(sampleCategoryModel,
                                             protocol,
                                             badCategories);

        if (!badCategories.isEmpty()) {

            if (log.isWarnEnabled()) {
                log.warn("There is some bad categories: " + badCategories);
            }

            String message = TuttiProtocols.getBadCategoriesMessage(
                    badCategories,
                    getDecorator(Caracteristic.class, null),
                    getContext().getPersistenceService());

            String htmlMessage = String.format(
                    AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                    message,
                    t("tutti.common.askBeforeImportProtocol.help"));
            int response = JOptionPane.showOptionDialog(
                    getContext().getActionUI(),
                    htmlMessage,
                    t("tutti.common.askBeforeEditProtocol.title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    UIManager.getIcon("warning"),
                    new Object[]{t("tutti.option.cleanAndImport"), t("tutti.option.import"), t("tutti.option.cancel")},
                    t("tutti.option.cancel")
            );

            switch (response) {
                case 0:
                    if (log.isInfoEnabled()) {
                        log.info("Clean and Import");
                    }
                    TuttiProtocols.removeBadCategories(sampleCategoryModel, protocol);
                    break;
                case 1:
                    if (log.isInfoEnabled()) {
                        log.info("Import with no cleaning");
                    }
                    break;
                default:

                    // cancel
                    doAction = false;
            }
        }
        return doAction;
    }


    protected boolean cleanSpecies(List<Species> allReferentSpecies, TuttiProtocol protocol) {

        boolean doAction = true;

        Map<Integer, String> missingSpecies = TuttiProtocols.detectMissingSpecies(protocol, allReferentSpecies);
        boolean withBadSpecies = !missingSpecies.isEmpty();

        if (withBadSpecies) {

            if (log.isWarnEnabled()) {
                log.warn("There is some bad species to remove.");
            }

            String message = TuttiProtocols.getBadSpeciesMessage(missingSpecies);

            String htmlMessage = String.format(
                    AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                    message,
                    t("tutti.common.askBeforeImportProtocolSpecies.help"));

            int response = JOptionPane.showOptionDialog(
                    getContext().getActionUI(),
                    htmlMessage,
                    t("tutti.common.askBeforeEditProtocolSpecies.title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    UIManager.getIcon("warning"),
                    new Object[]{t("tutti.option.cleanAndImport"), t("tutti.option.cancel")},
                    t("tutti.option.cancel"));

            switch (response) {
                case 0:

                    if (log.isInfoEnabled()) {
                        log.info("Clean species and Import");
                    }
                    TuttiProtocols.removeBadSpecies(protocol, missingSpecies);

                    break;
                default:

                    // cancel
                    doAction = false;
            }
        }
        return doAction;
    }

    protected boolean cleanBenthos(List<Species> allReferentSpecies, TuttiProtocol protocol) {

        boolean doAction = true;

        Map<Integer, String> missingBenthos = TuttiProtocols.detectMissingBenthos(protocol, allReferentSpecies);
        boolean withBadBenthos = !missingBenthos.isEmpty();
        if (withBadBenthos) {

            if (log.isWarnEnabled()) {
                log.warn("There is some bad benthos to remove.");
            }

            String message = TuttiProtocols.getBadBenthosMessage(missingBenthos);

            String htmlMessage = String.format(
                    AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                    message,
                    t("tutti.common.askBeforeImportProtocolBenthos.help"));

            int response = JOptionPane.showOptionDialog(
                    getContext().getActionUI(),
                    htmlMessage,
                    t("tutti.common.askBeforeEditProtocolBenthos.title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    UIManager.getIcon("warning"),
                    new Object[]{t("tutti.option.cleanAndImport"), t("tutti.option.cancel")},
                    t("tutti.option.cancel"));

            switch (response) {
                case 0:

                    if (log.isInfoEnabled()) {
                        log.info("Clean benthos and Import");
                    }
                    TuttiProtocols.removeBadBenthos(protocol, missingBenthos);

                    break;
                default:

                    // cancel
                    doAction = false;
            }
        }
        return doAction;
    }
}
