package fr.ifremer.tutti.ui.swing.util.caracteristics;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.editor.bean.BeanUIUtil;
import jaxx.runtime.swing.editor.cell.NumberCellEditor;
import jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
import org.nuiton.decorator.Decorator;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.util.List;

/**
 * Editor for the values of the caracteristics of the fishing operations.
 * The editor depends on the caracteristic value type.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 0.3
 */
public class CaracteristicValueEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;

    protected int caracteristicColumn;

    protected TableCellEditor editor;

    protected Decorator<CaracteristicQualitativeValue> decorator;

    public CaracteristicValueEditor(TuttiUIContext context) {
        this(0, context);
    }

    public CaracteristicValueEditor(int caracteristicColumn, TuttiUIContext context) {
        super();
        this.caracteristicColumn = caracteristicColumn;
        DecoratorService decoratorService = context.getDecoratorService();
        decorator = decoratorService.getDecoratorByType(CaracteristicQualitativeValue.class);
    }

    @Override
    public Object getCellEditorValue() {
        return editor.getCellEditorValue();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {

        Caracteristic caracteristic = (Caracteristic)
                table.getModel().getValueAt(row, caracteristicColumn);
        if (caracteristic == null) {

            // can't edit a null value ?

        } else {
            switch (caracteristic.getCaracteristicType()) {

                case NUMBER:
                    // by default this is a number
                    NumberCellEditor<Float> editor =
                            JAXXWidgetUtil.newNumberTableCellEditor(Float.class, false);
                    editor.getNumberEditor().setSelectAllTextOnError(true);
                    editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
                    this.editor = editor;
                    break;
                case QUALITATIVE:
                    JComboBox comboBox = new JComboBox();
                    comboBox.setRenderer(new DecoratorListCellRenderer(decorator));

                    // always use a copy of data
                    List<CaracteristicQualitativeValue> data =
                            Lists.newArrayList(caracteristic.getQualitativeValue());
                    // add a null value at first position
                    if (!data.isEmpty() && data.get(0) != null) {
                        data.add(0, null);
                    }
                    SwingUtil.fillComboBox(comboBox, data, null);

                    ObjectToStringConverter converter =
                            BeanUIUtil.newDecoratedObjectToStringConverter(decorator);
                    BeanUIUtil.decorate(comboBox, converter);
                    this.editor = new ComboBoxCellEditor(comboBox);
                    break;
                case TEXT:
                    // use default editor

                    this.editor = table.getDefaultEditor(Object.class);
                    break;
            }
        }

        return editor.getTableCellEditorComponent(
                table, value, isSelected, row, column);
    }

}
