package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import fr.ifremer.tutti.util.BeepFrequency;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.3
 */
public class SoundUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SoundUtil.class);

    public static String SOUNDS_DIRECTORY = "/sounds";

    public static float SAMPLE_RATE = 8000f;

    public static void beep(BeepFrequency beepFrequency) {
        beep(beepFrequency, 1);
    }

    public static void beep(BeepFrequency beepFrequency, int number) {

        try {
            for (int i = 0; i < number; i++) {
                tone(beepFrequency.getFrequency(), 500, 1.0);
            }

        } catch (LineUnavailableException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while playing a beep", e);
            }
        }
    }

    public static void tone(int hz, int msecs, double vol) throws LineUnavailableException {
        byte[] buf = new byte[1];
        AudioFormat af = new AudioFormat(SAMPLE_RATE, // sampleRate
                8,           // sampleSizeInBits
                1,           // channels
                true,        // signed
                false);      // bigEndian
        try (SourceDataLine sdl = AudioSystem.getSourceDataLine(af)) {
            sdl.open(af);
            sdl.start();
            for (int i = 0, end = msecs * 8; i < end; i++) {
                double angle = i / (SAMPLE_RATE / hz) * 2.0 * Math.PI;
                buf[0] = (byte) (Math.sin(angle) * 127.0 * vol);
                sdl.write(buf, 0, 1);
            }
            sdl.drain();
            sdl.stop();
        }
    }

    public static void readNumber(double number, Optional<String> unit) {

        try {

            int thousands = (int) (number / 1000);
            int hundreds = (int) (number % 1000) / 100;
            int tensAndUnits = (int) number % 100;
            int decimal = (int) (number * 10) % 10;

            if (log.isDebugEnabled()) {
                log.debug(thousands + " " + hundreds + " " + tensAndUnits + " " + decimal);
            }

            List<AudioInputStream> audioInputStreams = new ArrayList<>();

            addSound(audioInputStreams, thousands, 1000);
            addSound(audioInputStreams, hundreds, 100);
            if (tensAndUnits != 0 || thousands == 0 && hundreds == 0) {
                addSound(audioInputStreams, tensAndUnits);
            }
            if (decimal > 0) {
                addSound(audioInputStreams, ",");
                addSound(audioInputStreams, decimal);
            }
            if (unit.isPresent()) {
                addSound(audioInputStreams, unit.get());
            }

            if (!audioInputStreams.isEmpty()) {

                AudioFormat format = audioInputStreams.get(0).getFormat();
                DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
                try (SourceDataLine audioLine = (SourceDataLine) AudioSystem.getLine(info)) {
                    audioLine.open(format);
                    audioLine.start();

                    byte[] bytesBuffer = new byte[4096];

                    while (!audioInputStreams.isEmpty()) {
                        try (AudioInputStream audioInputStream = audioInputStreams.remove(0)) {
                            int bytesRead;
                            while ((bytesRead = audioInputStream.read(bytesBuffer)) != -1) {
                                audioLine.write(bytesBuffer, 0, bytesRead);
                            }
                        }
                    }

                    audioLine.drain();

                }
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error while reading " + number + " " + unit, e);
            }
        }

    }

    protected static long addSound(List<AudioInputStream> inputStreams, int number) throws IOException, UnsupportedAudioFileException {
        return addSound(inputStreams, number, 1);
    }

    protected static long addSound(List<AudioInputStream> inputStreams, int number, int suffix) throws IOException, UnsupportedAudioFileException {
        long length = 0;

        if (number > 1 || suffix == 1) {
            if (log.isDebugEnabled()) {
                log.debug("-> add " + number + ".wav");
            }
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(SoundUtil.class.getResource(SOUNDS_DIRECTORY + "/" + number + ".wav"));
            inputStreams.add(audioInputStream);
            length += audioInputStream.getFrameLength();
        }

        if (number >= 1 && suffix > 1) {
            if (log.isDebugEnabled()) {
                log.debug("-> add " + suffix + ".wav");
            }
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(SoundUtil.class.getResource(SOUNDS_DIRECTORY + "/" + suffix + ".wav"));
            inputStreams.add(audioInputStream);
            length += audioInputStream.getFrameLength();
        }

        return length;
    }

    protected static long addSound(List<AudioInputStream> inputStreams, String name) throws IOException, UnsupportedAudioFileException {
        if (log.isDebugEnabled()) {
            log.debug("add " + name + ".wav");
        }
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(SoundUtil.class.getResource(SOUNDS_DIRECTORY + "/" + name + ".wav"));
        inputStreams.add(audioInputStream);
        return audioInputStream.getFrameLength();
    }

}
