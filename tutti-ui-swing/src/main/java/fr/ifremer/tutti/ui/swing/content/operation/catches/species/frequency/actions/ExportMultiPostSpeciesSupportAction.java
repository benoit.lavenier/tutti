package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.catches.multipost.MultiPostExportService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.actions.ExportMultiPostActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.JOptionPane;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public abstract class ExportMultiPostSpeciesSupportAction extends ExportMultiPostActionSupport<SpeciesFrequencyUIModel, SpeciesFrequencyUI, SpeciesFrequencyUIHandler> {

    public ExportMultiPostSpeciesSupportAction(SpeciesFrequencyUIHandler handler) {
        super(handler);
    }

    public abstract boolean isImportFrequencies();

    public abstract boolean isImportIndivudalObservations();

    @Override
    protected void doExport(MultiPostExportService multiPostImportExportService, File file, FishingOperation fishingOperation) {

        SpeciesBatch speciesBatch = getModel().getBatch().toEntity();

        SpeciesOrBenthosBatchUISupport batchUISupport = getModel().getSpeciesOrBenthosBatchUISupport();

        batchUISupport.exportMultiPost(file, fishingOperation, speciesBatch, isImportFrequencies(), isImportIndivudalObservations());

    }

    @Override
    protected Integer askToSaveBeforeExport() {

        SpeciesFrequencyUIModel model = getModel();

        Integer doSaveBeforeExportResponse = null;

        boolean canSave = model.isModify() && model.isValid();
        if (canSave) {

            String htmlMessage = String.format(
                    AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.askToSaveFrequenciesOrIndividualObservations.message"),
                    t("tutti.askToSaveFrequenciesOrIndividualObservations.help"));

            doSaveBeforeExportResponse = JOptionPane.showOptionDialog(getHandler().getTopestUI(),
                                                                      htmlMessage,
                                                                      t("tutti.askToSaveFrequenciesOrIndividualObservations.title"),
                                                                      JOptionPane.OK_CANCEL_OPTION,
                                                                      JOptionPane.QUESTION_MESSAGE,
                                                                      null,
                                                                      new String[]{t("tutti.option.saveCatch"), t("tutti.option.notSaveCatch"), t("tutti.option.cancelExport")},
                                                                      t("tutti.option.saveCatch"));

        }

        return doSaveBeforeExportResponse;

    }

    @Override
    protected void saveBeforeExport() {

        getHandler().getFrequencyEditor().save(getModel());

    }
}
