package fr.ifremer.tutti.ui.swing.content.protocol.maturity;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class EditMaturityCaracteristicPopupUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_ALL_MATURITY_STATES = "allMaturityStates";

    public static final String PROPERTY_MATURE_STATE_IDS = "matureStateIds";

    /**
     * Is the model valid?
     */
    protected boolean valid;

    protected final List<CaracteristicQualitativeValue> allMaturityStates = new ArrayList<>();

    protected final Collection<String> matureStateIds = new HashSet<>();

    public List<CaracteristicQualitativeValue> getAllMaturityStates() {
        return allMaturityStates;
    }

    public void setAllMaturityStates(List<CaracteristicQualitativeValue> allMaturityStates) {
        this.allMaturityStates.clear();
        if (allMaturityStates != null) {
            this.allMaturityStates.addAll(allMaturityStates);
        }
        firePropertyChange(PROPERTY_ALL_MATURITY_STATES, null, this.allMaturityStates);
    }

    public Collection<String> getMatureStateIds() {
        return matureStateIds;
    }

    public void setMatureStateIds(Collection<String> matureStateIds) {
        this.matureStateIds.clear();
        if (matureStateIds != null) {
            this.matureStateIds.addAll(matureStateIds);
        }
        firePropertyChange(PROPERTY_MATURE_STATE_IDS, null, this.matureStateIds);
    }

    public void addMatureState(CaracteristicQualitativeValue state) {
        Objects.requireNonNull(state);
        addMatureState(state.getId());
    }

    public void addMatureState(String stateId) {
        matureStateIds.add(stateId);
        firePropertyChange(PROPERTY_MATURE_STATE_IDS, null, matureStateIds);
    }

    public void removeMatureState(CaracteristicQualitativeValue state) {
        Objects.requireNonNull(state);
        removeMatureState(state.getId());
    }

    public void removeMatureState(String stateId) {
        matureStateIds.remove(stateId);
        firePropertyChange(PROPERTY_MATURE_STATE_IDS, null, matureStateIds);
    }

    public boolean isMature(CaracteristicQualitativeValue state) {
        Objects.requireNonNull(state);
        return isMature(state.getId());
    }

    public boolean isMature(String id) {
        return matureStateIds.contains(id);
    }

    public boolean hasMatureValues() {
        return matureStateIds.size() > 0;
    }

    public boolean hasImmatureValues() {
        return matureStateIds.size() < allMaturityStates.size();
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}