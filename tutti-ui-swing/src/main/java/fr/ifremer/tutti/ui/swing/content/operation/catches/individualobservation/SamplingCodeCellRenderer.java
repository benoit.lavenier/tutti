package fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class SamplingCodeCellRenderer extends DefaultTableCellRenderer {

    public static final String TEXT_PATTERN = "<html><body>%s</body></html>";

    private final String noneText;

    public static SamplingCodeCellRenderer newRender() {
        return new SamplingCodeCellRenderer();
    }

    protected SamplingCodeCellRenderer() {
        setHorizontalAlignment(LEFT);
        this.noneText = n("tutti.editIndividualObservationBatch.table.renderer.sampleCodeEdition.none");
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        String sampleCode = (String) value;
        setToolTipText(getText());
        String toolTipTextValue;

        if (StringUtils.isEmpty(sampleCode)) {
            // use HTML to show the tooltip in italic
            toolTipTextValue = "<i>" + t(noneText) + "</i>";

        } else {
            toolTipTextValue = sampleCode;

        }
        setToolTipText(String.format(TEXT_PATTERN, toolTipTextValue));

        return this;
    }
}
