package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import jaxx.runtime.swing.editor.bean.BeanDoubleList;
import org.apache.commons.lang3.mutable.MutableInt;

import javax.swing.JList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Un compteur de caractéristiques.
 *
 * Created on 27/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class CaracteristicsCount {

    public void attachPredicateToUi(BeanDoubleList<Caracteristic> ui) {
        Predicate<List<Caracteristic>> listPredicate = input -> {
            JList<Caracteristic> selectedList = ui.getSelectedList();
            List<Caracteristic> selectedValuesList = selectedList.getSelectedValuesList();
            for (Caracteristic caracteristic : selectedValuesList) {
                if (isCaracteristicUsed(caracteristic)) {
                    return false;
                }
            }
            return true;
        };
        ui.getModel().addCanRemoveItemsPredicate(listPredicate);

    }

    private final MutableInt ZERO = new MutableInt();

    private final Map<Integer, MutableInt> counts = new TreeMap<>();

    public boolean isCaracteristicUsed(Caracteristic caracteristic) {
        Objects.requireNonNull(caracteristic);
        MutableInt result = counts.getOrDefault(caracteristic.getIdAsInt(), ZERO);
        return result.intValue() > 0;
    }

    public void addCaracteristic(Caracteristic caracteristic) {
        Objects.requireNonNull(caracteristic);
        counts.compute(caracteristic.getIdAsInt(), (k, v) -> {
            if (v == null) {
                return new MutableInt(1);
            } else {
                v.increment();
                return v;
            }
        });

    }

    public void removeCaracteristic(Caracteristic caracteristic) {
        Objects.requireNonNull(caracteristic);
        counts.compute(caracteristic.getIdAsInt(), (k, v) -> {
            if (v == null || v.intValue() == 1) {
                return null;
            } else {
                v.decrement();
                return v;
            }
        });

    }

    public void removeCaracteristics(Collection<Caracteristic> caracteristics) {
        Objects.requireNonNull(caracteristics);
        caracteristics.forEach(this::removeCaracteristic);
    }

}
