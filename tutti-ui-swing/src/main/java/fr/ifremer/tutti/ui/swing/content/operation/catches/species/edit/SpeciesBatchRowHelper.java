package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.TuttiDecorator;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Helper methods aroun sorting species rows.
 * Created on 1/8/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class SpeciesBatchRowHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesBatchRowHelper.class);

    public static final String SPECIES_DECORATOR = "decorator";

    public static final String SPECIES_DECORATOR_INDEX = "decoratorIndex";

    public static SpeciesBatchDecoratorComparator<SpeciesBatchRowModel> getSpeciesRowComparator(JXTable table) {
        TableColumnExt speciesColumn = table.getColumnExt(SpeciesBatchTableModel.SPECIES);

        SpeciesBatchDecoratorComparator<SpeciesBatchRowModel> comparator =
                (SpeciesBatchDecoratorComparator<SpeciesBatchRowModel>) speciesColumn.getComparator();

        SpeciesBatchDecorator<SpeciesBatchRowModel> decorator = getSpeciesColumnDecorator(table);

        boolean comparatorNull = comparator == null;
        if (comparatorNull) {

            // first time coming here, add the comparator
            comparator = (SpeciesBatchDecoratorComparator) decorator.getCurrentComparator();
        }

        if (comparatorNull) {

            // affect it to colum
            speciesColumn.setComparator(comparator);
        }
        return comparator;
    }

    protected static SpeciesBatchDecorator<SpeciesBatchRowModel> getSpeciesColumnDecorator(JXTable table) {
        TableColumnExt speciesColumn = table.getColumnExt(SpeciesBatchTableModel.SPECIES);
        return (SpeciesBatchDecorator<SpeciesBatchRowModel>) SpeciesBatchRowHelper.getSpeciesColumnDecorator(speciesColumn);
    }

    public static <E extends SpeciesBatch> int getIndexToInsert(List<E> rows,
                                                                E newRow,
                                                                SpeciesSortMode sortMode,
                                                                SpeciesBatchDecorator decorator) {

        int result;

        int rowsSize = rows.size();
        if (sortMode == SpeciesSortMode.NONE) {
            // after the last row
            result = rowsSize;
        } else {

            // get the universe of species
            Set<Species> speciesSet = Sets.newHashSet();
            for (E row : rows) {
                speciesSet.add(row.getSpecies());
            }
            Species newSpecies = newRow.getSpecies();

            boolean speciestoAdd = speciesSet.add(newSpecies);

            int nbSpecies = speciesSet.size();

            // sort it
            List<Species> speciesList = Lists.newArrayList(speciesSet);
            TuttiDecorator.TuttiDecoratorComparator<Species> comparator = decorator.getOriginalComparator();
            comparator.init(decorator, speciesList);
            Collections.sort(speciesList, comparator);
            if (sortMode == SpeciesSortMode.DESC) {

                // reserve order
                Collections.reverse(speciesList);
            }

            int indexOf = speciesList.indexOf(newSpecies);

            if (!speciestoAdd) {
                // add after all rows of the species
                indexOf++;
            }

            if (indexOf == 0) {

                // limit case: first row
                result = 0;
            } else if (indexOf >= nbSpecies) {

                // limit case: last row
                result = rowsSize;
            } else {

                // must insert before this species
                Species beforeSpecies;
                if (speciestoAdd) {
                    beforeSpecies = speciesList.get(indexOf + 1);
                } else {
                    beforeSpecies = speciesList.get(indexOf);
                }
                result = 0;
                for (E row : rows) {
                    Species species = row.getSpecies();
                    if (beforeSpecies.equals(species)) {
                        break;
                    }
                    result++;
                }
            }
        }
        return result;
    }

    public static TuttiDecorator<Species> getSpeciesColumnDecorator(TableColumnExt tableColumn) {
        return (TuttiDecorator<Species>) tableColumn.getClientProperty(SPECIES_DECORATOR);
    }

    public static <R extends Serializable, T extends AbstractApplicationTableModel<R>> void installSpeciesColumnComparatorPopup(JXTable table,
                                                                                                                                TableColumnExt speciesColumn,
                                                                                                                                SpeciesSortableRowModel optionalModel,
                                                                                                                                String... tips) {

        ButtonGroup buttonGroup = new ButtonGroup();

        SpeciesDecoratorListener<R, T> speciesDecoratorListener =
                new SpeciesDecoratorListener<>(table, buttonGroup, speciesColumn, optionalModel);

        TuttiDecorator<Species> decorator =
                getSpeciesColumnDecorator(speciesColumn);

        JPopupMenu popup = new JPopupMenu();
        popup.add(new JLabel(t("tutti.ui.change.species.decorator")));
        popup.add(new JSeparator());

        for (int i = 0, nbContext = decorator.getNbContext(); i < nbContext; i++) {
            String property = decorator.getProperty(i);

            String i18nName = "tutti.property." + property;
            speciesColumn.putClientProperty(i18nName, tips[i]);
            JRadioButtonMenuItem item = new JRadioButtonMenuItem(tips[i]);
            item.putClientProperty(SPECIES_DECORATOR_INDEX, i);
            item.addActionListener(speciesDecoratorListener);
            if (i == 0) {
                // select the first property (as it is the
                item.setSelected(true);
            }
            buttonGroup.add(item);
            popup.add(item);
        }

        // recompute the header tip using the decorator
        speciesDecoratorListener.recomputeSpeciesColumnTip();

        // listen when to show popup menu on cell header
        table.getTableHeader().addMouseListener(
                new ShowSpeciesDecoratorPopupListener(popup));

    }

    public static <R extends SpeciesBatch> void sortSpeciesRows(JXTable table,
                                                                SpeciesBatchDecorator decorator,
                                                                List<R> rows,
                                                                SpeciesSortMode speciesSortMode) {

        SpeciesBatchDecoratorComparator comparator = getSpeciesRowComparator(table);
        comparator.setSpeciesSortMode(speciesSortMode);
        comparator.init(decorator, rows);

        switch (speciesSortMode) {

            case NONE:
                // nothing special to do
                break;
            case ASC:
            case DESC:
                // sort
                DecoratorUtil.sort(decorator, rows, decorator.getContextIndex());
                break;
        }
    }

    /**
     * Calcule le poids total des mensurations.
     *
     * S'il n'y a pas de meunsration retourne {@code null}, idem si l'une des mensurations n'a pas de poids.
     *
     * @return la somme des poids des mensurations
     * @since 3.10
     */
    public static <F extends SpeciesBatchFrequency> Float getFrequenciesTotalWeight(Collection<F> frequency) {

        if (CollectionUtils.isEmpty(frequency)) {

            // pas de mensuration, donc poids total {@code null}
            return null;

        }

        float frequencyTotalWeight = 0;
        for (SpeciesBatchFrequency aFrequency : frequency) {

            if (aFrequency.getWeight() == null) {

                // on a trouve une mensuration sans poids, on ne peut donc pas calcule
                // de poids total
                // on retourne {@code null}
                return null;
            }

            frequencyTotalWeight += aFrequency.getWeight();

        }

        return frequencyTotalWeight;

    }

    protected static class SpeciesDecoratorListener<R extends Serializable, T extends AbstractApplicationTableModel<R>> implements ActionListener {

        protected final JXTable table;

        protected final ButtonGroup buttonGroup;

        protected final SpeciesSortableRowModel optionalModel;

        protected final TuttiDecorator<Species> decorator;

        protected final TableColumnExt column;

        public SpeciesDecoratorListener(JXTable table,
                                        ButtonGroup buttonGroup,
                                        TableColumnExt speciesColumn,
                                        SpeciesSortableRowModel optionalModel) {
            this.table = table;
            this.buttonGroup = buttonGroup;
            this.optionalModel = optionalModel;
            this.column = speciesColumn;
            this.decorator = getSpeciesColumnDecorator(column);
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            JRadioButtonMenuItem source = (JRadioButtonMenuItem) e.getSource();
            buttonGroup.setSelected(source.getModel(), true);

            Integer index =
                    (Integer) source.getClientProperty(SPECIES_DECORATOR_INDEX);

            if (log.isInfoEnabled()) {
                log.info("Selected decorator context index: " + index);
            }

            // do the decoration from here
            decorator.setContextIndex(index);

            column.setComparator(decorator.getCurrentComparator());

            // recompute the header tip
            recomputeSpeciesColumnTip();

            if (optionalModel != null) {

                // set new index in model
                optionalModel.setSpeciesDecoratorContextIndex(index);
            } else {

                // reload table data
                T tableModel = (T) table.getModel();

                // keep selected rows
                List<R> rowsToReSelect = Lists.newArrayList();
                for (int rowIndex : SwingUtil.getSelectedModelRows(table)) {
                    R row = tableModel.getEntry(rowIndex);
                    rowsToReSelect.add(row);
                }

                // fire model (will reload the comparator)
                tableModel.fireTableDataChanged();

                // reselect rows
                for (R row : rowsToReSelect) {
                    int modelRowIndex = tableModel.getRowIndex(row);
                    SwingUtil.addRowSelectionInterval(table, modelRowIndex);
                }
            }
        }

        public void recomputeSpeciesColumnTip() {
            List<String> tips = Lists.newArrayList();
            for (int i = 0, nbContext = decorator.getNbContext(); i < nbContext; i++) {
                String property = decorator.getProperty(i);

                String i18nName = "tutti.property." + property;
                String tip = (String) column.getClientProperty(i18nName);
                tips.add(tip);
            }
            String tip = Joiner.on(" - ").join(tips);
            column.setToolTipText(tip);
        }
    }

    protected static class ShowSpeciesDecoratorPopupListener extends MouseAdapter {

        private final JPopupMenu popup;

        public ShowSpeciesDecoratorPopupListener(JPopupMenu popup) {
            this.popup = popup;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            JTableHeader source = (JTableHeader) e.getSource();
            Point point = e.getPoint();
            int columnIndex = source.columnAtPoint(point);

            boolean rightClick = SwingUtilities.isRightMouseButton(e);
            if (columnIndex == 0 && rightClick) {
                e.consume();
                popup.show(source, e.getX(), e.getY());
            }
        }
    }
}
