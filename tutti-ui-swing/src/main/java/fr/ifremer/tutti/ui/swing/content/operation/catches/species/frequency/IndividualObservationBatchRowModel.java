package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatchs;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.sampling.SamplingCodePrefix;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapColumnRowModel;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class IndividualObservationBatchRowModel extends AbstractTuttiBeanUIModel<IndividualObservationBatch, IndividualObservationBatchRowModel>
        implements AttachmentModelAware, IndividualObservationBatch, CaracteristicMapColumnRowModel {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_DEFAULT_CARACTERISTICS = "defaultCaracteristics";

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final IndividualObservationBatch editObject = IndividualObservationBatchs.newIndividualObservationBatch();

    /**
     * Attachments (should never be null).
     *
     * @since 0.2
     */
    protected final List<Attachment> attachment = Lists.newArrayList();

    /**
     * Map of default caracteristics (used if filled in the protocol.
     *
     * @since 2.5
     */
    protected CaracteristicMap defaultCaracteristics = new CaracteristicMap();

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

//    /**
//     * @since 4.5
//     */
//    protected SamplingCodePrefix samplingCodePrefix;

    protected static final Binder<IndividualObservationBatch, IndividualObservationBatchRowModel> fromBeanBinder =
            BinderFactory.newBinder(IndividualObservationBatch.class,
                                    IndividualObservationBatchRowModel.class);

    protected static final Binder<IndividualObservationBatchRowModel, IndividualObservationBatch> toBeanBinder =
            BinderFactory.newBinder(IndividualObservationBatchRowModel.class,
                                    IndividualObservationBatch.class);

    public IndividualObservationBatchRowModel(WeightUnit weightUnit,
                                              CaracteristicMap defaultCaracteristicMap) {
        super(fromBeanBinder, toBeanBinder);
        this.weightUnit = weightUnit;
        if (getCaracteristics() == null) {
            setCaracteristics(new CaracteristicMap());
        }
        setDefaultCaracteristics(CaracteristicMap.copy(defaultCaracteristicMap));
    }

    public IndividualObservationBatchRowModel(WeightUnit weightUnit,
                                              Collection<Caracteristic> defaultCaracteristicsSet,
                                              IndividualObservationBatch entity) {

        this(weightUnit, CaracteristicMap.fromCollection(defaultCaracteristicsSet));

        fromEntity(entity);
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public CaracteristicMap getDefaultCaracteristics() {
        return defaultCaracteristics;
    }

    public void setDefaultCaracteristics(CaracteristicMap defaultCaracteristics) {
        Object oldValue = CaracteristicMap.copy(getDefaultCaracteristics());
        this.defaultCaracteristics = defaultCaracteristics;
        firePropertyChange(PROPERTY_DEFAULT_CARACTERISTICS, oldValue, defaultCaracteristics);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBeanUIModel                                           --//
    //------------------------------------------------------------------------//

    @Override
    public void fromEntity(IndividualObservationBatch entity) {
        super.fromEntity(entity);

        // convert weight
        setWeight(weightUnit.fromEntity(getWeight()));

        CaracteristicMap caracteristics = getCaracteristics();
        if (caracteristics == null) {
            setCaracteristics(new CaracteristicMap());

        } else {
            // move default caracteristics from caracteristic map
            for (Caracteristic caracteristic : getDefaultCaracteristics().keySet()) {
                if (caracteristics.containsKey(caracteristic)) {
                    Serializable value = caracteristics.remove(caracteristic);
                    defaultCaracteristics.put(caracteristic, value);
                }
            }
        }
    }

    public static List<IndividualObservationBatchRowModel> fromEntity(WeightUnit weightUnit,
                                                                      Collection<Caracteristic> defaultCaracteristicsSet,
                                                                      List<IndividualObservationBatch> entities) {
        List<IndividualObservationBatchRowModel> result = Lists.newArrayList();
        for (IndividualObservationBatch entity : entities) {

            IndividualObservationBatchRowModel row =
                    new IndividualObservationBatchRowModel(weightUnit, defaultCaracteristicsSet, entity);
            result.add(row);
        }
        return result;
    }

    @Override
    public IndividualObservationBatch toEntity() {
        IndividualObservationBatch result = super.toEntity();

        // convert weight
        result.setWeight(weightUnit.toEntity(getWeight()));

        CaracteristicMap caracteristics = new CaracteristicMap();
        result.setCaracteristics(caracteristics);

        // push back not null extra caracteristics
        for (Map.Entry<Caracteristic, Serializable> entry : getCaracteristics().entrySet()) {
            Serializable value = entry.getValue();
            if (value != null) {
                caracteristics.put(entry.getKey(), value);
            }
        }

        // push back not null default caracteristics
        for (Map.Entry<Caracteristic, Serializable> entry : getDefaultCaracteristics().entrySet()) {
            Serializable value = entry.getValue();
            if (value != null) {
                caracteristics.put(entry.getKey(), value);
            }
        }

        return result;
    }

    public static List<IndividualObservationBatch> toEntity(List<IndividualObservationBatchRowModel> rows,
                                                            SpeciesBatch batch) {
        List<IndividualObservationBatch> result = Lists.newArrayList();
        for (IndividualObservationBatchRowModel row : rows) {

            IndividualObservationBatch entity = row.toEntity();
            entity.setBatchId(batch.getIdAsInt());
            entity.setFishingOperation(batch.getFishingOperation());
            entity.setSpecies(batch.getSpecies());
            result.add(entity);
        }
        return result;
    }

    @Override
    protected IndividualObservationBatch newEntity() {
        return IndividualObservationBatchs.newIndividualObservationBatch();
    }

    //------------------------------------------------------------------------//
    //-- IndividualObservationBatch                                         --//
    //------------------------------------------------------------------------//

    @Override
    public Integer getBatchId() {
        return editObject.getBatchId();
    }

    @Override
    public void setBatchId(Integer batchId) {
        editObject.setBatchId(batchId);
    }

    @Override
    public FishingOperation getFishingOperation() {
        return editObject.getFishingOperation();
    }

    @Override
    public void setFishingOperation(FishingOperation fishingOperation) {
        editObject.setFishingOperation(fishingOperation);
    }

    @Override
    public Float getWeight() {
        return editObject.getWeight();
    }

    @Override
    public void setWeight(Float weight) {
        Float oldValue = getWeight();
        editObject.setWeight(weight);
        firePropertyChange(PROPERTY_WEIGHT, oldValue, weight);
    }

    public boolean withWeight() {
        return getWeight() != null;
    }

    @Override
    public Species getSpecies() {
        return editObject.getSpecies();
    }

    @Override
    public void setSpecies(Species species) {
        Object oldValue = getSpecies();
        editObject.setSpecies(species);
        firePropertyChange(PROPERTY_SPECIES, oldValue, species);
    }

    @Override
    public Float getSize() {
        return editObject.getSize();
    }

    @Override
    public void setSize(Float size) {
        Float oldValue = getSize();
        editObject.setSize(size);
        firePropertyChange(PROPERTY_SIZE, oldValue, size);
    }

    public boolean withSize() {
        return getSize() != null;
    }

    @Override
    public Caracteristic getLengthStepCaracteristic() {
        return editObject.getLengthStepCaracteristic();
    }

    @Override
    public void setLengthStepCaracteristic(Caracteristic lengthStepCaracteristic) {
        Object oldValue = getLengthStepCaracteristic();
        editObject.setLengthStepCaracteristic(lengthStepCaracteristic);
        firePropertyChange(PROPERTY_LENGTH_STEP_CARACTERISTIC, oldValue, lengthStepCaracteristic);
    }

    @Override
    public CaracteristicMap getCaracteristics() {
        return editObject.getCaracteristics();
    }

    @Override
    public void setCaracteristics(CaracteristicMap caracteristics) {
        Object oldValue = getCaracteristics();
        editObject.setCaracteristics(caracteristics);
        firePropertyChange(PROPERTY_CARACTERISTICS, oldValue, caracteristics);
    }

    @Override
    public String getComment() {
        return editObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public Integer getRankOrder() {
        return editObject.getRankOrder();
    }

    @Override
    public void setRankOrder(Integer rankOrder) {
        editObject.setRankOrder(rankOrder);
    }

    @Override
    public String getSynchronizationStatus() {
        return editObject.getSynchronizationStatus();
    }

    @Override
    public void setSynchronizationStatus(String synchronizationStatus) {
        String oldValue = getSynchronizationStatus();
        editObject.setSynchronizationStatus(synchronizationStatus);
        firePropertyChange(PROPERTY_SYNCHRONIZATION_STATUS, oldValue, synchronizationStatus);
    }

    @Override
    public CopyIndividualObservationMode getCopyIndividualObservationMode() {
        return editObject.getCopyIndividualObservationMode();
    }

    @Override
    public void setCopyIndividualObservationMode(CopyIndividualObservationMode copyIndividualObservationMode) {
        editObject.setCopyIndividualObservationMode(copyIndividualObservationMode);
    }

    @Override
    public String getSamplingCode() {
        return editObject.getSamplingCode();
    }

    @Override
    public void setSamplingCode(String samplingCode) {
        Object oldValue = getSamplingCode();
        editObject.setSamplingCode(samplingCode);
        firePropertyChange(PROPERTY_SAMPLING_CODE, oldValue, samplingCode);
    }

    public Integer getSamplingCodeId() {
        return SamplingCodePrefix.extractSamplingCodeIdFromSamplingCode(getSamplingCode());
    }

//    public SamplingCodePrefix getSamplingCodePrefix() {
//        return samplingCodePrefix;
//    }
//
//    public void setSamplingCodePrefix(SamplingCodePrefix samplingCodePrefix) {
//        this.samplingCodePrefix = samplingCodePrefix;
//    }

    public boolean withSamplingCode() {
        return StringUtils.isNotBlank(getSamplingCode());
    }

//    public boolean withSamplingCodePrefix() {
//        return samplingCodePrefix != null;
//    }

    //------------------------------------------------------------------------//
    //-- AttachmentModelAware                                               --//
    //------------------------------------------------------------------------//

    @Override
    public ObjectTypeCode getObjectType() {
        return ObjectTypeCode.SAMPLE;
    }

    @Override
    public Integer getObjectId() {
        return getIdAsInt();
    }

    @Override
    public List<Attachment> getAttachment() {
        return attachment;
    }

    @Override
    public void addAllAttachment(Collection<Attachment> attachments) {
        this.attachment.addAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        this.attachment.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAllAttachment(Collection<Attachment> attachments) {
        this.attachment.removeAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        this.attachment.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    public boolean isEmpty() {
        return isEmpty(Collections.emptySet());
    }

    public boolean isEmpty(Collection<Caracteristic> caracteristicsToIgnore) {
        return getSize() == null
                && getWeight() == null
                && (getCaracteristics() == null || !getCaracteristics().hasNonNullValues(caracteristicsToIgnore))
                && (getDefaultCaracteristics() == null || !getDefaultCaracteristics().hasNonNullValues(caracteristicsToIgnore))
                && StringUtils.isBlank(getSamplingCode())
                && getComment() == null
                && (getAttachment() == null || getAttachment().isEmpty());
    }

    public void copy(IndividualObservationBatchRowModel source) {
        setId(source.getId());
        setLengthStepCaracteristic(source.getLengthStepCaracteristic());
        setSize(source.getSize());
        setWeight(source.getWeight());
        setBatchId(source.getBatchId());
        setCaracteristics(source.getCaracteristics());
        setDefaultCaracteristics(source.getDefaultCaracteristics());
        setComment(source.getComment());
        setSamplingCode(source.getSamplingCode());
        setCopyIndividualObservationMode(source.getCopyIndividualObservationMode());
        addAllAttachment(source.getAttachment());
    }

    public CaracteristicQualitativeValue getCaracteristicQualitativeValue(Caracteristic caracteristic) {
        CaracteristicQualitativeValue result = null;
        if (caracteristic != null) {
            result = getCaracteristics().getQualitativeValue(caracteristic);
            if (result == null) {
                result = defaultCaracteristics.getQualitativeValue(caracteristic);
            }
        }
        return result;
    }

    public boolean computeValid() {
        return computeValid(withSize(), withWeight());
    }

    public boolean computeValid(boolean withSize, boolean withWeight) {
        CopyIndividualObservationMode copyIndividualObservationMode = getCopyIndividualObservationMode();
        return copyIndividualObservationMode == CopyIndividualObservationMode.NOTHING
                || (copyIndividualObservationMode == CopyIndividualObservationMode.SIZE && withSize)
                || (copyIndividualObservationMode == CopyIndividualObservationMode.ALL && withSize && withWeight);
    }

}
