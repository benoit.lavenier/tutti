package fr.ifremer.tutti.ui.swing.util.attachment.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentEditorUI;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentItem;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentItemModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class RemoveAttachmentAction extends SimpleActionSupport<AttachmentItem> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RemoveAttachmentAction.class);

    private static final long serialVersionUID = 1L;

    public RemoveAttachmentAction(AttachmentItem ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(AttachmentItem ui) {

        AttachmentEditorUI upperUI = ui.getParentContainer(AttachmentEditorUI.class);

        AttachmentItemModel model = ui.getModel();
        boolean hackDialog = upperUI.isAlwaysOnTop();
        if (hackDialog) {
            upperUI.setAlwaysOnTop(false);
        }
        int answer = JOptionPane.showConfirmDialog(upperUI,
                                                   t("tutti.attachmentEditor.deleteAttachment.message", model.getName()),
                                                   t("tutti.attachmentEditor.deleteAttachment.title"),
                                                   JOptionPane.YES_NO_OPTION);
        if (hackDialog) {
            upperUI.setAlwaysOnTop(true);
        }

        if (answer == JOptionPane.YES_OPTION) {
            AttachmentModelAware bean = upperUI.getBean();

            if (log.isInfoEnabled()) {
                log.info("Remove attachment: " + model.getName());
            }

            // si la pj n'est pas persisté, pas besoin de la supprimer, juste l'enlever des pj à enregistrer
            if (!model.isCreate()) {
                ui.getHandler().getPersistenceService().deleteAttachment(model.getId());
            }
            bean.removeAttachment(model.toEntity());
            upperUI.getAttachments().remove(ui);

            upperUI.pack();
        }

    }

}