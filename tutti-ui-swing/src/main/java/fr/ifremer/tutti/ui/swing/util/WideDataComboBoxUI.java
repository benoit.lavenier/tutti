package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.ListSelectionModel;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.synth.SynthComboBoxUI;
import java.awt.Dimension;
import java.awt.Rectangle;

/**
 * ComboBox ui pour les listes déroulantes dont la popup est très large alors qu'on n'a pas trop la place pour une liste large.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class WideDataComboBoxUI extends SynthComboBoxUI {

    private Dimension popupSize;

    public WideDataComboBoxUI(Dimension popupSize) {
        this.popupSize = popupSize;
    }

    protected ComboPopup createPopup() {
        BasicComboPopup popup = new BasicComboPopup(comboBox) {

            /**
             * Configures the list which is used to hold the combo box items in the
             * popup. This method is called when the UI class
             * is created.
             *
             * @see #createList
             */
            @Override
            protected void configureList() {
                list.setFont( comboBox.getFont() );
                list.setCellRenderer( comboBox.getRenderer() );
                list.setFocusable( false );
                list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
                int selectedIndex = comboBox.getSelectedIndex();
                if ( selectedIndex == -1 ) {
                    list.clearSelection();
                }
                else {
                    list.setSelectedIndex( selectedIndex );
                    list.ensureIndexIsVisible( selectedIndex );
                }
                installListListeners();
            }

            /**
             * @inheritDoc
             *
             * Overridden to take into account any popup insets specified in
             * SynthComboBoxUI
             */
            @Override
            protected Rectangle computePopupBounds(int px, int py, int pw, int ph) {
                int width = Math.max(popupSize.width, comboBox.getPreferredSize().width);
                popupSize.setSize(width, getPopupHeightForRowCount(comboBox.getMaximumRowCount()));
                Rectangle popupBounds = super.computePopupBounds(0, comboBox.getBounds().height, width, popupSize.height);
                scroller.setMaximumSize(popupBounds.getSize());
                scroller.setPreferredSize(popupBounds.getSize());
                scroller.setMinimumSize(popupBounds.getSize());
                list.invalidate();
                return popupBounds;
            }

        };
        popup.getAccessibleContext().setAccessibleParent(comboBox);
        return popup;
    }
}
