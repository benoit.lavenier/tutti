package fr.ifremer.tutti.ui.swing.util.attachment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.util.EventObject;

/**
 * To edit attachments from a table cell.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0.2
 */
public class AttachmentCellEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(AttachmentCellEditor.class);

    public static TableCellEditor newEditor(TuttiUI ui) {

        return new AttachmentCellEditor(ui.getHandler().getContext());
    }

    protected JTable table;

    protected AbstractApplicationTableModel<AbstractTuttiBeanUIModel> tableModel;

    protected Integer rowIndex;

    protected Integer columnIndex;

    protected final ButtonAttachment editorButton;

    public AttachmentCellEditor(TuttiUIContext context) {

        this.editorButton = new ButtonAttachment(context, null);
        this.editorButton.setBorder(new LineBorder(Color.BLACK));
        addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(ChangeEvent e) {
                editorButton.setSelected(false);
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
                editorButton.setSelected(false);
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
        this.table = table;
        this.tableModel = (AbstractApplicationTableModel<AbstractTuttiBeanUIModel>) table.getModel();

        rowIndex = row;
        columnIndex = column;

        AttachmentModelAware model =
                (AttachmentModelAware) tableModel.getEntry(row);

        editorButton.init(model);

        return editorButton;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return false;
    }

    @Override
    public Object getCellEditorValue() {

        AttachmentModelAware model = editorButton.getBean();
        Preconditions.checkNotNull(model, "No model found in editor.");

        Object result = model.getAttachment();
        if (log.isDebugEnabled()) {
            log.debug("editor value: " + result);
        }

        return result;
    }

    @Override
    public boolean stopCellEditing() {
        boolean b = super.stopCellEditing();
        if (b) {
            editorButton.setBean(null);
        }
        return b;
    }

    @Override
    public void cancelCellEditing() {
        editorButton.setBean(null);
        super.cancelCellEditing();
    }
}
