package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatchs;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapColumnRowModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.List;

/**
 * Define a benthos batch row.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class AccidentalBatchRowModel extends AbstractTuttiBeanUIModel<AccidentalBatch, AccidentalBatchRowModel>
        implements AttachmentModelAware, AccidentalBatch, CaracteristicMapColumnRowModel {

    private static final long serialVersionUID = 1L;

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final AccidentalBatch editObject =
            AccidentalBatchs.newAccidentalBatch();

    /**
     * Attachments (should never be null).
     *
     * @since 0.2
     */
    protected final List<Attachment> attachment = Lists.newArrayList();

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    protected static final Binder<AccidentalBatch, AccidentalBatchRowModel> fromBeanBinder =
            BinderFactory.newBinder(AccidentalBatch.class,
                                    AccidentalBatchRowModel.class);

    protected static final Binder<AccidentalBatchRowModel, AccidentalBatch> toBeanBinder =
            BinderFactory.newBinder(AccidentalBatchRowModel.class,
                                    AccidentalBatch.class);

    public AccidentalBatchRowModel(WeightUnit weightUnit) {
        super(fromBeanBinder, toBeanBinder);
        this.weightUnit = weightUnit;
    }

    public AccidentalBatchRowModel(WeightUnit weightUnit,
                                   AccidentalBatch entity) {
        this(weightUnit);
        fromEntity(entity);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBeanUIModel                                           --//
    //------------------------------------------------------------------------//

    @Override
    protected AccidentalBatch newEntity() {
        return AccidentalBatchs.newAccidentalBatch();
    }

    @Override
    public void fromEntity(AccidentalBatch entity) {
        super.fromEntity(entity);

        // convert weight
        setWeight(weightUnit.fromEntity(getWeight()));
    }

    @Override
    public AccidentalBatch toEntity() {
        AccidentalBatch result = super.toEntity();

        // convert weight
        result.setWeight(weightUnit.toEntity(getWeight()));
        return result;
    }

    //------------------------------------------------------------------------//
    //-- AccidentalBatch                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public FishingOperation getFishingOperation() {
        return editObject.getFishingOperation();
    }

    @Override
    public void setFishingOperation(FishingOperation fishingOperation) {
        editObject.setFishingOperation(fishingOperation);
    }

    @Override
    public Float getWeight() {
        return editObject.getWeight();
    }

    @Override
    public void setWeight(Float weight) {
        Object oldValue = getWeight();
        editObject.setWeight(weight);
        firePropertyChange(PROPERTY_WEIGHT, oldValue, weight);
    }

    @Override
    public Species getSpecies() {
        return editObject.getSpecies();
    }

    @Override
    public void setSpecies(Species species) {
        Object oldValue = getSpecies();
        editObject.setSpecies(species);
        firePropertyChange(PROPERTY_SPECIES, oldValue, species);
    }

    @Override
    public Float getSize() {
        return editObject.getSize();
    }

    @Override
    public void setSize(Float size) {
        Object oldValue = getSize();
        editObject.setSize(size);
        firePropertyChange(PROPERTY_SIZE, oldValue, size);
    }

    @Override
    public Caracteristic getLengthStepCaracteristic() {
        return editObject.getLengthStepCaracteristic();
    }

    @Override
    public void setLengthStepCaracteristic(Caracteristic lengthStepCaracteristic) {
        Object oldValue = getLengthStepCaracteristic();
        editObject.setLengthStepCaracteristic(lengthStepCaracteristic);
        firePropertyChange(PROPERTY_LENGTH_STEP_CARACTERISTIC, oldValue, lengthStepCaracteristic);
    }

    @Override
    public CaracteristicQualitativeValue getGender() {
        return editObject.getGender();
    }

    @Override
    public void setGender(CaracteristicQualitativeValue gender) {
        Object oldValue = getLengthStepCaracteristic();
        editObject.setGender(gender);
        firePropertyChange(PROPERTY_GENDER, oldValue, gender);
    }

    @Override
    public CaracteristicQualitativeValue getDeadOrAlive() {
        return editObject.getDeadOrAlive();
    }

    @Override
    public void setDeadOrAlive(CaracteristicQualitativeValue deadOrAlive) {
        Object oldValue = getLengthStepCaracteristic();
        editObject.setDeadOrAlive(deadOrAlive);
        firePropertyChange(PROPERTY_DEAD_OR_ALIVE, oldValue, deadOrAlive);
    }

    @Override
    public CaracteristicMap getCaracteristics() {
        return editObject.getCaracteristics();
    }

    @Override
    public void setCaracteristics(CaracteristicMap caracteristics) {
        Object oldValue = getCaracteristics();
        editObject.setCaracteristics(caracteristics);
        firePropertyChange(PROPERTY_CARACTERISTICS, oldValue, caracteristics);
    }

    @Override
    public String getComment() {
        return editObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public void setRankOrder(Integer rankOrder) {
        editObject.setRankOrder(rankOrder);
    }

    @Override
    public Integer getRankOrder() {
        return editObject.getRankOrder();
    }

    @Override
    public String getSynchronizationStatus() {
        return editObject.getSynchronizationStatus();
    }

    @Override
    public void setSynchronizationStatus(String synchronizationStatus) {
        String oldValue = getSynchronizationStatus();
        editObject.setSynchronizationStatus(synchronizationStatus);
        firePropertyChange(PROPERTY_SYNCHRONIZATION_STATUS, oldValue, synchronizationStatus);
    }

    //------------------------------------------------------------------------//
    //-- AttachmentModelAware                                               --//
    //------------------------------------------------------------------------//

    @Override
    public ObjectTypeCode getObjectType() {
        return ObjectTypeCode.SAMPLE;
    }

    @Override
    public Integer getObjectId() {
        return getIdAsInt();
    }

    @Override
    public List<Attachment> getAttachment() {
        return attachment;
    }

    @Override
    public void addAllAttachment(Collection<Attachment> attachments) {
        this.attachment.addAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        this.attachment.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAllAttachment(Collection<Attachment> attachments) {
        this.attachment.removeAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        this.attachment.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }
}
