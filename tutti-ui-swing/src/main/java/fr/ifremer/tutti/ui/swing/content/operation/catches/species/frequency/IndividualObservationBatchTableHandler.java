package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.sampling.SamplingCodePrefix;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation.SamplingCodeCellEditor;
import fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation.SamplingCodeCellRenderer;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellEditor;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellRenderer;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapCellComponent;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellEditor;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellRenderer;
import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.decorator.Decorator;

import javax.swing.JComboBox;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import static org.nuiton.i18n.I18n.t;

/**
 * To manage individual observations.
 *
 * Created on 14/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationBatchTableHandler implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(IndividualObservationBatchTableHandler.class);

    /**
     * Le controleur principal de l'écran.
     */
    private final SpeciesFrequencyUIHandler uiHandler;
    /**
     * Le tableau des observations individuelleS.
     */
    private final JXTable individualObservationTable;
    /**
     * Le modèle globale de l'écran.
     */
    private final SpeciesFrequencyUIModel model;
    /**
     * Le modèle pour gérer les observations individuelles.
     */
    private final IndividualObservationBatchUIModel individualObservationsModel;
    /**
     * Le modèle du tableau des mensurations.
     */
    private final SpeciesFrequencyTableModel frequencyTableModel;
    /**
     * Le modèle du tableau des observations individuelles.
     */
    private final IndividualObservationBatchTableModel individualObservationTableModel;
    /**
     * Le dictionnaire (indexé par identifiant) des caractéristiques de maturités définies dans le protocole.
     */
    private final Map<String, Caracteristic> maturityCaracteristics;
    /**
     * Décorateur d'espèce.
     */
    private final Decorator<Species> speciesDecorator;
    private final Decorator<Caracteristic> caracteristicDecorator;
    private final Decorator<Caracteristic> caracteristicTipDecorator;
    /**
     * Pour calculer la recopie d'une observation individuelle vers les mensurations.
     */
    private final IndividualObservationToFrequencyEngine individualObservationToFrequencyEngine;
    /**
     * Pour calculer les actions à effectuer sur le cache des échantillons.
     */
    private final IndividualObservationToSamplingCacheEngine individualObservationToSamplingCacheEngine;
    /**
     * Pour écouter le changement de la taille sur une ligne.
     */
    private final PropertyChangeListener individualObservationRowSizeChangedListener;
    /**
     * Pour écouter les changement de poids sur une ligne.
     */
    private final PropertyChangeListener individualObservationRowWeightChangedListener;
    /**
     * Pour écouter les changement de code de prélèvement sur une ligne.
     */
    private final PropertyChangeListener individualObservationRowSamplingCodeChangedListener;
    /**
     * Pour écouter les changement de caractéristiques sur une ligne.
     */
    private final PropertyChangeListener individualObservationRowCaracteristicsChangedListener;

    /**
     * Editor for the other caracteristics column.
     * We must pass the updated list of default caracteristic every time we recompute the default caracteristic list.
     * Otherwise, the user can edit the maturity and sex as other caracteristics.
     */
    protected final CaracteristicMapCellComponent.CaracteristicMapCellEditor otherCaracteristicsEditor;

    public IndividualObservationBatchTableHandler(SpeciesFrequencyUI ui) {

        this.model = ui.getModel();
        this.individualObservationsModel = ui.getModel().getIndividualObservationModel();
        this.uiHandler = ui.getHandler();

        this.speciesDecorator = uiHandler.getDecorator(Species.class, DecoratorService.WITH_SURVEY_CODE_NO_NAME);
        this.caracteristicDecorator = uiHandler.getDecorator(Caracteristic.class, DecoratorService.CARACTERISTIC_PARAMETER_ONLY_WITH_UNIT);
        this.caracteristicTipDecorator = uiHandler.getDecorator(Caracteristic.class, DecoratorService.CARACTERISTIC_WITH_UNIT);

        this.individualObservationTable = ui.getObsTable();
        this.frequencyTableModel = uiHandler.getTableModel();
        this.individualObservationToFrequencyEngine = new IndividualObservationToFrequencyEngine(model);
        this.individualObservationToSamplingCacheEngine = new IndividualObservationToSamplingCacheEngine(model);

        List<Caracteristic> maturityCaracteristics = new ArrayList<>(uiHandler.getDataContext().getMaturityCaracteristics());

        this.maturityCaracteristics = TuttiEntities.splitById(maturityCaracteristics);

        // Ecoute quand la taille a changé sur une observation
        this.individualObservationRowSizeChangedListener = event -> {

            IndividualObservationBatchRowModel source = (IndividualObservationBatchRowModel) event.getSource();
            Float oldSize = (Float) event.getOldValue();

            Float weight = source.getWeight();

            CaracteristicQualitativeValue maturity = individualObservationsModel.getMaturityValue(source);
            CaracteristicQualitativeValue gender = individualObservationsModel.getGender(source);

            String samplingCode = source.getSamplingCode();

            boolean oldValid = source.computeValid(oldSize != null, weight != null);
            boolean newValid = source.computeValid();

            IndividualObservationBatchRowState oldState = new IndividualObservationBatchRowState(oldSize, weight, maturity, gender, samplingCode, oldValid);
            IndividualObservationBatchRowState newState = new IndividualObservationBatchRowState(source.getSize(), weight, maturity, gender, samplingCode, newValid);
            onSamplingRowChanged(source, oldState, newState);
            onRowChangedForFrequencies(source, oldState, newState);

            individualObservationsModel.recomputeRowValidState(source);
            model.recomputeCanEditLengthStep();
            model.setModify(true);

        };

        // Ecoute quand le poids a changé sur une observation
        this.individualObservationRowWeightChangedListener = event -> {

            IndividualObservationBatchRowModel source = (IndividualObservationBatchRowModel) event.getSource();
            if (source.withSize()) {

                Float oldWeight = (Float) event.getOldValue();
                Float size = source.getSize();

                CaracteristicQualitativeValue maturity = individualObservationsModel.getMaturityValue(source);
                CaracteristicQualitativeValue gender = individualObservationsModel.getGender(source);

                String samplingCode = source.getSamplingCode();

                boolean oldValid = source.computeValid(size != null, oldWeight != null);
                boolean newValid = source.computeValid();

                IndividualObservationBatchRowState oldState = new IndividualObservationBatchRowState(size, oldWeight, maturity, gender, samplingCode, oldValid);
                IndividualObservationBatchRowState newState = new IndividualObservationBatchRowState(size, source.getWeight(), maturity, gender, samplingCode, newValid);
                onSamplingRowChanged(source, oldState, newState);
                onRowChangedForFrequencies(source, oldState, newState);

            } else {

                individualObservationsModel.recomputeRowValidState(source);

            }

            model.recomputeCanEditLengthStep();
            model.setModify(true);

        };

        // Ecoute quand le code de prélèvement a changé
        this.individualObservationRowSamplingCodeChangedListener = event -> {

            IndividualObservationBatchRowModel source = (IndividualObservationBatchRowModel) event.getSource();
            if (source.withSize()) {

                String oldSamplingCode = (String) event.getOldValue();

                Float size = source.getSize();
                Float weight = source.getWeight();
                CaracteristicQualitativeValue maturity = individualObservationsModel.getMaturityValue(source);
                CaracteristicQualitativeValue gender = individualObservationsModel.getGender(source);

                boolean validState = source.computeValid();

                IndividualObservationBatchRowState oldState = new IndividualObservationBatchRowState(size, weight, maturity, gender, oldSamplingCode, validState);
                IndividualObservationBatchRowState newState = new IndividualObservationBatchRowState(size, weight, maturity, gender, source.getSamplingCode(), validState);
                onSamplingRowChanged(source, oldState, newState);

            } else {

                individualObservationsModel.recomputeRowValidState(source);

            }

            model.setModify(true);

        };

        // Ecoute quand les caractéristiques ont changées
        this.individualObservationRowCaracteristicsChangedListener = event -> {

            IndividualObservationBatchRowModel source = (IndividualObservationBatchRowModel) event.getSource();
            if (source.withSize()) {

                CaracteristicMap oldCaracteristicMap = (CaracteristicMap) event.getOldValue();

                CaracteristicQualitativeValue oldGender = individualObservationsModel.getGender(oldCaracteristicMap);
                CaracteristicQualitativeValue newGender = individualObservationsModel.getGender(source);

                if (!Objects.equals(oldGender, newGender)) {

                    // Le sexe a changé

                    Float size = source.getSize();
                    Float weight = source.getWeight();
                    CaracteristicQualitativeValue maturity = individualObservationsModel.getMaturityValue(source);
                    String samplingCode = source.getSamplingCode();

                    boolean validState = source.computeValid();

                    IndividualObservationBatchRowState oldState = new IndividualObservationBatchRowState(size, weight, maturity, oldGender, samplingCode, validState);
                    IndividualObservationBatchRowState newState = new IndividualObservationBatchRowState(size, weight, maturity, newGender, samplingCode, validState);
                    onSamplingRowChanged(source, oldState, newState);

                } else if (individualObservationsModel.withMaturityCaracteristic()) {

                    CaracteristicQualitativeValue oldMaturity = individualObservationsModel.getMaturityValue(oldCaracteristicMap);
                    CaracteristicQualitativeValue newMaturity = individualObservationsModel.getMaturityValue(source);

                    if (!Objects.equals(oldMaturity, newMaturity)) {

                        // La maturité a changée

                        Float size = source.getSize();
                        Float weight = source.getWeight();
                        CaracteristicQualitativeValue gender = individualObservationsModel.getGender(source);
                        String samplingCode = source.getSamplingCode();

                        boolean validState = source.computeValid();

                        IndividualObservationBatchRowState oldState = new IndividualObservationBatchRowState(size, weight, oldMaturity, gender, samplingCode, validState);
                        IndividualObservationBatchRowState newState = new IndividualObservationBatchRowState(size, weight, newMaturity, gender, samplingCode, validState);
                        onSamplingRowChanged(source, oldState, newState);

                    }

                }

            } else {

                individualObservationsModel.recomputeRowValidState(source);

            }

            model.setModify(true);

        };

        // can show / hide some columns in model
        individualObservationTable.setColumnControlVisible(true);

        // create obsTable column model
        DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        {
            // Rank column

            uiHandler.addIntegerColumnToModel(columnModel,
                                              IndividualObservationBatchTableModel.RANK,
                                              TuttiUI.INT_10_DIGITS_PATTERN,
                                              individualObservationTable);

        }

        { // Size column

            uiHandler.addFloatColumnToModel(columnModel,
                                            IndividualObservationBatchTableModel.SIZE,
                                            TuttiUI.DECIMAL3_PATTERN,
                                            individualObservationTable);
        }

        WeightUnit individualObservationWeightUnit = uiHandler.getConfig().getIndividualObservationWeightUnit();

        { // Weight column

            uiHandler.addFloatColumnToModel(columnModel,
                                            IndividualObservationBatchTableModel.WEIGHT,
                                            individualObservationWeightUnit,
                                            individualObservationTable);
        }

        Caracteristic sexCaracteristic = individualObservationsModel.getSexCaracteristic();

        { // sex column

            uiHandler.addCaracteristicColumnToModel(individualObservationTable,
                                                    columnModel,
                                                    sexCaracteristic);
        }

        List<Caracteristic> defaultCaracteristic = new ArrayList<>(individualObservationsModel.getProtocolIndividualObservationCaracteristics());
        // on retire le sexe puisqu'il a été ajouté avant
        defaultCaracteristic.remove(sexCaracteristic);

        for (Caracteristic caracteristic : defaultCaracteristic) {
            uiHandler.addCaracteristicColumnToModel(individualObservationTable,
                                                    columnModel,
                                                    caracteristic);
        }


        // Maturity caracteristic column

        Decorator<CaracteristicQualitativeValue> caracteristicQualitativeDecorator = uiHandler.getDecorator(CaracteristicQualitativeValue.class, null);
        uiHandler.addComboDataColumnToModel(columnModel,
                                            IndividualObservationBatchTableModel.MATURITY,
                                            caracteristicQualitativeDecorator,
                                            new ArrayList<>());

        { // Other caracteristics column

            otherCaracteristicsEditor = CaracteristicMapCellComponent.newEditor(ui, new HashSet<>());
            uiHandler.addColumnToModel(columnModel,
                                       otherCaracteristicsEditor,
                                       CaracteristicMapCellComponent.newRender(uiHandler.getContext()),
                                       IndividualObservationBatchTableModel.OTHER_CARACTERISTICS);

        }

        {
            // Smapling code column

            uiHandler.addColumnToModel(columnModel,
                                       SamplingCodeCellEditor.newEditor(model),
                                       SamplingCodeCellRenderer.newRender(),
                                       IndividualObservationBatchTableModel.SAMPLING_CODE);

        }

        { // Comment column

            uiHandler.addColumnToModel(columnModel,
                                       CommentCellEditor.newEditor(ui),
                                       CommentCellRenderer.newRender(),
                                       IndividualObservationBatchTableModel.COMMENT);

        }

        { // File column

            uiHandler.addColumnToModel(columnModel,
                                       AttachmentCellEditor.newEditor(ui),
                                       AttachmentCellRenderer.newRender(uiHandler.getDecorator(Attachment.class, null)),
                                       IndividualObservationBatchTableModel.ATTACHMENT);
        }

        // create table model

        // Pour installer les listener sur chaque ligne d'observation individuelle
        Function<IndividualObservationBatchRowModel, Void> installListenersOnRow = row -> {
            row.addPropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_SIZE, individualObservationRowSizeChangedListener);
            row.addPropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_WEIGHT, individualObservationRowWeightChangedListener);
            row.addPropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_SAMPLING_CODE, individualObservationRowSamplingCodeChangedListener);
            row.addPropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_CARACTERISTICS, individualObservationRowCaracteristicsChangedListener);
            row.addPropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_DEFAULT_CARACTERISTICS, individualObservationRowCaracteristicsChangedListener);
            return null;
        };

        // Pour désinstaller les listener sur chaque ligne d'observation individuelle.
        Function<IndividualObservationBatchRowModel, Void> uninstallListenersOnRow = row -> {
            row.removePropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_SIZE, individualObservationRowSizeChangedListener);
            row.removePropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_WEIGHT, individualObservationRowWeightChangedListener);
            row.removePropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_SAMPLING_CODE, individualObservationRowSamplingCodeChangedListener);
            row.removePropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_CARACTERISTICS, individualObservationRowCaracteristicsChangedListener);
            row.removePropertyChangeListener(IndividualObservationBatchRowModel.PROPERTY_DEFAULT_CARACTERISTICS, individualObservationRowCaracteristicsChangedListener);
            return null;
        };
        individualObservationTableModel = new IndividualObservationBatchTableModel(individualObservationWeightUnit,
                                                                                   model,
                                                                                   columnModel,
                                                                                   installListenersOnRow,
                                                                                   uninstallListenersOnRow);
        individualObservationsModel.setIndividualObservationTableModel(individualObservationTableModel);

        individualObservationTable.setModel(individualObservationTableModel);
        individualObservationTable.setColumnModel(columnModel);

        // by default do not authorize to change column orders
        individualObservationTable.getTableHeader().setReorderingAllowed(false);

        uiHandler.addHighlighters(individualObservationTable);

        Color cellWithValueColor = uiHandler.getConfig().getColorCellWithValue();

        Highlighter commentHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.IdentifierHighlightPredicate(IndividualObservationBatchTableModel.COMMENT),
                        // for not null value
                        (renderer, adapter) -> {
                            String value = (String) adapter.getValue();
                            return StringUtils.isNotBlank(value);
                        }), cellWithValueColor);
        individualObservationTable.addHighlighter(commentHighlighter);

        uiHandler.installTableKeyListener(columnModel, individualObservationTable, false);

        // always scroll to selected row
        SwingUtil.scrollToTableSelection(individualObservationTable);

        individualObservationsModel.addPropertyChangeListener(IndividualObservationBatchUIModel.PROPERTY_ROWS, evt -> individualObservationTableModel.setRows((List<IndividualObservationBatchRowModel>) evt.getNewValue()));

        // Pour mettre à jour les mensurations suite au changement du mode de recopie des observations individuelles
        model.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_COPY_INDIVIDUAL_OBSERVATION_MODE, evt -> {

            SpeciesFrequencyUIModel source = (SpeciesFrequencyUIModel) evt.getSource();
            CopyIndividualObservationMode oldCopyMode = (CopyIndividualObservationMode) evt.getOldValue();
            CopyIndividualObservationMode newCopyMode = (CopyIndividualObservationMode) evt.getNewValue();

            if (newCopyMode == null) {

                return;
            }
            boolean nothingCopyMode = CopyIndividualObservationMode.NOTHING == newCopyMode;

            if (!nothingCopyMode) {

                source.setFrequenciesConfigurationMode(FrequencyConfigurationMode.RAFALE);
                source.setAddIndividualObservationOnRafale(true);

            }

            // si on est en initialisation, ca ne sert à rien de regénérer les lignes, elles sont sensées être chargées
            if (source.isInitBatchEdition()) {

                if (log.isInfoEnabled()) {
                    log.info("Skip recompute frequencies from indivudal observations (flag initBatchEdition is on), copyIndividualObservationMode changed from " + oldCopyMode + " to " + newCopyMode);
                }
                return;
            }

            model.clear();
            model.reloadRows();

            // On change le mode de recopie sur toutes les observation individuelles
            individualObservationsModel.setCopyIndividualObservationMode(newCopyMode);

            // Recalcul des mensurations à partir des observations individuelles
            frequencyTableModel.reloadRowsFromIndividualObservations();

        });

        model.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_LENGTH_STEP_CARACTERISTIC, evt -> {

            Caracteristic newValue = (Caracteristic) evt.getNewValue();
            individualObservationTableModel.setLengthstepCaracteristic(newValue);

        });

    }

    @Override
    public void close() {

        model.getIndividualObservationUICache().close();
        model.getSamplingCodeUICache().close();

    }

    public List<IndividualObservationBatchRowModel> loadRows(Species species, List<IndividualObservationBatchRowModel> individualObservations) {

        individualObservationTableModel.setSpecies(species);

        SamplingCodePrefix samplingCodePrefix = new SamplingCodePrefix(uiHandler.getConfig().getSamplingCodePrefix(), speciesDecorator.toString(species));
        individualObservationsModel.setSamplingCodePrefix(samplingCodePrefix);

        return individualObservationTableModel.loadRows(individualObservations);

    }

    public void loadSpeciesBatch(SpeciesBatchRowModel speciesBatch, List<IndividualObservationBatchRowModel> individualObservationRows, boolean addToCache) {

        individualObservationsModel.setRows(individualObservationRows);
//        individualObservationsModel.recomputeRowsValidateState();

        model.getIndividualObservationUICache().init(speciesBatch.getSpecies(), individualObservationsModel.getRows(), addToCache);
        model.getSamplingCodeUICache().init(speciesBatch.getSpecies(), individualObservationsModel.getRows(), addToCache);

        individualObservationTable.packAll();

    }

    public boolean isSampleCodeMenusEnabled(int modelRowIndex) {
        return modelRowIndex >= 0 && individualObservationTable.getSelectedRowCount() == 1
                && individualObservationTableModel.getRows().get(individualObservationTable.getSelectedRow()).withSamplingCode();
    }

    public void initMaturityCaracteristic(SpeciesProtocol speciesProtocol) {

        Caracteristic maturityCaracteristic;

        if (speciesProtocol == null) {
            maturityCaracteristic = null;
        } else {
            maturityCaracteristic = maturityCaracteristics.get(speciesProtocol.getMaturityPmfmId());
        }

        // Add maturity column if necessary

        individualObservationsModel.setMaturityCaracteristic(maturityCaracteristic);

        MaturityColumnIdentifier columnIdentifier = IndividualObservationBatchTableModel.MATURITY;
        columnIdentifier.setCaracteristic(maturityCaracteristic);

        TableColumnExt columnModel = ((TableColumnModelExt) individualObservationTable.getColumnModel()).getColumnExt(columnIdentifier);

        boolean noMaturity = maturityCaracteristic == null;

        columnModel.setVisible(!noMaturity);
        ComboBoxCellEditor cellEditor = (ComboBoxCellEditor) columnModel.getCellEditor();
        JComboBox comboBox = (JComboBox) cellEditor.getComponent();

        List<CaracteristicQualitativeValue> dataList;

        String title;
        String tip;

        if (noMaturity) {
            dataList = Collections.emptyList();
            title = t(columnIdentifier.getHeaderI18nKey());
            tip = t(columnIdentifier.getHeaderTipI18nKey());

        } else {
            dataList = new ArrayList<>(maturityCaracteristic.getQualitativeValue());
            if (!dataList.isEmpty() && dataList.get(0) != null) {
                dataList.add(0, null);
            }
            title = caracteristicDecorator.toString(maturityCaracteristic);
            tip = caracteristicTipDecorator.toString(maturityCaracteristic);
        }
        columnModel.setTitle(title);
        columnModel.setToolTipText(tip);
        SwingUtil.fillComboBox(comboBox, dataList, null);

    }

    public void initDefaultCaracteristics(SpeciesBatchRowModel speciesBatch) {

        // compute the default caracteristics
        Set<Caracteristic> defaultCaracteristics = new HashSet<>();
        defaultCaracteristics.addAll(individualObservationsModel.getProtocolIndividualObservationCaracteristics());
        defaultCaracteristics.add(individualObservationsModel.getSexCaracteristic());
        if (individualObservationsModel.withMaturityCaracteristic()) {
            defaultCaracteristics.add(individualObservationsModel.getMaturityCaracteristic());
        }
        // update the caracteristics which are not editable in the other caracteristic editor
        otherCaracteristicsEditor.setCaracteristicsToSkip(defaultCaracteristics);

        // create the default caracteristic map
        CaracteristicMap defaultCaracteristicMap = CaracteristicMap.fromCollection(defaultCaracteristics);

        // and put the values of the sampling categories, if the categories are in the map
        CaracteristicMap sampleCategoryValues = new CaracteristicMap();
        Iterator<SampleCategory<?>> iterator = speciesBatch.iterator();
        iterator.forEachRemaining(sampleCategory -> {
            Caracteristic caracteristic = sampleCategory.getCategoryDef().getCaracteristic();
            defaultCaracteristicMap.replace(caracteristic, sampleCategory.getCategoryValue());
            sampleCategoryValues.put(caracteristic, sampleCategory.getCategoryValue());
        });

        individualObservationTableModel.setDefaultCaracteristics(defaultCaracteristicMap);
        individualObservationsModel.setNotEditableCaracteristic(sampleCategoryValues.keySet());

    }

    private void onSamplingRowChanged(IndividualObservationBatchRowModel row,
                                      IndividualObservationBatchRowState oldValue,
                                      IndividualObservationBatchRowState newValue) {

        boolean needUpdateSamplingNotificationZone = individualObservationToSamplingCacheEngine.computeSamplingCacheUpdate(oldValue, newValue);

        if (needUpdateSamplingNotificationZone) {

            // recalcul de la zone de notification
            individualObservationsModel.getSamplingNotificationZoneModel().setUpdatedRow(row);

        }

    }

    private void onRowChangedForFrequencies(IndividualObservationBatchRowModel row,
                                            IndividualObservationBatchRowState oldValue,
                                            IndividualObservationBatchRowState newValue) {

        boolean needRecomputeRowsValidState = individualObservationToFrequencyEngine.computeFrequencyUpdate(model.getCopyIndividualObservationMode(), oldValue, newValue);

        if (needRecomputeRowsValidState) {

            // l'état de validité de la ligne a changé, on recalcule les lignes en erreurs
            individualObservationsModel.recomputeRowValidState(row);

        }

    }

}

