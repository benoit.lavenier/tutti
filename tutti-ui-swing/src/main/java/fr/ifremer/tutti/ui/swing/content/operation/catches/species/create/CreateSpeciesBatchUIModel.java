package fr.ifremer.tutti.ui.swing.content.operation.catches.species.create;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SelectedCategoryAble;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.split.SplitSpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Model of {@link CreateSpeciesBatchUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class CreateSpeciesBatchUIModel extends AbstractTuttiTableUIModel<CreateSpeciesBatchUIModel, SplitSpeciesBatchRowModel, CreateSpeciesBatchUIModel> implements SelectedCategoryAble {

    private final static Log log = LogFactory.getLog(CreateSpeciesBatchUIModel.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_AVAILABLE_SPECIES = "availableSpecies";

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_SAMPLE_CATEGORY = "sampleCategory";

    public static final String PROPERTY_BATCH_SAMPLE_CATEGORY_WEIGHT = "batchSampleCategoryWeight";

    public static final String PROPERTY_LAST_SAMPLE_CATEGORY_USED = "lastSampleCategoryUsed";

    public static final String PROPERTY_BATCH_WEIGHT = "batchWeight";

    public static final String PROPERTY_BATCH_COUNT = "batchCount";

    public static final String PROPERTY_CATEGORIZATION_ENABLED = "categorizationEnabled";

    public static final String PROPERTY_CATEGORY = "category";

    public static final String PROPERTY_SELECTED_CATEGORY = "selectedCategory";

    public static final String PROPERTY_SAMPLE_WEIGHT = "sampleWeight";

    public static final String PROPERTY_SPECIES_PROTOCOL_FREQUENCY_MODE = "speciesProtocolFrequencyMode";

    /**
     * Frequency mode according to the selected species and the protocol
     */
    public enum SpeciesProtocolFrequencyMode {
        MEASURE(n("tutti.createSpeciesBatch.species.toMeasure")),
        COUNT(n("tutti.createSpeciesBatch.species.toCount"));

        private String label;

        SpeciesProtocolFrequencyMode(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return t(label);
        }
    }

    /**
     * All available species (sets by protocol).
     *
     * @since 0.3
     */
    protected List<Species> availableSpecies;

    /**
     * Species to select.
     *
     * @since 0.3
     */
    protected Species species;

    /**
     * First sample category.
     *
     * @since 2.4
     */
    protected CaracteristicQualitativeValue sampleCategory;

    /**
     * Last first category used.
     *
     * @since 4.2
     */
    protected CaracteristicQualitativeValue lastSampleCategoryUsed;

    /**
     * Batch weight.
     *
     * @since 0.3
     */
    protected Float batchSampleCategoryWeight;

    /**
     * Batch sample weight.
     *
     * @since 4.2
     */
    protected Float batchWeight;

    /**
     * Batch count.
     *
     * @since 2.5
     */
    protected Integer batchCount;

    /**
     * Sample categories.
     *
     * @since 2.5
     */
    protected List<SampleCategoryModelEntry> category;

    /**
     * Selected Sample category.
     *
     * @since 2.5
     */
    protected SampleCategoryModelEntry selectedCategory;

    /**
     * Sample weight of split batches.
     *
     * @since 2.5
     */
    protected Float sampleWeight;

    private final SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;
    /**
     * Model of sample categories.
     *
     * @since 2.5
     */
    protected SampleCategoryModel sampleCategoryModel;

    /**
     * Already used species by sample category.
     *
     * @since 0.3
     */
    protected final Multimap<CaracteristicQualitativeValue, Species> speciesUsed = ArrayListMultimap.create();

    /**
     * Frequency measurement mode according to the species
     *
     * @since 4.3
     */
    protected SpeciesProtocolFrequencyMode speciesProtocolFrequencyMode;

    public CreateSpeciesBatchUIModel(SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport, SampleCategoryModel sampleCategoryModel) {
        super(CreateSpeciesBatchUIModel.class, null, null);
        this.speciesOrBenthosBatchUISupport = speciesOrBenthosBatchUISupport;
        this.sampleCategoryModel = sampleCategoryModel;
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return sampleCategoryModel;
    }

    public List<Species> getAvailableSpecies() {
        if (log.isDebugEnabled()) {
            log.debug("getAvailableSpecies " + availableSpecies);
        }
        return availableSpecies;
    }

    public void setAvailableSpecies(List<Species> availableSpecies) {

        if (log.isDebugEnabled()) {
            log.debug("setAvailableSpecies " + availableSpecies);
        }
        Object oldValue = getAvailableSpecies();
        this.availableSpecies = availableSpecies;
        firePropertyChange(PROPERTY_AVAILABLE_SPECIES, oldValue, availableSpecies);
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        Object oldSpecies = getSpecies();
        Object oldCategorizationEnabled = isCategorizationEnabled();
        this.species = species;
        firePropertyChange(PROPERTY_SPECIES, oldSpecies, getSpecies());
        firePropertyChange(PROPERTY_CATEGORIZATION_ENABLED, oldCategorizationEnabled, isCategorizationEnabled());
    }

    public CaracteristicQualitativeValue getSampleCategory() {
        return sampleCategory;
    }

    public void setSampleCategory(CaracteristicQualitativeValue sampleCategory) {
        Object oldValue = getSampleCategory();
        this.sampleCategory = sampleCategory;
        firePropertyChange(PROPERTY_SAMPLE_CATEGORY, oldValue, sampleCategory);
    }

    public CaracteristicQualitativeValue getLastSampleCategoryUsed() {
        return lastSampleCategoryUsed;
    }

    public void setLastSampleCategoryUsed(CaracteristicQualitativeValue lastSampleCategoryUsed) {
        Object oldValue = getLastSampleCategoryUsed();
        this.lastSampleCategoryUsed = lastSampleCategoryUsed;
        firePropertyChange(PROPERTY_LAST_SAMPLE_CATEGORY_USED, oldValue, lastSampleCategoryUsed);
    }

    public Float getBatchWeight() {
        return batchWeight;
    }

    public void setBatchWeight(Float batchWeight) {
        Object oldValue = getBatchWeight();
        Object oldCategorizationEnabled = isCategorizationEnabled();
        this.batchWeight = batchWeight;
        firePropertyChange(PROPERTY_BATCH_WEIGHT, oldValue, batchWeight);
        firePropertyChange(PROPERTY_CATEGORIZATION_ENABLED, oldCategorizationEnabled, isCategorizationEnabled());
    }

    public Float getBatchSampleCategoryWeight() {
        return batchSampleCategoryWeight;
    }

    public void setBatchSampleCategoryWeight(Float batchSampleCategoryWeight) {
        Object oldValue = getBatchSampleCategoryWeight();
        this.batchSampleCategoryWeight = batchSampleCategoryWeight;
        firePropertyChange(PROPERTY_BATCH_SAMPLE_CATEGORY_WEIGHT, oldValue, batchSampleCategoryWeight);
    }

    public Integer getBatchCount() {
        return batchCount;
    }

    public void setBatchCount(Integer batchCount) {
        Object oldValue = getBatchCount();
        Object oldCategorizationEnabled = isCategorizationEnabled();
        this.batchCount = batchCount;
        firePropertyChange(PROPERTY_BATCH_COUNT, oldValue, batchCount);
        firePropertyChange(PROPERTY_CATEGORIZATION_ENABLED, oldCategorizationEnabled, isCategorizationEnabled());
    }

    public boolean isCategorizationEnabled() {
        return getSpecies() != null && getBatchCount() == null && getBatchWeight() == null;
    }

    public List<SampleCategoryModelEntry> getCategory() {
        return category;
    }

    public void setCategory(List<SampleCategoryModelEntry> category) {
        Object oldValue = getCategory();
        this.category = category;
        firePropertyChange(PROPERTY_CATEGORY, oldValue, category);
    }

    @Override
    public SampleCategoryModelEntry getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(SampleCategoryModelEntry selectedCategory) {
        Object oldValue = getSelectedCategory();
        this.selectedCategory = selectedCategory;
        firePropertyChange(PROPERTY_SELECTED_CATEGORY, oldValue, selectedCategory);
    }

    public Float getSampleWeight() {
        return sampleWeight;
    }

    public void setSampleWeight(Float sampleWeight) {
        Object oldValue = getSampleWeight();
        this.sampleWeight = sampleWeight;
        firePropertyChange(PROPERTY_SAMPLE_WEIGHT, oldValue, sampleWeight);
    }

    public Multimap<CaracteristicQualitativeValue, Species> getSpeciesUsed() {
        return speciesUsed;
    }

    public boolean isSpeciesAndCategoryAvailable() {
        return species != null && sampleCategory != null &&
                         isSpeciesAndCategoryAvailable(species, sampleCategory);
    }

    public boolean isSpeciesAndCategoryAvailable(Species species,
                                                 CaracteristicQualitativeValue sampleCategory) {
        return !speciesUsed.containsEntry(sampleCategory, species);
    }

    public boolean isCategoryIsFilled() {
        boolean result = selectedCategory != null;
        if (result) {
            result = false;
            for (SplitSpeciesBatchRowModel rowModel : getRows()) {
                if (rowModel.isValid()) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public SpeciesProtocolFrequencyMode getSpeciesProtocolFrequencyMode() {
        return speciesProtocolFrequencyMode;
    }

    public void setSpeciesProtocolFrequencyMode(SpeciesProtocolFrequencyMode speciesProtocolFrequencyMode) {
        Object oldValue = getSpeciesProtocolFrequencyMode();
        this.speciesProtocolFrequencyMode = speciesProtocolFrequencyMode;
        firePropertyChange(PROPERTY_SPECIES_PROTOCOL_FREQUENCY_MODE, oldValue, speciesProtocolFrequencyMode);
    }

    @Override
    protected CreateSpeciesBatchUIModel newEntity() {
        return new CreateSpeciesBatchUIModel(speciesOrBenthosBatchUISupport, sampleCategoryModel);
    }

    public SpeciesOrBenthosBatchUISupport getSpeciesOrBenthosBatchUISupport() {
        return speciesOrBenthosBatchUISupport;
    }

    public WeightUnit getWeightUnit() {
        return speciesOrBenthosBatchUISupport.getWeightUnit();
    }
}
