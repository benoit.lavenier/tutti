package fr.ifremer.tutti.ui.swing.content.operation.catches.species;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportResult;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 17/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SpeciesBatchUISupportImpl extends SpeciesOrBenthosBatchUISupport {

    protected static final Map<String, String> CATCHES_UI_MODEL_PROPERTIES_MAPPING =
            ImmutableMap.<String, String>builder()
                    .put(EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT, PROPERTY_TOTAL_COMPUTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT, PROPERTY_TOTAL_SORTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_UNSORTED_COMPUTED_WEIGHT, PROPERTY_TOTAL_UNSORTED_COMPUTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT, PROPERTY_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT, PROPERTY_TOTAL_INERT_WEIGHT)
                    .put(EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, PROPERTY_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT)
                    .build();

    public SpeciesBatchUISupportImpl(TuttiUIContext context, EditCatchesUIModel catchesUIModel, WeightUnit weightUnit) {
        super(context, catchesUIModel, CATCHES_UI_MODEL_PROPERTIES_MAPPING, weightUnit);
    }

    @Override
    public BatchContainer<SpeciesBatch> getRootSpeciesBatch(Integer batchId) {
        return getPersistenceService().getRootSpeciesBatch(batchId, true);
    }

    @Override
    public String getTitle() {
        return n("tutti.label.tab.species");
    }

    @Override
    public boolean canPupitriImport() {
        return true;
    }

    @Override
    public boolean canPsionImport() {
        return true;
    }

    @Override
    public boolean canBigfinImport() {
        return true;
    }

    @Override
    public Float getTotalComputedWeight() {
        return catchesUIModel.getSpeciesTotalComputedWeight();
    }

    @Override
    public void setTotalComputedWeight(Float totalComputedWeight) {
        catchesUIModel.setSpeciesTotalComputedWeight(totalComputedWeight);
    }

    @Override
    public ComputableData<Float> getTotalSortedComputedOrNotWeight() {
        return catchesUIModel.getSpeciesTotalSortedComputedOrNotWeight();
    }

    @Override
    public Float getTotalSortedWeight() {
        return catchesUIModel.getSpeciesTotalSortedWeight();
    }

    @Override
    public void setTotalSortedWeight(Float totalSortedWeight) {
        catchesUIModel.setSpeciesTotalSortedWeight(totalSortedWeight);
    }

    @Override
    public Float getTotalSortedComputedWeight() {
        return catchesUIModel.getSpeciesTotalSortedComputedWeight();
    }

    @Override
    public void setTotalSortedComputedWeight(Float totalSortedComputedWeight) {
        catchesUIModel.setSpeciesTotalSortedComputedWeight(totalSortedComputedWeight);
    }

    @Override
    public Float getTotalUnsortedComputedWeight() {
        return catchesUIModel.getSpeciesTotalUnsortedComputedWeight();
    }

    @Override
    public void setTotalUnsortedComputedWeight(Float totalUnsortedComputedWeight) {
        catchesUIModel.setSpeciesTotalUnsortedComputedWeight(totalUnsortedComputedWeight);
    }

    @Override
    public Float getTotalSampleSortedComputedWeight() {
        return catchesUIModel.getSpeciesTotalSampleSortedComputedWeight();
    }

    @Override
    public void setTotalSampleSortedComputedWeight(Float totalSampleSortedComputedWeight) {
        catchesUIModel.setSpeciesTotalSampleSortedComputedWeight(totalSampleSortedComputedWeight);
    }

    @Override
    public ComputableData<Float> getTotalInertComputedOrNotWeight() {
        return catchesUIModel.getSpeciesTotalInertComputedOrNotWeight();
    }

    @Override
    public Float getTotalInertWeight() {
        return catchesUIModel.getSpeciesTotalInertWeight();
    }

    @Override
    public void setTotalInertWeight(Float totalInertWeight) {
        catchesUIModel.setSpeciesTotalInertWeight(totalInertWeight);
    }

    @Override
    public Float getTotalInertComputedWeight() {
        return catchesUIModel.getSpeciesTotalInertComputedWeight();
    }

    @Override
    public void setTotalInertComputedWeight(Float totalInertComputedWeight) {
        catchesUIModel.setSpeciesTotalInertComputedWeight(totalInertComputedWeight);
    }

    @Override
    public ComputableData<Float> getTotalLivingNotItemizedComputedOrNotWeight() {
        return catchesUIModel.getSpeciesTotalLivingNotItemizedComputedOrNotWeight();
    }

    @Override
    public Float getTotalLivingNotItemizedWeight() {
        return catchesUIModel.getSpeciesTotalLivingNotItemizedWeight();
    }

    @Override
    public void setTotalLivingNotItemizedWeight(Float totalLivingNotItemizedWeight) {
        catchesUIModel.setSpeciesTotalLivingNotItemizedWeight(totalLivingNotItemizedWeight);
    }

    @Override
    public Float getTotalLivingNotItemizedComputedWeight() {
        return catchesUIModel.getSpeciesTotalLivingNotItemizedComputedWeight();
    }

    @Override
    public void setTotalLivingNotItemizedComputedWeight(Float totalLivingNotItemizedComputedWeight) {
        catchesUIModel.setSpeciesTotalLivingNotItemizedComputedWeight(totalLivingNotItemizedComputedWeight);
    }

    @Override
    public Integer getDistinctSortedSpeciesCount() {
        return catchesUIModel.getSpeciesDistinctSortedSpeciesCount();
    }

    @Override
    public void setDistinctSortedSpeciesCount(Integer distinctSortedSpeciesCount) {
        catchesUIModel.setSpeciesDistinctSortedSpeciesCount(distinctSortedSpeciesCount);
    }

    @Override
    public Integer getDistinctUnsortedSpeciesCount() {
        return catchesUIModel.getSpeciesDistinctUnsortedSpeciesCount();
    }

    @Override
    public void setDistinctUnsortedSpeciesCount(Integer distinctUnsortedSpeciesCount) {
        catchesUIModel.setSpeciesDistinctUnsortedSpeciesCount(distinctUnsortedSpeciesCount);
    }

    @Override
    public Map<String, Object> importMultiPost(File file, FishingOperation operation, boolean importFrequencies, boolean importIndivudalObservations) {
        return context.getMultiPostImportService().importSpecies(file, operation, importFrequencies, importIndivudalObservations);
    }

    @Override
    public MultiPostImportResult importMultiPost(File file, FishingOperation operation, SpeciesBatch speciesBatch, boolean importFrequencies, boolean importIndivudalObservations) {
        return context.getMultiPostImportService().importSpeciesBatch(file, operation, speciesBatch, importFrequencies, importIndivudalObservations);
    }

    @Override
    public void exportMultiPost(File file, FishingOperation operation, boolean importFrequencies, boolean importIndivudalObservations) {
        context.getMultiPostExportService().exportSpecies(file, operation, importFrequencies, importIndivudalObservations);
    }

    @Override
    public void exportMultiPost(File file, FishingOperation operation, SpeciesBatch speciesBatch, boolean importFrequencies, boolean importIndivudalObservations) {
        context.getMultiPostExportService().exportBatch(file, operation, speciesBatch, importFrequencies, importIndivudalObservations);
    }

    @Override
    public SpeciesBatch createBatch(SpeciesBatch speciesBatch, Integer parentBatchId) {
        return getPersistenceService().createSpeciesBatch(speciesBatch, parentBatchId, true);
    }

    @Override
    public SpeciesBatch saveBatch(SpeciesBatch speciesBatch) {
        return getPersistenceService().saveSpeciesBatch(speciesBatch);
    }

    @Override
    public List<SpeciesBatchFrequency> saveBatchFrequencies(Integer speciesBatchId, List<SpeciesBatchFrequency> frequency) {
        return getPersistenceService().saveSpeciesBatchFrequency(speciesBatchId, frequency);
    }

    @Override
    public List<Species> getReferentSpeciesWithSurveyCode(boolean restrictToProtocol) {
        return context.getDataContext().getReferentSpeciesWithSurveyCode(restrictToProtocol);
    }

    @Override
    public List<Species> getReferentOtherSpeciesWithSurveyCode(boolean restrictToProtocol) {
        return context.getDataContext().getReferentBenthosWithSurveyCode(restrictToProtocol);
    }

    @Override
    public List<SpeciesProtocol> getSpeciesFromProtocol() {
        return context.getDataContext().getProtocol().getSpecies();
    }

    @Override
    public void deleteSpeciesSubBatch(Integer speciesBatchId) {
        getPersistenceService().deleteSpeciesSubBatch(speciesBatchId);
    }

    @Override
    public void deleteSpeciesBatch(Integer speciesBatchId) {
        getPersistenceService().deleteSpeciesBatch(speciesBatchId);
    }
}
