package fr.ifremer.tutti.ui.swing.content.genericformat.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportConfiguration;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportService;
import fr.ifremer.tutti.service.genericformat.GenericFormatValidateFileResult;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUI;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUIHandler;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Component;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportAction extends LongActionSupport<GenericFormatImportUIModel, GenericFormatImportUI, GenericFormatImportUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatImportAction.class);

    public static final DateFormat df = new SimpleDateFormat("yyy-MM-dd-hh-mm");

    private File backupFile;

    private GenericFormatImportResult importResult;

    protected boolean overrideProtocol;

    public GenericFormatImportAction(GenericFormatImportUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            doAction = getModel().isCanValidate();

        }

        // Check override data
        if (doAction) {

            doAction = acceptOverrideData();

        }

        // Check if protocol already exist with same name
        if (doAction) {

            doAction = acceptProtocol();

        }

        // Do a backup before import

        if (doAction && !getConfig().isGenericFormatImportSkipBackup()) {

            // choose file to export
            backupFile = saveFileWithStartDirectory(
                    getConfig().getDbBackupDirectory(),
                    false,
                    "tutti-db-" + df.format(new Date()),
                    "zip",
                    t("tutti.genericFormat.title.choose.dbBackupFile"),
                    t("tutti.genericFormat.action.chooseDbBackupFile"),
                    "^.+\\.zip$", t("tutti.common.file.genericFormat"));

            if (backupFile == null) {

                displayWarningMessage(
                        t("tutti.dbManager.title.backup.db"),
                        t("tutti.dbManager.action.importdb.no.backup.db.choosen"));

                doAction = false;
            }

        }

        if (doAction) {

            // Remove any previous import result
            updateResult(null);

        }

        return doAction;

    }

    @Override
    public void doAction() throws Exception {

        getModel().setImportReportFile(getConfig().newTempFile("genericFormatImportReport", ".pdf"));

        GenericFormatImportConfiguration configuration = getModel().toImportConfiguration();

        configuration.setOverrideProtocol(overrideProtocol);

        GenericFormatImportService service = getContext().getGenericFormatImportService();

        boolean doBackup = !getConfig().isGenericFormatImportSkipBackup() && backupFile != null;

        int nbSteps = service.getImportProgramNbSteps(configuration);
        if (doBackup) {
            nbSteps += 3;
        }

        if (log.isInfoEnabled()) {
            log.info("Import nb steps: " + nbSteps);
        }
        createProgressionModelIfRequired(nbSteps);

        if (doBackup) {

            // close db
            getProgressionModel().increments(t("tutti.genericFormatImport.step.closeDb"));
            getContext().closePersistenceService();

            // backup db
            getProgressionModel().increments(t("tutti.genericFormatImport.step.backupDb", backupFile));
            getContext().getPersistenceService().exportDb(backupFile);

            // reopen db
            getProgressionModel().increments(t("tutti.genericFormatImport.step.reopenDb", backupFile));
            getContext().openPersistenceService();

            // Must reload service
            service = getContext().getGenericFormatImportService();

        }

        Program program = getModel().getProgram();

        File importFile = getModel().getImportFile();

        if (log.isInfoEnabled()) {
            log.info("Do generic format import for program: " + program.getName() + " from file: " + importFile);
        }

        importResult = service.importProgram(configuration, getProgressionModel());

    }

    @Override
    public void postSuccessAction() {

        updateResult(importResult);

        if (importResult.getProtocol() != null) {
            getDataContext().setProtocolId(importResult.getProtocol().getId());
        }

    }

    @Override
    public void releaseAction() {
        backupFile = null;
        importResult = null;
        overrideProtocol = false;
        super.releaseAction();
    }

    protected void updateResult(GenericFormatImportResult result) {

        getModel().setImportResult(result);

        if (result != null) {

            Icon icon;
            String text;
            String tip;
            if (result.isValid()) {

                icon = SwingUtil.createActionIcon("accept");
                text = t("tutti.genericFormat.import.success");
                tip = t("tutti.genericFormat.import.success.tip");

            } else {

                icon = SwingUtil.createActionIcon("cancel");
                text = t("tutti.genericFormat.import.error");
                tip = t("tutti.genericFormat.import.error.tip");

            }

            JLabel resultText = getUI().getImportResultText();
            resultText.setIcon(icon);
            resultText.setText(text);
            resultText.setToolTipText(tip);

        }

    }

    protected boolean acceptOverrideData() {

        boolean doAction = true;

        if (getModel().isSelectedDataExists()) {

            // Ask user to confirm to override data
            String htmlMessage = String.format(
                    AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.genericformat.overrideData.conflict.message"),
                    t("tutti.genericformat.overrideData.conflict.help"));
            Component ui = getDialogParentComponent();
            int i = JOptionPane.showConfirmDialog(
                    ui,
                    htmlMessage,
                    t("tutti.genericformat.overrideData.conflict.title"),
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);

            doAction = i == JOptionPane.OK_OPTION;
        }
        return doAction;

    }

    protected boolean acceptProtocol() {

        GenericFormatValidateFileResult validateResult = getModel().getValidateResult();

        boolean withProtocolToImport = validateResult.getProtocolFileResult().isImported();

        boolean doAction = true;
        if (withProtocolToImport) {

            List<String> allProtocolNames = getContext().getPersistenceService().getAllProtocolNames();

            String protocolOriginalName = validateResult.getProtocolOriginalName();

            if (allProtocolNames.contains(protocolOriginalName)) {

                // Ask user what to do ?
                // - generate a new name
                // - delete previous protocol
                // - Cancel

                String cancel = t("tutti.genericformat.protocol.action.cancel");
                String override = t("tutti.genericformat.protocol.action.override");
                String add = t("tutti.genericformat.protocol.action.add");

                String htmlMessage = String.format(
                        AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                        t("tutti.genericformat.protocolName.conflict.message", protocolOriginalName),
                        t("tutti.genericformat.protocolName.conflict.help", validateResult.getProtocol().getName()));
                Component ui = getDialogParentComponent();
                int response = JOptionPane.showOptionDialog(
                        ui,
                        htmlMessage,
                        t("tutti.genericformat.protocolName.conflict.title"),
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        new String[]{add, override, cancel},
                        add);

                switch (response) {
                    case 2:
                        // cancel
                        doAction = false;
                        break;
                    case 1:
                        // override
                        doAction = true;
                        overrideProtocol = true;
                        break;
                    case 0:
                        // add
                        doAction = true;
                        break;
                }
            }
        }
        return doAction;
    }
}