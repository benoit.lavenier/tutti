/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#dialog {
  title: {handler.getTile(getWeightUnit())};
}

#message {
  text: {handler.getMessage(getWeightUnit())};
  horizontalAlignment: {JLabel.CENTER};
}

#editor {
  numberType: {Float.class};
  useSign: false;
  autoPopup: false;
  showPopupButton: true;
  showReset: true;
  numberPattern: {getWeightUnit().getNumberEditorPattern()};
}

#cancelButton {
  actionIcon: cancel;
  text: "tutti.createSpeciesMelag.action.cancel";
  toolTipText: "tutti.createSpeciesMelag.action.cancel.tip";
  i18nMnemonic: "tutti.createSpeciesMelag.action.cancel.mnemonic";
  _simpleAction: {fr.ifremer.tutti.ui.swing.util.species.actions.EnterMelagWeightCancelAction.class};
}

#validateButton {
  actionIcon: accept;
  text: "tutti.createSpeciesMelag.action.validate";
  toolTipText: "tutti.createSpeciesMelag.action.validate.tip";
  i18nMnemonic: "tutti.createSpeciesMelag.action.validate.mnemonic";
  enabled: {editor.getModel() != null};
  _simpleAction: {fr.ifremer.tutti.ui.swing.util.species.actions.EnterMelagWeightValidateAction.class};
}
