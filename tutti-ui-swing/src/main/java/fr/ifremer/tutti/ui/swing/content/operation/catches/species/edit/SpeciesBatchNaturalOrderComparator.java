package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * To compare some rows on their natural order. Means the order the data were stored.
 *
 * For this we use the {@link SpeciesBatch#getRankOrder()} for the parents and keep using the
 * rowIndex for the children.
 *
 * Created on 8/30/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class SpeciesBatchNaturalOrderComparator<R extends SpeciesBatch> implements Comparator<R>, Serializable {

    private static final long serialVersionUID = 1L;

    public static <R extends SpeciesBatch> void sort(List<R> data) {

        SpeciesBatchNaturalOrderComparator<R> comparator = new SpeciesBatchNaturalOrderComparator<>(data);
        Collections.sort(data, comparator);

    }

    private final Map<R, Integer> data;

    public SpeciesBatchNaturalOrderComparator(List<R> data) {

        this.data = new HashMap<>();
        int index = 0;
        for (R r : data) {
            this.data.put(r, index++);
        }

    }

    @Override
    public int compare(R o1, R o2) {

        SpeciesBatch o1ParentBatch = getRootBatch(o1);
        SpeciesBatch o2ParentBatch = getRootBatch(o2);

        int result = o1ParentBatch.getRankOrder() - o2ParentBatch.getRankOrder();

        if (result == 0) {

            // on same root batch, compare then their rowIndex
            int o1Index = data.get(o1);
            int o2Index = data.get(o2);

            result = o1Index - o2Index;

        }

        return result;
    }

    protected SpeciesBatch getRootBatch(SpeciesBatch o) {

        SpeciesBatch root;

        if (o.getParentBatch() == null) {

            root = o;

        } else {

            root = getRootBatch(o.getParentBatch());

        }

        return root;

    }
}
