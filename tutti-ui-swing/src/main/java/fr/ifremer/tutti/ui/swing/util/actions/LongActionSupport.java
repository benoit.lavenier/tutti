package fr.ifremer.tutti.ui.swing.util.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.species.SelectSpeciesUI;
import fr.ifremer.tutti.ui.swing.util.species.SelectSpeciesUIModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;
import org.nuiton.util.StringUtil;

import javax.swing.JOptionPane;
import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

/**
 * Tutti base action.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public abstract class LongActionSupport<M extends AbstractBean, UI extends TuttiUI<M, ?>, H extends AbstractTuttiUIHandler<M, UI>>
        extends AbstractApplicationAction<M, UI, H> {

    public abstract void doAction() throws Exception;

    protected LongActionSupport(H handler, boolean hideBody) {
        super(handler, hideBody);
    }

    @Override
    public TuttiUIContext getContext() {
        return handler.getContext();
    }

    public void setProgressionModel(ProgressionModel progressionModel) {
        super.setProgressionModel(progressionModel);
    }

    @Override
    protected ProgressionModel getProgressionModel() {
        return (ProgressionModel) getContext().getActionUI().getModel().getProgressionModel();
    }

    public TuttiDataContext getDataContext() {
        return getContext().getDataContext();
    }

    @Override
    protected TuttiConfiguration getConfig() {
        return getContext().getConfig();
    }

    @Override
    protected void sendMessage(String message) {
        getContext().showInformationMessage(message);
    }

    @Override
    protected void createProgressionModelIfRequired(int total) {
        ProgressionModel progressionModel = getProgressionModel();
        if (progressionModel == null) {
            progressionModel = new ProgressionModel();
            progressionModel.setMessage("");
            progressionModel.setCurrent(0);
            setProgressionModel(progressionModel);
            progressionModel.setTotal(total);

        } else {
            progressionModel.adaptTotal(total);
        }
    }

    protected Species openAddSpeciesDialog(String title, List<Species> species) {
        return openAddSpeciesDialog(title, species, null);
    }

    protected Species openAddSpeciesDialog(String title, List<Species> species, List<Species> filteredSpecies) {
        SelectSpeciesUI dialogContent = new SelectSpeciesUI(true, getUI());
        SelectSpeciesUIModel model = dialogContent.getModel();
        model.setSelectedSpecies(null);
        model.setSpecies(species);
        model.setFilteredSpecies(filteredSpecies);
        model.setShowAllSpecies(CollectionUtils.isEmpty(filteredSpecies));

        getHandler().openDialog(dialogContent, title, new Dimension(400, 130));

        return model.getSelectedSpecies();
    }


    protected boolean askAdminPassword(String askMessage,
                                       String askMessageTitle,
                                       String errorMessage,
                                       String errorMessageTitle) {

        Component container = getContext().getMainUI();

        String answer;
        boolean result;
        do {
            answer = JOptionPane.showInputDialog(container,
                                                 askMessage,
                                                 askMessageTitle,
                                                 JOptionPane.WARNING_MESSAGE);
            if (answer != null) {
                String cryptedAnswer = StringUtil.encodeMD5(answer);
                String correctAnswer = getConfig().getAdminPassword();
                result = StringUtils.equals(cryptedAnswer, correctAnswer);

                if (!result) {
                    JOptionPane.showMessageDialog(container,
                                                  errorMessage,
                                                  errorMessageTitle,
                                                  JOptionPane.ERROR_MESSAGE);
                }

            } else {
                result = false;
            }

        } while (!result && answer != null);
        return result;
    }
}
