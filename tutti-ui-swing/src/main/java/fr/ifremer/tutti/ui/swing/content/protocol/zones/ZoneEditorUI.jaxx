<!--
  #%L
  Tutti :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2012 - 2016 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<Table id='editProtocolTopPanel'
       fill="both"
       implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;EditProtocolUIModel, ZoneEditorUIHandler&gt;'>

  <import>
    fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.ZoneEditorTreeCellRenderer
    fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel
    fr.ifremer.tutti.ui.swing.util.TuttiUI
    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil
    jaxx.runtime.SwingUtil
  </import>

  <script><![CDATA[

      public ZoneEditorUI(TuttiUI parentUI) {
          TuttiUIUtil.setParentUI(this, parentUI);
      }
  ]]>
  </script>

  <!-- model -->
  <EditProtocolUIModel id='model' javaBean='getContextValue(EditProtocolUIModel.class)'/>

  <BeanValidator id='validator' bean='model'
                 uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <field name='availableStratas' component='editProtocolTopPanel'/>
  </BeanValidator>

  <JPopupMenu id='zonePopupMenu'>
    <JMenuItem id='deleteZoneMenuItem'/>
    <JMenuItem id='renameZoneMenuItem'/>
  </JPopupMenu>

  <row>
    <cell>
      <JToolBar id="availableStratasTreeToolBar">
        <JButton id="expandAvailableStratasTree"/>
        <JButton id="collapseAvailableStratasTree"/>
      </JToolBar>
    </cell>
    <cell>
      <JLabel/>
    </cell>
    <cell>
      <JToolBar id="zonesTreeToolBar">
        <JButton id="createZone"/>
        <JButton id="expandZonesTree"/>
        <JButton id="collapseZonesTree"/>
      </JToolBar>
    </cell>
  </row>
  <row>

    <cell weightx='0.5' weighty='1' fill='both'>
      <JScrollPane onFocusGained='availableStratasTree.requestFocus()'>
        <!-- List of the available stratas and substratas -->
        <JTree id='availableStratasTree'
               onValueChanged="availableStratasTree.expandPath(event.getNewLeadSelectionPath())"
               onMouseClicked="handler.onMouseClickedOnAvailableStratas(event)"
               onKeyPressed="handler.onKeyPressedOnAvailableStratas(event)"/>
        <!--onFocusGained='handler.selectFirstRowIfNoSelection(event)'-->
        <!--onMouseClicked='handler.onSelectedListClicked(event)'-->
        <!--onKeyPressed='handler.onKeyPressedOnSelectedList(event)'/>-->
      </JScrollPane>
    </cell>

    <cell anchor='north'>
      <JPanel layout='{new GridLayout(0,1)}'>
        <JButton id='addButton'/>
        <JButton id='removeButton'/>
      </JPanel>
    </cell>

    <cell weightx='0.5' weighty='1' fill='both'>
      <JScrollPane onFocusGained='zonesTree.requestFocus()'>
        <!-- List of the zones -->
        <JTree id='zonesTree' onMouseClicked="handler.onMouseClickedPressedOnZones(event, zonePopupMenu)"
               onKeyPressed="handler.onKeyPressedOnZones(event)"/>
        <!--onFocusGained='handler.selectFirstRowIfNoSelection(event)'-->
        <!--onMouseClicked='handler.onUniverseListClicked(event)'-->
        <!--onKeyPressed='handler.onKeyPressedOnUniverseList(event)'-->
      </JScrollPane>
    </cell>

  </row>

</Table>