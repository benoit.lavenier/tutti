package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicBean;
import fr.ifremer.tutti.service.cruise.CruiseCache;
import fr.ifremer.tutti.service.sampling.IndividualObservationSamplingCacheRequest;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;
import fr.ifremer.tutti.util.Numbers;
import fr.ifremer.tutti.util.Weights;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class SpeciesFrequencyUIModel extends AbstractTuttiTableUIModel<SpeciesBatchRowModel, SpeciesFrequencyRowModel, SpeciesFrequencyUIModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesFrequencyUIModel.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_BATCH = "batch";

    public static final String PROPERTY_CONFIGURATION_MODE = "configurationMode";

    public static final String PROPERTY_FREQUENCIES_CONFIGURATION_MODE = "frequenciesConfigurationMode";

    public static final String PROPERTY_STEP = "step";

    private static final String PROPERTY_MIN_STEP = "minStep";

    private static final String PROPERTY_MAX_STEP = "maxStep";

    public static final String PROPERTY_CAN_GENERATE = "canGenerate";

    public static final String PROPERTY_FREQUENCIES_MODE = "frequenciesMode";

    public static final String PROPERTY_AUTO_GEN_MODE = "autoGenMode";

    public static final String PROPERTY_RAFALE_MODE = "rafaleMode";

    public static final String PROPERTY_SIMPLE_COUNTING_MODE = "simpleCountingMode";

    public static final String PROPERTY_SIMPLE_COUNT = "simpleCount";

    public static final String PROPERTY_LENGTH_STEP_CARACTERISTIC = "lengthStepCaracteristic";

    public static final String PROPERTY_LENGTH_STEP_CARACTERISTIC_UNIT = "lengthStepCaracteristicUnit";

    public static final String PROPERTY_TOTAL_NUMBER = "totalNumber";

    public static final String PROPERTY_TOTAL_WEIGHT = "totalWeight";

    public static final String PROPERTY_TOTAL_COMPUTED_WEIGHT = "totalComputedWeight";

    public static final String PROPERTY_COPY_RTP_WEIGHTS = "copyRtpWeights";

    public static final String PROPERTY_RTP = "rtp";

    public static final String PROPERTY_ADD_INDIVIDUAL_OBSERVATION_ON_RAFALE = "addIndividualObservationOnRafale";

    public static final String PROPERTY_EMPTY_ROWS = "emptyRows";

    public static final String PROPERTY_NEXT_EDITABLE_ROW_INDEX = "nextEditableRowIndex";

    public static final String PROPERTY_CAN_EDIT_LENGTH_STEP = "canEditLengthStep";

    public static final String PROPERTY_COPY_INDIVIDUAL_OBSERVATION_MODE = "copyIndividualObservationMode";

    public static final String PROPERTY_COPY_INDIVIDUAL_OBSERVATION_ALL = "copyIndividualObservationAll";

    public static final String PROPERTY_COPY_INDIVIDUAL_OBSERVATION_NOTHING = "copyIndividualObservationNothing";

    public static final String PROPERTY_COPY_INDIVIDUAL_OBSERVATION_SIZE = "copyIndividualObservationSize";

    public static final String PROPERTY_INIT_BATCH_EDITION = "initBatchEdition";

    public static final String PROPERTY_NON_EMPTY_INDIVIDUAL_OBSERVATION_ROWS_IN_ERROR = "nonEmptyIndividualObservationRowsInError";

    private final SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    /**
     * Fill mode.
     *
     * @since 0.2
     */
    private FrequencyConfigurationMode configurationMode;

    /**
     * Fill mode.
     *
     * @since 4.5
     */
    private FrequencyConfigurationMode frequenciesConfigurationMode;

    /**
     * Fishing operation of the species batch.
     *
     * @since 4.5
     */
    private FishingOperation fishingOperation;

    /**
     * Batch that contains frequencies.
     *
     * @since 0.2
     */
    private SpeciesBatchRowModel batch;

    /**
     * Default step to addIndividualObservation length step.
     *
     * @since 0.2
     */
    private Float step;

    /**
     * Min step to auto generate length steps.
     *
     * @since 0.2
     */
    private Float minStep;

    /**
     * Max step to auto generate length steps.
     *
     * @since 0.2
     */
    private Float maxStep;

    /**
     * Length step caracteristic.
     *
     * @since 0.3
     */
    private Caracteristic lengthStepCaracteristic;

    /**
     * Sum of the number of each valid row
     *
     * @since 2.3
     */
    private Integer totalNumber;

    /**
     * Sum of the weight of each valid row, or sample weight
     *
     * @since 3.8
     */
    private ComputableData<Float> totalComputedOrNotWeight;

    /**
     * Rtp of the species batch
     *
     * @since 4.5
     */
    private Rtp rtp;

    /**
     * copy the weigths computed with the RTPs of the protocol
     *
     * @since 4.5
     */
    private boolean copyRtpWeights;

    /**
     * Add individual observation rows when a lengthstep is added with the rafale mode
     *
     * @since 4.5
     */
    private boolean addIndividualObservationOnRafale;

    /**
     * Number in case of simple counting mode
     *
     * @since 1.0
     */
    private Integer simpleCount;

    /**
     * The index of the next editable row (null if none).
     *
     * @since 2.5
     */
    private Integer nextEditableRowIndex;

    private Set<SpeciesFrequencyRowModel> emptyRows;

    /**
     * Sample categories model.
     *
     * @since 2.4
     */
    private final SampleCategoryModel sampleCategoryModel;

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    private final WeightUnit weightUnit;

    private final WeightUnit individualObservationWeightUnit;

    /**
     * To store all caches used by the screen
     *
     * @since 3.11
     */
    private final SpeciesFrequencyUIModelCache cache;

    /**
     * Can edit length step? (only if no row is filled).
     * see https://forge.codelutin.com/issues/5694
     *
     * @since 3.11
     */
    private boolean canEditLengthStep;

    /**
     * What to copy from the individual observations to the data table
     *
     * @since 4.5
     */
    private CopyIndividualObservationMode copyIndividualObservationMode;

    /**
     * Is the model is loading?
     */
    private boolean initBatchEdition;

    private final AverageWeightsHistogramModel averageWeightsHistogramModel;
    private final FrequenciesHistogramModel frequenciesHistogramModel;
    private final IndividualObservationBatchUIModel individualObservationModel;

    private final IndividualObservationUICache individualObservationUICache;
    private final SamplingCodeUICache samplingCodeUICache;
    private SpeciesFrequencyTableModel frequencyTableModel;

    private final boolean protocolFilled;
    private final boolean protocolUseCalcifiedPieceSampling;

    public SpeciesFrequencyUIModel(SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport,
                                   WeightUnit individualObservationWeightUnit,
                                   SampleCategoryModel sampleCategoryModel,
                                   Caracteristic sexCaracteristic,
                                   List<Caracteristic> protocolIndividualObservationCaracteristics,
                                   CruiseCache cruiseCache,
                                   Integer cruiseId,
                                   TuttiProtocol protocol) {
        super(SpeciesBatchRowModel.class, null, null);
        this.protocolFilled = protocol != null;
        this.protocolUseCalcifiedPieceSampling = protocolFilled && protocol.isUseCalcifiedPieceSampling();
        this.speciesOrBenthosBatchUISupport = speciesOrBenthosBatchUISupport;
        this.weightUnit = speciesOrBenthosBatchUISupport.getWeightUnit();
        this.individualObservationWeightUnit = individualObservationWeightUnit;
        this.sampleCategoryModel = sampleCategoryModel;
        this.totalComputedOrNotWeight = new ComputableData<>();
        this.totalComputedOrNotWeight.addPropagateListener(PROPERTY_TOTAL_WEIGHT, this);
        this.copyIndividualObservationMode = CopyIndividualObservationMode.NOTHING;
        this.canEditLengthStep = true;
        this.cache = new SpeciesFrequencyUIModelCache();
        this.frequenciesConfigurationMode = FrequencyConfigurationMode.AUTO_GEN;
        setEmptyRows(new HashSet<>());

        this.individualObservationModel = new IndividualObservationBatchUIModel(this, sexCaracteristic, protocolIndividualObservationCaracteristics);
        this.averageWeightsHistogramModel = new AverageWeightsHistogramModel(weightUnit);
        this.frequenciesHistogramModel = new FrequenciesHistogramModel();

        this.individualObservationUICache = new IndividualObservationUICache(cruiseCache, this);
        this.samplingCodeUICache = new SamplingCodeUICache(cruiseCache.getSamplingCodeCache(), this, cruiseId);

    }

    public SpeciesFrequencyUIModelCache getCache() {
        return cache;
    }

    public SamplingCodeUICache getSamplingCodeUICache() {
        return samplingCodeUICache;
    }

    public IndividualObservationUICache getIndividualObservationUICache() {
        return individualObservationUICache;
    }

    public AverageWeightsHistogramModel getAverageWeightsHistogramModel() {
        return averageWeightsHistogramModel;
    }

    public FrequenciesHistogramModel getFrequenciesHistogramModel() {
        return frequenciesHistogramModel;
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }

    public boolean isInitBatchEdition() {
        return initBatchEdition;
    }

    public void setInitBatchEdition(boolean initBatchEdition) {
        Object oldValue = isInitBatchEdition();
        this.initBatchEdition = initBatchEdition;
        firePropertyChange(PROPERTY_INIT_BATCH_EDITION, oldValue, initBatchEdition);
    }

    public void reloadRows() {

        setEmptyRows(new HashSet<>());

        cache.loadCache(rows);

        recomputeRowsValidateState();

        //FIXME Je préfèrerais ne pas pusher, ...
        frequenciesHistogramModel.reloadRows(rows);
        averageWeightsHistogramModel.reloadRows(rows);

        recomputeTotalNumber();
        recomputeTotalWeight();

    }

    public boolean isRowValid(SpeciesFrequencyRowModel row) {

        // lengthStepCaracteristic conditions : not null
        boolean valid = row.getLengthStepCaracteristic() != null;

        if (valid) {

            // lengthStep conditions : not null and positive + not found in more than one row
            Float lengthStep = row.getLengthStep();
            valid = lengthStep != null
                    && lengthStep > 0
                    && cache.numberOfRows(lengthStep) < 2;
        }

        if (valid) {

            // number conditions : number filled
            valid = row.withNumber();

        }

        if (valid) {

            // weight conditions : with no weight, or weight filled
            valid = getNbRowsWithWeight() == 0 || row.withWeight();

        }

        return valid;
    }

    @Override
    protected SpeciesBatchRowModel newEntity() {
        return new SpeciesBatchRowModel(weightUnit, sampleCategoryModel);
    }

    public FrequencyConfigurationMode getConfigurationMode() {
        return configurationMode;
    }

    public void setConfigurationMode(FrequencyConfigurationMode configurationMode) {
        Object oldValue = getConfigurationMode();
        this.configurationMode = configurationMode;
        firePropertyChange(PROPERTY_FREQUENCIES_MODE, null, isFrequenciesMode());
        firePropertyChange(PROPERTY_SIMPLE_COUNTING_MODE, null, isSimpleCountingMode());
        firePropertyChange(PROPERTY_CONFIGURATION_MODE, oldValue, configurationMode);
    }

    public FrequencyConfigurationMode getFrequenciesConfigurationMode() {
        return frequenciesConfigurationMode;
    }

    public void setFrequenciesConfigurationMode(FrequencyConfigurationMode frequenciesConfigurationMode) {
        Object oldValue = getFrequenciesConfigurationMode();
        this.frequenciesConfigurationMode = frequenciesConfigurationMode;
        firePropertyChange(PROPERTY_AUTO_GEN_MODE, null, isAutoGenMode());
        firePropertyChange(PROPERTY_RAFALE_MODE, null, isRafaleMode());
        firePropertyChange(PROPERTY_FREQUENCIES_CONFIGURATION_MODE, oldValue, getFrequenciesConfigurationMode());
    }

    public Float getStep() {
        return step;
    }

    public void setStep(Caracteristic caracteristic) {
        Float step = null;
        if (caracteristic != null) {
            step = caracteristic.getPrecision();
        }
        if (step == null) {
            // on ne met pas 1 c'est la valeur par défaut
            step = CaracteristicBean.DEFAULT_PRECISION;
        }
        setStep(step);
    }

    public void setStep(Float step) {
        Object oldValue = getStep();
        this.step = step;
        firePropertyChange(PROPERTY_STEP, oldValue, step);
    }

    public Caracteristic getLengthStepCaracteristic() {
        return lengthStepCaracteristic;
    }

    public void setLengthStepCaracteristic(Caracteristic lengthStepCaracteristic) {
        Object oldValue = getLengthStepCaracteristic();
        this.lengthStepCaracteristic = lengthStepCaracteristic;
        firePropertyChange(PROPERTY_LENGTH_STEP_CARACTERISTIC, oldValue, lengthStepCaracteristic);
        firePropertyChange(PROPERTY_CAN_GENERATE, null, isCanGenerate());
        firePropertyChange(PROPERTY_LENGTH_STEP_CARACTERISTIC_UNIT, null, getLengthStepCaracteristicUnit());
    }

    public String getLengthStepCaracteristicUnit() {
        return lengthStepCaracteristic == null ? null : lengthStepCaracteristic.getUnit();
    }

    public float convertFromMm(float source) {
        return Numbers.convertFromMm(source, getLengthStepCaracteristicUnit());
    }

    public Float getMinStep() {
        return minStep;
    }

    public void setMinStep(Float minStep) {
        Object oldValue = getMinStep();
        this.minStep = minStep;
        firePropertyChange(PROPERTY_MIN_STEP, oldValue, minStep);
        firePropertyChange(PROPERTY_CAN_GENERATE, null, isCanGenerate());
    }

    public Float getMaxStep() {
        return maxStep;
    }

    public void setMaxStep(Float maxStep) {
        Object oldValue = getMaxStep();
        this.maxStep = maxStep;
        firePropertyChange(PROPERTY_MAX_STEP, oldValue, maxStep);
        firePropertyChange(PROPERTY_CAN_GENERATE, null, isCanGenerate());
    }

    public Integer getSimpleCount() {
        return simpleCount;
    }

    public void setSimpleCount(Integer simpleCount) {
        Object oldValue = getSimpleCount();
        this.simpleCount = simpleCount;
        firePropertyChange(PROPERTY_SIMPLE_COUNT, oldValue, simpleCount);
    }

    public boolean isCanEditLengthStep() {
        return canEditLengthStep;
    }

    public void setCanEditLengthStep(boolean canEditLengthStep) {
        Object oldValue = isCanEditLengthStep();
        this.canEditLengthStep = canEditLengthStep;
        firePropertyChange(PROPERTY_CAN_EDIT_LENGTH_STEP, oldValue, canEditLengthStep);
    }

    public Integer getNextEditableRowIndex() {
        return nextEditableRowIndex;
    }

    public void setNextEditableRowIndex(Integer nextEditableRowIndex) {
        Object oldValue = getNextEditableRowIndex();
        this.nextEditableRowIndex = nextEditableRowIndex;
        firePropertyChange(PROPERTY_NEXT_EDITABLE_ROW_INDEX, oldValue, nextEditableRowIndex);
    }

    public boolean isSimpleCountingMode() {
        return FrequencyConfigurationMode.SIMPLE_COUNTING == configurationMode;
    }

    public boolean isFrequenciesMode() {
        return FrequencyConfigurationMode.FREQUENCIES == configurationMode;
    }

    public boolean isAutoGenMode() {
        return FrequencyConfigurationMode.AUTO_GEN == frequenciesConfigurationMode;
    }

    public boolean isRafaleMode() {
        return FrequencyConfigurationMode.RAFALE == frequenciesConfigurationMode;
    }

    public boolean isCanGenerate() {
        return minStep != null && maxStep != null && maxStep > minStep && lengthStepCaracteristic != null;
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        this.fishingOperation = fishingOperation;
    }

    public SpeciesBatchRowModel getBatch() {
        return batch;
    }

    public void setBatch(SpeciesBatchRowModel batch) {
        this.batch = batch;
        firePropertyChange(PROPERTY_BATCH, null, batch);
    }

    public float getLengthStep(float lengthStep) {
        int intValue = (int) (lengthStep * 10);
        int intStep = (int) (step * 10);
        int correctIntStep = intValue - (intValue % intStep);
        return correctIntStep / 10f;
    }

    // FIXME poussin 20160608 ce code est duplique avec le CaracteristicBean#getLengthStepInMm
    // Il faudrait supprimer le code qui est ici pour utiliser celui de l'objet.
    //
    public Integer getLengthStepInMm(Float lengthStep) {
        Integer lengthClass;
        
        Caracteristic c = getLengthStepCaracteristic();
        if (c != null && c.getPrecision().equals(step)) {
            lengthClass = c.getLengthStepInMm(lengthStep);
        } else {
            // legacy code, here because step and unit can be set independantly :(
            // this is very strange, and must be refactored (step and unit must
            // be all time associate via Caracteristic object)
            if (lengthStep == null) {
                lengthClass = null;
            } else {
                int intValue = (int) (lengthStep * 10);
                int intStep = (int) (step * 10);
                int correctIntStep = intValue - (intValue % intStep);
                lengthClass = Numbers.convertToMm(correctIntStep / 10f, getLengthStepCaracteristicUnit());
            }
        }
        return lengthClass;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        Object oldValue = getTotalNumber();
        this.totalNumber = totalNumber;
        firePropertyChange(PROPERTY_TOTAL_NUMBER, oldValue, totalNumber);
    }

    public ComputableData<Float> getTotalComputedOrNotWeight() {
        return totalComputedOrNotWeight;
    }

    public Float getTotalWeight() {
        return totalComputedOrNotWeight.getData();
    }

    public void setTotalWeight(Float totalWeight) {
        Object oldValue = getTotalWeight();
        this.totalComputedOrNotWeight.setData(weightUnit.round(totalWeight));
        firePropertyChange(PROPERTY_TOTAL_WEIGHT, oldValue, totalWeight);
    }

    public Float getTotalComputedWeight() {
        return totalComputedOrNotWeight.getComputedData();
    }

    public void setTotalComputedWeight(Float totalComputedWeight) {
        Object oldValue = getTotalComputedWeight();
        this.totalComputedOrNotWeight.setComputedData(weightUnit.round(totalComputedWeight));
        firePropertyChange(PROPERTY_TOTAL_COMPUTED_WEIGHT, oldValue, totalComputedWeight);
    }

    // Utilisé dans un validateur, ne pas supprimer
    public boolean isTotalWeightSameAsComputedWeight() {
        Float totalWeight = getTotalWeight();
        Float totalComputedWeight = getTotalComputedWeight();
        return totalWeight != null && totalComputedWeight != null
                && WeightUnit.KG.isEquals(totalWeight, totalComputedWeight);
    }

    public Rtp getRtp() {
        return rtp;
    }

    public void setRtp(Rtp rtp) {
        Object oldValue = getRtp();
        this.rtp = rtp;
        firePropertyChange(PROPERTY_RTP, oldValue, rtp);
    }

    public boolean isCopyRtpWeights() {
        return copyRtpWeights;
    }

    public void setCopyRtpWeights(boolean copyRtpWeights) {
        boolean oldValue = isCopyRtpWeights();
        try {
            fireVetoableChange(PROPERTY_COPY_RTP_WEIGHTS, oldValue, copyRtpWeights);
            this.copyRtpWeights = copyRtpWeights;
            firePropertyChange(PROPERTY_COPY_RTP_WEIGHTS, oldValue, copyRtpWeights);

        } catch (PropertyVetoException e) {
            if (log.isErrorEnabled()) {
                log.error("error in setting copyRtpWeights", e);
            }
            firePropertyChange(PROPERTY_COPY_RTP_WEIGHTS, copyRtpWeights, oldValue);
        }
    }

    public CopyIndividualObservationMode getCopyIndividualObservationMode() {
        return copyIndividualObservationMode;
    }

    public void setCopyIndividualObservationMode(CopyIndividualObservationMode copyIndividualObservationMode) {
        CopyIndividualObservationMode oldValue = getCopyIndividualObservationMode();
        boolean oldCopyAll = isCopyIndividualObservationAll();
        boolean oldCopyNothing = isCopyIndividualObservationNothing();
        boolean oldCopySize = isCopyIndividualObservationSize();
        this.copyIndividualObservationMode = copyIndividualObservationMode;
        firePropertyChange(PROPERTY_COPY_INDIVIDUAL_OBSERVATION_ALL, oldCopyAll, isCopyIndividualObservationAll());
        firePropertyChange(PROPERTY_COPY_INDIVIDUAL_OBSERVATION_NOTHING, oldCopyNothing, isCopyIndividualObservationNothing());
        firePropertyChange(PROPERTY_COPY_INDIVIDUAL_OBSERVATION_SIZE, oldCopySize, isCopyIndividualObservationSize());
        firePropertyChange(PROPERTY_COPY_INDIVIDUAL_OBSERVATION_MODE, oldValue, getCopyIndividualObservationMode());
    }

    public boolean mustCopyIndividualObservationSize() {
        return isCopyIndividualObservationAll() || isCopyIndividualObservationSize();
    }

    public boolean isCopyIndividualObservationAll() {
        return CopyIndividualObservationMode.ALL == copyIndividualObservationMode;
    }

    public boolean isCopyIndividualObservationNothing() {
        return CopyIndividualObservationMode.NOTHING == copyIndividualObservationMode;
    }

    public boolean isCopyIndividualObservationSize() {
        return CopyIndividualObservationMode.SIZE == copyIndividualObservationMode;
    }

    public Set<SpeciesFrequencyRowModel> getEmptyRows() {
        return emptyRows;
    }

    public void setEmptyRows(Set<SpeciesFrequencyRowModel> emptyRows) {
        this.emptyRows = emptyRows;
        firePropertyChange(PROPERTY_EMPTY_ROWS, null, emptyRows);
    }

    public boolean isAddIndividualObservationOnRafale() {
        return addIndividualObservationOnRafale;
    }

    public void setAddIndividualObservationOnRafale(boolean addIndividualObservationOnRafale) {
        Object oldValue = isAddIndividualObservationOnRafale();
        this.addIndividualObservationOnRafale = addIndividualObservationOnRafale;
        firePropertyChange(PROPERTY_ADD_INDIVIDUAL_OBSERVATION_ON_RAFALE, oldValue, addIndividualObservationOnRafale);
    }

    public int getNbRowsWithWeight() {
        return cache.getNbRowsWithWeight();
    }

    public boolean isSomeRowsWithWeightAndOtherWithout() {

        boolean result;

        if (CollectionUtils.isEmpty(rows)) {

            // no row
            result = false;

        } else {

            // there is some rows

            int nbNoneEmptyRows = 0;
            int nbNoneEmptyRowsWithWeight = 0;

            for (SpeciesFrequencyRowModel row : rows) {

                if (row.isEmpty()) {

                    // no value on rows, no check on it
                    continue;
                }

                nbNoneEmptyRows++;

                if (row.getWeight() != null) {

                    nbNoneEmptyRowsWithWeight++;

                }

            }

            result = nbNoneEmptyRowsWithWeight > 0 && nbNoneEmptyRows != nbNoneEmptyRowsWithWeight;

        }

        return result;

    }

    public void updateEmptyRow(SpeciesFrequencyRowModel row) {
        if (row.isValid() && row.getNumber() == null && row.getWeight() == null) {
            emptyRows.add(row);
        } else {
            emptyRows.remove(row);
        }
        firePropertyChange(PROPERTY_EMPTY_ROWS, null, emptyRows);
    }

    public void recomputeTotalWeight() {

        Float computeTotalWeight = cache.computeTotalWeight();
        setTotalComputedWeight(computeTotalWeight);

    }

    public void recomputeTotalNumber() {

        int computeTotalNumber = 0;
        if (rows != null) {
            for (SpeciesFrequencyRowModel row : rows) {
                if (!row.isValid()) {
                    continue;
                }
                if (row.getNumber() != null) {
                    computeTotalNumber += row.getNumber();
                }
            }
        }
        setTotalNumber(computeTotalNumber);

    }

    public void recomputeCanEditLengthStep() {

        boolean result = frequencyTableModel.recomputeCanEditLengthStep();

        if (result) {
            result = individualObservationModel.recomputeCanEditLengthStep();
        }

        setCanEditLengthStep(result);

    }

    public void computeRowWeightWithRtp() {
        rows.forEach(this::computeRowWeightWithRtp);
    }

    public void computeRowWeightWithRtp(SpeciesFrequencyRowModel row) {

        Float computedWeight = null;

        if (row.withNumber() && row.getLengthStep() != null && withRtp()) {
            // computedWeightForLengthStep in grams
            float computedWeightForLengthStep = Weights.computeWithRtp(getRtp(), row.getLengthStep(), getLengthStepCaracteristicUnit());
            computedWeight = Weights.convert(WeightUnit.G, weightUnit, row.getNumber() * computedWeightForLengthStep);
        }

        row.setRtpComputedWeight(computedWeight);

        if (isCopyRtpWeights() && !isCopyIndividualObservationAll()) {
            row.setWeight(computedWeight);
        }

    }

    public boolean withRtp() {
        return rtp != null;
    }

    public void recomputeRowsValidateState() {

        if (log.isInfoEnabled()) {
            log.info("Revalidate all frequency rows");
        }

        rowsInError.clear();

        rows.forEach(row -> {
            // recompute row valid state
            boolean valid = isRowValid(row);

            // apply it to row
            row.setValid(valid);
            if (!valid) {
                rowsInError.add(row);
            }

        });

        firePropertyChange(PROPERTY_ROWS_IN_ERROR, null, rowsInError);

    }

    protected final void recomputeRowValidState(SpeciesFrequencyRowModel row) {

        // recompute row valid state
        boolean valid = isRowValid(row);

        // apply it to row
        row.setValid(valid);

        if (valid) {
            removeRowInError(row);
        } else {
            addRowInError(row);
        }

    }

    public SpeciesOrBenthosBatchUISupport getSpeciesOrBenthosBatchUISupport() {
        return speciesOrBenthosBatchUISupport;
    }

    public WeightUnit getIndividualObservationWeightUnit() {
        return individualObservationWeightUnit;
    }

    public IndividualObservationBatchUIModel getIndividualObservationModel() {
        return individualObservationModel;
    }

    private boolean nonEmptyIndividualObservationRowsInError;

    // Ne pas supprimer utiliser par la validation
    public boolean isNonEmptyIndividualObservationRowsInError() {
        return nonEmptyIndividualObservationRowsInError;
    }

    // Ne pas supprimer utiliser par la validation
    public void setNonEmptyIndividualObservationRowsInError(boolean nonEmptyIndividualObservationRowsInError) {
        boolean oldValue = isNonEmptyIndividualObservationRowsInError();
        this.nonEmptyIndividualObservationRowsInError = nonEmptyIndividualObservationRowsInError;
        firePropertyChanged(PROPERTY_NON_EMPTY_INDIVIDUAL_OBSERVATION_ROWS_IN_ERROR, oldValue, nonEmptyIndividualObservationRowsInError);
    }

    public void clear() {

        setRows(new ArrayList<>());

        recomputeCanEditLengthStep();

    }

    public IndividualObservationSamplingCacheRequest toSamplingCacheRequest(IndividualObservationBatchRowModel row) {
        return new IndividualObservationSamplingCacheRequest(fishingOperation,
                                                             row.getSpecies(),
                                                             getLengthStepInMm(row.getSize()),
                                                             individualObservationModel.getMaturityValue(row),
                                                             individualObservationModel.getGender(row),
                                                             row.getSamplingCode());
    }

    public void addIndividualObservationsInCache(List<IndividualObservationBatchRowModel> individualObservations) {

        individualObservationUICache.addIndividualObservations(individualObservations);
        samplingCodeUICache.addIndividualObservations(individualObservations);

    }

    public List<IndividualObservationBatchRowModel> getValidIndividualObservations() {

        List<IndividualObservationBatchRowModel> result = new ArrayList<>(individualObservationModel.getRows());
        result.removeAll(individualObservationModel.getRowsInError());
        return result;

    }

    public void setFrequencyTableModel(SpeciesFrequencyTableModel frequencyTableModel) {
        this.frequencyTableModel = frequencyTableModel;
    }

    public SpeciesFrequencyTableModel getFrequencyTableModel() {
        return frequencyTableModel;
    }

    public boolean isProtocolFilled() {
        return protocolFilled;
    }

    public boolean isProtocolUseCalcifiedPieceSampling() {
        return protocolUseCalcifiedPieceSampling;
    }


    public void loadSpeciesBatch(SpeciesBatchRowModel speciesBatch) {

        Integer number = speciesBatch.getNumber();
        setSimpleCount(number);

        setTotalNumber(null);
        setTotalComputedWeight(null);
        setTotalWeight(null);

        setTotalWeight(speciesBatch.getWeight());

    }

    public FrequencyConfigurationMode guessFrequencyConfigurationMode() {

        FrequencyConfigurationMode mode;
        if (getSimpleCount() != null || lengthStepCaracteristic == null) {

            mode = FrequencyConfigurationMode.SIMPLE_COUNTING;

        } else {

            mode = FrequencyConfigurationMode.FREQUENCIES;

        }
        return mode;

    }
}
