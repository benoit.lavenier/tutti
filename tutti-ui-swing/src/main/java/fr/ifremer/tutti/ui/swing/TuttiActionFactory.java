package fr.ifremer.tutti.ui.swing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.JAXXObject;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;
import org.nuiton.jaxx.application.swing.action.ApplicationActionFactory;

import javax.swing.AbstractButton;
import javax.swing.Action;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/24/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class TuttiActionFactory extends ApplicationActionFactory {

    @Override
    public <A extends AbstractApplicationAction> A createLogicAction(AbstractApplicationUIHandler handler,
                                                                     Class<A> actionName) {
        TuttiUIContext context = (TuttiUIContext) handler.getContext();
        if (AbstractMainUITuttiAction.class.isAssignableFrom(actionName) &&
            context.getMainUI() != null) {
            handler = context.getMainUI().getHandler();
        }

        try {
            // create action
            return ConstructorUtils.invokeConstructor(actionName, (AbstractTuttiUIHandler) handler);
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("application.action.create.error", actionName), e);
        }
    }

    public <A extends Action> A createSimpleAction(AbstractApplicationUIHandler handler,
                                                   AbstractButton abstractButton,
                                                   Class<A> actionType) {
        try {

            A action = ConstructorUtils.invokeConstructor(actionType, (JAXXObject) handler.getUI());
            TuttiUIUtil.prepareAction(abstractButton, action, abstractButton.getName());

            return action;

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("jaxx.application.action.create.error", actionType), e);
        }

    }

    public <A extends Action> A createSimpleAction(JAXXObject ui,
                                                   AbstractButton abstractButton,
                                                   Class<A> actionType) {
        try {

            A action = ConstructorUtils.invokeConstructor(actionType,  ui);
            TuttiUIUtil.prepareAction(abstractButton, action, abstractButton.getName());

            return action;

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("jaxx.application.action.create.error", actionType), e);
        }

    }
}
