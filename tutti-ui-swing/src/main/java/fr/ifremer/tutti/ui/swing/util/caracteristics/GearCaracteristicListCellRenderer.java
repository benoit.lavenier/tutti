package fr.ifremer.tutti.ui.swing.util.caracteristics;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import org.apache.commons.collections4.MapUtils;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.decorator.Decorator;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Component;
import java.io.Serializable;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 10/2/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7
 */
public class GearCaracteristicListCellRenderer implements ListCellRenderer {

    private final ListCellRenderer delegate;

    private final Decorator<Caracteristic> decorator;

    private final Decorator<Gear> gearDecorator;

    protected final Map<Gear, String> cache;

    protected final boolean useCache;

    public GearCaracteristicListCellRenderer(ListCellRenderer delegate,
                                             Decorator<Caracteristic> decorator,
                                             Decorator<Gear> gearDecorator, boolean useCache) {
        this.gearDecorator = gearDecorator;
        this.useCache = useCache;
        this.delegate = delegate;
        this.decorator = decorator;
        this.cache = Maps.newHashMap();
    }

    public void clear() {
        cache.clear();
    }

    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object item,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {

        String toolTipText = getToolTipText((Gear) item);

        Component result = delegate.getListCellRendererComponent(list, item, index, isSelected, cellHasFocus);
        ((JComponent) result).setToolTipText(toolTipText);
        return result;
    }

    public String getToolTipText(Gear gear) {
        String toolTipText;

        if (gear == null) {
            toolTipText = "";
        } else {

            if (useCache) {
                if (cache.containsKey(gear)) {

                    toolTipText = cache.get(gear);
                } else {
                    toolTipText = buildTip(gear);
                    cache.put(gear, toolTipText);
                }
            } else {
                toolTipText = buildTip(gear);
            }
        }
        return toolTipText;
    }

    protected String buildTip(Gear gear) {
        String toolTipText;

        ValueFormatter<Serializable> caracteristicValueFormatter = TuttiCsvUtil.CARACTERISTIC_VALUE_FORMATTER;
        StringBuilder sb = new StringBuilder("<html><body>");

        String gearStr = gearDecorator.toString(gear);
        sb.append("<h3>").append(t("tutti.gear.withCaracteristics", gearStr)).append("</h3></hr>");

        if (MapUtils.isNotEmpty(gear.getCaracteristics())) {

            // got some caracteristics

            sb.append("<ul>");
            for (Map.Entry<Caracteristic, Serializable> entry : gear.getCaracteristics().entrySet()) {
                Caracteristic key = entry.getKey();
                String keyStr = decorator.toString(key);
                Serializable value = entry.getValue();
                String valueStr = caracteristicValueFormatter.format(value);
                sb.append("<li>").append(keyStr).append(" :");
                sb.append(valueStr).append("</strong></li>");
            }
            sb.append("</ul>");
        } else {

            sb.append("<i>").append(t("tutti.gear.noCaracteristics")).append("</i>");
        }
        sb.append("</body></html>");

        toolTipText = sb.toString();
        return toolTipText;
    }
}
