package fr.ifremer.tutti.ui.swing.content.genericformat.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.model.DataModelSupport;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public abstract class DataSelectTreeNodeSupport<O extends DataModelSupport> extends DefaultMutableTreeNode {

    private static final long serialVersionUID = 1L;

    public abstract boolean isSelected();

    public abstract void setSelected(boolean selected);

    public abstract O getSelectedDataModel();

    public DataSelectTreeNodeSupport(O userObject) {
        super(userObject);
    }

    public String getId() {
        return getUserObject().getId();
    }

    public String getLabel() {
        return getUserObject().getLabel();
    }

    @Override
    public O getUserObject() {
        return (O) super.getUserObject();
    }

    @Override
    public void setUserObject(Object userObject) {
        if (userObject instanceof Boolean) {
            setSelected((boolean) userObject);
        } else {
            super.setUserObject(userObject);
        }
    }

    public boolean isExist() {
        return getOptionalId() != null;
    }

    public String getOptionalId() {
        return getUserObject().getOptionalId();
    }


}
