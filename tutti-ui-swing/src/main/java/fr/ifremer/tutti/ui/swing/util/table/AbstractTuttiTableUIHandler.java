package fr.ifremer.tutti.ui.swing.util.table;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SampleCategoryComponent.SampleCategoryEditor;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableDataTableCell.TuttiComputedOrNotDataTableCellEditor;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.cell.NumberCellEditor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.MoveToNextEditableCellAction;
import org.nuiton.jaxx.application.swing.table.MoveToNextEditableRowAction;
import org.nuiton.jaxx.application.swing.table.MoveToPreviousEditableCellAction;
import org.nuiton.jaxx.application.swing.table.MoveToPreviousEditableRowAction;

import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @param <R> type of a row
 * @param <M> type of the ui model
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public abstract class AbstractTuttiTableUIHandler<R extends AbstractTuttiBeanUIModel, M extends AbstractTuttiTableUIModel<?, R, M>, UI extends TuttiUI<M, ?>> extends AbstractTuttiUIHandler<M, UI> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractTuttiTableUIHandler.class);

    /**
     * @return the table model handled byt the main table.
     * @since 0.2
     */
    public abstract AbstractApplicationTableModel<R> getTableModel();

    /**
     * @return the main table of the ui.
     * @since 0.2
     */
    public abstract JXTable getTable();

    /**
     * Validates the given row.
     *
     * @param row row to validate
     * @return {@code true} if row is valid, {@code false} otherwise.
     * @since 0.2
     */
    protected abstract boolean isRowValid(R row);

    /**
     * Invoke each time the {@link AbstractTuttiBeanUIModel#modify} state on
     * the current selected row changed.
     *
     * @param rowIndex     row index of the modified row
     * @param row          modified row
     * @param propertyName name of the modified property of the row
     * @param oldValue     old value of the modified property
     * @param newValue     new value of the modified property
     * @since 0.3
     */
    protected void onRowModified(int rowIndex,
                                 R row,
                                 String propertyName,
                                 Object oldValue,
                                 Object newValue) {
        getModel().setModify(true);
    }

    /**
     * Given the row monitor and his monitored row, try to save it if required.
     *
     * Coming in this method, we are sure that row is not null.
     *
     * @param rowMonitor the row monitor (see {@link #rowMonitor})
     * @param row        the row to save if necessary
     * @since 0.3
     */
    protected abstract void saveSelectedRowIfRequired(TuttiBeanMonitor<R> rowMonitor, R row);

    /**
     * Monitor the selected row (save it only if something has changed).
     *
     * @since 0.2
     */
    private final TuttiBeanMonitor<R> rowMonitor;

    protected AbstractTuttiTableUIHandler(String... properties) {

        rowMonitor = new TuttiBeanMonitor<>(properties);

        // listen when bean is changed
        rowMonitor.addPropertyChangeListener(TuttiBeanMonitor.PROPERTY_BEAN, new PropertyChangeListener() {

            final Set<String> propertiesToSkip =
                    Sets.newHashSet(getRowPropertiesToIgnore());

            final PropertyChangeListener l = new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    String propertyName = evt.getPropertyName();

                    R row = (R) evt.getSource();

                    Object oldValue = evt.getOldValue();
                    Object newValue = evt.getNewValue();

                    int rowIndex = getTableModel().getRowIndex(row);

                    if (AbstractTuttiBeanUIModel.PROPERTY_VALID.equals(propertyName)) {
                        onRowValidStateChanged(rowIndex, row,
                                               (Boolean) oldValue,
                                               (Boolean) newValue);
                    } else if (AbstractTuttiBeanUIModel.PROPERTY_MODIFY.equals(propertyName)) {
                        onRowModifyStateChanged(rowIndex, row,
                                                (Boolean) oldValue,
                                                (Boolean) newValue);
                    } else if (!propertiesToSkip.contains(propertyName)) {

                        if (log.isDebugEnabled()) {
                            log.debug("row [" + rowIndex + "] property " +
                                              propertyName + " changed from " + oldValue +
                                              " to " + newValue);
                        }
                        onRowModified(rowIndex, row,
                                      propertyName,
                                      oldValue,
                                      newValue);
                    }
                }
            };

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                R oldValue = (R) evt.getOldValue();
                R newValue = (R) evt.getNewValue();
                if (log.isDebugEnabled()) {
                    log.debug("Monitor row changed from " +
                                      oldValue + " to " + newValue);
                }
                if (oldValue != null) {
                    oldValue.removePropertyChangeListener(l);
                }
                if (newValue != null) {
                    newValue.addPropertyChangeListener(l);
                }
            }
        });
    }

    //------------------------------------------------------------------------//
    //-- Internal methods (row methods)                                     --//
    //------------------------------------------------------------------------//

    protected String[] getRowPropertiesToIgnore() {
        return ArrayUtils.EMPTY_STRING_ARRAY;
    }

    protected void onModelRowsChanged(List<R> rows) {
        if (log.isDebugEnabled()) {
            log.debug("Will set " + (rows == null ? 0 : rows.size()) +
                              " rows on model.");
        }
        if (CollectionUtils.isNotEmpty(rows)) {

            // compute valid state for each row
            for (R row : rows) {
                recomputeRowValidState(row);
            }
        }
        getTableModel().setRows(rows);
    }

    protected void onRowModifyStateChanged(int rowIndex,
                                           R row,
                                           Boolean oldValue,
                                           Boolean newValue) {
        if (log.isDebugEnabled()) {
            log.debug("row [" + rowIndex + "] modify state changed from " +
                              oldValue + " to " + newValue);
        }
    }

    protected void onRowValidStateChanged(int rowIndex,
                                          R row,
                                          Boolean oldValue,
                                          Boolean newValue) {

        if (log.isDebugEnabled()) {
            log.debug("row [" + rowIndex + "] valid state changed from " +
                              oldValue + " to " + newValue);
        }

        if (rowIndex > -1) {
            getTableModel().fireTableRowsUpdated(rowIndex, rowIndex);
        }
    }

    protected void onAfterSelectedRowChanged(int oldRowIndex,
                                             R oldRow,
                                             int newRowIndex,
                                             R newRow) {
        if (log.isDebugEnabled()) {
            log.debug("Selected row changed from [" + oldRowIndex + "] to [" +
                              newRowIndex + "]");
        }
    }

    //------------------------------------------------------------------------//
    //-- Internal methods (init methods)                                    --//
    //------------------------------------------------------------------------//

    protected void initTable(JXTable table) {

        // by default do not authorize to change column orders
        table.getTableHeader().setReorderingAllowed(false);

        addHighlighters(table);

        // when model data change let's propagate it table model
        getModel().addPropertyChangeListener(AbstractTuttiTableUIModel.PROPERTY_ROWS, evt -> onModelRowsChanged((List<R>) evt.getNewValue()));

        // always scroll to selected row
        SwingUtil.scrollToTableSelection(getTable());

        // always force to uninstall listener
        uninstallTableSaveOnRowChangedSelectionListener();

        // save when row chaged and was modified
        installTableSaveOnRowChangedSelectionListener();
    }

    //------------------------------------------------------------------------//
    //-- Internal methods (listener methods)                                --//
    //------------------------------------------------------------------------//

    private ListSelectionListener tableSelectionListener;

    private Map<JTable, KeyAdapter> keyAdapters = new HashMap<>();

    protected void installTableSaveOnRowChangedSelectionListener() {

        Preconditions.checkState(
                tableSelectionListener == null,
                "There is already a tableSelectionListener registred, " +
                        "remove it before invoking this method.");

        // create new listener
        // save when row chaged and was modified

        tableSelectionListener = new ListSelectionListener() {
            /**
             * Current selected row index.
             *
             * @since 0.3
             */
            protected int selectedRowIndex;

            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (log.isDebugEnabled()) {
                    log.debug("Selection changed: " + e);
                }
                // need this for the first modification when no selection,
                // otherwise monitor is set after the first alter, so won't be
                // save directly...

                ListSelectionModel source = (ListSelectionModel) e.getSource();

                int oldRowIndex = selectedRowIndex;
                int newRowIndex = source.getLeadSelectionIndex();

                R oldRow = rowMonitor.getBean();

                if (oldRow == null || oldRowIndex != newRowIndex) {

                    R newRow;

                    if (source.isSelectionEmpty()) {

                        newRow = null;
                    } else {
                        newRow = getTableModel().getEntry(newRowIndex);
                    }

                    if (log.isDebugEnabled()) {
                        log.debug("Will monitor entry: " + newRowIndex);
                    }
                    rowMonitor.setBean(newRow);

                    selectedRowIndex = newRowIndex;

                    onAfterSelectedRowChanged(oldRowIndex,
                                              oldRow,
                                              selectedRowIndex,
                                              rowMonitor.getBean());
                }
            }
        };

        if (log.isDebugEnabled()) {
            log.debug("Intall " + tableSelectionListener + " on tableModel " + getTableModel());
        }

        getTable().getSelectionModel().addListSelectionListener(tableSelectionListener);
    }

    protected void uninstallTableSaveOnRowChangedSelectionListener() {

        if (tableSelectionListener != null) {

            if (log.isDebugEnabled()) {
                log.debug("Desintall " + tableSelectionListener);
            }

            // there was a previous selection listener, remove it
            getTable().getSelectionModel().removeListSelectionListener(tableSelectionListener);
            tableSelectionListener = null;
        }
    }

    protected void installTableKeyListener(TableColumnModel columnModel, final JTable table) {
        installTableKeyListener(columnModel, table, true);
    }

    protected void installTableKeyListener(TableColumnModel columnModel, JTable table, boolean enterToChangeRow) {

        Preconditions.checkState(
                keyAdapters.get(table) == null,
                "There is already a tableSelectionListener registred, " +
                        "remove it before invoking this method.");

        AbstractApplicationTableModel model = (AbstractApplicationTableModel) table.getModel();

        MoveToNextEditableCellAction nextCellAction = MoveToNextEditableCellAction.newAction(model, table);
        MoveToPreviousEditableCellAction previousCellAction = MoveToPreviousEditableCellAction.newAction(model, table);
        MoveToNextEditableRowAction nextRowAction = MoveToNextEditableRowAction.newAction(model, table);
        MoveToPreviousEditableRowAction previousRowAction = MoveToPreviousEditableRowAction.newAction(model, table);

        KeyAdapter keyAdapter = new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                TableCellEditor editor = table.getCellEditor();

                int keyCode = e.getKeyCode();
                boolean shiftDown = e.isShiftDown();

                if (gotoPreviousCell(keyCode, shiftDown)) {

                    consumeAction(e, editor, previousCellAction);

                } else if (gotoNextCell(keyCode)) {
                    consumeAction(e, editor, nextCellAction);

                } else if (gotoPreviousRow(keyCode, shiftDown)) {

                    consumeAction(e, editor, previousRowAction);

                } else if (gotoNextRow(keyCode)) {

                    consumeAction(e, editor, nextRowAction);

                }
            }

            protected void consumeAction(KeyEvent e, TableCellEditor editor, AbstractAction action) {
                e.consume();
                if (editor != null) {
                    editor.stopCellEditing();
                }
                action.actionPerformed(null);
            }

            protected boolean gotoPreviousCell(int keyCode, boolean shiftDown) {
                return keyCode == KeyEvent.VK_LEFT
                        || (keyCode == KeyEvent.VK_TAB && shiftDown)
                        || (!enterToChangeRow && keyCode == KeyEvent.VK_ENTER && shiftDown);
            }

            protected boolean gotoNextCell(int keyCode) {
                return keyCode == KeyEvent.VK_RIGHT
                        || keyCode == KeyEvent.VK_TAB
                        || (!enterToChangeRow && keyCode == KeyEvent.VK_ENTER);
            }

            protected boolean gotoPreviousRow(int keyCode, boolean shiftDown) {
                return keyCode == KeyEvent.VK_UP
                        || (enterToChangeRow && keyCode == KeyEvent.VK_ENTER && shiftDown);
            }

            protected boolean gotoNextRow(int keyCode) {
                return keyCode == KeyEvent.VK_DOWN
                        || (enterToChangeRow && keyCode == KeyEvent.VK_ENTER);
            }

        };
        keyAdapters.put(table, keyAdapter);

        if (log.isDebugEnabled()) {
            log.debug("Intall " + keyAdapter);
        }

        table.addKeyListener(keyAdapter);

        Enumeration<TableColumn> columns = columnModel.getColumns();
        while (columns.hasMoreElements()) {
            TableColumn tableColumn = columns.nextElement();
            TableCellEditor cellEditor = tableColumn.getCellEditor();
            if (cellEditor instanceof NumberCellEditor) {
                NumberCellEditor editor = (NumberCellEditor) cellEditor;
                editor.getNumberEditor().getTextField().addKeyListener(keyAdapter);

            } else if (cellEditor instanceof TuttiComputedOrNotDataTableCellEditor) {
                TuttiComputedOrNotDataTableCellEditor editor =
                        (TuttiComputedOrNotDataTableCellEditor) cellEditor;
                editor.getNumberEditor().getTextField().addKeyListener(keyAdapter);

            } else if (cellEditor instanceof SampleCategoryEditor) {
                SampleCategoryEditor editor = (SampleCategoryEditor) cellEditor;
                editor.getNumberEditor().getTextField().addKeyListener(keyAdapter);
            }
        }
    }

    protected void uninstallTableKeyListener(JTable table) {

        KeyAdapter keyAdapter = keyAdapters.get(table);
        if (keyAdapter != null) {

            if (log.isDebugEnabled()) {
                log.debug("Desintall " + keyAdapter);
            }

            table.removeKeyListener(keyAdapter);

            TableColumnModel columnModel = table.getColumnModel();
            Enumeration<TableColumn> columns = columnModel.getColumns();
            while (columns.hasMoreElements()) {
                TableColumn tableColumn = columns.nextElement();
                TableCellEditor cellEditor = tableColumn.getCellEditor();
                if (cellEditor instanceof NumberCellEditor) {
                    NumberCellEditor editor = (NumberCellEditor) cellEditor;
                    editor.getNumberEditor().getTextField().removeKeyListener(keyAdapter);
                }
            }
            keyAdapters.remove(table);
        }
    }

    protected final void saveSelectedRowIfNeeded() {

        R row = rowMonitor.getBean();

        if (row != null) {
            saveSelectedRowIfRequired(rowMonitor, row);
        }
    }


    protected void cleanrRowMonitor() {
        rowMonitor.clearModified();
    }

    protected final void recomputeRowValidState(R row) {

        // recompute row valid state
        boolean valid = isRowValid(row);

        // apply it to row
        row.setValid(valid);

        if (valid) {
            getModel().removeRowInError(row);
        } else {
            getModel().addRowInError(row);
        }
    }

}
