package fr.ifremer.tutti.ui.swing.content.referential.replace;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class ReplaceTemporaryPersonUIHandler extends AbstractTuttiUIHandler<ReplaceTemporaryPersonUIModel, ReplaceTemporaryPersonUI> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ReplaceTemporaryPersonUIHandler.class);

    @Override
    public void afterInit(ReplaceTemporaryPersonUI ui) {

        initUI(ui);

        ReplaceTemporaryPersonUIModel model = getModel();
        initBeanFilterableComboBox(ui.getSourceListComboBox(), model.getSourceList(), null);
        initBeanFilterableComboBox(ui.getTargetListComboBox(), model.getTargetList(), null);

        SwingValidator validator = ui.getValidator();
        listenValidatorValid(validator, model);

        registerValidators(validator);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getSourceListComboBox();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();
        ui.dispose();
    }

    @Override
    public SwingValidator getValidator() {
        return ui.getValidator();
    }

    public void cancel() {
        getModel().setValid(false);
        onCloseUI();
    }

}