package fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.sampling.SamplingCodePrefix;
import org.jdesktop.beans.AbstractSerializableBean;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class SampleCodeEditionPopupUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_SAMPLE_CODE = "sampleCode";

    public static final String PROPERTY_SAMPLE_CODE_PREFIX = "sampleCodePrefix";

    /**
     * Is the model valid?
     */
    protected boolean valid;

    protected Integer sampleCode;

    protected SamplingCodePrefix sampleCodePrefix;

    public Integer getSampleCode() {
        return sampleCode;
    }

    public void setSampleCode(Integer sampleCode) {
        Object oldValue = getSampleCode();
        this.sampleCode = sampleCode;
        firePropertyChange(PROPERTY_SAMPLE_CODE, oldValue, sampleCode);
    }

    public SamplingCodePrefix getSampleCodePrefix() {
        return sampleCodePrefix;
    }

    public void setSampleCodePrefix(SamplingCodePrefix sampleCodePrefix) {
        Object oldValue = getSampleCodePrefix();
        this.sampleCodePrefix = sampleCodePrefix;
        firePropertyChange(PROPERTY_SAMPLE_CODE_PREFIX, oldValue, sampleCodePrefix);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
