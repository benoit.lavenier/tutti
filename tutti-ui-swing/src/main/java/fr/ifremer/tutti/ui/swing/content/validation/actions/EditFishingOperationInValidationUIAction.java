package fr.ifremer.tutti.ui.swing.content.validation.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.catches.ValidateCruiseOperationsService;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.AbstractTuttiBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.BatchSavedEvent;
import fr.ifremer.tutti.ui.swing.content.operation.catches.BatchSavedListener;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUI;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.nuiton.validator.NuitonValidatorResult;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To edit the given fishing operation.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class EditFishingOperationInValidationUIAction extends LongActionSupport<ValidateCruiseUIModel, ValidateCruiseUI, ValidateCruiseUIHandler> {

    /** Validation service. */
    private final ValidateCruiseOperationsService validationService;

    /**
     * The incoming fishing operation to edit.
     *
     * Can be null (means do not edit any fishing operation), or with no id
     * (means create a ne fishing operation), or with an id (means edit an
     * existing fishing operation).
     */
    protected FishingOperation fishingOperation;

    protected final PropertyChangeListener editFishingOperationModelListener = new PropertyChangeListener() {

        protected final List<String> propertiesToIgnore = Arrays.asList(EditFishingOperationUIModel.PROPERTY_VALID, EditFishingOperationUIModel.PROPERTY_PERSISTED);

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            EditFishingOperationUIModel model = (EditFishingOperationUIModel) evt.getSource();

            if (!model.isLoadingData() && model.getFishingOperation() != null) {

                if (EditFishingOperationUIModel.PROPERTY_MODIFY.equals(evt.getPropertyName())) {

                    if (!model.isModify()) {

                        // after a save, or a reset, reload model cruise, since the synchronizationStatus may have changed
                        Cruise cruise = getDataContext().reloadCruise();
                        getModel().setCruise(cruise);
                    }

                } else if (!propertiesToIgnore.contains(evt.getPropertyName())) {
                    model.convertGearShootingCoordinatesToDD();

                    FishingOperation operation = model.toEntity();
                    NuitonValidatorResult validationResult = validationService.validateCruiseOperation(operation);

                    ValidateCruiseUIModel uiModel = getModel();
                    uiModel.addValidatorResult(uiModel.getSelectedFishingOperation(), validationResult);

                    getHandler().updateCurrentOperationNode(validationResult);
                }
            }
        }
    };

    protected final PropertyChangeListener editCatchesModelListener = new PropertyChangeListener() {

        protected final List<String> propertiesToIgnore = Arrays.asList(
                EditCatchesUIModel.PROPERTY_VALID,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_REJECTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_INERT_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_INERT_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT
        );

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            EditCatchesUIModel model = (EditCatchesUIModel) evt.getSource();

            if (!model.isLoadingData() && model.getFishingOperation() != null) {

                if (EditFishingOperationUIModel.PROPERTY_MODIFY.equals(evt.getPropertyName())) {

                    if (!model.isModify()) {

                        // after a save, or a reset, reload model cruise, since the synchronizationStatus may have changed
                        Cruise cruise = getDataContext().reloadCruise();
                        getModel().setCruise(cruise);
                    }

                } else if (!propertiesToIgnore.contains(evt.getPropertyName())) {
                    CatchBatch catchBatch = model.toEntity();
                    NuitonValidatorResult validationResult = validationService.validateCruiseOperation(catchBatch);

                    ValidateCruiseUIModel uiModel = getModel();
                    uiModel.addValidatorResult(uiModel.getSelectedFishingOperation(), validationResult);

                    getHandler().updateCurrentOperationNode(validationResult);
                }
            }
        }
    };

    protected final BatchSavedListener batchSavedListener = new BatchSavedListener() {

        @Override
        public void onBatchSaved(BatchSavedEvent evt) {

            AbstractTuttiBatchUIModel model = evt.getSource();

            if (model.getFishingOperation() != null) {

                // after a save, or a reset, reload model cruise, since the synchronizationStatus may have changed
                Cruise cruise = getDataContext().reloadCruise();
                getModel().setCruise(cruise);

                NuitonValidatorResult validationResult = validationService.validateCruiseOperation(model.getFishingOperation());

                ValidateCruiseUIModel uiModel = getModel();
                uiModel.addValidatorResult(uiModel.getSelectedFishingOperation(), validationResult);

                getHandler().updateCurrentOperationNode(validationResult);
            }
        }
    };

    public EditFishingOperationInValidationUIAction(ValidateCruiseUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.editFishingOperation.action.editFishingOperation.tip"));
        validationService = getContext().getValidateCruiseOperationsService();
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        this.fishingOperation = fishingOperation;
    }

    @Override
    public boolean prepareAction() throws Exception {

        removeListener();

        return super.prepareAction();
    }

    public void removeListener() {
        FishingOperationsUI operationPanel = getUI().getOperationPanel();
        operationPanel.getFishingOperationTabContent().getModel().removePropertyChangeListener(editFishingOperationModelListener);

        EditCatchesUI catchesTabContent = operationPanel.getCatchesTabContent();

        catchesTabContent.getModel().removePropertyChangeListener(editCatchesModelListener);

        getSpeciesBatchUI().getHandler().removeBatchSavedListener(batchSavedListener);
        getBenthosBatchUI().getHandler().removeBatchSavedListener(batchSavedListener);
        catchesTabContent.getAccidentalTabContent().getHandler().removeBatchSavedListener(batchSavedListener);
        catchesTabContent.getMarineLitterTabContent().getHandler().removeBatchSavedListener(batchSavedListener);
    }

    @Override
    public void doAction() throws Exception {
        getUI().getOperationPanel().getModel().setSelectedFishingOperation(fishingOperation);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        FishingOperationsUI operationPanel = getUI().getOperationPanel();
        operationPanel.getFishingOperationTabContent().getModel().addPropertyChangeListener(editFishingOperationModelListener);

        EditCatchesUI catchesTabContent = operationPanel.getCatchesTabContent();

        catchesTabContent.getModel().addPropertyChangeListener(editCatchesModelListener);

        getSpeciesBatchUI().getHandler().addBatchSavedListener(batchSavedListener);
        getBenthosBatchUI().getHandler().addBatchSavedListener(batchSavedListener);
        catchesTabContent.getAccidentalTabContent().getHandler().addBatchSavedListener(batchSavedListener);
        catchesTabContent.getMarineLitterTabContent().getHandler().addBatchSavedListener(batchSavedListener);
    }

    @Override
    public void releaseAction() {
        super.releaseAction();
        fishingOperation = null;
    }

    private SpeciesBatchUI getBenthosBatchUI() {
        return getUI().getOperationPanel().getCatchesTabContent().getBenthosTabPanel().getEditBatchesUI();
    }

    private SpeciesBatchUI getSpeciesBatchUI() {
        return getUI().getOperationPanel().getCatchesTabContent().getSpeciesTabPanel().getEditBatchesUI();
    }
}
