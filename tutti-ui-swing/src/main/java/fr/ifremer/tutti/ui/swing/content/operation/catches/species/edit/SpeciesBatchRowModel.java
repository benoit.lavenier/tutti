package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyRowModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.bean.JavaBeanObjectUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Represents a species batch (i.e a row in the batch table).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class SpeciesBatchRowModel extends AbstractTuttiBeanUIModel<SpeciesBatch, SpeciesBatchRowModel> implements SpeciesBatch, AttachmentModelAware, SampleCategoryAble<SpeciesBatchRowModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SPECIES_ROW = "speciesRow";

    public static final String PROPERTY_SAMPLE_CATEGORY = "sampleCategory";

    public static final String PROPERTY_SAMPLE_CATEGORY_VALUE = "sampleCategoryValue";

    public static final String PROPERTY_SAMPLE_CATEGORY_WEIGHT = "sampleCategoryWeight";

    public static final String PROPERTY_SAMPLE_CATEGORY_COMPUTED_WEIGHT = "sampleCategoryComputedWeight";

    public static final String PROPERTY_FREQUENCY = "frequency";

    public static final String PROPERTY_INDIVIDUAL_OBSERVATION = "individualObservation";

    public static final String PROPERTY_COMPUTED_NUMBER = "computedOrNotNumber";

    public static final String PROPERTY_COMPUTED_WEIGHT = "computedOrNotWeight";

    public static final String PROPERTY_CHILD_BATCH = "childBatch";

    public static final String PROPERTY_BATCH_LEAF = "batchLeaf";

    public static final String PROPERTY_BATCH_ROOT = "batchRoot";

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final SpeciesBatch editObject = SpeciesBatchs.newSpeciesBatch();

    /**
     * All categories(can not be null).
     *
     * @since 2.4
     */
    protected final SampleCategory<?>[] categories;

    /**
     * Observed weight.
     *
     * @since 0.2
     */
    protected ComputableData<Float> computedOrNotWeight =
            new ComputableData<>();

    /**
     * Total computed number (from frequencies).
     *
     * @since 0.2
     */
    protected ComputableData<Integer> computedOrNotNumber =
            new ComputableData<>();

    /**
     * Attachments (should never be null).
     *
     * @since 0.2
     */
    protected final List<Attachment> attachment = Lists.newArrayList();

    /**
     * List of frequencies observed for this batch.
     *
     * @since 0.2
     */
    protected List<SpeciesFrequencyRowModel> frequency = Lists.newArrayList();

    /**
     * List of individual observations observed for this batch.
     *
     * @since 4.5
     */
    protected List<IndividualObservationBatchRowModel> individualObservation = Lists.newArrayList();

    /**
     * List of child batches (can be null or empty if batch is a leaf).
     *
     * @see #isBatchLeaf()
     * @since 0.3
     */
    protected List<SpeciesBatchRowModel> childBatch;

    /**
     * Sample categories model.
     *
     * @since 2.4
     */
    protected final SampleCategoryModel sampleCategoryModel;

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    protected static final Binder<SpeciesBatch, SpeciesBatchRowModel> fromBeanBinder =
            BinderFactory.newBinder(SpeciesBatch.class,
                                    SpeciesBatchRowModel.class);

    protected static final Binder<SpeciesBatchRowModel, SpeciesBatch> toBeanBinder =
            BinderFactory.newBinder(SpeciesBatchRowModel.class,
                                    SpeciesBatch.class);

    public SpeciesBatchRowModel(WeightUnit weightUnit,
                                SampleCategoryModel sampleCategoryModel) {
        super(fromBeanBinder, toBeanBinder);
        this.weightUnit = weightUnit;
        this.sampleCategoryModel = sampleCategoryModel;
        categories = new SampleCategory[sampleCategoryModel.getNbSampling()];

        for (int i = 0; i < categories.length; i++) {
            SampleCategoryModelEntry entry = sampleCategoryModel.getCategoryByIndex(i);
            SampleCategory<?> category = SampleCategory.newSample(entry);
            categories[i] = category;
        }

        computedOrNotWeight.addPropagateListener(PROPERTY_WEIGHT, this);
        computedOrNotWeight.addPropagateListener(PROPERTY_COMPUTED_WEIGHT, this);
        computedOrNotNumber.addPropagateListener(PROPERTY_NUMBER, this);
        computedOrNotNumber.addPropagateListener(PROPERTY_COMPUTED_NUMBER, this);
    }

    public SpeciesBatchRowModel getSpeciesRow() {
        return this;
    }

    public SpeciesBatchRowModel(WeightUnit weightUnit,
                                WeightUnit observationWeightUnit,
                                SampleCategoryModel sampleCategoryModel,
                                SpeciesBatch aBatch,
                                List<SpeciesBatchFrequency> frequencies,
                                List<IndividualObservationBatch> individualObservations,
                                List<Caracteristic> defautObservationCaracteristic) {
        this(weightUnit, sampleCategoryModel);

        fromEntity(aBatch);

        loadFrequencies(frequencies);

        loadIndividualObservations(observationWeightUnit, defautObservationCaracteristic, individualObservations);

    }

    public void loadFrequencies(List<SpeciesBatchFrequency> frequencies) {

        List<SpeciesFrequencyRowModel> frequencyRows = SpeciesFrequencyRowModel.fromEntity(weightUnit, frequencies);
        frequency.clear();
        frequency.addAll(frequencyRows);
        Collections.sort(frequency);

    }

    public void loadIndividualObservations(WeightUnit observationWeightUnit,
                                           List<Caracteristic> defautObservationCaracteristic,
                                           List<IndividualObservationBatch> individualObservations) {

        List<IndividualObservationBatchRowModel> obsRows =
                IndividualObservationBatchRowModel.fromEntity(observationWeightUnit, defautObservationCaracteristic, individualObservations);
        individualObservation.clear();
        individualObservation.addAll(obsRows);

    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBeanUIModel                                           --//
    //------------------------------------------------------------------------//

    @Override
    protected SpeciesBatch newEntity() {
        return SpeciesBatchs.newSpeciesBatch();
    }

    @Override
    public void fromEntity(SpeciesBatch entity) {
        super.fromEntity(entity);

        // convert weight
        setWeight(weightUnit.fromEntity(getWeight()));
        setComputedWeight(weightUnit.fromEntity(getComputedWeight()));
    }

    @Override
    public SpeciesBatch toEntity() {
        SpeciesBatch result = super.toEntity();

        // convert weight
        result.setWeight(weightUnit.toEntity(getWeight()));

        SampleCategory<?> sampleCategory = getFinestCategory();
        Preconditions.checkNotNull(sampleCategory);
        Preconditions.checkNotNull(sampleCategory.getCategoryId());
        Preconditions.checkNotNull(sampleCategory.getCategoryValue());

        // apply sample category
        result.setSampleCategoryId(sampleCategory.getCategoryId());
        result.setSampleCategoryValue(sampleCategory.getCategoryValue());

        // convert sample category weight
        Float categoryWeight = sampleCategory.getCategoryWeight();
        result.setSampleCategoryWeight(weightUnit.toEntity(categoryWeight));

        return result;
    }

    //------------------------------------------------------------------------//
    //-- SpeciesBatch                                                       --//
    //------------------------------------------------------------------------//

    @Override
    public Species getSpecies() {
        return editObject.getSpecies();
    }

    @Override
    public void setSpecies(Species species) {
        Object oldCategory = getSpecies();
        editObject.setSpecies(species);
        firePropertyChange(PROPERTY_SPECIES, oldCategory, species);
    }

    @Override
    public String getComment() {
        return editObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public boolean isSpeciesToConfirm() {
        return editObject.isSpeciesToConfirm();
    }

    @Override
    public void setSpeciesToConfirm(boolean speciesToConfirm) {
        boolean oldValue = isSpeciesToConfirm();
        editObject.setSpeciesToConfirm(speciesToConfirm);
        firePropertyChange(PROPERTY_SPECIES_TO_CONFIRM, oldValue, speciesToConfirm);
    }

    @Override
    public Integer getComputedNumber() {
        return computedOrNotNumber.getComputedData();
    }

    @Override
    public void setComputedNumber(Integer computedNumber) {
        computedOrNotNumber.setComputedData(computedNumber);
    }

    @Override
    public Float getComputedWeight() {
        return computedOrNotWeight.getComputedData();
    }

    @Override
    public void setComputedWeight(Float computedWeight) {
        computedOrNotWeight.setComputedData(computedWeight);
    }

    @Override
    public FishingOperation getFishingOperation() {
        return editObject.getFishingOperation();
    }

    @Override
    public void setFishingOperation(FishingOperation fishingOperation) {
        editObject.setFishingOperation(fishingOperation);
    }

    @Override
    public SpeciesBatchRowModel getParentBatch() {
        return (SpeciesBatchRowModel) editObject.getParentBatch();
    }

    @Override
    public void setParentBatch(SpeciesBatch parentBatch) {
        Object oldValue = getParentBatch();
        editObject.setParentBatch(parentBatch);
        firePropertyChange(PROPERTY_PARENT_BATCH, oldValue, parentBatch);
        firePropertyChange(PROPERTY_BATCH_ROOT, null, isBatchRoot());
    }

    @Override
    public Float getWeight() {
        return computedOrNotWeight.getData();
    }

    @Override
    public void setWeight(Float weight) {
        this.computedOrNotWeight.setData(weight);
    }

    @Override
    public Integer getSampleCategoryId() {
        return null;
    }

    @Override
    public void setSampleCategoryId(Integer sampleCategoryId) {
    }

    @Override
    public Serializable getSampleCategoryValue() {
        return null;
    }

    @Override
    public void setSampleCategoryValue(Serializable sampleCategoryValue) {
    }

    @Override
    public Float getSampleCategoryWeight() {
        return null;
    }

    @Override
    public void setSampleCategoryWeight(Float sampleCategoryWeight) {
    }

    @Override
    public Integer getNumber() {
        return computedOrNotNumber.getData();
    }

    @Override
    public void setNumber(Integer number) {
        computedOrNotNumber.setData(number);
    }

    @Override
    public Float getSampleCategoryComputedWeight() {
        return null;
    }

    @Override
    public void setSampleCategoryComputedWeight(Float sampleCategoryComputedWeight) {
    }

    @Override
    public SpeciesBatch getChildBatchs(int index) {
        return childBatch.get(index);
    }

    @Override
    public boolean isChildBatchsEmpty() {
        return childBatch == null || childBatch.isEmpty();
    }

    @Override
    public int sizeChildBatchs() {
        return childBatch == null ? 0 : childBatch.size();
    }

    @Override
    public void addChildBatchs(SpeciesBatch childBatchs) {
    }

    @Override
    public void addAllChildBatchs(Collection<SpeciesBatch> childBatchs) {
    }

    @Override
    public boolean removeChildBatchs(SpeciesBatch childBatchs) {
        return false;
    }

    @Override
    public boolean removeAllChildBatchs(Collection<SpeciesBatch> childBatchs) {
        return false;
    }

    @Override
    public boolean containsChildBatchs(SpeciesBatch childBatchs) {
        return false;
    }

    @Override
    public boolean containsAllChildBatchs(Collection<SpeciesBatch> childBatchs) {
        return false;
    }

    @Override
    public List<SpeciesBatch> getChildBatchs() {
        return null;
    }

    @Override
    public void setChildBatchs(List childBatchs) {
    }

    @Override
    public Integer getRankOrder() {
        return editObject.getRankOrder();
    }

    @Override
    public void setRankOrder(Integer rankOrder) {
        editObject.setRankOrder(rankOrder);
    }

    @Override
    public boolean isBenthosBatch() {
        return editObject.isBenthosBatch();
    }

    @Override
    public void setBenthosBatch(boolean benthosBatch) {
        //NEVER!
    }

    //------------------------------------------------------------------------//
    //-- SampleCategoryAble                                                 --//
    //------------------------------------------------------------------------//

    @Override
    public Integer getCategoryIndex(Integer id) {
        int result = 0;
        for (SampleCategory<?> category : categories) {
            if (category != null && id.equals(category.getCategoryId())) {
                break;
            } else {
                result++;
            }
        }
        return result;
    }

    @Override
    public void setSampleCategory(SampleCategory sampleCategory) {
        int index = getCategoryIndex(sampleCategory.getCategoryId());
        SampleCategory<?> oldCategory = categories[index];
        Object oldValue = oldCategory.getCategoryValue();
        Object oldWeight = oldCategory.getCategoryWeight();
        Object oldComputedWeight = oldCategory.getComputedWeight();
        categories[index] = sampleCategory;

        //FIXME (indexed)
        fireIndexedPropertyChange(PROPERTY_SAMPLE_CATEGORY, index, oldCategory, sampleCategory);
        fireIndexedPropertyChange(PROPERTY_SAMPLE_CATEGORY_VALUE, index, oldValue, sampleCategory.getCategoryValue());
        fireIndexedPropertyChange(PROPERTY_SAMPLE_CATEGORY_WEIGHT, index, oldWeight, sampleCategory.getCategoryWeight());
        fireIndexedPropertyChange(PROPERTY_SAMPLE_CATEGORY_COMPUTED_WEIGHT, index, oldComputedWeight, sampleCategory.getComputedWeight());
    }

    @Override
    public SampleCategory<?> getSampleCategoryById(Integer sampleCategoryId) {
        Integer index = getCategoryIndex(sampleCategoryId);
        return index == null ? null : categories[index];
    }

    @Override
    public SampleCategory<?> getSampleCategoryByIndex(int sampleCategoryIndex) {
        return categories[sampleCategoryIndex];
    }

    @Override
    public Serializable getSampleCategoryValue(Integer sampleCategoryId) {
        SampleCategory<?> sampleCategory = getSampleCategoryById(sampleCategoryId);
        return (Serializable) JavaBeanObjectUtil.getProperty(sampleCategory, SampleCategory.PROPERTY_CATEGORY_VALUE);
    }

    @Override
    public void setSampleCategoryValue(Integer sampleCategoryId, Serializable value) {
        SampleCategory<?> sampleCategory =
                getSampleCategoryById(sampleCategoryId);
        JavaBeanObjectUtil.setProperty(sampleCategory,
                                       SampleCategory.PROPERTY_CATEGORY_VALUE, value);
        firePropertyChange(PROPERTY_SAMPLE_CATEGORY_VALUE, null, sampleCategory);
    }

    @Override
    public void setSampleCategoryWeight(Integer sampleCategoryId, Object value) {
        SampleCategory<?> sampleCategory =
                getSampleCategoryById(sampleCategoryId);
        JavaBeanObjectUtil.setProperty(sampleCategory,
                                       SampleCategory.PROPERTY_CATEGORY_WEIGHT, value);
        firePropertyChange(PROPERTY_SAMPLE_CATEGORY_WEIGHT, null, sampleCategory);
    }

    @Override
    public SampleCategory getFinestCategory() {
        SampleCategory result = null;
        for (int i = categories.length - 1; i > -1; i--) {
            SampleCategory<?> category = categories[i];
            if (category != null && category.isValid()) {
                result = category;
                break;
            }
        }
        return result;
    }

    @Override
    public SampleCategory<?> getFirstSampleCategory() {
        return categories[0];
    }

    @Override
    public SpeciesBatchRowModel getFirstAncestor(SampleCategory<?> entrySampleCategory) {
        SpeciesBatchRowModel result = this;
        if (getParentBatch() != null) {
            SpeciesBatchRowModel parentBatch = getParentBatch();
            SampleCategory<?> parentSampleCategory = parentBatch.getSampleCategoryById(entrySampleCategory.getCategoryId());
            if (Objects.equals(entrySampleCategory, parentSampleCategory)) {

                result = parentBatch.getFirstAncestor(entrySampleCategory);
            }
        }
        return result;
    }

    @Override
    public Iterator<SampleCategory<?>> iterator() {
        return Arrays.asList(categories).iterator();
    }

    //------------------------------------------------------------------------//
    //-- AttachmentModelAware                                               --//
    //------------------------------------------------------------------------//

    @Override
    public ObjectTypeCode getObjectType() {
        return ObjectTypeCode.BATCH;
    }

    @Override
    public Integer getObjectId() {
        return getIdAsInt();
    }

    @Override
    public List<Attachment> getAttachment() {
        return attachment;
    }

    @Override
    public void addAllAttachment(Collection<Attachment> attachments) {
        this.attachment.addAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        this.attachment.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAllAttachment(Collection<Attachment> attachments) {
        this.attachment.removeAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        this.attachment.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    //------------------------------------------------------------------------//
    //-- Other properties                                                   --//
    //------------------------------------------------------------------------//

    public SpeciesBatchRowModel getFirstAncestor(Integer sampleCategoryId) {
        SampleCategory<?> sampleCategory = getSampleCategoryById(sampleCategoryId);
        return getFirstAncestor(sampleCategory);
    }

    public List<SpeciesBatchRowModel> getChildBatch() {
        return childBatch;
    }

    public void setChildBatch(List<SpeciesBatchRowModel> childBatch) {
        this.childBatch = childBatch;
        // force to propagate child changes
        firePropertyChange(PROPERTY_CHILD_BATCH, null, childBatch);
        firePropertyChange(PROPERTY_BATCH_LEAF, null, isBatchLeaf());
    }

    public boolean isBatchLeaf() {
        return CollectionUtils.isEmpty(childBatch);
    }

    public boolean isBatchRoot() {
        return getParentBatch() == null;
    }

    public List<SpeciesFrequencyRowModel> getFrequency() {
        return frequency;
    }

    public void setFrequency(List<SpeciesFrequencyRowModel> frequency) {
        this.frequency = frequency;
        // force to propagate frequencies changes
        firePropertyChange(PROPERTY_FREQUENCY, null, frequency);
    }

    public List<IndividualObservationBatchRowModel> getIndividualObservation() {
        return individualObservation;
    }

    public void setIndividualObservation(List<IndividualObservationBatchRowModel> individualObservation) {
        this.individualObservation = individualObservation;
        // force to propagate observation changes
        firePropertyChange(PROPERTY_INDIVIDUAL_OBSERVATION, null, individualObservation);
    }

    public ComputableData<Integer> getComputedOrNotNumber() {
        return computedOrNotNumber;
    }

    public void setComputedOrNotNumber(ComputableData<Integer> computedOrNotNumber) {
        this.computedOrNotNumber = computedOrNotNumber;
    }

    public ComputableData<Float> getComputedOrNotWeight() {
        return computedOrNotWeight;
    }

    public void setComputedOrNotWeight(ComputableData<Float> computedOrNotWeight) {
        this.computedOrNotWeight = computedOrNotWeight;
    }

    public void collectShell(Set<SpeciesBatchRowModel> collectedRows) {

        collectedRows.add(this);

        if (!isBatchLeaf()) {

            for (SpeciesBatchRowModel batchChild : getChildBatch()) {
                collectedRows.add(batchChild);
                batchChild.collectShell(collectedRows);
            }
        }
    }

    public boolean containsIndividualObservations() {

        boolean result = CollectionUtils.isNotEmpty(getIndividualObservation());

        if (!result && !isChildBatchsEmpty()) {

            for (SpeciesBatchRowModel childRow : childBatch) {
                result = childRow.containsIndividualObservations();
                if (result) {
                    break;
                }
            }
        }

        return result;

    }
}
