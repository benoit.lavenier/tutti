package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.service.sampling.SamplingCodePrefix;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.bean.JavaBeanObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created on 16/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationBatchUIModel extends AbstractTuttiTableUIModel<SpeciesBatchRowModel, IndividualObservationBatchRowModel, IndividualObservationBatchUIModel> implements JavaBeanObject {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(IndividualObservationBatchUIModel.class);

    /**
     * Global screen model.
     *
     * @since 4.5
     */
    private final SpeciesFrequencyUIModel parentModel;

    /**
     * Samping notification model.
     *
     * @since 4.5
     */
    private final SamplingNotificationZoneModel samplingNotificationZoneModel;

    /**
     * Sexe caracteristic.
     *
     * @since 4.5
     */
    private final Caracteristic sexCaracteristic;

    /**
     * Default caracteristics coming from protocol.
     *
     * @since 4.5
     */
    private final List<Caracteristic> protocolIndividualObservationCaracteristics;

    /**
     * Caracteristique de maturité du protocole
     */
    private Caracteristic maturityCaracteristic;

    /**
     * Caracteristiques non éditables, car caractéristiques de catégorisation du lot (sexe, maturité, etc)
     *
     * @since 4.5
     */
    private Collection<Caracteristic> notEditableCaracteristic;

    /**
     * Le préfixe du code de prélèvement.
     *
     * @since 4.5
     */
    protected SamplingCodePrefix samplingCodePrefix;

    /**
     * Table model.
     *
     * @since 4.5
     */
    private IndividualObservationBatchTableModel individualObservationTableModel;

    @Override
    protected SpeciesBatchRowModel newEntity() {
        return null; // Jamais utilisé!
    }

    public IndividualObservationBatchUIModel(SpeciesFrequencyUIModel parentModel,
                                             Caracteristic sexCaracteristic,
                                             List<Caracteristic> protocolIndividualObservationCaracteristics) {
        super(SpeciesBatchRowModel.class, null, null);
        this.parentModel = parentModel;
        this.sexCaracteristic = sexCaracteristic;
        this.protocolIndividualObservationCaracteristics = protocolIndividualObservationCaracteristics == null ?
                                                           new ArrayList<>() : new ArrayList<>(protocolIndividualObservationCaracteristics);
        this.samplingNotificationZoneModel = new SamplingNotificationZoneModel();
    }

    public SamplingNotificationZoneModel getSamplingNotificationZoneModel() {
        return samplingNotificationZoneModel;
    }

    public SamplingCodePrefix getSamplingCodePrefix() {
        return samplingCodePrefix;
    }

    public void setSamplingCodePrefix(SamplingCodePrefix samplingCodePrefix) {
        this.samplingCodePrefix = samplingCodePrefix;
    }

    public List<Caracteristic> getProtocolIndividualObservationCaracteristics() {
        return protocolIndividualObservationCaracteristics;
    }

    public boolean withMaturityCaracteristic() {
        return maturityCaracteristic != null;
    }

    public Caracteristic getMaturityCaracteristic() {
        return maturityCaracteristic;
    }

    public void setMaturityCaracteristic(Caracteristic maturityCaracteristic) {
        this.maturityCaracteristic = maturityCaracteristic;
    }

    public CaracteristicQualitativeValue getMaturityValue(IndividualObservationBatchRowModel row) {

        CaracteristicQualitativeValue caracteristicValue = null;
        if (withMaturityCaracteristic()) {
            caracteristicValue = row.getCaracteristicQualitativeValue(maturityCaracteristic);
        }
        return caracteristicValue;

    }

    public CaracteristicQualitativeValue getMaturityValue(CaracteristicMap caracteristicMap) {

        CaracteristicQualitativeValue caracteristicValue = null;
        if (withMaturityCaracteristic()) {
            caracteristicValue = caracteristicMap.getQualitativeValue(maturityCaracteristic);
        }
        return caracteristicValue;

    }

    public void moveMaturityValueFromCaracteristicsToDefaultCaracteristics(IndividualObservationBatchRowModel newRow) {

        if (withMaturityCaracteristic()) {

            Serializable caracteristicValue = newRow.getCaracteristics().remove(maturityCaracteristic);
            newRow.getDefaultCaracteristics().putIfAbsent(maturityCaracteristic, caracteristicValue);

        }
    }

    public void setMaturityValueToDefaultCaracterictis(IndividualObservationBatchRowModel result, CaracteristicQualitativeValue maturityState) {
        result.getDefaultCaracteristics().put(maturityCaracteristic, maturityState);
    }

    public Caracteristic getSexCaracteristic() {
        return sexCaracteristic;
    }

    public CaracteristicQualitativeValue getGender(IndividualObservationBatchRowModel row) {
        return row.getCaracteristicQualitativeValue(sexCaracteristic);
    }

    public CaracteristicQualitativeValue getGender(CaracteristicMap caracteristicMap) {
        return caracteristicMap.getQualitativeValue(sexCaracteristic);
    }

    public void setGenderValueToDefaultCaracterictis(IndividualObservationBatchRowModel result, CaracteristicQualitativeValue gender) {
        result.getDefaultCaracteristics().put(sexCaracteristic, gender);
    }

    public void moveGenderValueFromCaracteristicsToDefaultCaracteristics(IndividualObservationBatchRowModel newRow) {
        Serializable caracteristicValue = newRow.getCaracteristics().remove(sexCaracteristic);
        if (caracteristicValue != null) {
            newRow.getDefaultCaracteristics().putIfAbsent(sexCaracteristic, caracteristicValue);
        }
    }

    public void setNotEditableCaracteristic(Collection<Caracteristic> notEditableCaracteristic) {
        this.notEditableCaracteristic = notEditableCaracteristic;
    }

    /**
     * TODO Voir si c'est vraiment nécessaire de vérifier que les lignes ne sont pas vides ?
     *
     * @return {@code true} s'il existe au moins une ligne en erreur avec des caractéristiques remplies
     */
    public boolean isNonEmptyRowInError() {
        return rowsInError.stream().anyMatch(row -> !row.isEmpty(notEditableCaracteristic));
    }

    public void recomputeRowsValidateState() {

        if (log.isInfoEnabled()) {
            log.info("Revalidate all individual observation rows");
        }

        rowsInError.clear();

        rows.forEach(row -> {
            // recompute row valid state
            boolean valid = row.computeValid();

            // apply it to row
            row.setValid(valid);
            if (!valid) {
                rowsInError.add(row);
            }

        });

        firePropertyChange(PROPERTY_ROWS_IN_ERROR, null, rowsInError);

    }

    public boolean recomputeCanEditLengthStep() {
        boolean result = true;
        if (rows != null) {
            for (IndividualObservationBatchRowModel row : rows) {

                if (row.isEmpty(notEditableCaracteristic)) {
                    // la ligne est vide
                    continue;
                }

                // une ligne non vide et complete a ete trouvee
                // on ne peut plus editer
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public void firePropertyChanged(String propertyName, Object oldValue, Object newValue) {
        super.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void recomputeRowValidState(IndividualObservationBatchRowModel row) {

        // recompute row valid state
        boolean valid = row.computeValid();

        // apply it to row
        row.setValid(valid);

        if (valid) {
            removeRowInError(row);
        } else {
            addRowInError(row);
        }

    }

    public void setCopyIndividualObservationMode(CopyIndividualObservationMode newCopyMode) {

        getRows().forEach(row -> row.setCopyIndividualObservationMode(newCopyMode));
        recomputeRowsValidateState();

        individualObservationTableModel.fireTableDataChanged();

    }

    public void removeIndividualObservations(Collection<IndividualObservationBatchRowModel> rows) {

        // on supprime les observations individuelles des caches
        parentModel.getIndividualObservationUICache().removeIndividualObservations(rows);
        parentModel.getSamplingCodeUICache().removeIndividualObservations(rows);

        List<IndividualObservationBatchRowModel> newRows = new ArrayList<>(getRows());
        newRows.removeAll(rows);

        setRows(newRows);

    }

    public void setIndividualObservationTableModel(IndividualObservationBatchTableModel individualObservationTableModel) {
        this.individualObservationTableModel = individualObservationTableModel;
    }

    public void clear() {

        removeIndividualObservations(rows);

    }
}
