package fr.ifremer.tutti.ui.swing.content.operation.fishing.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUIModel;
import org.nuiton.util.DateUtil;

import java.util.Date;
import java.util.List;

/**
 * To create a new fishing operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class NewFishingOperationAction extends EditFishingOperationAction {

    public NewFishingOperationAction(FishingOperationsUIHandler handler) {
        super(handler);
    }

    @Override
    public void doAction() throws Exception {

        FishingOperationsUIModel model = getModel();

        // deselect selected fishingOperation
        // Will remove the selection fishing operation from the comboBox

        model.setCatchEnabled(true);

        model.setEditionAdjusting(true);
        try {
            model.setSelectedFishingOperation(null);
        } finally {
            model.setEditionAdjusting(false);
        }

        // creates a empty bean

        FishingOperation newFishingOperation =
                FishingOperations.newFishingOperation();
        Cruise cruise = getDataContext().getCruise();
        newFishingOperation.setCruise(cruise);
        newFishingOperation.setVessel(cruise.getVessel());

        List<GearWithOriginalRankOrder> gears = cruise.getGear();
        if (gears.size() == 1) {
            newFishingOperation.setGear(gears.get(0));
        }

        if (cruise.getMultirigNumber() == 1) {
            newFishingOperation.setMultirigAggregation("1");
        }

        Date today = DateUtil.getDay(new Date());
        newFishingOperation.setGearShootingStartDate(today);
        newFishingOperation.setGearShootingEndDate(today);
        newFishingOperation.setFishingOperationValid(true);
        setFishingOperation(newFishingOperation);

        super.doAction();
    }
}
