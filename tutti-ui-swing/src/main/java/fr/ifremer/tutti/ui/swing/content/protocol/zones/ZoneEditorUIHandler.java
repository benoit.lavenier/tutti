package fr.ifremer.tutti.ui.swing.content.protocol.zones;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.SubStrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.ZoneNode;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class ZoneEditorUIHandler extends AbstractTuttiUIHandler<EditProtocolUIModel, ZoneEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZoneEditorUIHandler.class);

    @Override
    public void afterInit(ZoneEditorUI zoneEditorUI) {

        initUI(zoneEditorUI);

        // init trees

        JTree availableStratasTree = ui.getAvailableStratasTree();
        SwingUtil.addExpandOnClickListener(availableStratasTree);

        JTree zonesTree = ui.getZonesTree();
        SwingUtil.addExpandOnClickListener(zonesTree);

        zonesTree.addTreeSelectionListener(event -> {

            TreePath[] selectedPaths = zonesTree.getSelectionPaths();
            boolean allStrataNode = true;

            if (selectedPaths != null) {

                if (selectedPaths.length == 1) {
                    boolean zoneSelected = event.getPath().getLastPathComponent() instanceof ZoneNode;
                    getUI().getDeleteZoneMenuItem().setEnabled(zoneSelected);
                    getUI().getRenameZoneMenuItem().setEnabled(zoneSelected);
                }

                for (TreePath selectedPath : selectedPaths) {
                    Object lastPathComponent = selectedPath.getLastPathComponent();
                    if (!(lastPathComponent instanceof StrataNode
                            || lastPathComponent instanceof SubStrataNode)) {
                        allStrataNode = false;
                        break;
                    }
                }

            }

            getUI().getRemoveButton().setEnabled(selectedPaths != null && allStrataNode);

        });

        TreeSelectionListener enableAddStrataListener = evt -> {

            boolean addButtonEnabled = zonesTree.getSelectionCount() == 1
                    && zonesTree.getSelectionPath().getLastPathComponent() instanceof ZoneNode
                    && availableStratasTree.getSelectionCount() >= 1;
            getUI().getAddButton().setEnabled(addButtonEnabled);

        };
        zonesTree.addTreeSelectionListener(enableAddStrataListener);
        availableStratasTree.addTreeSelectionListener(enableAddStrataListener);

    }

    @Override
    public SwingValidator<EditProtocolUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return ui.getZonesTree();
    }

    @Override
    public void onCloseUI() {
//        getModel().getZone().forEach(zone -> ((ZoneUIModel) zone).removePropertyChangeListener(ZoneUIModel.PROPERTY_STRATA,
//                                                                                stratasChangeListener));
    }

    public void onKeyPressedOnZones(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_ENTER && getUI().getRemoveButton().isEnabled()) {
            getContext().getActionEngine().runAction(getUI().getRemoveButton());
        }
    }

    public void onKeyPressedOnAvailableStratas(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_ENTER && getUI().getAddButton().isEnabled()) {
            getContext().getActionEngine().runAction(getUI().getAddButton());
        }
    }


    public void onMouseClickedPressedOnZones(MouseEvent e, JPopupMenu popup) {

        if (SwingUtilities.isRightMouseButton(e)) {

            // get the coordinates of the mouse click
            Point p = e.getPoint();

            JTree source = (JTree) e.getSource();

            // get the row index at this point
            int rowIndex = source.getRowForLocation(p.x, p.y);

            if (log.isDebugEnabled()) {
                log.debug("At point [" + p + "] found Row " + rowIndex);
            }

            source.setSelectionRow(rowIndex);

            // on right click show popup
            popup.show(source, e.getX(), e.getY());

        } else if (e.getClickCount() == 2 && getUI().getRemoveButton().isEnabled()) {
            getContext().getActionEngine().runAction(getUI().getRemoveButton());
        }
    }

    public void onMouseClickedOnAvailableStratas(MouseEvent mouseEvent) {

        if (mouseEvent.getClickCount() == 2 && getUI().getAddButton().isEnabled()) {
            getContext().getActionEngine().runAction(getUI().getAddButton());
        }
    }

}
