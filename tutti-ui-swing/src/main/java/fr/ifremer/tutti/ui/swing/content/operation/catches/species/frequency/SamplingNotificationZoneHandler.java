package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.sampling.CalcifiedPiecesSamplingAlgorithmEntryNotFoundException;
import fr.ifremer.tutti.service.sampling.IndividualObservationSamplingContext;
import fr.ifremer.tutti.service.sampling.IndividualObservationSamplingStatus;
import fr.ifremer.tutti.service.sampling.SizeNotDefinedOnIndividualObservationException;
import fr.ifremer.tutti.service.sampling.ZoneNotDefinedOnFishingOperationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.io.Closeable;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Pour gérer la zone de notification des observations individuelles.
 *
 * Created on 18/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SamplingNotificationZoneHandler implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SamplingNotificationZoneHandler.class);

    private final SpeciesFrequencyUIModel uiModel;
    private final SamplingNotificationZoneModel model;
    private final IndividualObservationBatchTableModel individualObservationTableModel;
    private final ListSelectionModel listSelectionModel;

    private final IndividualObservationUICache individualObservationUICache;

    private final JLabel samplingWarningLabel;
    private final JLabel samplingSummaryLabel;

    private final ListSelectionListener listSelectionListener;
    private final PropertyChangeListener modelSummaryTextChanged;
    private final PropertyChangeListener modelSelectedRowChanged;
    private final PropertyChangeListener modelUpdatedRowChanged;
    private final PropertyChangeListener modelStatusChanged;

    private final Decorator<Integer> infiniteDecorator;
    private final Decorator<Species> speciesDecorator;
    private final Decorator<Zone> zoneDecorator;

    private final Color colorHighlightInfoForeground;
    private final Color colorHighlightInfoBackground;

    public SamplingNotificationZoneHandler(SpeciesFrequencyUI ui, SamplingNotificationZoneModel model, IndividualObservationUICache individualObservationUICache) {
        this.uiModel = ui.getModel();
        this.samplingWarningLabel = ui.getSamplingWarningLabel();
        this.samplingSummaryLabel = ui.getSamplingResumeLabel();
        this.individualObservationTableModel = (IndividualObservationBatchTableModel) ui.getObsTable().getModel();
        this.model = model;
        this.listSelectionModel = ui.getObsTable().getSelectionModel();

        SpeciesFrequencyUIHandler uiHandler = ui.getHandler();
        this.individualObservationUICache = individualObservationUICache;
        this.infiniteDecorator = uiHandler.getDecorator(Integer.class, DecoratorService.NULL_INFINITE);
        this.speciesDecorator = uiHandler.getDecorator(Species.class, DecoratorService.WITH_SURVEY_CODE);
        this.zoneDecorator = uiHandler.getDecorator(Zone.class, null);
        this.colorHighlightInfoForeground = uiHandler.getConfig().getColorHighlightInfoForeground();
        this.colorHighlightInfoBackground = uiHandler.getConfig().getColorHighlightInfoBackground();

        // Ecoute le changement d'état de la zone de notification pour mettre à jour le statut
        this.modelStatusChanged = event -> {

            SamplingNotificationZoneStatus newValue = (SamplingNotificationZoneStatus) event.getNewValue();

            switch (newValue) {

                case DISABLED:

                    samplingWarningLabel.setText(t("tutti.editSpeciesFrequencies.samplingNotification.warning.samplingDisabled"));
                    samplingWarningLabel.setForeground(null);
                    samplingWarningLabel.setBackground(Color.LIGHT_GRAY);

                    break;
                case NOT_USING_SAMPLING:

                    samplingWarningLabel.setText(t("tutti.editSpeciesFrequencies.samplingNotification.warning.samplingNotUsed"));
                    samplingWarningLabel.setForeground(null);
                    samplingWarningLabel.setBackground(Color.LIGHT_GRAY);

                    break;
                case NEED_SAMPLING:

                    samplingWarningLabel.setText(t("tutti.editSpeciesFrequencies.samplingNotification.warning.samplingNeeded"));
                    samplingWarningLabel.setForeground(colorHighlightInfoForeground);
                    samplingWarningLabel.setBackground(colorHighlightInfoBackground);

                    break;
                case NO_SAMPLING_REQUIRED:

                    samplingWarningLabel.setText(t("tutti.editSpeciesFrequencies.samplingNotification.warning.noSamplingRequired"));
                    samplingWarningLabel.setForeground(null);
                    samplingWarningLabel.setBackground(Color.LIGHT_GRAY);

                    break;
                case COUNT_ATTAINED:

                    samplingWarningLabel.setText(t("tutti.editSpeciesFrequencies.samplingNotification.warning.samplingTotalCountAttained"));
                    samplingWarningLabel.setForeground(null);
                    samplingWarningLabel.setBackground(Color.LIGHT_GRAY);

                    break;

                case NONE:

                    samplingWarningLabel.setText(null);
                    samplingWarningLabel.setForeground(null);
                    samplingWarningLabel.setBackground(null);

                    break;
            }
        };

        // Ecoute le changement du texte de la zone de notification pour mettre à jour l'ui
        this.modelSummaryTextChanged = event -> {

            String newValue = (String) event.getNewValue();
            samplingSummaryLabel.setText(newValue);

        };

        // Ecoute le changement de sélection d'une observation individuelle pour demander au cache le statut de celle-ci
        this.modelSelectedRowChanged = event -> {

            IndividualObservationBatchRowModel selectedRow = (IndividualObservationBatchRowModel) event.getNewValue();

            if (selectedRow == null) {

                hideAll();
                return;
            }

            IndividualObservationSamplingStatus status = getIndividualObservationSamplingStatus(selectedRow);

            if (status != null) {

                // mise à jour du résumé
                String summaryText = getSummaryText(status);
                model.setSummaryText(summaryText);

                // mise à jour du statut de l'observation individuelle
                SamplingNotificationZoneStatus samplingNotificationZoneStatus;

                if (status.isOneTotalCountIsAttained()) {

                    // plus besoin de prélever (max atteint)
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.COUNT_ATTAINED;
                } else if (status.isNotUsingSampling()) {

                    // ne jamais prélever (max à 0)
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.NOT_USING_SAMPLING;
                } else {

                    // rien à afficher (en mode sélection, on ne peut pas dire s'il faut ou non prélever)
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.NONE;
                }

                model.setSamplingNotificationZoneStatus(samplingNotificationZoneStatus);

            }

        };

        // Ecoute le changement de modification d'une observation individuelle pour demander au cache le statut de celle-ci
        this.modelUpdatedRowChanged = event -> {

            IndividualObservationBatchRowModel updatedRow = (IndividualObservationBatchRowModel) event.getNewValue();

            Objects.requireNonNull(updatedRow);

            // récupération du status de l'observation individuelle
            IndividualObservationSamplingStatus status = getIndividualObservationSamplingStatus(updatedRow);

            if (status != null) {

                // mise à jour du résumé
                String summaryText = getSummaryText(status);
                model.setSummaryText(summaryText);

                // mise à jour du statut de l'observation individuelle
                SamplingNotificationZoneStatus samplingNotificationZoneStatus;

                if (status.isNeedSampling()) {

                    // demande de prélèvement, bingo!
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.NEED_SAMPLING;
                } else if (status.isOneTotalCountIsAttained()) {

                    // plus besoin de prélever (max atteint)
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.COUNT_ATTAINED;
                } else if (status.isNotUsingSampling()) {

                    // ne jamais prélever (max à 0)
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.NOT_USING_SAMPLING;
                } else if (updatedRow.withSamplingCode()) {

                    // l'observation a un code de prélèvement, on n'affiche pas de notification
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.NONE;
                } else {

                    // pas de prélèvement requis (samplingInterval)
                    samplingNotificationZoneStatus = SamplingNotificationZoneStatus.NO_SAMPLING_REQUIRED;
                }

                model.setSamplingNotificationZoneStatus(samplingNotificationZoneStatus);

            }

        };

        // Ecoute la sélection sur le tableau des observations individuelles pour rafraîchir la zone de notification
        this.listSelectionListener = event -> {

            ListSelectionModel source = (ListSelectionModel) event.getSource();

            if (model.isValueAdjusting()) {

                // évènement non terminée (en cours d'ajustement)
                return;
            }

            if (event.getValueIsAdjusting()) {

                // évènement non terminée (en cours d'ajustement)
                return;
            }

            if (source.isSelectionEmpty()) {

                // on supprime la sélection dans le modèle
                model.setSelectedRow(null);
                return;
            }

            if (getSelectedRowCount(source) > 1) {

                // plusieurs lignes sélectionnées, on ne peut pas afficher d'informations
                hideAll();
                return;
            }

            IndividualObservationBatchRowModel selectedRow = individualObservationTableModel.getEntry(source.getMinSelectionIndex());
            model.setSelectedRow(selectedRow);

        };

    }

    public void editBatch(SpeciesBatch speciesBatch) {

        if (log.isInfoEnabled()) {
            log.info("Edit batch for " + speciesBatch);
        }

        // toujours supprimer les listeners
        listSelectionModel.removeListSelectionListener(listSelectionListener);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SELECTED_ROW, modelSelectedRowChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_UPDATED_ROW, modelUpdatedRowChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SAMPLING_NOTIFICATION_ZONE_STATUS, modelStatusChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SUMMARY_TEXT, modelSummaryTextChanged);

        // on les ajoutent (on peut en avoir besoin pour désactiver la zone de notification)
        model.addPropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SELECTED_ROW, modelSelectedRowChanged);
        model.addPropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_UPDATED_ROW, modelUpdatedRowChanged);
        model.addPropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SAMPLING_NOTIFICATION_ZONE_STATUS, modelStatusChanged);
        model.addPropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SUMMARY_TEXT, modelSummaryTextChanged);

        if (!uiModel.isProtocolFilled()) {

            // pas de protocole
            stopUsingStatusNotication(t("tutti.editSpeciesFrequencies.samplingNotification.warning.noProtocol"));
            return;
        }

        if (!uiModel.isProtocolUseCalcifiedPieceSampling()) {

            // pas d'utilisation de l'algorithme de prélèvement des pièces calcifiées
            stopUsingStatusNotication(t("tutti.editSpeciesFrequencies.samplingNotification.warning.notUsingAlgorithm"));
            return;
        }

        if (!individualObservationUICache.isFishingOperationWithZone()) {

            // pas de zone définie sur l'opération de pêche
            stopUsingStatusNotication(t("tutti.editSpeciesFrequencies.samplingNotification.warning.fishingOperationNotInAZone"));
            return;
        }

        if (!individualObservationUICache.isSpeciesDefinedInCalcifiedPiecesSampling()) {

            // pas de définition d'algorithme sur cette espèce
            stopUsingStatusNotication(t("tutti.editSpeciesFrequencies.samplingNotification.warning.speciesNotInAlgorithm"));
            return;
        }

        Preconditions.checkState(individualObservationUICache.useCruiseSamplingCache());

        model.setSelectedRow(null);

        listSelectionModel.addListSelectionListener(listSelectionListener);

    }

    @Override
    public void close() {

        listSelectionModel.removeListSelectionListener(listSelectionListener);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SELECTED_ROW, modelSelectedRowChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_UPDATED_ROW, modelUpdatedRowChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SAMPLING_NOTIFICATION_ZONE_STATUS, modelStatusChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SUMMARY_TEXT, modelSummaryTextChanged);

    }

    private void hideAll() {

        model.setSummaryText(null);
        model.setSamplingNotificationZoneStatus(SamplingNotificationZoneStatus.NONE);

    }

    private void whenCanNotUseSampling(String message) {

        model.setSummaryText(message);
        model.setSamplingNotificationZoneStatus(SamplingNotificationZoneStatus.DISABLED);

    }

    private void stopUsingStatusNotication(String message) {

        whenCanNotUseSampling(message);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SELECTED_ROW, modelSelectedRowChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_UPDATED_ROW, modelUpdatedRowChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SAMPLING_NOTIFICATION_ZONE_STATUS, modelStatusChanged);
        model.removePropertyChangeListener(SamplingNotificationZoneModel.PROPERTY_SUMMARY_TEXT, modelSummaryTextChanged);

    }

    private String getSummaryText(IndividualObservationSamplingStatus status) {

        IndividualObservationSamplingContext individualObservationSamplingContext = status.getIndividualObservationSamplingContext();

        CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition = status.getCalcifiedPiecesSamplingDefinition();

        int nbForOperation = status.getSamplingCountInFishingOperation();
        int nbForZone = status.getSamplingCountInZone();
        int nbForCruise = status.getSamplingCountInCruise();

        String nbForOperationLabel = getLabelForSamplingNumber(nbForOperation, calcifiedPiecesSamplingDefinition.getOperationLimitation());
        String nbForZoneLabel = getLabelForSamplingNumber(nbForZone, calcifiedPiecesSamplingDefinition.getZoneLimitation());
        String nbForCruiseLabel = getLabelForSamplingNumber(nbForCruise, calcifiedPiecesSamplingDefinition.getMaxByLenghtStep());

        String key = speciesDecorator.toString(individualObservationSamplingContext.getSpecies())
                + " " + uiModel.convertFromMm(individualObservationSamplingContext.getLengthStep())
                + " " + uiModel.getLengthStepCaracteristicUnit();
        if (individualObservationSamplingContext.withGender()) {
            key += " " + individualObservationSamplingContext.getGender().getDescription();
        }
        if (individualObservationSamplingContext.withMaturity()) {
            if (individualObservationSamplingContext.getMaturity()) {
                key += " " + t("tutti.editSpeciesFrequencies.samplingNeeded.mature");
            } else {
                key += " " + t("tutti.editSpeciesFrequencies.samplingNeeded.immature");
            }
        }

        String zone = zoneDecorator.toString(individualObservationSamplingContext.getZone());

        return t("tutti.editSpeciesFrequencies.samplingNeeded.summary", key, nbForOperationLabel, zone, nbForZoneLabel, nbForCruiseLabel);

    }

    /**
     * Returns the number of selected rows.
     *
     * @return the number of selected rows, 0 if no rows are selected
     */
    private int getSelectedRowCount(ListSelectionModel selectionModel) {
        int iMin = selectionModel.getMinSelectionIndex();
        int iMax = selectionModel.getMaxSelectionIndex();
        int count = 0;

        for (int i = iMin; i <= iMax; i++) {
            if (selectionModel.isSelectedIndex(i)) {
                count++;
            }
        }
        return count;
    }

    private String getLabelForSamplingNumber(int value, Integer max) {
        return "<strong>" + infiniteDecorator.toString(value) + "</strong> (" + infiniteDecorator.toString(max) + ")";
    }

    private IndividualObservationSamplingStatus getIndividualObservationSamplingStatus(IndividualObservationBatchRowModel selectedRow) {

        IndividualObservationSamplingStatus individualObservationSamplingStatus = null;

        try {

            individualObservationSamplingStatus = individualObservationUICache.getIndividualObservationSamplingStatus(selectedRow);

        } catch (CalcifiedPiecesSamplingAlgorithmEntryNotFoundException e) {
            whenCanNotUseSampling(t("tutti.editSpeciesFrequencies.samplingNotification.warning.noAlgorithmEntry"));
        } catch (SizeNotDefinedOnIndividualObservationException e) {
            whenCanNotUseSampling(t("tutti.editSpeciesFrequencies.samplingNotification.warning.sizeNotDefined"));
        } catch (ZoneNotDefinedOnFishingOperationException e) {
            whenCanNotUseSampling(t("tutti.editSpeciesFrequencies.samplingNotification.warning.fishingOperationNotInAZone"));
        }

        return individualObservationSamplingStatus;

    }

}
