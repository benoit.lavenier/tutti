package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportResult;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyCellComponent;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public abstract class ImportMultiPostSpeciesSupportAction extends LongActionSupport<SpeciesFrequencyUIModel, SpeciesFrequencyUI, SpeciesFrequencyUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportMultiPostSpeciesSupportAction.class);

    /**
     * File to import.
     */
    private File file;

    /**
     * Optional data that were not imported.
     */
    private MultiPostImportResult importResult;
    protected Float totalWeight;

    protected ImportMultiPostSpeciesSupportAction(SpeciesFrequencyUIHandler handler) {
        super(handler, false);
    }

    public abstract boolean isImportFrequencies();

    public abstract boolean isImportIndivudalObservations();

    protected abstract String getSuccessMessage(File file);

    protected abstract String getFileExtension();

    protected abstract String getFileExtensionDescription();

    protected abstract String getFileChooserTitle();

    protected abstract String getFileChooserButton();

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {

            // choose file to import
            file = chooseFile(getFileChooserTitle(),
                              getFileChooserButton(),
                              "^.*\\." + getFileExtension(),
                              getFileExtensionDescription());

            doAction = file != null;

        }

        return doAction;

    }

    @Override
    public final void doAction() throws Exception {

        FishingOperation operation = getDataContext().getFishingOperation();

        SpeciesFrequencyUIModel model = getModel();
        SpeciesOrBenthosBatchUISupport batchUISupport = model.getSpeciesOrBenthosBatchUISupport();
        SpeciesBatch speciesBatch = model.getBatch().toEntity();
        totalWeight = model.getTotalWeight();
        importResult = batchUISupport.importMultiPost(file, operation, speciesBatch, isImportFrequencies(), isImportIndivudalObservations());

    }

    @Override
    public final void postSuccessAction() {
        super.postSuccessAction();

        sendMessage(getSuccessMessage(file));

        SpeciesFrequencyCellComponent.FrequencyCellEditor frequencyEditor = getHandler().getFrequencyEditor();
        SpeciesBatchRowModel editRow = frequencyEditor.getEditRow();

        Integer speciesBatchId = editRow.getIdAsInt();

        List<SpeciesBatchFrequency> frequencies = importResult.getImportedFrequencies();
        if (log.isInfoEnabled()) {
            log.info("[SpeciesBatch: " + speciesBatchId + "] Frequencies: " + frequencies.size());
        }
        SpeciesFrequencyUIModel model = getModel();
        List<SpeciesFrequencyRowModel> frequencyRows = SpeciesFrequencyRowModel.fromEntity(model.getSpeciesOrBenthosBatchUISupport().getWeightUnit(), frequencies);

        List<IndividualObservationBatch> individualObservations = importResult.getImportedObservations();
        if (log.isInfoEnabled()) {
            log.info("[SpeciesBatch: " + speciesBatchId + "] Individual Observations: " + individualObservations.size());
        }
        List<IndividualObservationBatchRowModel> individualObservationRows = IndividualObservationBatchRowModel.fromEntity(getConfig().getIndividualObservationWeightUnit(),
                                                                                                                           getDataContext().getDefaultIndividualObservationCaracteristics(),
                                                                                                                           individualObservations);

        // clear frequency model
        model.clear();

        // clear individual observations model
        model.getIndividualObservationModel().clear();

        // reload frequencies and individual observations (add refill the individual observations cache)
        getHandler().loadFrequenciesAndObservations(frequencyRows, individualObservationRows, true);

        if (totalWeight != null) {

            // on reporte ce poids qui ne peut pas être importé
            model.setTotalWeight(totalWeight);
        }

        if (!isImportFrequencies() && model.mustCopyIndividualObservationSize()) {

            // on doit recopier les mensurations depuis les observations individuelles
            model.getFrequencyTableModel().reloadRowsFromIndividualObservations();

        }

        model.setModify(true);

    }

    @Override
    public final void releaseAction() {
        file = null;
        importResult = null;
        totalWeight = null;
        super.releaseAction();
    }
}
