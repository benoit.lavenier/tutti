package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class ExportMultiPostSpeciesOnlyIndividualObservationsAction extends ExportMultiPostSpeciesSupportAction {

    public ExportMultiPostSpeciesOnlyIndividualObservationsAction(SpeciesFrequencyUIHandler handler) {
        super(handler);
    }

    @Override
    protected String getFileExtension() {
        return "tuttiSpeciesBatchOnlyIndividualObservations";
    }

    @Override
    protected String getFileExtensionDescription() {
        return t("tutti.common.file.tuttiSpeciesBatchOnlyIndividualObservations");
    }

    @Override
    protected String getFileChooserTitle() {
        return t("tutti.editSpeciesFrequency.action.exportMultiPostOnlyIndividualObservations.destinationFile.title");
    }

    @Override
    protected String getFileChooserButton() {
        return t("tutti.editSpeciesFrequency.action.exportMultiPostOnlyIndividualObservations.destinationFile.button");
    }

    @Override
    protected String getSuccessMessage(File file) {
        return t("tutti.editSpeciesFrequency.action.exportMultiPostOnlyIndividualObservations.success", file);
    }

    @Override
    public boolean isImportFrequencies() {
        return false;
    }

    @Override
    public boolean isImportIndivudalObservations() {
        return true;
    }

}
