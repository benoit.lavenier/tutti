package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractChangeScreenAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.context.JAXXContextEntryDef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Opens the protocol edition screen.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class EditProtocolAction extends AbstractChangeScreenAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditProtocolAction.class);

    public static final JAXXContextEntryDef<TuttiProtocol> CLEAN_PROTOCOL_ENTRY = new JAXXContextEntryDef<>("cleanProtocol", TuttiProtocol.class);

    public EditProtocolAction(MainUIHandler handler) {
        super(handler, true, TuttiScreen.EDIT_PROTOCOL);
    }

    @Override
    public boolean prepareAction() throws Exception {

        CLEAN_PROTOCOL_ENTRY.removeContextValue(getContext().getMainUI());

        boolean doAction = super.prepareAction();
        if (doAction) {

            // check that protocol is compatible with sample category model
            SampleCategoryModel sampleCategoryModel =
                    getDataContext().getSampleCategoryModel();

            TuttiProtocol protocol = getDataContext().getProtocol();

            Set<Integer> badCategories = Sets.newHashSet();

            TuttiProtocols.checkSampleCategories(sampleCategoryModel,
                                                 protocol,
                                                 badCategories);

            if (!badCategories.isEmpty()) {

                // detect some bad categories
                if (log.isWarnEnabled()) {
                    log.warn("There is some bad categories: " + badCategories);
                }

                String message = TuttiProtocols.getBadCategoriesMessage(
                        badCategories,
                        getDecorator(Caracteristic.class, null),
                        getContext().getPersistenceService());

                String htmlMessage = String.format(
                        AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                        message,
                        t("tutti.common.askBeforeEditProtocol.help"));
                int response = JOptionPane.showOptionDialog(
                        getContext().getActionUI(),
                        htmlMessage,
                        t("tutti.common.askBeforeEditProtocol.title"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        UIManager.getIcon("warning"),
                        new Object[]{t("tutti.option.cleanAndEdit"), t("tutti.option.edit"), t("tutti.option.cancel")},
                        t("tutti.option.cancel")
                );

                switch (response) {
                    case 0:
                        // edit and clean
                        if (log.isInfoEnabled()) {
                            log.info("Clean and edit");
                        }

                        protocol = getContext().getPersistenceService().getProtocol(getContext().getProtocolId());

                        CLEAN_PROTOCOL_ENTRY.setContextValue(getContext().getMainUI(), protocol);

                        TuttiProtocols.removeBadCategories(sampleCategoryModel,
                                                           protocol);

                        break;

                    case 1:
                        // edit with no modification
                        if (log.isInfoEnabled()) {
                            log.info("Edit with no cleaning");
                        }
                        break;

                    default:

                        // cancel
                        doAction = false;
                }

            }
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkState(getContext().isProtocolFilled());
        if (log.isInfoEnabled()) {
            log.info("Edit protocol: " + getContext().getProtocolId());
        }
        createProgressionModelIfRequired(4);
        super.doAction();
    }

}
