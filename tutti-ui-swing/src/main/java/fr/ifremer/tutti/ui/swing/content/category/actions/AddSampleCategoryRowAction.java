package fr.ifremer.tutti.ui.swing.content.category.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelRowModel;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelTableModel;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUI;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;

import javax.swing.SwingUtilities;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class AddSampleCategoryRowAction extends SimpleActionSupport<EditSampleCategoryModelUI> {

    private static final long serialVersionUID = 1L;

    public AddSampleCategoryRowAction(EditSampleCategoryModelUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(final EditSampleCategoryModelUI ui) {

        BeanFilterableComboBox<Caracteristic> keyCombo = ui.getAvailableCaracteristicsComboBox();
        Caracteristic selectedItem = (Caracteristic) keyCombo.getSelectedItem();

        EditSampleCategoryModelUIModel model = ui.getModel();

        model.removeCaracteristic(selectedItem);

        EditSampleCategoryModelTableModel tableModel = ui.getHandler().getTableModel();

        EditSampleCategoryModelRowModel row = tableModel.createNewRow();
        row.setCaracteristic(selectedItem);

        tableModel.addNewRow(row);

        row.setValid(false);

        model.addRowInError(row);
        model.setModify(true);

        SwingUtilities.invokeLater(() -> {
            ui.getTable().requestFocus();

            int rowIndex = ui.getTable().getRowCount() - 1;

            SwingUtil.editCell(ui.getTable(), rowIndex, 1);
        });

    }

}