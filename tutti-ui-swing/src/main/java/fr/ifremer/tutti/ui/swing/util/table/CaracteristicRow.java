package fr.ifremer.tutti.ui.swing.util.table;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.LabelAware;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 0.3
 */
public class CaracteristicRow implements LabelAware {

    private static final long serialVersionUID = 1L;

    protected Object[] values;

    protected Caracteristic key;

    public CaracteristicRow(Caracteristic key, Object... values) {
        this.key = key;
        this.values = values;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object... values) {
        this.values = values;
    }

    public Caracteristic getKey() {
        return key;
    }

    public void setKey(Caracteristic key) {
        this.key = key;
    }

    @Override
    public String getLabel() {
        return key.getName();
    }
}
