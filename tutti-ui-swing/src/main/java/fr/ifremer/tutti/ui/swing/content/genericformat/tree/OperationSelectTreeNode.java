package fr.ifremer.tutti.ui.swing.content.genericformat.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.model.OperationDataModel;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class OperationSelectTreeNode extends DataSelectTreeNodeSupport<OperationDataModel> {

    private static final long serialVersionUID = 1L;

    private boolean selected;

    public OperationSelectTreeNode(OperationDataModel userObject) {
        super(userObject);
        setAllowsChildren(false);
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
        getParent().updateSelectedSate();
    }

    @Override
    public OperationDataModel getSelectedDataModel() {

        OperationDataModel result;
        if (isSelected()) {
            result = new OperationDataModel(getId(), getLabel());
        } else {
            result = null;
        }
        return result;

    }

    @Override
    public CruiseSelectTreeNode getParent() {
        return (CruiseSelectTreeNode) super.getParent();
    }

}
