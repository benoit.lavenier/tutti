package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.caliper.feed.event.CaliperFeedReaderEvent;
import fr.ifremer.tutti.caliper.feed.event.CaliperFeedReaderListener;
import fr.ifremer.tutti.caliper.feed.record.CaliperFeedReaderMeasureRecord;
import fr.ifremer.tutti.ichtyometer.feed.event.IchtyometerFeedReaderEvent;
import fr.ifremer.tutti.ichtyometer.feed.event.IchtyometerFeedReaderListener;
import fr.ifremer.tutti.ichtyometer.feed.record.IchtyometerFeedReaderMeasureRecord;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Sexs;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCache;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCaches;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.cruise.CruiseCache;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyCellComponent.FrequencyCellEditor;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.ApplySpeciesFrequencyRafaleAction;
import fr.ifremer.tutti.ui.swing.util.SoundEngine;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapColumnRowModel;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapColumnUIHandler;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorUI;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import fr.ifremer.tutti.ui.swing.util.table.CaracteristicColumnIdentifier;
import fr.ifremer.tutti.util.Units;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.action.ApplicationUIAction;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class SpeciesFrequencyUIHandler extends AbstractTuttiTableUIHandler<SpeciesFrequencyRowModel, SpeciesFrequencyUIModel, SpeciesFrequencyUI>
        implements CaracteristicMapColumnUIHandler {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesFrequencyUIHandler.class);

    public static final String OBS_TABLE_CARD = "obsTableCard";
    public static final String EDIT_CARACTERISTICS_CARD = "editCaracteristicsCard";

    private FrequencyCellEditor frequencyEditor;

    private TaxonCache taxonCache;

    private Map<String, Caracteristic> lengthStepCaracteristics;

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected WeightUnit weightUnit;

    /**
     * To consume measures when they arrive from the ichtyometer.
     */
    protected final IchtyometerFeedReaderListener ichtyometerFeedReaderListener;
    /**
     * To consume measures when they arrive from the caliper.
     */
    protected final CaliperFeedReaderListener caliperFeedReaderListener;
    /**
     * Pour écouter quand le big fin se connecte.
     */
    protected final PropertyChangeListener listenIchtyomerIsConnected;
    /**
     * Pour écouter quand le pied à coulisse se connecte.
     */
    protected final PropertyChangeListener listenCaliperIsConnected;

    protected ApplySpeciesFrequencyRafaleAction applySpeciesFrequencyRafaleAction;

    protected SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    protected IndividualObservationBatchTableHandler individualObservationBatchTableHandler;
    protected AverageWeightsHistogramHandler averageWeightsHistogramHandler;
    protected FrequenciesHistogramHandler frequenciesHistogramHandler;
    protected SamplingNotificationZoneHandler samplingNotificationZoneHandler;

    protected Decorator<Caracteristic> caracteristicDecorator;
    protected Decorator<Caracteristic> caracteristicTipDecorator;
    protected Decorator<CaracteristicQualitativeValue> caracteristicQualitativeDecorator;
    protected SoundEngine soundEngine;
    protected PropertyChangeListener listenFishingOperationReloadInDataContext;

    public SpeciesFrequencyUIHandler() {

        super(SpeciesFrequencyRowModel.PROPERTY_LENGTH_STEP,
              SpeciesFrequencyRowModel.PROPERTY_NUMBER,
              SpeciesFrequencyRowModel.PROPERTY_WEIGHT);

        this.ichtyometerFeedReaderListener = new IchtyometerFeedReaderListener() {

            @Override
            public void recordRead(IchtyometerFeedReaderEvent event) {
                final IchtyometerFeedReaderMeasureRecord record = event.getRecord();

                SwingUtilities.invokeLater(
                        () -> {
                            if (!getModel().isSimpleCountingMode()) {

                                // can try to consume value
                                consumeIchtyometerFeedRecord(record);
                            }
                        }
                );
            }
        };

        this.caliperFeedReaderListener = new CaliperFeedReaderListener() {

            @Override
            public void recordRead(CaliperFeedReaderEvent event) {
                final CaliperFeedReaderMeasureRecord record = event.getRecord();

                SwingUtilities.invokeLater(
                        () -> {
                            if (!getModel().isSimpleCountingMode()) {

                                // can try to consume value
                                consumeCaliperFeedRecord(record);
                            }
                        }
                );
            }
        };

        this.listenIchtyomerIsConnected = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                boolean connected = (boolean) evt.getNewValue();

                if (connected && SpeciesFrequencyUIHandler.this.frequencyEditor != null) {

                    // listen when itchtyometer is connected and this ui is showing
                    SpeciesFrequencyUIHandler.this.listenItchtyometer();
                }

                SwingUtilities.invokeLater(SpeciesFrequencyUIHandler.this::updateLogVisibility);
            }
        };

        this.listenCaliperIsConnected = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                boolean connected = (boolean) evt.getNewValue();

                if (connected && SpeciesFrequencyUIHandler.this.frequencyEditor != null) {

                    // listen when caliper is connected and this ui is showing
                    SpeciesFrequencyUIHandler.this.listenCaliper();
                }

                SwingUtilities.invokeLater(SpeciesFrequencyUIHandler.this::updateLogVisibility);
            }
        };

        // pour recharger l'opération dans le modèle quand celle ci a changée (après une sauvegarde de l'opération par exemple)
        this.listenFishingOperationReloadInDataContext = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent event) {
                TuttiDataContext source = (TuttiDataContext) event.getSource();
                FishingOperation fishingOperation = source.getFishingOperation();
                if (log.isInfoEnabled()) {
                    log.info("Reloading fishing operation in model: " + fishingOperation);
                }
                SpeciesFrequencyUIHandler.this.getModel().setFishingOperation(fishingOperation);
            }
        };

    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public SpeciesFrequencyTableModel getTableModel() {
        return (SpeciesFrequencyTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getTable();
    }

    @Override
    public boolean isRowValid(SpeciesFrequencyRowModel row) {

        SpeciesFrequencyUIModel model = getModel();
        return model.isRowValid(row);

    }

    @Override
    public CaracteristicMapEditorUI getCaracteristicMapEditor() {
        return ui.getObsCaracteristicCaracteristicMapEditor();
    }

    @Override
    public void showCaracteristicMapEditor(CaracteristicMapColumnRowModel editRow) {
        IndividualObservationBatchRowModel row = (IndividualObservationBatchRowModel) editRow;
        WeightUnit individualObservationWeightUnit = getConfig().getIndividualObservationWeightUnit();
        String title = String.format("<html><body style='color:black;'><strong>%s - %s %s - %s %s</strong> - %s</body></html>",
                                     row.getRankOrder(),
                                     row.getSize(),
                                     getModel().getLengthStepCaracteristicUnit(),
                                     individualObservationWeightUnit.renderWeight(row.getWeight()),
                                     individualObservationWeightUnit.getShortLabel(),
                                     t("tutti.editIndividualObservationBatch.table.header.otherCaracteristics"));
        ui.getObsCaracteristicMapEditorReminderLabel().setTitle(title);
        ui.getObsPanelLayout().setSelected(EDIT_CARACTERISTICS_CARD);

    }

    @Override
    public void hideCaracteristicMapEditor() {
        ui.getObsPanelLayout().setSelected(OBS_TABLE_CARD);
    }

    @Override
    protected void onModelRowsChanged(List<SpeciesFrequencyRowModel> rows) {

        SpeciesFrequencyUIModel model = getModel();

        model.reloadRows();

        getTableModel().setRows(rows);

        // clean log table
        SpeciesFrequencyLogsTableModel logsTableModel = (SpeciesFrequencyLogsTableModel) ui.getLogsTable().getModel();
        logsTableModel.setRows(new ArrayList<>());

        getModel().setModify(false);

    }

    @Override
    protected void onRowModified(int rowIndex,
                                 SpeciesFrequencyRowModel row,
                                 String propertyName,
                                 Object oldValue,
                                 Object newValue) {

        // We do nothing here. This API works only on the selected row.
        // On this screen, we can interacts with not selected row, so won't come here.
        // Better then to work directly on rows in the table model
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<SpeciesFrequencyRowModel> rowMonitor,
                                             SpeciesFrequencyRowModel row) {
    }

    @Override
    protected void onRowValidStateChanged(int rowIndex,
                                          SpeciesFrequencyRowModel row,
                                          Boolean oldValue,
                                          Boolean newValue) {
        super.onRowValidStateChanged(rowIndex, row, oldValue, newValue);
        ui.getValidator().doValidate();
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public SwingValidator<SpeciesFrequencyUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public void beforeInit(SpeciesFrequencyUI ui) {

        super.beforeInit(ui);

        this.speciesOrBenthosBatchUISupport = ui.getContextValue(SpeciesOrBenthosBatchUISupport.class, ui.getSpeciesOrBenthosContext());

        this.weightUnit = speciesOrBenthosBatchUISupport.getWeightUnit();
        this.caracteristicDecorator = getDecorator(Caracteristic.class, DecoratorService.CARACTERISTIC_PARAMETER_ONLY_WITH_UNIT);
        this.caracteristicTipDecorator = getDecorator(Caracteristic.class, DecoratorService.CARACTERISTIC_WITH_UNIT);
        this.caracteristicQualitativeDecorator = getDecorator(CaracteristicQualitativeValue.class, null);
        this.soundEngine = getContext().getSoundEngine();

        Caracteristic sexCaracteristic = getPersistenceService().getSexCaracteristic();

        TuttiDataContext dataContext = getDataContext();
        SampleCategoryModel sampleCategoryModel = dataContext.getSampleCategoryModel();

        // get the default caracteristics from protocol
        List<Caracteristic> protocolIndividualObservationCaracteristics = new ArrayList<>(dataContext.getDefaultIndividualObservationCaracteristics());

        Optional<CruiseCache> optionalCruiseCache = dataContext.getOptionalCruiseCache();
        if (!optionalCruiseCache.isPresent()) {
            throw new IllegalStateException("Can't find cruise cache");
        }

        TuttiProtocol protocol = dataContext.isProtocolFilled() ? dataContext.getProtocol() : null;

        SpeciesFrequencyUIModel model = new SpeciesFrequencyUIModel(speciesOrBenthosBatchUISupport,
                                                                    getConfig().getIndividualObservationWeightUnit(),
                                                                    sampleCategoryModel,
                                                                    sexCaracteristic,
                                                                    protocolIndividualObservationCaracteristics,
                                                                    optionalCruiseCache.get(),
                                                                    dataContext.getCruiseId(),
                                                                    protocol);

        this.ui.setContextValue(model);

        // listen when ichtyometer is connected or not and adjust the listener
        getContext().addPropertyChangeListener(TuttiUIContext.PROPERTY_ICHTYOMETER_CONNECTED, listenIchtyomerIsConnected);

        // listen when caliper is connected or not and adjust the listener
        getContext().addPropertyChangeListener(TuttiUIContext.PROPERTY_CALIPER_CONNECTED, listenCaliperIsConnected);

        // listen when fishing operation reloaded in data context to propagate it to our model
        getDataContext().addPropertyChangeListener(TuttiDataContext.PROPERTY_FISHING_OPERATION_ID, listenFishingOperationReloadInDataContext);

    }

    @Override
    public void afterInit(SpeciesFrequencyUI ui) {

        applySpeciesFrequencyRafaleAction = new ApplySpeciesFrequencyRafaleAction(ui);

        initUI(ui);

        List<Caracteristic> lengthStepCaracteristics = new ArrayList<>(getDataContext().getLengthStepCaracteristics());

        this.lengthStepCaracteristics = TuttiEntities.splitById(lengthStepCaracteristics);

        SpeciesFrequencyUIModel model = getModel();

        taxonCache = TaxonCaches.createSpeciesCacheWithoutVernacularCode(getPersistenceService(), getDataContext().getProtocol());

        Caracteristic modelCaracteristic = model.getLengthStepCaracteristic();
        initBeanFilterableComboBox(this.ui.getLengthStepCaracteristicComboBox(),
                                   lengthStepCaracteristics,
                                   modelCaracteristic);

        // get step from the pmfm
        model.setStep(modelCaracteristic);

        model.setMinStep(null);
        model.setMaxStep(null);

        ui.getRafaleStepField().getTextField().addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    Float step = (Float) SpeciesFrequencyUIHandler.this.ui.getRafaleStepField().getModel().getNumberValue();

                    applySpeciesFrequencyRafaleAction.applyRafaleStep(step, false);

                    //select text
                    JTextField field = (JTextField) e.getSource();
                    field.selectAll();
                }
            }
        });

        // when lengthStepCaracteristic changed, let's updates all row with the new value
        model.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_LENGTH_STEP_CARACTERISTIC, evt -> {
            Caracteristic newValue = (Caracteristic) evt.getNewValue();
            // get step from the pmfm
            model.setStep(newValue);

            if (CollectionUtils.isNotEmpty(getModel().getRows())) {
                for (SpeciesFrequencyRowModel rowModel : getModel().getRows()) {
                    rowModel.setLengthStepCaracteristic(newValue);
                    recomputeRowValidState(rowModel);
                }
            }

            if (!model.isInitBatchEdition()) {

                // on valide uniquement si on est pas en mode de chargement de l'écran
                SpeciesFrequencyUIHandler.this.ui.getValidator().doValidate();
            }

//            getObsTableModel().setLengthstepCaracteristic(newValue);
        });

        // when lengthStepCaracteristicUnit changed, let's updates the label of some fields
        model.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_LENGTH_STEP_CARACTERISTIC_UNIT, evt -> {

            String unit = (String) evt.getNewValue();

            if (unit == null) {

                unit = t("tutti.editSpeciesFrequencies.unkownStepUnit");
            }

            getUI().getMinStepLabel().setText(Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.field.minStep"), unit));
            getUI().getMinStepLabel().setToolTipText(Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.field.minStep.tip"), unit));

            getUI().getMaxStepLabel().setText(Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.field.maxStep"), unit));
            getUI().getMaxStepLabel().setToolTipText(Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.field.maxStep.tip"), unit));

            getUI().getRafaleStepLabel().setText(Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.field.rafaleStep"), unit));
            getUI().getRafaleStepLabel().setToolTipText(Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.field.rafaleStep.tip"), unit));

            TableColumnExt column = (TableColumnExt) getUI().getTable().getColumn(SpeciesFrequencyTableModel.LENGTH_STEP);
            String lengthStepLabelWithUnit = Units.getLabelWithUnit(t("tutti.editSpeciesFrequencies.table.header.lengthStep"), unit);
            column.setHeaderValue(lengthStepLabelWithUnit);
            column.setToolTipText(lengthStepLabelWithUnit);

            column = (TableColumnExt) getUI().getObsTable().getColumn(IndividualObservationBatchTableModel.SIZE);
            column.setHeaderValue(Units.getLabelWithUnit(t("tutti.editIndividualObservationBatch.table.header.size"), unit));
            column.setToolTipText(Units.getLabelWithUnit(t("tutti.editIndividualObservationBatch.table.header.size"), unit));

        });

        // when configuration mode change, let's focus the best component (see http://forge.codelutin.com/issues/4035)
        model.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_FREQUENCIES_CONFIGURATION_MODE, evt -> {
            FrequencyConfigurationMode newValue = (FrequencyConfigurationMode) evt.getNewValue();
            SwingUtilities.invokeLater(
                    () -> {
                        JComponent componentToFocus = getComponentToFocus(newValue);
                        if (componentToFocus != null) {
                            componentToFocus.grabFocus();
                        }
                        updateLogVisibility();
                    }
            );
        });

        // Pour bloquer le changement de copie des poids rtp s'il y a deja des poids
        model.addVetoableChangeListener(SpeciesFrequencyUIModel.PROPERTY_COPY_RTP_WEIGHTS, evt -> {

            boolean newCopyRtpWeights = (boolean) evt.getNewValue();

            if (newCopyRtpWeights) {

                long rowsWithUserData = model.getRows().stream().filter(SpeciesFrequencyRowModel::withWeight).count();

                if (rowsWithUserData > 0) {

                    String htmlMessage = String.format(
                            CONFIRMATION_FORMAT,
                            t("tutti.editSpeciesFrequencies.changeCopyRtpWeights.confirm.message"),
                            t("tutti.editSpeciesFrequencies.changeCopyRtpWeights.confirm.help"));
                    int i = JOptionPane.showConfirmDialog(
                            getTopestUI(),
                            htmlMessage,
                            t("tutti.editSpeciesFrequencies.changeCopyRtpWeights.confirm.title"),
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE);

                    if (i == JOptionPane.CANCEL_OPTION) {
                        throw new PropertyVetoException("The user does not want to erase his data.", evt);
                    }
                }
            }
        });

        model.addPropertyChangeListener(SpeciesFrequencyUIModel.PROPERTY_COPY_RTP_WEIGHTS, evt -> {
            SpeciesFrequencyUIModel source = (SpeciesFrequencyUIModel) evt.getSource();
            source.getRows().forEach(source::computeRowWeightWithRtp);
            source.reloadRows();
            source.getFrequencyTableModel().fireTableDataChanged();
        });

        // si le tableau des observations est en erreur, on recalcule si il existe une ligne non vide en erreur
        model.getIndividualObservationModel().addPropertyChangeListener(IndividualObservationBatchUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            IndividualObservationBatchUIModel source = (IndividualObservationBatchUIModel) evt.getSource();
            if (source.getRows() != null) {
                boolean nonEmptyRow = source.isNonEmptyRowInError();
                model.setNonEmptyIndividualObservationRowsInError(nonEmptyRow);
            }
        });

        // set the pattern to the weight in simple counting mode according to the weight unit

        ui.getSimpleCountingWeightField().setNumberPattern(weightUnit.getNumberEditorPattern());

        initDataTable();

        initLogTable();

        this.individualObservationBatchTableHandler = new IndividualObservationBatchTableHandler(ui);
        this.averageWeightsHistogramHandler = new AverageWeightsHistogramHandler(ui);
        this.frequenciesHistogramHandler = new FrequenciesHistogramHandler(ui);
        this.samplingNotificationZoneHandler = new SamplingNotificationZoneHandler(ui,
                                                                                   model.getIndividualObservationModel().getSamplingNotificationZoneModel(),
                                                                                   model.getIndividualObservationUICache());

        listenValidatorValid(ui.getValidator(), model);

    }

    @Override
    protected JComponent getComponentToFocus() {
        FrequencyConfigurationMode configurationMode = getModel().getConfigurationMode();
        JComponent componentToFocus = getComponentToFocus(configurationMode);
        if (componentToFocus == null) {
            componentToFocus = getUI().getLengthStepCaracteristicComboBox();
        }
        return componentToFocus;
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        getDataContext().removePropertyChangeListener(TuttiDataContext.PROPERTY_FISHING_OPERATION_ID, listenFishingOperationReloadInDataContext);

        TuttiUIContext context = getContext();
        context.removePropertyChangeListener(TuttiUIContext.PROPERTY_ICHTYOMETER_CONNECTED, listenIchtyomerIsConnected);
        context.removePropertyChangeListener(TuttiUIContext.PROPERTY_CALIPER_CONNECTED, listenCaliperIsConnected);

        if (context.isIchtyometerConnected()) {

            context.getIchtyometerReader().removeFeedModeReaderListener(ichtyometerFeedReaderListener);
        }

        if (context.isCaliperConnected()) {

            context.getCaliperReader().removeFeedModeReaderListener(caliperFeedReaderListener);
        }

        SpeciesFrequencyUIModel model = getModel();

        IOUtils.closeQuietly(individualObservationBatchTableHandler);
        IOUtils.closeQuietly(averageWeightsHistogramHandler);
        IOUtils.closeQuietly(frequenciesHistogramHandler);
        IOUtils.closeQuietly(samplingNotificationZoneHandler);

        frequencyEditor = null;

        // evict model from validator
        ui.getValidator().setBean(null);
        clearValidators();

        // when canceling always invalid model (in that way)
        model.setValid(false);
        model.setSimpleCount(null);
        model.setModify(false);

        EditSpeciesBatchPanelUI parent = getParentContainer(EditSpeciesBatchPanelUI.class);
        parent.switchToEditBatch();

    }

    public void editBatch(FrequencyCellEditor editor, String title) {

        SpeciesBatchRowModel speciesBatch = editor.getEditRow();
        Objects.requireNonNull(speciesBatch, "Impossible d'éditer un lot non renseigné");

        SpeciesFrequencyUIModel model = getModel();

        Objects.requireNonNull(title, "title can't be null here ?!");

        model.getAverageWeightsHistogramModel().setTitle(title);
        model.getFrequenciesHistogramModel().setTitle(title);

        frequencyEditor = editor;

        model.setNextEditableRowIndex(frequencyEditor.getNextEditableRowIndex());

        List<SpeciesFrequencyRowModel> frequency = speciesBatch.getFrequency();
        List<IndividualObservationBatchRowModel> individualObservations = speciesBatch.getIndividualObservation();

        // keep batch (will be used to push back editing entry)
        model.setBatch(speciesBatch);
        model.setFishingOperation(getDataContext().getFishingOperation());
        model.setMinStep(null);
        model.setMaxStep(null);
        model.setRtp(null);
        model.setCopyRtpWeights(false);

        // get species from protocol
        SpeciesProtocol speciesProtocol = getDataContext().isProtocolFilled() ? speciesOrBenthosBatchUISupport.getSpeciesProtocol(speciesBatch.getSpecies()) : null;

        // set rtp
        Rtp rtp = null;

        if (speciesProtocol != null) {

            Caracteristic sexCaracteristic = model.getIndividualObservationModel().getSexCaracteristic();
            CaracteristicQualitativeValue sampleCategoryValue = (CaracteristicQualitativeValue) speciesBatch.getSampleCategoryValue(sexCaracteristic.getIdAsInt());

            if (sampleCategoryValue != null) {

                if (Sexs.isMale(sampleCategoryValue)) {
                    rtp = speciesProtocol.getRtpMale();
                } else if (Sexs.isFemale(sampleCategoryValue)) {
                    rtp = speciesProtocol.getRtpFemale();
                } else {
                    rtp = speciesProtocol.getRtpUndefined();
                }

            } else {
                rtp = speciesProtocol.getRtpUndefined();
            }

        }

        model.setRtp(rtp);

        individualObservationBatchTableHandler.initMaturityCaracteristic(speciesProtocol);
        individualObservationBatchTableHandler.initDefaultCaracteristics(speciesBatch);

        model.getIndividualObservationUICache().initFishingOperation(model.getFishingOperation());

        loadFrequenciesAndObservations(frequency, individualObservations, false);

        samplingNotificationZoneHandler.editBatch(speciesBatch);

        if (getContext().isIchtyometerConnected()) {

            // let's listen the ichtyometer
            listenItchtyometer();

        }

        if (getContext().isCaliperConnected()) {

            // let's listen the caliper
            listenCaliper();

        }

        model.setModify(false);

    }

    public boolean leaveIfConfirmed() {

        boolean result = !getModel().isModify() || askCancelEditBeforeLeaving();

        if (result) {

            // Cancel edit screen
            ApplicationUIAction action = (ApplicationUIAction) getUI().getCancelButton().getAction();
            getContext().getActionEngine().runInternalAction(action.getLogicAction());

        } else {

            // Use cancel this operation
            if (log.isInfoEnabled()) {
                log.info("Use cancel change on tab, stay on frequencies screen.");
            }

        }

        return result;

    }

    public boolean askCancelEditBeforeLeaving() {
        // Ask confirmation to quit screen
        String htmlMessage = String.format(AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                                           t("tutti.askToCancelEditFrequencies.message"),
                                           t("tutti.askToCancelEditFrequencies.help"));

        int saveResponse = JOptionPane.showOptionDialog(getTopestUI(),
                                                        htmlMessage,
                                                        t("tutti.askToCancelEditFrequencies.title"),
                                                        JOptionPane.OK_CANCEL_OPTION,
                                                        JOptionPane.QUESTION_MESSAGE,
                                                        null,
                                                        new String[]{t("tutti.option.continue"), t("tutti.option.cancel")},
                                                        t("tutti.option.cancel"));

        return saveResponse == 0;
    }

    public void setCopyIndividualObservationMode(CopyIndividualObservationMode newCopyMode) {

        SpeciesFrequencyUIModel model = getModel();

        if (model.isInitBatchEdition()) {

            if (log.isInfoEnabled()) {
                log.info("Skip ask user to confirm copyIndividualObservationMode changed from " + model.getCopyIndividualObservationMode() + " to " + newCopyMode);
            }
            return;
        }

        // le seul mode où l'utilisateur ne peut rien saisir est le mode tout
        if (!model.isCopyIndividualObservationAll()) {

            long rowsWithUserData;

            // si on etait en mode taille et que l'utilisateur avait saisi des tailles
            if (model.isCopyIndividualObservationSize()) {
                rowsWithUserData = model.getRows().stream()
                                        .filter(SpeciesFrequencyRowModel::withWeight)
                                        .count();

            } else {
                rowsWithUserData = model.getRows().stream()
                                        .filter(row -> row.withLengthStep() || row.withNumber() || row.withWeight())
                                        .count();
            }

            if (rowsWithUserData > 0) {

                String htmlMessage = String.format(
                        CONFIRMATION_FORMAT,
                        t("tutti.editSpeciesFrequencies.changeCopyMode.confirm.message"),
                        t("tutti.editSpeciesFrequencies.changeCopyMode.confirm.help"));
                int i = JOptionPane.showConfirmDialog(
                        ui.getHandler().getTopestUI(),
                        htmlMessage,
                        t("tutti.editSpeciesFrequencies.changeCopyMode.confirm.title"),
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE);

                if (i == JOptionPane.CANCEL_OPTION) {
                    if (log.isDebugEnabled()) {
                        log.debug("User cancel modification...");
                    }

                    // on repositionne sur le bon radio-bouton
                    // le code n'est pas optimal mais est moins dangeureux que de relancer des fires je pense
                    switch (model.getCopyIndividualObservationMode()) {
                        case ALL:
                            ui.getCopyAllButton().setSelected(true);
                            break;
                        case NOTHING:
                            ui.getCopyNothingButton().setSelected(true);
                            break;
                        case SIZE:
                            ui.getCopySizesButton().setSelected(true);
                            break;
                    }

                    return;
                }
            }

        }

        model.setCopyIndividualObservationMode(newCopyMode);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void initDataTable() {
        JXTable table = getTable();

        // create table column model
        DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        { // LengthStep

            addFloatColumnToModel(columnModel,
                                  SpeciesFrequencyTableModel.LENGTH_STEP,
                                  TuttiUI.DECIMAL1_PATTERN,
                                  table);
        }

        { // Number

            addIntegerColumnToModel(columnModel,
                                    SpeciesFrequencyTableModel.NUMBER,
                                    TuttiUI.INT_6_DIGITS_PATTERN,
                                    table);
        }

        { // Weight
            addFloatColumnToModel(columnModel, SpeciesFrequencyTableModel.WEIGHT, weightUnit, table);
        }

        { // RTP computed Weight

            Color computedDataColor = getConfig().getColorComputedWeights();
            TableCellRenderer renderer = new TableCellRenderer() {

                TableCellRenderer delegate = newWeightCellRenderer(table.getDefaultRenderer(Number.class), weightUnit);

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    Component result = delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    if (result instanceof JLabel) {
                        JLabel jLabel = (JLabel) result;
                        jLabel.setForeground(computedDataColor);
                        jLabel.setFont(jLabel.getFont().deriveFont(Font.ITALIC));
                    }
                    return result;
                }
            };

            addColumnToModel(columnModel,
                             null,
                             renderer,
                             SpeciesFrequencyTableModel.RTP_COMPUTED_WEIGHT,
                             weightUnit);
        }

        // create table model
        SpeciesFrequencyTableModel tableModel = new SpeciesFrequencyTableModel(weightUnit,
                                                                               getConfig().getIndividualObservationWeightUnit(),
                                                                               columnModel,
                                                                               getModel());
        getModel().setFrequencyTableModel(tableModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        initTable(table);

        installTableKeyListener(columnModel, table);
    }

    @Override
    protected void addHighlighters(final JXTable table) {

        super.addHighlighters(table);

        HighlightPredicate notSelectedPredicate = new HighlightPredicate.NotHighlightPredicate(HighlightPredicate.IS_SELECTED);
        HighlightPredicate weightTooDifferentFromRtpPredicate = (Component renderer, ComponentAdapter adapter) -> {

            boolean result = false;
            if (table.getModel() instanceof SpeciesFrequencyTableModel) {

                SpeciesFrequencyTableModel tableModel = (SpeciesFrequencyTableModel) table.getModel();
                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                SpeciesFrequencyRowModel row = tableModel.getEntry(modelRow);

                Float rate = getConfig().getDifferenceRateBetweenWeightAndRtpWeight();
                result = row.withWeight() && row.withRtpComputedWeight()
                        && Math.abs(row.getWeight() - row.getRtpComputedWeight()) >= row.getWeight() * rate / 100;
            }

            return result;
        };

        // paint in a special color rows with weight in warning (not selected)
        Highlighter weightTooDifferentFromRtpHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        notSelectedPredicate,
                        weightTooDifferentFromRtpPredicate),
                getConfig().getColorWarningRow());
        table.addHighlighter(weightTooDifferentFromRtpHighlighter);

        // paint in a special color rows with weight in warning (selected)
        Highlighter weightTooDifferentFromRtpSelectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.IS_SELECTED,
                        weightTooDifferentFromRtpPredicate),
                getConfig().getColorWarningRow().darker());
        table.addHighlighter(weightTooDifferentFromRtpSelectedHighlighter);

    }

    protected void initLogTable() {
        JXTable logTable = ui.getLogsTable();

        // create log table column model
        DefaultTableColumnModelExt logColumnModel = new DefaultTableColumnModelExt();

        { // Date
            addColumnToModel(logColumnModel,
                             SpeciesFrequencyLogCellComponent.newEditor(ui),
                             SpeciesFrequencyLogCellComponent.newRender(),
                             SpeciesFrequencyLogsTableModel.LABEL);
        }

        // create log table model
        SpeciesFrequencyLogsTableModel logTableModel = new SpeciesFrequencyLogsTableModel(logColumnModel);
        logTableModel.setRows(new ArrayList<>());

        logTable.setModel(logTableModel);
        logTable.setColumnModel(logColumnModel);

        // by default do not authorize to change column orders
        logTable.getTableHeader().setReorderingAllowed(false);
        Highlighter evenHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                HighlightPredicate.ODD,
                getConfig().getColorAlternateRow());
        logTable.addHighlighter(evenHighlighter);
    }

    protected CaracteristicColumnIdentifier<IndividualObservationBatchRowModel> addCaracteristicColumnToModel(JXTable table,
                                                                                                              TableColumnModelExt columnModel,
                                                                                                              Caracteristic caracteristic) {

        String header = caracteristicDecorator.toString(caracteristic);
        String headerTip = caracteristicTipDecorator.toString(caracteristic);

        CaracteristicColumnIdentifier<IndividualObservationBatchRowModel> id = CaracteristicColumnIdentifier.newCaracteristicId(
                caracteristic,
                IndividualObservationBatchRowModel.PROPERTY_DEFAULT_CARACTERISTICS,
                header,
                headerTip
        );

        if (columnModel.getColumnExt(id) == null) {

            switch (caracteristic.getCaracteristicType()) {

                case NUMBER:

                    addFloatColumnToModel(columnModel,
                                          id,
                                          TuttiUI.DECIMAL3_PATTERN,
                                          table);

                    break;
                case QUALITATIVE:
                    List<CaracteristicQualitativeValue> values = caracteristic.getQualitativeValue();
                    addComboDataColumnToModel(columnModel,
                                              id,
                                              caracteristicQualitativeDecorator,
                                              values);
                    break;
                case TEXT:

                    addColumnToModel(columnModel, id);

                    break;
            }
        }

        return id;
    }

    // override la méthode pour mettre en index de modèle l'index après la dernière colonne cachée
    @Override
    protected <R> TableColumnExt addColumnToModel(TableColumnModel model,
                                                  TableCellEditor editor,
                                                  TableCellRenderer renderer,
                                                  ColumnIdentifier<R> identifier) {

        //TODO à remonter dans jaxx
        TableColumnExt col = new TableColumnExt(((DefaultTableColumnModelExt) model).getColumnCount(true));
        col.setCellEditor(editor);
        col.setCellRenderer(renderer);
        String label = t(identifier.getHeaderI18nKey());

        col.setHeaderValue(label);
        String tip = t(identifier.getHeaderTipI18nKey());

        col.setToolTipText(tip);

        col.setIdentifier(identifier);
        model.addColumn(col);
        // by default no column is sortable, must specify it
        col.setSortable(false);
        return col;
    }

    protected void consumeIchtyometerFeedRecord(IchtyometerFeedReaderMeasureRecord record) {
        if (record.isValid()) {

            float length = getModel().convertFromMm(record.getMeasure());
//            String unit = getModel().getLengthStepCaracteristicUnit();

//            // board measurements are in mm
//
//            float length;
//
//            if ("mm".equals(unit)) {
//
//                // measurement in mm asked
//                length = record.getMeasure();
//
//            } else {
//
//                // measurement in cm asked
//                length = record.getMeasure() / 10f;
//
//            }

            applySpeciesFrequencyRafaleAction.applyRafaleStep(length, true);

        } else {

            soundEngine.beepOnExternalDeviceErrorReception();

            throw new ApplicationBusinessException(t("tutti.editSpeciesFrequencies.error.itchyometer.bad.record", record.getRecord()));
        }
    }

    protected void consumeCaliperFeedRecord(CaliperFeedReaderMeasureRecord record) {
        if (record.isValid()) {

            float length = getModel().convertFromMm(record.getMeasure());

//            String unit = getModel().getLengthStepCaracteristicUnit();
//
//            // board measurements are in mm
//
//            float length;
//
//            if ("mm".equals(unit)) {
//
//                // measurement in mm asked
//                length = record.getMeasure();
//
//            } else {
//
//                // measurement in cm asked
//                length = record.getMeasure() / 10f;
//
//            }

            applySpeciesFrequencyRafaleAction.applyRafaleStep(length, true);

        } else {

            soundEngine.beepOnExternalDeviceErrorReception();

            throw new ApplicationBusinessException(t("tutti.editSpeciesFrequencies.error.caliper.bad.record", record.getRecord()));
        }
    }

    protected void listenItchtyometer() {

        // always remove the listener before adding it to be sure it will not be there twice
        getContext().getIchtyometerReader().removeAllFeedModeReaderListeners();
        if (log.isInfoEnabled()) {
            log.info("Start listen ichtyometer");
        }
        getContext().getIchtyometerReader().addFeedModeReaderListener(ichtyometerFeedReaderListener);
    }

    protected void listenCaliper() {

        // always remove the listener before adding it to be sure it will not be there twice
        getContext().getCaliperReader().removeAllFeedModeReaderListeners();
        if (log.isInfoEnabled()) {
            log.info("Start listen caliper");
        }
        getContext().getCaliperReader().addFeedModeReaderListener(caliperFeedReaderListener);
    }

    protected JComponent getComponentToFocus(FrequencyConfigurationMode mode) {
        JComponent componentToFocus = null;
        if (mode != null) {
            boolean withLengthStepCaracteristic =
                    getModel().getLengthStepCaracteristic() != null;
            switch (mode) {
                case AUTO_GEN:
                    if (withLengthStepCaracteristic) {

                        componentToFocus = ui.getMinStepField();
                    } else {
                        componentToFocus = ui.getLengthStepCaracteristicComboBox();
                    }
                    break;
                case RAFALE:

                    if (withLengthStepCaracteristic) {

                        componentToFocus = ui.getRafaleStepField();
                    } else {
                        componentToFocus = ui.getLengthStepCaracteristicComboBox();
                    }

                    break;
                case SIMPLE_COUNTING:
                    componentToFocus = ui.getSimpleCountingNumberField();
                    break;
                default:
                    componentToFocus = null;
            }
        }
        return componentToFocus;
    }

    protected void updateLogVisibility() {

        boolean logVisible = getModel().isRafaleMode() || getContext().isIchtyometerConnected() || getContext().isCaliperConnected();
        JSplitPane firstSplitPane = ui.getFirstSplitPane();
        JSplitPane secondSplitPane = ui.getSecondSplitPane();

        int lastDividerLocation = secondSplitPane.getLastDividerLocation();
        if (lastDividerLocation == 0) {
            lastDividerLocation = 200;
        }
        secondSplitPane.setDividerLocation(logVisible ? lastDividerLocation : 0);
        secondSplitPane.setDividerSize(logVisible ? firstSplitPane.getDividerSize() : 0);

        ui.getLogsScrollPane().setVisible(logVisible);

    }

    @Override
    public <E> void initBeanFilterableComboBox(BeanFilterableComboBox<E> comboBox, List<E> data, E selectedData) {
        super.initBeanFilterableComboBox(comboBox, data, selectedData);
    }

    public FrequencyCellEditor getFrequencyEditor() {
        return frequencyEditor;
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {
        super.beforeOpenPopup(modelRowIndex, modelColumnIndex);

        boolean sampleCodeMenusEnabled = individualObservationBatchTableHandler.isSampleCodeMenusEnabled(modelRowIndex);

        ui.getEditSampleCodeMenu().setEnabled(sampleCodeMenusEnabled);
        ui.getDeleteSampleCodeMenu().setEnabled(sampleCodeMenusEnabled);
    }

    public void loadFrequenciesAndObservations(List<SpeciesFrequencyRowModel> frequency, List<IndividualObservationBatchRowModel> individualObservations, boolean addToCache) {

        SpeciesFrequencyUIModel model = getModel();
        SpeciesBatchRowModel speciesBatch = model.getBatch();

        model.setInitBatchEdition(true);

        try {

            model.loadSpeciesBatch(speciesBatch);

            Species species = speciesBatch.getSpecies();

            List<SpeciesFrequencyRowModel> frequencyRows = getTableModel().loadRows(frequency);

            List<IndividualObservationBatchRowModel> individualObservationRows = individualObservationBatchTableHandler.loadRows(species, individualObservations);

            if (log.isDebugEnabled()) {
                log.debug("Will edit batch row: " + speciesBatch + " with " + frequencyRows.size() + " frequencies and " + individualObservationRows.size() + " individual observations.");
            }

            CopyIndividualObservationMode copyIndividualObservationMode = computeCopyIndividualObservationMode(individualObservationRows);

            if (log.isInfoEnabled()) {
                log.info("copyIndividualObservationMode: " + copyIndividualObservationMode);
            }

            Caracteristic lengthStepCaracteristic = computeLengthStepCaracteristic(species, frequencyRows, individualObservationRows);

            if (log.isInfoEnabled()) {
                log.info("lengthStepCaracteristic: " + lengthStepCaracteristic);
            }

//            model.setLengthStepCaracteristic(null);
            model.setLengthStepCaracteristic(lengthStepCaracteristic);

            FrequencyConfigurationMode configurationMode = model.guessFrequencyConfigurationMode();

            if (log.isInfoEnabled()) {
                log.info("configurationMode: " + configurationMode);
            }

            // make sure configuration mode will be rebound
            model.setConfigurationMode(null);
            model.setConfigurationMode(configurationMode);

            model.setFrequenciesConfigurationMode(null);
            model.setFrequenciesConfigurationMode(FrequencyConfigurationMode.AUTO_GEN);

            // connect model to validator
            ui.getValidator().setBean(model);

            model.setRows(frequencyRows);

            // let's change the copy mode (mark it in init mode to avoid user change confirmation and some recomputations)
            model.setCopyIndividualObservationMode(null);
            model.setCopyIndividualObservationMode(copyIndividualObservationMode);

            // chargement des observations après le changement de mode de copie, pour que la validation des lignes prenne en compte le nouveau mode de copie
            individualObservationBatchTableHandler.loadSpeciesBatch(speciesBatch, individualObservationRows, addToCache);

            model.computeRowWeightWithRtp();

        } finally {

            model.setInitBatchEdition(false);

        }
    }

    // Attention on surcharge les méthodes suivantes pour pouvoir les utiliser dans l'autre handler, ne rien changer (pour le moment)...

    @Override
    protected <R> TableColumnExt addIntegerColumnToModel(TableColumnModel model,
                                                         ColumnIdentifier<R> identifier,
                                                         String numberPattern,
                                                         JTable table) {
        return super.addIntegerColumnToModel(model, identifier, numberPattern, table);
    }

    @Override
    protected <R> TableColumnExt addFloatColumnToModel(TableColumnModel model,
                                                       ColumnIdentifier<R> identifier,
                                                       String numberPattern,
                                                       JTable table) {
        return super.addFloatColumnToModel(model, identifier, numberPattern, table);
    }

    @Override
    protected <R> TableColumnExt addFloatColumnToModel(TableColumnModel model, ColumnIdentifier<R> identifier, WeightUnit weightUnit, JTable table) {
        return super.addFloatColumnToModel(model, identifier, weightUnit, table);
    }

    @Override
    protected <R, B> TableColumnExt addComboDataColumnToModel(TableColumnModel model,
                                                              ColumnIdentifier<R> identifier,
                                                              Decorator<B> decorator,
                                                              List<B> data) {
        return super.addComboDataColumnToModel(model, identifier, decorator, data);
    }

    @Override
    protected void installTableKeyListener(TableColumnModel columnModel, JTable table, boolean enterToChangeRow) {
        super.installTableKeyListener(columnModel, table, enterToChangeRow);
    }

    @Override
    protected String decorate(Serializable object, String context) {
        return super.decorate(object, context);
    }

    private CopyIndividualObservationMode computeCopyIndividualObservationMode(List<IndividualObservationBatchRowModel> individualObservationRows) {

        CopyIndividualObservationMode copyIndividualObservationMode;
        if (individualObservationRows.isEmpty()) {

            copyIndividualObservationMode = CopyIndividualObservationMode.NOTHING;

        } else {

            IndividualObservationBatchRowModel firstIndividualObservationRow = individualObservationRows.get(0);
            copyIndividualObservationMode = firstIndividualObservationRow.getCopyIndividualObservationMode();

        }
        return copyIndividualObservationMode;

    }

    private Caracteristic computeLengthStepCaracteristic(Species species, List<SpeciesFrequencyRowModel> frequencyRows, List<IndividualObservationBatchRowModel> individualObservationRows) {

        Caracteristic lengthStepCaracteristic = null;

        if (!frequencyRows.isEmpty()) {

            SpeciesFrequencyRowModel firstFrequencyRow = frequencyRows.get(0);
            lengthStepCaracteristic = firstFrequencyRow.getLengthStepCaracteristic();
            if (log.isInfoEnabled()) {
                log.info("Use existing lengthStep caracteristic / step from first existing frequency: " + decorate(lengthStepCaracteristic));
            }

        }

        if (lengthStepCaracteristic == null) {

            if (!individualObservationRows.isEmpty()) {
                IndividualObservationBatchRowModel firstIndividualObservationRow = individualObservationRows.get(0);

                lengthStepCaracteristic = firstIndividualObservationRow.getLengthStepCaracteristic();

                if (log.isInfoEnabled()) {
                    log.info("Use existing lengthStep caracteristic / step from first individual observation : " + decorate(lengthStepCaracteristic));
                }
            }

        }

        SpeciesBatchRowModel previousSiblingRow = frequencyEditor.getPreviousSiblingRow();

        if (lengthStepCaracteristic == null && previousSiblingRow != null) {

            // try to get it from his previous brother row
            List<SpeciesFrequencyRowModel> previousFrequency = previousSiblingRow.getFrequency();

            if (CollectionUtils.isNotEmpty(previousFrequency)) {

                // use the first frequency length step caracteristic / step
                SpeciesFrequencyRowModel rowModel = previousFrequency.get(0);
                lengthStepCaracteristic = rowModel.getLengthStepCaracteristic();
                if (log.isInfoEnabled()) {
                    log.info("Use previous sibling existing lengthStep caracteristic / step " + decorate(lengthStepCaracteristic));
                }
            }
        }

        if (lengthStepCaracteristic == null) {

            if (taxonCache.containsLengthStepPmfmId(species)) {

                String lengthStepPmfmId = taxonCache.getLengthStepPmfmId(species);

                lengthStepCaracteristic = lengthStepCaracteristics.get(lengthStepPmfmId);

                if (log.isInfoEnabled()) {
                    log.info("Use existing from protocol lengthStep caracteristic / step " + decorate(lengthStepCaracteristic));
                }
            }

        }

        return lengthStepCaracteristic;

    }

}
