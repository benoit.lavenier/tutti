package fr.ifremer.tutti.ui.swing.content.validation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.ui.swing.content.validation.actions.EditCruiseInValidationUIAction;
import fr.ifremer.tutti.ui.swing.content.validation.actions.EditFishingOperationInValidationUIAction;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUI;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.validation.tree.CruiseTreeNode;
import fr.ifremer.tutti.ui.swing.content.validation.tree.OperationTreeNode;
import fr.ifremer.tutti.ui.swing.content.validation.tree.ValidationTreeCellRenderer;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.RemoveablePropertyChangeListener;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUIModel;
import org.nuiton.jaxx.application.swing.util.CloseableUI;
import org.nuiton.validator.NuitonValidatorResult;

import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class ValidateCruiseUIHandler extends AbstractTuttiUIHandler<ValidateCruiseUIModel, ValidateCruiseUI> implements CloseableUI {

    private final static Log log = LogFactory.getLog(ValidateCruiseUIHandler.class);

    protected EditFishingOperationInValidationUIAction editFishingOperationAction;

    protected EditCruiseInValidationUIAction editCruiseAction;

    @Override
    public void beforeInit(ValidateCruiseUI ui) {

        super.beforeInit(ui);

        ValidateCruiseUIModel model = new ValidateCruiseUIModel();
        model.setCruise(getDataContext().getCruise());
        ProgressionModel progressionModel = new ProgressionModel();
        ApplicationActionUIModel actionUIModel = getContext().getActionUI().getModel();
        actionUIModel.setProgressionModel(progressionModel);

        Integer cruiseId = getDataContext().getCruiseId();
        List<Integer> operationIds = getPersistenceService().getAllFishingOperationIds(cruiseId);
        progressionModel.setTotal(2 * operationIds.size() + 2);

        NuitonValidatorResult cruiseValidatorResult = getValidateCruiseOperationsService().validateCruise(progressionModel, cruiseId);
        model.setCruiseValidatorResult(cruiseValidatorResult);

        LinkedHashMap<FishingOperation, NuitonValidatorResult> validator = getValidateCruiseOperationsService().validateOperations(progressionModel, operationIds);
        model.addFishingOperationValidatorResults(validator);

        actionUIModel.setProgressionModel(null);

        model.addPropertyChangeListener(ValidateCruiseUIModel.PROPERTY_SELECTED_FISHING_OPERATION,
                                        new RemoveablePropertyChangeListener() {
                                            @Override
                                            public void propertyChange(PropertyChangeEvent evt) {
                                                FishingOperation operation = (FishingOperation) evt.getNewValue();
                                                editFishingOperationAction.setFishingOperation(operation);
                                                getContext().getActionEngine().runAction(editFishingOperationAction);
                                                getUI().getEditPanelLayout().setSelected("fishingOperation");
                                            }
                                        }
        );
        model.addPropertyChangeListener(ValidateCruiseUIModel.PROPERTY_SELECTED_CRUISE,
                                        new RemoveablePropertyChangeListener() {
                                            @Override
                                            public void propertyChange(PropertyChangeEvent evt) {
                                                Cruise cruise = (Cruise) evt.getNewValue();

                                                editCruiseAction.setCruise(cruise);
                                                getContext().getActionEngine().runAction(editCruiseAction);
                                                getUI().getEditPanelLayout().setSelected("cruise");
                                            }
                                        }
        );
        this.ui.setContextValue(model);
    }

    @Override
    public void afterInit(ValidateCruiseUI ui) {
        initUI(ui);

        editFishingOperationAction = getContext().getActionFactory().createLogicAction(this, EditFishingOperationInValidationUIAction.class);
        editCruiseAction = getContext().getActionFactory().createLogicAction(this, EditCruiseInValidationUIAction.class);

        ui.getOperationPanel().getModel().setSelectedFishingOperation(null);


        ValidateCruiseUIModel uiModel = getModel();

        DefaultMutableTreeNode root = new DefaultMutableTreeNode();

        {
            // add cruise node
            NuitonValidatorResult result = uiModel.getCruiseValidatorResult();
            CruiseTreeNode node = new CruiseTreeNode(uiModel.getCruise(), result);
            root.add(node);
        }
        {
            // add operations node
            for (FishingOperation operation : uiModel.getFishingOperations()) {
                NuitonValidatorResult result = uiModel.getValidatorResult(operation);
                OperationTreeNode node = new OperationTreeNode(operation, result);
                root.add(node);
            }
        }
        TreeModel model = new DefaultTreeModel(root);

        JTree navigation = ui.getNavigation();
        navigation.setModel(model);

        Decorator<Cruise> cruiseDecorator = getDecorator(Cruise.class, null);
        Decorator<FishingOperation> fishingOperationDecorator = getDecorator(FishingOperation.class, null);
        navigation.setCellRenderer(new ValidationTreeCellRenderer(cruiseDecorator, fishingOperationDecorator));
        SwingUtil.expandTree(navigation);
        SwingUtil.addExpandOnClickListener(navigation);

        FishingOperationsUI operationPanel = ui.getOperationPanel();
        operationPanel.getTopPanel().setVisible(false);

        EditCruiseUI cruisePanel = ui.getCruisePanel();
        cruisePanel.remove(cruisePanel.getTopToolBar());
        cruisePanel.getCloseButton().getParent().remove(cruisePanel.getCloseButton());
        cruisePanel.getResetButton().setIcon(cruisePanel.getCloseButton().getIcon());

        getModel().computeReadyToSynch();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();
    }

    @Override
    public SwingValidator<ValidateCruiseUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    public void autoSelectNode(MouseEvent e, JPopupMenu popup) {
        JTree navigation = (JTree) e.getSource();

        boolean rightClick = SwingUtilities.isRightMouseButton(e);

        if (rightClick) {

            // get the path at this point
            TreePath path = navigation.getPathForLocation(e.getX(), e.getY());

            // select row (could empty selection)
            if (path == null) {
                navigation.clearSelection();

            } else {
                navigation.setSelectionPath(path);
            }

            // on right click show popup
            popup.show(navigation, e.getX(), e.getY());

        }

        TreePath selectionPath = navigation.getSelectionPath();

        if (selectionPath != null && selectionPath.getPathCount() > 1) {
            Object o = selectionPath.getPathComponent(1);

            if (o instanceof OperationTreeNode) {
                FishingOperation operation = ((OperationTreeNode) o).getUserObject();

                if (!Objects.equals(operation, getModel().getLastSelectedObject())) {

                    getModel().setSelectedFishingOperation(operation);
                }
            }
            if (o instanceof CruiseTreeNode) {
                Cruise cruise = ((CruiseTreeNode) o).getUserObject();
                if (!Objects.equals(cruise, getModel().getLastSelectedObject())) {

                    getModel().setSelectedCruise(cruise);
                }

            }

        }

    }

    public void openNodeMenu(KeyEvent e, JPopupMenu popup) {

        if (e.getKeyCode() == KeyEvent.VK_CONTEXT_MENU) {

            JTree navigation = (JTree) e.getSource();

            TreePath path = navigation.getSelectionPath();

            if (path != null) {
                Rectangle r = navigation.getPathBounds(path);

                // get the point in the middle lower of the node
                Point p = new Point(r.x + r.width / 2, r.y + r.height);

                popup.show(navigation, p.x, p.y);
            }
        }
    }

    public void updateCurrentOperationNode(NuitonValidatorResult validationResult) {
        JTree navigation = ui.getNavigation();
        TreePath selectionPath = navigation.getSelectionPath();
        Preconditions.checkArgument(selectionPath.getPathCount() > 1);

        OperationTreeNode node = (OperationTreeNode) selectionPath.getPathComponent(1);
        node.removeAllChildren();
        node.createChildren(validationResult);

        DefaultTreeModel treeModel = (DefaultTreeModel) navigation.getModel();
        treeModel.reload(node);
        navigation.setSelectionPath(selectionPath);
    }

    public void updateCurrentCruiseNode(NuitonValidatorResult validationResult) {
        JTree navigation = ui.getNavigation();
        TreePath selectionPath = navigation.getSelectionPath();
        Preconditions.checkArgument(selectionPath.getPathCount() > 1);

        CruiseTreeNode node = (CruiseTreeNode) selectionPath.getPathComponent(1);
        node.removeAllChildren();
        node.createChildren(validationResult);

        DefaultTreeModel treeModel = (DefaultTreeModel) navigation.getModel();
        treeModel.reload(node);
        navigation.setSelectionPath(selectionPath);
    }

    @Override
    public boolean quitUI() {
        editFishingOperationAction.removeListener();
        editCruiseAction.removeListener();
        return true;
    }
}
