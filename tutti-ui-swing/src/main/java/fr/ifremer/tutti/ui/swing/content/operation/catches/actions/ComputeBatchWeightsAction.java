package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.catches.BenthosWeightComputingException;
import fr.ifremer.tutti.service.catches.CatchWeightComputingException;
import fr.ifremer.tutti.service.catches.MarineLitterWeightComputingException;
import fr.ifremer.tutti.service.catches.SpeciesWeightComputingException;
import fr.ifremer.tutti.service.catches.TuttiWeightComputingException;
import fr.ifremer.tutti.service.catches.WeightCleaningService;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SampleCategoryColumnIdentifier;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchNaturalOrderComparator;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesSortMode;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.util.Numbers;
import jaxx.runtime.swing.JTables;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import javax.swing.JOptionPane;
import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class ComputeBatchWeightsAction extends LongActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {
    
    protected final WeightCleaningService cleaningService;
    protected final PersistenceService persistenceService;

    protected List<SpeciesBatchRowModel> speciesBatchRows;
    protected List<SpeciesBatchRowModel> benthosBatchRows;

    protected CatchBatch catchBatch;

    protected Boolean modified;
    protected Integer tabInError;

    public ComputeBatchWeightsAction(EditCatchesUIHandler handler) {
        super(handler, true);
        this.cleaningService = getContext().getWeightCleaningService();
        this.persistenceService = getContext().getPersistenceService();
    }

    @Override
    public boolean prepareAction() throws Exception {

        // Clean internal states

        speciesBatchRows = null;
        benthosBatchRows = null;
        catchBatch = null;
        modified = null;
        tabInError = null;

        boolean doAction = super.prepareAction();
        if (doAction) {

            // do a check of double weights
            FishingOperation fishingOperation = getModel().getFishingOperation();
            Multimap<String, String> errors = cleaningService.checkFishingOperation(fishingOperation.getIdAsInt());

            if (errors.isEmpty()) {

                // no errors

                sendMessage(t("tutti.editCatchBatch.action.computeWeights.no.double.weight.detected"));
            } else {

                // show errors to user as warning

                String errorsStr = cleaningService.errorsToString(errors);
                String htmlMessage = t("tutti.editCatchBatch.action.computeWeights.double.weight.detected", errorsStr);

                JOptionPane.showMessageDialog(getContext().getActionUI(),
                                              htmlMessage,
                                              t("tutti.editCatchBatch.action.computeWeights.double.weight.detected.title"),
                                              JOptionPane.WARNING_MESSAGE);


            }
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {

        WeightComputingService computingService = getContext().getWeightComputingService();

        EditCatchesUIModel model = getModel();
        Integer operationId = model.getFishingOperation().getIdAsInt();

        // ---------
        // Compute species batches
        // ---------

        BatchContainer<SpeciesBatch> computedSpeciesBatches = computingService.getComputedSpeciesBatches(operationId);

        Float totalSpeciesSortedWeight;
        if (computedSpeciesBatches == null) {
            totalSpeciesSortedWeight = 0f;
        } else {
            totalSpeciesSortedWeight = computeSpeciesBatches(getSpeciesBatchUI(), computedSpeciesBatches, speciesBatchRows = new ArrayList<>());
        }

        // ---------
        // Compute benthos batches
        // ---------

        BatchContainer<SpeciesBatch> computedBenthosBatches = computingService.getComputedBenthosBatches(operationId);

        Float totalBenthosSortedWeight;
        if (computedBenthosBatches == null) {
            totalBenthosSortedWeight = 0f;
        } else {
            totalBenthosSortedWeight = computeSpeciesBatches(getBenthosBatchUI(), computedBenthosBatches, benthosBatchRows = new ArrayList<>());
        }

        // ---------
        // Compute marine litter batches
        // ---------

        BatchContainer<MarineLitterBatch> computedMarineLitterBatches = computingService.getComputedMarineLitterBatches(operationId, model.getMarineLitterTotalWeight());

        modified = model.isModify();
        catchBatch = model.toEntity();

        // ---------
        // Check species rates
        // ---------

        Float rate = getConfig().getDifferenceRateBetweenSortedAndTotalWeights();
        //FIXME Do this in Weights
        if (model.getSpeciesTotalSortedWeight() != null
                && model.getSpeciesTotalSortedWeight() >= totalSpeciesSortedWeight
                && model.getSpeciesTotalSortedWeight() < (1 + rate / 100) * totalSpeciesSortedWeight) {

            // Si  le "Poids total VRAC" est saisi est que sa valeur
            // est supérieure de moins de x% (x en configuration)
            // du "Poids total Vrac trié", demander confirmation que
            // le "Poids total VRAC" est bien une valeur observée
            // sinon la remplacer par le "Poids total Vrac trié"

            getUI().getTabPane().setSelectedIndex(1);

            String htmlMessage = String.format(
                    AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.editCatchBatch.action.computeWeights.replaceTotalSortedWeight.message.species", rate),
                    t("tutti.editCatchBatch.action.computeWeights.replaceTotalSortedWeight.help"));

            int answer = JOptionPane.showConfirmDialog(getContext().getActionUI(),
                                                       htmlMessage,
                                                       t("tutti.editCatchBatch.action.computeWeights.replaceTotalSortedWeight.title"),
                                                       JOptionPane.YES_NO_OPTION,
                                                       JOptionPane.QUESTION_MESSAGE);

            if (answer == JOptionPane.NO_OPTION) {

                catchBatch.setSpeciesTotalSortedWeight(null);
                catchBatch.setSpeciesTotalSortedComputedWeight(totalSpeciesSortedWeight);
                modified = true;

            }

        }

        // ---------
        // Check benthos rates
        // ---------

        //FIXME Do this in Weights
        if (model.getBenthosTotalSortedWeight() != null
                && model.getBenthosTotalSortedWeight() >= totalBenthosSortedWeight
                && model.getBenthosTotalSortedWeight() < (1 + rate / 100) * totalBenthosSortedWeight) {

            // Si  le "Poids total VRAC" est saisi est que sa valeur
            // est supérieure de moins de x% (x en configuration)
            // du "Poids total Vrac trié", demander confirmation que
            // le "Poids total VRAC" est bien une valeur observée
            // sinon la remplacer par le "Poids total Vrac trié"

            getUI().getTabPane().setSelectedIndex(2);

            String htmlMessage = String.format(
                    AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.editCatchBatch.action.computeWeights.replaceTotalSortedWeight.message.benthos", rate),
                    t("tutti.editCatchBatch.action.computeWeights.replaceTotalSortedWeight.help"));

            int answer = JOptionPane.showConfirmDialog(getContext().getActionUI(),
                                                       htmlMessage,
                                                       t("tutti.editCatchBatch.action.computeWeights.replaceTotalSortedWeight.title"),
                                                       JOptionPane.YES_NO_OPTION,
                                                       JOptionPane.QUESTION_MESSAGE);

            if (answer == JOptionPane.NO_OPTION) {

                catchBatch.setBenthosTotalSortedWeight(null);
                catchBatch.setBenthosTotalSortedComputedWeight(totalBenthosSortedWeight);
                modified = true;

            }
        }

        computingService.computeCatchBatchWeights(catchBatch,
                                                  computedSpeciesBatches,
                                                  computedBenthosBatches,
                                                  computedMarineLitterBatches);

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        afterAction();

        getUI().repaint();
    }

    protected void afterAction() {

        EditCatchesUIModel editCatchesUIModel = getModel();

        editCatchesUIModel.setLoadingData(true);

        SpeciesBatchUI speciesBatchUI = getSpeciesBatchUI();
        SpeciesBatchUI benthosBatchesUI = getBenthosBatchUI();

        try {

            if (speciesBatchRows != null) {

                speciesBatchUI.getModel().setRows(speciesBatchRows);

            }

            if (benthosBatchRows != null) {

                benthosBatchesUI.getModel().setRows(benthosBatchRows);

            }

            if (catchBatch != null) {

                editCatchesUIModel.fromEntity(catchBatch);

            }

            if (modified != null) {

                editCatchesUIModel.setModify(modified);

            }

        } finally {

            editCatchesUIModel.setLoadingData(false);

        }

        // keep sortMode
        // see https://forge.codelutin.com/issues/5699

        {
            SpeciesBatchUIModel model = speciesBatchUI.getModel();
            SpeciesSortMode sortMode = model.getSpeciesSortMode();
            if (sortMode != SpeciesSortMode.NONE) {

                // must reload order
                model.setSpeciesSortMode(SpeciesSortMode.NONE);
                model.setSpeciesSortMode(sortMode);

            }
        }

        {
            SpeciesBatchUIModel model = benthosBatchesUI.getModel();
            SpeciesSortMode sortMode = model.getSpeciesSortMode();
            if (sortMode != SpeciesSortMode.NONE) {

                // must reload order
                model.setSpeciesSortMode(SpeciesSortMode.NONE);
                model.setSpeciesSortMode(sortMode);

            }
        }

    }

    @Override
    public void postFailedAction(Throwable error) {

        afterAction();

        if (error instanceof CatchWeightComputingException) {

            getUI().getTabPane().setSelectedIndex(0);

        }

        if (error instanceof SpeciesWeightComputingException) {

            SpeciesWeightComputingException e = (SpeciesWeightComputingException) error;

            getUI().getTabPane().setSelectedIndex(1);

            treatSpeciesBatchError(getSpeciesBatchUI(), e);

        }

        if (error instanceof BenthosWeightComputingException) {

            BenthosWeightComputingException e = (BenthosWeightComputingException) error;

            getUI().getTabPane().setSelectedIndex(2);

            treatSpeciesBatchError(getBenthosBatchUI(), e);

        }

        if (error instanceof MarineLitterWeightComputingException) {

            MarineLitterWeightComputingException e = (MarineLitterWeightComputingException) error;

            getUI().getTabPane().setSelectedIndex(3);

            Optional<Integer> optionalRowIndex = e.getIndex();

            if (optionalRowIndex.isPresent()) {

                JTables.doSelectCell(getUI().getMarineLitterTabContent().getTable(), optionalRowIndex.get(), 3);

            }

        }

    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected Float computeSpeciesBatches(SpeciesBatchUI ui, BatchContainer<SpeciesBatch> computedSpeciesBatches, List<SpeciesBatchRowModel> speciesBatchRows) {

        Float totalSortedWeight = 0f;

        for (SpeciesBatch speciesBatch : computedSpeciesBatches.getChildren()) {
            SpeciesBatchRowModel row = ui.getHandler().loadBatch(speciesBatch, null, speciesBatchRows);
            if (persistenceService.isVracBatch(speciesBatch)) {
                SampleCategory<?> sampleCategory = row.getFirstSampleCategory();
                Float weight = Numbers.getValueOrComputedValue(sampleCategory.getCategoryWeight(), sampleCategory.getComputedWeight());
                totalSortedWeight += weight;
            }
        }

        return totalSortedWeight;

    }

    protected void treatSpeciesBatchError(SpeciesBatchUI ui, TuttiWeightComputingException e) {

        Optional<Integer> optionalRowIndex = e.getIndex();

        Optional<String> optionalProperty = e.getProperty();

        if (optionalRowIndex.isPresent() && optionalProperty.isPresent()) {

            SpeciesBatchUIModel speciesBatchUIModel = ui.getModel();

            Integer index = optionalRowIndex.get();

            SpeciesBatchRowModel row;

            if (SpeciesSortMode.NONE != speciesBatchUIModel.getSpeciesSortMode()) {

                // must resort rows in natural order (in service we use this order)
                List<SpeciesBatchRowModel> rows = new ArrayList<>(speciesBatchUIModel.getRows());
                SpeciesBatchNaturalOrderComparator.sort(rows);
                // get the row
                row = rows.get(index);
                // get the correct rowIndex in the sorted list
                index = speciesBatchUIModel.getRows().indexOf(row);

            } else {

                // correct order can directly get row from list
                List<SpeciesBatchRowModel> rows = speciesBatchUIModel.getRows();
                row = rows.get(index);

            }

            JXTable table = ui.getTable();
            SpeciesBatchTableModel tableModel = (SpeciesBatchTableModel) table.getModel();
            Set<SampleCategoryColumnIdentifier> sampleCols = tableModel.getSampleCols();
            selectBatchCell(table, index, row, optionalProperty.get(), sampleCols, SpeciesBatchTableModel.WEIGHT);

        }

    }

    protected void selectBatchCell(JXTable table,
                                   int index,
                                   SpeciesBatchRowModel row,
                                   String property,
                                   Set<SampleCategoryColumnIdentifier> sampleCols,
                                   ColumnIdentifier<SpeciesBatchRowModel> weightColumn) {

        ColumnIdentifier columnIdentifier = weightColumn;

        if (SpeciesBatch.PROPERTY_SAMPLE_CATEGORY_WEIGHT.equals(property)) {

            // get category column for category
            Integer categoryId = row.getFinestCategory().getCategoryId();
            for (SampleCategoryColumnIdentifier sampleCol : sampleCols) {
                if (categoryId.equals(sampleCol.getSampleCategoryId())) {
                    columnIdentifier = sampleCol;
                    break;
                }
            }

        }

        int column = 0;
        TableColumnExt columnExt = table.getColumnExt(columnIdentifier);
        int modelIndex = columnExt.getModelIndex();
        for (TableColumn tableColumn : table.getColumns(false)) {

            if (columnExt.equals(tableColumn) || tableColumn.getModelIndex() > modelIndex) {
                break;
            }
            column++;
        }

        JTables.doSelectCell(table, index, column);

    }

    private SpeciesBatchUI getBenthosBatchUI() {
        return getUI().getBenthosTabPanel().getEditBatchesUI();
    }

    private SpeciesBatchUI getSpeciesBatchUI() {
        return getUI().getSpeciesTabPanel().getEditBatchesUI();
    }

}
