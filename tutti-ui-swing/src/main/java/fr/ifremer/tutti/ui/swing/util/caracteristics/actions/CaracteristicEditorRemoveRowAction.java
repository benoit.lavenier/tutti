package fr.ifremer.tutti.ui.swing.util.caracteristics.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorRowModel;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorTableModel;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorUI;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorUIModel;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class CaracteristicEditorRemoveRowAction extends SimpleActionSupport<CaracteristicMapEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CaracteristicEditorRemoveRowAction.class);

    private static final long serialVersionUID = 1L;

    public CaracteristicEditorRemoveRowAction(CaracteristicMapEditorUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(CaracteristicMapEditorUI ui) {

        int rowIndex = ui.getCaracteristicMapEditorTable().getSelectedRow();

        Preconditions.checkState(
                rowIndex != -1,
                "Cant remove caracteristic if no caracteristic selected");
        CaracteristicMapEditorTableModel tableModel = ui.getHandler().getTableModel();
        CaracteristicMapEditorRowModel row = tableModel.getEntry(rowIndex);

        if (log.isInfoEnabled()) {
            log.info("Remove caracteristic row: " + row);
        }

        CaracteristicMapEditorUIModel model = ui.getModel();
        CaracteristicMap caracteristicMap = model.getCaracteristicMap();
        if (caracteristicMap != null) {
            caracteristicMap.remove(row.getKey());
        }

        //add the row in the combo
        BeanFilterableComboBox<Caracteristic> keyCombo = ui.getNewRowKey();
        keyCombo.addItem(row.getKey());
        keyCombo.reset();

        // remove the row from the model
        model.getRows().remove(rowIndex);

        // refresh all the table
        tableModel.fireTableRowsDeleted(rowIndex, rowIndex);

        model.removeRowInError(row);

    }

}