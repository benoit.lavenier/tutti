package fr.ifremer.tutti.ui.swing.util.comment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CommentAware;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.spi.UIHandler;
import jaxx.runtime.swing.ComponentMover;
import jaxx.runtime.swing.ComponentResizer;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class CommentEditorUIHandler implements UIHandler<CommentEditorUI> {

    private static final int DEFAULT_EDITOR_WIDTH = 400;

    private static final int DEFAULT_EDITOR_HEIGHT = 200;

    public static final String CLOSE_DIALOG_ACTION = "closeDialog";

    public static final String SHOW_DIALOG_ACTION = "showDialog";

    protected TuttiUIContext context;

    private CommentEditorUI ui;

    @Override
    public void beforeInit(CommentEditorUI ui) {
        this.ui = ui;
        this.context = TuttiUIUtil.getApplicationContext(ui);
    }

    @Override
    public void afterInit(CommentEditorUI ui) {

        ui.pack();
        ui.setResizable(true);
//        ui.setSize(800, 300);

        ui.setSize(DEFAULT_EDITOR_WIDTH, DEFAULT_EDITOR_HEIGHT);

        ComponentResizer cr = new ComponentResizer();
        cr.registerComponent(ui);
        ComponentMover cm = new ComponentMover();
        cm.setDragInsets(cr.getDragInsets());
        cm.registerComponent(ui);

        JRootPane rootPane = ui.getRootPane();

        KeyStroke shortcutClosePopup =
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                shortcutClosePopup, CLOSE_DIALOG_ACTION);

        closeAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                CommentEditorUIHandler.this.ui.dispose();
                CommentEditorUIHandler.this.ui.setVisible(false);
            }
        };

        openAction = new AbstractAction() {

            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                CommentEditorUIHandler.this.ui.setVisible(true);
            }
        };

        //TODO Use fr.ifremer.tutti.ui.swing.util.TuttiUIUtil.prepareAction()
        ImageIcon actionIcon = SwingUtil.createActionIcon("close-dialog");
        closeAction.putValue(Action.SMALL_ICON, actionIcon);
        closeAction.putValue(Action.LARGE_ICON_KEY, actionIcon);
        closeAction.putValue(Action.ACTION_COMMAND_KEY, "close");
        closeAction.putValue(Action.NAME, "close");
        closeAction.putValue(Action.SHORT_DESCRIPTION, t("tutti.commentEditor.action.close.tip"));

        rootPane.getActionMap().put(CLOSE_DIALOG_ACTION, closeAction);
        rootPane.getActionMap().put(SHOW_DIALOG_ACTION, openAction);

        JButton closeButton = new JButton(closeAction);
        closeButton.setText(null);
        closeButton.setFocusPainted(false);
        closeButton.setRequestFocusEnabled(false);
        closeButton.setFocusable(false);

        JToolBar jToolBar = new JToolBar();
        jToolBar.setOpaque(false);
        jToolBar.add(closeAction);
        jToolBar.setBorderPainted(false);
        jToolBar.setFloatable(false);
        ui.getCommentEditorTopPanel().setRightDecoration(jToolBar);
    }

    protected Action closeAction;

    protected Action openAction;

    public void closeEditor() {

        closeAction.actionPerformed(null);
    }

    public void openEditor(JComponent component) {

        if (component != null) {
            place(component);
        }
        openAction.actionPerformed(null);
    }

    public void init() {

        CommentAware bean = ui.getBean();
        String content = bean == null ? null : bean.getComment();
        ui.getTextContent().setText(content);
    }

    public void place(JComponent component) {
        // Computes the location of bottom left corner of the cell
        Component comp = component;
        int x = 0;
        int y = component.getHeight();
        while (comp != null) {
            x += comp.getX();
            y += comp.getY();
            comp = comp.getParent();
        }

//        ui.pack();
        // if the editor is too big on the right,
        // then align its right side to the right side of the cell
        if (x + ui.getWidth() > ui.getOwner().getX() + ui.getOwner().getWidth()) {
            x = x - ui.getWidth() + component.getWidth();
        }
        ui.setLocation(x, y);
    }

    /** pushes the typed text in the property of the bean */
    public void setText(String value) {
        CommentAware bean = ui.getBean();
        bean.setComment(value);
    }

}
