package fr.ifremer.tutti.ui.swing.content.operation.catches.species.split;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Handler of {@link SplitSpeciesBatchUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class SplitSpeciesBatchUIHandler extends AbstractTuttiTableUIHandler<SplitSpeciesBatchRowModel, SplitSpeciesBatchUIModel, SplitSpeciesBatchUI> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SplitSpeciesBatchUIHandler.class);

    protected final PropertyChangeListener PROPERTY_CATEGORY_CHANGED_LISTENER = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            SplitSpeciesBatchUIModel source =
                    (SplitSpeciesBatchUIModel) evt.getSource();

            // unselect previous selected category
            source.setSelectedCategory(null);

            // fill comboBox with new list
            List<SampleCategoryModelEntry> data = (List<SampleCategoryModelEntry>) evt.getNewValue();
            SplitSpeciesBatchUIHandler.this.ui.getCategoryComboBox().setModel(new DefaultComboBoxModel(data.toArray()));
        }
    };

    protected final PropertyChangeListener PROPERTY_SELECTED_CATEGORY_CHANGED_LISTENER = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            SplitSpeciesBatchUIModel source =
                    (SplitSpeciesBatchUIModel) evt.getSource();

            // when selected category change, sample total weight is reset
            source.setSampleWeight(null);

            SampleCategoryModelEntry newValue =
                    (SampleCategoryModelEntry) evt.getNewValue();
            generateTableModel(newValue);
        }
    };

    protected final ActionListener CATEGORY_ACTION_LISTENER = new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            JComboBox comboBox = (JComboBox) e.getSource();
            getModel().setSelectedCategory((SampleCategoryModelEntry) comboBox.getSelectedItem());
        }
    };

    protected final PropertyChangeListener PROPERTY_WEIGHT_CHANGED_LISTENER = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getNewValue() != null) {
                ((SplitSpeciesBatchRowModel) evt.getSource()).setSelected(true);
            }
        }
    };

//    protected SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    public SplitSpeciesBatchUIHandler() {
        super(SplitSpeciesBatchRowModel.PROPERTY_SELECTED,
              SplitSpeciesBatchRowModel.PROPERTY_CATEGORY_VALUE,
              SplitSpeciesBatchRowModel.PROPERTY_WEIGHT);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public SplitSpeciesBatchTableModel getTableModel() {
        return (SplitSpeciesBatchTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getTable();
    }

    @Override
    protected boolean isRowValid(SplitSpeciesBatchRowModel row) {
        return row.isSelected();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<SplitSpeciesBatchRowModel> rowMonitor,
                                             SplitSpeciesBatchRowModel row) {
        if (rowMonitor.wasModified()) {

            if (row.isValid()) {
                if (log.isInfoEnabled()) {
                    log.info("Change row that was modified and valid");
                }
            }

            rowMonitor.clearModified();
        }
    }

    @Override
    protected void onAfterSelectedRowChanged(int oldRowIndex,
                                             SplitSpeciesBatchRowModel oldRow,
                                             int newRowIndex,
                                             SplitSpeciesBatchRowModel newRow) {
        super.onAfterSelectedRowChanged(oldRowIndex, oldRow, newRowIndex, newRow);
        if (newRow != null) {

            // Recompute the valid state of the row
            recomputeRowValidState(newRow);

            // Need to recompute the sample weight
            computeSampleWeight();
        }
    }

    @Override
    protected void onRowModified(int rowIndex,
                                 SplitSpeciesBatchRowModel row,
                                 String propertyName,
                                 Object oldValue,
                                 Object newValue) {

        // Recompute the valid state of the row
        recomputeRowValidState(row);

        // Need to recompute the sample weight
        computeSampleWeight();
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public SwingValidator<SplitSpeciesBatchUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public void beforeInit(SplitSpeciesBatchUI ui) {
        super.beforeInit(ui);
        SampleCategoryModel sampleCategoryModel = getDataContext().getSampleCategoryModel();

        SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport = ui.getContextValue(SpeciesOrBenthosBatchUISupport.class, ui.getSpeciesOrBenthosContext());

        SplitSpeciesBatchUIModel model = new SplitSpeciesBatchUIModel(speciesOrBenthosBatchUISupport, sampleCategoryModel);

        ui.setContextValue(model);
    }

    @Override
    public void afterInit(SplitSpeciesBatchUI ui) {

        initUI(ui);

        SplitSpeciesBatchUIModel model = getModel();

        // when category changed, remove selected category
        model.addPropertyChangeListener(SplitSpeciesBatchUIModel.PROPERTY_CATEGORY, PROPERTY_CATEGORY_CHANGED_LISTENER);

        // when selected category changed, regenerate the table model + add inside some default rows

        model.addPropertyChangeListener(SplitSpeciesBatchUIModel.PROPERTY_SELECTED_CATEGORY, PROPERTY_SELECTED_CATEGORY_CHANGED_LISTENER);

        ui.getCategoryComboBox().setRenderer(newListCellRender(SampleCategoryModelEntry.class));

        ui.getCategoryComboBox().addActionListener(CATEGORY_ACTION_LISTENER);

        generateTableModel(null);

        initTable(getTable());

        listenValidatorValid(ui.getValidator(), model);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getCategoryComboBox();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        // evict model from validator
        ui.getValidator().setBean(null);

        clearValidators();

        // when canceling always invalid model
        getModel().setValid(false);
        getModel().setSelectedCategory(null);

        EditSpeciesBatchPanelUI parent = getParentContainer(EditSpeciesBatchPanelUI.class);
        parent.switchToEditBatch();

    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void editBatch(SpeciesBatchRowModel batch) {

        // get possible the last used
        List<SampleCategoryModelEntry> categories = new ArrayList<>();

        SplitSpeciesBatchUIModel model = getModel();

        SampleCategoryModelEntry bestSampleCategory = null;
        if (batch != null) {

            // get sample category model
            SampleCategoryModel sampleCategoryModel = getModel().getSampleCategoryModel();

            // set all categories (the no more available will be removed later)
            categories.addAll(sampleCategoryModel.getCategory());

            SampleCategory<?> lastCategory = batch.getFinestCategory();

            Objects.requireNonNull(lastCategory, "Can't split a species batch with no sample category.");

            int firstOrder = lastCategory.getCategoryDef().getOrder();

            for (Integer sampleCategoryId : sampleCategoryModel.getSamplingOrder()) {
                SampleCategory<?> sampleCategory = batch.getSampleCategoryById(sampleCategoryId);
                int order = sampleCategory.getCategoryDef().getOrder();
                if (order < firstOrder || sampleCategory.isValid()) {

                    // remove category if before the finest one
                    // remove category if not filled
                    categories.remove(sampleCategory.getCategoryDef());
                }
            }

            bestSampleCategory = model.getSpeciesOrBenthosBatchUISupport().getBestFirstSampleCategory(categories, batch.getSpecies());
        }

        // connect model to validator
        ui.getValidator().setBean(model);

        model.setSampleWeight(null);
        model.setCategory(categories);
        model.setSelectedCategory(bestSampleCategory);

        // keep batch (will be used to push back editing entry)
        model.setBatch(batch);
    }

    public void editBatch(SpeciesBatchRowModel batch, int sampleCategoryId) {

        Objects.requireNonNull(batch);
        Objects.requireNonNull(sampleCategoryId);

        List<SpeciesBatchRowModel> rows = batch.getChildBatch();
        Objects.requireNonNull(rows);

        // get possible the last used
        List<SampleCategoryModelEntry> categories = new ArrayList<>();

        // get sample category model
        SampleCategoryModel sampleCategoryModel = getModel().getSampleCategoryModel();

        // set only the given category
        SampleCategoryModelEntry selectedCategory = sampleCategoryModel.getCategoryById(sampleCategoryId);
        categories.add(selectedCategory);

        SplitSpeciesBatchUIModel model = getModel();

        // connect model to validator
        ui.getValidator().setBean(model);

        model.setSampleWeight(null);
        model.setCategory(categories);
        model.setSelectedCategory(selectedCategory);

        // keep batch (will be used to push back editing entry)
        model.setBatch(batch);

        // add existing rows
        Map<Serializable, SplitSpeciesBatchRowModel> rowsByValue = Maps.uniqueIndex(model.getRows(), SplitSpeciesBatchRowModel::getCategoryValue);
        for (SpeciesBatchRowModel row : rows) {

            SampleCategory<?> sampleCategory = row.getSampleCategoryById(sampleCategoryId);
            Serializable categoryValue = sampleCategory.getCategoryValue();
            SplitSpeciesBatchRowModel splitRow = rowsByValue.get(categoryValue);
            splitRow.setWeight(sampleCategory.getCategoryWeight());
            splitRow.setSelected(true);
            splitRow.setEditable(false);
        }

        computeSampleWeight();

        getTableModel().fireTableDataChanged();
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void computeSampleWeight() {

        Optional<Float> result = getTableModel().getTotalWeight();
//        List<SplitSpeciesBatchRowModel> rows = getTableModel().getRows();
//        for (SplitSpeciesBatchRowModel row : rows) {
//            if (row.isSelected()) {
//                Float weight = row.getWeight();
//                if (weight != null) {
//                    if (result == null) {
//                        result = 0f;
//                    }
//                    result += weight;
//                }
//            }
//        }
        getModel().setSampleWeight(result.orElse(null));
    }

    protected void generateTableModel(SampleCategoryModelEntry category) {

        // when generate a new table model, then reset previous rows from model
        getModel().setRows(null);

        Caracteristic data = null;

        JXTable table = getTable();

        DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        { // Selection

            addBooleanColumnToModel(columnModel, SplitSpeciesBatchTableModel.SELECTED, table);
        }

        boolean editableCategoryValue = false;

        log.debug("category " + category);
        if (category != null) {
            log.debug("category " + category.getLabel());
            if (!category.getCaracteristic().isQualitativeValueEmpty()) {

                // qualitative category
                data = category.getCaracteristic();

                log.debug("data " + data);

            } else {
                editableCategoryValue = true;
                addFloatColumnToModel(columnModel,
                                      SplitSpeciesBatchTableModel.EDITABLE_CATEGORY_VALUE,
                                      TuttiUI.DECIMAL1_PATTERN,
                                      table);
            }

            if (data != null) {

                if (log.isDebugEnabled()) {
                    log.debug("Got " + data.sizeQualitativeValue() + " qualitative data to add");
                }
                addColumnToModel(columnModel,
                                 null,
                                 newTableCellRender(CaracteristicQualitativeValue.class),
                                 SplitSpeciesBatchTableModel.READ_ONLY_CATEGORY_VALUE);
            }
            { // Weight

                addFloatColumnToModel(columnModel,
                                      SplitSpeciesBatchTableModel.WEIGHT,
                                      getConfig().getSpeciesWeightUnit(),
                                      table);
            }
        }

        // create table model
        SplitSpeciesBatchTableModel tableModel = new SplitSpeciesBatchTableModel(columnModel,
                                                                                 getModel(),
                                                                                 editableCategoryValue,
                                                                                 getModel().isSplitMode());

        // remove all listener on tables we could add before
        uninstallTableSaveOnRowChangedSelectionListener();
        uninstallTableKeyListener(getTable());

        if (log.isDebugEnabled()) {
            log.debug("Install new table model " + tableModel);
        }
        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        // install table listeners
        installTableSaveOnRowChangedSelectionListener();
        installTableKeyListener(columnModel, table);

        // fill datas

        List<SplitSpeciesBatchRowModel> rows = new ArrayList<>();

        if (data != null) {

            // add a row for each qualitative value
            for (CaracteristicQualitativeValue qualitativeValue : data.getQualitativeValue()) {
                log.debug("QV: " + qualitativeValue);
                if (log.isDebugEnabled()) {
                    log.debug("Add QV: " + qualitativeValue);
                }
                SplitSpeciesBatchRowModel newRow = tableModel.createNewRow();
                newRow.setCategoryValue(qualitativeValue);
                newRow.addPropertyChangeListener(SplitSpeciesBatchRowModel.PROPERTY_WEIGHT, PROPERTY_WEIGHT_CHANGED_LISTENER);
                rows.add(newRow);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Will add " + rows.size() + " rows in table model " +
                              "(can add a first empty row? " + editableCategoryValue + ").");
        }

        getModel().setRows(rows);
    }

}
