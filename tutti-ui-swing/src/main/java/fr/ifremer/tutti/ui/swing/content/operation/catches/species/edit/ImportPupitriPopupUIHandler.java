package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import java.io.File;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class ImportPupitriPopupUIHandler extends AbstractTuttiUIHandler<ImportPupitriPopupUIModel, ImportPupitriPopupUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportPupitriPopupUIHandler.class);

    @Override
    public void beforeInit(ImportPupitriPopupUI ui) {
        super.beforeInit(ui);

        ImportPupitriPopupUIModel model = new ImportPupitriPopupUIModel();
        boolean pupitriImportMissingBatches = getConfig().getPupitriImportMissingBatches();
        model.setImportMissingBatches(pupitriImportMissingBatches);

        ui.setContextValue(model);
    }

    @Override
    public void afterInit(ImportPupitriPopupUI ui) {
        initUI(ui);
        ui.getTrunkFile().setDialogOwner(ui);
        ui.getCarrouselFile().setDialogOwner(ui);
        ui.pack();
        ui.setResizable(true);

        getModel().addPropertyChangeListener(ImportPupitriPopupUIModel.PROPERTY_TRUNK_FILE, evt -> updateCarrouselFile());

        getModel().addPropertyChangeListener(ImportPupitriPopupUIModel.PROPERTY_CARROUSEL_FILE, evt -> updateTrunkFile());
    }

    @Override
    protected JComponent getComponentToFocus() {
        return ui.getTrunkFile();
    }

    @Override
    public void onCloseUI() {
        resetFields();
    }

    @Override
    public SwingValidator<ImportPupitriPopupUIModel> getValidator() {
        return null;
    }

    protected void updateTrunkFile() {

        File carrouselFile = getModel().getCarrouselFile();

        if (carrouselFile != null) {
            String nameWithoutExtension = Files.getNameWithoutExtension(carrouselFile.getName());
            File trunkFile = new File(carrouselFile.getParentFile(), nameWithoutExtension + ".tnk");
            if (trunkFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("Select from carrousel file, trunk file: " + trunkFile);
                }

               getModel().setTrunkFile(trunkFile);

            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No trunk file at " + trunkFile);
                }
            }
        }

    }

    protected void updateCarrouselFile() {

        File trunkFile = getModel().getTrunkFile();
        if (trunkFile != null) {
            String nameWithoutExtension = Files.getNameWithoutExtension(trunkFile.getName());
            File carrouselFile = new File(trunkFile.getParentFile(), nameWithoutExtension + ".car");
            if (carrouselFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("Select from trunk file, carrousel file: " + carrouselFile);
                }

                getModel().setCarrouselFile(carrouselFile);

            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No carrousel file at " + carrouselFile);
                }
            }
        }

    }

    public void open() {
        resetFields();
        SwingUtil.center(TuttiUIUtil.getApplicationContext(ui).getMainUI(), ui);
        ui.setVisible(true);
    }

    public void resetFields() {
        ui.getTrunkFile().setSelectedFilePath(null);
        ui.getCarrouselFile().setSelectedFilePath(null);
    }

}
