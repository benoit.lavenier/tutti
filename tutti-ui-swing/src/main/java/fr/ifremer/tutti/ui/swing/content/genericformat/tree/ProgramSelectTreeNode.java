package fr.ifremer.tutti.ui.swing.content.genericformat.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterators;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created on 3/30/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class ProgramSelectTreeNode extends DataSelectTreeNodeSupport<ProgramDataModel> implements  Iterable<CruiseSelectTreeNode> {

    private static final long serialVersionUID = 1L;

    public ProgramSelectTreeNode(ProgramDataModel userObject) {
        super(userObject);

        for (CruiseDataModel cruise : userObject) {

            CruiseSelectTreeNode cruiseNode = new CruiseSelectTreeNode(cruise);
            add(cruiseNode);

        }
    }

    @Override
    public boolean isSelected() {
        //Not use here
        return false;
    }

    @Override
    public void setSelected(boolean selected) {
        //Not use here
    }

    @Override
    public ProgramDataModel getSelectedDataModel() {

        Set<CruiseDataModel> cruises = new HashSet<>();

        for (CruiseSelectTreeNode o : this) {
            CruiseDataModel cruise = o.getSelectedDataModel();
            if (cruise != null) {
                cruises.add(cruise);
            }
        }

        return new ProgramDataModel(getId(), getLabel(), cruises);
    }

    @Override
    public Iterator<CruiseSelectTreeNode> iterator() {
        return Iterators.forEnumeration(children());
    }

    public boolean isSelectedDataExists() {

        boolean result=false;
        for (CruiseSelectTreeNode cruise : this) {
            if (cruise.isSelectedDataExists()) {
                result = true;
                break;
            }
        }
        return result;

    }
}
