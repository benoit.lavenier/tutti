package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.application.swing.ApplicationUI;

import javax.swing.UIManager;
import java.awt.Font;

/**
 * Contract to place on each generated jaxx ui.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public interface TuttiUI<M, H extends AbstractTuttiUIHandler<M, ?>> extends ApplicationUI<M, H> {

    /**
     * Pattern to use for signed decimal numeric values with 4 decimal digits in editors.
     *
     * @since 4.2
     */
    String SIGNED_DECIMAL4_PATTERN = "-?\\d{0,4}(\\.\\d{0,4})?";

    /**
     * Pattern to use for unsigned decimal numeric values with 9 decimal digits in editors.
     *
     * @since 4.4
     */
    String DECIMAL9_PATTERN = "\\d{0,6}(\\.\\d{0,9})?";

    Font TEXTFIELD_NORMAL_FONT = UIManager.getDefaults().getFont("TextField.font");

    Font TEXTFIELD_COMPUTED_FONT = UIManager.getDefaults().getFont("TextField.font").deriveFont(Font.ITALIC);

    H getHandler();

}
