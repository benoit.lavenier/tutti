package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.caliper.feed.CaliperConnectionException;
import fr.ifremer.tutti.caliper.feed.CaliperFeedReader;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import static org.nuiton.i18n.I18n.t;

/**
 * Establish a connection to an ichtyometer.
 *
 * Created on 1/29/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class ConnectCaliperAction extends AbstractMainUITuttiAction {

    public ConnectCaliperAction(MainUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void doAction() throws Exception {

        CaliperFeedReader caliperReader = new CaliperFeedReader();

        int caliperSerialPort = getConfig().getCaliperSerialPort();
        try {
            caliperReader.start(caliperSerialPort);
        } catch (CaliperConnectionException e) {
            throw new ApplicationBusinessException(t("tutti.caliper.connection.error", caliperSerialPort, e.getMessage()));
        }

        getContext().setCaliperReader(caliperReader);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        CaliperFeedReader caliperReader = getContext().getCaliperReader();
        String serialPort = caliperReader.getSerialPortName();
        sendMessage(t("tutti.caliper.connection.establish", serialPort));

        displayInfoMessage(
                t("tutti.caliper.connection.establish.title"),
                t("tutti.caliper.connection.establish.message", serialPort)
        );
    }
}
