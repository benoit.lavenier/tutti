package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class MinSizePopupUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_MIN_SIZE = "minSize";

    public static final String PROPERTY_MIN_MIN_SIZE = "minMinSize";

    public static final String PROPERTY_MAX_MIN_SIZE = "maxMinSize";

    /**
     * Is the model valid?
     */
    protected boolean valid;

    protected Integer minSize;

    protected int minMinSize;

    protected Integer maxMinSize;

    public Integer getMaxMinSize() {
        return maxMinSize;
    }

    public void setMaxMinSize(Integer maxMinSize) {
        Object oldValue = getMaxMinSize();
        this.maxMinSize = maxMinSize;
        firePropertyChange(PROPERTY_MAX_MIN_SIZE, oldValue, maxMinSize);
    }

    public int getMinMinSize() {
        return minMinSize;
    }

    public void setMinMinSize(int minMinSize) {
        Object oldValue = getMinMinSize();
        this.minMinSize = minMinSize;
        firePropertyChange(PROPERTY_MIN_MIN_SIZE, oldValue, minMinSize);
    }

    public Integer getMinSize() {
        return minSize;
    }

    public void setMinSize(Integer minSize) {
        Object oldValue = getMinSize();
        this.minSize = minSize;
        firePropertyChange(PROPERTY_MIN_SIZE, oldValue, minSize);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
