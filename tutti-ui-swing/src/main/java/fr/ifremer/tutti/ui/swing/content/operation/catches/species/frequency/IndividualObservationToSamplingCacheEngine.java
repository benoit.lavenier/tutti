package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.sampling.IndividualObservationSamplingCacheRequest;

import java.util.Objects;

/**
 * Pour effectuer depuis une modification ou sélection d'une observation individuelle, les actions sur les caches.
 *
 * Created on 20/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationToSamplingCacheEngine {

    private final SpeciesFrequencyUIModel speciesFrequencyUIModel;
    private final IndividualObservationUICache individualObservationUICache;
    private final SamplingCodeUICache samplingCodeUICache;

    public IndividualObservationToSamplingCacheEngine(SpeciesFrequencyUIModel speciesFrequencyUIModel) {

        this.speciesFrequencyUIModel = speciesFrequencyUIModel;
        this.individualObservationUICache = speciesFrequencyUIModel.getIndividualObservationUICache();
        this.samplingCodeUICache = speciesFrequencyUIModel.getSamplingCodeUICache();

    }

    public boolean computeSamplingCacheUpdate(IndividualObservationBatchRowState oldState, IndividualObservationBatchRowState newState) {

        IndividualObservationSamplingCacheRequest addIndividualObservation = null;
        IndividualObservationSamplingCacheRequest removeIndividualObservation = null;
        IndividualObservationSamplingCacheRequest addSamplingAction = null;
        IndividualObservationSamplingCacheRequest removeSamplingAction = null;
        IndividualObservationSamplingCacheRequest addSamplingCodeAction = null;
        IndividualObservationSamplingCacheRequest removeSamplingCodeAction = null;

        boolean sizeChanged = !Objects.equals(oldState.getSize(), newState.getSize());
        boolean maturityChanged = !Objects.equals(oldState.getMaturity(), newState.getMaturity());
        boolean genderChanged = !Objects.equals(oldState.getGender(), newState.getGender());
        boolean samplingCodeChanged = !Objects.equals(oldState.getSamplingCode(), newState.getSamplingCode());
        boolean withOldSamplingCode = oldState.withSamplingCode();
        boolean withNewSamplingCode = newState.withSamplingCode();

        if (sizeChanged || maturityChanged || genderChanged) {

            removeIndividualObservation = toRequest(oldState);
            addIndividualObservation = toRequest(newState);

        } else if (samplingCodeChanged) {

            boolean removeSampling = withOldSamplingCode && !withNewSamplingCode;
            boolean addSampling = withNewSamplingCode && !withOldSamplingCode;

            if (removeSampling) {

                removeSamplingAction = toRequest(oldState);

            } else if (addSampling) {

                addSamplingAction = toRequest(newState);

            } else {

                removeSamplingCodeAction = toRequest(oldState);
                addSamplingCodeAction = toRequest(newState);

            }

        }

        boolean actionDone = false;
        if (removeIndividualObservation != null) {
            individualObservationUICache.removeIndividualObservation(removeIndividualObservation);
            actionDone=true;
        }
        if (addIndividualObservation != null) {
            individualObservationUICache.addIndividualObservation(addIndividualObservation);
            actionDone=true;
        }
        if (removeSamplingAction != null) {
            individualObservationUICache.removeSampling(removeSamplingAction);
            samplingCodeUICache.removeSampling(removeSamplingAction);
            actionDone=true;
        }
        if (addSamplingAction != null) {
            individualObservationUICache.addSampling(addSamplingAction);
            samplingCodeUICache.addSampling(addSamplingAction);
            actionDone=true;
        }
        if (removeSamplingCodeAction != null) {
            samplingCodeUICache.removeSampling(removeSamplingCodeAction);
            actionDone=true;
        }
        if (addSamplingCodeAction != null) {
            samplingCodeUICache.addSampling(addSamplingCodeAction);
            actionDone=true;
        }

        return actionDone;

    }

    private IndividualObservationSamplingCacheRequest toRequest(IndividualObservationBatchRowState state) {

        return new IndividualObservationSamplingCacheRequest(speciesFrequencyUIModel.getFishingOperation(),
                                                             speciesFrequencyUIModel.getBatch().getSpecies(),
                                                             speciesFrequencyUIModel.getLengthStepInMm(state.getSize()),
                                                             state.getMaturity(),
                                                             state.getGender(),
                                                             state.getSamplingCode());
    }

}
