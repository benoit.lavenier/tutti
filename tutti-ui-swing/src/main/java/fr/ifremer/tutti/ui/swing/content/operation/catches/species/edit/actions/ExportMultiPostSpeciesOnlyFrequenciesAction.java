package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.catches.multipost.MultiPostExportService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.actions.ExportMultiPostActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class ExportMultiPostSpeciesOnlyFrequenciesAction extends ExportMultiPostActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    public ExportMultiPostSpeciesOnlyFrequenciesAction(SpeciesBatchUIHandler handler) {
        super(handler);
    }

    @Override
    protected String getFileExtension() {
        return "tuttiSpeciesOnlyFrequencies";
    }

    @Override
    protected String getFileExtensionDescription() {
        return t("tutti.common.file.tuttiSpeciesOnlyFrequencies");
    }

    @Override
    protected String getFileChooserTitle() {
        return t("tutti.editSpeciesBatch.action.exportMultiPostOnlyFrequencies.destinationFile.title");
    }

    @Override
    protected String getFileChooserButton() {
        return t("tutti.editSpeciesBatch.action.exportMultiPostOnlyFrequencies.destinationFile.button");
    }

    @Override
    protected String getSuccessMessage(File file) {
        return t("tutti.editSpeciesBatch.action.exportMultiPostOnlyFrequencies.success", file);
    }

    @Override
    protected void doExport(MultiPostExportService multiPostImportExportService, File file, FishingOperation fishingOperation) {

        getModel().getSpeciesOrBenthosBatchUISupport().exportMultiPost(file, fishingOperation, true, false);

    }

}
