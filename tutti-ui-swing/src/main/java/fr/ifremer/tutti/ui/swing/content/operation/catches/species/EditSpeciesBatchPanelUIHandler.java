package fr.ifremer.tutti.ui.swing.content.operation.catches.species;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyCellComponent;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.swing.CardLayout2Ext;
import jaxx.runtime.validator.swing.SwingValidator;
import org.jdesktop.swingx.JXTable;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.table.TableColumn;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 18/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class EditSpeciesBatchPanelUIHandler extends AbstractTuttiUIHandler<EditSpeciesBatchPanelUIModel, EditSpeciesBatchPanelUI> {

    protected SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    @Override
    public void onCloseUI() {

        closeUI(ui.getEditBatchesUI());
        closeUI(ui.getEditFrequenciesUI());

    }

    @Override
    public void beforeInit(EditSpeciesBatchPanelUI ui) {
        super.beforeInit(ui);

        speciesOrBenthosBatchUISupport = ui.getContextValue(SpeciesOrBenthosBatchUISupport.class, ui.getSpeciesOrBenthosContext());

        EditSpeciesBatchPanelUIModel model = new EditSpeciesBatchPanelUIModel(speciesOrBenthosBatchUISupport);
        ui.setContextValue(model);

    }

    @Override
    public void afterInit(EditSpeciesBatchPanelUI editSpeciesBatchPanelUI) {

        ui.getSplitBatchUI().getModel().setSplitMode(true);
        ui.getAddSampleCategoryBatch().getModel().setSplitMode(false);


    }

    @Override
    public SwingValidator<EditSpeciesBatchPanelUIModel> getValidator() {
        return null;
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    public void editSpeciesFrequencies(int rowIndex) {
        JXTable speciesTable = ui.getEditBatchesUI().getTable();

        TableColumn frequenciesColumn = speciesTable.getColumn(SpeciesBatchTableModel.COMPUTED_NUMBER);
        int frequenciesColumnIndex = speciesTable.getColumns(false).indexOf(frequenciesColumn);

        SpeciesFrequencyCellComponent.FrequencyCellEditor frequencyCellEditor =
                (SpeciesFrequencyCellComponent.FrequencyCellEditor) speciesTable.getCellEditor(rowIndex, frequenciesColumnIndex);

        frequencyCellEditor.initEditor(speciesTable, rowIndex, frequenciesColumnIndex);
        frequencyCellEditor.startEdit();
    }

    public void editSpeciesFrequencies(SpeciesFrequencyCellComponent.FrequencyCellEditor editor) {

        SpeciesFrequencyUI frequencyEditor = ui.getEditFrequenciesUI();

        SpeciesBatchRowModel editRow = editor.getEditRow();

        String editBatchTitle = ui.getEditBatchesUIPanel().getTitle();

        String frequenciesTitle = buildReminderLabelTitle(editRow.getSpecies(),
                                                          editRow,
                                                          editBatchTitle,
                                                          "",
                                                          false);
        frequencyEditor.getHandler().editBatch(editor, frequenciesTitle);

        // open frequency editor
        ui.switchToEditFrequency();

        // update title
        String title = buildReminderLabelTitle(editRow.getSpecies(),
                                               editRow,
                                               editBatchTitle,
                                               t("tutti.editSpeciesFrequencies.title"));
        ui.getEditFrequenciesUIPanel().setTitle(title);
    }

    protected void setSpeciesSelectedCard(String card) {

        CardLayout2Ext layout = (CardLayout2Ext) ui.getLayout();

        if (!card.equals(layout.getSelected())) {

            layout.setSelected(card);

            EditCatchesUI parentContainer = getParentContainer(EditCatchesUI.class);
            JPanel actionPanel = parentContainer.getCreateFishingOperationActions();

            actionPanel.setVisible(false);

            SwingValidator<?> validator = null;

            switch (card) {

                case EditSpeciesBatchPanelUI.EDIT_BATCH_CARD:
                    actionPanel.setVisible(true);
                    break;

                case EditSpeciesBatchPanelUI.CREATE_BATCH_CARD:
                    validator = ui.getCreateBatchUI().getValidator();
                    String title = n("tutti.createSpeciesBatch.title");
                    ui.getCreateBatchUIPanel().setTitle(ui.getEditBatchesUIPanel().getTitle() + " - " + t(title));

                    break;

                case EditSpeciesBatchPanelUI.SPLIT_BATCH_CARD:
                    validator = ui.getSplitBatchUI().getValidator();
                    break;

                case EditSpeciesBatchPanelUI.ADD_SAMPLE_CATEGORY_BATCH_CARD:
                    validator = ui.getAddSampleCategoryBatch().getValidator();
                    break;

                case EditSpeciesBatchPanelUI.EDIT_FREQUENCY_CARD:

                    validator = ui.getEditFrequenciesUI().getValidator();
                    break;

            }

            if (validator != null) {
                registerValidators(validator);
            }

        }

    }
}
