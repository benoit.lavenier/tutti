package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.protocol.ProtocolCaracteristicsImportExportService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolCaracteristicsRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Collections;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To import protocol caracteristics.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ImportProtocolCaracteristicAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ImportProtocolCaracteristicAction.class);

    private File file;
    protected TuttiProtocol protocol;

    public ImportProtocolCaracteristicAction(EditProtocolUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            // choose file to import
            file = chooseFile(
                    t("tutti.editProtocol.title.choose.caracteristicImportFile"),
                    t("tutti.editProtocol.action.importProtocolCaracteristicFile"),
                    "^.*\\.csv", t("tutti.common.file.csv")
            );

            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will import protocol caracteristic file: " + file);
        }

        EditProtocolUIModel model = getModel();

        // bind to a protocol
        protocol = model.toEntity();

        ProtocolCaracteristicsImportExportService service = getContext().getProtocolCaracteristicsImportExportService();

        service.importProtocolCaracteristic(file, protocol, model.getAllCaracteristic());

    }

    @Override
    public void postSuccessAction() {

        super.postSuccessAction();

        handler.addDoubleListListeners();

        try {
            // rebind to model
            getModel().fromEntity(protocol);
        } finally {
            handler.removeDoubleListListeners();
        }

        List<CaracteristicMappingRow> caracteristicMapping = protocol.getCaracteristicMapping();
        List<EditProtocolCaracteristicsRowModel> caracteristicsRowModels = getHandler().toProtocolCaracteristicRows(caracteristicMapping);
        getModel().setCaracteristicMappingRows(Collections.emptyList());
        getModel().setCaracteristicMappingRows(caracteristicsRowModels);
        getHandler().getCaracteristicMappingTableModel().setRows(caracteristicsRowModels);

        sendMessage(t("tutti.flash.info.caracteristic.imported.in.protocol", file));

    }

}
