package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.time.DateFormatUtils;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.Date;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class SpeciesFrequencyLogRowModel extends AbstractSerializableBean {

    public static final String PROPERTY_LABEL = "label";

    private static final long serialVersionUID = 1L;

    protected Float lengthStep;

    protected Date date;

    protected IndividualObservationBatchRowModel obsRow;

    public Float getLengthStep() {
        return lengthStep;
    }

    public void setLengthStep(Float lengthStep) {
        this.lengthStep = lengthStep;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public IndividualObservationBatchRowModel getObsRow() {
        return obsRow;
    }

    public void setObsRow(IndividualObservationBatchRowModel obsRow) {
        this.obsRow = obsRow;
    }

    public String getLabel() {
        String formattedTime = DateFormatUtils.format(date, "HH:mm:ss");
        return formattedTime + " : " + lengthStep.toString();
    }

    public void setLabel(String label) {
        //do nothing, used by the editor
    }
}
