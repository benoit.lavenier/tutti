package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import jaxx.runtime.SwingUtil;
import org.jdesktop.swingx.JXTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * To rename the species for a species batch and all his children.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class RenameSpeciesBatchAction extends LongActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    protected Species selectedSpecies;

    public RenameSpeciesBatchAction(SpeciesBatchUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport = getModel().getSpeciesOrBenthosBatchUISupport();

        SpeciesBatchTableModel tableModel = handler.getTableModel();
        JXTable table = handler.getTable();
        int selectedRowIndex = SwingUtil.getSelectedModelRow(table);
        SpeciesBatchRowModel row = tableModel.getEntry(selectedRowIndex);

        List<Species> referents = getDataContext().getReferentSpecies();
        Map<String, Species> referentsById = Speciess.splitReferenceSpeciesByReferenceTaxonId(referents);

        SpeciesBatchUIModel model = handler.getModel();

        SampleCategory<?> firstSampleCategory = row.getFirstSampleCategory();
        Serializable categoryValue = firstSampleCategory.getCategoryValue();
        Collection<Species> speciesUsedForFirstCategory =
                model.getSpeciesUsed().get((CaracteristicQualitativeValue) categoryValue);

        List<Species> speciesList = new ArrayList<>(speciesOrBenthosBatchUISupport.getReferentSpeciesWithSurveyCode(true));
        speciesList.removeAll(speciesUsedForFirstCategory);

        List<Species> allSpeciesList = new ArrayList<>(getDataContext().getSpecies());
        allSpeciesList.removeAll(speciesUsedForFirstCategory);
        // do not show the benthos species
        allSpeciesList.removeAll(speciesOrBenthosBatchUISupport.getReferentOtherSpeciesWithSurveyCode(true));

        //remove the synonyms of the already used species
        Multimap<String, Species> speciesByReferent = Speciess.splitByReferenceTaxonId(allSpeciesList);
        for (Species species : speciesUsedForFirstCategory) {
            Collection<Species> synonyms = speciesByReferent.get(String.valueOf(species.getReferenceTaxonId()));
            allSpeciesList.removeAll(synonyms);
        }

        selectedSpecies = openAddSpeciesDialog(t("tutti.selectSpecies.title"), allSpeciesList, speciesList);

        if (selectedSpecies != null && !selectedSpecies.isReferenceTaxon()) {
            String decoratedSynonym = decorate(selectedSpecies, DecoratorService.FROM_PROTOCOL);
            String taxonId = String.valueOf(selectedSpecies.getReferenceTaxonId());
            selectedSpecies = referentsById.get(taxonId);
            String decoratedReferent = decorate(selectedSpecies, DecoratorService.FROM_PROTOCOL);
            sendMessage(t("tutti.flash.info.species.replaced", decoratedSynonym, decoratedReferent));
        }

        result &= selectedSpecies != null;
        return result;
    }

    @Override
    public void releaseAction() {
        selectedSpecies = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {

        SpeciesBatchTableModel tableModel = handler.getTableModel();
        JXTable table = handler.getTable();
        int selectedRowIndex = SwingUtil.getSelectedModelRow(table);
        SpeciesBatchRowModel row = tableModel.getEntry(selectedRowIndex);

        PersistenceService persistenceService = getContext().getPersistenceService();
        persistenceService.changeSpeciesBatchSpecies(row.getIdAsInt(), selectedSpecies);

        SampleCategory<?> firstSampleCategory = row.getFirstSampleCategory();
        Serializable categoryValue = firstSampleCategory.getCategoryValue();

        Collection<Species> speciesUsed = getModel().getSpeciesUsed()
                .get((CaracteristicQualitativeValue) categoryValue);
        speciesUsed.remove(row.getSpecies());
        changeChildrenSpecies(row, selectedSpecies);
        speciesUsed.add(selectedSpecies);
    }

    protected void changeChildrenSpecies(SpeciesBatchRowModel row, Species species) {
        row.setSpecies(species);
        List<SpeciesBatchRowModel> children = row.getChildBatch();
        if (children != null) {
            for (SpeciesBatchRowModel child : children) {
                changeChildrenSpecies(child, species);
            }
        }
    }
}
