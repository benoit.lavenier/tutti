package fr.ifremer.tutti.ui.swing.content.category;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableModel;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * Model of table of {@link SampleCategoryModelEntry}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class EditSampleCategoryModelTableModel extends AbstractTuttiTableModel<EditSampleCategoryModelRowModel> {

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<EditSampleCategoryModelRowModel> CARACTERISTIC = ColumnIdentifier.newId(
            EditSampleCategoryModelRowModel.PROPERTY_CARACTERISTIC,
            n("tutti.editSampleCategoryModel.table.header.caracteristic"),
            n("tutti.editSampleCategoryModel.table.header.caracteristic.tip"));

    public static final ColumnIdentifier<EditSampleCategoryModelRowModel> CODE = ColumnIdentifier.newId(
            EditSampleCategoryModelRowModel.PROPERTY_CODE,
            n("tutti.editSampleCategoryModel.table.header.code"),
            n("tutti.editSampleCategoryModel.table.header.code.tip"));

    public static final ColumnIdentifier<EditSampleCategoryModelRowModel> LABEL = ColumnIdentifier.newId(
            EditSampleCategoryModelRowModel.PROPERTY_LABEL,
            n("tutti.editSampleCategoryModel.table.header.label"),
            n("tutti.editSampleCategoryModel.table.header.label.tip"));

    public EditSampleCategoryModelTableModel(TableColumnModelExt columnModel) {
        super(columnModel, false, false);

        setNoneEditableCols(CARACTERISTIC);
    }

    @Override
    public EditSampleCategoryModelRowModel createNewRow() {
        EditSampleCategoryModelRowModel result = new EditSampleCategoryModelRowModel();

        // by default empty row is not valid
        result.setValid(false);
        return result;
    }

}
