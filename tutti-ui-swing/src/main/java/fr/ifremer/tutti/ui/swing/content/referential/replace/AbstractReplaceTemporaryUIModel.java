package fr.ifremer.tutti.ui.swing.content.referential.replace;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

import java.util.List;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class AbstractReplaceTemporaryUIModel<E extends TuttiReferentialEntity> extends AbstractTuttiBeanUIModel<E, AbstractReplaceTemporaryUIModel<E>> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SOURCE_LIST = "sourceList";

    public static final String PROPERTY_TARGET_LIST = "targetList";

    public static final String PROPERTY_SELECTED_SOURCE = "selectedSource";

    public static final String PROPERTY_SELECTED_TARGET = "selectedTarget";

    public static final String PROPERTY_DELETE = "delete";

    protected List<E> sourceList;

    protected List<E> targetList;

    protected E selectedSource;

    protected E selectedTarget;

    protected boolean delete;

    public AbstractReplaceTemporaryUIModel() {
        super(null, null);
    }

    public List<E> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<E> sourceList) {
        this.sourceList = sourceList;
        firePropertyChange(PROPERTY_SOURCE_LIST, null, sourceList);
    }

    public List<E> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<E> targetList) {
        this.targetList = targetList;
        firePropertyChange(PROPERTY_TARGET_LIST, null, targetList);
    }

    public E getSelectedTarget() {
        return selectedTarget;
    }

    public void setSelectedTarget(E selectedTarget) {
        E oldvalue = getSelectedTarget();
        this.selectedTarget = selectedTarget;
        firePropertyChange(PROPERTY_SELECTED_TARGET, oldvalue, selectedTarget);
    }

    public E getSelectedSource() {
        return selectedSource;
    }

    public void setSelectedSource(E selectedSource) {
        E oldvalue = getSelectedSource();
        this.selectedSource = selectedSource;
        firePropertyChange(PROPERTY_SELECTED_SOURCE, oldvalue, selectedSource);
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
        firePropertyChange(PROPERTY_DELETE, null/*force boolean propagation*/, delete);
    }

    @Override
    protected E newEntity() {
        return null;
    }
}