package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class AddSpeciesAction extends SimpleActionSupport<CalcifiedPiecesSamplingEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AddSpeciesAction.class);

    public AddSpeciesAction(CalcifiedPiecesSamplingEditorUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(CalcifiedPiecesSamplingEditorUI ui) {
        BeanFilterableComboBox<Species> speciesComboBox = ui.getSpeciesComboBox();
        Species species = (Species) speciesComboBox.getSelectedItem();

        List<CalcifiedPiecesSamplingEditorRowModel> newRows = new ArrayList<>();
        if (ui.getMaturityCheckBox().isSelected()) {

            CalcifiedPiecesSamplingEditorRowModel  immatureRow = ui.getHandler().createNewRow(species, false);
            newRows.add(immatureRow);

            CalcifiedPiecesSamplingEditorRowModel matureRow = ui.getHandler().createNewRow(species, true);
            newRows.add(matureRow);

        } else {

            CalcifiedPiecesSamplingEditorRowModel newRow = ui.getHandler().createNewRow(species, null);
            newRows.add(newRow);

        }

        List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = ui.getModel().getCpsRows();
        cpsRows.addAll(newRows);
        cpsRows.sort(CalcifiedPiecesSamplingEditorRowModel.COMPARATOR);

        speciesComboBox.removeItem(species);

        int firstRowIndex = cpsRows.indexOf(newRows.get(0));
        int lastRowIndex = cpsRows.indexOf(newRows.get(newRows.size() - 1));

        CalcifiedPiecesSamplingEditorTableModel tableModel =
                (CalcifiedPiecesSamplingEditorTableModel) ui.getCpsTable().getModel();

        if (log.isInfoEnabled()) {
            log.info(cpsRows);
            log.info(tableModel.getRows());
            log.info("insert in  " +firstRowIndex + " " + lastRowIndex );
        }

        tableModel.fireTableRowsInserted(firstRowIndex, lastRowIndex);
        // select this new row
        SwingUtil.setSelectionInterval(ui.getCpsTable(), firstRowIndex);
    }

}
