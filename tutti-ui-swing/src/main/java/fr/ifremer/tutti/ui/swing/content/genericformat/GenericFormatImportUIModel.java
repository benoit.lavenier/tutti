package fr.ifremer.tutti.ui.swing.content.genericformat;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportConfiguration;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatValidateFileResult;
import fr.ifremer.tutti.ui.swing.content.genericformat.tree.ProgramSelectTreeNode;
import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;
import java.util.Collections;

/**
 * Created on 2/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_PROGRAM = "program";

    public static final String PROPERTY_IMPORT_FILE = "importFile";

    public static final String PROPERTY_CLEAN_WEIGHTS = "cleanWeights";

    public static final String PROPERTY_CHECK_WEIGHTS = "checkWeights";

    public static final String PROPERTY_IMPORT_ATTACHMENTS = "importAttachments";

    public static final String PROPERTY_IMPORT_SPECIES = "importSpecies";

    public static final String PROPERTY_IMPORT_BENTHOS = "importBenthos";

    public static final String PROPERTY_IMPORT_MARINE_LITTER = "importMarineLitter";

    public static final String PROPERTY_IMPORT_ACCIDENTAL_CATCH = "importAccidentalCatch";

    public static final String PROPERTY_IMPORT_INDIVIDUAL_OBSERVATION = "importIndividualObservation";

    public static final String PROPERTY_UPDATE_CRUISES = "updateCruises";

    public static final String PROPERTY_UPDATE_OPERATIONS = "updateOperations";

    public static final String PROPERTY_CAN_VALIDATE = "canValidate";

    public static final String PROPERTY_VALIDATE_RESULT = "validateResult";

    public static final String PROPERTY_VALIDATE_VALID = "validateValid";

    public static final String PROPERTY_VALIDATE_DONE = "validateDone";

    public static final String PROPERTY_CAN_IMPORT = "canImport";

    public static final String PROPERTY_IMPORT_RESULT = "importResult";

    public static final String PROPERTY_IMPORT_DONE = "importDone";

    public static final String PROPERTY_IMPORT_VALID = "importValid";

    public static final String PROPERTY_DATA_SELECTED = "dataSelected";

    public static final String PROPERTY_AUTHORIZE_OBSOLETE_REFERENTIALS = "authorizeObsoleteReferentials";

    private Program program;

    private File importFile;

    private boolean updateCruises = true;

    private boolean updateOperations = true;

    private boolean importSpecies = true;

    private boolean importBenthos = true;

    private boolean importMarineLitter = true;

    private boolean importAccidentalCatch = true;

    private boolean importIndividualObservation = true;

    private boolean importAttachments = true;

    private boolean cleanWeights;

    private boolean checkWeights;

    private boolean canValidate;

    private boolean authorizeObsoleteReferentials;

    private File validateReportFile;

    private GenericFormatValidateFileResult validateResult;

    private boolean canImport;

    private File importReportFile;

    private GenericFormatImportResult importResult;

    private ProgramSelectTreeNode rootNode;

    private boolean dataSelected;

    public GenericFormatImportConfiguration toValidateImportFileConfiguration(int maximumRowsInErrorsPerFile) {

        GenericFormatImportConfiguration configuration = new GenericFormatImportConfiguration();

        ProgramDataModel selectedDataModel = new ProgramDataModel(program, Collections.<CruiseDataModel>emptySet());
        configuration.setDataToExport(selectedDataModel);

        configuration.setMaximumRowsInErrorPerFile(maximumRowsInErrorsPerFile);

        configuration.setImportSpecies(importSpecies);
        configuration.setImportBenthos(importBenthos);
        configuration.setImportMarineLitter(importMarineLitter);
        configuration.setImportAccidentalCatch(importAccidentalCatch);
        configuration.setImportIndividualObservation(importIndividualObservation);
        configuration.setImportAttachments(importAttachments);
        configuration.setAuthorizeObsoleteReferentials(authorizeObsoleteReferentials);

        configuration.setCleanWeights(cleanWeights);
        configuration.setCheckWeights(checkWeights);

        configuration.setImportFile(importFile);
        configuration.setReportFile(validateReportFile);

        return configuration;

    }

    public GenericFormatImportConfiguration toImportConfiguration() {

        GenericFormatImportConfiguration configuration = new GenericFormatImportConfiguration();

        ProgramDataModel selectedDataModel = rootNode.getSelectedDataModel();
        configuration.setDataToExport(selectedDataModel);

        configuration.setUpdateCruises(updateCruises);
        configuration.setUpdateOperations(updateOperations);

        configuration.setImportSpecies(importSpecies);
        configuration.setImportBenthos(importBenthos);
        configuration.setImportMarineLitter(importMarineLitter);
        configuration.setImportAccidentalCatch(importAccidentalCatch);
        configuration.setImportIndividualObservation(importIndividualObservation);
        configuration.setImportAttachments(importAttachments);
        configuration.setAuthorizeObsoleteReferentials(authorizeObsoleteReferentials);

        configuration.setCleanWeights(cleanWeights);
        configuration.setCheckWeights(checkWeights);
        configuration.setImportFile(importFile);
        configuration.setReportFile(importReportFile);

        return configuration;

    }

    public boolean isSelectedDataExists() {
        return rootNode.isSelectedDataExists();
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        Object oldValue = getProgram();
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, oldValue, program);
    }

    public File getImportFile() {
        return importFile;
    }

    public void setImportFile(File importFile) {
        Object oldValue = getImportFile();
        this.importFile = importFile;
        firePropertyChange(PROPERTY_IMPORT_FILE, oldValue, importFile);
        firePropertyChange(PROPERTY_VALIDATE_VALID, null, isValidateValid());
    }

    public boolean isCleanWeights() {
        return cleanWeights;
    }

    public void setCleanWeights(boolean cleanWeights) {
        this.cleanWeights = cleanWeights;
        firePropertyChange(PROPERTY_CLEAN_WEIGHTS, null, cleanWeights);
    }

    public boolean isCheckWeights() {
        return checkWeights;
    }

    public void setCheckWeights(boolean checkWeights) {
        this.checkWeights = checkWeights;
        firePropertyChange(PROPERTY_CHECK_WEIGHTS, null, checkWeights);
    }

    public boolean isUpdateCruises() {
        return updateCruises;
    }

    public void setUpdateCruises(boolean updateCruises) {
        this.updateCruises = updateCruises;
        firePropertyChange(PROPERTY_UPDATE_CRUISES, null, updateCruises);
    }

    public boolean isUpdateOperations() {
        return updateOperations;
    }

    public void setUpdateOperations(boolean updateOperations) {
        this.updateOperations = updateOperations;
        firePropertyChange(PROPERTY_UPDATE_OPERATIONS, null, updateOperations);
    }

    public boolean isImportSpecies() {
        return importSpecies;
    }

    public void setImportSpecies(boolean importSpecies) {
        this.importSpecies = importSpecies;
        firePropertyChange(PROPERTY_IMPORT_SPECIES, null, importSpecies);
    }

    public boolean isImportBenthos() {
        return importBenthos;
    }

    public void setImportBenthos(boolean importBenthos) {
        this.importBenthos = importBenthos;
        firePropertyChange(PROPERTY_IMPORT_BENTHOS, null, importBenthos);
    }

    public boolean isImportMarineLitter() {
        return importMarineLitter;
    }

    public void setImportMarineLitter(boolean importMarineLitter) {
        this.importMarineLitter = importMarineLitter;
        firePropertyChange(PROPERTY_IMPORT_MARINE_LITTER, null, importMarineLitter);
    }

    public boolean isImportAccidentalCatch() {
        return importAccidentalCatch;
    }

    public void setImportAccidentalCatch(boolean importAccidentalCatch) {
        this.importAccidentalCatch = importAccidentalCatch;
        firePropertyChange(PROPERTY_IMPORT_ACCIDENTAL_CATCH, null, importAccidentalCatch);
    }

    public boolean isImportIndividualObservation() {
        return importIndividualObservation;
    }

    public void setImportIndividualObservation(boolean importIndividualObservation) {
        this.importIndividualObservation = importIndividualObservation;
        firePropertyChange(PROPERTY_IMPORT_INDIVIDUAL_OBSERVATION, null, importIndividualObservation);
    }

    public boolean isImportAttachments() {
        return importAttachments;
    }

    public void setImportAttachments(boolean importAttachments) {
        this.importAttachments = importAttachments;
        firePropertyChange(PROPERTY_IMPORT_ATTACHMENTS, null, importAttachments);
    }

    public File getValidateReportFile() {
        return validateReportFile;
    }

    public void setValidateReportFile(File validateReportFile) {
        this.validateReportFile = validateReportFile;
    }

    public boolean isCanValidate() {
        return canValidate;
    }

    public void setCanValidate(boolean canValidate) {
        this.canValidate = canValidate;
        firePropertyChange(PROPERTY_CAN_VALIDATE, null, canValidate);
    }

    public GenericFormatValidateFileResult getValidateResult() {
        return validateResult;
    }

    public void setValidateResult(GenericFormatValidateFileResult validateResult) {
        this.validateResult = validateResult;
        firePropertyChange(PROPERTY_VALIDATE_RESULT, null, validateResult);
        firePropertyChange(PROPERTY_VALIDATE_DONE, null, isValidateDone());
        firePropertyChange(PROPERTY_VALIDATE_VALID, null, isValidateValid());
    }

    public boolean isValidateDone() {
        return validateResult != null;
    }

    public boolean isValidateValid() {
        return validateResult != null && validateResult.isValid();
    }

    public boolean isCanImport() {
        return canImport;
    }

    public void setCanImport(boolean canImport) {
        this.canImport = canImport;
        firePropertyChange(PROPERTY_CAN_IMPORT, null, canImport);
    }

    public File getImportReportFile() {
        return importReportFile;
    }

    public void setImportReportFile(File importReportFile) {
        this.importReportFile = importReportFile;
    }

    public GenericFormatImportResult getImportResult() {
        return importResult;
    }

    public void setImportResult(GenericFormatImportResult importResult) {
        this.importResult = importResult;
        firePropertyChange(PROPERTY_IMPORT_RESULT, null, importResult);
        firePropertyChange(PROPERTY_IMPORT_DONE, null, isImportDone());
        firePropertyChange(PROPERTY_IMPORT_VALID, null, isImportValid());
    }

    public boolean isDataSelected() {
        return dataSelected;
    }

    public void setDataSelected(boolean dataSelected) {
        this.dataSelected = dataSelected;
        firePropertyChange(PROPERTY_DATA_SELECTED, null, dataSelected);
    }

    public boolean isAuthorizeObsoleteReferentials() {
        return authorizeObsoleteReferentials;
    }

    public void setAuthorizeObsoleteReferentials(boolean authorizeObsoleteReferentials) {
        Object oldValue = isAuthorizeObsoleteReferentials();
        this.authorizeObsoleteReferentials = authorizeObsoleteReferentials;
        firePropertyChange(PROPERTY_AUTHORIZE_OBSOLETE_REFERENTIALS, oldValue, authorizeObsoleteReferentials);
    }

    public boolean isImportDone() {
        return importResult != null;
    }

    public boolean isImportValid() {
        return importResult != null && importResult.isValid();
    }

    public boolean computeIsCanValidate() {
        return program != null && importFile != null && importFile.exists();
    }

    public boolean computeIsCanImport() {
        return isValidateDone() && isValidateValid() && isDataSelected() && importResult == null;
    }

    public void setRootNode(ProgramSelectTreeNode rootNode) {
        this.rootNode = rootNode;
    }
}
