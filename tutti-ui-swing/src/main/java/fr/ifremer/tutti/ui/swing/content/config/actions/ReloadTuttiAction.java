package fr.ifremer.tutti.ui.swing.content.config.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.RunTutti;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractChangeScreenAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * To reload Tutti application.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.3
 */
public class ReloadTuttiAction extends AbstractChangeScreenAction {

    public ReloadTuttiAction(MainUIHandler handler) {
        super(handler, true, null);
        setActionDescription(t("tutti.main.action.reloadTutti"));
    }

    @Override
    public void doAction() throws Exception {

        // Close the application, will exit and restart application
        RunTutti.closeTutti(getHandler(), RunTutti.RESTART_EXIT_CODE);
    }

}
