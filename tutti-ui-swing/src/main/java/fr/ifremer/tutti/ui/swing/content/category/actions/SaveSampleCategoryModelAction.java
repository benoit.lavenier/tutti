package fr.ifremer.tutti.ui.swing.content.category.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUI;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUIHandler;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * To save the sample category model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SaveSampleCategoryModelAction extends LongActionSupport<EditSampleCategoryModelUIModel, EditSampleCategoryModelUI, EditSampleCategoryModelUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SaveSampleCategoryModelAction.class);

    public SaveSampleCategoryModelAction(EditSampleCategoryModelUIHandler handler) {
        super(handler, true);
    }

    @Override
    public void doAction() throws Exception {
        EditSampleCategoryModelUIModel model = getModel();

        SampleCategoryModel bean = model.toEntity();

        if (log.isInfoEnabled()) {
            log.info("Will save sampleCategoryModel: " + bean);
        }
        getConfig().setSampleCategoryModel(bean);

        getConfig().save();

        getDataContext().setSampleCategoryModel(bean);

        model.setModify(false);

    }

    @Override
    public void postSuccessAction() {
        sendMessage(t("tutti.editSampleCategoryModel.saved"));
    }
}
