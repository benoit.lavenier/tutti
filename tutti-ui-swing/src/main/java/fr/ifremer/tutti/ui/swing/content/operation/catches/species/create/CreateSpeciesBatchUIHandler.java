package fr.ifremer.tutti.ui.swing.content.operation.catches.species.create;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.split.SplitSpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.split.SplitSpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;

import javax.swing.JComponent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Handler of {@link CreateSpeciesBatchUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class CreateSpeciesBatchUIHandler extends AbstractTuttiTableUIHandler<SplitSpeciesBatchRowModel, CreateSpeciesBatchUIModel, CreateSpeciesBatchUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CreateSpeciesBatchUIHandler.class);

    public static final PropertyChangeListener PROPERTY_WEIGHT_CHANGED_LISTENER = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getNewValue() != null) {
                ((SplitSpeciesBatchRowModel) evt.getSource()).setSelected(true);
            }
        }
    };

    protected final PropertyChangeListener PROPERTY_SELECTED_CATEGORY_CHANGED_LISTENER = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            // can change the selected category
            CreateSpeciesBatchUIModel source =
                    (CreateSpeciesBatchUIModel) evt.getSource();

            // when selected category change, sample total weight is reset
            source.setSampleWeight(null);

            SampleCategoryModelEntry newValue =
                    (SampleCategoryModelEntry) evt.getNewValue();
            generateTableModel(newValue);
        }
    };

    protected final PropertyChangeListener PROPERTY_SPECIES_CHANGED_LISTENER = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            CreateSpeciesBatchUIModel source = (CreateSpeciesBatchUIModel) evt.getSource();

            Species newValue = (Species) evt.getNewValue();

            if (log.isDebugEnabled()) {
                log.debug("New Selected species " + (newValue == null ? null : decorate(newValue)));
            }

            if (newValue == null || source.getSpeciesUsed() == null) {

                // reset V/HV category
                source.setSampleCategory(null);

                source.setSpeciesProtocolFrequencyMode(null);

            } else {

                // select the last used V/HV category used, or sorted if no batch has been created

                List<CaracteristicQualitativeValue> qualitativeValues =
                        CreateSpeciesBatchUIHandler.this.ui.getSampleCategoryComboBox().getData();

                CaracteristicQualitativeValue newCategory = null;

                CaracteristicQualitativeValue defaultCategory = getModel().getLastSampleCategoryUsed();
                if (defaultCategory == null) {
                    defaultCategory = sortedValue;
                }

                for (CaracteristicQualitativeValue qualitativeValue : qualitativeValues) {
                    if (source.isSpeciesAndCategoryAvailable(newValue, qualitativeValue)) {
                        newCategory = qualitativeValue;

                        if (newCategory.equals(defaultCategory)) {
                            break;
                        }
                    }
                }
                source.setSampleCategory(newCategory);

                // reset selected category
                if (log.isDebugEnabled()) {
                    log.debug("Remove selected category before changing the categories...");
                }
                source.setSelectedCategory(null);

                // compute the selected sample category


                SampleCategoryModelEntry selectedCategory = source.getSpeciesOrBenthosBatchUISupport().getBestFirstSampleCategory(
                        getUI().getCategoryComboBox().getData(),
                        newValue
                );

                if (log.isDebugEnabled()) {
                    log.debug("Selected category : " + selectedCategory);
                }

                // set new selected category
                source.setSelectedCategory(selectedCategory);

                // get species protocol to check the measurement method
                SpeciesProtocol speciesProtocol = TuttiProtocols.getSpeciesProtocol(getDataContext().getProtocol(),
                                                                                    newValue.getReferenceTaxonId());

                CreateSpeciesBatchUIModel.SpeciesProtocolFrequencyMode frequencyMode = null;
                if (speciesProtocol != null) {
                    if (speciesProtocol.getLengthStepPmfmId() != null) {
                        frequencyMode = CreateSpeciesBatchUIModel.SpeciesProtocolFrequencyMode.MEASURE;
                    } else {
                        frequencyMode = CreateSpeciesBatchUIModel.SpeciesProtocolFrequencyMode.COUNT;
                    }
                }
                source.setSpeciesProtocolFrequencyMode(frequencyMode);
            }
        }
    };

    /**
     * Qualitative value for the Vrac.
     *
     * @since 2.5
     */
    protected CaracteristicQualitativeValue sortedValue;
//
//    /**
//     * Sample categories model.
//     *
//     * @since 2.4
//     */
//    protected SampleCategoryModel sampleCategoryModel;

    public CreateSpeciesBatchUIHandler() {
        super(SplitSpeciesBatchRowModel.PROPERTY_SELECTED,
              SplitSpeciesBatchRowModel.PROPERTY_CATEGORY_VALUE,
              SplitSpeciesBatchRowModel.PROPERTY_WEIGHT);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public SplitSpeciesBatchTableModel getTableModel() {
        return (SplitSpeciesBatchTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getTable();
    }

    @Override
    protected boolean isRowValid(SplitSpeciesBatchRowModel row) {
        return row.isSelected();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<SplitSpeciesBatchRowModel> rowMonitor, SplitSpeciesBatchRowModel row) {
        if (rowMonitor.wasModified()) {

            if (row.isValid()) {
                if (log.isInfoEnabled()) {
                    log.info("Change row that was modified and valid");
                }
            }

            rowMonitor.clearModified();
        }
    }

    @Override
    protected void onAfterSelectedRowChanged(int oldRowIndex,
                                             SplitSpeciesBatchRowModel oldRow,
                                             int newRowIndex,
                                             SplitSpeciesBatchRowModel newRow) {
        super.onAfterSelectedRowChanged(oldRowIndex, oldRow, newRowIndex, newRow);
        if (newRow != null) {

            // Recompute the valid state of the row
            recomputeRowValidState(newRow);

            // Need to recompute the sample weight
            computeSampleWeight();
        }
    }

    @Override
    protected void onRowModified(int rowIndex,
                                 SplitSpeciesBatchRowModel row,
                                 String propertyName,
                                 Object oldValue,
                                 Object newValue) {

        // Recompute the valid state of the row
        recomputeRowValidState(row);

        // Need to recompute the sample weight
        computeSampleWeight();
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(CreateSpeciesBatchUI ui) {

        super.beforeInit(ui);

        SampleCategoryModel sampleCategoryModel = getDataContext().getSampleCategoryModel();

        SampleCategoryModelEntry caracteristic = sampleCategoryModel.getCategoryById(sampleCategoryModel.getFirstCategoryId());

        Integer vracId = QualitativeValueId.SORTED_VRAC.getValue();

        CaracteristicQualitativeValue vracValue = null;
        for (CaracteristicQualitativeValue caracteristicQualitativeValue : caracteristic.getCaracteristic().getQualitativeValue()) {

            if (vracId.equals(caracteristicQualitativeValue.getIdAsInt())) {
                vracValue = caracteristicQualitativeValue;
                break;
            }
        }
        Objects.requireNonNull(vracValue, "Could not found vrac qualitative value");
        sortedValue = vracValue;

        SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport = ui.getContextValue(SpeciesOrBenthosBatchUISupport.class, ui.getSpeciesOrBenthosContext());
        CreateSpeciesBatchUIModel model = new CreateSpeciesBatchUIModel(speciesOrBenthosBatchUISupport, sampleCategoryModel);
        this.ui.setContextValue(model);
        listModelIsModify(model);
    }

    @Override
    public void afterInit(CreateSpeciesBatchUI ui) {

        initUI(this.ui);

        CreateSpeciesBatchUIModel model = getModel();

        initBeanFilterableComboBox(this.ui.getSpeciesComboBox(),
                                   new ArrayList<>(),
                                   null,
                                   DecoratorService.FROM_PROTOCOL);

        List<SampleCategoryModelEntry> categories = new ArrayList<>();

        // add all categories
        categories.addAll(model.getSampleCategoryModel().getCategory());

        // remove the first one (V/HV)
        categories.remove(0);

        initBeanFilterableComboBox(this.ui.getCategoryComboBox(),
                                   new ArrayList<>(categories),
                                   null);

        Caracteristic caracteristic =
                getPersistenceService().getSortedUnsortedCaracteristic();

        initBeanFilterableComboBox(this.ui.getSampleCategoryComboBox(),
                                   new ArrayList<>(caracteristic.getQualitativeValue()),
                                   null);


        model.addPropertyChangeListener(CreateSpeciesBatchUIModel.PROPERTY_SPECIES, PROPERTY_SPECIES_CHANGED_LISTENER);

        // when selected category changed, regenerate the table model + add inside some default rows
        model.addPropertyChangeListener(CreateSpeciesBatchUIModel.PROPERTY_SELECTED_CATEGORY, PROPERTY_SELECTED_CATEGORY_CHANGED_LISTENER);

        generateTableModel(null);

        initTable(getTable());

        listenValidatorValid(this.ui.getValidator(), model);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getSpeciesComboBox();
    }

    @Override
    public void onCloseUI() {

        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        clearValidators();

        // evict model from validator
        ui.getValidator().setBean(null);

        // when canceling always invalid model
        getModel().setValid(false);

        EditSpeciesBatchPanelUI parent = getParentContainer(EditSpeciesBatchPanelUI.class);
        parent.switchToEditBatch();

    }

    @Override
    public SwingValidator<CreateSpeciesBatchUIModel> getValidator() {
        return ui.getValidator();
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void openUI(SpeciesBatchUIModel batchModel) {

        CreateSpeciesBatchUIModel model = getModel();

        // connect model to validator
        ui.getValidator().setBean(model);

        model.setSpecies(null);
        model.setSampleCategory(null);
        model.setBatchSampleCategoryWeight(null);
        model.setBatchWeight(null);
        model.setBatchCount(null);

        List<Species> speciesToUse = new ArrayList<>();

        Multimap<CaracteristicQualitativeValue, Species> speciesUsed = model.getSpeciesUsed();
        speciesUsed.clear();

        if (batchModel != null) {

            speciesUsed.putAll(batchModel.getSpeciesUsed());

            // compute which species can still be used

            List<Species> allSpecies = model.getSpeciesOrBenthosBatchUISupport().getReferentSpeciesWithSurveyCode(true);

            speciesToUse.addAll(allSpecies);
        }

        model.setAvailableSpecies(speciesToUse);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void computeSampleWeight() {

        Optional<Float> result = getTableModel().getTotalWeight();
//        List<SplitSpeciesBatchRowModel> rows = getTableModel().getRows();
//        for (SplitSpeciesBatchRowModel row : rows) {
//            if (row.isSelected()) {
//                Float weight = row.getWeight();
//                if (weight != null) {
//                    if (result == null) {
//                        result = 0f;
//                    }
//                    result += weight;
//                }
//            }
//        }
        getModel().setSampleWeight(result.orElse(null));
    }

    protected void generateTableModel(SampleCategoryModelEntry category) {

        if (log.isDebugEnabled()) {
            log.debug("Generate table model for category " + category);
        }
        CreateSpeciesBatchUIModel model = getModel();

        // when generate a new table model, then reset previous rows from model
        model.setRows(null);

        Caracteristic data = null;

        JXTable table = getTable();

        DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        { // Selection

            addBooleanColumnToModel(columnModel,
                                    SplitSpeciesBatchTableModel.SELECTED,
                                    table);
        }

        boolean editableCategoryValue = false;
        if (category != null) {

            if (!category.getCaracteristic().isQualitativeValueEmpty()) {

                // qualitative category
                data = category.getCaracteristic();
            } else {
                editableCategoryValue = true;
                addFloatColumnToModel(columnModel,
                                      SplitSpeciesBatchTableModel.EDITABLE_CATEGORY_VALUE,
                                      TuttiUI.DECIMAL1_PATTERN,
                                      table);
            }

            if (data != null) {

                if (log.isDebugEnabled()) {
                    log.debug("Got " + data.sizeQualitativeValue() + " qualitative data to add");
                }
                addColumnToModel(columnModel,
                                 null,
                                 newTableCellRender(CaracteristicQualitativeValue.class),
                                 SplitSpeciesBatchTableModel.READ_ONLY_CATEGORY_VALUE);
            }
            { // Weight

                addFloatColumnToModel(columnModel,
                                      SplitSpeciesBatchTableModel.WEIGHT,
                                      model.getSpeciesOrBenthosBatchUISupport().getWeightUnit(),
                                      table);
            }
        }

        // create table model
        SplitSpeciesBatchTableModel tableModel = new SplitSpeciesBatchTableModel(columnModel,
                                                                                 model,
                                                                                 editableCategoryValue,
                                                                                 false);

        // remove all listener on tables we could add before
        uninstallTableSaveOnRowChangedSelectionListener();
        uninstallTableKeyListener(getTable());

        if (log.isDebugEnabled()) {
            log.debug("Install new table model " + tableModel);
        }
        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        // install table listeners
        installTableSaveOnRowChangedSelectionListener();
        installTableKeyListener(columnModel, table);

        // fill datas

        List<SplitSpeciesBatchRowModel> rows = new ArrayList<>();

        if (data != null) {

            // add a row for each qualitive value
            for (CaracteristicQualitativeValue qualitativeValue : data.getQualitativeValue()) {
                if (log.isDebugEnabled()) {
                    log.debug("Add QV: " + qualitativeValue);
                }
                SplitSpeciesBatchRowModel newRow = tableModel.createNewRow();
                newRow.setCategoryValue(qualitativeValue);
                newRow.addPropertyChangeListener(SplitSpeciesBatchRowModel.PROPERTY_WEIGHT, PROPERTY_WEIGHT_CHANGED_LISTENER);
                rows.add(newRow);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Will add " + rows.size() + " rows in table model " +
                              "(can add a first empty row? " + editableCategoryValue + ").");
        }

        model.setRows(rows);
    }

}
