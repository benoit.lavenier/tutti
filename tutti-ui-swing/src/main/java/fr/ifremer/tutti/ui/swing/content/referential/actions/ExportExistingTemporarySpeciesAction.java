package fr.ifremer.tutti.ui.swing.content.referential.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.service.referential.ReferentialTemporarySpeciesService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUI;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIHandler;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Export an example file of temporary species referential.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ExportExistingTemporarySpeciesAction extends LongActionSupport<ManageTemporaryReferentialUIModel, ManageTemporaryReferentialUI, ManageTemporaryReferentialUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ExportExistingTemporarySpeciesAction.class);

    private File file;

    public ExportExistingTemporarySpeciesAction(ManageTemporaryReferentialUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction() && getUI().getExportExistingSpeciesButton().isEnabled();

        if (doAction) {

            // choose file to export
            file = saveFile(
                    "exportTemporarySpecies",
                    "csv",
                    t("tutti.manageTemporaryReferential.title.choose.exportExistingTemporarySpeciesFile"),
                    t("tutti.manageTemporaryReferential.action.chooseExistingReferentialSpeciesFile.export"),
                    "^.*\\.csv", t("tutti.common.file.csv")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will export existing species temporary " +
                     "referential to file: " + file);
        }

        ReferentialTemporarySpeciesService service = getContext().getReferentialTemporarySpeciesService();
        service.exportExistingTemporarySpecies(file);

    }

    @Override
    public void postSuccessAction() {

        getHandler().resetComboBoxAction(getUI().getSpeciesActionComboBox());
        super.postSuccessAction();
        sendMessage(t("tutti.manageTemporaryReferential.action.chooseExistingReferentialSpeciesFile.export.success", file));

    }
}
