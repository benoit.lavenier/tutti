<!--
  #%L
  Tutti :: UI
  %%
  Copyright (C) 2012 - 2014 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='editAccidentalBatchTopPanel' layout='{new BorderLayout()}'
        decorator='help'
        implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;AccidentalBatchUIModel, AccidentalBatchUIHandler&gt;'>

  <import>
    fr.ifremer.tutti.ui.swing.TuttiHelpBroker
    fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel
    fr.ifremer.tutti.ui.swing.util.TuttiUI
    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil
    fr.ifremer.tutti.ui.swing.util.attachment.ButtonAttachment

    org.jdesktop.swingx.JXTable

    javax.swing.ListSelectionModel

    java.awt.Color

    static org.nuiton.i18n.I18n.t
  </import>

  <script><![CDATA[

public AccidentalBatchUI(TuttiUI<?,?> parentUI) {
    TuttiUIUtil.setParentUI(this, parentUI);
}
  ]]></script>

  <AccidentalBatchUIModel id='model'
                          initializer='getContextValue(AccidentalBatchUIModel.class)'/>

  <BeanValidator id='validator' bean='model'
                 uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
  </BeanValidator>

  <TuttiHelpBroker id='broker'
                   constructorParams='"tutti.editAccidentalBatch.help"'/>

  <JToolBar id='accidentalBatchTabToolBar'>
    <JMenuBar id='menu'>
      <JMenu id='menuAction'>
        <JMenuItem id='importMultiPostButton'/>
        <JMenuItem id='exportMultiPostButton'/>
      </JMenu>
    </JMenuBar>
    <ButtonAttachment id='accidentalBatchAttachmentsButton'
                      constructorParams='handler.getContext(), getContextValue(EditCatchesUIModel.class)'/>
  </JToolBar>

  <JPopupMenu id='tablePopup'>
    <JMenuItem id='removeAccidentalBatchMenu'/>
  </JPopupMenu>

  <JPanel id='tableToolbar'
          constraints='BorderLayout.NORTH'
          layout='{new BorderLayout()}'
          styleClass="buttonPanel">
    <JPanel layout='{new GridLayout(1,0)}'
            constraints='BorderLayout.WEST'>
      <JButton id='createAccidentalBatchButton'/>
    </JPanel>
  </JPanel>

  <JScrollPane id='tableScrollPane' constraints='BorderLayout.CENTER'>
    <JXTable id='table'
             onMouseClicked='handler.autoSelectRowInTable(event, tablePopup)'
             onKeyPressed='handler.openRowMenu(event, tablePopup)'/>
  </JScrollPane>

  <!--JPanel constraints='BorderLayout.SOUTH'
          minimumSize="{new Dimension(10,30)}"
          maximumSize="{new Dimension(10,30)}"
          preferredSize="{new Dimension(10,30)}"/-->
</JPanel>
