package fr.ifremer.tutti.ui.swing.content.operation.catches.species;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportResult;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Contient la logique propre aux espèces et benthos.
 *
 * Created on 17/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public abstract class SpeciesOrBenthosBatchUISupport {

    public static final String SPECIES = "SPECIES";
    public static final String BENTHOS = "BENTHOS";
    public static final String PROPERTY_TOTAL_COMPUTED_WEIGHT = "totalComputedWeight";
    public static final String PROPERTY_TOTAL_SORTED_WEIGHT = "totalSortedWeight";
    public static final String PROPERTY_TOTAL_UNSORTED_COMPUTED_WEIGHT = "totalUnsortedComputedWeight";
    public static final String PROPERTY_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT = "totalSampleSortedComputedWeight";
    public static final String PROPERTY_TOTAL_INERT_WEIGHT = "totalInertWeight";
    public static final String PROPERTY_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT = "totalLivingNotItemizedWeight";

    protected final TuttiUIContext context;

    protected final EditCatchesUIModel catchesUIModel;

    protected final Map<String, String> catchesUIModelPropertiesMapping;

    protected final WeightUnit weightUnit;

    protected SpeciesOrBenthosBatchUISupport(TuttiUIContext context,
                                             EditCatchesUIModel catchesUIModel,
                                             Map<String, String> catchesUIModelPropertiesMapping,
                                             WeightUnit weightUnit) {
        this.context = context;
        this.catchesUIModel = catchesUIModel;
        this.catchesUIModelPropertiesMapping = catchesUIModelPropertiesMapping;
        this.weightUnit = weightUnit;
    }

    public EditCatchesUIModel getCatchesUIModel() {
        return catchesUIModel;
    }

    public Map<String, String> getCatchesUIModelPropertiesMapping() {
        return catchesUIModelPropertiesMapping;
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }

    protected PersistenceService getPersistenceService() {
        return context.getPersistenceService();
    }

    public abstract BatchContainer<SpeciesBatch> getRootSpeciesBatch(Integer batchId);

    public abstract String getTitle();

    public abstract boolean canPupitriImport();

    public abstract boolean canPsionImport();

    public abstract boolean canBigfinImport();

    public abstract Float getTotalComputedWeight();

    public abstract void setTotalComputedWeight(Float totalComputedWeight);

    public abstract ComputableData<Float> getTotalSortedComputedOrNotWeight();

    public abstract Float getTotalSortedWeight();

    public abstract void setTotalSortedWeight(Float totalSortedWeight);

    public abstract Float getTotalSortedComputedWeight();

    public abstract void setTotalSortedComputedWeight(Float totalSortedComputedWeight);

    public abstract Float getTotalUnsortedComputedWeight();

    public abstract void setTotalUnsortedComputedWeight(Float totalUnsortedComputedWeight);

    public abstract Float getTotalSampleSortedComputedWeight();

    public abstract void setTotalSampleSortedComputedWeight(Float totalSampleSortedComputedWeight);

    public abstract ComputableData<Float> getTotalInertComputedOrNotWeight();

    public abstract Float getTotalInertWeight();

    public abstract void setTotalInertWeight(Float totalInertWeight);

    public abstract Float getTotalInertComputedWeight();

    public abstract void setTotalInertComputedWeight(Float totalInertComputedWeight);

    public abstract ComputableData<Float> getTotalLivingNotItemizedComputedOrNotWeight();

    public abstract Float getTotalLivingNotItemizedWeight();

    public abstract void setTotalLivingNotItemizedWeight(Float totalLivingNotItemizedWeight);

    public abstract Float getTotalLivingNotItemizedComputedWeight();

    public abstract void setTotalLivingNotItemizedComputedWeight(Float totalLivingNotItemizedComputedWeight);

    public abstract Integer getDistinctSortedSpeciesCount();

    public abstract void setDistinctSortedSpeciesCount(Integer distinctSortedSpeciesCount);

    public abstract Integer getDistinctUnsortedSpeciesCount();

    public abstract void setDistinctUnsortedSpeciesCount(Integer distinctUnsortedSpeciesCount);

    public abstract Map<String, Object> importMultiPost(File file, FishingOperation operation, boolean importFrequencies, boolean importIndivudalObservations);

    public abstract MultiPostImportResult importMultiPost(File file, FishingOperation operation, SpeciesBatch speciesBatch, boolean importFrequencies, boolean importIndivudalObservations);

    public abstract void exportMultiPost(File file, FishingOperation operation, boolean importFrequencies, boolean importIndivudalObservations);

    public abstract void exportMultiPost(File file, FishingOperation operation, SpeciesBatch speciesBatch, boolean importFrequencies, boolean importIndivudalObservations);

    public abstract SpeciesBatch createBatch(SpeciesBatch speciesBatch, Integer parentBatchId);

    public abstract SpeciesBatch saveBatch(SpeciesBatch speciesBatch);

    public abstract List<SpeciesBatchFrequency> saveBatchFrequencies(Integer speciesBatchId, List<SpeciesBatchFrequency> frequency);

    public abstract List<Species> getReferentSpeciesWithSurveyCode(boolean restrictToProtocol);

    public abstract List<Species> getReferentOtherSpeciesWithSurveyCode(boolean restrictToProtocol);

    public abstract List<SpeciesProtocol> getSpeciesFromProtocol();

    public final List<Species> getReferentSpeciesWithSurveyCode() {
        return getReferentSpeciesWithSurveyCode(false);
    }

    public final SpeciesProtocol getSpeciesProtocol(Species species) {
        return TuttiProtocols.getSpeciesProtocol(species, getSpeciesFromProtocol());
    }

    public final SampleCategoryModelEntry getBestFirstSampleCategory(List<SampleCategoryModelEntry> data, Species species) {
        SpeciesProtocol speciesProtocol = getSpeciesProtocol(species);
        return context.getDataContext().getBestFirstSampleCategory(data, speciesProtocol);
    }

    public abstract void deleteSpeciesSubBatch(Integer speciesBatchId);

    public abstract void deleteSpeciesBatch(Integer speciesBatchId);
}
