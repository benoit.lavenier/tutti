package fr.ifremer.tutti.ui.swing.content.genericformat;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.ui.swing.content.genericformat.tree.DataSelectTreeModel;
import fr.ifremer.tutti.ui.swing.content.genericformat.tree.ProgramSelectTreeNode;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatExportUIHandler extends AbstractTuttiUIHandler<GenericFormatExportUIModel, GenericFormatExportUI> implements CloseableUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatExportUIHandler.class);

    @Override
    public void beforeInit(GenericFormatExportUI ui) {

        super.beforeInit(ui);

        getDataContext().resetValidationDataContext();

        GenericFormatExportUIModel model = new GenericFormatExportUIModel();

        ui.setContextValue(model);

        model.addPropertyChangeListener(new PropertyChangeListener() {

            final Set<String> propertyNamesToCanExport = Sets.newHashSet(GenericFormatExportUIModel.PROPERTY_PROGRAM,
                                                                         GenericFormatExportUIModel.PROPERTY_DATA_SELECTED);

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                GenericFormatExportUIModel source = (GenericFormatExportUIModel) evt.getSource();
                String propertyName = evt.getPropertyName();

                if (propertyNamesToCanExport.contains(propertyName)) {

                    boolean canExport = source.computeIsCanExport();
                    source.setCanExport(canExport);

                }

                if (GenericFormatExportUIModel.PROPERTY_PROGRAM.equals(propertyName)) {

                    // Reload data + remove selection
                    Program program = (Program) evt.getNewValue();
                    onProgramChanged(program);

                }

            }
        });

    }

    @Override
    public void afterInit(GenericFormatExportUI ui) {

        initUI(ui);

        GenericFormatExportUIModel model = getModel();
        initBeanFilterableComboBox(ui.getProgramComboBox(),
                                   Lists.newArrayList(getPersistenceService().getAllProgram()),
                                   model.getProgram());

        SwingValidator validator = ui.getValidator();

        registerValidators(validator);

        JTree tree = ui.getDataSelectionTree();
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        ToolTipManager.sharedInstance().registerComponent(tree);
        DataSelectTreeModel.installDataSelectionHandler(tree);

        DataSelectTreeModel treeModel = ui.getTreeModel();
        treeModel.addTreeModelListener(new TreeModelListener() {
            @Override
            public void treeNodesChanged(TreeModelEvent e) {

                DataSelectTreeModel source = (DataSelectTreeModel) e.getSource();
                boolean dataSelected = source.isDataSelected();
                getModel().setDataSelected(dataSelected);

            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {

                DataSelectTreeModel source = (DataSelectTreeModel) e.getSource();
                boolean dataSelected = source.isDataSelected();
                getModel().setDataSelected(dataSelected);

            }
        });

        if (getDataContext().isProgramFilled()) {

            Program program = getDataContext().getProgram();

            if (log.isInfoEnabled()) {
                log.info("Using selected program " + program);
            }

            model.setProgram(program);

        }

    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getProgramComboBox();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();
    }

    @Override
    public boolean quitUI() {
        return true;
    }

    @Override
    public SwingValidator<GenericFormatExportUIModel> getValidator() {
        return ui.getValidator();
    }

    protected void onProgramChanged(Program program) {

        if (log.isInfoEnabled()) {
            log.info("Program changed: " + program);
        }

        ProgramSelectTreeNode root;
        if (program == null) {

            root = null;

        } else {

            ProgramDataModel dataModel = getPersistenceService().loadProgram(program.getId(), true);
            root = new ProgramSelectTreeNode(dataModel);

        }

        DefaultTreeModel treeModel = (DefaultTreeModel) ui.getDataSelectionTree().getModel();
        treeModel.setRoot(root);
        getModel().setRootNode(root);

    }

}
