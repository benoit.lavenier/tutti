package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.LabelAware;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.service.catches.ValidateCruiseOperationsService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.MainUI;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.util.actions.ShowComboBoxPopupActions;
import fr.ifremer.tutti.ui.swing.util.attachment.ButtonAttachment;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableDataEditor;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.editor.cell.NumberCellEditor;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.decorator.FontHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.nuiton.jaxx.application.swing.util.ActionListCellRenderer;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Contract of any UI handler.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public abstract class AbstractTuttiUIHandler<M, UI extends TuttiUI<M, ?>> extends AbstractApplicationUIHandler<M, UI> implements UIMessageNotifier {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractTuttiUIHandler.class);

    public static final String CAN_EDIT = "_canEdit_";

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void showInformationMessage(String message) {
        getContext().showInformationMessage(message);
    }

    @Override
    public TuttiUIContext getContext() {
        return (TuttiUIContext) super.getContext();
    }

    public TuttiDataContext getDataContext() {
        return getContext().getDataContext();
    }

    public TuttiConfiguration getConfig() {
        return getContext().getConfig();
    }

    public PersistenceService getPersistenceService() {
        return getContext().getPersistenceService();
    }

    public ValidationService getValidationService() {
        return getContext().getValidationService();
    }

    public ValidateCruiseOperationsService getValidateCruiseOperationsService() {
        return getContext().getValidateCruiseOperationsService();
    }

    @Override
    public Component getTopestUI() {
        Component result;
        //        if (actionUI.isVisible()) {
        result = getContext().getActionUI();
//        } else {
//            result = getContext().getMainUI();
//        }
        return result;
    }

    public void clearValidators() {
        MainUI main = getContext().getMainUI();
        Preconditions.checkNotNull(
                main, "No mainUI registred in application context");
        MainUIHandler handler = main.getHandler();
        handler.clearValidators();
    }

    public String getWeightStringValue(JComponent component, Float weight) {


        WeightUnit weightUnit = (WeightUnit) component.getClientProperty("addWeightUnit");
        Objects.requireNonNull(weightUnit, "can't find addWeightUnit client property on component: " + component);
        return weightUnit.renderWeight(weight);
    }


    @Override
    public <O> Decorator<O> getDecorator(Class<O> type, String name) {
        DecoratorService decoratorService =
                getContext().getDecoratorService();

        Preconditions.checkNotNull(type);

        Decorator decorator = decoratorService.getDecoratorByType(type, name);
        if (decorator == null) {

            if (LabelAware.class.isAssignableFrom(type)) {
                decorator = getDecorator(LabelAware.class, null);
            }
        }
        Preconditions.checkNotNull(decorator);
        return decorator;
    }

    @Override
    protected void addHighlighters(final JXTable table) {

        HighlightPredicate notSelectedPredicate = new HighlightPredicate.NotHighlightPredicate(HighlightPredicate.IS_SELECTED);
        HighlightPredicate rowIsInvalidPredicate = (renderer, adapter) -> {

            boolean result = false;
            if (adapter.isEditable()) {
                AbstractApplicationTableModel model = (AbstractApplicationTableModel) table.getModel();
                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                AbstractTuttiBeanUIModel row = (AbstractTuttiBeanUIModel) model.getEntry(modelRow);
                result = !row.isValid();
            }
            return result;
        };
        HighlightPredicate rowIsValidPredicate =
                new HighlightPredicate.NotHighlightPredicate(rowIsInvalidPredicate);
        Highlighter selectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                HighlightPredicate.IS_SELECTED,
                getConfig().getColorSelectedRow());
        table.addHighlighter(selectedHighlighter);

        // paint in a special color for read only cells (not selected)
        Highlighter readOnlyHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.READ_ONLY,
                        notSelectedPredicate),
                getConfig().getColorRowReadOnly());
        table.addHighlighter(readOnlyHighlighter);

        // paint in a special color for read only cells (selected)
        Highlighter readOnlySelectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.READ_ONLY,
                        HighlightPredicate.IS_SELECTED),
                getConfig().getColorRowReadOnly().darker());
        table.addHighlighter(readOnlySelectedHighlighter);

        // paint in a special color inValid rows (not selected)
        Highlighter validHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.EDITABLE,
                        notSelectedPredicate,
                        rowIsInvalidPredicate),
                getConfig().getColorRowInvalid());
        table.addHighlighter(validHighlighter);

        // paint in a special color inValid rows (selected)
        Highlighter validSelectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.EDITABLE,
                        HighlightPredicate.IS_SELECTED,
                        rowIsInvalidPredicate),
                getConfig().getColorRowInvalid().darker());
        table.addHighlighter(validSelectedHighlighter);

        // use configured color odd row (not for selected)
        Highlighter evenHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.ODD,
                        notSelectedPredicate,
                        rowIsValidPredicate,
                        HighlightPredicate.READ_ONLY),
                getConfig().getColorAlternateRow().darker());
        table.addHighlighter(evenHighlighter);

        Highlighter evenNotReadOnlyHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.ODD,
                        notSelectedPredicate,
                        rowIsValidPredicate,
                        HighlightPredicate.EDITABLE),
                getConfig().getColorAlternateRow());
        table.addHighlighter(evenNotReadOnlyHighlighter);

        // use configured color odd row (for selected)
        Highlighter evenSelectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.ODD,
                        HighlightPredicate.IS_SELECTED,
                        rowIsValidPredicate,
                        HighlightPredicate.EDITABLE),
                getConfig().getColorSelectedRow());
        table.addHighlighter(evenSelectedHighlighter);


        // paint in a special color inValid rows
        Font font = table.getFont().deriveFont(Font.BOLD);
        Highlighter selectHighlighter = new FontHighlighter(HighlightPredicate.IS_SELECTED, font);
        table.addHighlighter(selectHighlighter);
    }

    protected void listenModelModifiy(AbstractTuttiBeanUIModel model) {
        model.addPropertyChangeListener(AbstractTuttiBeanUIModel.PROPERTY_MODIFY, evt -> {
            Boolean modify = (Boolean) evt.getNewValue();
            if (modify != null && modify) {
                ((AbstractTuttiBeanUIModel) getModel()).setModify(true);
            }
        });
    }

    //------------------------------------------------------------------------//
    //-- Init methods                                                       --//
    //------------------------------------------------------------------------//

    @Override
    protected void initUIComponent(Object component) {
        if (component instanceof NumberEditor) {
            initNumberEditor((NumberEditor) component);
        } else if (component instanceof JXTitledPanel) {
            initJXTitledPanel((JXTitledPanel) component);
        } else if (component instanceof ButtonAttachment) {

            initButtonAttachment((ButtonAttachment) component);
        } else if (component instanceof JComboBox) {

            initComboBox((JComboBox) component);
        } else {
            super.initUIComponent(component);
        }
    }

    private void initComboBox(JComboBox<?> comboBox) {

        super.initUIComponent(comboBox);
        List<JButton> comboboxActions = (List<JButton>) comboBox.getClientProperty("comboboxActions");
        if (comboboxActions != null) {

            comboBox.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
            comboBox.addMouseListener(TuttiUIUtil.GRAB_FOCUS_ON_ENTER_LISTENER);

            comboBox.setRenderer(new ActionListCellRenderer());
            comboBox.setModel(new DefaultComboBoxModel(comboboxActions.toArray()));
            comboBox.addPropertyChangeListener("enabled", evt -> {
                JComboBox source = (JComboBox) evt.getSource();
                source.setFocusable((Boolean) evt.getNewValue());
            });
            comboBox.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {

                    JComboBox source = (JComboBox) e.getSource();

                    if (source.isEnabled()) {

                        ShowComboBoxPopupActions task = new ShowComboBoxPopupActions(source);

                        try {
                            getContext().getTimer().schedule(task, 300);
                        } catch (IllegalStateException e1) {
                            getContext().reloadTimer().schedule(task, 300);
                        }

                    }

                }

                @Override
                public void mouseClicked(MouseEvent e) {

                    JComboBox source = (JComboBox) e.getSource();
                    if (source.isEnabled()) {

                        AbstractButton action = (AbstractButton) source.getItemAt(0);
                        getContext().getActionEngine().runAction(action);

                    }

                }
            });

            comboBox.addActionListener(e -> {

                JComboBox source = (JComboBox) e.getSource();
                Boolean canEdit = (Boolean) source.getClientProperty(CAN_EDIT);
                if (canEdit == null || canEdit) {

                    JButton selectedAction = (JButton) source.getSelectedItem();
                    // hide popup before performing the action, otherwise, if the action
                    // opens a popup, the user must click a first time to hide the combobox
                    // popup to then interact with the popup opened by the action
                    // (see http://forge.codelutin.com/issues/2032)
                    source.setSelectedIndex(0);
                    source.hidePopup();
                    getContext().getActionEngine().runAction(selectedAction);

                }
            });
        }
    }

    public void resetComboBoxAction(JComboBox source) {
        source.putClientProperty(CAN_EDIT, false);

        try {
            source.setSelectedIndex(0);
        } finally {
            source.putClientProperty(CAN_EDIT, null);
        }
    }

    protected void initJXTitledPanel(JXTitledPanel jTextField) {
//        Boolean boldFont = (Boolean) jTextField.getClientProperty("boldFont");
//        if (boldFont!= null && boldFont) {
//            Font font = jTextField.getFont().deriveFont(Font.BOLD, 13.f);
//            jTextField.setTitleFont(font);
//        }
    }

    @Override
    protected void initTextField(JTextField jTextField) {
        super.initTextField(jTextField);
        Boolean computed = (Boolean) jTextField.getClientProperty("computed");
        if (computed != null && computed) {
            Font font = jTextField.getFont().deriveFont(Font.ITALIC);
            jTextField.setFont(font);
            jTextField.setEditable(false);
            jTextField.setEnabled(false);
            jTextField.setDisabledTextColor(getConfig().getColorComputedWeights());
        }
    }

    protected void initButtonAttachment(ButtonAttachment component) {

        component.init();
    }

    @Override
    protected void initLabel(JLabel jLabel) {

        WeightUnit weightUnit = (WeightUnit) jLabel.getClientProperty("addWeightUnit");
        if (weightUnit != null) {
            String text = weightUnit.decorateLabel(jLabel.getText());
            jLabel.setText(text);

            String tip = weightUnit.decorateTip(jLabel.getToolTipText());
            jLabel.setToolTipText(tip);

            Component labelFor = jLabel.getLabelFor();
            if (labelFor instanceof ComputableDataEditor) {

                // set also the number of digits (4 for kg, 1 for g)
                ComputableDataEditor editor = (ComputableDataEditor) labelFor;
                editor.setWeightUnit(weightUnit);
            } else if (labelFor instanceof NumberEditor) {

                // set also the number of digits (4 for kg, 1 for g)
                NumberEditor editor = (NumberEditor) labelFor;
                editor.setNumberPattern(weightUnit.getNumberEditorPattern());
            }
        }
    }

    @Override
    protected void initButton(AbstractButton abstractButton) {

        super.initButton(abstractButton);

        TuttiUIUtil.initButton(getContext(), this.getUI(), abstractButton);

    }

    protected void initNumberEditor(NumberEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init number editor " + editor.getName());
        }
        editor.init();

        // Force binding if value is already in model
        Number model = editor.getModel().getNumberValue();
        if (model != null) {
            editor.setNumberValue(null);
            editor.setNumberValue(model);
        }

        if (isAutoSelectOnFocus(editor)) {

            addAutoSelectOnFocus(editor.getTextField());
        }
    }


    public String buildReminderLabelTitle(Species species,
                                          Iterable<SampleCategory<?>> categories,
                                          String prefix,
                                          String suffix) {
        return buildReminderLabelTitle(species, categories, prefix, suffix, true);

    }

    public String buildReminderLabelTitle(Species species,
                                          Iterable<SampleCategory<?>> categories,
                                          String prefix,
                                          String suffix,
                                          boolean html) {
        return buildReminderLabelTitle(
                decorate(species, DecoratorService.WITH_SURVEY_CODE),
                categories,
                prefix,
                suffix,
                html);

    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void registerValidators(SwingValidator... validators) {
        MainUI main = getContext().getMainUI();
        Preconditions.checkNotNull(
                main, "No mainUI registred in application context");
        MainUIHandler handler = main.getHandler();
        handler.clearValidators();
        for (SwingValidator validator : validators) {
            handler.registerValidator(validator);
        }
    }

    protected void listenValidatorValid(SimpleBeanValidator validator,
                                        final AbstractTuttiBeanUIModel model) {
        validator.addPropertyChangeListener(SimpleBeanValidator.VALID_PROPERTY, evt -> {
            if (log.isDebugEnabled()) {
                log.debug("Model [" + model +
                          "] pass to valid state [" +
                          evt.getNewValue() + "]");
            }
            model.setValid((Boolean) evt.getNewValue());
        });
    }

    protected void listenValidationTableHasNoFatalError(final SimpleBeanValidator validator,
                                                        final AbstractTuttiBeanUIModel model) {
        getContext().getMainUI().getValidatorMessageWidget().addTableModelListener(e -> {
            boolean valid = !validator.hasFatalErrors();
            if (log.isDebugEnabled()) {
                log.debug("Model [" + model +
                          "] pass to valid state [" + valid + "]");
            }
            model.setValid(valid);
        });
    }

    protected void listModelIsModify(AbstractTuttiBeanUIModel model) {
        model.addPropertyChangeListener(new PropertyChangeListener() {

            final Set<String> excludeProperties = getPropertiesToIgnore();

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!excludeProperties.contains(evt.getPropertyName())) {
                    ((AbstractTuttiBeanUIModel) evt.getSource()).setModify(true);
                }
            }
        });
    }

    protected Set<String> getPropertiesToIgnore() {
        return Sets.newHashSet(
                AbstractTuttiBeanUIModel.PROPERTY_MODIFY,
                AbstractTuttiBeanUIModel.PROPERTY_VALID);
    }

    protected void closeUI(TuttiUI ui) {
        ui.getHandler().onCloseUI();
    }

    protected String buildReminderLabelTitle(String species,
                                             Iterable<SampleCategory<?>> categories,
                                             String prefix,
                                             String suffix) {
        return buildReminderLabelTitle(species, categories, prefix, suffix, true);
    }

    protected String buildReminderLabelTitle(String species,
                                             Iterable<SampleCategory<?>> categories,
                                             String prefix,
                                             String suffix,
                                             boolean html) {
        StringBuilder title = new StringBuilder();
        if (html) {
            title.append("<html><body style='color:black;'>");
        }
        title.append(prefix).append(" - [");

        if (html) {
            title.append("<strong>");
        }
        title.append(species);

        if (html) {
            title.append("</strong>");
        }
        title.append("]");

        if (categories != null) {
            for (SampleCategory<?> sampleCategory : categories) {
                if (sampleCategory.getCategoryValue() != null) {
                    title.append(" - ");
                    title.append(decorate(sampleCategory.getCategoryValue()));
                }
            }
        }

        title.append(" - ").append(suffix);
        if (html) {
            title.append("</body></html>");
        }

        return title.toString();
    }

    protected <R> TableColumnExt addFloatColumnToModel(TableColumnModel model,
                                                       ColumnIdentifier<R> identifier,
                                                       WeightUnit weightUnit,
                                                       JTable table) {

        Preconditions.checkNotNull(weightUnit);
        NumberCellEditor<Float> editor =
                JAXXWidgetUtil.newNumberTableCellEditor(Float.class, false);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern(weightUnit.getNumberEditorPattern());

        TableCellRenderer renderer = newWeightCellRenderer(table.getDefaultRenderer(Number.class), weightUnit);

        return addColumnToModel(model, editor, renderer, identifier, weightUnit);
    }

    protected <R> TableColumnExt addColumnToModel(TableColumnModel model,
                                                  TableCellEditor editor,
                                                  TableCellRenderer renderer,
                                                  ColumnIdentifier<R> identifier,
                                                  WeightUnit weightUnit) {

        TableColumnExt col = new TableColumnExt(model.getColumnCount());
        col.setCellEditor(editor);
        col.setCellRenderer(renderer);
        String label = t(identifier.getHeaderI18nKey());
        if (weightUnit != null) {
            label = weightUnit.decorateLabel(label);
        }
        col.setHeaderValue(label);
        String tip = t(identifier.getHeaderTipI18nKey());
        if (weightUnit != null) {
            tip = weightUnit.decorateTip(tip);
        }
        col.setToolTipText(tip);

        col.setIdentifier(identifier);
        model.addColumn(col);
        // by default no column is sortable, must specify it
        col.setSortable(false);
        return col;
    }

    protected TableCellRenderer newWeightCellRenderer( TableCellRenderer delegate, WeightUnit weightUnit) {
        return (table, value, isSelected, hasFocus, row, column) -> {
            Component result = delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if(result instanceof JLabel) {
                JLabel jLabel = (JLabel)result;
                jLabel.setHorizontalTextPosition(4);
                jLabel.setText(weightUnit.renderWeight((Float) value));
            }

            return result;
        };
    }
}
