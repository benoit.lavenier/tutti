/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
document.write('<div class="navbar navbar-default navbar-inverse navbar-fixed-top">');
document.write('  <div class="navbar-inner">');
document.write('    <div class="container" id="menu"><a class="navbar-brand" href="index.html">Allegro Campagne</a>');
document.write('      <ul class="nav navbar-nav">');
document.write('        <li class="dropdown">');
document.write('          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">');
document.write('            Table des matières <strong class="caret"></strong>');
document.write('          </a>');
document.write('          <ul class="dropdown-menu" role="menu">');
document.write('            <li class="dropdown-submenu">');
document.write('              <a href="menu.html">Description du Menu</a>');
document.write('              <ul class="dropdown-menu" role="menu">');
document.write('                <li class="dropdown-submenu">');
document.write('                  <a href="menu.html#menu_fichier">Menu Fichier</a>');
document.write('                  <ul class="dropdown-menu" role="menu">');
document.write('                    <li><a href="menu.html#menu_fichier_configuration">Configuration</a></li>');
document.write('                    <li><a href="menu.html#menu_fichier_mise_a_jour_logicelle">Vérifier les mises à jour logicielles</a></li>');
document.write('                    <li><a href="menu.html#menu_fichier_mise_a_jour_rapport">Vérifier les mises à jour des rapports</a></li>');
document.write('                    <li><a href="menu.html#menu_fichier_connection_ichtyometre">Se connecter / déconnecter à un ichtyomètre</a></li>');
document.write('                    <li><a href="menu.html#menu_fichier_gestionnaire_base">Gestionnaire de bases</a></li>');
document.write('                    <li><a href="menu.html#menu_fichier_quitter">Quitter</a></li>');
document.write('                  </ul>');
document.write('                </li>');
document.write('                <li class="dropdown-submenu">');
document.write('                  <a href="menu.html#menu_action">Menu Aller à</a>');
document.write('                  <ul class="dropdown-menu" role="menu">');
document.write('                    <li><a href="menu.html#menu_action_selection">Sélectionne une campagne</a></li>');
document.write('                    <li><a href="menu.html#menu_action_serie">Série de campagne</a></li>');
document.write('                    <li><a href="menu.html#menu_action_campagne">Campagne</a></li>');
document.write('                    <li><a href="menu.html#menu_action_protocole">Protocole</a></li>');
document.write('                    <li><a href="menu.html#menu_action_saisie_capture">Saisir les captures</a></li>');
document.write('                    <li><a href="menu.html#menu_action_valider_capture">Valider les captures</a></li>');
document.write('                    <li><a href="menu.html#menu_action_rapport">Rapport</a></li>');
document.write('                    <li><a href="menu.html#menu_action_genericFormat_export">Export générique</a></li>');
document.write('                    <li><a href="menu.html#menu_action_genericFormat_import">Import générique</a></li>');
document.write('                  </ul>');
document.write('                </li>');
document.write('                <li class="dropdown-submenu">');
document.write('                  <a href="menu.html#menu_administration">Menu Administrations</a>');
document.write('                  <ul class="dropdown-menu" role="menu">');
document.write('                    <li><a href="menu.html#menu_administration_referentiel">Référentiels temporaires</a></li>');
document.write('                    <li><a href="menu.html#menu_administration_sample_category">Configurer les catégorisations</a></li>');
document.write('                  </ul>');
document.write('                </li>');
document.write('                <li class="dropdown-submenu">');
document.write('                  <a href="menu.html#menu_aide">Menu Aide</a>');
document.write('                  <ul class="dropdown-menu" role="menu">');
document.write('                    <li><a href="menu.html#menu_aide_aide">Aide</a></li>');
document.write('                    <li><a href="menu.html#menu_aide_site">Site</a></li>');
document.write('                    <li><a href="menu.html#menu_aide_about">À propos</a></li>');
document.write('                    <li><a href="menu.html#menu_aide_langue">Langue</a></li>');
document.write('                  </ul>');
document.write('                </li>');
document.write('              </ul>');
document.write('            </li>');
document.write('            <li class="dropdown-submenu">');
document.write('              <a href="#">Description des écrans de saisie de données</a>');
document.write('              <ul class="dropdown-menu" role="menu">');
document.write('                <li><a href="selectCruise.html">Sélection Série / Campagne / Protocole</a></li>');
document.write('                <li><a href="editProgram.html">Créer / Éditer une série de campagne</a></li>');
document.write('                <li><a href="editCruise.html">Créer / Éditer une campagne</a></li>');
document.write('                <li><a href="editProtocol.html">Créer / Éditer un protocole</a></li>');
document.write('                <li class="dropdown-submenu">');
document.write('                  <a href="editFishingOperation.html">Saisie du trait et de la capture</a>');
document.write('                  <ul class="dropdown-menu" role="menu">');
document.write('                    <li><a href="editFishingOperation.html#traitTrait">Trait / Trait</a></li>');
document.write('                    <li><a href="editFishingOperation.html#traitMiseEnOuvreEngin">Trait / Mise en œuvre de l\'engin</a></li>');
document.write('                    <li><a href="editFishingOperation.html#traitAutreParametres">Trait / Autres paramètres</a></li>');
document.write('                    <li><a href="editFishingOperation.html#captureResume">Captures / Résumé</a></li>');
document.write('                    <li><a href="editFishingOperation.html#captureEspeces">Captures / Espèces</a></li>');
document.write('                    <li><a href="editFishingOperation.html#captureBenthos">Captures / Benthos</a></li>');
document.write('                    <li><a href="editFishingOperation.html#captureMacroDechet">Captures / Macro déchets</a></li>');
document.write('                    <li><a href="editFishingOperation.html#captureDonnesIndividuelles">Captures / Observations individuelles</a></li>');
document.write('                    <li><a href="editFishingOperation.html#captureCapturesAccidentelles">Captures / Captures accidentelles</a></li>');
document.write('                  </ul>');
document.write('                </li>');
document.write('              </ul>');
document.write('            </li>');
document.write('            <li class="dropdown-submenu">');
document.write('              <a href="#">Autres fonctionnalités</a>');
document.write('              <ul class="dropdown-menu" role="menu">');
document.write('                <li><a href="dbManager.html">Gestionnaire de base</a></li>');
document.write('                <li><a href="config.html">Configuration</a></li>');
document.write('                <li><a href="manageTemporaryReferential.html">Gestionnaire de référentiels temporaires</a></li>');
document.write('                <li><a href="report.html">Générer des rapports</a></li>');
document.write('                <li><a href="genericFormat.html">Import - Export au format générique</a></li>');
document.write('                <li><a href="dbMapping.html">Mapping des écrans / base de données</a></li>');
document.write('                <li><a href="validation.html">Règles de validation</a></li>');
document.write('              </ul>');
document.write('            </li>');
document.write('            <li class="dropdown-submenu">');
document.write('              <a href="fonctionnalites_transversales.html">Fonctionnalités transversales</a>');
document.write('              <ul class="dropdown-menu" role="menu">');
document.write('                <li><a href="fonctionnalites_transversales.html#recherche_liste">Recherche dans une liste</a></li>');
document.write('                <li><a href="fonctionnalites_transversales.html#tri_liste">Tri dans une liste</a></li>');
document.write('                <li><a href="fonctionnalites_transversales.html#vider_valeur">Vider la valeur d\'un champ</a></li>');
document.write('                <li><a href="fonctionnalites_transversales.html#raccourcis_clavier">Raccourcis clavier</a></li>');
document.write('                <li><a href="fonctionnalites_transversales.html#origine_poids">Origine des poids affichés</a></li>');
document.write('                <li><a href="fonctionnalites_transversales.html#controle_saisie">Contrôle de la saisie</a></li>');
document.write('                <li><a href="fonctionnalites_transversales.html#rappel_context">Rappel du context de saisie</a></li>');
document.write('              </ul>');
document.write('            </li>');
document.write('            <li class="dropdown-submenu">');
document.write('              <a href="faq.html">Foire aux questions</a>');
document.write('              <ul class="dropdown-menu" role="menu">');
document.write('                <li><a href="faq.html#melag">Gestion de la saisie d\'un mélange d\'espèces</a></li>');
document.write('                <li><a href="faq.html#gestion_du_protocole">Gestion du protocole</a></li>');
document.write('                <li><a href="faq.html#information_dans_protocole">Quelles sont les informations à renseigner dans le protocole</a></li>');
document.write('                <li><a href="faq.html#format_fichier_import">Format des fichiers d\'imports dans le logiciel</a></li>');
document.write('                <li><a href="faq.html#manipulation_fichiers">Manipulation des fichiers d\'import/export</a></li>');
document.write('                <li><a href="faq.html#sauvegarde_protocole">Sauvegarde du protocole dans Harmonie</a></li>');
document.write('                <li><a href="faq.html#vrac_definition">Vrac / Hors Vrac : définitions</a></li>');
document.write('                <li><a href="faq.html#saisie_multiposte">Saisie multi-ordinateurs</a></li>');
document.write('                <li><a href="faq.html#sauvegarde_reguliere">Sauvegarde régulière de la base</a></li>');
document.write('              </ul>');
document.write('            </li>');
document.write('          </ul>');
document.write('        </li>');
document.write('      </ul>');
document.write('    </div>');
document.write('  </div>');
document.write('</div>');
