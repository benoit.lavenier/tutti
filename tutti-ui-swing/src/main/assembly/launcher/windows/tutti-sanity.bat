@echo off

set OLDDIR=%CD%
cd /d %~dp0%

set TUTTI_BASEDIR="%CD%"
set JAVA_HOME=%TUTTI_BASEDIR%\jre
set JAVA_COMMAND=%JAVA_HOME%\bin\java

echo Allegro Campaign basedir:  %TUTTI_BASEDIR%

@start tutti.exe --db-sanity true
%JAVA_COMMAND% -jar launcher.jar --db-sanity true
