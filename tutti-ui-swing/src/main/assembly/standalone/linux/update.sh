#!/bin/bash
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !                                                                         !
echo ! THE LAUNCH AND UPDATE PROCESSES HAVE BEEN CHANGED                       !
echo ! PLEASE EXECUTE './tutti.sh'                                             !
echo !                                                                         !
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !                                                                         !
echo ! LES PROCESSUS DE DEMARRAGE ET MISE A JOUR ONT CHANGES                   !
echo ! VEUILLEZ EXECUTER './tutti.sh'                                          !
echo !                                                                         !
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
