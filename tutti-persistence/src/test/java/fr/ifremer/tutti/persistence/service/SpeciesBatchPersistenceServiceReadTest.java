package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * To test {@link SpeciesBatchPersistenceService} for read operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
@Ignore
public class SpeciesBatchPersistenceServiceReadTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected SpeciesBatchPersistenceService service;

    protected FishingOperationPersistenceService fishingOperationService;

    protected FishingOperation fishingOperation;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getSpeciesBatchPersistenceService();
        fishingOperationService = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();

        List<FishingOperation> fishingOperations = fishingOperationService.getAllFishingOperation(dbResource.getFixtures().cruiseId());
        assertNotNull(fishingOperations);
        assertTrue(fishingOperations.size() > 0);
        fishingOperation = fishingOperations.get(0);
        assertNotNull(fishingOperation);
        assertNotNull(fishingOperation.getId());
    }

    @Ignore
    @Test
    public void getRootSpeciesBatch(/*String fishingOperationId*/) {
        //TODO Do me!
        service.getRootSpeciesBatch(fishingOperation.getIdAsInt(), false);
    }

    @Ignore
    @Test
    public void getAllSpeciesBatchFrequency(/*String speciesBatchId*/) {
        //TODO Do me!
    }

    @Ignore
    @Test
    public void getRootBenthosBatch(/*String fishingOperationId*/) {
        //TODO Do me!
    }


    public void assertSpeciesBatch(SpeciesBatch expectedBatch,
                                   SpeciesBatch actualBatch,
                                   boolean assertIdEquals) {
        assertNotNull(actualBatch);
        assertNotNull(actualBatch.getId());
        if (assertIdEquals && expectedBatch.getId() != null) {
            assertEquals(expectedBatch.getId(), actualBatch.getId());
        }
        assertEquals(expectedBatch.getWeight(), actualBatch.getWeight());
        assertEquals(expectedBatch.getSampleCategoryId(), actualBatch.getSampleCategoryId());
        if (expectedBatch.getSampleCategoryValue() != null && expectedBatch.getSampleCategoryValue() instanceof CaracteristicQualitativeValue) {
            assertNotNull("Bad sampleCategoryValue : expected <" + ((CaracteristicQualitativeValue) expectedBatch.getSampleCategoryValue()).getId() + "> but was <null>",
                          actualBatch.getSampleCategoryValue());
            assertEquals(
                    ((CaracteristicQualitativeValue) expectedBatch.getSampleCategoryValue()).getId(),
                    ((CaracteristicQualitativeValue) actualBatch.getSampleCategoryValue()).getId());
        } else {
            assertEquals(expectedBatch.getSampleCategoryValue(), actualBatch.getSampleCategoryValue());
        }
        assertEquals(expectedBatch.getSampleCategoryWeight(), actualBatch.getSampleCategoryWeight());
        assertEquals(expectedBatch.getNumber(), actualBatch.getNumber());
        assertEquals(expectedBatch.getComment(), actualBatch.getComment());

        // Check species only if Vrac/HorsVrac or if batch has been load throw getAllxxx method
        // (Because getSpeciesBatch(id) could not always retrieve the species)
        if (expectedBatch.getSpecies() != null && (
                PmfmId.SORTED_UNSORTED.getValue().equals(expectedBatch.getSampleCategoryId())
                || actualBatch.getSpecies() != null)) {
            assertNotNull(actualBatch.getSpecies());
            assertEquals(expectedBatch.getSpecies().getId(), actualBatch.getSpecies().getId());
        }
    }

    public static void assertBatchFrequencies(List<SpeciesBatchFrequency> expectedFrequencies,
                                              List<SpeciesBatchFrequency> actualFrequencies,
                                              boolean assertIdEquals) {
        assertNotNull(actualFrequencies);
        assertEquals(expectedFrequencies.size(), actualFrequencies.size());

        // Store actual batches into a map, using the length as key
        Map<Float, SpeciesBatchFrequency> expectedLengthMap = Maps.newHashMap();
        for (SpeciesBatchFrequency speciesBatchFrequency : expectedFrequencies) {
            expectedLengthMap.put(speciesBatchFrequency.getLengthStep(), speciesBatchFrequency);
        }

        // Store expected batches into a map, using the length as key
        Map<Float, SpeciesBatchFrequency> actualLengthMap = Maps.newHashMap();
        for (SpeciesBatchFrequency speciesBatchFrequency : actualFrequencies) {
            assertFalse("Duplicate lengthStep found in batchFrequencies, for length=" + speciesBatchFrequency.getLengthStep(), actualLengthMap.containsKey(speciesBatchFrequency.getLengthStep()));
            actualLengthMap.put(speciesBatchFrequency.getLengthStep(), speciesBatchFrequency);
            assertNotNull(speciesBatchFrequency.getId());
        }

        for (Float lengthStep : expectedLengthMap.keySet()) {
            SpeciesBatchFrequency expectedBatchFrequency = expectedLengthMap.get(lengthStep);
            SpeciesBatchFrequency actualBatchFrequency = actualLengthMap.get(lengthStep);
            if (assertIdEquals) {
                assertEquals(expectedBatchFrequency.getId(), actualBatchFrequency.getId());
            }
            assertNotNull(expectedBatchFrequency.getLengthStepCaracteristic());
            assertEquals(expectedBatchFrequency.getLengthStepCaracteristic().getId(), actualBatchFrequency.getLengthStepCaracteristic().getId());
            assertEquals(expectedBatchFrequency.getNumber(), actualBatchFrequency.getNumber());
            assertEquals(expectedBatchFrequency.getWeight(), actualBatchFrequency.getWeight());
            //assertNotNull(expectedBatchFrequency.getBatch());
            //assertEquals(expectedBatchFrequency.getBatch().getId(), actualBatchFrequency.getBatch().getId());
        }
    }

    public SpeciesBatch getSpeciesBatch(Integer fishingOperationId,
                                        Integer speciesBatchId) {
        BatchContainer<SpeciesBatch> rootSpeciesBatch = service.getRootSpeciesBatch(fishingOperationId, false);
        return getSpeciesBatch(speciesBatchId, rootSpeciesBatch.getChildren());
    }

    public static SpeciesBatch getSpeciesBatch(Integer speciesBatchId,
                                               List<SpeciesBatch> speciesBatchs) {
        if (speciesBatchs == null) {
            return null;
        }
        for (SpeciesBatch speciesBatch : speciesBatchs) {
            if (speciesBatchId.equals(speciesBatch.getIdAsInt())) {
                return speciesBatch;
            }
            if (speciesBatch.getChildBatchs() != null) {
                speciesBatch = getSpeciesBatch(speciesBatchId, speciesBatch.getChildBatchs());
                if (speciesBatch != null) {
                    return speciesBatch;
                }
            }
        }
        return null;
    }

}
