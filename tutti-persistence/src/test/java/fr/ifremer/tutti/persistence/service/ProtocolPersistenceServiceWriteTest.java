package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

/**
 * To test {@link ProtocolPersistenceService} for write operations.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ProtocolPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected ProtocolPersistenceService service;

    public static final String PROTOCOL_FILE_CONTENT =
            "id: 1\n" +
            "name: protocolName\n" +
            "benthos: \n" +
            "- !SpeciesProtocol\n" +
            "   id: 1\n" +
            "   lengthStepPmfmId: 1394\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 174\n" +
            "   - 196\n" +
            "   speciesReferenceTaxonId: 11242\n" +
            "   speciesSurveyCode: BAR\n" +
            "   weightEnabled: true\n" +
            "- !SpeciesProtocol\n" +
            "   id: 2\n" +
            "   lengthStepPmfmId: 323\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 1430\n" +
            "   - 174\n" +
            "   - 196\n" +
            "   - 198\n" +
            "   speciesReferenceTaxonId: 3835\n" +
            "   speciesSurveyCode: CHIN\n" +
            "   weightEnabled: true\n" +
            "comment: Commentaire\n" +
            "gearUseFeaturePmfmId: \n" +
            "- 21\n" +
            "- 22\n" +
            "lengthClassesPmfmId: \n" +
            "- 14\n" +
            "- 18\n" +
            "species: \n" +
            "- !SpeciesProtocol\n" +
            "   id: 1\n" +
            "   lengthStepPmfmId: 1394\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 174\n" +
            "   - 196\n" +
            "   speciesReferenceTaxonId: 11242\n" +
            "   speciesSurveyCode: BAR\n" +
            "   weightEnabled: true\n" +
            "- !SpeciesProtocol\n" +
            "   id: 2\n" +
            "   lengthStepPmfmId: 323\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 1430\n" +
            "   - 174\n" +
            "   - 196\n" +
            "   - 198\n" +
            "   speciesReferenceTaxonId: 3835\n" +
            "   speciesSurveyCode: CHIN\n" +
            "   weightEnabled: true\n" +
            "version: " + TuttiProtocols.CURRENT_PROTOCOL_VERSION + "\n" +
            "vesselUseFeaturePmfmId: \n" +
            "- 114\n" +
            "- 228\n" +
            "- 821";

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getProtocolPersistenceService();
    }

    @Test
    public void createProtocol(/*TuttiProtocol bean*/) {

        TuttiProtocol protocol = ProtocolPersistenceServiceReadTest.createProtocolFixture();
        TuttiProtocol createdProtocol = service.createProtocol(protocol);
        Assert.assertNotNull(createdProtocol);
        String id = createdProtocol.getId();
        Assert.assertTrue(service.getAllProtocolId().contains(id));

    }

    @Test
    public void saveProtocol(/*TuttiProtocol bean*/) {

        TuttiProtocol protocol = ProtocolPersistenceServiceReadTest.createProtocolFixture();
        TuttiProtocol createdProtocol = service.createProtocol(protocol);

        Assert.assertNotNull(createdProtocol);
        String id = createdProtocol.getId();
        Assert.assertTrue(service.getAllProtocolId().contains(id));

        TuttiProtocol savedProtocol = service.saveProtocol(createdProtocol);
        Assert.assertNotNull(savedProtocol);
    }

    @Test
    public void deleteProtocol() {

        TuttiProtocol protocol = ProtocolPersistenceServiceReadTest.createProtocolFixture();
        TuttiProtocol createdProtocol = service.createProtocol(protocol);
        Assert.assertNotNull(createdProtocol);

        String id = createdProtocol.getId();
        Assert.assertTrue(service.getAllProtocolId().contains(id));

        service.deleteProtocol(id);
        Assert.assertFalse(service.getAllProtocolId().contains(id));
    }
}
