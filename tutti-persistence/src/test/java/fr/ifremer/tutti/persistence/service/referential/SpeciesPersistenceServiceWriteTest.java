package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class SpeciesPersistenceServiceWriteTest extends ReferentialPersistenceServiceWriteTestSupport {

    protected SpeciesPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getSpeciesPersistenceService();
    }

    @Test
    public void testAddTemporarySpecies() {
        List<Species> species = Lists.newArrayList();

        long timestamp1 = System.nanoTime();
        Species sp1 = Speciess.newSpecies();
        sp1.setReferenceTaxonId((int) timestamp1);
        sp1.setId("Don't care" + timestamp1);
        sp1.setRefTaxCode("Don't care" + timestamp1);
        sp1.setSurveyCode("Don't care" + timestamp1);
        sp1.setName("Genus name" + timestamp1);
        species.add(sp1);

        long timestamp2 = System.nanoTime();
        Species sp2 = Speciess.newSpecies();
        sp2.setReferenceTaxonId((int) timestamp2);
        sp2.setId("Don't care" + timestamp2);
        sp2.setRefTaxCode("Don't care" + timestamp2);
        sp2.setSurveyCode("Don't care" + timestamp2);
        sp2.setName("Genus name" + timestamp2);
        species.add(sp2);

        Assert.assertNull(service.getSpeciesByReferenceTaxonId(sp1.getReferenceTaxonId()));
        //Assert.assertNull(service.getSpecies(sp2.getId()));

        Collection<Species> speciesList = service.addTemporarySpecies(species);
        Assert.assertNotNull(speciesList);
        Assert.assertEquals(2, speciesList.size());

        Species createdSp1 = Iterables.get(speciesList, 0);
        Assert.assertNotNull(createdSp1);
        Assert.assertEquals(sp1.getName(), createdSp1.getName());
        // TODO TC :question pour TC  pourquoi faire le test suivant NotNull, car tu n'a rien mis en entree de ce code ?
        // faut-il que le genere ?
        //Assert.assertNotNull(createdSp1.getExternalCode());
        //Assert.assertNotSame(sp1.getExternalCode(), createdSp1.getExternalCode());
        Assert.assertNull(createdSp1.getSurveyCode());
        Assert.assertNotNull(createdSp1.getId());
        Assert.assertNotSame(sp1.getId(), createdSp1.getId());
        Assert.assertEquals(createdSp1, service.getSpeciesByReferenceTaxonId(createdSp1.getReferenceTaxonId()));

        Species createdSp2 = Iterables.get(speciesList, 1);
        Assert.assertNotNull(createdSp2);
        Assert.assertEquals(sp2.getName(), createdSp2.getName());
        Assert.assertTrue(createdSp2.isReferenceTaxon());
        Assert.assertNotSame(sp2.getReferenceTaxonId(), createdSp2.getReferenceTaxonId());
        Assert.assertNull(createdSp2.getRefTaxCode());
        Assert.assertNull(createdSp2.getSurveyCode());
        Assert.assertNotNull(createdSp2.getId());
        Assert.assertNotSame(sp2.getId(), createdSp2.getId());
        Assert.assertEquals(createdSp2, service.getSpeciesByReferenceTaxonId(createdSp2.getReferenceTaxonId()));
    }

}
