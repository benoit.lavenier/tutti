package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;

/**
 * To test {@link IndividualObservationBatchPersistenceService} for write
 * operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class IndividualObservationBatchPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected IndividualObservationBatchPersistenceService service;

    private CaracteristicPersistenceService caracteristicService;

    protected FishingOperation fishingOperation;

    @Before
    public void setUp() throws Exception {
        caracteristicService = TuttiPersistenceServiceLocator.getCaracteristicPersistenceService();
        service = TuttiPersistenceServiceLocator.getIndividualObservationBatchPersistenceService();
        Cruise cruise = dbResource.getFixtures().createCruise();
        fishingOperation = dbResource.getFixtures().createFishingOperation(cruise);
        dbResource.getFixtures().createMinimalCatchBatch(fishingOperation);
    }


//    @Test
//    public void createIndividualObservationBatch(/*IndividualObservationBatch bean*/) {
//
//        // -----------------------------------------------------------------------------
//        // 1. Create with only mandatory properties
//        // -----------------------------------------------------------------------------
//
//        IndividualObservationBatch createdIndividualObservationBatch = dbResource.getFixtures().createMinimalIndividualObservationBatch(fishingOperation);
//
//        Assert.assertNotNull(createdIndividualObservationBatch);
//        Assert.assertNotNull(createdIndividualObservationBatch.getId());
//
//        // reload it
//        List<IndividualObservationBatch> allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt());
//        Assert.assertTrue(CollectionUtils.isNotEmpty(allIndividualObservationBatch));
//        IndividualObservationBatch reloadedIndividualObservationBatch = allIndividualObservationBatch.get(0);
//        reloadedIndividualObservationBatch.setFishingOperation(fishingOperation);
//        assertEqualsIndividualObservationBatch(createdIndividualObservationBatch, reloadedIndividualObservationBatch);
//
//        // -----------------------------------------------------------------------------
//        // 2. Create a full with all properties
//        // -----------------------------------------------------------------------------
//
//        createdIndividualObservationBatch.setId((String) null);
//
//        CaracteristicMap caracteristicMap = new CaracteristicMap();
//        createdIndividualObservationBatch.setCaracteristics(caracteristicMap);
//
//        // add a qualitative caracteristic
//        Caracteristic maturityCaracteristic = caracteristicService.getMaturityCaracteristic();
//        caracteristicMap.put(maturityCaracteristic, maturityCaracteristic.getQualitativeValue(0));
//
//        // add a numeric caracteristic
//        Caracteristic ageCaracteristic = caracteristicService.getAgeCaracteristic();
//        caracteristicMap.put(ageCaracteristic, 10.f);
//
//        // add a string caracteristic
//        Caracteristic stringCaracteristic = caracteristicService.getCaracteristic(dbResource.getFixtures().refAlphanumericPmfmId());
//        caracteristicMap.put(stringCaracteristic, "Un texte!");
//
//        createdIndividualObservationBatch.setComment("IndividualObservationBatch-full");
////        fullIndividualObservationBatch.setSamplingCode("SamplingCode");
////        fullIndividualObservationBatch.setCalcifiedPieceSamplingCode("CalcifiedPieceSamplingCode");
//
//        createdIndividualObservationBatch.setLengthStepCaracteristic(caracteristicService.getCaracteristic(dbResource.getFixtures().refNumericalPmfmId()));
//        createdIndividualObservationBatch.setSize(10.0f);
//        createdIndividualObservationBatch.setWeight(5.f);
//
//        IndividualObservationBatch createdIndividualObservationBatch2 =
//                service.createIndividualObservationBatch(createdIndividualObservationBatch);
//
//        assertEqualsIndividualObservationBatch(createdIndividualObservationBatch, createdIndividualObservationBatch2);
//
//        Assert.assertNotNull(createdIndividualObservationBatch2);
//        Assert.assertNotNull(createdIndividualObservationBatch2.getId());
//        assertEqualsIndividualObservationBatch(createdIndividualObservationBatch, createdIndividualObservationBatch2);
//
//        // reload it
//        allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt());
//        Assert.assertTrue(CollectionUtils.isNotEmpty(allIndividualObservationBatch));
//        Assert.assertEquals(2, allIndividualObservationBatch.size());
//        IndividualObservationBatch reloadedIndividualObservationBatch2 = TuttiEntities.splitById(allIndividualObservationBatch).get(createdIndividualObservationBatch2.getId());
//        reloadedIndividualObservationBatch2.setFishingOperation(fishingOperation);
//        assertEqualsIndividualObservationBatch(createdIndividualObservationBatch2, reloadedIndividualObservationBatch2);
//
//        // -----------------------------------------------------------------------------
//        // 2. Modify some properties and save
//        // -----------------------------------------------------------------------------
//        reloadedIndividualObservationBatch2.setSize(15f);
//        reloadedIndividualObservationBatch2.setWeight(35f);
//        createdIndividualObservationBatch.setComment("IndividualObservationBatch-modified");
////        fullIndividualObservationBatch.setSamplingCode("SamplingCode");
////        fullIndividualObservationBatch.setCalcifiedPieceSamplingCode("CalcifiedPieceSamplingCode");
//        reloadedIndividualObservationBatch2.getCaracteristics().clear();
//
//        IndividualObservationBatch savedIndividualObservationBatch = service.saveIndividualObservationBatch(reloadedIndividualObservationBatch2);
//        assertEqualsIndividualObservationBatch(reloadedIndividualObservationBatch2, savedIndividualObservationBatch);
//
//        allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt());
//        Assert.assertTrue(CollectionUtils.isNotEmpty(allIndividualObservationBatch));
//        IndividualObservationBatch reloadedIndividualObservationBatch3 = TuttiEntities.splitById(allIndividualObservationBatch).get(createdIndividualObservationBatch2.getId());
//        reloadedIndividualObservationBatch3.setFishingOperation(fishingOperation);
//        assertEqualsIndividualObservationBatch(savedIndividualObservationBatch, reloadedIndividualObservationBatch3);
//    }

//    @Test
//    public void deleteIndividualObservationBatch(/*String id*/) {
//
//        IndividualObservationBatch createdIndividualObservationBatch =
//                dbResource.getFixtures().createMinimalIndividualObservationBatch(fishingOperation);
//
//        List<IndividualObservationBatch> allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt() );
//        Assert.assertFalse(CollectionUtils.isEmpty(allIndividualObservationBatch));
//
//        service.deleteIndividualObservationBatch(createdIndividualObservationBatch.getIdAsInt() );
//
//        List<IndividualObservationBatch> allIndividualObservationBatch2 = service.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt() );
//        Assert.assertTrue(CollectionUtils.isEmpty(allIndividualObservationBatch2));
//    }
//
//    @Test
//    public void deleteIndividualObservationBatchForFishingOperation(/*String fishingOperationId*/) {
//
//        dbResource.getFixtures().createMinimalIndividualObservationBatch(fishingOperation);
//
//        dbResource.getFixtures().createMinimalIndividualObservationBatch(fishingOperation);
//
//        List<IndividualObservationBatch> allIndividualObservationBatch = service.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt() );
//        Assert.assertFalse(CollectionUtils.isEmpty(allIndividualObservationBatch));
//
//        service.deleteIndividualObservationBatchForFishingOperation(fishingOperation.getIdAsInt() );
//
//        List<IndividualObservationBatch> allIndividualObservationBatch2 = service.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt());
//        Assert.assertTrue(CollectionUtils.isEmpty(allIndividualObservationBatch2));
//    }

    protected void assertEqualsIndividualObservationBatch(IndividualObservationBatch expected,
                                                          IndividualObservationBatch actual) {
        Assert.assertEquals(expected, actual);
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getSize(), actual.getSize());
        Assert.assertEquals(expected.getComment(), actual.getComment());
//        Assert.assertEquals(expected.getSamplingCode(), actual.getSamplingCode());
//        Assert.assertEquals(expected.getCalcifiedPieceSamplingCode(), actual.getCalcifiedPieceSamplingCode());
        Assert.assertEquals(expected.getLengthStepCaracteristic(), actual.getLengthStepCaracteristic());
        Assert.assertEquals(expected.getWeight(), actual.getWeight());
        Assert.assertEquals(expected.getCaracteristics(), actual.getCaracteristics());
        Assert.assertEquals(expected.getFishingOperation(), actual.getFishingOperation());
    }
}
