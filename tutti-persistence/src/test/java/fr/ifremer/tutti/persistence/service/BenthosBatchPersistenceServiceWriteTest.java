package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * To test {@link BenthosBatchPersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
@Ignore
public class BenthosBatchPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected BenthosBatchPersistenceService service;

    protected CatchBatchPersistenceService catchBatchService;

    protected CruisePersistenceService cruiseService;

    protected FishingOperationPersistenceService fishingOperationService;

    protected CaracteristicPersistenceService caracteristicService;

    protected LocationPersistenceService locationService;

    protected SpeciesPersistenceService speciesService;

    /* 
     * Entities prepared in setUp() :
     * */
    protected Cruise cruise;

    protected FishingOperation fishingOperationNoCatchBatch;

    protected FishingOperation fishingOperationWithEmptyBatch;

    protected CatchBatch catchBacth;

    protected List<Species> species;

    protected Caracteristic sortedUnsortedPMFM;

    protected CaracteristicQualitativeValue horsVracQualitativeValue;

    protected CaracteristicQualitativeValue vracQualitativeValue;

    protected Caracteristic maturityPMFM;

    protected CaracteristicQualitativeValue firstMaturityQualitativeValue;

    protected Caracteristic sexPMFM;

    protected CaracteristicQualitativeValue maleQualitativeValue;

    protected CaracteristicQualitativeValue femaleQualitativeValue;

    protected CaracteristicQualitativeValue unkQualitativeValue;

    protected Caracteristic frequencyPMFM;

    @Before
    public void setUp() throws Exception {

        service = TuttiPersistenceServiceLocator.getBenthosBatchPersistenceService();
        cruiseService = TuttiPersistenceServiceLocator.getCruisePersistenceService();
        catchBatchService = TuttiPersistenceServiceLocator.getCatchBatchPersistenceService();
        fishingOperationService = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();
        caracteristicService = TuttiPersistenceServiceLocator.getCaracteristicPersistenceService();
        locationService = TuttiPersistenceServiceLocator.getLocationPersistenceService();
        speciesService = TuttiPersistenceServiceLocator.getSpeciesPersistenceService();

        species = speciesService.getAllSpecies();
        assertNotNull(species);
        assertTrue(species.size() > 2);

        cruise = cruiseService.getCruise(dbResource.getFixtures().cruiseId());
        cruise.setId((String) null);
        Calendar calendar = new GregorianCalendar();
        cruise.setBeginDate(calendar.getTime());
        calendar.add(Calendar.MONTH, 1); // add one month
        cruise.setEndDate(calendar.getTime());
        List<TuttiLocation> allHarbour = locationService.getAllHarbour();
        Assert.assertNotNull(allHarbour);
        Assert.assertTrue(allHarbour.size() > 1);
        cruise.setDepartureLocation(allHarbour.get(0));
        cruise.setReturnLocation(allHarbour.get(1));

        cruise = cruiseService.createCruise(cruise);

        // Create a first operation, with no cacth batch : to test CatchBatch insert/update :
        List<FishingOperation> fishingOperations = fishingOperationService.getAllFishingOperation(dbResource.getFixtures().cruiseId());
        assertNotNull(fishingOperations);
        assertTrue(fishingOperations.size() > 0);
        fishingOperationNoCatchBatch = fishingOperations.get(0);
        fishingOperationNoCatchBatch = fishingOperationService.getFishingOperation(fishingOperationNoCatchBatch.getIdAsInt());
        fishingOperationNoCatchBatch.setId((String) null);
        fishingOperationNoCatchBatch.setCruise(cruise);
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationNoCatchBatch.setGearShootingStartDate(calendar.getTime());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationNoCatchBatch.setGearShootingEndDate(calendar.getTime());
        fishingOperationNoCatchBatch = fishingOperationService.createFishingOperation(fishingOperationNoCatchBatch);

        // Create a second operation, with no cacth batch : to test CatchBatch insert/update :
        fishingOperationWithEmptyBatch = fishingOperations.get(1);
        fishingOperationWithEmptyBatch = fishingOperationService.getFishingOperation(fishingOperationWithEmptyBatch.getIdAsInt());
        fishingOperationWithEmptyBatch.setId((String) null);
        fishingOperationWithEmptyBatch.setCruise(cruise);
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 11);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationWithEmptyBatch.setGearShootingStartDate(calendar.getTime());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationWithEmptyBatch.setGearShootingEndDate(calendar.getTime());
        fishingOperationWithEmptyBatch = fishingOperationService.createFishingOperation(fishingOperationWithEmptyBatch);

        catchBacth = CatchBatchs.newCatchBatch();
        catchBacth.setFishingOperation(fishingOperationWithEmptyBatch);
        catchBacth = catchBatchService.createCatchBatch(catchBacth);

        sortedUnsortedPMFM = caracteristicService.getSortedUnsortedCaracteristic();
        horsVracQualitativeValue = sortedUnsortedPMFM.getQualitativeValue(0);
        vracQualitativeValue = sortedUnsortedPMFM.getQualitativeValue(1);
        maturityPMFM = caracteristicService.getMaturityCaracteristic();
        firstMaturityQualitativeValue = maturityPMFM.getQualitativeValue(0);
        sexPMFM = caracteristicService.getSexCaracteristic();
        maleQualitativeValue = sexPMFM.getQualitativeValue(1);
        femaleQualitativeValue = sexPMFM.getQualitativeValue(2);
        unkQualitativeValue = sexPMFM.getQualitativeValue(3);

        List<Caracteristic> cara = caracteristicService.getAllCaracteristic();
        for (Caracteristic caracteristic : cara) {
            if (caracteristic.getCaracteristicType() == CaracteristicType.NUMBER
                    && caracteristic.getPrecision() != null
                    && caracteristic.getPrecision() == 0.5f) {
                frequencyPMFM = caracteristic;
                break;
            }
        }
        assertNotNull("no numerical PMFM with a precision has been found. Could not define a PMFM for batch frequencies.", frequencyPMFM);

    }

    @Test
    public void createAndSaveBenthosBatchAndFrequencies() {
        SpeciesBatch esp1Batch;
        SpeciesBatch esp2Batch;
        SpeciesBatch frequenciesParentBatch;
        SpeciesBatch batch;
        Species taxon1 = species.get(0);
        Species taxon2 = species.get(1);

        // -----------------------------------------------------------------------------
        // 1. Test with only mandatory properties
        // -----------------------------------------------------------------------------
        // batch : "ESP1 - Vrac/5"
        batch = SpeciesBatchs.newBenthosBatch();
        batch.setParentBatch(null);
        batch.setFishingOperation(fishingOperationNoCatchBatch);
        batch.setSpecies(taxon1);
        batch.setSampleCategoryId(PmfmId.SORTED_UNSORTED.getValue());
        batch.setSampleCategoryValue(vracQualitativeValue);
        batch.setSampleCategoryWeight(5f);

        assertCreateAndReloadBenthosBatch(batch, null);

        // Save ESP1 batch
        esp1Batch = batch;

        // -----------------------------------------------------------------------------
        // 2. Test child "Male/2"
        // -----------------------------------------------------------------------------
        // Batch : ESP1 - Vrac/5 Male/2 ss-ech/1 Nombre/7
        batch = SpeciesBatchs.newBenthosBatch();
        batch.setId((String) null);
        batch.setParentBatch(esp1Batch);
        batch.setSpecies(taxon1);
        batch.setComment("ESP1 - Vrac/5   Male/2 ss-ech/1  Nombre/7");
        batch.setSampleCategoryId(PmfmId.SEX.getValue());
        batch.setSampleCategoryValue(maleQualitativeValue);
        batch.setSampleCategoryWeight(2f);
        batch.setWeight(1f);
        batch.setNumber(7);

        assertCreateAndReloadBenthosBatch(batch, batch.getParentBatch().getIdAsInt());

        // -----------------------------------------------------------------------------
        // 3. Test child "Female/2"
        // -----------------------------------------------------------------------------
        // Batch : ESP1 - Vrac/5 Female/3 Nombre/14
        batch = SpeciesBatchs.newBenthosBatch();
        batch.setId((String) null);
        batch.setParentBatch(esp1Batch);
        batch.setSpecies(taxon1);
        batch.setComment("ESP1 - Vrac/5   Female/3   Nombre/14");
        batch.setSampleCategoryId(PmfmId.SEX.getValue());
        batch.setSampleCategoryValue(femaleQualitativeValue);
        batch.setSampleCategoryWeight(3f);
        batch.setWeight(null);
        batch.setNumber(14);

        assertCreateAndReloadBenthosBatch(batch, batch.getParentBatch().getIdAsInt());

        // -----------------------------------------------------------------------------
        // 4. Test : ESP2 - Vrac/7
        // \- ESP2 - Vrac/7 UNK/2 ss-ech/1 Nombre/11
        // -----------------------------------------------------------------------------
        // batch : "ESP2 - Vrac/7 "
        batch = SpeciesBatchs.newBenthosBatch();
        batch.setParentBatch(null);
        batch.setSpecies(taxon2);
        batch.setSampleCategoryId(PmfmId.SORTED_UNSORTED.getValue());
        batch.setSampleCategoryValue(vracQualitativeValue);
        batch.setSampleCategoryWeight(7f);

        assertCreateAndReloadBenthosBatch(batch, null);
        esp2Batch = batch;

        // Batch : ESP2 - Vrac/7 UNK/2 ss-ech/1 Nombre/11
        batch = SpeciesBatchs.newBenthosBatch();
        batch.setId((String) null);
        batch.setParentBatch(esp2Batch);
        batch.setSpecies(taxon2);
        batch.setComment("ESP2 - Vrac/7   UNK/2  ss-ech/1   Nombre/11");
        batch.setSampleCategoryId(PmfmId.MATURITY.getValue());
        batch.setSampleCategoryValue(firstMaturityQualitativeValue);
        batch.setSampleCategoryWeight(2f);
        batch.setWeight(1f);
        batch.setNumber(11);

        assertCreateAndReloadBenthosBatch(batch, batch.getParentBatch().getIdAsInt());

        // -----------------------------------------------------------------------------
        // 5. Test save after modifications
        // -----------------------------------------------------------------------------
        // Batch : ESP2 - Vrac/7 UNK/1.75 ss-ech/1.11 Nombre/99
        batch.setComment("ESP2 - Vrac/7   UNK/1.75  ss-ech/1.11   Nombre/99");
        batch.setSampleCategoryId(PmfmId.SEX.getValue());
        batch.setSampleCategoryValue(unkQualitativeValue);
        batch.setSampleCategoryWeight(1.75f);
        batch.setWeight(1.11f);
        batch.setFishingOperation(fishingOperationWithEmptyBatch);
        batch.setNumber(99);

        // Save and reload, then check
        SpeciesBatch savedBatch = service.saveBenthosBatch(batch);
        assertBenthosBatch(savedBatch, batch, false);
        SpeciesBatch reloadedBatch = getBenthosBatch(fishingOperationWithEmptyBatch.getIdAsInt(), savedBatch.getIdAsInt());
        assertBenthosBatch(savedBatch, reloadedBatch, true);

        // Save batch for later
        frequenciesParentBatch = batch;

        // -----------------------------------------------------------------------------
        // 6. Test change species
        // -----------------------------------------------------------------------------
        esp2Batch.setSpecies(taxon1);
        service.changeBenthosBatchSpecies(esp2Batch.getIdAsInt(), taxon1);
        assertBenthosBatch(savedBatch, batch, false);
        reloadedBatch = getBenthosBatch(fishingOperationWithEmptyBatch.getIdAsInt(), esp2Batch.getIdAsInt());
        assertBenthosBatch(esp2Batch, reloadedBatch, true);

        // -----------------------------------------------------------------------------
        // 7. Test get all root species
        // -----------------------------------------------------------------------------
        List<SpeciesBatch> rootBenthosBatch = service.getRootBenthosBatch(fishingOperationWithEmptyBatch.getIdAsInt(), false).getChildren();
        assertNotNull(rootBenthosBatch);
        assertEquals(2, rootBenthosBatch.size());
        assertNotNull(rootBenthosBatch.get(0).getChildBatchs());
        assertTrue(rootBenthosBatch.get(0).getChildBatchs().size() > 0);
        assertNotNull(rootBenthosBatch.get(1).getChildBatchs());
        assertTrue(rootBenthosBatch.get(1).getChildBatchs().size() > 0);

        // -----------------------------------------------------------------------------
        // 8. Test batch frequency creation
        // -----------------------------------------------------------------------------

        List<SpeciesBatchFrequency> frequencies = Lists.newArrayList();
        float lengthStep = 0.5f;
        for (float length = lengthStep; length < lengthStep * 20; length += lengthStep) {
            SpeciesBatchFrequency frequency = SpeciesBatchFrequencys.newBenthosBatchFrequency();
            frequency.setLengthStep(length);
            frequency.setNumber((int) (length * 2));
            frequency.setWeight(0.01f * length * 2);
            frequency.setLengthStepCaracteristic(frequencyPMFM);
            frequency.setBatch(frequenciesParentBatch);
            frequencies.add(frequency);
        }
        List<SpeciesBatchFrequency> createdFrequencies = assertCreateAndReloadBenthosBatchFrequency(frequencies, frequenciesParentBatch.getIdAsInt());

        // -----------------------------------------------------------------------------
        // 9. Test batch frequency update
        // -----------------------------------------------------------------------------
        // Update some batchs (1cm, 2cm, etc)
        for (SpeciesBatchFrequency speciesBatchFrequency : createdFrequencies) {
            float length = speciesBatchFrequency.getLengthStep();
            if ((float) (int) length == length) {
                speciesBatchFrequency.setNumber(12);
            }
        }
        // And remove the last item (should be deleted in DB)
        // Note: use a new list (everything list coming from service are not modifiable)
        createdFrequencies = Lists.newArrayList(createdFrequencies);
        createdFrequencies.remove(createdFrequencies.size() - 1);

        List<SpeciesBatchFrequency> savedFrequencies = service.saveBenthosBatchFrequency(frequenciesParentBatch.getIdAsInt(), createdFrequencies);
        assertBatchFrequencies(createdFrequencies, savedFrequencies, true);
    }

    @Test
    public void deleteBenthosBatch(/* String id */) {
        SpeciesBatch esp1Batch;
        SpeciesBatch batch;
        Species taxon1 = species.get(0);

        // -----------------------------------------------------------------------------
        // 1. Create two batchs (parent + child), then remove the parent batch
        // -----------------------------------------------------------------------------
        // batch : ESP1 Vrac/5
        batch = SpeciesBatchs.newBenthosBatch();
        batch.setParentBatch(null);
        batch.setFishingOperation(fishingOperationWithEmptyBatch);
        batch.setSpecies(taxon1);
        batch.setSampleCategoryId(PmfmId.SORTED_UNSORTED.getValue());
        batch.setSampleCategoryValue(vracQualitativeValue);
        batch.setSampleCategoryWeight(5f);
        assertCreateAndReloadBenthosBatch(batch, null);
        esp1Batch = batch;

        // batch : ESP1 Vrac/5 Male/2
        batch = SpeciesBatchs.newBenthosBatch();
        batch.setParentBatch(esp1Batch);
        batch.setFishingOperation(fishingOperationWithEmptyBatch);
        batch.setSpecies(taxon1);
        batch.setSampleCategoryId(PmfmId.SEX.getValue());
        batch.setSampleCategoryValue(maleQualitativeValue);
        batch.setSampleCategoryWeight(2f);
        assertCreateAndReloadBenthosBatch(batch, esp1Batch.getIdAsInt());

        // Try to remove
        service.deleteBenthosBatch(esp1Batch.getIdAsInt());

        // Check if remove
        try {
            batch = getBenthosBatch(fishingOperationWithEmptyBatch.getIdAsInt(), esp1Batch.getIdAsInt());
            assertNull(batch);
        } catch (DataRetrievalFailureException drfe) {
            assertNotNull(drfe);
        }
    }

    protected void assertCreateAndReloadBenthosBatch(SpeciesBatch batch, Integer parentBatchId) {
        batch.setFishingOperation(fishingOperationWithEmptyBatch);

        // Create batch
        SpeciesBatch createdBatch = service.createBenthosBatch(batch, parentBatchId, true);
        assertBenthosBatch(batch, createdBatch, false);

        // then reload (for round trip check)
        SpeciesBatch reloadedBatch = getBenthosBatch(fishingOperationWithEmptyBatch.getIdAsInt(), createdBatch.getIdAsInt());
        if (parentBatchId == null) {
            assertNull(reloadedBatch.getParentBatch());
        } else {
            assertNotNull(reloadedBatch.getParentBatch());
            assertEquals(parentBatchId, reloadedBatch.getParentBatch().getIdAsInt());
        }
        assertBenthosBatch(createdBatch, reloadedBatch, false);

        batch.setId(createdBatch.getId());
    }

    protected void assertBenthosBatch(SpeciesBatch expectedBatch, SpeciesBatch actualBatch, boolean assertIdEquals) {
        assertNotNull(actualBatch);
        assertNotNull(actualBatch.getId());
        if (assertIdEquals && expectedBatch.getId() != null) {
            assertEquals(expectedBatch.getId(), actualBatch.getId());
        }
        assertEquals(expectedBatch.getWeight(), actualBatch.getWeight());
        assertEquals(expectedBatch.getSampleCategoryId(), actualBatch.getSampleCategoryId());
        if (expectedBatch.getSampleCategoryValue() != null && expectedBatch.getSampleCategoryValue() instanceof CaracteristicQualitativeValue) {
            assertNotNull("Bad sampleCategoryValue : expected <" + ((CaracteristicQualitativeValue) expectedBatch.getSampleCategoryValue()).getId()
                                  + "> but was <null>",
                          actualBatch.getSampleCategoryValue()
            );
            assertEquals(
                    ((CaracteristicQualitativeValue) expectedBatch.getSampleCategoryValue()).getId(),
                    ((CaracteristicQualitativeValue) actualBatch.getSampleCategoryValue()).getId());
        } else {
            assertEquals(expectedBatch.getSampleCategoryValue(), actualBatch.getSampleCategoryValue());
        }
        assertEquals(expectedBatch.getSampleCategoryWeight(), actualBatch.getSampleCategoryWeight());
        assertEquals(expectedBatch.getNumber(), actualBatch.getNumber());
        assertEquals(expectedBatch.getComment(), actualBatch.getComment());

        // Check species only if Vrac/HorsVrac or if batch has been load throw getAllxxx method
        // (Because getBenthosBatch(id) could not always retrieve the species)
        if (expectedBatch.getSpecies() != null && (
                PmfmId.SORTED_UNSORTED.getValue().equals(expectedBatch.getSampleCategoryId())
                        || actualBatch.getSpecies() != null)) {
            assertNotNull(actualBatch.getSpecies());
            assertEquals(expectedBatch.getSpecies().getId(), actualBatch.getSpecies().getId());
        }
    }

    protected List<SpeciesBatchFrequency> assertCreateAndReloadBenthosBatchFrequency(List<SpeciesBatchFrequency> frequencies, Integer parentBatchId) {

        // Create batch
        List<SpeciesBatchFrequency> createdFrequencies = service.saveBenthosBatchFrequency(parentBatchId, frequencies);
        assertBatchFrequencies(frequencies, createdFrequencies, false);

        // then reload (for round trip check)
        List<SpeciesBatchFrequency> reloadedFrequencies = service.getAllBenthosBatchFrequency(parentBatchId);
        assertBatchFrequencies(createdFrequencies, reloadedFrequencies, true);

        return createdFrequencies;
    }

    protected void assertBatchFrequencies(List<SpeciesBatchFrequency> expectedFrequencies,
                                          List<SpeciesBatchFrequency> actualFrequencies,
                                          boolean assertIdEquals) {
        assertNotNull(actualFrequencies);
        assertEquals(expectedFrequencies.size(), actualFrequencies.size());

        // Store actual batches into a map, using the length as key
        Map<Float, SpeciesBatchFrequency> expectedLengthMap = Maps.newHashMap();
        for (SpeciesBatchFrequency speciesBatchFrequency : expectedFrequencies) {
            expectedLengthMap.put(speciesBatchFrequency.getLengthStep(), speciesBatchFrequency);
        }

        // Store expected batches into a map, using the length as key
        Map<Float, SpeciesBatchFrequency> actualLengthMap = Maps.newHashMap();
        for (SpeciesBatchFrequency speciesBatchFrequency : actualFrequencies) {
            assertFalse("Duplicate lengthStep found in batchFrequencies, for length=" + speciesBatchFrequency.getLengthStep(),
                        actualLengthMap.containsKey(speciesBatchFrequency.getLengthStep()));
            actualLengthMap.put(speciesBatchFrequency.getLengthStep(), speciesBatchFrequency);
            assertNotNull(speciesBatchFrequency.getId());
        }

        for (Float lengthStep : expectedLengthMap.keySet()) {
            SpeciesBatchFrequency expectedBatchFrequency = expectedLengthMap.get(lengthStep);
            SpeciesBatchFrequency actualBatchFrequency = actualLengthMap.get(lengthStep);
            if (assertIdEquals) {
                assertEquals(expectedBatchFrequency.getId(), actualBatchFrequency.getId());
            }
            assertNotNull(expectedBatchFrequency.getLengthStepCaracteristic());
            assertEquals(expectedBatchFrequency.getLengthStepCaracteristic().getId(), actualBatchFrequency.getLengthStepCaracteristic().getId());
            assertEquals(expectedBatchFrequency.getNumber(), actualBatchFrequency.getNumber());
            assertEquals(expectedBatchFrequency.getWeight(), actualBatchFrequency.getWeight());
            // assertNotNull(expectedBatchFrequency.getBatch());
            // assertEquals(expectedBatchFrequency.getBatch().getId(), actualBatchFrequency.getBatch().getId());
        }
    }

    protected SpeciesBatch getBenthosBatch(Integer fishingOperationId, Integer speciesBatchId) {
        return getBenthosBatch(speciesBatchId, service.getRootBenthosBatch(fishingOperationId, false).getChildren());
    }

    protected SpeciesBatch getBenthosBatch(Integer speciesBatchId, List<SpeciesBatch> speciesBatchs) {
        if (speciesBatchs == null) {
            return null;
        }
        for (SpeciesBatch speciesBatch : speciesBatchs) {
            if (speciesBatchId.equals(speciesBatch.getIdAsInt())) {
                return speciesBatch;
            }
            if (speciesBatch.getChildBatchs() != null) {
                speciesBatch = getBenthosBatch(speciesBatchId, speciesBatch.getChildBatchs());
                if (speciesBatch != null) {
                    return speciesBatch;
                }
            }
        }
        return null;
    }
}
