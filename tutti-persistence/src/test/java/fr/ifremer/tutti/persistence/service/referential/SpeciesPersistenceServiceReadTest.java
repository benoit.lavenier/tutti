package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SpeciesPersistenceServiceReadTest extends ReferentialPersistenceServiceReadTestSupport {

    protected SpeciesPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getSpeciesPersistenceService();
        super.setUp();

    }

    @Test
    public void getAllSpecies() {
        List<Species> result = service.getAllSpecies();
        assertResultList(result, fixtures.refNbSpecies());
    }

    @Test
    public void getAllReferentSpecies() {
        List<Species> result = service.getAllReferentSpecies();
        TuttiEntities.splitById(result);
        assertResultList(result, fixtures.refNbReferentSpecies());

        Speciess.splitReferenceSpeciesByReferenceTaxonId(result);
    }

    @Test
    public void getSpeciesByReferenceTaxonId(/*String speciesReferenceTaxonId*/) {
        String speciesId = fixtures.refSpeciesId();
        Integer taxonId = fixtures.refSpeciesTaxonId();
        Species species = service.getSpeciesByReferenceTaxonId(taxonId);
        Assert.assertNotNull(species);
        Assert.assertNull(species.getVernacularCode());
        Assert.assertEquals(speciesId, species.getId());
        Assert.assertEquals(taxonId, species.getReferenceTaxonId());
        Assert.assertEquals(fixtures.refSpeciesRefTaxCode(), species.getRefTaxCode());
    }

    @Test
    public void getSpeciesByReferenceTaxonIdWithVernacularCode(/*String speciesReferenceTaxonId*/) {
        String speciesId = fixtures.refSpeciesId();
        Integer taxonId = fixtures.refSpeciesTaxonId();
        Species species = service.getSpeciesByReferenceTaxonIdWithVernacularCode(taxonId);
        Assert.assertNotNull(species);
        Assert.assertNull(species.getRefTaxCode());
        Assert.assertEquals(speciesId, species.getId());
        Assert.assertEquals(taxonId, species.getReferenceTaxonId());
        Assert.assertEquals(fixtures.refSpeciesVernacularCode(), species.getVernacularCode());
    }

    @Test
    public void getBadSpeciesByReferenceTaxonId(/*String speciesReferenceTaxonId*/) {
        Integer taxonId = fixtures.refBadSpeciesTaxonId();
        service.getSpeciesByReferenceTaxonId(taxonId);
    }

    @Test
    public void getBad2SpeciesByReferenceTaxonId(/*String speciesReferenceTaxonId*/) {
        Integer taxonId = fixtures.refBad2SpeciesTaxonId();
        service.getSpeciesByReferenceTaxonId(taxonId);
    }

}