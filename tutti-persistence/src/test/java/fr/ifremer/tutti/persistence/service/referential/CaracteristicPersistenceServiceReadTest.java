package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristics;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class CaracteristicPersistenceServiceReadTest extends ReferentialPersistenceServiceReadTestSupport {

    protected CaracteristicPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getCaracteristicPersistenceService();
        super.setUp();
    }

    @Test
    public void getAllCaracteristic() {
        List<Caracteristic> result = service.getAllCaracteristic();
        assertResultList(result, fixtures.refNbCaracteristic());
    }

    @Test
    public void getSizeCategoryCaracteristic() {
        Caracteristic result = service.getSizeCategoryCaracteristic();
        assertCaracteristicQualitative(result, 6);
    }

    @Test
    public void getSexCaracteristic() {
        Caracteristic result = service.getSexCaracteristic();
        assertCaracteristicQualitative(result, 4);
    }

    @Test
    public void getSortedUnsortedCaracteristic() {
        Caracteristic result = service.getSortedUnsortedCaracteristic();
        assertCaracteristicQualitative(result, 2);
    }

    @Test
    public void getMaturityCaracteristic() {
        Caracteristic result = service.getMaturityCaracteristic();
        assertCaracteristicQualitative(result, 6);
    }

    @Test
    public void getMacroWasteCategoryCaracteristic() {
        Caracteristic result = service.getMarineLitterCategoryCaracteristic();
        assertCaracteristicQualitative(result, 34);
    }

    @Test
    public void getMacroWasteSizeCategoryCaracteristic() {
        Caracteristic result = service.getMarineLitterSizeCategoryCaracteristic();
        assertCaracteristicQualitative(result, 7);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getCaracteristicWithNullQualitativeValue() {
        Caracteristic result = service.getCaracteristic(fixtures.caracteristicWithNullQualitativeValue());
        assertCaracteristicQualitative(result, 2);
        result.getQualitativeValue().add(null);
    }

    protected void assertCaracteristicQualitative(Caracteristic result, int nbValues) {
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getCaracteristicType());
        Assert.assertTrue(Caracteristics.isQualitativeCaracteristic(result));
        Assert.assertNotNull(result.getQualitativeValue());
        Assert.assertEquals(nbValues, result.sizeQualitativeValue());
        for (CaracteristicQualitativeValue qualitativeValue : result.getQualitativeValue()) {
            Assert.assertNotNull(qualitativeValue);
        }
    }

    protected void assertCaracteristicSize(Caracteristic incoming,
                                           Caracteristic caracteristic) {
        Assert.assertNotNull(incoming);
        Assert.assertNotNull(caracteristic);
        Assert.assertEquals(incoming, caracteristic);
        Assert.assertEquals(incoming.getCaracteristicType(),
                            caracteristic.getCaracteristicType());
        Assert.assertEquals(incoming.sizeQualitativeValue(),
                            caracteristic.sizeQualitativeValue());
    }

}