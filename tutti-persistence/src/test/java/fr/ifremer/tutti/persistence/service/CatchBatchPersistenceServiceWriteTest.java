package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * To test {@link CatchBatchPersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class CatchBatchPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected CatchBatchPersistenceService service;

    protected CruisePersistenceService cruiseService;

    protected FishingOperationPersistenceService fishingOperationService;

    protected IndividualObservationBatchPersistenceService individualObservationBatchPersistenceService;

    /* 
     * Entities prepared in setUp() :
     * */
    protected Cruise cruise;

    protected FishingOperation fishingOperation;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getCatchBatchPersistenceService();
        cruiseService = TuttiPersistenceServiceLocator.getCruisePersistenceService();
        fishingOperationService = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();
        individualObservationBatchPersistenceService = TuttiPersistenceServiceLocator.getIndividualObservationBatchPersistenceService();

        cruise = dbResource.getFixtures().createCruise();
        fishingOperation = dbResource.getFixtures().createFishingOperation(cruise);
    }

    @Test
    public void createAndSaveCatchBatch() throws Exception {
        CatchBatch catchBatch;

        catchBatch = CatchBatchs.newCatchBatch();
        catchBatch.setFishingOperation(fishingOperation);

        // -----------------------------------------------------------------------------
        // 1. Test with only mandatory properties
        // -----------------------------------------------------------------------------

        // Create and reload (test round trip)
        assertCreateAndReloadCatchBatch(catchBatch, fishingOperation.getIdAsInt());

        // -----------------------------------------------------------------------------
        // 2. Test with all properties
        // -----------------------------------------------------------------------------
        catchBatch.setId((String) null);
        // total weight : 100kg
        catchBatch.setCatchTotalWeight(75f);
        // Vrac :
        {
            // note : poids trie par la balance tremis (thalassa) (init par pupitri)
            catchBatch.setCatchTotalSortedTremisWeight(50f);
            // note : poids vrac caroussel (thalassa) (init par pupitri) (vrac trie) ou bien "poids trié fournie par la
            // table de tri (Sum(Si)
            catchBatch.setCatchTotalSortedCarousselWeight(45f);

            // Species
            {
                catchBatch.setSpeciesTotalSortedWeight(12f);
                catchBatch.setSpeciesTotalLivingNotItemizedWeight(0.2f);
                catchBatch.setSpeciesTotalInertWeight(0.1f);
            }

            // Benthos
            {
                catchBatch.setBenthosTotalSortedWeight(24f);
                catchBatch.setBenthosTotalLivingNotItemizedWeight(0.4f);
                catchBatch.setBenthosTotalInertWeight(0.2f);
            }
        }
        // Hors-Vrac :
        // MarineLitter
        catchBatch.setMarineLitterTotalWeight(100f);

        // Rejet : 15kg
        catchBatch.setCatchTotalRejectedWeight(15f);

        // Create and reload (test round trip)
        assertCreateAndReloadCatchBatch(catchBatch, fishingOperation.getIdAsInt());

        // -----------------------------------------------------------------------------
        // 2. Test save after modification
        // -----------------------------------------------------------------------------
        catchBatch.setCatchTotalSortedTremisWeight(null);
        catchBatch.setCatchTotalSortedCarousselWeight(null);
        catchBatch.setSpeciesTotalSortedWeight(null);
        catchBatch.setBenthosTotalSortedWeight(null);

        assertSaveAndReloadCatchBatch(catchBatch, fishingOperation.getIdAsInt());
    }

    @Test
    public void deleteCatchBatch(/*String fishingOperationId*/) {

        CatchBatch catchBatch = dbResource.getFixtures().createMinimalCatchBatch(fishingOperation);

        // total weight : 100kg
        catchBatch.setCatchTotalWeight(75f);
        // Vrac :
        // note : poids trie par la balance tremis (thalassa) (init par pupitri)
        catchBatch.setCatchTotalSortedTremisWeight(50f);
        // note : poids vrac caroussel (thalassa) (init par pupitri) (vrac trie) ou bien "poids trié fournie par la
        // table de tri (Sum(Si))
        catchBatch.setCatchTotalSortedCarousselWeight(45f);

        // Species
        catchBatch.setSpeciesTotalSortedWeight(12f);
        catchBatch.setSpeciesTotalLivingNotItemizedWeight(0.2f);
        catchBatch.setSpeciesTotalInertWeight(0.1f);

        // Benthos
        catchBatch.setBenthosTotalSortedWeight(24f);
        catchBatch.setBenthosTotalLivingNotItemizedWeight(0.4f);
        catchBatch.setBenthosTotalInertWeight(0.2f);

        // Hors-Vrac :
        // MarineLitter
        catchBatch.setMarineLitterTotalWeight(100f);

        // Rejet : 15kg
        catchBatch.setCatchTotalRejectedWeight(15f);
        service.saveCatchBatch(catchBatch);

        Integer fishingOperationId = fishingOperation.getIdAsInt();

        Assert.assertNotNull(service.getCatchBatchFromFishingOperation(fishingOperationId));
        Assert.assertFalse(CollectionUtils.isEmpty(individualObservationBatchPersistenceService.getAllIndividualObservationBatchsForFishingOperation(fishingOperationId)));
        service.deleteCatchBatch(fishingOperationId);

        try {
            service.getCatchBatchFromFishingOperation(fishingOperationId);
            Assert.fail();
        } catch (DataRetrievalFailureException e) {
            // feel good, no catch batch associated with fishing operation
        }

        try {
            individualObservationBatchPersistenceService.getAllIndividualObservationBatchsForFishingOperation(fishingOperationId);
            Assert.fail();
        } catch (DataRetrievalFailureException e) {
            // feel good, no catch batch associated with fishing operation
        }

    }

    protected void assertCreateAndReloadCatchBatch(CatchBatch catchBatch, Integer fishingOperationId) {
        CatchBatch createdCatchBatch = service.createCatchBatch(catchBatch);
        assertNotNull(createdCatchBatch);
        assertNotNull(createdCatchBatch.getId());
        assertCatchBatch(catchBatch, createdCatchBatch, false);

        CatchBatch reloadedCatchBatch = null;
        try {
            reloadedCatchBatch = service.getCatchBatchFromFishingOperation(fishingOperationId);
        } catch (InvalidBatchModelException e) {
            Assert.fail(e.getMessage());
        }
        assertCatchBatch(createdCatchBatch, reloadedCatchBatch, true);

        catchBatch.setId(createdCatchBatch.getId());
    }

    protected void assertSaveAndReloadCatchBatch(CatchBatch catchBatch, Integer fishingOperationId) {
        CatchBatch savedCatchBatch = service.saveCatchBatch(catchBatch);
        assertNotNull(savedCatchBatch);
        assertNotNull(savedCatchBatch.getId());
        assertCatchBatch(catchBatch, savedCatchBatch, false);

        CatchBatch reloadedCatchBatch = null;
        try {
            reloadedCatchBatch = service.getCatchBatchFromFishingOperation(fishingOperationId);
        } catch (InvalidBatchModelException e) {
            Assert.fail(e.getMessage());
        }
        assertCatchBatch(savedCatchBatch, reloadedCatchBatch, true);
    }

    protected void assertCatchBatch(CatchBatch expectedCatchBatch, CatchBatch actualCatchBatch, boolean assertIdEquals) {
        if (expectedCatchBatch == null) {
            assertNull(actualCatchBatch);
            return;
        }

        assertNotNull(actualCatchBatch);
        if (assertIdEquals) {
            assertEquals(expectedCatchBatch.getId(), actualCatchBatch.getId());
        }
        assertEquals(expectedCatchBatch.getCatchTotalWeight(), actualCatchBatch.getCatchTotalWeight());
        assertEquals(expectedCatchBatch.getCatchTotalSortedCarousselWeight(), actualCatchBatch.getCatchTotalSortedCarousselWeight());
        assertEquals(expectedCatchBatch.getCatchTotalSortedTremisWeight(), actualCatchBatch.getCatchTotalSortedTremisWeight());

        assertEquals(expectedCatchBatch.getSpeciesTotalSortedWeight(), actualCatchBatch.getSpeciesTotalSortedWeight());
        assertEquals(expectedCatchBatch.getSpeciesTotalInertWeight(), actualCatchBatch.getSpeciesTotalInertWeight());
        assertEquals(expectedCatchBatch.getSpeciesTotalLivingNotItemizedWeight(), actualCatchBatch.getSpeciesTotalLivingNotItemizedWeight());

        assertEquals(expectedCatchBatch.getBenthosTotalSortedWeight(), actualCatchBatch.getBenthosTotalSortedWeight());
        assertEquals(expectedCatchBatch.getBenthosTotalInertWeight(), actualCatchBatch.getBenthosTotalInertWeight());
        assertEquals(expectedCatchBatch.getBenthosTotalLivingNotItemizedWeight(), actualCatchBatch.getBenthosTotalLivingNotItemizedWeight());

        assertEquals(expectedCatchBatch.getMarineLitterTotalWeight(), actualCatchBatch.getMarineLitterTotalWeight());
    }
}
