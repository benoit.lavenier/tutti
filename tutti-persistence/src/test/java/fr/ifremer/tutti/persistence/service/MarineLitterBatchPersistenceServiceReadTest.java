package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

/**
 * To test {@link MarineLitterBatchPersistenceService} for read operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Ignore
public class MarineLitterBatchPersistenceServiceReadTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.readDb();

    protected MarineLitterBatchPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getMarineLitterBatchPersistenceService();
    }

    @Test
    public void getAllRootMarineLitterBatch(/*String fishingOperationId*/) {

    }
}
