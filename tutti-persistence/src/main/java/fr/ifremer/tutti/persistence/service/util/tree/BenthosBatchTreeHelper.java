package fr.ifremer.tutti.persistence.service.util.tree;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import org.springframework.stereotype.Component;

/**
 * Helper to build or navigate into the benthos batch tree.
 *
 * Created on 4/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
@Component("benthosBatchTreeHelper")
public class BenthosBatchTreeHelper extends SpeciesBatchTreeHelperSupport {

    public BenthosBatchTreeHelper() {
        super("Benthos", QualitativeValueId.SORTING_TYPE_BENTHOS, (short)2);
    }

}
