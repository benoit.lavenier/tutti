package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import java.io.Serializable;

/**
 * To define a sample category in application.
 *
 * It just wrap the underligned category, his order and his label.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SampleCategoryModelEntry implements Comparable<SampleCategoryModelEntry>, Serializable {

    private static final long serialVersionUID = 1L;

    protected String code;

    protected String label;

    protected Integer categoryId;

    protected int order;

    protected transient Caracteristic caracteristic;

    public void load(TuttiPersistence service) {
        Preconditions.checkNotNull(code, "Can't have a null code");
        Preconditions.checkNotNull(categoryId, "Can't have a null category id");
        caracteristic = service.getCaracteristic(categoryId);
        Preconditions.checkNotNull(caracteristic, "Could not find category if id: " + categoryId);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Caracteristic getCaracteristic() {
        return caracteristic;
    }

    public void setCaracteristic(Caracteristic caracteristic) {
        this.caracteristic = caracteristic;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int compareTo(SampleCategoryModelEntry o) {
        return Ints.compare(order, o.getOrder());
    }

    @Override
    public String toString() {
        return categoryId + "," + label + "," + code;
    }
}
