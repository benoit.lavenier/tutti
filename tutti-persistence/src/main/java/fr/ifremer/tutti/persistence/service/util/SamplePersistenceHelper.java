package fr.ifremer.tutti.persistence.service.util;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.data.measure.SampleMeasurement;
import fr.ifremer.adagio.core.dao.data.sample.Sample;
import fr.ifremer.adagio.core.dao.data.sample.SampleDao;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmImpl;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.SampleEntity;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.service.AbstractPersistenceService;
import fr.ifremer.tutti.persistence.service.AttachmentPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;

/**
 * Helper around {@link Sample}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.6
 */
@Component("samplePersistenceHelper")
public class SamplePersistenceHelper extends AbstractPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SamplePersistenceHelper.class);

    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

    @Resource(name = "attachmentPersistenceService")
    protected AttachmentPersistenceService attachmentPersistenceService;

    @Resource(name = "synchronizationStatusHelper")
    protected SynchronizationStatusHelper synchronizationStatusHelper;

    @Resource(name = "sampleDao")
    protected SampleDao sampleDao;

    public Sample create(Sample sample) {

        // Synchronization Status
        synchronizationStatusHelper.setDirty(sample);

        return sampleDao.create(sample);
    }

    public void update(Sample sample) {

        // Synchronization Status
        synchronizationStatusHelper.setDirty(sample);

        sampleDao.update(sample);
    }

    public Sample load(Integer id) {
        Sample sample = sampleDao.load(id);
        if (sample == null) {
            throw new DataRetrievalFailureException("Could not retrieve sample id=" + id);
        }
        return sample;
    }

    public void deleteSample(Integer sampleId) {

        if (log.isDebugEnabled()) {
            log.debug("Delete sample: " + sampleId);
        }
        Sample sample = load(sampleId);

        // Synchronization Status
        synchronizationStatusHelper.setDirty(sample);

        sample.getSampleMeasurements().clear();
        sampleDao.remove(sample);
        attachmentPersistenceService.deleteAllAttachment(ObjectTypeCode.SAMPLE, sampleId);
    }

    public void setSampleMeasurements(Sample target, CaracteristicMap caracteristics) {

        Set<SampleMeasurement> notChangedSampleMeasurements = Sets.newHashSet();
        if (target.getSampleMeasurements() != null) {
            notChangedSampleMeasurements.addAll(target.getSampleMeasurements());
        }

        for (Caracteristic caracteristic : caracteristics.keySet()) {
            SampleMeasurement vum = setSampleMeasurement(target, caracteristic, caracteristics.get(caracteristic));
            notChangedSampleMeasurements.remove(vum);
        }

        if (target.getSampleMeasurements() != null && notChangedSampleMeasurements.size() > 0) {
            target.getSampleMeasurements().removeAll(notChangedSampleMeasurements);
        }
    }

    public <S extends SampleEntity> void loadSampleMeasurements(S sample) {

        Integer sampleId = sample.getIdAsInt();
        Iterator<Object[]> list = queryList("sampleMeasurements", "sampleId", IntegerType.INSTANCE, sampleId);

        CaracteristicMap caracteristicMap = sample.getCaracteristics();

        while (list.hasNext()) {
//            int colIndex = 0;
            Object[] source = list.next();
            Integer pmfmId = (Integer) source[0];
            Caracteristic caracteristic = caracteristicService.getCaracteristic(pmfmId);
            Serializable value = getMeasurementValue(caracteristic, source);

//            Float numericalValue = (Float) source[colIndex++];
//            String alphanumericalValue = (String) source[colIndex++];
//            Integer qualitativeValueId = (Integer) source[colIndex];
//
//
//            Serializable value = null;
//            switch (caracteristic.getCaracteristicType()) {
//
//                case NUMBER:
//                    value = numericalValue;
//                    break;
//                case QUALITATIVE:
//                    value = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, qualitativeValueId);
//                    break;
//                case TEXT:
//                    value = alphanumericalValue;
//                    break;
//            }
            caracteristicMap.put(caracteristic, value);
        }

        Float weight = caracteristicMap.removeFloatValue(caracteristicService.getCaracteristic(PmfmId.WEIGHT_MEASURED.getValue()));
        if (weight != null) {
            sample.setWeight(weight);
        }

        String lengthClassId = caracteristicMap.removeStringValue(caracteristicService.getCaracteristic(PmfmId.ID_PMFM.getValue()));
        if (lengthClassId != null) {
            Caracteristic lengthStepCaracteristic = caracteristicService.getCaracteristic(Integer.valueOf(lengthClassId));
            sample.setLengthStepCaracteristic(lengthStepCaracteristic);

            Float length = caracteristicMap.removeFloatValue(lengthStepCaracteristic);
            sample.setSize(length);
        }

    }

    public <S extends SampleEntity> Serializable getSampleMeasurementValue(Integer sampleId, Caracteristic caracteristic) {

        Object[] source = queryUnique("sampleMeasurement",
                                      "sampleId", IntegerType.INSTANCE, sampleId,
                                      "pmfmId", IntegerType.INSTANCE, caracteristic.getIdAsInt());

        Serializable value = null;
        if (source != null) {
            value = getMeasurementValue(caracteristic, source);
        }

        return value;
    }

    public <S extends SampleEntity> CaracteristicMap extractCommonSampleCaracteristics(S sample) {

        CaracteristicMap caracteristics = CaracteristicMap.copy(sample.getCaracteristics());

        if (sample.getWeight() != null) {

            Caracteristic caracteristic = caracteristicService.getWeightMeasuredCaracteristic();
            caracteristics.put(caracteristic, sample.getWeight());
        }

        if (sample.getLengthStepCaracteristic() != null) {

            Caracteristic caracteristic = caracteristicService.getCaracteristic(PmfmId.ID_PMFM.getValue());
            caracteristics.put(caracteristic, sample.getLengthStepCaracteristic().getId());

            caracteristics.put(sample.getLengthStepCaracteristic(), sample.getSize());
        }

        return caracteristics;

    }

    protected SampleMeasurement setSampleMeasurement(Sample sample, Caracteristic caracteristic, Serializable value) {

        Integer pmfmId = caracteristic.getIdAsInt();

        SampleMeasurement result = null;
        if (sample.getSampleMeasurements() != null) {
            for (SampleMeasurement vum : sample.getSampleMeasurements()) {
                if (pmfmId.equals(vum.getPmfm().getId())) {
                    result = vum;
                    break;
                }
            }
        }
        if (result == null) {

            result = SampleMeasurement.Factory.newInstance();

            result.setSample(sample);
            if (sample.getSampleMeasurements() == null) {
                sample.setSampleMeasurements(Sets.newHashSet(result));
            } else {
                sample.getSampleMeasurements().add(result);
            }
            result.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));
            result.setDepartment(sample.getRecorderDepartment());
            result.setPmfm(load(PmfmImpl.class, pmfmId));
        }
        measurementPersistenceHelper.setMeasurement(result, caracteristic, value);
        return result;
    }

    protected Serializable getMeasurementValue(Caracteristic caracteristic, Object[] source) {

        Serializable value;
        // On commence à 1 car à 0 il y a le pmfmId
        int colIndex = 1;
//        Integer pmfmId = (Integer) source[colIndex++];
        Float numericalValue = (Float) source[colIndex++];
        String alphanumericalValue = (String) source[colIndex++];
        Integer qualitativeValueId = (Integer) source[colIndex];

        switch (caracteristic.getCaracteristicType()) {

            case NUMBER:
                value = numericalValue;
                break;
            case QUALITATIVE:
                value = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, qualitativeValueId);
                break;
            case TEXT:
                value = alphanumericalValue;
                break;
            default:
                throw new IllegalStateException("Can't deal with caracteristicType: " + caracteristic.getCaracteristicType());
        }
        return value;
    }

}
