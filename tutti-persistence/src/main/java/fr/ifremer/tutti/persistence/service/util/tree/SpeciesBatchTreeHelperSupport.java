package fr.ifremer.tutti.persistence.service.util.tree;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import org.springframework.dao.DataIntegrityViolationException;

import java.io.Serializable;
import java.text.MessageFormat;

/**
 * Helper to build or navigauet into the batch tree.
 *
 * Created on 4/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
public abstract class SpeciesBatchTreeHelperSupport extends BatchTreeHelperSupport {

    private final String prefix;

    private final QualitativeValueId sortingType;

    private final short rootBatchRankOrder;

    protected SpeciesBatchTreeHelperSupport(String prefix, QualitativeValueId sortingType, short rootBatchRankOrder) {
        this.prefix = prefix;
        this.sortingType = sortingType;
        this.rootBatchRankOrder = rootBatchRankOrder;
    }


    public void setBatchParents(Integer sampleCategoryId,
                                Serializable sampleCategoryValue,
                                SortingBatch target,
                                Integer parentBatchId,
                                CatchBatch catchBatch) {

        Preconditions.checkNotNull(target);
        Preconditions.checkNotNull(catchBatch);

        target.setRootBatch(catchBatch);

        SortingBatch parentBatch;
        if (parentBatchId != null) {

            // Load existing parent and root
            parentBatch = catchBatchDao.getSortingBatchById(catchBatch, parentBatchId);
        } else {

            // Or retrieve parent batch, from pmfm id
            // Retrieve category type
            if (!sampleCategoryId.equals(PmfmId.SORTED_UNSORTED.getValue())) {
                throw new DataIntegrityViolationException(MessageFormat.format(
                        "A species or benthos batch with no parent should have a sampleCategoryType {0} (PMFM.ID={1})",
                        PmfmId.SORTED_UNSORTED.getValue(),
                        sampleCategoryId));
            }

            Integer qualitativeValueId = convertSampleCategoryValueIntoQualitativeId(sampleCategoryValue);

            if (QualitativeValueId.SORTED_VRAC.getValue().equals(qualitativeValueId)) {

                // -- Vrac > Species > Alive itemized
                parentBatch = getVracAliveItemizedRootBatch(catchBatch);

                if (parentBatch == null) {

                    // -- Vrac
                    SortingBatch vracBatch = getVracBatch(catchBatch);

                    if (vracBatch == null) {
                        vracBatch = getOrCreateVracBatch(catchBatch, null, null);
                    }

                    // -- Vrac > Species
                    SortingBatch vracSpeciesBatch = getVracRootBatch(vracBatch);

                    if (vracSpeciesBatch == null) {
                        vracSpeciesBatch = getOrCreateVracRootBatch(catchBatch, vracBatch, null);
                    }

                    // -- Vrac > Species > Alive itemized
                    parentBatch = getOrCreateVracAliveItemizedRootBatch(catchBatch, vracSpeciesBatch);

                }
            } else if (QualitativeValueId.SORTED_HORS_VRAC.getValue().equals(qualitativeValueId)) {

                // -- Hors Vrac > Species
                parentBatch = getHorsVracRootBatch(catchBatch);

                if (parentBatch == null) {

                    // -- Hors Vrac
                    SortingBatch horsVracBatch = getOrCreateHorsVracBatch(catchBatch);

                    // -- Hors Vrac > Species
                    parentBatch = getOrCreateHorsVracRootBatch(catchBatch, horsVracBatch);
                }
            } else {

                // not possible
                throw new DataIntegrityViolationException("Should have Vrac / Hors Vrac qualitative value, but had: " + qualitativeValueId);
            }

        }

        Preconditions.checkNotNull(parentBatch);
        target.setParentBatch(parentBatch);
    }

    //------------------------------------------------------------------------//
    //-- Get CatchBatch navigation methods                                  --//
    //------------------------------------------------------------------------//

    public SortingBatch getVracAliveItemizedRootBatch(CatchBatch batch) {
        return getSortingBatch(batch,
                               "Vrac > " + prefix + " > Alive Itemized",
                               PmfmId.SORTED_UNSORTED.getValue(), QualitativeValueId.SORTED_VRAC.getValue(),
                               SORTING_TYPE_ID, sortingType.getValue(),
                               SORTING_TYPE2_ID, QualitativeValueId.SORTING_TYPE2_ALIVE_ITEMIZED.getValue());
    }

    public SortingBatch getHorsVracRootBatch(CatchBatch batch) {
        return getSortingBatch(batch,
                               "Hors Vrac > " + prefix,
                               PmfmId.SORTED_UNSORTED.getValue(), QualitativeValueId.SORTED_HORS_VRAC.getValue(),
                               SORTING_TYPE_ID, sortingType.getValue());
    }

//    public SortingBatch getVracBatch(CatchBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Vrac",
//                PmfmId.SORTED_UNSORTED.getValue(),
//                QualitativeValueId.SORTED_VRAC.getValue()
//        );
//    }
//
//    public SortingBatch getHorsVracBatch(CatchBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Hors Vrac",
//                PmfmId.SORTED_UNSORTED.getValue(),
//                QualitativeValueId.SORTED_HORS_VRAC.getValue()
//        );
//    }

//    public SortingBatch getRejectedBatch(CatchBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Unsorted",
//                PmfmId.SORTED_UNSORTED.getValue(),
//                QualitativeValueId.UNSORTED.getValue()
//        );
//    }

    //------------------------------------------------------------------------//
    //-- Get SortingBatch navigation methods                                --//
    //------------------------------------------------------------------------//

    public SortingBatch getVracRootBatch(SortingBatch batch) {
        return getSortingBatch(batch,
                               "Vrac > " + prefix,
                               SORTING_TYPE_ID,
                               sortingType.getValue());
    }

    public SortingBatch getVracAliveNotItemizedRootBatch(SortingBatch batch) {
        return getSortingBatch(batch,
                               "Vrac > " + prefix + " > Alive not itemized",
                               SORTING_TYPE2_ID,
                               QualitativeValueId.SORTING_TYPE2_ALIVE_NOT_ITEMIZED.getValue());
    }

//    public SortingBatch getBenthosVracAliveNotItemizedRootBatch(SortingBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Vrac > Benthos > Alive not itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_NOT_ITEMIZED.getValue()
//        );
//    }

    public SortingBatch getVracInertRootBatch(SortingBatch batch) {
        return getSortingBatch(batch,
                               "Vrac > " + prefix + " > Inert (not alive)",
                               SORTING_TYPE2_ID,
                               QualitativeValueId.SORTING_TYPE2_INERT.getValue());
    }

//    public SortingBatch getBenthosVracInertRootBatch(SortingBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Vrac > Benthos > Inert (not alive)",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_INERT.getValue()
//        );
//    }

    public SortingBatch getVracAliveItemizedRootBatch(SortingBatch batch) {
        return getSortingBatch(batch,
                               "Vrac > " + prefix + "> Alive itemized",
                               SORTING_TYPE2_ID,
                               QualitativeValueId.SORTING_TYPE2_ALIVE_ITEMIZED.getValue());
    }

//    public SortingBatch getBenthosVracAliveItemizedRootBatch(SortingBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Vrac > Benthos > Alive itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_ITEMIZED.getValue()
//        );
//    }

    public SortingBatch getHorsVracRootBatch(SortingBatch batch) {
        return getSortingBatch(batch,
                               "Hors Vrac > " + prefix,
                               SORTING_TYPE_ID,
                               QualitativeValueId.SORTING_TYPE_SPECIES.getValue());
    }

//    public SortingBatch getBenthosHorsVracRootBatch(SortingBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Hors Vrac > Benthos",
//                SORTING_TYPE_ID,
//                QualitativeValueId.SORTING_TYPE_BENTHOS.getValue()
//        );
//    }

    //------------------------------------------------------------------------//
    //-- getOrCreate methods                                                --//
    //------------------------------------------------------------------------//

//    public SortingBatch getOrCreateVracBatch(CatchBatch batch, Float weight, Float weightBeforeSampling) {
//        return getOrCreate(batch,
//                           batch,
//                           "Vrac",
//                           PmfmId.SORTED_UNSORTED.getValue(),
//                           QualitativeValueId.SORTED_VRAC.getValue(),
//                           weight,
//                           weightBeforeSampling,
//                           (short) 1);
//    }

    public SortingBatch getOrCreateVracRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return getOrCreate(
                target,
                batch,
                "Vrac > " + prefix,
                SORTING_TYPE_ID,
                sortingType.getValue(),
                totalWeight,
                rootBatchRankOrder
        );
    }

    public SortingBatch getOrCreateVracAliveNotItemizedRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return getOrCreate(
                target,
                batch,
                "Vrac > " + prefix + " > Alive Not Itemized",
                SORTING_TYPE2_ID,
                QualitativeValueId.SORTING_TYPE2_ALIVE_NOT_ITEMIZED.getValue(),
                totalWeight,
                (short) 1
        );
    }

    public SortingBatch getOrCreateVracInertRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return getOrCreate(
                target,
                batch,
                "Vrac > " + prefix + " > Inert",
                SORTING_TYPE2_ID,
                QualitativeValueId.SORTING_TYPE2_INERT.getValue(),
                totalWeight,
                (short) 2
        );
    }

    public SortingBatch getOrCreateVracAliveItemizedRootBatch(CatchBatch target, SortingBatch batch) {
        return getOrCreate(
                target,
                batch,
                "Vrac > " + prefix + " > Alive Itemized",
                SORTING_TYPE2_ID,
                QualitativeValueId.SORTING_TYPE2_ALIVE_ITEMIZED.getValue(),
                (short) 3
        );
    }

//    public SortingBatch getOrCreateBenthosVracRootBatch(CatchBatch target,
//                                                        SortingBatch batch,
//                                                        Float totalWeight) {
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos",
//                SORTING_TYPE_ID,
//                QualitativeValueId.SORTING_TYPE_BENTHOS.getValue(),
//                totalWeight,
//                (short) 2
//        );
//    }

//    public SortingBatch getOrCreateBenthosVracAliveNotItemizedRootBatch(CatchBatch target,
//                                                                        SortingBatch batch,
//                                                                        Float totalWeight) {
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos > Alive Not itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_NOT_ITEMIZED.getValue(),
//                totalWeight,
//                (short) 1
//        );
//    }

//    public SortingBatch getOrCreateBenthosVracInertRootBatch(CatchBatch target,
//                                                             SortingBatch batch,
//                                                             Float totalWeight) {
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos > Inert",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_INERT.getValue(),
//                totalWeight,
//                (short) 2
//        );
//    }

//    public SortingBatch getOrCreateBenthosVracAliveItemizedRootBatch(CatchBatch target,
//                                                                     SortingBatch batch) {
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos > Alive Itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_ITEMIZED.getValue(),
//                (short) 3
//        );
//    }

//    public SortingBatch getOrCreateHorsVracBatch(CatchBatch batch) {
//        return getOrCreate(
//                batch,
//                batch,
//                "Hors Vrac",
//                PmfmId.SORTED_UNSORTED.getValue(),
//                QualitativeValueId.SORTED_HORS_VRAC.getValue(),
//                (short) 2
//        );
//    }

    public SortingBatch getOrCreateHorsVracRootBatch(CatchBatch target, SortingBatch batch) {
        return getOrCreate(
                target,
                batch,
                "Hors Vrac > " + prefix,
                SORTING_TYPE_ID,
                sortingType.getValue(),
                rootBatchRankOrder
        );
    }

//    public SortingBatch getOrCreateBenthosHorsVracRootBatch(CatchBatch target,
//                                                            SortingBatch batch) {
//        return getOrCreate(
//                target,
//                batch,
//                "Hors Vrac > Benthos",
//                SORTING_TYPE_ID,
//                QualitativeValueId.SORTING_TYPE_BENTHOS.getValue(),
//                (short) 2
//        );
//    }


    private Integer convertSampleCategoryValueIntoQualitativeId(Serializable value) {
        if (value == null) {
            return null;
        }
        Integer qualitativeValueId = null;
        if (value instanceof CaracteristicQualitativeValue) {
            CaracteristicQualitativeValue cqValue = (CaracteristicQualitativeValue) value;
            qualitativeValueId = cqValue.getIdAsInt();
        } else if (value instanceof String) {
            qualitativeValueId = Integer.valueOf((String) value);
        }
        return qualitativeValueId;
    }

}
