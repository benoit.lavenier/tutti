package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Transactional(readOnly = true)
public interface PersonPersistenceService extends TuttiPersistenceServiceImplementor {

    @Cacheable(value = "persons")
    List<Person> getAllPerson();

    @Cacheable(value = "personsWithObsoletes")
    List<Person> getAllPersonWithObsoletes();

    @Cacheable(value = "personById", key = "#personId")
    Person getPerson(Integer personId);

    /**
     * Check if the temporary person with the given {@code id} is used.
     *
     * @param id id of the person to check
     * @return {@code true} if person is temporary
     * @since 3.8
     */
    boolean isTemporaryPersonUsed(Integer id);

    /**
     * Add temporary persons.
     *
     * @param persons persons to add
     * @return added persons
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"persons", "personById", "personsWithObsoletes"}, allEntries = true)
    List<Person> addTemporaryPersons(List<Person> persons);

    /**
     * Update temporary persons.
     *
     * @param persons persons to update
     * @return updated persons
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"persons", "personById", "personsWithObsoletes"}, allEntries = true)
    List<Person> updateTemporaryPersons(List<Person> persons);

    /**
     * Link temporary persons. (Means get existing references using persons natural ids).
     *
     * @param persons persons to link
     * @return linked persons
     * @since 3.14
     */
    List<Person> linkTemporaryPersons(List<Person> persons);

    /**
     * Replace the {@code source} person by
     * the {@code target} one in all data.
     *
     * @param source the source person to replace
     * @param target the target person to use
     * @param delete flag to delete the temporary vessel after replacement
     * @since 3.6
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fr.ifremer.adagio.core.dao.data.batch.CatchBatchCache", "persons", "personById", "personsWithObsoletes"},
            allEntries = true)
    void replacePerson(Person source, Person target, boolean delete);

    /**
     * Delete the temporary persons with the given {@code ids}.
     *
     * @param ids ids of the persons to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"persons", "personById", "personsWithObsoletes"}, allEntries = true)
    void deleteTemporaryPersons(Collection<Integer> ids);

    /**
     * Delete the temporary person with the given {@code id}.
     *
     * @param id id of the person to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"persons", "personById", "personsWithObsoletes"}, allEntries = true)
    void deleteTemporaryPerson(Integer id);

}
