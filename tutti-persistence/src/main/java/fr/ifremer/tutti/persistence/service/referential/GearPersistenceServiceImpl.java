package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.StatusCode;
import fr.ifremer.adagio.core.dao.referential.gear.FishingGearExtendDao;
import fr.ifremer.adagio.core.dao.referential.gear.GearClassificationId;
import fr.ifremer.adagio.core.dao.referential.gear.GearClassificationImpl;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Service("gearPersistenceService")
public class GearPersistenceServiceImpl extends ReferentialPersistenceServiceSupport implements GearPersistenceService {

    @Resource(name = "fishingGearDao")
    protected FishingGearExtendDao fishingGearDao;

    @Override
    public List<Gear> getAllScientificGear() {

        Iterator<Object[]> sources = queryListWithStatus(
                "allGears",
                "gearClassificiationId", IntegerType.INSTANCE, GearClassificationId.SCIENTIFIC_CRUISE.getValue());
        List<Gear> result = Lists.newArrayList();
        loadGears(sources, result);
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Gear> getAllFishingGear() {

        Iterator<Object[]> sources = queryListWithStatus(
                "allGears",
                "gearClassificiationId", IntegerType.INSTANCE, GearClassificationId.FAO.getValue());
        List<Gear> result = Lists.newArrayList();
        loadGears(sources, result);
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Gear> getAllGearWithObsoletes() {

        List<Gear> result = Lists.newArrayList();

        Iterator<Object[]> fishingSources = queryListWithStatus2(
                "allGearsWithObsoletes",
                "gearClassificiationId", IntegerType.INSTANCE, GearClassificationId.FAO.getValue());
        loadGears(fishingSources, result);

        Iterator<Object[]> scientificSources = queryListWithStatus2(
                "allGearsWithObsoletes",
                "gearClassificiationId", IntegerType.INSTANCE, GearClassificationId.SCIENTIFIC_CRUISE.getValue());
        loadGears(scientificSources, result);
        return Collections.unmodifiableList(result);

    }

    @Override
    public Gear getGear(Integer gearId) {

        Object[] source = queryUniqueWithStatus2(
                "gearById",
                "gearId", IntegerType.INSTANCE, gearId);

        return source == null ? null : loadGear(source);

    }

    protected void loadGears(Iterator<Object[]> sources, List<Gear> result) {
        while (sources.hasNext()) {
            Object[] source = sources.next();
            Gear target = loadGear(source);
            result.add(target);
        }
    }

    @Override
    public boolean isTemporaryGearUsed(Integer id) {
        Long count = queryUniqueTyped("countGearInGearPhysicalFeatures", "id", IntegerType.INSTANCE, id);
        boolean result = count > 0;

        if (!result) {
            count = queryUniqueTyped("countGearInGearUseFeatures", "id", IntegerType.INSTANCE, id);
            result = count > 0;
        }
        return result;
    }

    @Override
    public List<Gear> addTemporaryGears(List<Gear> gears) {

        List<Gear> result = Lists.newArrayList();
        for (Gear source : gears) {
            Gear added = addTemporaryGear(source);
            result.add(added);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Gear> updateTemporaryGears(List<Gear> gears) {

        List<Gear> result = Lists.newArrayList();
        for (Gear source : gears) {
            Gear updated = updateTemporaryGear(source);
            result.add(updated);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Gear> linkTemporaryGears(List<Gear> gears) {

        List<Gear> result = Lists.newArrayList();
        for (Gear source : gears) {
            Gear linked = linkTemporaryGear(source);
            result.add(linked);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public void replaceGear(Gear source, Gear target, boolean delete) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(target);
        Preconditions.checkState(Gears.isTemporary(source));
        Preconditions.checkState(!Gears.isTemporary(target));

        Integer sourceId = source.getIdAsInt();
        Integer targetId = target.getIdAsInt();

        queryUpdate("replaceGearInGearPhysicalFeatures",
                    "sourceId", IntegerType.INSTANCE, sourceId,
                    "targetId", IntegerType.INSTANCE, targetId);

        queryUpdate("replaceGearInGearUseFeatures",
                    "sourceId", IntegerType.INSTANCE, sourceId,
                    "targetId", IntegerType.INSTANCE, targetId);

        //TODO Check doublon...

        if (delete) {

            deleteTemporaryGear(sourceId);

        }

    }

    @Override
    public void deleteTemporaryGears(Collection<Integer> ids) {

        for (Integer id : ids) {
            deleteTemporaryGear(id);
        }

    }

    @Override
    public void deleteTemporaryGear(Integer id) {

        Preconditions.checkNotNull(id);
        if (id > 0) {
            throw new ApplicationBusinessException(String.format("Can't delete a Gear with a positive id %d.", id));
        }
        Gear gear = getGear(id);
        if (gear == null) {
            throw new ApplicationBusinessException(String.format("Gear with id %d does not exists", id));
        }

        fishingGearDao.remove(id);

    }

    protected Gear addTemporaryGear(Gear source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getLabel());
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkArgument(source.getId() == null || Gears.isTemporaryId(source.getIdAsInt()));

        Integer gearClassificationId = getGearClassificationId(source);

        fr.ifremer.adagio.core.dao.referential.gear.Gear target = fishingGearDao.createAsTemporary(source.getLabel(), source.getName(), gearClassificationId);
        Gear result = Gears.newGear();
        result.setId(target.getId());

        // Fill the result bean
        result.setLabel(source.getLabel());
        result.setName(source.getName());
        result.setScientificGear(source.isScientificGear());
        setStatus(StatusCode.TEMPORARY.getValue(), result);

        return result;

    }

    protected Gear updateTemporaryGear(Gear source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkNotNull(source.getLabel());
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkArgument(Gears.isTemporaryId(source.getIdAsInt()));

        Gear result = getGear(source.getIdAsInt());

        // Fill the result bean
        result.setLabel(source.getLabel());
        result.setName(source.getName());
        result.setScientificGear(source.isScientificGear());
        setStatus(StatusCode.TEMPORARY.getValue(), result);

        fr.ifremer.adagio.core.dao.referential.gear.FishingGear toUpdate = fishingGearDao.load(source.getIdAsInt());
        toUpdate.setLabel(result.getLabel());
        toUpdate.setName(result.getName());
        Integer gearClassificationId = getGearClassificationId(source);
        toUpdate.setGearClassification(load(GearClassificationImpl.class, gearClassificationId));
        fishingGearDao.update(toUpdate);

        return result;

    }

    private Integer getGearClassificationId(Gear source) {
        Integer gearClassificationId;
        if (source.isScientificGear()) {
            gearClassificationId = GearClassificationId.SCIENTIFIC_CRUISE.getValue();
        } else {
            gearClassificationId = GearClassificationId.FAO.getValue();
        }
        return gearClassificationId;
    }

    protected Gear linkTemporaryGear(Gear source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkNotNull(source.getLabel());
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkArgument(Gears.isTemporaryId(source.getIdAsInt()));

        Object[] row = queryUniqueWithStatus2(
                "gearByName",
                "gearName", StringType.INSTANCE, source.getName());

        return row == null ? null : loadGear(row);

    }

    protected Gear loadGear(Object... source) {

        Gear result = Gears.newGear();
        result.setId(String.valueOf(source[0]));
        result.setLabel((String) source[1]);
        result.setName((String) source[2]);
        Integer classification = (Integer) source[3];
        boolean scientific = false;
        if (classification != null) {
            scientific = GearClassificationId.SCIENTIFIC_CRUISE.getValue().equals(classification);
        }
        result.setScientificGear(scientific);

        setStatus((String) source[4], result);
        return result;

    }
}
