package fr.ifremer.tutti.persistence;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * A class loader that search first in a given directory before in parent
 * class loader.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class RessourceClassLoader extends ClassLoader {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RessourceClassLoader.class);

    public static final URL[] EMPTY_URL_ARRAY = new URL[0];

    protected URLClassLoader loader;

    protected Predicate<String> searchInDirectoriesPredicate;

    protected final List<File> directories;

    public RessourceClassLoader(ClassLoader parent) {
        super(parent);

        // by default try in directories if there is some
        this.searchInDirectoriesPredicate = new Predicate<String>() {
            @Override
            public boolean apply(String input) {
                return !directories.isEmpty();
            }
        };
        directories = Lists.newArrayList();
        loader = URLClassLoader.newInstance(EMPTY_URL_ARRAY);
    }

    public void addDirectory(File... directories) {
        for (File directory : directories) {
            if (!this.directories.contains(directory)) {
                this.directories.add(directory);

                // force to create directory
                ApplicationIOUtil.forceMkdir(directory, t("tutti.io.mkDir.error", directory));
            }
        }
        loader = null;
    }

    public void removeDirectory(File... directories) {
        for (File directory : directories) {
            this.directories.remove(directory);
        }
        loader = null;
    }

    public Predicate<String> getSearchInDirectoriesPredicate() {
        return searchInDirectoriesPredicate;
    }

    public void addSearchInDirectoriesPredicate(Predicate<String> predicate) {
        Preconditions.checkNotNull(predicate,
                                   "search predicate can not be null");
        this.searchInDirectoriesPredicate =
                Predicates.and(searchInDirectoriesPredicate, predicate);
    }

    public void setSearchInDirectoriesPredicate(Predicate<String> searchInDirectoriesPredicate) {
        Preconditions.checkNotNull(searchInDirectoriesPredicate,
                                   "search predicate can not be null");
        this.searchInDirectoriesPredicate = searchInDirectoriesPredicate;
    }

    @Override
    public URL findResource(String name) {
        URL result = null;
        if (searchInDirectoriesPredicate.apply(name)) {
            if (log.isDebugEnabled()) {
                log.debug("findResource [" + name + "] in " + directories);
            }
            result = getLoader().findResource(name);
        }
        if (result == null) {
            result = super.findResource(name);
        }
        return result;
    }

    @Override
    public Enumeration<URL> findResources(String name) throws IOException {
        Enumeration<URL> result = null;
        if (searchInDirectoriesPredicate.apply(name)) {
            if (log.isDebugEnabled()) {
                log.debug("findResources [" + name + "] in " + directories);
            }
            result = getLoader().findResources(name);
        }
        if (result == null || !result.hasMoreElements()) {
            result = super.findResources(name);
        }
        return result;
    }

    @Override
    public URL getResource(String name) {
        URL result = null;
        if (searchInDirectoriesPredicate.apply(name)) {
            if (log.isDebugEnabled()) {
                log.debug("getResource [" + name + "] in " +
                          directories);
            }
            result = getLoader().getResource(name);
        }
        if (result == null) {
            result = super.getResource(name);
        }
        return result;
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        Enumeration<URL> result = null;
        if (searchInDirectoriesPredicate.apply(name)) {
            if (log.isDebugEnabled()) {
                log.debug("getResources [" + name + "] in " + directories);
            }
            result = getLoader().getResources(name);
        }
        if (result == null || !result.hasMoreElements()) {
            result = super.getResources(name);
        }
        return result;
    }

    protected URLClassLoader getLoader() {
        if (loader == null) {
            try {
                URL[] urls = FileUtils.toURLs(
                        directories.toArray(new File[directories.size()]));
                loader = URLClassLoader.newInstance(urls, null);
            } catch (IOException e) {
                throw new ApplicationTechnicalException(t("tutti.persistence.loader.error", directories), e);
            }
        }
        return loader;
    }
}
