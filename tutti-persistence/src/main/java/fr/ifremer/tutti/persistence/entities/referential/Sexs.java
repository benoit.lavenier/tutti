package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;

/**
 * Created on 02/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class Sexs {


    public static boolean isMale(CaracteristicQualitativeValue caracteristicQualitativeValue) {
        return QualitativeValueId.SEX_MALE.getValue().equals(caracteristicQualitativeValue.getIdAsInt());
    }

    public static boolean isFemale(CaracteristicQualitativeValue caracteristicQualitativeValue) {
        return QualitativeValueId.SEX_FEMALE.getValue().equals(caracteristicQualitativeValue.getIdAsInt());
    }

}
