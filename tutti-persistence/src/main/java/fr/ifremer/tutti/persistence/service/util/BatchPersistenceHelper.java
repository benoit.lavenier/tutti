package fr.ifremer.tutti.persistence.service.util;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatchExtendDao;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatchDao;
import fr.ifremer.adagio.core.dao.data.batch.validator.CatchBatchValidationError;
import fr.ifremer.adagio.core.dao.data.batch.validator.CatchBatchValidationException;
import fr.ifremer.adagio.core.dao.data.operation.FishingOperation;
import fr.ifremer.adagio.core.dao.data.operation.FishingOperationDao;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.service.AbstractPersistenceService;
import fr.ifremer.tutti.persistence.service.AccidentalBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.AttachmentPersistenceService;
import fr.ifremer.tutti.persistence.service.IndividualObservationBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.batch.TuttiCatchBatchValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Helper around batches.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
@Component("batchPersistenceHelper")
public class BatchPersistenceHelper extends AbstractPersistenceService {

//    /** Logger. */
//    private static final Log log =
//            LogFactory.getLog(BatchPersistenceHelper.class);

//    @Resource(name = "caracteristicPersistenceService")
//    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "attachmentPersistenceService")
    protected AttachmentPersistenceService attachmentPersistenceService;

    @Resource(name = "individualObservationBatchPersistenceService")
    protected IndividualObservationBatchPersistenceService individualObservationBatchPersistenceService;

    @Resource(name = "accidentalBatchPersistenceService")
    protected AccidentalBatchPersistenceService accidentalBatchService;

    @Resource(name = "catchBatchDao")
    protected CatchBatchExtendDao catchBatchDao;

    @Resource(name = "sortingBatchDao")
    protected SortingBatchDao sortingBatchDao;

    @Resource(name = "fishingOperationDao")
    protected FishingOperationDao fishingOperationDao;

    @Resource(name = "scientificCruiseCatchBatchValidator")
    protected TuttiCatchBatchValidator catchBatchValidator;

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

//    @Resource(name = "batchTreeHelper")
//    protected BatchTreeHelper batchTreeHelper;

    @Resource(name = "synchronizationStatusHelper")
    protected SynchronizationStatusHelper synchronizationStatusHelper;

    @Override
    public void init() {
        super.init();
        catchBatchDao.registerCatchBatchValidator(catchBatchValidator);
    }

    @Override
    public void close() {
        catchBatchDao.unregisterCatchBatchValidator(catchBatchValidator);
        super.close();
    }

    public fr.ifremer.tutti.persistence.entities.data.CatchBatch createCatchBatch(fr.ifremer.tutti.persistence.entities.data.CatchBatch bean, CatchBatch catchBatch) {
        catchBatch = catchBatchDao.create(catchBatch);
        bean.setId(catchBatch.getId());
        return bean;
    }

    public void validateSpecies(SampleCategoryModel sampleCategoryModel,
                                BatchContainer<SpeciesBatch> species) throws InvalidBatchModelException {
        List<CatchBatchValidationError> errors = catchBatchValidator.validateSpecies(sampleCategoryModel, species);
        List<String> errorsStr = Lists.newArrayList();
        errorsStr.addAll(errors.stream()
                               .filter(error -> error.getGravity() == CatchBatchValidationError.GRAVITY_ERROR)
                               .map(CatchBatchValidationError::getMessage)
                               .collect(Collectors.toList()));
        if (!errorsStr.isEmpty()) {
            String join = Joiner.on("<br/>").join(errorsStr);
            throw new InvalidBatchModelException(t("tutti.persistence.batch.validation.bad.sample.categories", join));
        }
    }

    public void validateBenthos(SampleCategoryModel sampleCategoryModel,
                                BatchContainer<SpeciesBatch> benthos) throws InvalidBatchModelException {
        List<CatchBatchValidationError> errors = catchBatchValidator.validateBenthos(sampleCategoryModel, benthos);

        if (CollectionUtils.isNotEmpty(errors)) {
            List<String> errorsStr = Lists.newArrayList();
            errorsStr.addAll(errors.stream()
                                   .filter(error -> error.getGravity() == CatchBatchValidationError.GRAVITY_ERROR)
                                   .map(CatchBatchValidationError::getMessage)
                                   .collect(Collectors.toList()));
            if (!errorsStr.isEmpty()) {
                String join = Joiner.on("<br/>").join(errorsStr);
                throw new InvalidBatchModelException(t("tutti.persistence.batch.validation.bad.sample.categories", join));
            }
        }
    }

    public void deleteCatchBatch(Integer fishingOperationId, Integer catchBatchId) {

        // delete accidental batchs
        accidentalBatchService.deleteAccidentalBatchForFishingOperation(fishingOperationId);
        getCurrentSession().flush();

        // delete all samples
        individualObservationBatchPersistenceService.deleteAllIndividualObservationsForFishingOperation(fishingOperationId);
        getCurrentSession().flush();

        // SynchronizationStatus on fishingTrip
        FishingOperation fishingOperation = fishingOperationDao.load(fishingOperationId);
        synchronizationStatusHelper.setDirty(fishingOperation.getFishingTrip());

        // get all catch batch children ids (to delete attachments)
        Set<Integer> ids = getBatchIds(catchBatchId);
        ids.remove(catchBatchId);

        catchBatchDao.remove(catchBatchId);
        attachmentPersistenceService.deleteAllAttachment(ObjectTypeCode.CATCH_BATCH, catchBatchId);

        getCurrentSession().flush();

        attachmentPersistenceService.deleteAllAttachment(ObjectTypeCode.BATCH, ids);

    }

    public <D extends TuttiEntity> D createSortingBatch(D bean, CatchBatch catchBatch, SortingBatch batch) {
        // Synchronization status
        synchronizationStatusHelper.setDirty(catchBatch);
        batch = catchBatchDao.createSortingBatch(batch, catchBatch);
        bean.setId(batch.getId());
        return bean;
    }

    public Set<Integer> getBatchIds(Integer batchId) {
        Set<Integer> ids = Sets.newHashSet(catchBatchDao.getAllChildrenIds(batchId));
        ids.add(batchId);
        return ids;
    }

    public void removeWithChildren(Integer batchId) {

        individualObservationBatchPersistenceService.deleteAllIndividualObservationsForBatch(batchId);

        Set<Integer> ids = getBatchIds(batchId);

        catchBatchDao.removeWithChildren(batchId);

        attachmentPersistenceService.deleteAllAttachment(ObjectTypeCode.BATCH, ids);

    }

    public void removeWithChildren(Integer batchId, CatchBatch parentCatchBatch) {

        individualObservationBatchPersistenceService.deleteAllIndividualObservationsForBatch(batchId);

        Set<Integer> ids = getBatchIds(batchId);

        catchBatchDao.removeWithChildren(batchId, parentCatchBatch);

        attachmentPersistenceService.deleteAllAttachment(ObjectTypeCode.BATCH, ids);
    }

    public void updateSortingBatch(List<SortingBatch> sortingBatchs, CatchBatch parentCatchBatch) {
        catchBatchDao.updateSortingBatch(sortingBatchs, parentCatchBatch);
    }

    public SortingBatch loadSortingBatch(Integer sortingBatchId, CatchBatch parentCatchBatch) {
        return catchBatchDao.loadSortingBatch(sortingBatchId, parentCatchBatch);
    }

    public void update(CatchBatch catchBatch) {
        catchBatchDao.update(catchBatch);
    }

    public SortingBatch getSortingBatchById(CatchBatch catchBatch, Integer sortingBatchId) {
        return catchBatchDao.getSortingBatchById(catchBatch, sortingBatchId);
    }

    public void updateSortingBatch(SortingBatch sortingBatch, CatchBatch parentCatchBatch) {
        // Synchronization status
        synchronizationStatusHelper.setDirty(parentCatchBatch);
        catchBatchDao.updateSortingBatch(sortingBatch, parentCatchBatch);
    }

//    public List<SortingBatch> getFrequencyChilds(SortingBatch sortingBatch) {
//        List<SortingBatch> result = Lists.newArrayList();
//
//        SampleCategoryModel sampleCategoryModel = getSampleCategoryModel();
//
//        for (Batch batch : sortingBatch.getChildBatchs()) {
//            SortingBatch child = (SortingBatch) batch;
//            if (isFrequencyBatch(sampleCategoryModel, child)) {
//                result.add(child);
//            }
//        }
//        return result;
//    }

//    public List<SortingBatch> getFrequencies(Integer batchId) {
//        Preconditions.checkNotNull(batchId);
//        CatchBatch catchBatch = getRootCatchBatchByBatchId(batchId);
//        SortingBatch sortingBatch = catchBatchDao.getSortingBatchById(
//                catchBatch, batchId);
//
//        return getFrequencyChilds(sortingBatch);
//    }

    public fr.ifremer.adagio.core.dao.data.batch.CatchBatch getRootCatchBatchByFishingOperationId(Integer fishingOperationId, boolean validate) {
        Preconditions.checkNotNull(fishingOperationId);

        Integer catchBatchId = catchBatchDao.getIdByFishingOperationId(fishingOperationId);
        Preconditions.checkNotNull(catchBatchId);

        // whenever want to repair anything from Tutti
        fr.ifremer.adagio.core.dao.data.batch.CatchBatch result;

        if (validate) {

            try {
                result = catchBatchDao.loadFullTree(catchBatchId, PmfmId.WEIGHT_MEASURED.getValue(), true, false);
            } catch (CatchBatchValidationException e) {
                throw new InvalidBatchModelException(
                        "L'arbre d'échantillonage n'est pas compatible avec celui de Tutti.", e);
            }
        } else {
            result = catchBatchDao.loadFullTree(catchBatchId, PmfmId.WEIGHT_MEASURED.getValue());
        }
        Preconditions.checkNotNull(result);
        return result;
    }

    public boolean isCatchBatchExistsForFishingOperation(Integer fishingOperationId) {
        return catchBatchDao.isCatchBatchExistsForFishingOperation(fishingOperationId);
    }

    public Integer getCatchBatchIdByFishingOperationId(Integer fishingOperationId) throws DataRetrievalFailureException {
        return catchBatchDao.getIdByFishingOperationId(fishingOperationId);
    }

    public fr.ifremer.adagio.core.dao.data.batch.CatchBatch getRootCatchBatchByBatchId(Integer batchId) {
        Preconditions.checkNotNull(batchId);

        Integer catchBatchId = catchBatchDao.getIdBySortingBatchId(batchId);
        Preconditions.checkNotNull(catchBatchId);

        // whenever want to repair anything from Tutti
        fr.ifremer.adagio.core.dao.data.batch.CatchBatch result;

        result = catchBatchDao.loadFullTree(catchBatchId, PmfmId.WEIGHT_MEASURED.getValue());

        Preconditions.checkNotNull(result);
        return result;
    }

    public void setSortingBatchReferenceTaxon(Integer batchId, Species species) {
        catchBatchDao.setSortingBatchReferenceTaxon(String.valueOf(batchId), species.getReferenceTaxonId());
    }

//    public void setSpeciesBatchParents(Integer sampleCategoryId,
//                                       Serializable sampleCategoryValue,
//                                       SortingBatch target,
//                                       Integer parentBatchId,
//                                       CatchBatch catchBatch) {
//
//        Preconditions.checkNotNull(target);
//        Preconditions.checkNotNull(catchBatch);
//
//        target.setRootBatch(catchBatch);
//
//        SortingBatch parentBatch;
//        if (parentBatchId != null) {
//
//            // Load existing parent and root
//            parentBatch = catchBatchDao.getSortingBatchById(catchBatch, parentBatchId);
//        } else {
//
//            // Or retrieve parent batch, from pmfm id
//            // Retrieve category type
//            if (!sampleCategoryId.equals(PmfmId.SORTED_UNSORTED.getValue())) {
//                throw new DataIntegrityViolationException(MessageFormat.format(
//                        "A species or benthos batch with no parent should have a sampleCategoryType {0} (PMFM.ID={1})",
//                        PmfmId.SORTED_UNSORTED.getValue(),
//                        sampleCategoryId));
//            }
//
//            Integer qualitativeValueId = convertSampleCategoryValueIntoQualitativeId(sampleCategoryValue);
//
//            if (QualitativeValueId.SORTED_VRAC.getValue().equals(qualitativeValueId)) {
//
//                // -- Vrac > Species > Alive itemized
//                parentBatch = batchTreeHelper.getSpeciesVracAliveItemizedRootBatch(catchBatch);
//
//                if (parentBatch == null) {
//
//                    // -- Vrac
//                    SortingBatch vracBatch = batchTreeHelper.getVracBatch(catchBatch);
//
//                    if (vracBatch == null) {
//                        vracBatch = batchTreeHelper.getOrCreateVracBatch(catchBatch, null, null);
//                    }
//
//                    // -- Vrac > Species
//                    SortingBatch vracSpeciesBatch = batchTreeHelper.getSpeciesVracRootBatch(vracBatch);
//
//                    if (vracSpeciesBatch == null) {
//                        vracSpeciesBatch = batchTreeHelper.getOrCreateSpeciesVracRootBatch(catchBatch, vracBatch, null);
//                    }
//
//                    // -- Vrac > Species > Alive itemized
//                    parentBatch = batchTreeHelper.getOrCreateSpeciesVracAliveItemizedRootBatch(catchBatch, vracSpeciesBatch);
//
//                }
//            } else if (QualitativeValueId.SORTED_HORS_VRAC.getValue().equals(qualitativeValueId)) {
//
//                // -- Hors Vrac > Species
//                parentBatch = batchTreeHelper.getSpeciesHorsVracRootBatch(catchBatch);
//
//                if (parentBatch == null) {
//
//                    // -- Hors Vrac
//                    SortingBatch horsVracBatch = batchTreeHelper.getOrCreateHorsVracBatch(catchBatch);
//
//                    // -- Hors Vrac > Species
//                    parentBatch = batchTreeHelper.getOrCreateSpeciesHorsVracRootBatch(catchBatch, horsVracBatch);
//                }
//            } else {
//
//                // not possible
//                throw new DataIntegrityViolationException("Should have Vrac / Hors Vrac qualitative value, but had: " + qualitativeValueId);
//            }
//
//        }
//
//        Preconditions.checkNotNull(parentBatch);
//        target.setParentBatch(parentBatch);
//    }

//    public void setBenthosBatchParents(Integer sampleCategoryType,
//                                       Serializable sampleCategoryValue,
//                                       SortingBatch target,
//                                       Integer parentBatchId,
//                                       CatchBatch catchBatch) {
//
//        Preconditions.checkNotNull(target);
//        Preconditions.checkNotNull(catchBatch);
//
//        target.setRootBatch(catchBatch);
//
//        SortingBatch parentBatch;
//        if (parentBatchId != null) {
//
//            // Load existing parent and root
//            parentBatch = catchBatchDao.getSortingBatchById(catchBatch, parentBatchId);
//        } else {
//
//            // Or retrieve parent batch, from pmfm id
//            // Retrieve category type
//            if (!sampleCategoryType.equals(PmfmId.SORTED_UNSORTED.getValue())) {
//                throw new DataIntegrityViolationException(MessageFormat.format(
//                        "A species or benthos batch with no parent should have a sampleCategoryType {0} (PMFM.ID={1})",
//                        sampleCategoryType,
//                        PmfmId.SORTED_UNSORTED.getValue()));
//            }
//
//            Integer qualitativeValueId = convertSampleCategoryValueIntoQualitativeId(sampleCategoryValue);
//
//            if (QualitativeValueId.SORTED_VRAC.getValue().equals(qualitativeValueId)) {
//
//                // -- Vrac > Benthos > Alive Itemized
//                parentBatch = batchTreeHelper.getBenthosVracAliveItemizedRootBatch(catchBatch);
//
//                if (parentBatch == null) {
//
//                    // -- Vrac
//                    SortingBatch vracBatch = batchTreeHelper.getVracBatch(catchBatch);
//
//                    if (vracBatch == null) {
//                        vracBatch = batchTreeHelper.getOrCreateVracBatch(catchBatch, null, null);
//                    }
//
//                    // -- Vrac > Benthos
//                    SortingBatch vracBenthosBatch = batchTreeHelper.getBenthosVracRootBatch(vracBatch);
//
//                    if (vracBenthosBatch == null) {
//                        vracBenthosBatch = batchTreeHelper.getOrCreateBenthosVracRootBatch(catchBatch, vracBatch, null);
//                    }
//
//                    // -- Vrac > Benthos > Alive itemized
//                    parentBatch = batchTreeHelper.getOrCreateBenthosVracAliveItemizedRootBatch(catchBatch, vracBenthosBatch);
//
//                }
//            } else if (QualitativeValueId.SORTED_HORS_VRAC.getValue().equals(qualitativeValueId)) {
//
//                // -- Hors Vrac > Benthos
//                parentBatch = batchTreeHelper.getBenthosHorsVracRootBatch(catchBatch);
//
//                if (parentBatch == null) {
//
//                    // -- Hors Vrac
//                    SortingBatch horsVracBatch = batchTreeHelper.getOrCreateHorsVracBatch(catchBatch);
//
//                    // -- Hors Vrac > Benthos
//                    parentBatch = batchTreeHelper.getOrCreateBenthosHorsVracRootBatch(catchBatch, horsVracBatch);
//                }
//            } else {
//
//                // not possible
//                throw new DataIntegrityViolationException("Should have Vrac / Hors Vrac qualitative value, but had: " + qualitativeValueId);
//            }
//        }
//
//        Preconditions.checkNotNull(parentBatch);
//        target.setParentBatch(parentBatch);
//    }

//    public void setMarineLitterBatchParents(SortingBatch target, CatchBatch catchBatch) {
//
//        Preconditions.checkNotNull(target);
//
//        // -- Hors Vrac > Marine Litter
//        SortingBatch parentBatch = batchTreeHelper.getMarineLitterRootBatch(catchBatch);
//
//        if (parentBatch == null) {
//
//            // -- Hors Vrac
//            SortingBatch horsVracBatch = batchTreeHelper.getOrCreateHorsVracBatch(catchBatch);
//
//            // -- Hors Vrac > Marine Litter
//            parentBatch = batchTreeHelper.getOrCreateMarineLitterRootBatch(catchBatch, horsVracBatch, null);
//        }
//
//        target.setParentBatch(parentBatch);
//        target.setRootBatch(catchBatch);
//    }

//    public void beanToEntity(Integer parentBatchId,
//                             SpeciesBatch source,
//                             SortingBatch target,
//                             boolean computeRankOrder) {
//
//        // --- RankOrder (initialize once, at creation) --- //
//        {
//            if (target.getRankOrder() == null) {
//
//                short rankOrder;
//
//                if (computeRankOrder) {
//
//                    // Start rank order at 1
//                    rankOrder = (short) 1;
//                    //FIXME : tchemit-2015-04-04 This code can not be used to save multiple batches at the same time, since it will always give the
//                    //FIXME : tchemit-2015-04-04 same values for all batches
//                    if (source.getParentBatch() != null && CollectionUtils.isNotEmpty(source.getParentBatch().getChildBatchs())) {
//                        int maxRankOrder = 0;
//                        for (SpeciesBatch batch : source.getParentBatch().getChildBatchs()) {
//                            Integer r = batch.getRankOrder();
//                            if (r != null && r > maxRankOrder) {
//                                maxRankOrder = r;
//                            }
//                        }
//                        rankOrder += (short) maxRankOrder;
//
//                    } else {
//
//                        rankOrder = computeRankOrder(target);
//
//                    }
//
//                } else {
//
//                    Preconditions.checkState(source.getRankOrder() != null, "Not using computeRankOrder requires source rankOrder to be not null, but was on batch: " + source);
//                    rankOrder = (short) (int) source.getRankOrder();
//
//                }
//
//                target.setRankOrder(rankOrder);
//
//            }
//        }
//
//        // --- Force subgroup count to '1', as Allegro --- //
//        target.setSubgroupCount(1f);
//
//        // --- Individual count --- //
//        target.setIndividualCount(source.getNumber());
//
//        // --- Comments --- //
//        target.setComments(source.getComment());
//
//        // --- Exhaustive inventory (always true under a species batch) --- //
//        target.setExhaustiveInventory(true);
//
//        // --- Species --- //
//        {
//            ReferenceTaxon referenceTaxon;
//            if (source.getSpecies() == null || parentBatchId != null) {
//                referenceTaxon = null;
//            } else {
//                referenceTaxon = load(ReferenceTaxonImpl.class, source.getSpecies().getReferenceTaxonId());
//            }
//            target.setReferenceTaxon(referenceTaxon);
//        }
//
//        // --- QualityFlag --- //
//        {
//            String qualityFlag;
//            if (source.isSpeciesToConfirm()) {
//                qualityFlag = QualityFlagCode.DOUBTFUL.getValue();
//            } else {
//                qualityFlag = QualityFlagCode.NOTQUALIFIED.getValue();
//            }
//            target.setQualityFlag(load(QualityFlagImpl.class, qualityFlag));
//        }
//
//        Float weight = source.getWeight();
//        Float sampleCategoryWeight = source.getSampleCategoryWeight();
//
//        // --- Sampling Ratio + QuantificationMeasurement --- //
//        batchTreeHelper.setWeightAndSampleRatio(target, weight, sampleCategoryWeight);
//
//        // --- Sorting measurement --- //
//        {
//            Collection<SortingMeasurement> sortingMeasurements = target.getSortingMeasurements();
//            Set<SortingMeasurement> notChangedSortingMeasurements = Sets.newHashSet();
//            if (sortingMeasurements != null) {
//                notChangedSortingMeasurements.addAll(sortingMeasurements);
//            }
//
//            if (source.getSampleCategoryId() != null && source.getSampleCategoryValue() != null) {
//                Integer pmfmId = source.getSampleCategoryId();
//                // Do not store sorting measurement if pmfm = SORTED (already store in an ancestor batch)
//                if (!pmfmId.equals(PmfmId.SORTED_UNSORTED.getValue())) {
//                    SortingMeasurement sortingMeasurement = measurementPersistenceHelper.setSortingMeasurement(
//                            target,
//                            pmfmId,
//                            source.getSampleCategoryValue());
//                    notChangedSortingMeasurements.remove(sortingMeasurement);
//                }
//            }
//            if (sortingMeasurements != null) {
//                sortingMeasurements.removeAll(notChangedSortingMeasurements);
//            }
//        }
//
//    }

//    public void beanToEntity(SpeciesBatchFrequency source,
//                             SortingBatch target,
//                             SortingBatch parentBatch,
//                             short rankOrder) {
//        Preconditions.checkNotNull(source.getBatch());
//        Preconditions.checkNotNull(source.getBatch().getId());
//
//        // If parent and root need to be set
//        if (target.getId() == null
//                || target.getRootBatch() == null
//                || (target.getParentBatch() != null && !target.getParentBatch().getId().equals(parentBatch.getId()))) {
//
//            target.setParentBatch(parentBatch);
//            target.setRootBatch(parentBatch.getRootBatch());
//        }
//
//        // --- RankOrder --- //
//        target.setRankOrder(rankOrder);
//
//        // --- Individual count --- //
//        target.setIndividualCount(source.getNumber());
//
//        // --- Species --- //
//        target.setReferenceTaxon(null);
//
//        // --- QualityFlag --- //
//        target.setQualityFlag(parentBatch.getQualityFlag());
//
//        // --- Exhaustive inventory (always true under a species batch) --- //
//        target.setExhaustiveInventory(true);
//
//        // --- Sampling Ratio + QuantificationMeasurement --- //
//        batchTreeHelper.setWeightAndSampleRatio(target, source.getWeight(), null);
//
//        // --- Sorting measurement --- //
//        {
//            Collection<SortingMeasurement> sortingMeasurements = target.getSortingMeasurements();
//            Set<SortingMeasurement> notChangedSortingMeasurements = Sets.newHashSet();
//            if (sortingMeasurements != null) {
//                notChangedSortingMeasurements.addAll(sortingMeasurements);
//            }
//            if ((source.getLengthStepCaracteristic() != null && source.getLengthStep() != null)) {
//                Integer pmfmId = source.getLengthStepCaracteristic().getIdAsInt();
//                SortingMeasurement sortingMeasurement = measurementPersistenceHelper.setSortingMeasurement(target, pmfmId,
//                                                                                                           source.getLengthStep());
//                notChangedSortingMeasurements.remove(sortingMeasurement);
//            }
//            if (sortingMeasurements != null) {
//                sortingMeasurements.removeAll(notChangedSortingMeasurements);
//            }
//        }
//
//    }

//    public SpeciesBatch entityToBean(SampleCategoryModel sampleCategoryModel,
//                                     SortingBatch source,
//                                     SpeciesBatch target) {
//
//        Preconditions.checkNotNull(target.getSpecies());
//
//        target.setId(source.getId().toString());
//
//        // Rank order
//        target.setRankOrder(Integer.valueOf(source.getRankOrder()));
//
//        // Individual count
//        target.setNumber(source.getIndividualCount());
//
//        // Convert database weight (and sampling ratio) into UI weight and sampleCategoryWeight
//        if (source.getWeight() != null && source.getWeightBeforeSampling() == null) {
//            target.setSampleCategoryWeight(source.getWeight());
//        } else {
//            target.setWeight(source.getWeight());
//            target.setSampleCategoryWeight(source.getWeightBeforeSampling());
//
////            if (Objects.equals(source.getWeight(), source.getWeightBeforeSampling())) {
////
////                // after a allegro synchronize, can happen, we do not use quantification measurement on a not leaf node
////                // the weight comes from sampleRatioText, but in facts there only one weight...
////                target.setWeight(null);
////
////            }
//        }
//
////        if (CollectionUtils.isNotEmpty(source.getChildBatchs()) && target.getWeight() != null) {
////
////            // can't use this sample weight on a node
////            // the weight comes from sampleRatioText, but must NOT be used here
////            target.setWeight(null);
////
////        }
//
//
//        // Comments
//        target.setComment(source.getComments());
//
//        // Sample category type (only one is applied)
//        SortingMeasurement sm = null;
//        if (source.getSortingMeasurements().size() == 1) {
//            sm = source.getSortingMeasurements().iterator().next();
//        } else if (source.getReferenceTaxon() != null && source.getReferenceTaxon().getId() != null) {
//            sm = measurementPersistenceHelper.getInheritedSortingMeasurement(source);
//        }
//        if (sm != null) {
//
//            boolean isFrequency = isFrequencyBatch(sampleCategoryModel, source);
//
//            if (!isFrequency) {
//                Integer qualitativeId = null;
//                if (sm.getQualitativeValue() != null) {
//                    qualitativeId = sm.getQualitativeValue().getId();
//                }
//                setSampleCategoryQualitative(
//                        target,
//                        sm.getPmfm().getId(),
//                        sm.getNumericalValue(),
//                        sm.getAlphanumericalValue(),
//                        qualitativeId);
//            }
//        }
//
//        if (target.getSampleCategoryId() != null) {
//            List<SpeciesBatch> targetChilds = Lists.newArrayList();
//            for (Batch batch : source.getChildBatchs()) {
//                SortingBatch sourceChild = (SortingBatch) batch;
//                SpeciesBatch targetChild = SpeciesBatchs.newInstance(target);
//                targetChild.setSpecies(target.getSpecies());
//                entityToBean(sampleCategoryModel, sourceChild, targetChild);
//                if (log.isDebugEnabled()) {
//                    log.debug("Loaded CatchBatch (Vrac|Hors Vrac) > Species > " + targetChild.getSpecies().getReferenceTaxonId() + " : " + target.getId());
//                }
//                if (targetChild.getSampleCategoryValue() != null) {
//                    targetChilds.add(targetChild);
//                    targetChild.setParentBatch(target);
//                }
//            }
//            target.setChildBatchs(targetChilds);
//
//        }
//
//        //FIXME tchemit-2014-08-29 We can only do this if not an a leaf node (means with no frequencies...)
//        //FIXME tchemit-2014-08-29 But need to see if this is really need to do that .
//        // see https://forge.codelutin.com/issues/5698
//
//        if (CollectionUtils.isNotEmpty(source.getChildBatchs()) && target.getWeight() != null) {
//
//            SortingBatch childBatch = (SortingBatch) Iterables.get(source.getChildBatchs(), 0);
//
//            boolean isFrequency = isFrequencyBatch(sampleCategoryModel, childBatch);
//
//            if (!isFrequency) {
//
//                // can't use this sample weight on a node
//                // the weight comes from sampleRatioText, but must NOT be used here
//                // but we can only do this if childs are not frequencies
//
//                target.setWeight(null);
//
//            }
//
//        }
//
//        QualityFlag qualityFlag = source.getQualityFlag();
//        target.setSpeciesToConfirm(qualityFlag != null && QualityFlagCode.DOUBTFUL.getValue().equals(qualityFlag.getCode()));
//
//        return target;
//
//    }

    public short computeRankOrder(SortingBatch target) {

        // Start rank order at 1, nothing before it
        short rankOrder = (short) 1;
        if (target.getParentBatch() != null && target.getParentBatch().getChildBatchs() != null) {
            int maxRankOrder = 0;
            for (Batch batch : target.getParentBatch().getChildBatchs()) {
                Short r = batch.getRankOrder();
                if (r != null && r > maxRankOrder) {
                    maxRankOrder = r;
                }
            }
            rankOrder += maxRankOrder;
        }
        return rankOrder;

    }

//    public Integer convertSampleCategoryValueIntoQualitativeId(Serializable value) {
//        if (value == null) {
//            return null;
//        }
//        Integer qualitativeValueId = null;
//        if (value instanceof CaracteristicQualitativeValue) {
//            CaracteristicQualitativeValue cqValue = (CaracteristicQualitativeValue) value;
//            qualitativeValueId = cqValue.getIdAsInt();
//        } else if (value instanceof String) {
//            qualitativeValueId = Integer.valueOf((String) value);
//        }
//        return qualitativeValueId;
//    }

    public void deleteBatch(Integer batchId) {
        Preconditions.checkNotNull(batchId);

        CatchBatch catchBatch = getRootCatchBatchByBatchId(batchId);
        synchronizationStatusHelper.setDirty(catchBatch);

        removeWithChildren(batchId);
    }

//    public void deleteSpeciesSubBatch(Integer speciesBatchId) {
//        Preconditions.checkNotNull(speciesBatchId);
//
//        CatchBatch catchBatch = getRootCatchBatchByBatchId(speciesBatchId);
//        synchronizationStatusHelper.setDirty(catchBatch);
//
//        SortingBatch sortingBatch = getSortingBatchById(catchBatch, speciesBatchId);
//
//        // get his children
//        Collection<Batch> childBatchs = sortingBatch.getChildBatchs();
//
//        if (CollectionUtils.isNotEmpty(childBatchs)) {
//
//            for (Batch childBatch : childBatchs) {
//
//                // delete this child and all his children
//                Integer childBatchId = childBatch.getId();
//
//                if (log.isDebugEnabled()) {
//                    log.debug("Delete child [" + childBatchId + "] of species batch: " + speciesBatchId);
//                }
//                removeWithChildren(childBatchId);
//            }
//        }
//    }

//    public void changeBatchSpecies(Integer batchId, Species species) {
//
//        Preconditions.checkNotNull(batchId);
//        Preconditions.checkNotNull(species);
//        Preconditions.checkNotNull(species.getReferenceTaxonId());
//
//        CatchBatch catchBatch = getRootCatchBatchByBatchId(batchId);
//        synchronizationStatusHelper.setDirty(catchBatch);
//
//        catchBatchDao.setSortingBatchReferenceTaxon(String.valueOf(batchId), species.getReferenceTaxonId());
//    }

//    public void setSampleCategoryQualitative(SpeciesBatch target,
//                                             Integer pmfmId,
//                                             Float numericalvalue,
//                                             String alphanumericalValue,
//                                             Integer qualitativeValueId) {
//        // skip if null or corresponding to the SORTING_TYPE PMFM (Espèce, Benthos, Plancton, etc.)
//        if (pmfmId == null || pmfmId.equals(BatchTreeHelper.SORTING_TYPE_ID)) {
//            return;
//        }
//        SampleCategoryModel sampleCategoryModel = getSampleCategoryModel();
//
//        boolean isSamplingCategory = sampleCategoryModel.containsCategoryId(pmfmId);
//        Preconditions.checkNotNull(isSamplingCategory, "Unable to find corresponding SampleCategoryEnum for PMFM.ID : " + pmfmId);
//
//        target.setSampleCategoryId(pmfmId);
//        Serializable categoryValue = getSampleCategoryQualitative(
//                pmfmId,
//                numericalvalue,
//                alphanumericalValue,
//                qualitativeValueId);
//        target.setSampleCategoryValue(categoryValue);
//    }

//    public void entityToBatchFrequency(SortingBatch source,
//                                       SpeciesBatchFrequency target) {
//
//        target.setId(source.getId());
//
//        // Rank order
//        target.setRankOrder(Integer.valueOf(source.getRankOrder()));
//
//        target.setNumber(source.getIndividualCount());
//        target.setWeight(source.getWeight());
//
//        Preconditions.checkState(source.getSortingMeasurements().size() == 1, "SortingBatch [" + source.getId() + "] need exactly one sortingMeasurement (to store the length step category), but had " + source.getSortingMeasurements().size());
//        SortingMeasurement sm = source.getSortingMeasurements().iterator().next();
//        Preconditions.checkNotNull(sm.getPmfm(), "SortingMeasurement [" + sm.getId() + "] can not have a null pmfm");
//        Preconditions.checkNotNull(sm.getPmfm().getId(), "SortingMeasurement [" + sm.getId() + "] can not have a pmfm with null id");
//
//        // Length step category
//        Caracteristic lengthStepCaracteristic = caracteristicService.getCaracteristic(sm.getPmfm().getId());
//        target.setLengthStepCaracteristic(lengthStepCaracteristic);
//
//        // Length
//        target.setLengthStep(sm.getNumericalValue());
//    }

//    public Serializable getSampleCategoryQualitative(Integer pmfmId,
//                                                     Float numericalvalue,
//                                                     String alphanumericalValue,
//                                                     Integer qualitativeValueId) {
//
//        if (numericalvalue != null) {
//            return numericalvalue;
//        }
//        if (alphanumericalValue != null) {
//            return alphanumericalValue;
//        }
//
//        Caracteristic caracteristic = caracteristicService.getCaracteristic(pmfmId);
//        if (caracteristic == null || caracteristic.getCaracteristicType() != CaracteristicType.QUALITATIVE) {
//            return null;
//        }
//        CaracteristicQualitativeValue value = null;
//        for (CaracteristicQualitativeValue qv : caracteristic.getQualitativeValue()) {
//            if (qualitativeValueId.equals(qv.getIdAsInt())) {
//                value = qv;
//                break;
//            }
//        }
//
//        return value;
//    }

//    /**
//     * Check if the given {@code sortingBatch} is a frequency one.
//     *
//     * We test that:
//     * <ul>
//     * <li>batch has exactly one measurement</li>
//     * <li>the measurement pmfm is not a sample category</li>
//     * </ul>
//     *
//     * @param sampleCategoryModel model of authorized sample categories
//     * @param sortingBatch        batch to check
//     * @return {@code true} if given batch is a frequency batch,
//     * {@code false} otherwise.
//     */
//    public boolean isFrequencyBatch(SampleCategoryModel sampleCategoryModel,
//                                    SortingBatch sortingBatch) {
//        boolean result = false;
//        if (sortingBatch.getSortingMeasurements().size() == 1) {
//            SortingMeasurement sm
//                    = sortingBatch.getSortingMeasurements().iterator().next();
//            Pmfm pmfm = sm.getPmfm();
//
//            result = sortingBatch.getIndividualCount() != null &&
//                    !sampleCategoryModel.containsCategoryId(pmfm.getId());
//        }
//        return result;
//    }

}
