package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.service.ServiceLocator;
import fr.ifremer.adagio.core.service.technical.sanity.DatabaseSanityService;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroService;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.ObjectTypePersistenceService;
import fr.ifremer.tutti.persistence.service.referential.PersonPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.VesselPersistenceService;

/**
 * To obtain services from spring context.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class TuttiPersistenceServiceLocator extends ServiceLocator {

    static {
        initTuttiDefault();
    }

    public static void initTuttiDefault() {
        instance().init("tuttiBeanRefFactory.xml", "TuttiBeanRefFactory");
    }

    public static void initTutti(String beanFactoryReferenceLocation,
                                 String beanRefFactoryReferenceId) {
        instance().init(beanFactoryReferenceLocation, beanRefFactoryReferenceId);
    }

    public static TuttiPersistence getPersistenceService() {
        return getPersistenceService("tuttiPersistence",
                                     TuttiPersistence.class);
    }

    public static TechnicalPersistenceService getTechnicalPersistenceService() {
        return instance().getService("technicalPersistenceService",
                                     TechnicalPersistenceService.class);
    }

    public static ProgramPersistenceService getProgramPersistenceService() {
        return getPersistenceService("programPersistenceService",
                                     ProgramPersistenceService.class);
    }

    public static CruisePersistenceService getCruisePersistenceService() {
        return getPersistenceService("cruisePersistenceService",
                                     CruisePersistenceService.class);
    }

    public static FishingOperationPersistenceService getFishingOperationPersistenceService() {
        return getPersistenceService("fishingOperationPersistenceService",
                                     FishingOperationPersistenceService.class);
    }

    public static CatchBatchPersistenceService getCatchBatchPersistenceService() {
        return getPersistenceService("batchPersistenceService",
                                     CatchBatchPersistenceService.class);
    }

    public static SpeciesBatchPersistenceService getSpeciesBatchPersistenceService() {
        return getPersistenceService("speciesBatchPersistenceService",
                                     SpeciesBatchPersistenceService.class);
    }

    public static BenthosBatchPersistenceService getBenthosBatchPersistenceService() {
        return getPersistenceService("benthosBatchPersistenceService",
                                     BenthosBatchPersistenceService.class);
    }

    public static MarineLitterBatchPersistenceService getMarineLitterBatchPersistenceService() {
        return getPersistenceService("marineLitterBatchPersistenceService",
                                     MarineLitterBatchPersistenceService.class);
    }

    public static AccidentalBatchPersistenceService getAccidentalBatchPersistenceService() {
        return getPersistenceService("accidentalBatchPersistenceService",
                                     AccidentalBatchPersistenceService.class);
    }

    public static IndividualObservationBatchPersistenceService getIndividualObservationBatchPersistenceService() {
        return getPersistenceService("individualObservationBatchPersistenceService",
                                     IndividualObservationBatchPersistenceService.class);
    }

    public static ProtocolPersistenceService getProtocolPersistenceService() {
        return getPersistenceService("protocolPersistenceService",
                                     ProtocolPersistenceService.class);
    }

    //TODO Move this to adagio
    public static ReferentialSynchroService getReferentialSynchroService() {
        return instance().getService(
                "referentialSynchroServiceTutti", ReferentialSynchroService.class);
    }

    //TODO Move this to adagio
    public static DatabaseSanityService getDatabaseSanityService() {
        return instance().getService(
                "databaseSanityService", DatabaseSanityService.class);
    }

    public static AttachmentPersistenceService getAttachmentPersistenceService() {
        return getPersistenceService("attachmentPersistenceService",
                                     AttachmentPersistenceService.class);
    }

    public static <S extends TuttiPersistenceServiceImplementor> S getPersistenceService(String name, Class<S> serviceType) {
        S service = instance().getService(name, serviceType);
        service.init();
        return service;
    }

    public static void shutdownTutti() {
        instance().shutdown();
    }

    public static CaracteristicPersistenceService getCaracteristicPersistenceService() {
        return getPersistenceService("caracteristicPersistenceService",
                                     CaracteristicPersistenceService.class);
    }

    public static GearPersistenceService getGearPersistenceService() {
        return getPersistenceService("gearPersistenceService",
                                     GearPersistenceService.class);
    }

    public static LocationPersistenceService getLocationPersistenceService() {
        return getPersistenceService("locationPersistenceService",
                                     LocationPersistenceService.class);
    }

    public static ObjectTypePersistenceService getObjectTypePersistenceService() {
        return getPersistenceService("objectTypePersistenceService",
                                     ObjectTypePersistenceService.class);
    }

    public static PersonPersistenceService getPersonPersistenceService() {
        return getPersistenceService("personPersistenceService",
                                     PersonPersistenceService.class);
    }

    public static SpeciesPersistenceService getSpeciesPersistenceService() {
        return getPersistenceService("speciesPersistenceService",
                                     SpeciesPersistenceService.class);
    }


    public static VesselPersistenceService getVesselPersistenceService() {
        return getPersistenceService("vesselPersistenceService",
                                     VesselPersistenceService.class);
    }
}
