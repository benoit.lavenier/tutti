package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * To persist {@link Attachment}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
@Transactional(readOnly = true)
public interface AttachmentPersistenceService extends TuttiPersistenceServiceImplementor {

    /**
     * Get all attachments for the given object {@code objectId}.
     *
     * @param objectType type of object.
     * @param objectId   id of the object
     * @return list of all attachments for the given {@code objectId}.
     * (see {@link ObjectTypeCode})
     */
    List<Attachment> getAllAttachments(ObjectTypeCode objectType,
                                       Integer objectId);

    /**
     * Get the file of the given {@code attachmentId}.
     *
     * @param attachmentId id of the attachment
     * @return the file for the given attachment
     */
    File getAttachmentFile(String attachmentId);

    /**
     * Creates the given attachment.
     *
     * @param attachment attachment to create
     * @param file       file to store in this attachment
     * @return the attachment with his id.
     */
    @Transactional(readOnly = false)
    Attachment createAttachment(Attachment attachment, File file);


    /**
     * Saves the given attachment.
     *
     * @param attachment attachment to create
     * @return the attachment with his id.
     */
    @Transactional(readOnly = false)
    Attachment saveAttachment(Attachment attachment);


    /**
     * Deletes the given attachment given his id.
     *
     * @param attachmentId id of the attachment to delete
     */
    @Transactional(readOnly = false)
    void deleteAttachment(String attachmentId);

    /**
     * Deletes all attachments of the given object id.
     *  @param objectType type of attachment
     * @param objectId  id of object
     */
    @Transactional(readOnly = false)
    void deleteAllAttachment(ObjectTypeCode objectType, Integer objectId);

    /**
     * Deletes all attachments of the given object ids.
     *  @param objectType type of attachment
     * @param objectIds  ids of object
     */
    @Transactional(readOnly = false)
    void deleteAllAttachment(ObjectTypeCode objectType, Set<Integer> objectIds);

}
