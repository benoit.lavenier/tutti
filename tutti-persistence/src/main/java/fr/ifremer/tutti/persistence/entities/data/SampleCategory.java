package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * Represents a sample category value in the species / benthos batch.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class SampleCategory<C extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CATEGORY_WEIGHT = "categoryWeight";

    public static final String PROPERTY_CATEGORY_VALUE = "categoryValue";

    /**
     * Sample category definition.
     *
     * @since 2.4
     */
    protected SampleCategoryModelEntry categoryDef;

    /**
     * Sample category value.
     *
     * @since 0.3
     */
    protected C categoryValue;

    /**
     * Sample category weight.
     *
     * @since 0.3
     */
    protected Float categoryWeight;

    /**
     * Sample computed weight.
     *
     * @since 1.0
     */
    protected Float computedWeight;

    /**
     * Is this sample a subsample ?
     * Available only if the category is the finest category of the row
     *
     * @since 1.0
     */
    protected boolean subSample;

    /**
     * Has the row only one frequency ?
     * Available only if the category is the finest category of the row
     *
     * @since 1.0
     */
    protected boolean onlyOneFrequency;

    public static <C extends Serializable> SampleCategory<C> newSample(SampleCategoryModelEntry categoryDef) {
        SampleCategory<C> result = new SampleCategory<>();
        result.setCategoryDef(categoryDef);
        return result;
    }

    protected SampleCategory() {
    }

    public SampleCategoryModelEntry getCategoryDef() {
        return categoryDef;
    }

    public void setCategoryDef(SampleCategoryModelEntry categoryDef) {
        this.categoryDef = categoryDef;
    }

    public Integer getCategoryId() {
        return categoryDef.getCategoryId();
    }

    public C getCategoryValue() {
        return categoryValue;
    }

    public void setCategoryValue(C categoryValue) {
        this.categoryValue = categoryValue;
    }

    public Float getCategoryWeight() {
        return categoryWeight;
    }

    public void setCategoryWeight(Float categoryWeight) {
        this.categoryWeight = categoryWeight;
    }

    public Float getComputedWeight() {
        return computedWeight;
    }

    public void setComputedWeight(Float computedWeight) {
        this.computedWeight = computedWeight;
    }

    public boolean isSubSample() {
        return subSample;
    }

    public void setSubSample(boolean subSample) {
        this.subSample = subSample;
    }

    public boolean hasOnlyOneFrequency() {
        return onlyOneFrequency;
    }

    public void setOnlyOneFrequency(boolean onlyOneFrequency) {
        this.onlyOneFrequency = onlyOneFrequency;
    }

    public boolean isValid() {
        return categoryValue != null;
    }

    public boolean isEmpty() {
        return categoryValue == null
               && categoryWeight == null
               && computedWeight == null;
    }

    public boolean isEmptyOrValid() {
        return isEmpty() || isValid();
    }

    public Float getNotNullWeight() {
        Float result = categoryWeight;
        if (result == null) {
            result = computedWeight;
        }
        return result;
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this).
                appendSuper(super.toString()).
                toString();
    }
}
