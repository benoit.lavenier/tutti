package fr.ifremer.tutti.persistence;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.ObjectType;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.persistence.service.UpdateSchemaContextSupport;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Mock implementation With no persistence at all behind.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TuttiPersistenceNoDbImpl implements TuttiPersistence {

    @Override
    public String getImplementationName() {
        return "Mock persistence service implementation";
    }

    @Override
    public void setSkipShutdownDbWhenClosing() {
        throw notImplemented();
    }

    @Override
    public ProgramDataModel loadProgram(String programId, boolean loadFishingOperation) {
        throw notImplemented();
    }

    @Override
    public ProgramDataModel loadCruises(String programId, boolean loadFishingOperation, Integer... cruiseIds) {
        throw notImplemented();
    }

    @Override
    public ProgramDataModel loadCruise(String programId, Integer cruiseId, Integer... fishingOperationIds) {
        throw notImplemented();
    }

    @Override
    public <V> V invoke(Callable<V> call) {
        throw notImplemented();
    }

    @Override
    public void lazyInit() {
        throw notImplemented();
    }

    @Override
    public <U extends UpdateSchemaContextSupport> void prepareUpdateSchemaContext(U context) {
        throw notImplemented();
    }

    @Override
    public Version getSchemaVersion() {
        throw notImplemented();
    }

    @Override
    public Version getSchemaVersionIfUpdate() {
        throw notImplemented();
    }

    @Override
    public void updateSchema() {
        throw notImplemented();
    }

    @Override
    public void sanityDb() {
        throw notImplemented();
    }

    @Override
    public void clearAllCaches() {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllProgramZone() {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllCountry() {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllHarbour() {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllHarbourWithObsoletes() {
        throw notImplemented();
    }

    @Override
    public ImmutableSet<Integer> getAllFishingOperationStratasAndSubstratasIdsForProgram(String zoneId) {
        throw notImplemented();
    }

    @Override
    public Multimap<TuttiLocation, TuttiLocation> getAllFishingOperationStratasAndSubstratas(String zoneId) {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrata(String zoneId) {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrataWithObsoletes(String zoneId) {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrata(String zoneId, String strataId) {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrataWithObsoletes(String zoneId, String strataId) {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocation(String zoneId, String strataId, String subStrataId) {
        throw notImplemented();
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocationWithObsoletes(String zoneId, String strataId, String subStrataId) {
        throw notImplemented();
    }

    @Override
    public String getLocationLabelByLatLong(Float latitude, Float longitude) {
        throw notImplemented();
    }

    @Override
    public Integer getLocationIdByLatLong(Float latitude, Float longitude) {
        throw notImplemented();
    }

    @Override
    public List<Vessel> getAllScientificVessel() {
        throw notImplemented();
    }

    @Override
    public List<Vessel> getAllFishingVessel() {
        throw notImplemented();
    }

    @Override
    public List<Vessel> getAllVesselWithObsoletes() {
        throw notImplemented();
    }

    @Override
    public List<Species> getAllSpecies() {
        throw notImplemented();
    }

    @Override
    public List<Species> getAllReferentSpecies() {
        throw notImplemented();
    }

    @Override
    public List<Species> getAllReferentSpeciesWithObsoletes() {
        throw notImplemented();
    }

    @Override
    public Species getSpeciesByReferenceTaxonIdWithVernacularCode(Integer referenceTaxonId) {
        throw notImplemented();
    }

    @Override
    public List<Caracteristic> getAllCaracteristic() {
        throw notImplemented();
    }

    @Override
    public List<Caracteristic> getAllCaracteristicWithProtected() {
        throw notImplemented();
    }

    @Override
    public List<Caracteristic> getAllCaracteristicForSampleCategory() {
        throw notImplemented();
    }

    @Override
    public List<Caracteristic> getAllNumericCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getSizeCategoryCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getSexCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getSortedUnsortedCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getMaturityCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getAgeCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getMarineLitterCategoryCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getMarineLitterSizeCategoryCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getVerticalOpeningCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getHorizontalOpeningWingsCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getHorizontalOpeningDoorCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getDeadOrAliveCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getCalcifiedStructureCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getPmfmIdCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getWeightMeasuredCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getCopyIndividualObservationModeCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getSampleCodeCaracteristic() {
        throw notImplemented();
    }

    @Override
    public Caracteristic getCaracteristic(Integer pmfmId) {
        throw notImplemented();
    }

    @Override
    public boolean isVracBatch(SpeciesBatch speciesBatch) {
        throw notImplemented();
    }

    @Override
    public boolean isHorsVracBatch(SpeciesBatch speciesBatch) {
        throw notImplemented();
    }

    @Override
    public Predicate<SpeciesBatch> getVracBatchPredicate() {
        throw notImplemented();
    }

    @Override
    public boolean isTemporary(TuttiReferentialEntity entity) {
        throw notImplemented();
    }

    @Override
    public List<Gear> getAllScientificGear() {
        throw notImplemented();
    }

    @Override
    public List<Gear> getAllFishingGear() {
        throw notImplemented();
    }

    @Override
    public List<Gear> getAllGearWithObsoletes() {
        throw notImplemented();
    }

    @Override
    public List<Person> getAllPerson() {
        throw notImplemented();
    }

    @Override
    public List<Person> getAllPersonWithObsoletes() {
        throw notImplemented();
    }

    @Override
    public Person getPerson(Integer personId) {
        throw notImplemented();
    }

    @Override
    public Gear getGear(Integer gearCode) {
        throw notImplemented();
    }

    @Override
    public Vessel getVessel(String vesselCode) {
        throw notImplemented();
    }

    @Override
    public List<Species> addTemporarySpecies(List<Species> species) {
        throw notImplemented();
    }

    @Override
    public List<Species> updateTemporarySpecies(List<Species> species) {
        throw notImplemented();
    }

    @Override
    public List<Species> linkTemporarySpecies(List<Species> specieses) {
        throw notImplemented();
    }

    @Override
    public List<Vessel> addTemporaryVessels(List<Vessel> vessels) {
        throw notImplemented();
    }

    @Override
    public List<Vessel> updateTemporaryVessels(List<Vessel> vessels) {
        throw notImplemented();
    }

    @Override
    public List<Vessel> linkTemporaryVessels(List<Vessel> vessels) {
        throw notImplemented();
    }

    @Override
    public List<Person> addTemporaryPersons(List<Person> persons) {
        throw notImplemented();
    }

    @Override
    public List<Person> updateTemporaryPersons(List<Person> persons) {
        throw notImplemented();
    }

    @Override
    public List<Person> linkTemporaryPersons(List<Person> persons) {
        throw notImplemented();
    }

    @Override
    public List<Gear> addTemporaryGears(List<Gear> gears) {
        throw notImplemented();
    }

    @Override
    public List<Gear> updateTemporaryGears(List<Gear> gears) {
        throw notImplemented();
    }

    @Override
    public List<Gear> linkTemporaryGears(List<Gear> gears) {
        throw notImplemented();
    }

    @Override
    public List<ObjectType> getAllObjectType() {
        throw notImplemented();
    }

    @Override
    public ObjectType getObjectType(String objectTypeCode) {
        throw notImplemented();
    }

    @Override
    public Map<Integer, Integer> getAllObsoleteReferentTaxons() {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Program methods                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public List<Program> getAllProgram() {
        throw notImplemented();
    }

    @Override
    public Program getProgram(String id) {
        throw notImplemented();
    }

    @Override
    public Program createProgram(Program bean) {
        throw notImplemented();
    }

    @Override
    public Program saveProgram(Program bean) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Cruise methods                                                     --//
    //------------------------------------------------------------------------//


    @Override
    public List<Integer> getAllCruiseId(String programId) {
        throw notImplemented();
    }

    @Override
    public List<Cruise> getAllCruise(String programId) {
        throw notImplemented();
    }

    @Override
    public Cruise getCruise(Integer id) {
        throw notImplemented();
    }

    @Override
    public Cruise createCruise(Cruise bean) {
        throw notImplemented();
    }

    @Override
    public Cruise saveCruise(Cruise bean,
                             boolean updateVessel,
                             boolean updateGear) {
        throw notImplemented();
    }

    @Override
    public void setCruiseReadyToSynch(Integer cruiseId) {
        throw notImplemented();
    }

    @Override
    public CaracteristicMap getGearCaracteristics(Integer cruiseId, Integer gearId, short rankOrder) {
        throw notImplemented();
    }

    @Override
    public boolean isOperationUseGears(Integer cruiseId, Collection<Gear> gears) {
        throw notImplemented();
    }

    @Override
    public void saveGearCaracteristics(Gear gear, Cruise cruise) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Protocol methods                                                   --//
    //------------------------------------------------------------------------//

    @Override
    public TuttiProtocol getProtocol() {
        throw notImplemented();
    }

    @Override
    public TuttiProtocol getProtocolByName(String protocolName) {
        throw notImplemented();
    }

    @Override
    public void setProtocol(TuttiProtocol protocol) {
        throw notImplemented();
    }

    @Override
    public String getFirstAvailableName(String protocolName) {
        throw notImplemented();
    }

    @Override
    public List<String> getAllProtocolNames() {
        throw notImplemented();
    }

    @Override
    public List<TuttiProtocol> getAllProtocol() {
        throw notImplemented();
    }

    @Override
    public List<TuttiProtocol> getAllProtocol(String programId) {
        throw notImplemented();
    }

    @Override
    public boolean isProtocolExist(String id) {
        throw notImplemented();
    }

    @Override
    public TuttiProtocol getProtocol(String id) {
        throw notImplemented();
    }

    @Override
    public TuttiProtocol createProtocol(TuttiProtocol bean) {
        throw notImplemented();
    }

    @Override
    public TuttiProtocol saveProtocol(TuttiProtocol bean) {
        throw notImplemented();
    }

    @Override
    public void deleteProtocol(String id) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Fishing operation methods                                          --//
    //------------------------------------------------------------------------//

    @Override
    public int getFishingOperationCount(Integer cruiseId) {
        throw notImplemented();
    }

    @Override
    public List<Integer> getAllFishingOperationIds(Integer cruiseId) {
        throw notImplemented();
    }

    @Override
    public List<FishingOperation> getAllFishingOperation(Integer cruiseId) {
        throw notImplemented();
    }

    @Override
    public FishingOperation getFishingOperation(Integer id) {
        throw notImplemented();
    }

    @Override
    public List<Vessel> getFishingOperationSecondaryVessel(Integer fishingOperationId) {
        throw notImplemented();
    }

    @Override
    public FishingOperation createFishingOperation(FishingOperation bean) {
        throw notImplemented();
    }

    @Override
    public FishingOperation saveFishingOperation(FishingOperation bean) {
        throw notImplemented();
    }

    @Override
    public void deleteFishingOperation(Integer id) {
        throw notImplemented();
    }

    @Override
    public boolean isFishingOperationWithCatchBatch(Integer operationId) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Catch batch methods                                                --//
    //------------------------------------------------------------------------//

    @Override
    public CatchBatch getCatchBatchFromFishingOperation(Integer id) {
        throw notImplemented();
    }

    @Override
    public CatchBatch createCatchBatch(CatchBatch bean) {
        throw notImplemented();
    }

    @Override
    public CatchBatch saveCatchBatch(CatchBatch bean) {
        throw notImplemented();
    }

    @Override
    public void recomputeCatchBatchSampleRatios(Integer fishingOperationId) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Species batch methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<SpeciesBatch> getRootSpeciesBatch(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException {
        throw notImplemented();
    }

    @Override
    public Set<Integer> getBatchChildIds(Integer id) {
        throw notImplemented();
    }

    @Override
    public SpeciesBatch createSpeciesBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {
        throw notImplemented();
    }

    @Override
    public Collection<SpeciesBatch> createSpeciesBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {
        throw notImplemented();
    }

    @Override
    public SpeciesBatch saveSpeciesBatch(SpeciesBatch bean) {
        throw notImplemented();
    }

    @Override
    public void deleteSpeciesBatch(Integer id) {
        throw notImplemented();
    }

    @Override
    public void deleteSpeciesSubBatch(Integer id) {
        throw notImplemented();
    }

    @Override
    public void changeSpeciesBatchSpecies(Integer batchId, Species species) {
        throw notImplemented();
    }

    @Override
    public List<SpeciesBatch> getAllSpeciesBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        throw notImplemented();
    }

    @Override
    public List<SpeciesBatchFrequency> getAllSpeciesBatchFrequency(Integer speciesBatchId) {
        throw notImplemented();
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllSpeciesBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        throw notImplemented();
    }

    @Override
    public List<SpeciesBatchFrequency> saveSpeciesBatchFrequency(Integer speciesBatchId, List<SpeciesBatchFrequency> frequencies) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Benthos batch methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<SpeciesBatch> getRootBenthosBatch(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException {
        throw notImplemented();
    }

    @Override
    public SpeciesBatch createBenthosBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {
        throw notImplemented();
    }

    @Override
    public Collection<SpeciesBatch> createBenthosBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {
        throw notImplemented();
    }

    @Override
    public SpeciesBatch saveBenthosBatch(SpeciesBatch bean) {
        throw notImplemented();
    }

    @Override
    public void deleteBenthosBatch(Integer id) {
        throw notImplemented();
    }

    @Override
    public void deleteBenthosSubBatch(Integer id) {
        throw notImplemented();
    }

    @Override
    public void changeBenthosBatchSpecies(Integer batchId, Species species) {
        throw notImplemented();
    }

    @Override
    public List<SpeciesBatch> getAllBenthosBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        throw notImplemented();
    }

    @Override
    public List<SpeciesBatchFrequency> getAllBenthosBatchFrequency(Integer benthosBatchId) {
        throw notImplemented();
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllBenthosBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        throw notImplemented();
    }

    @Override
    public List<SpeciesBatchFrequency> saveBenthosBatchFrequency(Integer benthosBatchId, List<SpeciesBatchFrequency> frequencies) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Marine litter batch methods                                        --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<MarineLitterBatch> getRootMarineLitterBatch(Integer fishingOperationId) {
        throw notImplemented();
    }

    @Override
    public MarineLitterBatch createMarineLitterBatch(MarineLitterBatch bean) {
        throw notImplemented();
    }

    @Override
    public Collection<MarineLitterBatch> createMarineLitterBatches(Integer fishingOperationId, Collection<MarineLitterBatch> beans) {
        throw notImplemented();
    }

    @Override
    public MarineLitterBatch saveMarineLitterBatch(MarineLitterBatch bean) {
        throw notImplemented();
    }

    @Override
    public void deleteMarineLitterBatch(Integer id) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- Accidental Batch methods                                           --//
    //------------------------------------------------------------------------//

    @Override
    public List<AccidentalBatch> getAllAccidentalBatch(Integer fishingOperationId) {
        throw notImplemented();
    }

    @Override
    public AccidentalBatch createAccidentalBatch(AccidentalBatch bean) {
        throw notImplemented();
    }

    @Override
    public Collection<AccidentalBatch> createAccidentalBatches(Collection<AccidentalBatch> beans) {
        throw notImplemented();
    }

    @Override
    public AccidentalBatch saveAccidentalBatch(AccidentalBatch bean) {
        throw notImplemented();
    }

    @Override
    public void deleteAccidentalBatch(String id) {
        throw notImplemented();
    }

    @Override
    public List<Attachment> getAllAttachments(ObjectTypeCode objectType,
                                              Integer objectId) {
        throw notImplemented();
    }

    @Override
    public File getAttachmentFile(String attachmentId) {
        throw notImplemented();
    }

    @Override
    public Attachment createAttachment(Attachment attachment, File file) {
        throw notImplemented();
    }

    @Override
    public Attachment saveAttachment(Attachment attachment) {
        throw notImplemented();
    }

    @Override
    public void deleteAttachment(String attachmentId) {
        throw notImplemented();
    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Integer objectId) {
        throw notImplemented();
    }

    //------------------------------------------------------------------------//
    //-- IndividualObservation Batch methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForBatch(Integer batchId) {
        throw notImplemented();
    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForFishingOperation(Integer fishingOperationId) {
        throw notImplemented();
    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForCruise(Integer cruiseId) {
        throw notImplemented();
    }

    @Override
    public boolean isSamplingCodeAvailable(Integer cruiseId, Integer referenceTaxonId, String samplingCodeSuffix) {
        throw notImplemented();
    }

    @Override
    public List<IndividualObservationBatch> createIndividualObservationBatches(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {
        throw notImplemented();
    }

    @Override
    public List<IndividualObservationBatch> saveBatchIndividualObservation(Integer batchId,
                                                                           List<IndividualObservationBatch> individualObservation) {
        throw notImplemented();
    }

    @Override
    public void deleteAllIndividualObservationsForFishingOperation(Integer fishingOperationId) {
        throw notImplemented();
    }

    @Override
    public void deleteAllIndividualObservationsForBatch(Integer speciesBatchId) {
        throw notImplemented();
    }

    @Override
    public void init() {
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public void deleteAccidentalBatchForFishingOperation(Integer fishingOperationId) {
        throw notImplemented();
    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Set<Integer> objectIds) {
        throw notImplemented();
    }

    @Override
    public void deleteCatchBatch(Integer fishingOperationId) {
        throw notImplemented();
    }

    @Override
    public Collection<FishingOperation> saveFishingOperations(Collection<FishingOperation> beans) {
        throw notImplemented();
    }

    @Override
    public List<String> getAllProtocolId() {
        throw notImplemented();
    }

    @Override
    public TuttiLocation getLocation(String id) {
        throw notImplemented();
    }

    @Override
    public Species getSpeciesByReferenceTaxonId(Integer referenceTaxonId) {
        throw notImplemented();
    }

    @Override
    public void replaceGear(Gear source, Gear target, boolean delete) {
        throw notImplemented();
    }

    @Override
    public void replacePerson(Person source, Person target, boolean delete) {
        throw notImplemented();
    }

    @Override
    public void replaceSpecies(Species source, Species target, boolean delete) {
        throw notImplemented();
    }

    @Override
    public void replaceVessel(Vessel source, Vessel target, boolean delete) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporaryGear(Integer id) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporaryGears(Collection<Integer> id) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporarySpecies(Integer referenceTaxonId) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporarySpecies(Collection<Integer> referenceTaxonIds) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporaryPerson(Integer id) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporaryPersons(Collection<Integer> ids) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporaryVessel(String code) {
        throw notImplemented();
    }

    @Override
    public void deleteTemporaryVessels(Collection<String> codes) {
        throw notImplemented();
    }

    @Override
    public boolean isTemporaryPersonUsed(Integer id) {
        throw notImplemented();
    }

    @Override
    public boolean isTemporarySpeciesUsed(Integer referenceTaxonId) {
        throw notImplemented();
    }

    @Override
    public boolean isTemporaryGearUsed(Integer id) {
        throw notImplemented();
    }

    @Override
    public boolean isTemporaryVesselUsed(String code) {
        throw notImplemented();
    }

    protected RuntimeException notImplemented() {
        return new RuntimeException("method not implemented");
    }
}
