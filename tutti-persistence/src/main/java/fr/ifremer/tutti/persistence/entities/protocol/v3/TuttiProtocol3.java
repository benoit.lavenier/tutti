package fr.ifremer.tutti.persistence.entities.protocol.v3;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CommentAware;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;

import java.util.Collection;
import java.util.List;

public interface TuttiProtocol3 extends CommentAware, TuttiEntity {

    String PROPERTY_NAME = "name";

    String PROPERTY_COMMENT = "comment";

    String PROPERTY_LENGTH_CLASSES_PMFM_ID = "lengthClassesPmfmId";

    String PROPERTY_INDIVIDUAL_OBSERVATION_PMFM_ID = "individualObservationPmfmId";

    String PROPERTY_VERSION = "version";

    String PROPERTY_IMPORT_COLUMNS = "importColumns";

    String PROPERTY_PROGRAM_ID = "programId";

    String PROPERTY_USE_CALCIFIED_PIECE_SAMPLING = "useCalcifiedPieceSampling";

    String PROPERTY_SPECIES = "species";

    String PROPERTY_BENTHOS = "benthos";

    String PROPERTY_CARACTERISTIC_MAPPING = "caracteristicMapping";

    String PROPERTY_OPERATION_FIELD_MAPPING = "operationFieldMapping";

    String PROPERTY_ZONE = "zone";

    String getName();

    void setName(String name);

    String getComment();

    void setComment(String comment);

    String getLengthClassesPmfmId(int index);

    boolean isLengthClassesPmfmIdEmpty();

    int sizeLengthClassesPmfmId();

    void addLengthClassesPmfmId(String lengthClassesPmfmId);

    void addAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId);

    boolean removeLengthClassesPmfmId(String lengthClassesPmfmId);

    boolean removeAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId);

    boolean containsLengthClassesPmfmId(String lengthClassesPmfmId);

    boolean containsAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId);

    List<String> getLengthClassesPmfmId();

    void setLengthClassesPmfmId(List<String> lengthClassesPmfmId);

    String getIndividualObservationPmfmId(int index);

    boolean isIndividualObservationPmfmIdEmpty();

    int sizeIndividualObservationPmfmId();

    void addIndividualObservationPmfmId(String individualObservationPmfmId);

    void addAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId);

    boolean removeIndividualObservationPmfmId(String individualObservationPmfmId);

    boolean removeAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId);

    boolean containsIndividualObservationPmfmId(String individualObservationPmfmId);

    boolean containsAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId);

    List<String> getIndividualObservationPmfmId();

    void setIndividualObservationPmfmId(List<String> individualObservationPmfmId);

    Integer getVersion();

    void setVersion(Integer version);

    String getImportColumns(int index);

    boolean isImportColumnsEmpty();

    int sizeImportColumns();

    void addImportColumns(String importColumns);

    void addAllImportColumns(Collection<String> importColumns);

    boolean removeImportColumns(String importColumns);

    boolean removeAllImportColumns(Collection<String> importColumns);

    boolean containsImportColumns(String importColumns);

    boolean containsAllImportColumns(Collection<String> importColumns);

    Collection<String> getImportColumns();

    void setImportColumns(Collection<String> importColumns);

    String getProgramId();

    void setProgramId(String programId);

    boolean isUseCalcifiedPieceSampling();

    void setUseCalcifiedPieceSampling(boolean useCalcifiedPieceSampling);

    SpeciesProtocol3 getSpecies(int index);

    boolean isSpeciesEmpty();

    int sizeSpecies();

    void addSpecies(SpeciesProtocol3 species);

    void addAllSpecies(Collection<SpeciesProtocol3> species);

    boolean removeSpecies(SpeciesProtocol3 species);

    boolean removeAllSpecies(Collection<SpeciesProtocol3> species);

    boolean containsSpecies(SpeciesProtocol3 species);

    boolean containsAllSpecies(Collection<SpeciesProtocol3> species);

    List<SpeciesProtocol3> getSpecies();

    void setSpecies(List<SpeciesProtocol3> species);

    SpeciesProtocol3 getBenthos(int index);

    boolean isBenthosEmpty();

    int sizeBenthos();

    void addBenthos(SpeciesProtocol3 benthos);

    void addAllBenthos(Collection<SpeciesProtocol3> benthos);

    boolean removeBenthos(SpeciesProtocol3 benthos);

    boolean removeAllBenthos(Collection<SpeciesProtocol3> benthos);

    boolean containsBenthos(SpeciesProtocol3 benthos);

    boolean containsAllBenthos(Collection<SpeciesProtocol3> benthos);

    List<SpeciesProtocol3> getBenthos();

    void setBenthos(List<SpeciesProtocol3> benthos);

    CaracteristicMappingRow getCaracteristicMapping(int index);

    boolean isCaracteristicMappingEmpty();

    int sizeCaracteristicMapping();

    void addCaracteristicMapping(CaracteristicMappingRow caracteristicMapping);

    void addAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping);

    boolean removeCaracteristicMapping(CaracteristicMappingRow caracteristicMapping);

    boolean removeAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping);

    boolean containsCaracteristicMapping(CaracteristicMappingRow caracteristicMapping);

    boolean containsAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping);

    List<CaracteristicMappingRow> getCaracteristicMapping();

    void setCaracteristicMapping(List<CaracteristicMappingRow> caracteristicMapping);

    OperationFieldMappingRow getOperationFieldMapping(int index);

    boolean isOperationFieldMappingEmpty();

    int sizeOperationFieldMapping();

    void addOperationFieldMapping(OperationFieldMappingRow operationFieldMapping);

    void addAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping);

    boolean removeOperationFieldMapping(OperationFieldMappingRow operationFieldMapping);

    boolean removeAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping);

    boolean containsOperationFieldMapping(OperationFieldMappingRow operationFieldMapping);

    boolean containsAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping);

    Collection<OperationFieldMappingRow> getOperationFieldMapping();

    void setOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping);

    Zone getZone(int index);

    boolean isZoneEmpty();

    int sizeZone();

    void addZone(Zone zone);

    void addAllZone(Collection<Zone> zone);

    boolean removeZone(Zone zone);

    boolean removeAllZone(Collection<Zone> zone);

    boolean containsZone(Zone zone);

    boolean containsAllZone(Collection<Zone> zone);

    Collection<Zone> getZone();

    void setZone(Collection<Zone> zone);

}
