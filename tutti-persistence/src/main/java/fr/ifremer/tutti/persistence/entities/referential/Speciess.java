package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.util.Comparator;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

public class Speciess extends AbstractSpeciess {

    public static final Predicate<Species> IS_TEMPORARY = Speciess::isTemporary;

    /**
     * Is the given {@code species} a temporary data ?
     *
     * @param species species to test
     * @return {@code true} if the given {@code species} is temporary
     * @since 3.8
     */
    public static boolean isTemporary(Species species) {

        Preconditions.checkNotNull(species);
        Preconditions.checkNotNull(species.getId());

        return TuttiReferentialEntities.isStatusTemporary(species) && isTemporaryId(species.getIdAsInt());

    }

    /**
     * Is the given {@code id} is a temporary species id ?
     *
     * @param id id to test
     * @return {@code true} if the id is a species temporary id
     * @since 3.14
     */
    public static boolean isTemporaryId(Integer id) {

        Preconditions.checkNotNull(id);
        return id < 0;

    }

    public static final Comparator<Species> SPECIES_BY_NAME_COMPARATOR = (o1, o2) -> {
        if (o1 == null) {
            return -1;
        }
        if (o2 == null) {
            return 1;
        }
        return o1.getName().compareTo(o2.getName());
    };

    public static final Function<Species, String> GET_REFERECE_TAXON_ID = input -> String.valueOf(input.getReferenceTaxonId());

    public static final Function<Species, Integer> GET_REFERECE_TAXON_ID_AS_INT = Species::getReferenceTaxonId;

    public static final Function<Species, String> GET_REF_TAX_CODE = input -> String.valueOf(input.getRefTaxCode());

    public static final Function<Species, String> GET_SURVEY_CODE = input -> String.valueOf(input.getSurveyCode());

    public static final Function<Species, String> GET_NAME = Species::getName;

    /**
     * Indexe une liste d'espèces référentes par la propriété {@link Species#PROPERTY_REFERENCE_TAXON_ID}.
     *
     * Attention de ne pas utiliser cette méthode sur une liste d'espèces non référentes, car le {@link Species#PROPERTY_REFERENCE_TAXON_ID}
     * peut servir pour différentes espèces (le référent et ses synonymes).
     *
     * Cette méthode vérifie donc avant d'effectuer le split que toutes les espèces données sont référentes.
     *
     * @param list la liste des espèces référentes.
     * @return la dictionnaire des espèces référentes indexées par leur code {@link Species#PROPERTY_REFERENCE_TAXON_ID}.
     */
    public static Map<String, Species> splitReferenceSpeciesByReferenceTaxonId(Iterable<Species> list) {

        for (Species species : list) {
            Preconditions.checkArgument(species.isReferenceTaxon(), "L'espèce " + species.getId() + " n'est pas référente.");
        }

        return Maps.uniqueIndex(list, GET_REFERECE_TAXON_ID);
    }

    public static Multimap<String, Species> splitByReferenceTaxonId(Iterable<Species> list) {
        return Multimaps.index(list, GET_REFERECE_TAXON_ID);
    }

    public static Multimap<String, Species> splitByRefTaxCode(Iterable<Species> list) {
        return Multimaps.index(list, GET_REF_TAX_CODE);
    }

    public static Multimap<String, Species> splitBySurveyCode(Iterable<Species> list) {
        return Multimaps.index(list, GET_SURVEY_CODE);
    }

//    public static Set<String> toReferenceTaxonIds(List<Species> list) {
//        return list == null ?
//                             Collections.<String>emptySet() :
//                             Sets.newHashSet(Lists.transform(list, GET_REFERECE_TAXON_ID));
//    }

    public static String getSurveyCodeOrRefTaxCode(Species species) {
        String code = species.getSurveyCode();

        if (code == null) {

            // use refTaxCode
            code = species.getRefTaxCode();
        }

        if (StringUtils.isEmpty(code)) {


            throw new ApplicationBusinessException(t("tutti.persistence.error.species.withNoSurveyCodeOrRefTaxCode", species.getReferenceTaxonId(), species.getName()));

        }

        return code;
    }
} //Speciess
