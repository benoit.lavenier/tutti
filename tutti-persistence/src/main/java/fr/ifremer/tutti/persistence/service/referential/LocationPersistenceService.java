package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Transactional(readOnly = true)
public interface LocationPersistenceService extends TuttiPersistenceServiceImplementor {

    /**
     * @return all available zones (used by a {@link Program}.
     * @see Program#getZone()
     * @see Program#setZone(TuttiLocation)
     * @since 0.3
     */
    @Cacheable(value = "programZones")
    List<TuttiLocation> getAllProgramZone();

    /**
     * @return all countries (used by a {@link Cruise}).
     * @since 0.1
     */
    @Cacheable(value = "countries")
    List<TuttiLocation> getAllCountry();

    /**
     * @return all harbours (used by a {@link Cruise}).
     * @see Cruise#getDepartureLocation()
     * @see Cruise#setDepartureLocation(TuttiLocation)
     * @see Cruise#getReturnLocation()
     * @see Cruise#setReturnLocation(TuttiLocation)
     * @since 1.2
     */
    @Cacheable(value = "harbours")
    List<TuttiLocation> getAllHarbour();

    @Cacheable(value = "harboursWithObsoletes")
    List<TuttiLocation> getAllHarbourWithObsoletes();

    /**
     * Get the set of all ids of stratas or substratas that match the given zone id.
     *
     * @param zoneId id of the program zone
     * @return set of all strata or substrata that match the given zone id.
     * @since 4.5
     */
    ImmutableSet<Integer> getAllFishingOperationStratasAndSubstratasIdsForProgram(String zoneId);

    /**
     * Get a multimap of location of type substrata by location of type strata that match the given zone id.
     *
     * @param zoneId id of the parent zone (can not be null)
     * @return the stratas and substratas with given zone id as location parent
     * @since 1.0
     */
    Multimap<TuttiLocation, TuttiLocation> getAllFishingOperationStratasAndSubstratas(String zoneId);

    /**
     * Get the list of location of type strata that match the given zone id.
     *
     * @param zoneId id of the parent zone (can not be null)
     * @return the stratas with given zone id as location parent
     * @since 1.0
     */
    List<TuttiLocation> getAllFishingOperationStrata(String zoneId);

    List<TuttiLocation> getAllFishingOperationStrataWithObsoletes(String zoneId);

    /**
     * Get the list of location of type substra that match the given zone id or
     * if not null the given strata id.
     *
     * @param zoneId   id of the parent zone (can not be null)
     * @param strataId id of the optional parent strata
     * @return the list of localite with given zone id as location parent / or strata
     * @since 1.0
     */
    List<TuttiLocation> getAllFishingOperationSubStrata(String zoneId, String strataId);

    List<TuttiLocation> getAllFishingOperationSubStrataWithObsoletes(String zoneId, String strataId);

    /**
     * Get the list of location of type substra that  match the given zone id or
     * if not null the given strata id or if not null the given substrata id if
     * not null.
     *
     * @param zoneId      id of the parent zone (can not be null)
     * @param strataId    id of the optional parent strata
     * @param subStrataId id of the optional parent subStrata
     * @return the list of localite with given zone id as location parent / or strata or substrata
     * @since 1.0
     */
    List<TuttiLocation> getAllFishingOperationLocation(String zoneId, String strataId, String subStrataId);

    List<TuttiLocation> getAllFishingOperationLocationWithObsoletes(String zoneId, String strataId, String subStrataId);

    /**
     * Get a location by id .
     *
     * @param id the id of location to load
     * @return the location for the given id, or {@code null} if not found
     * @since 1.0
     */
    TuttiLocation getLocation(String id);

    /**
     * Return location label from a longitude and a latitude (in decimal degrees - WG84).
     *
     * @param latitude  a latitude (in decimal degrees - WG84)
     * @param longitude a longitude (in decimal degrees - WG84)
     * @return A location label (corresponding to a statistical rectangle), or null if no statistical rectangle exists for this position
     */
    String getLocationLabelByLatLong(Float latitude, Float longitude);

    /**
     * Return a location Id, from a longitude and a latitude (in decimal degrees - WG84).
     * This method typically use getLocationLabelByLatLong().
     *
     * @param latitude  a latitude (in decimal degrees - WG84)
     * @param longitude a longitude (in decimal degrees - WG84)
     * @return A location Id (corresponding to a statistical rectangle), or null if no statistical rectangle exists for this position
     */
    Integer getLocationIdByLatLong(Float latitude, Float longitude);

}
