package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated(value = "org.nuiton.eugene.java.SimpleJavaBeanTransformer", date = "Wed Oct 02 14:29:49 CEST 2013")
public class GearWithOriginalRankOrders extends AbstractGearWithOriginalRankOrders {

    public static <BeanType extends GearWithOriginalRankOrder> BeanType newGearWithOriginalRankOrder(Integer id, Short rankOrder) {
        BeanType result = (BeanType) newGearWithOriginalRankOrder();
        result.setId(id);
        result.setRankOrder(rankOrder);
        return result;
    }

    public static <BeanType extends GearWithOriginalRankOrder> BeanType newGearWithOriginalRankOrder(Gear source) {
        Class<BeanType> sourceType = typeOfGearWithOriginalRankOrder();
        Binder<Gear, BeanType> binder = BinderFactory.newBinder(Gear.class, sourceType);
        BeanType result = (BeanType) newGearWithOriginalRankOrder();
        binder.copy(source, result);
        return result;
    }

    public static List<GearWithOriginalRankOrder> toGearWithOriginalRankOrders(List<Gear> gears) {

        short rankOrder = 1;

        List<GearWithOriginalRankOrder> gearWithOriginalRankOrders = new ArrayList<>();
        for (Gear gear : gears) {

            GearWithOriginalRankOrder gearWithOriginalRankOrder = GearWithOriginalRankOrders.newGearWithOriginalRankOrder(gear);
            gearWithOriginalRankOrder.setRankOrder(rankOrder++);
            gearWithOriginalRankOrders.add(gearWithOriginalRankOrder);
        }

        return gearWithOriginalRankOrders;

    }

}
