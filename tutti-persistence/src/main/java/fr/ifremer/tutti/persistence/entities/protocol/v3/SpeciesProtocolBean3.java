package fr.ifremer.tutti.persistence.entities.protocol.v3;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntityBean;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.Rtp;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SpeciesProtocolBean3 extends TuttiEntityBean implements SpeciesProtocol3 {

    private static final long serialVersionUID = 3486411950096802662L;

    protected Integer speciesReferenceTaxonId;

    protected String speciesSurveyCode;

    protected String lengthStepPmfmId;

    protected boolean weightEnabled;

    protected boolean countIfNoFrequencyEnabled;

    protected boolean calcifySampleEnabled;

    protected Float lengthStep;

    protected boolean madeFromAReferentTaxon;

    protected List<Integer> mandatorySampleCategoryId;

    protected Rtp rtpMale;

    protected Rtp rtpFemale;

    protected Rtp rtpUndefined;

    protected boolean individualObservationEnabled;

    protected String maturityPmfmId;

    protected String calcifiedPiecesSamplingTypePmfmId;

    protected Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition;

    @Override
    public Integer getSpeciesReferenceTaxonId() {
        return speciesReferenceTaxonId;
    }

    @Override
    public void setSpeciesReferenceTaxonId(Integer speciesReferenceTaxonId) {
        this.speciesReferenceTaxonId = speciesReferenceTaxonId;
    }

    @Override
    public String getSpeciesSurveyCode() {
        return speciesSurveyCode;
    }

    @Override
    public void setSpeciesSurveyCode(String speciesSurveyCode) {
        this.speciesSurveyCode = speciesSurveyCode;
    }

    @Override
    public String getLengthStepPmfmId() {
        return lengthStepPmfmId;
    }

    @Override
    public void setLengthStepPmfmId(String lengthStepPmfmId) {
        this.lengthStepPmfmId = lengthStepPmfmId;
    }

    @Override
    public boolean isWeightEnabled() {
        return weightEnabled;
    }

    @Override
    public void setWeightEnabled(boolean weightEnabled) {
        this.weightEnabled = weightEnabled;
    }

    @Override
    public boolean isCountIfNoFrequencyEnabled() {
        return countIfNoFrequencyEnabled;
    }

    @Override
    public void setCountIfNoFrequencyEnabled(boolean countIfNoFrequencyEnabled) {
        this.countIfNoFrequencyEnabled = countIfNoFrequencyEnabled;
    }

    @Override
    public boolean isCalcifySampleEnabled() {
        return calcifySampleEnabled;
    }

    @Override
    public void setCalcifySampleEnabled(boolean calcifySampleEnabled) {
        this.calcifySampleEnabled = calcifySampleEnabled;
    }

    @Override
    public Float getLengthStep() {
        return lengthStep;
    }

    @Override
    public void setLengthStep(Float lengthStep) {
        this.lengthStep = lengthStep;
    }

    @Override
    public boolean isMadeFromAReferentTaxon() {
        return madeFromAReferentTaxon;
    }

    @Override
    public void setMadeFromAReferentTaxon(boolean madeFromAReferentTaxon) {
        this.madeFromAReferentTaxon = madeFromAReferentTaxon;
    }

    @Override
    public Integer getMandatorySampleCategoryId(int index) {
        return getChild(mandatorySampleCategoryId, index);
    }

    @Override
    public boolean isMandatorySampleCategoryIdEmpty() {
        return mandatorySampleCategoryId == null || mandatorySampleCategoryId.isEmpty();
    }

    @Override
    public int sizeMandatorySampleCategoryId() {
        return mandatorySampleCategoryId == null ? 0 : mandatorySampleCategoryId.size();
    }

    @Override
    public void addMandatorySampleCategoryId(Integer mandatorySampleCategoryId) {
        getMandatorySampleCategoryId().add(mandatorySampleCategoryId);
    }

    @Override
    public void addAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId) {
        getMandatorySampleCategoryId().addAll(mandatorySampleCategoryId);
    }

    @Override
    public boolean removeMandatorySampleCategoryId(Integer mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().remove(mandatorySampleCategoryId);
    }

    @Override
    public boolean removeAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().removeAll(mandatorySampleCategoryId);
    }

    @Override
    public boolean containsMandatorySampleCategoryId(Integer mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().contains(mandatorySampleCategoryId);
    }

    @Override
    public boolean containsAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().containsAll(mandatorySampleCategoryId);
    }

    @Override
    public List<Integer> getMandatorySampleCategoryId() {
    if (mandatorySampleCategoryId == null) {
        mandatorySampleCategoryId = new LinkedList<>();
    }
    return mandatorySampleCategoryId;
}

    @Override
    public void setMandatorySampleCategoryId(List<Integer> mandatorySampleCategoryId) {
        this.mandatorySampleCategoryId = mandatorySampleCategoryId;
    }

    @Override
    public Rtp getRtpMale() {
        return rtpMale;
    }

    @Override
    public void setRtpMale(Rtp rtpMale) {
        this.rtpMale = rtpMale;
    }

    @Override
    public Rtp getRtpFemale() {
        return rtpFemale;
    }

    @Override
    public void setRtpFemale(Rtp rtpFemale) {
        this.rtpFemale = rtpFemale;
    }

    @Override
    public Rtp getRtpUndefined() {
        return rtpUndefined;
    }

    @Override
    public void setRtpUndefined(Rtp rtpUndefined) {
        this.rtpUndefined = rtpUndefined;
    }

    @Override
    public boolean isIndividualObservationEnabled() {
        return individualObservationEnabled;
    }

    @Override
    public void setIndividualObservationEnabled(boolean individualObservationEnabled) {
        this.individualObservationEnabled = individualObservationEnabled;
    }

    @Override
    public String getMaturityPmfmId() {
        return maturityPmfmId;
    }

    @Override
    public void setMaturityPmfmId(String maturityPmfmId) {
        this.maturityPmfmId = maturityPmfmId;
    }

    @Override
    public String getCalcifiedPiecesSamplingTypePmfmId() {
        return calcifiedPiecesSamplingTypePmfmId;
    }

    @Override
    public void setCalcifiedPiecesSamplingTypePmfmId(String calcifiedPiecesSamplingTypePmfmId) {
        this.calcifiedPiecesSamplingTypePmfmId = calcifiedPiecesSamplingTypePmfmId;
    }

    @Override
    public CalcifiedPiecesSamplingDefinition getCalcifiedPiecesSamplingDefinition(int index) {
        return getChild(calcifiedPiecesSamplingDefinition, index);
    }

    @Override
    public boolean isCalcifiedPiecesSamplingDefinitionEmpty() {
        return calcifiedPiecesSamplingDefinition == null || calcifiedPiecesSamplingDefinition.isEmpty();
    }

    @Override
    public int sizeCalcifiedPiecesSamplingDefinition() {
        return calcifiedPiecesSamplingDefinition == null ? 0 : calcifiedPiecesSamplingDefinition.size();
    }

    @Override
    public void addCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition) {
        getCalcifiedPiecesSamplingDefinition().add(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public void addAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        getCalcifiedPiecesSamplingDefinition().addAll(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean removeCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().remove(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean removeAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().removeAll(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean containsCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().contains(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean containsAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().containsAll(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public Collection<CalcifiedPiecesSamplingDefinition> getCalcifiedPiecesSamplingDefinition() {
    if (calcifiedPiecesSamplingDefinition == null) {
        calcifiedPiecesSamplingDefinition = new LinkedList<>();
    }
    return calcifiedPiecesSamplingDefinition;
}

    @Override
    public void setCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        this.calcifiedPiecesSamplingDefinition = calcifiedPiecesSamplingDefinition;
    }

    @Override
    public boolean withRtpMale() {
        return rtpMale != null;
    }

    @Override
    public boolean withRtpFemale() {
        return rtpFemale != null;
    }

    @Override
    public boolean withRtpUndefined() {
        return rtpUndefined != null;
    }

}
