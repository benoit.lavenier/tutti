package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.administration.programStrategy.ProgramDao;
import fr.ifremer.adagio.core.dao.administration.programStrategy.ProgramImpl;
import fr.ifremer.adagio.core.dao.administration.user.PersonDao;
import fr.ifremer.adagio.core.dao.administration.user.PersonId;
import fr.ifremer.adagio.core.dao.administration.user.PersonImpl;
import fr.ifremer.adagio.core.dao.data.measure.GearPhysicalMeasurement;
import fr.ifremer.adagio.core.dao.data.survey.fishingTrip.FishingTrip;
import fr.ifremer.adagio.core.dao.data.survey.fishingTrip.ObservedFishingTrip;
import fr.ifremer.adagio.core.dao.data.survey.scientificCruise.ScientificCruise;
import fr.ifremer.adagio.core.dao.data.survey.scientificCruise.ScientificCruiseDao;
import fr.ifremer.adagio.core.dao.data.vessel.VesselDao;
import fr.ifremer.adagio.core.dao.data.vessel.VesselImpl;
import fr.ifremer.adagio.core.dao.data.vessel.feature.person.VesselPersonFeatures;
import fr.ifremer.adagio.core.dao.data.vessel.feature.physical.GearPhysicalFeatures;
import fr.ifremer.adagio.core.dao.referential.QualityFlag;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagDao;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.VesselPersonRole;
import fr.ifremer.adagio.core.dao.referential.VesselPersonRoleId;
import fr.ifremer.adagio.core.dao.referential.gear.GearDao;
import fr.ifremer.adagio.core.dao.referential.location.Location;
import fr.ifremer.adagio.core.dao.referential.location.LocationDao;
import fr.ifremer.adagio.core.dao.referential.location.LocationImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmDao;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValue;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueDao;
import fr.ifremer.tutti.persistence.dao.GearPhysicalFeaturesDaoTutti;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.PersonPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.VesselPersistenceService;
import fr.ifremer.tutti.persistence.service.util.MeasurementPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.SynchronizationStatusHelper;
import fr.ifremer.tutti.persistence.service.util.VesselPersonFeaturesPersistenceHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ShortType;
import org.hibernate.type.StringType;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Service("cruisePersistenceService")
public class CruisePersistenceServiceImpl extends AbstractPersistenceService implements CruisePersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CruisePersistenceServiceImpl.class);


    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "gearPersistenceService")
    private GearPersistenceService gearService;

    @Resource(name = "locationPersistenceService")
    private LocationPersistenceService locationService;

    @Resource(name = "personPersistenceService")
    private PersonPersistenceService personService;

    @Resource(name = "vesselPersistenceService")
    private VesselPersistenceService vesselService;

    @Resource(name = "programPersistenceService")
    protected ProgramPersistenceService programService;

    @Resource(name = "fishingOperationPersistenceService")
    protected FishingOperationPersistenceService fishingOperationService;

    @Resource(name = "vesselPersonFeaturesPersistenceHelper")
    protected VesselPersonFeaturesPersistenceHelper vesselPersonFeaturesPersistenceHelper;

    @Resource(name = "scientificCruiseDao")
    protected ScientificCruiseDao scientificCruiseDao;

    @Resource(name = "programDao")
    protected ProgramDao programDao;

    @Resource(name = "personDao")
    protected PersonDao personDao;

    @Resource(name = "vesselExtendDao")
    protected VesselDao vesselDao;

    @Resource(name = "locationDao")
    protected LocationDao locationDao;

    @Resource(name = "qualityFlagDao")
    protected QualityFlagDao qualityFlagDao;

    @Resource(name = "pmfmDao")
    protected PmfmDao pmfmDao;

    @Resource(name = "qualitativeValueDao")
    protected QualitativeValueDao qualitativeValueDao;

    @Resource(name = "gearDao")
    protected GearDao gearDao;

    @Resource(name = "gearPhysicalFeaturesDaoTutti")
    protected GearPhysicalFeaturesDaoTutti gearPhysicalFeaturesDao;

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

    @Resource(name = "synchronizationStatusHelper")
    protected SynchronizationStatusHelper synchronizationStatusHelper;

    @Override
    public List<Integer> getAllCruiseId(String programId) {

        Preconditions.checkNotNull(programId);

        Iterator list = queryList(
                "allCruiseIds",
                "programCode", StringType.INSTANCE, programId);

        List<Integer> result = Lists.newArrayList();
        while (list.hasNext()) {
            Integer id = (Integer) list.next();
            result.add(id);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Cruise> getAllCruise(String programId) {

        Preconditions.checkNotNull(programId);

        Iterator<Object[]> list = queryList(
                "allCruises",
                "programCode", StringType.INSTANCE, programId);

        List<Cruise> result = Lists.newArrayList();
        while (list.hasNext()) {
            Object[] source = list.next();
            Cruise target = Cruises.newCruise();
            target.setId(String.valueOf(source[0]));
            target.setName((String) source[1]);
            target.setBeginDate((Date) source[2]);
            target.setSynchronizationStatus((String) source[3]);
            result.add(target);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public Cruise getCruise(Integer cruiseId) {

        Preconditions.checkNotNull(cruiseId);

        Object[] source = queryUnique(
                "cruise",
                "cruiseId", IntegerType.INSTANCE, cruiseId,
                "pmfmIdSurveyPart", IntegerType.INSTANCE, PmfmId.SURVEY_PART.getValue());

        if (source == null) {
            throw new DataRetrievalFailureException("Could not retrieve cruise with id=" + cruiseId);
        }
        Cruise result = Cruises.newCruise();
        result.setId(cruiseId);

        int index = 0;

        // departureLocation
        Integer departureLocationId = (Integer) source[index++];
        TuttiLocation departureLocation = locationService.getLocation(String.valueOf(departureLocationId));
        result.setDepartureLocation(departureLocation);

        // returnLocation
        Integer returnLocationId = (Integer) source[index++];
        TuttiLocation returnLocation = locationService.getLocation(String.valueOf(returnLocationId));
        result.setReturnLocation(returnLocation);

        // program
        String programCode = (String) source[index++];
        Program program = programService.getProgram(programCode);
        result.setProgram(program);

        // name
        result.setName((String) source[index++]);

        // begin Date
        result.setBeginDate((Date) source[index++]);

        // end Date
        result.setEndDate((Date) source[index++]);

        // vessel
        String vesselCode = (String) source[index++];
        Vessel vessel = vesselService.getVessel(vesselCode);
        result.setVessel(vessel);

        // head of sort room
        List<Person> headOfMissions = Lists.newArrayList();
        result.setHeadOfMission(headOfMissions);
        List<Person> headOfSortRoom = Lists.newArrayList();
        result.setHeadOfSortRoom(headOfSortRoom);

        Integer managerId = (Integer) source[index++];
        Person manager = personService.getPerson(managerId);
        headOfMissions.add(manager);

        // comment
        result.setComment((String) source[index++]);

        // synchronizationStatus
        result.setSynchronizationStatus((String) source[index++]);

        // surverPart
        result.setSurveyPart((String) source[index]);

        // get cruise gears
        Iterator<Object[]> list = queryList(
                "allCruiseGears",
                "cruiseId", IntegerType.INSTANCE, cruiseId,
                "pmfmIdTrawlNet", IntegerType.INSTANCE, PmfmId.MULTIRIG_NUMBER.getValue());

        List<GearWithOriginalRankOrder> gears = Lists.newArrayList();
        int maxMultirigNumberFound = 0;
        while (list.hasNext()) {
            Object[] gearRow = list.next();
            Gear simpleGear = gearService.getGear((Integer) gearRow[0]);
            GearWithOriginalRankOrder target = GearWithOriginalRankOrders.newGearWithOriginalRankOrder(simpleGear);
            target.setRankOrder((Short) gearRow[1]);
            Float multirigNumber = (Float) gearRow[2];
            if (multirigNumber != null && multirigNumber.intValue() > maxMultirigNumberFound) {
                maxMultirigNumberFound = multirigNumber.intValue();
            }
            gears.add(target);
        }
        result.setGear(gears);
        if (maxMultirigNumberFound > 0) {
            result.setMultirigNumber(maxMultirigNumberFound);
        }

        Iterator<Object[]> vesselPersonFeaturesList = queryList(
                "allCruiseVesselPersonFeatures",
                "cruiseId", IntegerType.INSTANCE, cruiseId);
        while (vesselPersonFeaturesList.hasNext()) {
            Object[] vesselPersonFeatures = vesselPersonFeaturesList.next();

            Integer personId = (Integer) vesselPersonFeatures[0];
            Person person = personService.getPerson(personId);
            Integer roleId = (Integer) vesselPersonFeatures[1];
            if (VesselPersonRoleId.SCIENTIFIC_CRUISE_MANAGER.getValue().equals(roleId)) {
                headOfMissions.add(person);

            } else if (VesselPersonRoleId.SORT_ROOM_MANAGER.getValue().equals(roleId)) {
                headOfSortRoom.add(person);
            }
        }

        // Force initialization of multirigNumber to 1 initialization (need for UI)
        if (result.getMultirigNumber() == null) {
            log.warn(MessageFormat.format("Cruise with id={0} has been load with a default multirigNumber=1, beacause not multirigNumber were found in database.", cruiseId));
            result.setMultirigNumber(1);
        }
        return result;

    }

    @Override
    public Cruise createCruise(Cruise bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkArgument(bean.getId() == null, "Cruise 'id' must be null to call createCruise().");
        Preconditions.checkNotNull(bean.getProgram());
        Preconditions.checkNotNull(bean.getBeginDate());
        Preconditions.checkNotNull(bean.getEndDate());
        Preconditions.checkNotNull(bean.getDepartureLocation());
        Preconditions.checkNotNull(bean.getReturnLocation());
        Preconditions.checkNotNull(bean.getVessel());
        Preconditions.checkState(CollectionUtils.isNotEmpty(bean.getHeadOfMission()));

        ScientificCruise scientificCruise = ScientificCruise.Factory.newInstance();
        cruiseToEntity(bean, scientificCruise);
        scientificCruiseDao.create(scientificCruise);

        bean.setId(String.valueOf(scientificCruise.getId()));
        bean.setSynchronizationStatus(scientificCruise.getSynchronizationStatus());
        return bean;
    }

    @Override
    public Cruise saveCruise(Cruise bean,
                             boolean updateVessel,
                             boolean updateGear) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkNotNull(bean.getId(), "Cruise 'id' must not be null or empty to be saved.");
        Preconditions.checkNotNull(bean.getProgram());
        Preconditions.checkNotNull(bean.getBeginDate());
        Preconditions.checkNotNull(bean.getEndDate());
        Preconditions.checkNotNull(bean.getDepartureLocation());
        Preconditions.checkNotNull(bean.getReturnLocation());
        Preconditions.checkNotNull(bean.getVessel());
        Preconditions.checkState(CollectionUtils.isNotEmpty(bean.getHeadOfMission()));

        ScientificCruise scientificCruise = scientificCruiseDao.load(Integer.valueOf(bean.getId()));
        if (scientificCruise == null) {
            throw new DataRetrievalFailureException(
                    "Could not retrieve cruise with id=" + bean.getId());
        }

        cruiseToEntity(bean, scientificCruise);
        scientificCruiseDao.update(scientificCruise);

        if (updateVessel) {
            Vessel vessel = bean.getVessel();
            if (log.isInfoEnabled()) {
                log.info("Change vessel to " + vessel.getId() +
                         " for cruise: " + bean.getId());
            }
            queryUpdate("updateOperationsVessel",
                        "cruiseId", IntegerType.INSTANCE, scientificCruise.getId(),
                        "vesselId", StringType.INSTANCE, scientificCruise.getVessel().getCode());
        }

        if (updateGear) {

            for (GearWithOriginalRankOrder gear : bean.getGear()) {

                Short originalRankOrder = gear.getOriginalRankOrder();
                Short newRankOrder = gear.getRankOrder();

                if (originalRankOrder != null && originalRankOrder != 0 &&
                    ObjectUtils.notEqual(originalRankOrder, newRankOrder)) {

                    // gear was used, but the rank order has changed
                    // must update all operation using it

                    if (log.isInfoEnabled()) {
                        log.info("Change gear " + gear.getId() + ", old rankOrder:" +
                                 originalRankOrder + " to " + newRankOrder +
                                 " for cruise: " + bean.getId());
                    }

                    queryUpdate("updateOperationsGear",
                                "cruiseId", IntegerType.INSTANCE, scientificCruise.getId(),
                                "gearId", IntegerType.INSTANCE, gear.getIdAsInt(),
                                "oldRankOrder", ShortType.INSTANCE, originalRankOrder,
                                "newRankOrder", ShortType.INSTANCE, newRankOrder
                    );
                }
            }
        }

        bean.setSynchronizationStatus(scientificCruise.getSynchronizationStatus());
        return bean;
    }

    @Override
    public void setCruiseReadyToSynch(Integer cruiseId) {
        synchronizationStatusHelper.setCruiseReadyToSynch(cruiseId);
    }

    @Override
    public CaracteristicMap getGearCaracteristics(Integer cruiseId,
                                                  Integer gearId,
                                                  short rankOrder) {
        Preconditions.checkNotNull(cruiseId, "Cruise 'id' must not be null or empty");
        Preconditions.checkNotNull(gearId, "Gear 'id' must not be null or empty");

        if (log.isDebugEnabled()) {
            log.debug("Get caracteristic for gear " + gearId + " - " + rankOrder);
        }

        CaracteristicMap result = new CaracteristicMap();

        Iterator<GearPhysicalMeasurement> list = queryListTyped(
                "gearCaracteristics",
                "cruiseId", IntegerType.INSTANCE, cruiseId,
                "gearId", IntegerType.INSTANCE, gearId,
                "rankOrder", ShortType.INSTANCE, rankOrder);

        while (list.hasNext()) {
            GearPhysicalMeasurement measurement = list.next();

            Integer pmfmId = measurement.getPmfm().getId();
            if (!PmfmId.MULTIRIG_NUMBER.getValue().equals(pmfmId)) {
                Caracteristic caracteristic = caracteristicService.getCaracteristic(pmfmId);

                Serializable value = null;
                switch (caracteristic.getCaracteristicType()) {
                    case NUMBER:
                        value = measurement.getNumericalValue();
                        break;
                    case QUALITATIVE:
                        QualitativeValue qualitativeValue = measurement.getQualitativeValue();
                        if (qualitativeValue != null) {
                            value = CaracteristicQualitativeValues.getQualitativeValue(caracteristic,
                                                                                       qualitativeValue.getId());
                        }
                        break;
                    case TEXT:
                        value = measurement.getAlphanumericalValue();
                        break;
                }
                result.put(caracteristic, value);
            }
        }
        return result;
    }

    @Override
    public void saveGearCaracteristics(Gear gear, Cruise cruise) {
        Preconditions.checkNotNull(cruise);
        Preconditions.checkNotNull(cruise.getId(), "Cruise 'id' must not be null or empty");
        Preconditions.checkNotNull(gear);
        Preconditions.checkNotNull(gear.getId(), "Gear 'id' must not be null or empty");

        short rankOrder;

        if (gear.getRankOrder() == null) {
            rankOrder = 1;
        } else {
            rankOrder = gear.getRankOrder();
        }

        ScientificCruise scientificCruise = scientificCruiseDao.load(cruise.getIdAsInt());
        if (scientificCruise == null) {
            throw new DataRetrievalFailureException("Could not retrieve cruise with id=" + cruise.getId());
        }

        // Retrieve entities : FishingTrip
        ObservedFishingTrip fishingTrip = (ObservedFishingTrip) scientificCruise.getFishingTrips().iterator().next();
        Preconditions.checkNotNull(fishingTrip);

        List<Integer> measurementsToRemove = Lists.newArrayList();

        if (log.isDebugEnabled()) {
            log.debug("Save caracteristic for gear " + gear.getIdAsInt() + " - " + rankOrder);
        }
        GearPhysicalFeatures gpf = gearPhysicalFeaturesDao.getGearPhysicalfeatures(
                fishingTrip,
                gear.getIdAsInt(),
                rankOrder,
                true);

        if (gpf.getGearPhysicalMeasurements() != null) {
            for (GearPhysicalMeasurement measurement : gpf.getGearPhysicalMeasurements()) {
                Integer pmfmId = measurement.getPmfm().getId();
                if (!PmfmId.MULTIRIG_NUMBER.getValue().equals(pmfmId)) {
                    measurementsToRemove.add(pmfmId);
                }
            }
        }

        CaracteristicMap caracteristicMap = gear.getCaracteristics();
        if (caracteristicMap != null) {
            for (Caracteristic caracteristic : caracteristicMap.keySet()) {

                Float numericalValue = null;
                String alphanumericalValue = null;
                Integer qualitativeValue = null;
                Object value = caracteristicMap.get(caracteristic);

                switch (caracteristic.getCaracteristicType()) {
                    case NUMBER:
                        numericalValue = (Float) value;
                        break;

                    case QUALITATIVE:
                        Integer qvId = null;
                        if (value instanceof CaracteristicQualitativeValue) {
                            qvId = Integer.valueOf(((CaracteristicQualitativeValue) value).getId());
                        } else if (value instanceof Integer) {
                            qvId = (Integer) value;
                        }
                        qualitativeValue = qvId;
                        break;

                    case TEXT:
                        alphanumericalValue = (String) value;
                        break;
                }

                GearPhysicalMeasurement gearPhysicalMeasurement =
                        gearPhysicalFeaturesDao.setGearPhysicalMeasurement(scientificCruise,
                                                                           gpf,
                                                                           caracteristic.getIdAsInt(),
                                                                           numericalValue,
                                                                           alphanumericalValue,
                                                                           qualitativeValue);

                measurementsToRemove.remove(gearPhysicalMeasurement.getPmfm().getId());
            }
        }

        for (Integer pmfmId : measurementsToRemove) {
            gearPhysicalFeaturesDao.removeGearPhysicalMeasurement(gpf, pmfmId);
        }

        gearPhysicalFeaturesDao.update(gpf);
    }

    @Override
    public boolean isOperationUseGears(Integer cruiseId, Collection<Gear> gears) {

        Preconditions.checkNotNull(cruiseId);
        Preconditions.checkNotNull(gears);

        boolean result = false;

        if (!gears.isEmpty()) {
            // ---
            // get all gears found in operations of the cruise
            // ---

            List<Integer> gearIds = TuttiEntities.toIntegerIds(gears);
            Iterator<Object[]> list = queryList(
                    "allFishingOperationsWithGear",
                    "cruiseId", IntegerType.INSTANCE, cruiseId,
                    "gearIds", IntegerType.INSTANCE, gearIds
            );

            while (list.hasNext()) {
                Object[] next = list.next();
                GearWithOriginalRankOrder g = GearWithOriginalRankOrders.newGearWithOriginalRankOrder(
                        (Integer) next[0],
                        (Short) next[1]
                );
                if (gears.contains(g)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    protected void cruiseToEntity(Cruise source, ScientificCruise target) {

        QualityFlag qualityFlagNotQualified = load(
                QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue());

        // Retrieve entities : FishingTrip
        ObservedFishingTrip fishingTrip;
        if (CollectionUtils.isEmpty(target.getFishingTrips())) {
            fishingTrip = ObservedFishingTrip.Factory.newInstance();
            if (target.getFishingTrips() == null) {
                target.setFishingTrips(Lists.newArrayList((FishingTrip) fishingTrip));
                fishingTrip.setScientificCruise(target);
            } else {
                target.getFishingTrips().add(fishingTrip);
                fishingTrip.setScientificCruise(target);
            }
        } else {
            fishingTrip = (ObservedFishingTrip) target.getFishingTrips().iterator().next();
        }

        // Name
        target.setName(source.getName());

        // Program
        ProgramImpl program = load(ProgramImpl.class, source.getProgram().getId());
        target.setProgram(program);

        // Vessel
        VesselImpl vessel = load(VesselImpl.class, source.getVessel().getId());
        target.setVessel(vessel);

        // BeginDate
        target.setDepartureDateTime(dateWithNoSecondAndMiliSecond(source.getBeginDate()));

        // EndDate
        target.setReturnDateTime(dateWithNoSecondAndMiliSecond(source.getEndDate()));

        // Comment
        target.setComments(source.getComment());

        // Optional values in UI, but mandatory in DB
        if (target.getManagerPerson() == null) {
            target.setManagerPerson(load(PersonImpl.class, PersonId.UNKNOWN_RECORDER_PERSON.getValue()));
        }

        // Default values :
        if (target.getCreationDate() == null) {
            target.setCreationDate(newCreateDate());
        }
        if (target.getManagerPerson() != null) {
            target.setRecorderPerson(target.getManagerPerson());
            target.setRecorderDepartment(target.getManagerPerson().getDepartment());
        }

        // Fill fishing trip with scientificCruise info:
        fishingTrip.setDepartureDateTime(target.getDepartureDateTime());
        fishingTrip.setReturnDateTime(target.getReturnDateTime());
        fishingTrip.setVessel(target.getVessel());
        fishingTrip.setProgram(target.getProgram());
        fishingTrip.setRecorderPerson(target.getRecorderPerson());
        fishingTrip.setRecorderDepartment(target.getRecorderDepartment());
        fishingTrip.setCreationDate(target.getCreationDate());
        fishingTrip.setQualityFlag(qualityFlagNotQualified);

        // SynchronizationStatus
        synchronizationStatusHelper.setDirty(target);
        synchronizationStatusHelper.setDirty(fishingTrip);

        // DepartureLocation
        Location departureLocation = load(LocationImpl.class, Integer.valueOf(source.getDepartureLocation().getId()));
        fishingTrip.setDepartureLocation(departureLocation);

        // ReturnLocation
        Location returnLocation = load(LocationImpl.class, Integer.valueOf(source.getReturnLocation().getId()));
        fishingTrip.setReturnLocation(returnLocation);

        if (StringUtils.isEmpty(source.getSurveyPart())) {

            // remove surveyMeasurement if exists
            measurementPersistenceHelper.removeSurveyMeasurement(fishingTrip, PmfmId.SURVEY_PART.getValue());
        } else {

            // update it or create it
            measurementPersistenceHelper.setSurveyMeasurement(fishingTrip, PmfmId.SURVEY_PART.getValue(), null, source.getSurveyPart(), null);
        }

        // Gear
        if (source.isGearEmpty() && fishingTrip.getGearPhysicalFeatures() != null) {
            fishingTrip.getGearPhysicalFeatures().clear();
        } else if (!source.isGearEmpty()) {
            // Create a list to trace not updated items, to be able to remove them later
            Set<GearPhysicalFeatures> notChangedGearPhysicalFeatures = Sets.newHashSet();
            if (fishingTrip.getGearPhysicalFeatures() != null) {
                notChangedGearPhysicalFeatures.addAll(fishingTrip.getGearPhysicalFeatures());
            }

            short gearRankOrder = 1;
            // Create or update a geaPhysicalFeatures for each gears in the cruise
            for (GearWithOriginalRankOrder gear : source.getGear()) {

                Short rankOrder = gear.getRankOrder();

                GearPhysicalFeatures guf;

                // try first to get the guf with originalRankOrder
                guf = gearPhysicalFeaturesDao.getGearPhysicalfeatures(fishingTrip, gear.getIdAsInt(), gear.getOriginalRankOrder(), false);

                if (guf == null) {

                    // try with new rankOrder
                    // in facts, only means create a new one
                    guf = gearPhysicalFeaturesDao.getGearPhysicalfeatures(fishingTrip, gear.getIdAsInt(), rankOrder, true);
                    if (log.isDebugEnabled()) {
                        log.debug("Will create guf: " + guf.getId() + " for gear: " + gear.getId() + " - " + gear.getRankOrder());
                    }
                } else {

                    // got an old guf to update
                    if (log.isDebugEnabled()) {
                        log.debug("Will update guf: " + guf.getId() + " for gear: " + gear.getId() + " - " + gear.getOriginalRankOrder() + ", new rankOrder: " + gear.getRankOrder());
                    }
                }

                notChangedGearPhysicalFeatures.remove(guf);

                guf.setStartDate(fishingTrip.getDepartureDateTime());
                guf.setEndDate(fishingTrip.getReturnDateTime());
                guf.setVessel(fishingTrip.getVessel());
                guf.setProgram(fishingTrip.getProgram());
                guf.setCreationDate(target.getCreationDate());
                guf.setQualityFlag(qualityFlagNotQualified);

                // realign the rankOrder using the order of the list of gears
                guf.setRankOrder(gearRankOrder++);

                // Trawl net (store in Gear Physical features)
                if (source.getMultirigNumber() == null) {
                    gearPhysicalFeaturesDao.removeGearPhysicalMeasurement(guf, PmfmId.MULTIRIG_NUMBER.getValue());
                } else {
                    gearPhysicalFeaturesDao.setGearPhysicalMeasurement(target, guf, PmfmId.MULTIRIG_NUMBER.getValue(), Float.valueOf(source.getMultirigNumber()), null, null);
                }
            }

            // Remove deleted gear physical features
            if (fishingTrip.getGearPhysicalFeatures() != null && notChangedGearPhysicalFeatures.size() > 0) {
                List<Gear> gears = Lists.transform(Lists.newArrayList(notChangedGearPhysicalFeatures),
                                                   input -> {
                                                       Gear result = Gears.newGear();

                                                       if (input != null && input.getGear() != null) {
                                                           result.setRankOrder(input.getRankOrder());
                                                           result.setId(input.getGear().getId());
                                                       }
                                                       return result;
                                                   }
                );

                boolean gearRemoved = isOperationUseGears(target.getId(), gears);
                if (gearRemoved) {
                    throw new ApplicationBusinessException(t("tutti.persistence.cruise.gearUsedInOperations.error"));
                }

                for (GearPhysicalFeatures guf : notChangedGearPhysicalFeatures) {
                    if (guf.getGearPhysicalMeasurements() != null) {
                        guf.getGearPhysicalMeasurements().clear();
                    }
                    fishingTrip.getGearPhysicalFeatures().remove(guf);
                }
            }
        }

        VesselPersonRole scientificCruiseManagerRole =
                vesselPersonFeaturesPersistenceHelper.getScientificCruiseManagerRole();

        VesselPersonRole sortRoomManagerRole =
                vesselPersonFeaturesPersistenceHelper.getSortRoomManagerRole();

        Map<Integer, VesselPersonFeatures> vesselPersonFeaturesMap = Maps.newLinkedHashMap();

        short personRankOrder = 1;

        // Head of Mission Managers (act as rankOrder 0)
        List<Person> persons = source.getHeadOfMission();
        target.setManagerPerson(load(PersonImpl.class, persons.get(0).getIdAsInt()));

        for (int i = 1; i < persons.size(); i++) {
            Integer personId = persons.get(i).getIdAsInt();
            vesselPersonFeaturesPersistenceHelper.fillVesselPersonFeatures(
                    vesselPersonFeaturesMap,
                    personId,
                    fishingTrip,
                    scientificCruiseManagerRole,
                    personRankOrder++);
        }

        // Sort Room Managers
        if (CollectionUtils.isNotEmpty(source.getHeadOfSortRoom())) {
            for (Person person : source.getHeadOfSortRoom()) {
                Integer personId = person.getIdAsInt();
                vesselPersonFeaturesPersistenceHelper.fillVesselPersonFeatures(
                        vesselPersonFeaturesMap,
                        personId,
                        fishingTrip,
                        sortRoomManagerRole,
                        personRankOrder++);
            }
        }

        if (fishingTrip.getVesselPersonFeatures() == null) {
            fishingTrip.setVesselPersonFeatures(Sets.<VesselPersonFeatures>newHashSet());
        }
        fishingTrip.getVesselPersonFeatures().clear();
        fishingTrip.getVesselPersonFeatures().addAll(vesselPersonFeaturesMap.values());
    }
}
