package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Define the complete sample category model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SampleCategoryModel implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final List<SampleCategoryModelEntry> category;

    protected final List<Integer> samplingOrder;

    protected final List<SampleCategoryModelEntry> reverseCategory;

    protected final Map<Integer, SampleCategoryModelEntry> categoryMap;

    public SampleCategoryModel(List<SampleCategoryModelEntry> category) {
        List<Integer> samplingOrder = Lists.newArrayList();
        this.category = Lists.newArrayList(category);
        this.categoryMap = Maps.newLinkedHashMap();
        for (SampleCategoryModelEntry def : category) {
            categoryMap.put(def.getCategoryId(), def);
            samplingOrder.add(def.getCategoryId());
        }

        this.samplingOrder = Collections.unmodifiableList(samplingOrder);

        List<SampleCategoryModelEntry> reverseCategory = Lists.newArrayList(category);
        Collections.reverse(reverseCategory);
        this.reverseCategory = Collections.unmodifiableList(reverseCategory);
    }

    public void load(TuttiPersistence service) {

        for (SampleCategoryModelEntry def : category) {
            def.load(service);
        }
    }

    public List<Integer> getSamplingOrder() {
        return samplingOrder;
    }

    public List<SampleCategoryModelEntry> getCategory() {
        return category;
    }

    public List<SampleCategoryModelEntry> getReverseCategory() {
        return reverseCategory;
    }

    public Map<Integer, SampleCategoryModelEntry> getCategoryMap() {
        return categoryMap;
    }

    public SampleCategoryModelEntry getCategoryById(Integer categoryId) {
        return categoryMap.get(categoryId);
    }

    public SampleCategoryModelEntry getCategoryByIndex(int index) {
        return category.get(index);
    }

    @Override
    public String toString() {
        List<String> entries = Lists.newArrayList();
        for (SampleCategoryModelEntry entry : category) {
            entries.add(entry.toString());
        }
        return Joiner.on('|').join(entries);
    }

    public Integer getLastCategoryId() {
        return samplingOrder.isEmpty() ? null : samplingOrder.get(samplingOrder.size() - 1);
    }

    public int getNbSampling() {
        return samplingOrder.size();
    }

    public Integer getFirstCategoryId() {
        return samplingOrder.isEmpty() ? null : samplingOrder.get(0);
    }

    public boolean containsCategoryId(Integer id) {
        return samplingOrder.contains(id);
    }

    public int indexOf(SampleCategoryModelEntry sampleCategoryDef) {
        return category.indexOf(sampleCategoryDef);
    }

    public Map<String, CaracteristicQualitativeValue> toMap() {

        Map<String, CaracteristicQualitativeValue> sampleCategoryValueMap = new TreeMap<>();

        for (SampleCategoryModelEntry sampleCategoryModelEntry : getCategory()) {
            Caracteristic caracteristic = sampleCategoryModelEntry.getCaracteristic();
            if (caracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE) {
                List<CaracteristicQualitativeValue> qualitativeValue = caracteristic.getQualitativeValue();
                for (CaracteristicQualitativeValue value : qualitativeValue) {
                    sampleCategoryValueMap.put(value.getId(), value);
                }
            }
        }
        return sampleCategoryValueMap;

    }
}
