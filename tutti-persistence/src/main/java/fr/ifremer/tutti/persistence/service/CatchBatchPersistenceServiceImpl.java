package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.base.Preconditions;
import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatchImpl;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatchImpl;
import fr.ifremer.adagio.core.dao.data.measure.QuantificationMeasurement;
import fr.ifremer.adagio.core.dao.data.operation.FishingOperationImpl;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchBean;
import fr.ifremer.tutti.persistence.service.util.BatchPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.tree.BatchTreeHelper;
import fr.ifremer.tutti.persistence.service.util.MeasurementPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.SynchronizationStatusHelper;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.Numbers;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.FlushMode;
import org.hibernate.type.IntegerType;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Service("batchPersistenceService")
public class CatchBatchPersistenceServiceImpl
        extends AbstractPersistenceService implements CatchBatchPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CatchBatchPersistenceServiceImpl.class);

    @Resource(name = "attachmentPersistenceService")
    protected AttachmentPersistenceService attachmentPersistenceService;

    @Resource(name = "batchPersistenceHelper")
    protected BatchPersistenceHelper batchHelper;

    @Resource(name = "batchTreeHelper")
    protected BatchTreeHelper batchTreeHelper;

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

    @Resource(name = "synchronizationStatusHelper")
    protected SynchronizationStatusHelper synchronizationStatusHelper;

    //------------------------------------------------------------------------//
    //-- CatchBatch methods                                                 --//
    //------------------------------------------------------------------------//

    @Override
    public boolean isFishingOperationWithCatchBatch(Integer operationId) {
        Preconditions.checkNotNull(operationId);
        return batchHelper.isCatchBatchExistsForFishingOperation(operationId);
    }

    @Override
    public CatchBatch getCatchBatchFromFishingOperation(Integer fishingOperationId) {
        Preconditions.checkNotNull(fishingOperationId);

        // whenever want to repair anything from Tutti
        fr.ifremer.adagio.core.dao.data.batch.CatchBatch source =
                batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, true);

        if (log.isDebugEnabled()) {
            log.debug("Loaded CatchBatch: " + source.getId());
        }

        boolean showBatchLog = TuttiConfiguration.getInstance().isShowBatchLog();

        if (showBatchLog) {

            batchTreeHelper.displayCatchBatch(source);

        }

        CatchBatch result = new CatchBatchBean();

        entityToBean(source, result);

        return result;
    }

    @Override
    public CatchBatch createCatchBatch(CatchBatch bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkArgument(bean.getId() == null);
        Preconditions.checkNotNull(bean.getFishingOperation());
        Preconditions.checkNotNull(bean.getFishingOperation().getId());

        fr.ifremer.adagio.core.dao.data.batch.CatchBatch catchBatch = fr.ifremer.adagio.core.dao.data.batch.CatchBatch.Factory.newInstance();
        beanToEntity(bean, catchBatch);
        bean = batchHelper.createCatchBatch(bean, catchBatch);

        // Link to fishing operation
        getCurrentSession().flush();
        Integer fishingOperationId = bean.getFishingOperation().getIdAsInt();
        int rowUpdated = queryUpdate("updateFishingOperationCatchBatch",
                                     "fishingOperationId", IntegerType.INSTANCE, fishingOperationId,
                                     "catchBatchId", IntegerType.INSTANCE, catchBatch.getId());
        if (rowUpdated == 0) {
            throw new DataIntegrityViolationException("Could not attach catch batch to the given operation : operation was not found.");
        }

        // bean is dirty
        synchronizationStatusHelper.setDirty(bean);
        return bean;
    }

    @Override
    public CatchBatch saveCatchBatch(CatchBatch bean) {

        Preconditions.checkNotNull(bean);
        Preconditions.checkNotNull(bean.getId());
        Preconditions.checkNotNull(bean.getFishingOperation());
        Preconditions.checkNotNull(bean.getFishingOperation().getId());

        getCurrentSession().enableFetchProfile("batch-with-childs");
        getCurrentSession().setFlushMode(FlushMode.COMMIT);
        fr.ifremer.adagio.core.dao.data.batch.CatchBatch catchBatch = load(CatchBatchImpl.class, bean.getIdAsInt());
        if (catchBatch == null) {
            throw new DataRetrievalFailureException("Could not retrieve catch batch with id=" + bean.getId());
        }

        beanToEntity(bean, catchBatch);
        batchHelper.update(catchBatch);
        getCurrentSession().flush();

        // bean is dirty
        synchronizationStatusHelper.setDirty(bean);
        return bean;
    }

    @Override
    public void deleteCatchBatch(Integer fishingOperationId) {
        Preconditions.checkNotNull(fishingOperationId);
        Integer catchBatchId = batchHelper.getCatchBatchIdByFishingOperationId(fishingOperationId);

        if (catchBatchId == null) {
            throw new DataRetrievalFailureException("Could not retrieve catch batch for fishingOperation id=" + fishingOperationId);
        }

        batchHelper.deleteCatchBatch(fishingOperationId, catchBatchId);
    }

    @Override
    public void recomputeCatchBatchSampleRatios(Integer fishingOperationId) {

        Preconditions.checkNotNull(fishingOperationId);
        Integer catchBatchId = batchHelper.getCatchBatchIdByFishingOperationId(fishingOperationId);

        if (catchBatchId == null) {
            throw new DataRetrievalFailureException("Could not retrieve catch batch for fishingOperation id=" + fishingOperationId);
        }

        fr.ifremer.adagio.core.dao.data.batch.CatchBatch catchBatch = batchTreeHelper.loadCatchBatch(catchBatchId);

        // To store computed indirect weights by batch
        // If a batch has a weight before sampling then nothing is computed
        Map<Integer, SortingBatch> indirectWeightByBatch = new TreeMap<>();

        // To store quantification measurement ids which need to update to pass
        // is_reference_quantification to false
        // such measurements are only on batch nodes (leaf always keep the value to true)
        Set<QuantificationMeasurement> quantificationMeasurements = new HashSet<>();

        Set<Integer> indirectWeightByBatchSkip = new HashSet<>();

        for (Batch rootBatch : catchBatch.getChildBatchs()) {

            computeIndirectWeight(rootBatch,
                                  indirectWeightByBatch,
                                  indirectWeightByBatchSkip,
                                  quantificationMeasurements);

        }

        if (!indirectWeightByBatch.isEmpty()) {

            // update batchs sample ratio
            for (SortingBatch batch : indirectWeightByBatch.values()) {

                Float weight = batch.getWeight();
                Float indirectWeight = batch.getIndirectWeight();
                batch.setIndirectWeight(null);

                if (indirectWeight == null) {

                    // will reuse then the weight
                    indirectWeight = weight;
                }

                if (log.isInfoEnabled()) {
                    log.info(String.format("setWeightAndSampleRatio :: %d (%s // %s)", batch.getId(), weight, indirectWeight));
                }

                SortingBatch sortingBatch = load(SortingBatchImpl.class, batch.getId());
                batchTreeHelper.setSortingSamplingRatio(sortingBatch, indirectWeight, weight);

            }

        }
        if (!quantificationMeasurements.isEmpty()) {

            // remove measurements
            if (log.isInfoEnabled()) {
                log.info("updateQuantificationMeasurementsForBatchNodes :: " + quantificationMeasurements);
            }

            for (QuantificationMeasurement quantificationMeasurement : quantificationMeasurements) {

                if (log.isInfoEnabled()) {
                    log.info(String.format("Remove quantification measurement %d on batch %d (batch is not a leaf)", quantificationMeasurement.getId(), quantificationMeasurement.getBatch().getId()));
                }
                Batch batch = quantificationMeasurement.getBatch();
                SortingBatch sortingBatch = load(SortingBatchImpl.class, batch.getId());
                measurementPersistenceHelper.removeWeightMeasurementQuantificationMeasurement(sortingBatch, quantificationMeasurement);
            }

        }

    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void entityToBean(fr.ifremer.adagio.core.dao.data.batch.CatchBatch source, CatchBatch target) {

        target.setId(source.getId());
        target.setCatchTotalWeight(source.getWeight());
        target.setSynchronizationStatus(source.getSynchronizationStatus());

        // -- Vrac
        SortingBatch vracBatch = batchTreeHelper.getVracBatch(source);

        if (vracBatch != null) {

            target.setCatchTotalSortedCarousselWeight(vracBatch.getWeight());
            target.setCatchTotalSortedTremisWeight(vracBatch.getWeightBeforeSampling());

            // -- Vrac > Species
            SortingBatch vracSpeciesBatch = batchTreeHelper.getSpeciesVracRootBatch(vracBatch);

            if (vracSpeciesBatch != null) {

                target.setSpeciesTotalSortedWeight(vracSpeciesBatch.getWeight());

                // -- Vrac > Species > Inert
                SortingBatch inertBatch = batchTreeHelper.getSpeciesVracInertRootBatch(vracSpeciesBatch);

                if (inertBatch != null) {
                    target.setSpeciesTotalInertWeight(inertBatch.getWeight());
                }

                // -- Vrac > Species > Alive not itemized
                SortingBatch livingNotItemizedBatch = batchTreeHelper.getSpeciesVracAliveNotItemizedRootBatch(vracSpeciesBatch);

                if (livingNotItemizedBatch != null) {
                    target.setSpeciesTotalLivingNotItemizedWeight(livingNotItemizedBatch.getWeight());
                }
            }

            // -- Vrac > Benthos
            SortingBatch vracBenthosBatch = batchTreeHelper.getBenthosVracRootBatch(vracBatch);

            if (vracBenthosBatch != null) {
                target.setBenthosTotalSortedWeight(vracBenthosBatch.getWeight());

                // -- Vrac > Benthos > Inert
                SortingBatch inertBatch = batchTreeHelper.getBenthosVracInertRootBatch(vracBenthosBatch);

                if (inertBatch != null) {
                    target.setBenthosTotalInertWeight(inertBatch.getWeight());
                }

                // -- Vrac > Benthos > Alive no itemized
                SortingBatch livingNotItemizedBatch = batchTreeHelper.getBenthosVracAliveNotItemizedRootBatch(vracBenthosBatch);

                if (livingNotItemizedBatch != null) {
                    target.setBenthosTotalLivingNotItemizedWeight(livingNotItemizedBatch.getWeight());
                }
            }
        }

        // -- Hors Vrac
        SortingBatch horsVracBatch = batchTreeHelper.getHorsVracBatch(source);

        if (horsVracBatch != null) {

            // -- Hors Vrac > Species
            batchTreeHelper.getSpeciesHorsVracRootBatch(horsVracBatch);

            // -- Hors Vrac > Benthos
            batchTreeHelper.getBenthosHorsVracRootBatch(horsVracBatch);

            // -- Hors Vrac > MarineLitter
            SortingBatch marineLitterBatch = batchTreeHelper.getMarineLitterRootBatch(horsVracBatch);

            if (marineLitterBatch != null) {
                target.setMarineLitterTotalWeight(marineLitterBatch.getWeight());
            }
        }

        // -- Unsorted
        SortingBatch unsortedBatch = batchTreeHelper.getRejectedBatch(source);

        if (unsortedBatch != null) {
            target.setCatchTotalRejectedWeight(unsortedBatch.getWeight());
        }
    }

    protected void beanToEntity(CatchBatch source,
                                fr.ifremer.adagio.core.dao.data.batch.CatchBatch target) {
        Preconditions.checkNotNull(source.getFishingOperation());
        Preconditions.checkNotNull(source.getFishingOperation().getId());

        // First initialization (when created)
        Integer fishingOperationId = source.getFishingOperation().getIdAsInt();
        target.setFishingOperation(load(FishingOperationImpl.class, fishingOperationId));

        // Quality flag
        target.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));

        // Rank order
        target.setRankOrder((short) 1);

        // Synchronization status
        synchronizationStatusHelper.setDirty(target);

        // Total Weight
        if (source.getCatchTotalWeight() == null) {

            QuantificationMeasurement quantificationMeasurement = measurementPersistenceHelper.getWeightMeasurementQuantificationMeasurement(target);
            if (quantificationMeasurement != null) {

                // remove it
                measurementPersistenceHelper.removeWeightMeasurementQuantificationMeasurement(target, quantificationMeasurement);
            }
        } else {

            measurementPersistenceHelper.setWeightMeasurementQuantificationMeasurement(
                    target,
                    source.getCatchTotalWeight());
        }

        SortingBatch vracBatch = batchTreeHelper.getVracBatch(target);

        SortingBatch horsVracBatch = batchTreeHelper.getHorsVracBatch(target);

        SortingBatch horsVracSpeciesRootBatch = null;
        SortingBatch horsVracBenthosRootBatch = null;
        SortingBatch horsVracMarineLitterRootBatch = null;

        if (horsVracBatch != null) {

            horsVracSpeciesRootBatch = batchTreeHelper.getSpeciesHorsVracRootBatch(horsVracBatch);
            horsVracBenthosRootBatch = batchTreeHelper.getBenthosHorsVracRootBatch(horsVracBatch);
            horsVracMarineLitterRootBatch = batchTreeHelper.getMarineLitterRootBatch(horsVracBatch);
        }

        SortingBatch speciesVracBatch = null;
        SortingBatch benthosVracBatch = null;

        SortingBatch speciesVracAliveNotItemizeRootBatch = null;
        SortingBatch speciesVracInertRootBatch = null;
        SortingBatch speciesVracAliveItemizeRootBatch = null;
        SortingBatch benthosVracAliveNotItemizeRootBatch = null;
        SortingBatch benthosVracInertRootBatch = null;
        SortingBatch benthosVracAliveItemizeRootBatch = null;

        if (vracBatch != null) {
            speciesVracBatch = batchTreeHelper.getSpeciesVracRootBatch(vracBatch);

            if (speciesVracBatch != null) {
                speciesVracAliveNotItemizeRootBatch = batchTreeHelper.getSpeciesVracAliveNotItemizedRootBatch(speciesVracBatch);
                speciesVracInertRootBatch = batchTreeHelper.getSpeciesVracInertRootBatch(speciesVracBatch);
                speciesVracAliveItemizeRootBatch = batchTreeHelper.getSpeciesVracAliveItemizedRootBatch(speciesVracBatch);
            }

            benthosVracBatch = batchTreeHelper.getBenthosVracRootBatch(vracBatch);
            if (benthosVracBatch != null) {
                benthosVracAliveNotItemizeRootBatch = batchTreeHelper.getBenthosVracAliveNotItemizedRootBatch(benthosVracBatch);
                benthosVracInertRootBatch = batchTreeHelper.getBenthosVracInertRootBatch(benthosVracBatch);
                benthosVracAliveItemizeRootBatch = batchTreeHelper.getBenthosVracAliveItemizedRootBatch(benthosVracBatch);
            }
        }

        boolean needVracSpeciesAliveNotItemized = speciesVracAliveNotItemizeRootBatch != null ||
                                                  source.getSpeciesTotalLivingNotItemizedWeight() != null;
        boolean needVracSpeciesInert = speciesVracInertRootBatch != null ||
                                       source.getSpeciesTotalInertWeight() != null;
        boolean needVracSpeciesAliveItemized = speciesVracAliveItemizeRootBatch != null;

        boolean needVracSpecies = speciesVracBatch != null ||
                                  source.getSpeciesTotalSortedWeight() != null ||
                                  needVracSpeciesAliveNotItemized ||
                                  needVracSpeciesInert ||
                                  needVracSpeciesAliveItemized;

        boolean needVracBenthosAliveNotItemized = benthosVracAliveNotItemizeRootBatch != null ||
                                                  source.getBenthosTotalLivingNotItemizedWeight() != null;
        boolean needVracBenthosInert = benthosVracInertRootBatch != null ||
                                       source.getBenthosTotalInertWeight() != null;
        boolean needVracBenthosAliveItemized = benthosVracAliveItemizeRootBatch != null;

        boolean needVracBenthos = benthosVracBatch != null ||
                                  source.getBenthosTotalSortedWeight() != null ||
                                  needVracBenthosAliveNotItemized ||
                                  needVracBenthosInert ||
                                  needVracBenthosAliveItemized;

        boolean needVrac = vracBatch != null ||
                           source.getCatchTotalSortedCarousselWeight() != null ||
                           source.getCatchTotalSortedTremisWeight() != null ||
                           needVracSpecies ||
                           needVracBenthos;

        boolean needHorsVracSpecies = horsVracSpeciesRootBatch != null;
        boolean needHorsVracBenthos = horsVracBenthosRootBatch != null;
        boolean needHorsVracMarineLitter = horsVracMarineLitterRootBatch != null ||
                                           source.getMarineLitterTotalWeight() != null;

        boolean needHorsVrac = needHorsVracSpecies ||
                               needHorsVracBenthos ||
                               needHorsVracMarineLitter;

        boolean needUnsorted = batchTreeHelper.getRejectedBatch(target) != null ||
                               source.getCatchTotalRejectedWeight() != null;

        if (needVrac) {
            // -- Vrac
            vracBatch = batchTreeHelper.getOrCreateVracBatch(
                    target,
                    source.getCatchTotalSortedCarousselWeight(),
                    source.getCatchTotalSortedTremisWeight());

            if (needVracSpecies) {
                // -- Vrac > Species
                speciesVracBatch = batchTreeHelper.getOrCreateSpeciesVracRootBatch(
                        target,
                        vracBatch,
                        source.getSpeciesTotalSortedWeight());

                if (needVracSpeciesAliveNotItemized) {
                    // -- Vrac > Species > Alive not itemized
                    batchTreeHelper.getOrCreateSpeciesVracAliveNotItemizedRootBatch(
                            target,
                            speciesVracBatch,
                            source.getSpeciesTotalLivingNotItemizedWeight());
                }

                if (needVracSpeciesInert) {
                    // -- Vrac > Species > Inert (not alive)
                    batchTreeHelper.getOrCreateSpeciesVracInertRootBatch(
                            target,
                            speciesVracBatch,
                            source.getSpeciesTotalInertWeight());
                }

                if (needVracSpeciesAliveItemized) {
                    // -- Vrac > Species > Alive itemized
                    batchTreeHelper.getOrCreateSpeciesVracAliveItemizedRootBatch(
                            target,
                            speciesVracBatch);
                }
            }

            if (needVracBenthos) {
                // -- Vrac > Benthos
                benthosVracBatch = batchTreeHelper.getOrCreateBenthosVracRootBatch(
                        target,
                        vracBatch,
                        source.getBenthosTotalSortedWeight());

                if (needVracBenthosAliveNotItemized) {
                    // -- Vrac > Benthos > Alive not itemized
                    batchTreeHelper.getOrCreateBenthosVracAliveNotItemizedRootBatch(
                            target,
                            benthosVracBatch,
                            source.getBenthosTotalLivingNotItemizedWeight());
                }

                if (needVracBenthosInert) {
                    // -- Vrac > Benthos > Inert (not alive)
                    batchTreeHelper.getOrCreateBenthosVracInertRootBatch(
                            target,
                            benthosVracBatch,
                            source.getBenthosTotalInertWeight());
                }

                if (needVracBenthosAliveItemized) {
                    // -- Vrac > Benthos > Alive itemized
                    batchTreeHelper.getOrCreateBenthosVracAliveItemizedRootBatch(
                            target,
                            benthosVracBatch);
                }
            }

        }

        if (needHorsVrac) {
            // -- Hors Vrac
            horsVracBatch = batchTreeHelper.getOrCreateHorsVracBatch(target);

            if (needHorsVracSpecies) {
                // -- Hors Vrac > Species
                batchTreeHelper.getOrCreateSpeciesHorsVracRootBatch(
                        target,
                        horsVracBatch);
            }

            if (needHorsVracBenthos) {
                // -- Hors Vrac > Benthos
                batchTreeHelper.getOrCreateBenthosHorsVracRootBatch(
                        target,
                        horsVracBatch);
            }

            if (needHorsVracMarineLitter) {
                // -- Hors Vrac > MarineLitter
                batchTreeHelper.getOrCreateMarineLitterRootBatch(
                        target,
                        horsVracBatch,
                        source.getMarineLitterTotalWeight());
            }
        }

        if (needUnsorted) {
            // -- Unsorted (=rejected)
            batchTreeHelper.getOrCreateRejectedBatch(
                    target, source.getCatchTotalRejectedWeight());
        }
    }

    protected void computeIndirectWeight(Batch batch,
                                         Map<Integer, SortingBatch> indirectWeightByBatch,
                                         Set<Integer> indirectWeightByBatchSkip,
                                         Set<QuantificationMeasurement> quantificationMeasurements) {

        Integer batchId = batch.getId();

        if (indirectWeightByBatch.containsKey(batchId) ||
            indirectWeightByBatchSkip.contains(batchId)) {

            // already computed
            return;

        }

        Collection<Batch> childBatchs = batch.getChildBatchs();
        boolean batchIsLeaf = CollectionUtils.isEmpty(childBatchs);

        if (batchIsLeaf) {

            // on a leaf, nothing else to do
            if (log.isDebugEnabled()) {
                log.debug("[BATCH :: " + batchId + "] - Do not compute on a leaf ");
            }
            return;

        }

        QuantificationMeasurement measurement = measurementPersistenceHelper.getWeightMeasurementQuantificationMeasurement(batch);
        if (measurement != null) {

            // need to remove the quantification measurement
            quantificationMeasurements.add(measurement);

            if (log.isDebugEnabled()) {
                log.debug("[BATCH :: " + batchId + "] - Need to remove the quantification measurement (not a leaf) :: " + measurement.getId());
            }

        }

        // compute indirect weights of childs before all

        for (Batch childBatch : childBatchs) {

            computeIndirectWeight(childBatch, indirectWeightByBatch, indirectWeightByBatchSkip, quantificationMeasurements);

        }

        if (batch.getWeightBeforeSampling() != null) {

            // no need to compute indirect weight, mark batch
            indirectWeightByBatchSkip.add(batchId);

            if (log.isDebugEnabled()) {
                log.debug("[BATCH :: " + batchId + "] - No need of indirect weight (there is a weight before sampling) ");
            }
            return;

        }

        // At last, compute the indirect weight
        Float indirectWeight = computeIndirectWeight(batch);
        batch.setIndirectWeight(indirectWeight);

        if (batch.getWeight() != null) {

            // Can only update the sample ratio if there is a weight on the batch
            indirectWeightByBatch.put(batchId, (SortingBatch) batch);

        }

        if (log.isDebugEnabled()) {
            log.debug("[BATCH :: " + batchId + "] - " + "Computed indirect weight " + indirectWeight);
        }

    }

    protected Float computeIndirectWeight(Batch batch) {

        if (log.isDebugEnabled()) {
            log.debug("[BATCH :: " + batch.getId() + "] compute indirect weight");
        }

        BigDecimal result = new BigDecimal(0);

        for (Batch childBatch : batch.getChildBatchs()) {

            Float weight = Numbers.getValueOrComputedValue(childBatch.getWeightBeforeSampling(),
                                                           childBatch.getWeight());

            if (weight == null) {

                //  at last use the indirectWeight
                weight = childBatch.getIndirectWeight();

            }

            if (weight == null) {

                if (log.isDebugEnabled()) {
                    log.debug("[BATCH :: " + batch.getId() + "] Found a child batch " + childBatch.getId() + " has no weight, skip computing indirect weight for batch :: ...");
                }

                return null;

            }
            result = result.add(new BigDecimal(String.valueOf(weight)));

        }

        return WeightUnit.KG.round(result.floatValue());

    }

}
