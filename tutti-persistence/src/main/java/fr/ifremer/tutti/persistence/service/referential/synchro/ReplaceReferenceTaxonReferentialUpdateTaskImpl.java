package fr.ifremer.tutti.persistence.service.referential.synchro;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.2
 */
public class ReplaceReferenceTaxonReferentialUpdateTaskImpl implements ReferentialUpdateTask {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ReplaceReferenceTaxonReferentialUpdateTaskImpl.class);

    public static final String GET_REFTAX_TO_REPLACE_QUERY = "SELECT object_id, external_code " +
                                                             "FROM transcribing_item ti " +
                                                                "JOIN transcribing_item_type tit " +
                                                                    "ON ti.transcribing_item_type_fk = tit.id " +
                                                                        "AND tit.label = 'TAXINOMIE-COMMUN.REFERENCE_HISTORY' " +
                                                             "WHERE (update_date IS NULL OR update_date > ?)";

    public static final String REPLACE_REFTAX_IN_BATCH_QUERY = "UPDATE batch SET reference_taxon_fk = ? " +
                                                               "WHERE reference_taxon_fk = ?";

    public static final String REPLACE_REFTAX_IN_SAMPLE_QUERY = "UPDATE sample SET reference_taxon_fk = ? " +
                                                               "WHERE reference_taxon_fk = ?";

    @Override
    public String getTable() {
        return "TRANSCRIBING_ITEM";
    }

    @Override
    public void update(Connection localConnection,
                       Timestamp lastUpdate) throws SQLException {


        PreparedStatement preparedStatement = localConnection.prepareStatement(GET_REFTAX_TO_REPLACE_QUERY);
        preparedStatement.setTimestamp(1, lastUpdate);
        ResultSet reftaxToReplace = preparedStatement.executeQuery();

        PreparedStatement replaceReftaxInBatchStatement = localConnection.prepareStatement(REPLACE_REFTAX_IN_BATCH_QUERY);
        PreparedStatement replaceReftaxInSampleStatement = localConnection.prepareStatement(REPLACE_REFTAX_IN_SAMPLE_QUERY);

        while (reftaxToReplace.next()) {

            Integer newRefTax = reftaxToReplace.getInt(1);
            Integer oldRefTax = reftaxToReplace.getInt(2);

            if (log.isInfoEnabled()) {
                log.info(String.format("[%s] Remplacement du taxon %s par le taxon %s", getTable(), oldRefTax, newRefTax));
            }

            replaceReftaxInBatchStatement.setInt(1, newRefTax);
            replaceReftaxInBatchStatement.setInt(2, oldRefTax);
            int batchUpdated = replaceReftaxInBatchStatement.executeUpdate();

            if (log.isInfoEnabled()) {
                log.info(String.format("[%s] %s batchs mis à jour", getTable(), batchUpdated));
            }

            replaceReftaxInSampleStatement.setInt(1, newRefTax);
            replaceReftaxInSampleStatement.setInt(2, oldRefTax);
            int sampleUpdated = replaceReftaxInSampleStatement.executeUpdate();

            if (log.isInfoEnabled()) {
                log.info(String.format("[%s] %s samples mis à jour", getTable(), sampleUpdated));
            }
        }

    }
}
