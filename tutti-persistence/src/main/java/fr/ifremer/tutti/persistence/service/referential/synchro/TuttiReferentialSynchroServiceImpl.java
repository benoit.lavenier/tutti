package fr.ifremer.tutti.persistence.service.referential.synchro;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroDatabaseMetadata;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroResult;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroServiceImpl;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroTableMetadata;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroTableTool;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.2
 */
@Service("referentialSynchroServiceTutti")
@Lazy
public class TuttiReferentialSynchroServiceImpl extends ReferentialSynchroServiceImpl {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(TuttiReferentialSynchroServiceImpl.class);

    protected Map<String, ReferentialUpdateTask> updateTasks;

    protected Map<String, ReferentialUpdateTask> getUpdateTasks() {
        if (updateTasks == null) {

            updateTasks = Maps.newHashMap();

            ServiceLoader<ReferentialUpdateTask> loader = ServiceLoader.load(ReferentialUpdateTask.class);
            for (ReferentialUpdateTask task : loader) {
                updateTasks.put(task.getTable(), task);
            }

        }
        return updateTasks;
    }

    protected void synchronizeTable(ReferentialSynchroDatabaseMetadata dbMetas,
                                    ReferentialSynchroTableMetadata table,
                                    Connection localConnection,
                                    Connection remoteConnection,
                                    ReferentialSynchroResult result) throws SQLException {

        Timestamp lastUpdate;

        log.info("Synchronize table " + table);

        if (table.getName().toLowerCase().equals("transcribing_item")) {

            // Specific synchronize logic

            ReferentialSynchroTableTool localDao = new ReferentialSynchroTableTool(localConnection, table);
            ReferentialSynchroTableTool remoteDao = new ReferentialSynchroTableTool(remoteConnection, table);

            // get all data to insert from remote db
            ResultSet dataToUpdate = remoteDao.getDataToUpdate(null);

            String tableName = localDao.getTable().getName();

            result.addTableName(tableName);

            String tablePrefix = table.getTableLogPrefix() + " - " + result.getNbRows(tableName);

            // delete table
            localDao.deleteAll();

            int countR = 0;

            // add all data from remote
            while (dataToUpdate.next()) {

                List<Object> pk = table.getPk(dataToUpdate);

                localDao.executeInsert(pk, dataToUpdate);

                countR++;

                reportProgress(result, localDao, countR, tablePrefix);
            }

            // No data in local
            lastUpdate = new Timestamp(0);

        } else {

            lastUpdate = getLastUpdateDate(localConnection, table);

            super.synchronizeTable(dbMetas, table, localConnection, remoteConnection, result);

        }

        String tableName = table.getName();

        ReferentialUpdateTask updateTask = getUpdateTasks().get(tableName);

        if (updateTask != null) {
            if (log.isInfoEnabled()) {
                log.info(table.getTableLogPrefix() + " - " + result.getNbRows(tableName) + " Will use specific update task: " + updateTask);
            }
            updateTask.update(localConnection, lastUpdate);
        }

    }

    /**
     * Gets the last updateDate for the given {@code table} using
     * the given datasource.
     *
     * @param connection incoming sql connection
     * @param table      the table to test
     * @return the last update date of the given table, or {@code null} if table does not use a updateDate columns or if
     * there is no data in table.
     * @throws SQLException if any sql problem
     */
    protected Timestamp getLastUpdateDate(Connection connection, ReferentialSynchroTableMetadata table) throws SQLException {
        Timestamp result = null;

        if (table.isWithUpdateDateColumn()) {

            String sql = table.getSelectMaxUpdateDateQuery();

            PreparedStatement statement = connection.prepareStatement(sql);
            try {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    result = resultSet.getTimestamp(1);
                }
                statement.close();
            } finally {
                closeSilently(statement);
            }
        }
        return result;
    }

    protected void closeSilently(Statement statement) {
        try {
            if (statement != null && !statement.isClosed()) {

                statement.close();
            }
        } catch (AbstractMethodError e) {
            if (log.isDebugEnabled()) {
                log.debug("Fix this linkage error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (IllegalAccessError e) {
            if (log.isDebugEnabled()) {
                log.debug("Fix this IllegalAccessError error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not close statement, but do not care", e);
            }
        }
    }
}
