package fr.ifremer.tutti.persistence.dao;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.data.survey.fishingTrip.FishingTrip;
import fr.ifremer.adagio.core.dao.data.vessel.feature.physical.GearPhysicalFeatures;
import fr.ifremer.adagio.core.dao.data.vessel.feature.physical.GearPhysicalFeaturesDaoImpl;
import fr.ifremer.adagio.core.dao.referential.gear.Gear;
import fr.ifremer.adagio.core.dao.referential.gear.GearImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.Objects;

/**
 * Created on 9/28/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
@Repository("gearPhysicalFeaturesDaoTutti")
@Lazy
public class GearPhysicalFeaturesDaoImplTutti extends GearPhysicalFeaturesDaoImpl implements GearPhysicalFeaturesDaoTutti {

    @Autowired
    public GearPhysicalFeaturesDaoImplTutti(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public GearPhysicalFeatures getGearPhysicalfeatures(FishingTrip fishingTrip,
                                                        Integer gearId,
                                                        Short rankOrder,
                                                        boolean createIfNotExists) {

        if (rankOrder == null) {
            rankOrder = 0;
        }
        // Retrieve entities : Gear Physical Features
        if (fishingTrip.getGearPhysicalFeatures() != null && fishingTrip.getGearPhysicalFeatures().size() >= 0) {
            for (GearPhysicalFeatures guf : fishingTrip.getGearPhysicalFeatures()) {
                if (Objects.equals(rankOrder, guf.getRankOrder()) &&
                    Objects.equals(gearId, guf.getGear().getId())) {
                    return guf;
                }
            }
        }
        if (!createIfNotExists) {
            return null;
        }

        GearPhysicalFeatures gearPhysicalFeature = GearPhysicalFeatures.Factory.newInstance();
        gearPhysicalFeature.setFishingTrip(fishingTrip);
        gearPhysicalFeature.setRankOrder(rankOrder);

        Gear gear = load(GearImpl.class, gearId);
        gearPhysicalFeature.setGear(gear);
        if (fishingTrip.getGearPhysicalFeatures() == null) {
            fishingTrip.setGearPhysicalFeatures(Lists.newArrayList(gearPhysicalFeature));
        } else {
            fishingTrip.getGearPhysicalFeatures().add(gearPhysicalFeature);
        }

        return gearPhysicalFeature;
    }
}
