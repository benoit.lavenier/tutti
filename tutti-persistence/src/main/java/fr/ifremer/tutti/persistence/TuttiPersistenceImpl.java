package fr.ifremer.tutti.persistence;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.ObjectType;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.OperationDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.persistence.service.AccidentalBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.AttachmentPersistenceService;
import fr.ifremer.tutti.persistence.service.BenthosBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.CatchBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.CruisePersistenceService;
import fr.ifremer.tutti.persistence.service.FishingOperationPersistenceService;
import fr.ifremer.tutti.persistence.service.IndividualObservationBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.MarineLitterBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.ProgramPersistenceService;
import fr.ifremer.tutti.persistence.service.ProtocolPersistenceService;
import fr.ifremer.tutti.persistence.service.SpeciesBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.TechnicalPersistenceService;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import fr.ifremer.tutti.persistence.service.UpdateSchemaContextSupport;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.ObjectTypePersistenceService;
import fr.ifremer.tutti.persistence.service.referential.PersonPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.VesselPersistenceService;
import fr.ifremer.tutti.util.Jdbcs;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class TuttiPersistenceImpl implements TuttiPersistence {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(TuttiPersistenceImpl.class);

    @Resource(name = "caracteristicPersistenceService")
    protected CaracteristicPersistenceService caracteristicService;

    @Resource(name = "gearPersistenceService")
    protected GearPersistenceService gearService;

    @Resource(name = "locationPersistenceService")
    protected LocationPersistenceService locationService;

    @Resource(name = "objectTypePersistenceService")
    protected ObjectTypePersistenceService objectTypeService;

    @Resource(name = "personPersistenceService")
    protected PersonPersistenceService personService;

    @Resource(name = "speciesPersistenceService")
    protected SpeciesPersistenceService speciesService;

    @Resource(name = "vesselPersistenceService")
    protected VesselPersistenceService vesselService;

    @Resource(name = "programPersistenceService")
    protected ProgramPersistenceService programService;

    @Resource(name = "cruisePersistenceService")
    protected CruisePersistenceService cruiseService;

    @Resource(name = "fishingOperationPersistenceService")
    protected FishingOperationPersistenceService fishingOperationService;

    @Resource(name = "batchPersistenceService")
    protected CatchBatchPersistenceService catchBatchService;

    @Resource(name = "speciesBatchPersistenceService")
    protected SpeciesBatchPersistenceService speciesBatchService;

    @Resource(name = "benthosBatchPersistenceService")
    protected BenthosBatchPersistenceService benthosBatchService;

    @Resource(name = "marineLitterBatchPersistenceService")
    protected MarineLitterBatchPersistenceService marineLitterBatchService;

    @Resource(name = "accidentalBatchPersistenceService")
    protected AccidentalBatchPersistenceService accidentalBatchService;

    @Resource(name = "individualObservationBatchPersistenceService")
    protected IndividualObservationBatchPersistenceService individualObservationBatchService;

    @Resource(name = "protocolPersistenceService")
    protected ProtocolPersistenceService protocolService;

    @Resource(name = "attachmentPersistenceService")
    protected AttachmentPersistenceService attachmentService;

    @Resource(name = "technicalPersistenceService")
    protected TechnicalPersistenceService technicalPersistenceService;

    private boolean skipShutdownDbWhenClosing;

    @Override
    public String getImplementationName() {
        return "Persistence Adagio implementation";
    }

    @Override
    public void setSkipShutdownDbWhenClosing() {
        this.skipShutdownDbWhenClosing = true;
    }

    @Override
    public void lazyInit() {
        // this service does not used lazy init
    }

    @Override
    public void clearAllCaches() {
        getTechnicalPersistenceService().clearAllCaches();
    }

    @Override
    public <V> V invoke(Callable<V> call) {
        return getTechnicalPersistenceService().invoke(call);
    }

    @Override
    public <U extends UpdateSchemaContextSupport> void prepareUpdateSchemaContext(U context) {
        getTechnicalPersistenceService().prepareUpdateSchemaContext(context);
    }

    @Override
    public Version getSchemaVersion() {
        return getTechnicalPersistenceService().getSchemaVersion();
    }

    @Override
    public Version getSchemaVersionIfUpdate() {
        return getTechnicalPersistenceService().getSchemaVersionIfUpdate();
    }

    @Override
    public void updateSchema() {
        getTechnicalPersistenceService().updateSchema();
    }

    @Override
    public void sanityDb() {
        getTechnicalPersistenceService().sanityDb();
    }

    @Override
    public boolean isTemporary(TuttiReferentialEntity entity) {
        return getTechnicalPersistenceService().isTemporary(entity);
    }

    @Override
    public void init() {
        if (log.isInfoEnabled()) {
            log.info("Open persistence driver " + getImplementationName());
        }

    }

    protected boolean close;

    @Override
    public void close() throws IOException {

        if (!close) {

            close = true;
            synchronized (this) {
                if (log.isInfoEnabled()) {
                    log.info("Close persistence driver " + getImplementationName());
                }

                caracteristicService.close();
                gearService.close();
                locationService.close();
                objectTypeService.close();
                personService.close();
                speciesService.close();
                vesselService.close();

                programService.close();
                cruiseService.close();
                fishingOperationService.close();
                catchBatchService.close();
                speciesBatchService.close();
                benthosBatchService.close();
                marineLitterBatchService.close();
                accidentalBatchService.close();
                individualObservationBatchService.close();
                protocolService.close();
                attachmentService.close();

                if (skipShutdownDbWhenClosing) {

                    if (log.isInfoEnabled()) {
                        log.info("Skip to shutdown db while closing, lucky you...");
                    }

                } else {

                    if (log.isInfoEnabled()) {
                        log.info("Do shutdown db nicely");
                    }
                    try {
                        Jdbcs.shutdown(TuttiConfiguration.getInstance());
                    } catch (SQLException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Could not shutdown the database", e);
                        }
                    }

                }


                TuttiPersistenceServiceLocator.shutdownTutti();
            }
        }
    }

    @Override
    public ProgramDataModel loadProgram(String programId, boolean loadFishingOperation) {

        Program program = getProgram(programId);

        Set<CruiseDataModel> cruiseModels = new LinkedHashSet<>();
        List<Cruise> cruises = new ArrayList<>(getAllCruise(programId));
        Cruises.sort(cruises);
        for (Cruise cruise : cruises) {
            List<Integer> fishingOperationIds = loadFishingOperation ? getAllFishingOperationIds(cruise.getIdAsInt()) : Collections.<Integer>emptyList();
            CruiseDataModel model = loadCruise(cruise, fishingOperationIds);
            cruiseModels.add(model);
        }

        return new ProgramDataModel(program, cruiseModels);

    }

    @Override
    public ProgramDataModel loadCruises(String programId, boolean loadFishingOperation, Integer... cruiseIds) {

        Program program = getProgram(programId);

        Set<CruiseDataModel> cruiseModels = new LinkedHashSet<>();
        for (Integer cruiseId : cruiseIds) {
            Cruise cruise = getCruise(cruiseId);
            List<Integer> fishingOperationIds = loadFishingOperation ? getAllFishingOperationIds(cruise.getIdAsInt()) : Collections.<Integer>emptyList();
            CruiseDataModel model = loadCruise(cruise, fishingOperationIds);
            cruiseModels.add(model);
        }

        return new ProgramDataModel(program, cruiseModels);

    }

    @Override
    public ProgramDataModel loadCruise(String programId, Integer cruiseId, Integer... fishingOperationIds) {

        Program program = getProgram(programId);

        Set<CruiseDataModel> cruiseModels = new LinkedHashSet<>();
        Cruise cruise = getCruise(cruiseId);
        CruiseDataModel model = loadCruise(cruise, Arrays.asList(fishingOperationIds));
        cruiseModels.add(model);

        return new ProgramDataModel(program, cruiseModels);

    }

    protected CruiseDataModel loadCruise(Cruise cruise, List<Integer> fishingOperationIds) {

        Set<OperationDataModel> fishingOperationModels = new LinkedHashSet<>();

        List<FishingOperation> fishingOperations = new ArrayList<>(getAllFishingOperation(cruise.getIdAsInt()));
        FishingOperations.sort(fishingOperations);
        fishingOperations.stream().filter(fishingOperation -> fishingOperationIds.contains(fishingOperation.getIdAsInt())).forEach(fishingOperation -> {
            OperationDataModel model = new OperationDataModel(fishingOperation);
            fishingOperationModels.add(model);
        });

        return new CruiseDataModel(cruise, fishingOperationModels);

    }

    //------------------------------------------------------------------------//
    //-- CaracteristicPersistenceService methods                            --//
    //------------------------------------------------------------------------//

    @Override
    public List<Caracteristic> getAllCaracteristic() {
        return getCaracteristicService().getAllCaracteristic();
    }

    @Override
    public List<Caracteristic> getAllCaracteristicWithProtected() {
        return getCaracteristicService().getAllCaracteristicWithProtected();
    }

    @Override
    public List<Caracteristic> getAllCaracteristicForSampleCategory() {
        return getCaracteristicService().getAllCaracteristicForSampleCategory();
    }

    @Override
    public List<Caracteristic> getAllNumericCaracteristic() {
        return getCaracteristicService().getAllNumericCaracteristic();
    }

    @Override
    public Caracteristic getSizeCategoryCaracteristic() {
        return getCaracteristicService().getSizeCategoryCaracteristic();
    }

    @Override
    public Caracteristic getSexCaracteristic() {
        return getCaracteristicService().getSexCaracteristic();
    }

    @Override
    public Caracteristic getSortedUnsortedCaracteristic() {
        return getCaracteristicService().getSortedUnsortedCaracteristic();
    }

    @Override
    public Caracteristic getMaturityCaracteristic() {
        return getCaracteristicService().getMaturityCaracteristic();
    }

    @Override
    public Caracteristic getAgeCaracteristic() {
        return getCaracteristicService().getAgeCaracteristic();
    }

    @Override
    public Caracteristic getMarineLitterCategoryCaracteristic() {
        return getCaracteristicService().getMarineLitterCategoryCaracteristic();
    }

    @Override
    public Caracteristic getMarineLitterSizeCategoryCaracteristic() {
        return getCaracteristicService().getMarineLitterSizeCategoryCaracteristic();
    }

    @Override
    public Caracteristic getVerticalOpeningCaracteristic() {
        return getCaracteristicService().getVerticalOpeningCaracteristic();
    }

    @Override
    public Caracteristic getHorizontalOpeningWingsCaracteristic() {
        return getCaracteristicService().getHorizontalOpeningWingsCaracteristic();
    }

    @Override
    public Caracteristic getHorizontalOpeningDoorCaracteristic() {
        return getCaracteristicService().getHorizontalOpeningDoorCaracteristic();
    }

    @Override
    public Caracteristic getDeadOrAliveCaracteristic() {
        return getCaracteristicService().getDeadOrAliveCaracteristic();
    }

    @Override
    public Caracteristic getCalcifiedStructureCaracteristic() {
        return getCaracteristicService().getCalcifiedStructureCaracteristic();
    }

    @Override
    public Caracteristic getPmfmIdCaracteristic() {
        return getCaracteristicService().getPmfmIdCaracteristic();
    }

    @Override
    public Caracteristic getWeightMeasuredCaracteristic() {
        return getCaracteristicService().getWeightMeasuredCaracteristic();
    }

    @Override
    public Caracteristic getCopyIndividualObservationModeCaracteristic() {
        return getCaracteristicService().getCopyIndividualObservationModeCaracteristic();
    }

    @Override
    public Caracteristic getSampleCodeCaracteristic() {
        return getCaracteristicService().getSampleCodeCaracteristic();
    }

    @Override
    public Caracteristic getCaracteristic(Integer pmfmId) {
        return getCaracteristicService().getCaracteristic(pmfmId);
    }

    @Override
    public boolean isVracBatch(SpeciesBatch speciesBatch) {
        return getCaracteristicService().isVracBatch(speciesBatch);
    }

    @Override
    public Predicate<SpeciesBatch> getVracBatchPredicate() {
        return getCaracteristicService().getVracBatchPredicate();
    }

    @Override
    public boolean isHorsVracBatch(SpeciesBatch speciesBatch) {
        return getCaracteristicService().isHorsVracBatch(speciesBatch);
    }

    //------------------------------------------------------------------------//
    //-- LocationPersistenceService methods                                 --//
    //------------------------------------------------------------------------//

    @Override
    public List<TuttiLocation> getAllProgramZone() {
        return getLocationService().getAllProgramZone();
    }

    @Override
    public List<TuttiLocation> getAllCountry() {
        return getLocationService().getAllCountry();
    }

    @Override
    public List<TuttiLocation> getAllHarbour() {
        return getLocationService().getAllHarbour();
    }

    @Override
    public List<TuttiLocation> getAllHarbourWithObsoletes() {
        return getLocationService().getAllHarbourWithObsoletes();
    }

    @Override
    public ImmutableSet<Integer> getAllFishingOperationStratasAndSubstratasIdsForProgram(String zoneId) {
        return getLocationService().getAllFishingOperationStratasAndSubstratasIdsForProgram(zoneId);
    }

    @Override
    public Multimap<TuttiLocation, TuttiLocation> getAllFishingOperationStratasAndSubstratas(String zoneId) {
        return getLocationService().getAllFishingOperationStratasAndSubstratas(zoneId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrata(String zoneId) {
        return getLocationService().getAllFishingOperationStrata(zoneId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrataWithObsoletes(String zoneId) {
        return getLocationService().getAllFishingOperationStrataWithObsoletes(zoneId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrata(String zoneId,
                                                               String strataId) {
        return getLocationService().getAllFishingOperationSubStrata(zoneId, strataId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrataWithObsoletes(String zoneId, String strataId) {
        return getLocationService().getAllFishingOperationSubStrataWithObsoletes(zoneId, strataId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocation(String zoneId,
                                                              String strataId,
                                                              String subStrataId) {
        return getLocationService().getAllFishingOperationLocation(zoneId,
                                                                   strataId,
                                                                   subStrataId);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocationWithObsoletes(String zoneId, String strataId, String subStrataId) {
        return getLocationService().getAllFishingOperationLocationWithObsoletes(zoneId,
                strataId,
                subStrataId);
    }

    @Override
    public String getLocationLabelByLatLong(Float latitude, Float longitude) {
        return getLocationService().getLocationLabelByLatLong(latitude, longitude);
    }

    @Override
    public Integer getLocationIdByLatLong(Float latitude, Float longitude) {
        return getLocationService().getLocationIdByLatLong(latitude, longitude);
    }

    @Override
    public TuttiLocation getLocation(String id) {
        return getLocationService().getLocation(id);
    }

    //------------------------------------------------------------------------//
    //-- ObjectTypePersistenceService methods                               --//
    //------------------------------------------------------------------------//

    @Override
    public List<ObjectType> getAllObjectType() {
        return getObjectTypeService().getAllObjectType();
    }

    @Override
    public ObjectType getObjectType(String objectTypeCode) {
        return getObjectTypeService().getObjectType(objectTypeCode);
    }

    //------------------------------------------------------------------------//
    //-- GearPersistenceService methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public List<Gear> getAllScientificGear() {
        return getGearService().getAllScientificGear();
    }

    @Override
    public List<Gear> getAllFishingGear() {
        return getGearService().getAllFishingGear();
    }

    @Override
    public List<Gear> getAllGearWithObsoletes() {
        return getGearService().getAllGearWithObsoletes();
    }

    @Override
    public Gear getGear(Integer gearId) {
        return getGearService().getGear(gearId);
    }

    @Override
    public boolean isTemporaryGearUsed(Integer id) {
        return getGearService().isTemporaryGearUsed(id);
    }

    @Override
    public List<Gear> addTemporaryGears(List<Gear> gears) {
        return getGearService().addTemporaryGears(gears);
    }

    @Override
    public List<Gear> updateTemporaryGears(List<Gear> gears) {
        return getGearService().updateTemporaryGears(gears);
    }

    @Override
    public List<Gear> linkTemporaryGears(List<Gear> gears) {
        return getGearService().linkTemporaryGears(gears);
    }

    @Override
    public void replaceGear(Gear source, Gear target, boolean delete) {
        getGearService().replaceGear(source, target, delete);
    }

    @Override
    public void deleteTemporaryGear(Integer id) {
        getGearService().deleteTemporaryGear(id);
    }

    @Override
    public void deleteTemporaryGears(Collection<Integer> id) {
        getGearService().deleteTemporaryGears(id);
    }

    //------------------------------------------------------------------------//
    //-- SpeciesPersistenceService methods                                  --//
    //------------------------------------------------------------------------//

    @Override
    public List<Species> getAllSpecies() {
        List<Species> result = getSpeciesService().getAllSpecies();
        setSpeciesSurveyCode(result, getProtocol());
        return result;
    }

    @Override
    public List<Species> getAllReferentSpecies() {
        List<Species> result = getSpeciesService().getAllReferentSpecies();
        setSpeciesSurveyCode(result, getProtocol());
        return result;
    }

    @Override
    public List<Species> getAllReferentSpeciesWithObsoletes() {
        List<Species> result = getSpeciesService().getAllReferentSpeciesWithObsoletes();
        setSpeciesSurveyCode(result, getProtocol());
        return result;
    }

    @Override
    public Species getSpeciesByReferenceTaxonIdWithVernacularCode(Integer referenceTaxonId) {
        return getSpeciesService().getSpeciesByReferenceTaxonIdWithVernacularCode(referenceTaxonId);
    }

    @Override
    public Species getSpeciesByReferenceTaxonId(Integer referenceTaxonId) {
        return getSpeciesService().getSpeciesByReferenceTaxonId(referenceTaxonId);
    }

    @Override
    public Map<Integer, Integer> getAllObsoleteReferentTaxons() {
        return getSpeciesService().getAllObsoleteReferentTaxons();
    }

    @Override
    public boolean isTemporarySpeciesUsed(Integer referenceTaxonId) {

        TuttiProtocol protocol = getProtocol();

        if (protocol != null) {

            // check first if species is used in protocol

            SpeciesProtocol speciesProtocol = TuttiProtocols.getSpeciesOrBenthosProtocol(protocol, referenceTaxonId);
            if (speciesProtocol != null) {

                // used in species or benthos protocol
                return true;
            }

        }
        return getSpeciesService().isTemporarySpeciesUsed(referenceTaxonId);
    }

    @Override
    public List<Species> addTemporarySpecies(List<Species> species) {
        return getSpeciesService().addTemporarySpecies(species);
    }

    @Override
    public List<Species> updateTemporarySpecies(List<Species> species) {
        return getSpeciesService().updateTemporarySpecies(species);
    }

    @Override
    public List<Species> linkTemporarySpecies(List<Species> species) {
        return getSpeciesService().linkTemporarySpecies(species);
    }

    @Override
    public void replaceSpecies(Species source, Species target, boolean delete) {

        getSpeciesService().replaceSpecies(source, target, delete);

        if (delete) {
            removeSpeciesFromProtocol(Lists.newArrayList(source.getReferenceTaxonId()));
        }

    }

    @Override
    public void deleteTemporarySpecies(Integer referenceTaxonId) {

        getSpeciesService().deleteTemporarySpecies(referenceTaxonId);
        removeSpeciesFromProtocol(Lists.newArrayList(referenceTaxonId));

    }

    @Override
    public void deleteTemporarySpecies(Collection<Integer> referenceTaxonIds) {

        getSpeciesService().deleteTemporarySpecies(referenceTaxonIds);

        removeSpeciesFromProtocol(referenceTaxonIds);

    }

    protected void removeSpeciesFromProtocol(Collection<Integer> referenceTaxonIds) {

        TuttiProtocol protocol = getProtocol();

        if (protocol != null) {

            boolean wasRemoved = false;

            for (Integer referenceTaxonId : referenceTaxonIds) {

                wasRemoved |= TuttiProtocols.removeSpeciesOrBenthosProtocol(protocol, referenceTaxonId);

            }

            if (wasRemoved) {

                if (log.isInfoEnabled()) {
                    log.info("Save protocol (some species or benthos were removed from it.)");
                }
                saveProtocol(protocol);

            }

        }

    }

    //------------------------------------------------------------------------//
    //-- PersonPersistenceService methods                                   --//
    //------------------------------------------------------------------------//

    @Override
    public List<Person> getAllPerson() {
        return getPersonService().getAllPerson();
    }

    @Override
    public List<Person> getAllPersonWithObsoletes() {
        return getPersonService().getAllPersonWithObsoletes();
    }

    @Override
    public Person getPerson(Integer personId) {
        return getPersonService().getPerson(personId);
    }

    @Override
    public boolean isTemporaryPersonUsed(Integer id) {
        return getPersonService().isTemporaryPersonUsed(id);
    }

    @Override
    public List<Person> addTemporaryPersons(List<Person> persons) {
        return getPersonService().addTemporaryPersons(persons);
    }

    @Override
    public List<Person> updateTemporaryPersons(List<Person> persons) {
        return getPersonService().updateTemporaryPersons(persons);
    }

    @Override
    public List<Person> linkTemporaryPersons(List<Person> persons) {
        return getPersonService().linkTemporaryPersons(persons);
    }

    @Override
    public void replacePerson(Person source, Person target, boolean delete) {
        getPersonService().replacePerson(source, target, delete);
    }

    @Override
    public void deleteTemporaryPerson(Integer id) {
        getPersonService().deleteTemporaryPerson(id);
    }

    @Override
    public void deleteTemporaryPersons(Collection<Integer> ids) {
        getPersonService().deleteTemporaryPersons(ids);
    }

    //------------------------------------------------------------------------//
    //-- VesselPersistenceService methods                                   --//
    //------------------------------------------------------------------------//

    @Override
    public List<Vessel> getAllScientificVessel() {
        return getVesselService().getAllScientificVessel();
    }

    @Override
    public List<Vessel> getAllFishingVessel() {
        return getVesselService().getAllFishingVessel();
    }

    @Override
    public List<Vessel> getAllVesselWithObsoletes() {
        return getVesselService().getAllVesselWithObsoletes();
    }

    @Override
    public Vessel getVessel(String vesselCode) {
        return getVesselService().getVessel(vesselCode);
    }

    @Override
    public boolean isTemporaryVesselUsed(String code) {
        return getVesselService().isTemporaryVesselUsed(code);
    }

    @Override
    public List<Vessel> addTemporaryVessels(List<Vessel> vessels) {
        return getVesselService().addTemporaryVessels(vessels);
    }

    @Override
    public List<Vessel> updateTemporaryVessels(List<Vessel> vessels) {
        return getVesselService().updateTemporaryVessels(vessels);
    }

    @Override
    public List<Vessel> linkTemporaryVessels(List<Vessel> vessels) {
        return getVesselService().linkTemporaryVessels(vessels);
    }

    @Override
    public void replaceVessel(Vessel source, Vessel target, boolean delete) {
        getVesselService().replaceVessel(source, target, delete);
    }

    @Override
    public void deleteTemporaryVessel(String code) {
        getVesselService().deleteTemporaryVessel(code);
    }

    @Override
    public void deleteTemporaryVessels(Collection<String> codes) {
        getVesselService().deleteTemporaryVessels(codes);
    }

    //------------------------------------------------------------------------//
    //-- Attachment methods                                                 --//
    //------------------------------------------------------------------------//

    @Override
    public List<Attachment> getAllAttachments(ObjectTypeCode objectType,
                                              Integer objectId) {
        return getAttachmentService().getAllAttachments(objectType, objectId);
    }

    @Override
    public File getAttachmentFile(String attachmentId) {
        return getAttachmentService().getAttachmentFile(attachmentId);
    }

    @Override
    public Attachment createAttachment(Attachment attachment, File file) {
        return getAttachmentService().createAttachment(attachment, file);
    }

    @Override
    public Attachment saveAttachment(Attachment attachment) {
        return getAttachmentService().saveAttachment(attachment);
    }

    @Override
    public void deleteAttachment(String attachmentId) {
        getAttachmentService().deleteAttachment(attachmentId);
    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Integer objectId) {
        getAttachmentService().deleteAllAttachment(objectType, objectId);
    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Set<Integer> objectIds) {
        getAttachmentService().deleteAllAttachment(objectType, objectIds);
    }

    //------------------------------------------------------------------------//
    //-- Program methods                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public List<Program> getAllProgram() {
        return getProgramService().getAllProgram();
    }

    @Override
    public Program getProgram(String id) {
        return getProgramService().getProgram(id);
    }

    @Override
    public Program createProgram(Program bean) {
        return getProgramService().createProgram(bean);
    }

    @Override
    public Program saveProgram(Program bean) {
        return getProgramService().saveProgram(bean);
    }

    //------------------------------------------------------------------------//
    //-- Cruise methods                                                     --//
    //------------------------------------------------------------------------//


    @Override
    public List<Integer> getAllCruiseId(String programId) {
        return getCruiseService().getAllCruiseId(programId);
    }

    @Override
    public List<Cruise> getAllCruise(String programId) {
        return getCruiseService().getAllCruise(programId);
    }

    @Override
    public Cruise getCruise(Integer id) {
        return getCruiseService().getCruise(id);
    }

    @Override
    public Cruise createCruise(Cruise bean) {
        return getCruiseService().createCruise(bean);
    }

    @Override
    public Cruise saveCruise(Cruise bean,
                             boolean updateVessel,
                             boolean updateGear) {
        return getCruiseService().saveCruise(bean, updateVessel, updateGear);
    }

    @Override
    public void setCruiseReadyToSynch(Integer cruiseId) {
        getCruiseService().setCruiseReadyToSynch(cruiseId);
    }

    @Override
    public CaracteristicMap getGearCaracteristics(Integer cruiseId, Integer gearId, short rankOrder) {
        return getCruiseService().getGearCaracteristics(cruiseId, gearId, rankOrder);
    }

    @Override
    public boolean isOperationUseGears(Integer cruiseId, Collection<Gear> gears) {
        return getCruiseService().isOperationUseGears(cruiseId, gears);
    }

    @Override
    public void saveGearCaracteristics(Gear gear, Cruise cruise) {
        getCruiseService().saveGearCaracteristics(gear, cruise);
    }

    //------------------------------------------------------------------------//
    //-- Protocol methods                                                   --//
    //------------------------------------------------------------------------//

    @Override
    public TuttiProtocol getProtocol() {
        return getProtocolService().getProtocol();
    }

    @Override
    public void setProtocol(TuttiProtocol protocol) {
        getProtocolService().setProtocol(protocol);
    }

    @Override
    public boolean isProtocolExist(String id) {
        return getProtocolService().isProtocolExist(id);
    }

    @Override
    public String getFirstAvailableName(String protocolName) {
        return getProtocolService().getFirstAvailableName(protocolName);
    }

    @Override
    public List<String> getAllProtocolNames() {
        return getProtocolService().getAllProtocolNames();
    }

    @Override
    public TuttiProtocol getProtocolByName(String protocolName) {
        return getProtocolService().getProtocolByName(protocolName);
    }


    @Override
    public List<TuttiProtocol> getAllProtocol() {
        return getProtocolService().getAllProtocol();
    }

    @Override
    public List<TuttiProtocol> getAllProtocol(String programId) {
        return getProtocolService().getAllProtocol(programId);
    }

    @Override
    public List<String> getAllProtocolId() {
        return getProtocolService().getAllProtocolId();
    }

    @Override
    public TuttiProtocol saveProtocol(TuttiProtocol bean) {
        return getProtocolService().saveProtocol(bean);
    }

    @Override
    public void deleteProtocol(String id) {
        getProtocolService().deleteProtocol(id);
    }

    @Override
    public TuttiProtocol createProtocol(TuttiProtocol bean) {
        return protocolService.createProtocol(bean);
    }

    @Override
    public TuttiProtocol getProtocol(String id) {
        TuttiProtocol protocol = getProtocolService().getProtocol(id);

        // translate obsolete referent taxons (See https://forge.codelutin.com/issues/7846)
        Map<Integer, Integer> allObsoleteReferentTaxons = getAllObsoleteReferentTaxons();
        TuttiProtocols.translateReferenceTaxonIds(protocol, allObsoleteReferentTaxons);

        // sanity it (remove all bad species and benthos)
        // see http://forge.codelutin.com/issues/4154

        List<Species> allReferentSpecies = getAllReferentSpecies();

        Map<Integer, String> missingSpecies = TuttiProtocols.detectMissingSpecies(protocol, allReferentSpecies);
        TuttiProtocols.removeBadSpecies(protocol, missingSpecies);

        Map<Integer, String> missingBenthos = TuttiProtocols.detectMissingBenthos(protocol, allReferentSpecies);
        TuttiProtocols.removeBadBenthos(protocol, missingBenthos);

        return protocol;
    }

    //------------------------------------------------------------------------//
    //-- Fishing operation methods                                          --//
    //------------------------------------------------------------------------//

    @Override
    public int getFishingOperationCount(Integer cruiseId) {
        return getFishingOperationService().getFishingOperationCount(cruiseId);
    }

    @Override
    public List<Integer> getAllFishingOperationIds(Integer cruiseId) {
        return getFishingOperationService().getAllFishingOperationIds(cruiseId);
    }

    @Override
    public List<FishingOperation> getAllFishingOperation(Integer cruiseId) {
        return getFishingOperationService().getAllFishingOperation(cruiseId);
    }

    @Override
    public FishingOperation getFishingOperation(Integer id) {
        return getFishingOperationService().getFishingOperation(id);
    }

    @Override
    public List<Vessel> getFishingOperationSecondaryVessel(Integer fishingOperationId) {
        return getFishingOperationService().getFishingOperationSecondaryVessel(fishingOperationId);
    }

    @Override
    public FishingOperation createFishingOperation(FishingOperation bean) {
        return getFishingOperationService().createFishingOperation(bean);
    }

    @Override
    public FishingOperation saveFishingOperation(FishingOperation bean) {
        return getFishingOperationService().saveFishingOperation(bean);
    }

    @Override
    public Collection<FishingOperation> saveFishingOperations(Collection<FishingOperation> beans) {
        return getFishingOperationService().saveFishingOperations(beans);
    }

    @Override
    public void deleteFishingOperation(Integer id) {
        getFishingOperationService().deleteFishingOperation(id);
    }

    //------------------------------------------------------------------------//
    //-- CatchBatch methods                                                 --//
    //------------------------------------------------------------------------//

    @Override
    public boolean isFishingOperationWithCatchBatch(Integer operationId) {
        return getCatchBatchService().isFishingOperationWithCatchBatch(operationId);
    }

    @Override
    public CatchBatch getCatchBatchFromFishingOperation(Integer id) throws InvalidBatchModelException {
        return getCatchBatchService().getCatchBatchFromFishingOperation(id);
    }

    @Override
    public CatchBatch createCatchBatch(CatchBatch bean) {
        return getCatchBatchService().createCatchBatch(bean);
    }

    @Override
    public CatchBatch saveCatchBatch(CatchBatch bean) {
        return getCatchBatchService().saveCatchBatch(bean);
    }

    @Override
    public void deleteCatchBatch(Integer fishingOperationId) {
        getCatchBatchService().deleteCatchBatch(fishingOperationId);
    }

    @Override
    public void recomputeCatchBatchSampleRatios(Integer fishingOperationId) {
        getCatchBatchService().recomputeCatchBatchSampleRatios(fishingOperationId);
    }

    //------------------------------------------------------------------------//
    //-- Species Batch methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<SpeciesBatch> getRootSpeciesBatch(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException {
        return getSpeciesBatchService().getRootSpeciesBatch(fishingOperationId, validateTree);
    }

    @Override
    public Set<Integer> getBatchChildIds(Integer id) {
        return getSpeciesBatchService().getBatchChildIds(id);
    }

    @Override
    public SpeciesBatch createSpeciesBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {
        return getSpeciesBatchService().createSpeciesBatch(bean, parentBatchId, computeRankOrder);
    }

    @Override
    public Collection<SpeciesBatch> createSpeciesBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {
        return getSpeciesBatchService().createSpeciesBatches(fishingOperationId, beans);
    }

    @Override
    public SpeciesBatch saveSpeciesBatch(SpeciesBatch bean) {
        return getSpeciesBatchService().saveSpeciesBatch(bean);
    }

    @Override
    public void deleteSpeciesBatch(Integer id) {
        getSpeciesBatchService().deleteSpeciesBatch(id);
    }

    @Override
    public void deleteSpeciesSubBatch(Integer id) {
        getSpeciesBatchService().deleteSpeciesSubBatch(id);
    }

    @Override
    public void changeSpeciesBatchSpecies(Integer batchId, Species species) {
        getSpeciesBatchService().changeSpeciesBatchSpecies(batchId, species);
    }

    @Override
    public List<SpeciesBatch> getAllSpeciesBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        return getSpeciesBatchService().getAllSpeciesBatchToConfirm(fishingOperationId);
    }

    @Override
    public List<SpeciesBatchFrequency> getAllSpeciesBatchFrequency(Integer speciesBatchId) {
        return getSpeciesBatchService().getAllSpeciesBatchFrequency(speciesBatchId);
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllSpeciesBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        return getSpeciesBatchService().getAllSpeciesBatchFrequencyForBatch(batchContainer);
    }

    @Override
    public List<SpeciesBatchFrequency> saveSpeciesBatchFrequency(Integer speciesBatchId, List<SpeciesBatchFrequency> frequencies) {
        return getSpeciesBatchService().saveSpeciesBatchFrequency(speciesBatchId, frequencies);
    }

    //------------------------------------------------------------------------//
    //-- Benthos Batch methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<SpeciesBatch> getRootBenthosBatch(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException {
        return getBenthosBatchService().getRootBenthosBatch(fishingOperationId, validateTree);
    }

    @Override
    public SpeciesBatch createBenthosBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {
        return getBenthosBatchService().createBenthosBatch(bean, parentBatchId, computeRankOrder);
    }

    @Override
    public Collection<SpeciesBatch> createBenthosBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {
        return getBenthosBatchService().createBenthosBatches(fishingOperationId, beans);
    }

    @Override
    public SpeciesBatch saveBenthosBatch(SpeciesBatch bean) {
        return getBenthosBatchService().saveBenthosBatch(bean);
    }

    @Override
    public void deleteBenthosBatch(Integer id) {
        getBenthosBatchService().deleteBenthosBatch(id);
    }

    @Override
    public void deleteBenthosSubBatch(Integer id) {
        getBenthosBatchService().deleteBenthosSubBatch(id);
    }

    @Override
    public void changeBenthosBatchSpecies(Integer batchId, Species species) {
        getBenthosBatchService().changeBenthosBatchSpecies(batchId, species);
    }

    @Override
    public List<SpeciesBatch> getAllBenthosBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        return getBenthosBatchService().getAllBenthosBatchToConfirm(fishingOperationId);
    }

    @Override
    public List<SpeciesBatchFrequency> getAllBenthosBatchFrequency(Integer benthosBatchId) {
        return getBenthosBatchService().getAllBenthosBatchFrequency(benthosBatchId);
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllBenthosBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        return getBenthosBatchService().getAllBenthosBatchFrequencyForBatch(batchContainer);
    }

    @Override
    public List<SpeciesBatchFrequency> saveBenthosBatchFrequency(Integer benthosBatchId,
                                                                 List<SpeciesBatchFrequency> frequencies) {
        return getBenthosBatchService().saveBenthosBatchFrequency(benthosBatchId, frequencies);
    }

    //------------------------------------------------------------------------//
    //-- MarineLitter Batch methods                                         --//
    //------------------------------------------------------------------------//

    @Override
    public BatchContainer<MarineLitterBatch> getRootMarineLitterBatch(Integer fishingOperationId) {
        return getMarineLitterBatchService().getRootMarineLitterBatch(fishingOperationId);
    }

    @Override
    public MarineLitterBatch createMarineLitterBatch(MarineLitterBatch bean) {
        return getMarineLitterBatchService().createMarineLitterBatch(bean);
    }

    @Override
    public Collection<MarineLitterBatch> createMarineLitterBatches(Integer fishingOperationId, Collection<MarineLitterBatch> beans) {
        return getMarineLitterBatchService().createMarineLitterBatches(fishingOperationId, beans);
    }

    @Override
    public MarineLitterBatch saveMarineLitterBatch(MarineLitterBatch bean) {
        return getMarineLitterBatchService().saveMarineLitterBatch(bean);
    }

    @Override
    public void deleteMarineLitterBatch(Integer id) {
        getMarineLitterBatchService().deleteMarineLitterBatch(id);
    }

    //------------------------------------------------------------------------//
    //-- Accidental Batch methods                                           --//
    //------------------------------------------------------------------------//

    @Override
    public List<AccidentalBatch> getAllAccidentalBatch(Integer fishingOperationId) {
        return getAccidentalBatchService().getAllAccidentalBatch(fishingOperationId);
    }

    @Override
    public AccidentalBatch createAccidentalBatch(AccidentalBatch bean) {
        return getAccidentalBatchService().createAccidentalBatch(bean);
    }

    @Override
    public Collection<AccidentalBatch> createAccidentalBatches(Collection<AccidentalBatch> beans) {
        return getAccidentalBatchService().createAccidentalBatches(beans);
    }

    @Override
    public AccidentalBatch saveAccidentalBatch(AccidentalBatch bean) {
        return getAccidentalBatchService().saveAccidentalBatch(bean);
    }

    @Override
    public void deleteAccidentalBatch(String id) {
        getAccidentalBatchService().deleteAccidentalBatch(id);
    }

    @Override
    public void deleteAccidentalBatchForFishingOperation(Integer fishingOperationId) {
        getAccidentalBatchService().deleteAccidentalBatchForFishingOperation(fishingOperationId);
    }

    //------------------------------------------------------------------------//
    //-- IndividualObservation Batch methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForBatch(Integer batchId) {
        return getIndividualObservationBatchService().getAllIndividualObservationBatchsForBatch(batchId);
    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForFishingOperation(Integer fishingOperationId) {
        return getIndividualObservationBatchService().getAllIndividualObservationBatchsForFishingOperation(fishingOperationId);
    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForCruise(Integer cruiseId) {
        return getIndividualObservationBatchService().getAllIndividualObservationBatchsForCruise(cruiseId);
    }

    @Override
    public boolean isSamplingCodeAvailable(Integer cruiseId, Integer referenceTaxonId, String samplingCodeSuffix) {
        return getIndividualObservationBatchService().isSamplingCodeAvailable(cruiseId, referenceTaxonId, samplingCodeSuffix);
    }

    @Override
    public List<IndividualObservationBatch> createIndividualObservationBatches(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {
        return getIndividualObservationBatchService().createIndividualObservationBatches(fishingOperation, individualObservations);
    }

    public List<IndividualObservationBatch> saveBatchIndividualObservation(Integer batchId,
                                                                           List<IndividualObservationBatch> individualObservation) {
        return getIndividualObservationBatchService().saveBatchIndividualObservation(batchId, individualObservation);
    }

    @Override
    public void deleteAllIndividualObservationsForFishingOperation(Integer fishingOperationId) {
        getIndividualObservationBatchService().deleteAllIndividualObservationsForFishingOperation(fishingOperationId);
    }

    @Override
    public void deleteAllIndividualObservationsForBatch(Integer speciesBatchId) {
        getIndividualObservationBatchService().deleteAllIndividualObservationsForBatch(speciesBatchId);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void setSpeciesSurveyCode(List<Species> speciesList, TuttiProtocol protocol) {
        if (protocol != null && !protocol.isSpeciesEmpty()) {

            Map<Integer, String> surveyCodeByTaxonId = Maps.newTreeMap();

            if (!protocol.isSpeciesEmpty()) {
                for (SpeciesProtocol speciesProtocol : protocol.getSpecies()) {
                    String surveyCode = speciesProtocol.getSpeciesSurveyCode();
                    if (StringUtils.isNotBlank(surveyCode)) {
                        Integer taxonId =
                                speciesProtocol.getSpeciesReferenceTaxonId();
                        surveyCodeByTaxonId.put(taxonId, surveyCode);
                    }
                }
            }
            if (!protocol.isBenthosEmpty()) {
                for (SpeciesProtocol speciesProtocol : protocol.getBenthos()) {
                    String surveyCode = speciesProtocol.getSpeciesSurveyCode();
                    if (StringUtils.isNotBlank(surveyCode)) {
                        Integer taxonId =
                                speciesProtocol.getSpeciesReferenceTaxonId();
                        String oldSurveyCode = surveyCodeByTaxonId.put(taxonId, surveyCode);
                        if (oldSurveyCode != null) {
                            if (log.isWarnEnabled()) {
                                log.warn(String.format("Detect a species in both species and benthos protocol: taxonId=%d, species surveyCode=%s, benthos surveyCode=%s", taxonId, oldSurveyCode, surveyCode));
                            }
                        }
                    }
                }
            }
            for (Species species : speciesList) {
                Integer taxonId = species.getReferenceTaxonId();
                String surveyCode = surveyCodeByTaxonId.get(taxonId);
                species.setSurveyCode(surveyCode);
            }
        }
    }

    public AccidentalBatchPersistenceService getAccidentalBatchService() {
        return getServiceInitialized(accidentalBatchService);
    }

    public AttachmentPersistenceService getAttachmentService() {
        return getServiceInitialized(attachmentService);
    }

    public BenthosBatchPersistenceService getBenthosBatchService() {
        return getServiceInitialized(benthosBatchService);
    }

    public CaracteristicPersistenceService getCaracteristicService() {
        return getServiceInitialized(caracteristicService);
    }

    public CatchBatchPersistenceService getCatchBatchService() {
        return getServiceInitialized(catchBatchService);
    }

    public CruisePersistenceService getCruiseService() {
        return getServiceInitialized(cruiseService);
    }

    public FishingOperationPersistenceService getFishingOperationService() {
        return getServiceInitialized(fishingOperationService);
    }

    public GearPersistenceService getGearService() {
        return getServiceInitialized(gearService);
    }

    public IndividualObservationBatchPersistenceService getIndividualObservationBatchService() {
        return getServiceInitialized(individualObservationBatchService);
    }

    public LocationPersistenceService getLocationService() {
        return getServiceInitialized(locationService);
    }

    public MarineLitterBatchPersistenceService getMarineLitterBatchService() {
        return getServiceInitialized(marineLitterBatchService);
    }

    public ObjectTypePersistenceService getObjectTypeService() {
        return getServiceInitialized(objectTypeService);
    }

    public PersonPersistenceService getPersonService() {
        return getServiceInitialized(personService);
    }

    public ProtocolPersistenceService getProtocolService() {
        return getServiceInitialized(protocolService);
    }

    public ProgramPersistenceService getProgramService() {
        return getServiceInitialized(programService);
    }

    public SpeciesBatchPersistenceService getSpeciesBatchService() {
        return getServiceInitialized(speciesBatchService);
    }

    public SpeciesPersistenceService getSpeciesService() {
        return getServiceInitialized(speciesService);
    }

    public TechnicalPersistenceService getTechnicalPersistenceService() {
        return getServiceInitialized(technicalPersistenceService);
    }

    public VesselPersistenceService getVesselService() {
        return getServiceInitialized(vesselService);
    }

    protected <S extends TuttiPersistenceServiceImplementor> S getServiceInitialized(S service) {
        service.lazyInit();
        return service;
    }

}
