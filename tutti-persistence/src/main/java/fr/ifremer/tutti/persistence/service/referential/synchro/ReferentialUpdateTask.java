package fr.ifremer.tutti.persistence.service.referential.synchro;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.2
 */
public interface ReferentialUpdateTask {

    String getTable();

    /**
     * @param localConnection connection to local db
     * @param lastUpdate      last update of the local table
     * @throws SQLException if any sql problem
     */
    void update(Connection localConnection, Timestamp lastUpdate) throws SQLException;
}
