package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface CatchBatchPersistenceService extends TuttiPersistenceServiceImplementor {

    //------------------------------------------------------------------------//
    //-- CatchBatch methods                                                 --//
    //------------------------------------------------------------------------//

    /**
     * @param operationId id of the fishing operation
     * @return {@code true} if there is a catchBatch for the given fishing
     * operation, {@code false} otherwise.
     * @since 2.2
     */
    boolean isFishingOperationWithCatchBatch(Integer operationId);

    /**
     * Get the catchBatch from the fishing Operation id.
     *
     * @param fishingOperationId id of the fishing operation
     * @return found catchBatch
     */
    CatchBatch getCatchBatchFromFishingOperation(Integer fishingOperationId);

    /**
     * Create the given CatchBatch and return it.
     *
     * @param bean catchBatch to create
     * @return created catchBatch
     */
    @Transactional(readOnly = false)
    CatchBatch createCatchBatch(CatchBatch bean);

    /**
     * Save the given catchBatch and return it.
     *
     * @param bean batch to save
     * @return the saved catchBatch
     */
    @Transactional(readOnly = false)
    CatchBatch saveCatchBatch(CatchBatch bean);

    /**
     * Delete catch batch for the given fishing Operation id (will then delete
     * in cascade all sorting batchs + individual observation batchs).
     *
     * @param fishingOperationId id of the fishing operation
     * @since 2.2
     */
    @Transactional(readOnly = false)
    void deleteCatchBatch(Integer fishingOperationId);

    /**
     * Recompute all the sampleRatio and sampleRatioText for the catch batch
     * and all his children batches for the given fishing operation id.
     *
     * @param fishingOperationId id of the fihsing operation
     * @since 3.5
     */
    @Transactional(readOnly = false)
    void recomputeCatchBatchSampleRatios(Integer fishingOperationId);
}
