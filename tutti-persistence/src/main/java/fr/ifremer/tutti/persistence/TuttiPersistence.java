package fr.ifremer.tutti.persistence;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.persistence.service.AccidentalBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.AttachmentPersistenceService;
import fr.ifremer.tutti.persistence.service.BenthosBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.CatchBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.CruisePersistenceService;
import fr.ifremer.tutti.persistence.service.FishingOperationPersistenceService;
import fr.ifremer.tutti.persistence.service.IndividualObservationBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.MarineLitterBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.ProgramPersistenceService;
import fr.ifremer.tutti.persistence.service.ProtocolPersistenceService;
import fr.ifremer.tutti.persistence.service.SpeciesBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.TechnicalPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.ObjectTypePersistenceService;
import fr.ifremer.tutti.persistence.service.referential.PersonPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.VesselPersistenceService;
import org.springframework.transaction.annotation.Transactional;

/**
 * Contract for a persistence driver used by Tutti.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
@Transactional(readOnly = true)
public interface TuttiPersistence extends TuttiPersistenceServiceImplementor,
        TechnicalPersistenceService,
        BenthosBatchPersistenceService,
        SpeciesBatchPersistenceService,
        CaracteristicPersistenceService,
        GearPersistenceService,
        LocationPersistenceService,
        ObjectTypePersistenceService,
        PersonPersistenceService,
        SpeciesPersistenceService,
        VesselPersistenceService,
        CatchBatchPersistenceService,
        AttachmentPersistenceService,
        ProgramPersistenceService,
        CruisePersistenceService,
        ProtocolPersistenceService,
        FishingOperationPersistenceService,
        MarineLitterBatchPersistenceService,
        AccidentalBatchPersistenceService,
        IndividualObservationBatchPersistenceService {

    String getImplementationName();

    void setSkipShutdownDbWhenClosing();

    /**
     * To load a program with all his cruises and fishing operations.
     *
     * @param programId            id of the program to load
     * @param loadFishingOperation flag to load cruise fishing operations
     * @return loaded program
     * @since 3.14.3
     */
    ProgramDataModel loadProgram(String programId, boolean loadFishingOperation);

    /**
     * * To load a program with given cruises and fishing operations.
     *
     * @param programId            id of the program to load
     * @param loadFishingOperation flag to load cruise fishing operations
     * @param cruiseIds            ids of cruises to load
     * @return loaded program
     * @since 3.14.3
     */
    ProgramDataModel loadCruises(String programId, boolean loadFishingOperation, Integer... cruiseIds);

    /**
     * * To load a program for his given cruise and fishing operations.
     *
     * @param programId           id of the program to load
     * @param cruiseId            id of cruise to load
     * @param fishingOperationIds ids of fishing operation to load
     * @return loaded program
     * @since 3.14.3
     */
    ProgramDataModel loadCruise(String programId, Integer cruiseId, Integer... fishingOperationIds);

}
