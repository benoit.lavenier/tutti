package fr.ifremer.tutti.persistence.entities.protocol.v3;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.Rtp;

import java.util.Collection;
import java.util.List;

public interface SpeciesProtocol3 extends TuttiEntity {

    String PROPERTY_SPECIES_REFERENCE_TAXON_ID = "speciesReferenceTaxonId";

    String PROPERTY_SPECIES_SURVEY_CODE = "speciesSurveyCode";

    String PROPERTY_LENGTH_STEP_PMFM_ID = "lengthStepPmfmId";

    String PROPERTY_WEIGHT_ENABLED = "weightEnabled";

    String PROPERTY_COUNT_IF_NO_FREQUENCY_ENABLED = "countIfNoFrequencyEnabled";

    String PROPERTY_CALCIFY_SAMPLE_ENABLED = "calcifySampleEnabled";

    String PROPERTY_LENGTH_STEP = "lengthStep";

    String PROPERTY_MADE_FROM_AREFERENT_TAXON = "madeFromAReferentTaxon";

    String PROPERTY_MANDATORY_SAMPLE_CATEGORY_ID = "mandatorySampleCategoryId";

    String PROPERTY_RTP_MALE = "rtpMale";

    String PROPERTY_RTP_FEMALE = "rtpFemale";

    String PROPERTY_RTP_UNDEFINED = "rtpUndefined";

    String PROPERTY_INDIVIDUAL_OBSERVATION_ENABLED = "individualObservationEnabled";

    String PROPERTY_MATURITY_PMFM_ID = "maturityPmfmId";

    String PROPERTY_CALCIFIED_PIECES_SAMPLING_DEFINITION = "calcifiedPiecesSamplingDefinition";

    Integer getSpeciesReferenceTaxonId();

    void setSpeciesReferenceTaxonId(Integer speciesReferenceTaxonId);

    String getSpeciesSurveyCode();

    void setSpeciesSurveyCode(String speciesSurveyCode);

    String getLengthStepPmfmId();

    void setLengthStepPmfmId(String lengthStepPmfmId);

    boolean isWeightEnabled();

    void setWeightEnabled(boolean weightEnabled);

    boolean isCountIfNoFrequencyEnabled();

    void setCountIfNoFrequencyEnabled(boolean countIfNoFrequencyEnabled);

    boolean isCalcifySampleEnabled();

    void setCalcifySampleEnabled(boolean calcifySampleEnabled);

    Float getLengthStep();

    void setLengthStep(Float lengthStep);

    boolean isMadeFromAReferentTaxon();

    void setMadeFromAReferentTaxon(boolean madeFromAReferentTaxon);

    Integer getMandatorySampleCategoryId(int index);

    boolean isMandatorySampleCategoryIdEmpty();

    int sizeMandatorySampleCategoryId();

    void addMandatorySampleCategoryId(Integer mandatorySampleCategoryId);

    void addAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId);

    boolean removeMandatorySampleCategoryId(Integer mandatorySampleCategoryId);

    boolean removeAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId);

    boolean containsMandatorySampleCategoryId(Integer mandatorySampleCategoryId);

    boolean containsAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId);

    List<Integer> getMandatorySampleCategoryId();

    void setMandatorySampleCategoryId(List<Integer> mandatorySampleCategoryId);

    Rtp getRtpMale();

    void setRtpMale(Rtp rtpMale);

    Rtp getRtpFemale();

    void setRtpFemale(Rtp rtpFemale);

    Rtp getRtpUndefined();

    void setRtpUndefined(Rtp rtpUndefined);

    boolean isIndividualObservationEnabled();

    void setIndividualObservationEnabled(boolean individualObservationEnabled);

    String getMaturityPmfmId();

    void setMaturityPmfmId(String maturityPmfmId);

    String getCalcifiedPiecesSamplingTypePmfmId();

    void setCalcifiedPiecesSamplingTypePmfmId(String calcifiedPiecesSamplingTypePmfmId);

    CalcifiedPiecesSamplingDefinition getCalcifiedPiecesSamplingDefinition(int index);

    boolean isCalcifiedPiecesSamplingDefinitionEmpty();

    int sizeCalcifiedPiecesSamplingDefinition();

    void addCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition);

    void addAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition);

    boolean removeCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition);

    boolean removeAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition);

    boolean containsCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition);

    boolean containsAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition);

    Collection<CalcifiedPiecesSamplingDefinition> getCalcifiedPiecesSamplingDefinition();

    void setCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition);

    boolean withRtpMale();

    boolean withRtpFemale();

    boolean withRtpUndefined();

}
