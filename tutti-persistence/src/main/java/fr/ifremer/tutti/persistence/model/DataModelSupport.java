package fr.ifremer.tutti.persistence.model;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Objects;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public abstract class DataModelSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Id representing the data.
     */
    private final String id;

    /**
     * Label of the data.
     */
    private final String label;

    /**
     * Optional id (used for example to link an id in a db to the same data in another db.
     */
    private String optionalId;

    public DataModelSupport(String id, String label) {
        this.id = id;
        this.label = label;
    }

    public String getId() {
        return id;
    }
    public Integer getIdAsInt() {
        Objects.requireNonNull(id);
        return Integer.valueOf(id);
    }

    public String getLabel() {
        return label;
    }

    public String getOptionalId() {
        return optionalId;
    }

    public void setOptionalId(String optionalId) {
        this.optionalId = optionalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataModelSupport)) return false;

        DataModelSupport that = (DataModelSupport) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
