package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.service.technical.CacheService;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.Strata;
import fr.ifremer.tutti.persistence.entities.protocol.Stratas;
import fr.ifremer.tutti.persistence.entities.protocol.SubStrata;
import fr.ifremer.tutti.persistence.entities.protocol.SubStratas;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.protocol.Zones;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Service("protocolPersistenceService")
public class ProtocolPersistenceServiceImpl extends AbstractPersistenceService implements ProtocolPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ProtocolPersistenceServiceImpl.class);

    public static final String TUTTI_PROTOCOL_EXTENSION = "tuttiProtocol";

    public static TuttiProtocol sharedProtocol;

    @Resource(name = "cacheService")
    protected CacheService cacheService;

    public TuttiProtocol getProtocol() {
        return sharedProtocol;
    }

    public void setProtocol(TuttiProtocol protocol) {
        if (log.isDebugEnabled()) {
            log.debug("Set shared protocol: " + protocol);
        }
        sharedProtocol = protocol;

        try {
            cacheService.clearCache("species");
            cacheService.clearCache("referentSpecies");
            cacheService.clearCache("referentSpeciesById");
            cacheService.clearCache("referentSpeciesByIdVernacular");
            cacheService.clearCache("obsoleteReferentTaxons");
        } catch (Exception e) {

            //FIXME This
            if (log.isErrorEnabled()) {
                log.error("Could not clear caches", e);
            }
        }
    }

    @Override
    public boolean isProtocolExist(String id) {
        return getAllProtocolId().contains(id);
    }

    @Override
    public String getFirstAvailableName(String protocolName) {

        List<String> allProtocolNames = getAllProtocolNames();

        String availableName;

        if (allProtocolNames.contains(protocolName)) {

            availableName = TuttiProtocols.getFirstAvailableName(protocolName, allProtocolNames);

        } else {

            availableName = protocolName;
        }

        return availableName;

    }

    @Override
    public TuttiProtocol getProtocolByName(String protocolName) {

        TuttiProtocol result = null;
        for (TuttiProtocol protocol : getAllProtocol()) {
            if (protocolName.equals(protocol.getName()))
                result = protocol;
        }
        return result;

    }

    @Override
    public List<String> getAllProtocolId() {

        File protocolDirectory = config.getProtocolDirectory();
        Collection<File> files = FileUtils.listFiles(
                protocolDirectory,
                new String[]{TUTTI_PROTOCOL_EXTENSION}, false);
        List<String> result = Lists.newArrayListWithCapacity(files.size());
        if (log.isDebugEnabled()) {
            log.debug("Found " + files.size() + " protocol(s).");
        }
        int suffixLength = TUTTI_PROTOCOL_EXTENSION.length() + 1;
        for (File file : files) {
            String fileName = file.getName();
            result.add(fileName.substring(0,
                                          fileName.length() - suffixLength));
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<String> getAllProtocolNames() {

        List<String> result = Lists.newArrayList();
        result.addAll(getAllProtocol().stream().map(TuttiProtocol::getName).collect(Collectors.toList()));
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiProtocol> getAllProtocol() {
        List<TuttiProtocol> result = Lists.newArrayList();
        for (String id : getAllProtocolId()) {
            TuttiProtocol protocol = getProtocol(id);
            result.add(protocol);
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiProtocol> getAllProtocol(String programId) {
        List<TuttiProtocol> result = Lists.newArrayList();
        for (String id : getAllProtocolId()) {
            TuttiProtocol protocol = getProtocol(id);
            if (TuttiProtocols.matchProgramId(protocol, programId)) {
                if (log.isDebugEnabled()) {
                    log.debug("Keep protocol: " + protocol.getName() + ", programId: " + programId + " matches!");
                }
                result.add(protocol);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Remove protocol: " + protocol.getName() + ", programId: " + programId + " does not match!");
                }
            }
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public TuttiProtocol getProtocol(String id) {
        File file = getProtocolFile(id);
        return TuttiProtocols.fromFile(file);
    }

    @Override
    public TuttiProtocol createProtocol(TuttiProtocol bean) {
        Binder<TuttiProtocol, TuttiProtocol> protocolBinder =
                BinderFactory.newBinder(TuttiProtocol.class);

        TuttiProtocol result = TuttiProtocols.newTuttiProtocol();
        protocolBinder.copy(bean, result);
        result.setId(UUID.randomUUID().toString());

//        result.setLengthClassesPmfmId(TuttiEntities.getList(bean.getLengthClassesPmfmId()));
//        result.setVesselUseFeaturePmfmId(TuttiEntities.getList(bean.getVesselUseFeaturePmfmId()));
//        result.setGearUseFeaturePmfmId(TuttiEntities.getList(bean.getGearUseFeaturePmfmId()));
        result.setLengthClassesPmfmId(bean.getLengthClassesPmfmId());
        result.setCaracteristicMapping(bean.getCaracteristicMapping());

        Binder<SpeciesProtocol, SpeciesProtocol> speciesProtocolBinder =
                BinderFactory.newBinder(SpeciesProtocol.class);

        List<SpeciesProtocol> species = Lists.newArrayList();
        if (!bean.isSpeciesEmpty()) {
            for (SpeciesProtocol speciesProtocol : bean.getSpecies()) {
                SpeciesProtocol s = SpeciesProtocols.newSpeciesProtocol();
                speciesProtocolBinder.copy(speciesProtocol, s);
                s.setId(UUID.randomUUID().toString());
                species.add(s);
            }
        }
        result.setSpecies(species);

        List<SpeciesProtocol> benthos = Lists.newArrayList();
        if (!bean.isBenthosEmpty()) {
            for (SpeciesProtocol speciesProtocol : bean.getBenthos()) {
                SpeciesProtocol s = SpeciesProtocols.newSpeciesProtocol();
                speciesProtocolBinder.copy(speciesProtocol, s);
                s.setId(UUID.randomUUID().toString());
                benthos.add(s);
            }
        }
        result.setBenthos(benthos);

        List<Zone> zones = new ArrayList<>();
        if (!bean.isZoneEmpty()) {
            for (Zone zone : bean.getZone()) {
                Zone z = Zones.newZone(zone);

                zones.add(z);

                List<Strata> stratas = new ArrayList<>();
                if (!zone.isStrataEmpty()) {
                    for (Strata strata : zone.getStrata()) {
                        Strata s = Stratas.newStrata(strata);
                        stratas.add(s);

                        List<SubStrata> subStratas = new ArrayList<>();
                        if (!strata.isSubstrataEmpty()) {
                            for (SubStrata subStrata : strata.getSubstrata()) {
                                SubStrata ss = SubStratas.newSubStrata(subStrata);
                                subStratas.add(ss);
                            }
                        }
                        s.setSubstrata(subStratas);
                    }
                }
                z.setStrata(stratas);
            }
        }
        result.setZone(zones);

        String id = result.getId();

        File file = getProtocolFile(id);
        TuttiProtocols.toFile(result, file);
        return result;
    }

    @Override
    public TuttiProtocol saveProtocol(TuttiProtocol bean) {
        String id = bean.getId();

        File file = getProtocolFile(id);
        TuttiProtocols.toFile(bean, file);
        return bean;
    }

    @Override
    public void deleteProtocol(String protocolId) {
        File file = getProtocolFile(protocolId);

        if (file.exists()) {
            ApplicationIOUtil.deleteFile(
                    file,
                    t("tutti.persistence.protocol.delete.error", protocolId, file));
        }
    }

    protected File getProtocolFile(String id) {
        File protocolDirectory = config.getProtocolDirectory();
        return new File(protocolDirectory, id + "." + TUTTI_PROTOCOL_EXTENSION);
    }
}
