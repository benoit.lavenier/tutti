package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.StatusCode;
import fr.ifremer.adagio.core.dao.referential.StatusDao;
import fr.ifremer.adagio.core.service.technical.CacheService;
import fr.ifremer.tutti.persistence.entities.referential.Status;
import fr.ifremer.tutti.persistence.entities.referential.Statuss;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.persistence.service.AbstractPersistenceService;
import org.hibernate.Query;

import javax.annotation.Resource;
import java.util.Iterator;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public abstract class ReferentialPersistenceServiceSupport extends AbstractPersistenceService {

    @Resource(name = "statusDao")
    private StatusDao statusDao;

    @Resource(name = "cacheService")
    protected CacheService cacheService;

    protected Iterator<Object[]> queryListWithStatus(String queryName, Object... params) {
        Query query = createQuery(queryName, params);
        query.setString("statusValidCode", StatusCode.ENABLE.getValue());
        query.setString("statusTemporaryCode", StatusCode.TEMPORARY.getValue());

        return query.iterate();
    }

    protected Iterator<Object[]> queryListWithStatus2(String queryName, Object... params) {
        Query query = createQuery(queryName, params);
        query.setString("statusValidCode", StatusCode.ENABLE.getValue());
        query.setString("statusTemporaryCode", StatusCode.TEMPORARY.getValue());
        query.setString("statusDisableCode", StatusCode.DISABLE.getValue());

        return query.iterate();
    }

    protected Object[] queryUniqueWithStatus(String queryName, Object... params) {

        Query query = createQuery(queryName, params);
        query.setString("statusValidCode", StatusCode.ENABLE.getValue());
        query.setString("statusTemporaryCode", StatusCode.TEMPORARY.getValue());

        Object result = query.uniqueResult();
        return (Object[]) result;
    }

    protected Object[] queryUniqueWithStatus2(String queryName, Object... params) {

        Query query = createQuery(queryName, params);
        query.setString("statusValidCode", StatusCode.ENABLE.getValue());
        query.setString("statusTemporaryCode", StatusCode.TEMPORARY.getValue());
        query.setString("statusDisableCode", StatusCode.DISABLE.getValue());

        Object result = query.uniqueResult();
        return (Object[]) result;
    }

    protected <E extends TuttiReferentialEntity> void setStatus(String statusCode, E entity) {
        Status newStatus = Statuss.newStatus();
        fr.ifremer.adagio.core.dao.referential.Status status = statusDao.load(statusCode);
        newStatus.setId(status.getCode());
        newStatus.setName(status.getName());
        entity.setStatus(newStatus);
    }

}
