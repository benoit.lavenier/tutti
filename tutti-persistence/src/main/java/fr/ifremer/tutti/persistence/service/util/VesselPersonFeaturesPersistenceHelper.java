package fr.ifremer.tutti.persistence.service.util;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.administration.user.Person;
import fr.ifremer.adagio.core.dao.administration.user.PersonImpl;
import fr.ifremer.adagio.core.dao.data.operation.FishingOperation;
import fr.ifremer.adagio.core.dao.data.survey.fishingTrip.FishingTrip;
import fr.ifremer.adagio.core.dao.data.vessel.feature.person.VesselPersonFeatures;
import fr.ifremer.adagio.core.dao.referential.VesselPersonRole;
import fr.ifremer.adagio.core.dao.referential.VesselPersonRoleId;
import fr.ifremer.adagio.core.dao.referential.VesselPersonRoleImpl;
import fr.ifremer.tutti.persistence.service.AbstractPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * Helper around {@link VesselPersonFeatures}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
@Component("vesselPersonFeaturesPersistenceHelper")
public class VesselPersonFeaturesPersistenceHelper extends AbstractPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(VesselPersonFeaturesPersistenceHelper.class);

    public VesselPersonFeaturesPersistenceHelper() {
    }

    public VesselPersonRole getScientificCruiseManagerRole() {
        return load(VesselPersonRoleImpl.class, VesselPersonRoleId.SCIENTIFIC_CRUISE_MANAGER.getValue());
    }

    public VesselPersonRole getSortRoomManagerRole() {
        return load(VesselPersonRoleImpl.class, VesselPersonRoleId.SORT_ROOM_MANAGER.getValue());
    }

    public VesselPersonRole getRecorderPersonRole() {
        return load(VesselPersonRoleImpl.class, VesselPersonRoleId.RECORDER_PERSON.getValue());
    }

    public void fillVesselPersonFeatures(Map<Integer, VesselPersonFeatures> vesselPersonFeaturesPerPerson,
                                         Integer personId,
                                         FishingTrip fishingTrip,
                                         VesselPersonRole role,
                                         short rankOrder) {

        VesselPersonFeatures vesselPersonFeatures =
                vesselPersonFeaturesPerPerson.get(personId);
        if (vesselPersonFeatures == null) {

            PersonImpl person = load(PersonImpl.class, personId);

            if (log.isDebugEnabled()) {
                log.debug("Create an new vesselPersonFeatures for person: [" + personId + "]" + person.getFirstname() + " - " + person.getLastname());
            }

            vesselPersonFeatures = VesselPersonFeatures.Factory.newInstance();
            vesselPersonFeaturesPerPerson.put(personId, vesselPersonFeatures);
            vesselPersonFeatures.setPerson(person);

            vesselPersonFeatures.setFishingTrip(fishingTrip);
            vesselPersonFeatures.setStartDate(fishingTrip.getDepartureDateTime());
            vesselPersonFeatures.setEndDate(fishingTrip.getReturnDateTime());
            vesselPersonFeatures.setVessel(fishingTrip.getVessel());
            vesselPersonFeatures.setProgram(fishingTrip.getProgram());

            vesselPersonFeatures.setCreationDate(fishingTrip.getCreationDate());
            vesselPersonFeatures.setQualityFlag(fishingTrip.getQualityFlag());
            vesselPersonFeatures.setRankOrder(rankOrder);
        }

        addRole(role, vesselPersonFeatures);
    }

    public void fillVesselPersonFeatures(Map<Integer, VesselPersonFeatures> vesselPersonFeaturesPerPerson,
                                         Integer personId,
                                         FishingOperation fishingOperation,
                                         VesselPersonRole role,
                                         short rankOrder) {

        VesselPersonFeatures vesselPersonFeatures =
                vesselPersonFeaturesPerPerson.get(personId);
        if (vesselPersonFeatures == null) {

            PersonImpl person = load(PersonImpl.class, personId);
            if (log.isDebugEnabled()) {
                log.debug("Create an new vesselPersonFeatures for person: [" + personId + "]" + person.getFirstname() + " - " + person.getLastname());
            }

            vesselPersonFeatures = VesselPersonFeatures.Factory.newInstance();
            vesselPersonFeaturesPerPerson.put(personId, vesselPersonFeatures);
            vesselPersonFeatures.setOperation(fishingOperation);
            vesselPersonFeatures.setPerson(person);
            vesselPersonFeatures.setStartDate(fishingOperation.getStartDateTime());
            vesselPersonFeatures.setEndDate(fishingOperation.getEndDateTime());
            vesselPersonFeatures.setVessel(fishingOperation.getVessel());
            vesselPersonFeatures.setProgram(fishingOperation.getFishingTrip().getProgram());

            vesselPersonFeatures.setCreationDate(new Date());
            vesselPersonFeatures.setQualityFlag(fishingOperation.getQualityFlag());
            vesselPersonFeatures.setRankOrder(rankOrder);
        }

        addRole(role, vesselPersonFeatures);
    }

    protected void addRole(VesselPersonRole role,
                           VesselPersonFeatures vesselPersonFeatures) {
        Person person = vesselPersonFeatures.getPerson();
        if (vesselPersonFeatures.getVesselPersonRoles().contains(role)) {
            if (log.isWarnEnabled()) {
                log.warn("vessel person feature for person: " + person.getId() + " with role: " + role.getName() + " already exist, do not add it twice.");
            }
        } else {

            // add this new role
            if (log.isInfoEnabled()) {
                log.info("Add vessel person feature for person: " + person.getId() + " with role: " + role.getName());
            }
            vesselPersonFeatures.getVesselPersonRoles().add(role);

        }
    }
}
