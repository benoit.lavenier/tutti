package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.data.vessel.VesselExtendDao;
import fr.ifremer.adagio.core.dao.referential.StatusCode;
import fr.ifremer.adagio.core.dao.referential.VesselTypeId;
import fr.ifremer.adagio.core.dao.referential.location.LocationExtendDao;
import fr.ifremer.adagio.core.dao.referential.location.LocationLabel;
import fr.ifremer.adagio.core.dao.referential.location.LocationLevelId;
import fr.ifremer.adagio.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.springframework.cache.Cache;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Service("vesselPersistenceService")
public class VesselPersistenceServiceImpl extends ReferentialPersistenceServiceSupport implements VesselPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VesselPersistenceServiceImpl.class);

    @Resource(name = "vesselExtendDao")
    protected VesselExtendDao vesselExtendDao;

    @Resource(name = "locationDao")
    protected LocationExtendDao locationDao;

    @Override
    public List<Vessel> getAllScientificVessel() {

        Iterator<Object[]> list = queryListWithStatus(
                "allVessels",
                "refDate", DateType.INSTANCE, new Date(),
                "vesselTypeId", IntegerType.INSTANCE, VesselTypeId.SCIENTIFIC_RESEARCH_VESSEL.getValue());

        List<Vessel> result = Lists.newArrayList();
        Cache vesselByCodeCache = cacheService.getCache("vesselByCode");
        loadVessels(list, result, true, vesselByCodeCache);

        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Vessel> getAllFishingVessel() {

        Iterator<Object[]> list = queryListWithStatus(
                "allVessels",
                "refDate", DateType.INSTANCE, new Date(),
                "vesselTypeId", IntegerType.INSTANCE, VesselTypeId.FISHING_VESSEL.getValue());

        List<Vessel> result = Lists.newArrayList();
        Cache vesselByCodeCache = cacheService.getCache("vesselByCode");
        loadVessels(list, result, false, vesselByCodeCache);

        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Vessel> getAllVesselWithObsoletes() {

        List<Vessel> result = Lists.newArrayList();

        Iterator<Object[]> fishingVesselList = queryListWithStatus2(
                "allVesselsWithObsoletes",
                "refDate", DateType.INSTANCE, new Date(),
                "vesselTypeId", IntegerType.INSTANCE, VesselTypeId.FISHING_VESSEL.getValue());
        loadVesselsWithObsoletes(fishingVesselList, result, false);
        int fishingVesselCount = result.size();
        if (log.isDebugEnabled()) {
            log.debug("fishing vessels: " + fishingVesselCount);
        }

        Iterator<Object[]> scientificVesselList = queryListWithStatus2(
                "allVesselsWithObsoletes",
                "refDate", DateType.INSTANCE, new Date(),
                "vesselTypeId", IntegerType.INSTANCE, VesselTypeId.SCIENTIFIC_RESEARCH_VESSEL.getValue());
        loadVesselsWithObsoletes(scientificVesselList, result, true);
        if (log.isDebugEnabled()) {
            log.debug("scientific vessels: " + (result.size() - fishingVesselCount));
        }

        return Collections.unmodifiableList(result);

    }

    @Override
    public Vessel getVessel(String vesselCode) {

        if (log.isDebugEnabled()) {
            log.debug("get vessel: " + vesselCode);
        }
        // On ne peut pas récupérer directement un unique objet à cause des période de validité d'immtriculation
        // L'ordre induit par la requete nous donne cependant un match en dernière position
        Iterator<Object[]> source = queryListWithStatus2(
                "vesselByCode",
                "vesselCode", StringType.INSTANCE, vesselCode,
                "refDate", DateType.INSTANCE, new Date()
        );
        Object[] vesselSource = null;
        while (source.hasNext()) {
            vesselSource = source.next();
        }
        if (vesselSource == null) {
            throw new ApplicationTechnicalException("Could not find vessel with code: " + vesselCode);
        }
        return loadVessel(vesselSource);

    }

    @Override
    public boolean isTemporaryVesselUsed(String code) {

        Long count = queryUniqueTyped("countVesselInCruise", "id", StringType.INSTANCE, code);
        boolean result = count > 0;

        if (!result) {
            count = queryUniqueTyped("countVesselInFishingOperation", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countVesselInOperationVesselAssociation", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countVesselInDailyActivityCalendar", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countVesselInLanding", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countVesselInFishingtrip", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countVesselInVesselUseFeatures", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countVesselInGearUseFeatures", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countVesselInGearPhysicalFeatures", "id", StringType.INSTANCE, code);
            result = count > 0;
        }

        return result;

    }

    @Override
    public List<Vessel> addTemporaryVessels(List<Vessel> vessels) {

        Integer countryLocationId =
                locationDao.getLocationIdByLabelAndLocationLevel(
                        LocationLabel.FRANCE.getValue(),
                        new Integer[]{LocationLevelId.PAYS_ISO3.getValue()});
        if (countryLocationId == null) {
            throw new DataIntegrityViolationException("Default country location not found, with label=" + LocationLabel.FRANCE.getValue());
        }

        List<Vessel> result = Lists.newArrayList();
        for (Vessel source : vessels) {
            Vessel added = addTemporaryVessel(source, countryLocationId);
            result.add(added);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Vessel> updateTemporaryVessels(List<Vessel> vessels) {

        Integer countryLocationId =
                locationDao.getLocationIdByLabelAndLocationLevel(
                        LocationLabel.FRANCE.getValue(),
                        new Integer[]{LocationLevelId.PAYS_ISO3.getValue()});
        if (countryLocationId == null) {
            throw new DataIntegrityViolationException("Default country location not found, with label=" + LocationLabel.FRANCE.getValue());
        }

        List<Vessel> result = Lists.newArrayList();
        for (Vessel source : vessels) {
            Vessel updated = updateTemporaryVessel(source, countryLocationId);
            result.add(updated);
            source = updateTemporaryVessel(source, countryLocationId);
            result.add(source);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Vessel> linkTemporaryVessels(List<Vessel> vessels) {

        List<Vessel> result = Lists.newArrayList();
        for (Vessel source : vessels) {
            Vessel linked = linkTemporaryVessel(source);
            result.add(linked);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public void replaceVessel(Vessel source, Vessel target, boolean delete) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(target);
        Preconditions.checkState(Vessels.isTemporary(source));
        Preconditions.checkState(!Vessels.isTemporary(target));

        String sourceId = source.getId();
        String targetId = target.getId();

        queryUpdate("replaceVesselInCruise",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInFishingOperation",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInOperationVesselAssociation",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInDailyActivityCalendar",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInLanding",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInFishingtrip",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInVesselUseFeatures",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInGearUseFeatures",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        queryUpdate("replaceVesselInGearPhysicalFeatures",
                "sourceId", StringType.INSTANCE, sourceId,
                "targetId", StringType.INSTANCE, targetId);

        //TODO Check doublon...

        if (delete) {

            deleteTemporaryVessel(sourceId);

        }

    }

    @Override
    public void deleteTemporaryVessels(Collection<String> codes) {

        for (String code : codes) {
            deleteTemporaryVessel(code);
        }

    }

    @Override
    public void deleteTemporaryVessel(String code) {

        Preconditions.checkNotNull(code);
        if (!code.startsWith(TemporaryDataHelper.TEMPORARY_NAME_PREFIX)) {
            throw new ApplicationBusinessException(String.format("Can't delete a Vessel with a code %s not beginning with %s.", code, TemporaryDataHelper.TEMPORARY_NAME_PREFIX));
        }
        Vessel vessel = getVessel(code);
        if (vessel == null) {
            throw new ApplicationBusinessException(String.format("Vessel with code %s does not exists", code));
        }

        vesselExtendDao.removeTemporaryVessel(code);

    }

    protected Vessel addTemporaryVessel(Vessel source, Integer registrationLocationId) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkNotNull(source.getInternationalRegistrationCode());
        Preconditions.checkArgument(StringUtils.isBlank(source.getId()) || Vessels.isTemporaryId(source.getId()));

        Integer vesselTypeId = getVesselTypeId(source);

        fr.ifremer.adagio.core.dao.data.vessel.Vessel target =
                vesselExtendDao.createAsTemporary(
                        null,
                        source.getInternationalRegistrationCode(),
                        registrationLocationId, source.getName(),
                        vesselTypeId,
                        false);

        // Fill the result bean
        Vessel result = Vessels.newVessel();
        result.setId(target.getCode());
        result.setName(source.getName());
        result.setRegistrationCode(source.getRegistrationCode());
        result.setInternationalRegistrationCode(source.getInternationalRegistrationCode());
        result.setScientificVessel(source.isScientificVessel());
        setStatus(StatusCode.TEMPORARY.getValue(), result);
        return result;

    }

    protected Vessel updateTemporaryVessel(Vessel source, Integer countryLocationId) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkNotNull(source.getInternationalRegistrationCode());
        Preconditions.checkArgument(Vessels.isTemporaryId(source.getId()));

        // Fill the result bean
        Vessel result = getVessel(source.getId());
        result.setName(source.getName());
        result.setRegistrationCode(source.getRegistrationCode());
        result.setInternationalRegistrationCode(source.getInternationalRegistrationCode());
        result.setScientificVessel(source.isScientificVessel());
        setStatus(StatusCode.TEMPORARY.getValue(), result);

        Integer vesselTypeId = getVesselTypeId(source);

        vesselExtendDao.updateTemporaryVessel(
                source.getId(),
                source.getRegistrationCode(),
                source.getInternationalRegistrationCode(),
                countryLocationId,
                source.getName(),
                vesselTypeId,
                false);

        return result;

    }

    private Integer getVesselTypeId(Vessel source) {
        Integer vesselTypeId;
        if (source.isScientificVessel()) {
            vesselTypeId = VesselTypeId.SCIENTIFIC_RESEARCH_VESSEL.getValue();
        } else {
            vesselTypeId = VesselTypeId.FISHING_VESSEL.getValue();
        }
        return vesselTypeId;
    }

    protected Vessel linkTemporaryVessel(Vessel source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkNotNull(source.getInternationalRegistrationCode());
        Preconditions.checkArgument(Vessels.isTemporaryId(source.getId()));

        // Warning : return a list because more than one line could be found,
        // but 'order by' assume that the first one in the good row
        Iterator<Object[]> row = queryListWithStatus(
                "vesselByInternationalRegistrationCode",
                "vesselInternationalRegistrationCode", StringType.INSTANCE, source.getInternationalRegistrationCode(),
                "refDate", DateType.INSTANCE, new Date()
        );
        return row.hasNext() ? loadVessel(row.next()) : null;

    }

    protected void loadVessels(Iterator<Object[]> list, List<Vessel> result, boolean scientificVessel, Cache vesselByCodeCache) {
        while (list.hasNext()) {
            Object[] source = list.next();
            Vessel target = loadVessel(source);
            target.setScientificVessel(scientificVessel);
            result.add(target);
            // Add to cache
            vesselByCodeCache.put(target.getId(), target);
        }
    }

    protected void loadVesselsWithObsoletes(Iterator<Object[]> list, List<Vessel> result, boolean scientificVessel) {

        Vessel lastTarget = null;
        while (list.hasNext()) {
            Object[] source = list.next();
            Vessel target = loadVessel(source);
            target.setScientificVessel(scientificVessel);
            if (lastTarget != null && !target.getId().equals(lastTarget.getId())) {

                // nouveau code à traiter, on doit enregister l'ancien navire
                result.add(lastTarget);
            }
            lastTarget = target;
        }
        result.add(lastTarget);

    }

    protected Vessel loadVessel(Object... source) {

        Vessel target = Vessels.newVessel();
        target.setId((String) source[0]);
        target.setRegistrationCode((String) source[1]);
        target.setInternationalRegistrationCode((String) source[2]);
        target.setName((String) source[3]);
        Integer vesselTypeId = (Integer) source[4];
        boolean scientificVessel = VesselTypeId.SCIENTIFIC_RESEARCH_VESSEL.getValue().equals(vesselTypeId);
        target.setScientificVessel(scientificVessel);
        setStatus((String) source[5], target);
        return target;

    }

}
