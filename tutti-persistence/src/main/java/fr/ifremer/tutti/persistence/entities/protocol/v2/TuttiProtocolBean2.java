package fr.ifremer.tutti.persistence.entities.protocol.v2;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntityBean;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TuttiProtocolBean2 extends TuttiEntityBean implements TuttiProtocol2 {

    private static final long serialVersionUID = 3847260679792845110L;

    protected String name;

    protected String comment;

    protected List<String> gearUseFeaturePmfmId;

    protected List<String> vesselUseFeaturePmfmId;

    protected List<String> lengthClassesPmfmId;

    protected List<String> individualObservationPmfmId;

    protected Integer version;

    protected List<SpeciesProtocol> species;

    protected List<SpeciesProtocol> benthos;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String getGearUseFeaturePmfmId(int index) {
        return getChild(gearUseFeaturePmfmId, index);
    }

    @Override
    public boolean isGearUseFeaturePmfmIdEmpty() {
        return gearUseFeaturePmfmId == null || gearUseFeaturePmfmId.isEmpty();
    }

    @Override
    public int sizeGearUseFeaturePmfmId() {
        return gearUseFeaturePmfmId == null ? 0 : gearUseFeaturePmfmId.size();
    }

    @Override
    public void addGearUseFeaturePmfmId(String gearUseFeaturePmfmId) {
        getGearUseFeaturePmfmId().add(gearUseFeaturePmfmId);
    }

    @Override
    public void addAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId) {
        getGearUseFeaturePmfmId().addAll(gearUseFeaturePmfmId);
    }

    @Override
    public boolean removeGearUseFeaturePmfmId(String gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().remove(gearUseFeaturePmfmId);
    }

    @Override
    public boolean removeAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().removeAll(gearUseFeaturePmfmId);
    }

    @Override
    public boolean containsGearUseFeaturePmfmId(String gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().contains(gearUseFeaturePmfmId);
    }

    @Override
    public boolean containsAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().containsAll(gearUseFeaturePmfmId);
    }

    @Override
    public List<String> getGearUseFeaturePmfmId() {
        if (gearUseFeaturePmfmId == null) {
            gearUseFeaturePmfmId = new LinkedList<>();
        }
        return gearUseFeaturePmfmId;
    }

    @Override
    public void setGearUseFeaturePmfmId(List<String> gearUseFeaturePmfmId) {
        this.gearUseFeaturePmfmId = gearUseFeaturePmfmId;
    }

    @Override
    public String getVesselUseFeaturePmfmId(int index) {
        return getChild(vesselUseFeaturePmfmId, index);
    }

    @Override
    public boolean isVesselUseFeaturePmfmIdEmpty() {
        return vesselUseFeaturePmfmId == null || vesselUseFeaturePmfmId.isEmpty();
    }

    @Override
    public int sizeVesselUseFeaturePmfmId() {
        return vesselUseFeaturePmfmId == null ? 0 : vesselUseFeaturePmfmId.size();
    }

    @Override
    public void addVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId) {
        getVesselUseFeaturePmfmId().add(vesselUseFeaturePmfmId);
    }

    @Override
    public void addAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId) {
        getVesselUseFeaturePmfmId().addAll(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean removeVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().remove(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean removeAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().removeAll(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean containsVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().contains(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean containsAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().containsAll(vesselUseFeaturePmfmId);
    }

    @Override
    public List<String> getVesselUseFeaturePmfmId() {
        if (vesselUseFeaturePmfmId == null) {
            vesselUseFeaturePmfmId = new LinkedList<>();
        }
        return vesselUseFeaturePmfmId;
    }

    @Override
    public void setVesselUseFeaturePmfmId(List<String> vesselUseFeaturePmfmId) {
        this.vesselUseFeaturePmfmId = vesselUseFeaturePmfmId;
    }

    @Override
    public String getLengthClassesPmfmId(int index) {
        return getChild(lengthClassesPmfmId, index);
    }

    @Override
    public boolean isLengthClassesPmfmIdEmpty() {
        return lengthClassesPmfmId == null || lengthClassesPmfmId.isEmpty();
    }

    @Override
    public int sizeLengthClassesPmfmId() {
        return lengthClassesPmfmId == null ? 0 : lengthClassesPmfmId.size();
    }

    @Override
    public void addLengthClassesPmfmId(String lengthClassesPmfmId) {
        getLengthClassesPmfmId().add(lengthClassesPmfmId);
    }

    @Override
    public void addAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        getLengthClassesPmfmId().addAll(lengthClassesPmfmId);
    }

    @Override
    public boolean removeLengthClassesPmfmId(String lengthClassesPmfmId) {
        return getLengthClassesPmfmId().remove(lengthClassesPmfmId);
    }

    @Override
    public boolean removeAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        return getLengthClassesPmfmId().removeAll(lengthClassesPmfmId);
    }

    @Override
    public boolean containsLengthClassesPmfmId(String lengthClassesPmfmId) {
        return getLengthClassesPmfmId().contains(lengthClassesPmfmId);
    }

    @Override
    public boolean containsAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        return getLengthClassesPmfmId().containsAll(lengthClassesPmfmId);
    }

    @Override
    public List<String> getLengthClassesPmfmId() {
        if (lengthClassesPmfmId == null) {
            lengthClassesPmfmId = new LinkedList<>();
        }
        return lengthClassesPmfmId;
    }

    @Override
    public void setLengthClassesPmfmId(List<String> lengthClassesPmfmId) {
        this.lengthClassesPmfmId = lengthClassesPmfmId;
    }

    @Override
    public String getIndividualObservationPmfmId(int index) {
        return getChild(individualObservationPmfmId, index);
    }

    @Override
    public boolean isIndividualObservationPmfmIdEmpty() {
        return individualObservationPmfmId == null || individualObservationPmfmId.isEmpty();
    }

    @Override
    public int sizeIndividualObservationPmfmId() {
        return individualObservationPmfmId == null ? 0 : individualObservationPmfmId.size();
    }

    @Override
    public void addIndividualObservationPmfmId(String individualObservationPmfmId) {
        getIndividualObservationPmfmId().add(individualObservationPmfmId);
    }

    @Override
    public void addAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        getIndividualObservationPmfmId().addAll(individualObservationPmfmId);
    }

    @Override
    public boolean removeIndividualObservationPmfmId(String individualObservationPmfmId) {
        return getIndividualObservationPmfmId().remove(individualObservationPmfmId);
    }

    @Override
    public boolean removeAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        return getIndividualObservationPmfmId().removeAll(individualObservationPmfmId);
    }

    @Override
    public boolean containsIndividualObservationPmfmId(String individualObservationPmfmId) {
        return getIndividualObservationPmfmId().contains(individualObservationPmfmId);
    }

    @Override
    public boolean containsAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        return getIndividualObservationPmfmId().containsAll(individualObservationPmfmId);
    }

    @Override
    public List<String> getIndividualObservationPmfmId() {
        if (individualObservationPmfmId == null) {
            individualObservationPmfmId = new LinkedList<>();
        }
        return individualObservationPmfmId;
    }

    @Override
    public void setIndividualObservationPmfmId(List<String> individualObservationPmfmId) {
        this.individualObservationPmfmId = individualObservationPmfmId;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public SpeciesProtocol getSpecies(int index) {
        return getChild(species, index);
    }

    @Override
    public boolean isSpeciesEmpty() {
        return species == null || species.isEmpty();
    }

    @Override
    public int sizeSpecies() {
        return species == null ? 0 : species.size();
    }

    @Override
    public void addSpecies(SpeciesProtocol species) {
        getSpecies().add(species);
    }

    @Override
    public void addAllSpecies(Collection<SpeciesProtocol> species) {
        getSpecies().addAll(species);
    }

    @Override
    public boolean removeSpecies(SpeciesProtocol species) {
        return getSpecies().remove(species);
    }

    @Override
    public boolean removeAllSpecies(Collection<SpeciesProtocol> species) {
        return getSpecies().removeAll(species);
    }

    @Override
    public boolean containsSpecies(SpeciesProtocol species) {
        return getSpecies().contains(species);
    }

    @Override
    public boolean containsAllSpecies(Collection<SpeciesProtocol> species) {
        return getSpecies().containsAll(species);
    }

    @Override
    public List<SpeciesProtocol> getSpecies() {
        if (species == null) {
            species = new LinkedList<>();
        }
        return species;
    }

    @Override
    public void setSpecies(List<SpeciesProtocol> species) {
        this.species = species;
    }

    @Override
    public SpeciesProtocol getBenthos(int index) {
        return getChild(benthos, index);
    }

    @Override
    public boolean isBenthosEmpty() {
        return benthos == null || benthos.isEmpty();
    }

    @Override
    public int sizeBenthos() {
        return benthos == null ? 0 : benthos.size();
    }

    @Override
    public void addBenthos(SpeciesProtocol benthos) {
        getBenthos().add(benthos);
    }

    @Override
    public void addAllBenthos(Collection<SpeciesProtocol> benthos) {
        getBenthos().addAll(benthos);
    }

    @Override
    public boolean removeBenthos(SpeciesProtocol benthos) {
        return getBenthos().remove(benthos);
    }

    @Override
    public boolean removeAllBenthos(Collection<SpeciesProtocol> benthos) {
        return getBenthos().removeAll(benthos);
    }

    @Override
    public boolean containsBenthos(SpeciesProtocol benthos) {
        return getBenthos().contains(benthos);
    }

    @Override
    public boolean containsAllBenthos(Collection<SpeciesProtocol> benthos) {
        return getBenthos().containsAll(benthos);
    }

    @Override
    public List<SpeciesProtocol> getBenthos() {
        if (benthos == null) {
            benthos = new LinkedList<>();
        }
        return benthos;
    }

    @Override
    public void setBenthos(List<SpeciesProtocol> benthos) {
        this.benthos = benthos;
    }

}
