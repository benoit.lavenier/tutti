package fr.ifremer.tutti.persistence.model;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.tutti.persistence.entities.data.Cruise;

import java.util.Iterator;
import java.util.Set;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class CruiseDataModel extends DataModelSupport implements Iterable<OperationDataModel> {

    private static final long serialVersionUID = 1L;

    private final Set<OperationDataModel> operations;

    public CruiseDataModel(Cruise cruise, Set<OperationDataModel> operations) {
        this(cruise.getId(), cruise.getName(), operations);
    }

    public CruiseDataModel(String id, String label, Set<OperationDataModel> operations) {
        super(id, label);
        this.operations = ImmutableSet.copyOf(operations);
    }

    @Override
    public Iterator<OperationDataModel> iterator() {
        return operations.iterator();
    }

    public int size() {
        return operations.size();
    }

    public OperationDataModel getOperation(String id) {

        OperationDataModel result = null;
        for (OperationDataModel operation : this) {

            if (id.equals(operation.getId())) {
                result = operation;
                break;
            }
        }
        return result;

    }

}
