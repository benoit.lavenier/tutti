package fr.ifremer.tutti.util;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 8/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class Distances {
    private static final double EARTH_RADIUS = 6378288.0;

    public static int computeDistanceInMeters(Float startLatitude,
                                              Float startLongitude,
                                              Float endLatitude,
                                              Float endLongitude) {

        double sLat = startLatitude * Math.PI / 180.0;
        double sLong = startLongitude * Math.PI / 180.0;
        double eLat = endLatitude * Math.PI / 180.0;
        double eLong = endLongitude * Math.PI / 180.0;

        Double d = EARTH_RADIUS *
                   (Math.PI / 2 - Math.asin(Math.sin(eLat) * Math.sin(sLat)
                                            + Math.cos(eLong - sLong) * Math.cos(eLat) * Math.cos(sLat)));
        return d.intValue();
    }

    public static String getDistanceInMilles(Float distance) {
        String distanceText;
        if (distance != null) {
            Float distanceInMilles = distance / 1852;
            distanceText = String.format("%.3f", distanceInMilles);

        } else {
            distanceText = "";
        }
        return distanceText;
    }
}
