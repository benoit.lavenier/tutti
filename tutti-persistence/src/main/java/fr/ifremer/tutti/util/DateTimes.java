package fr.ifremer.tutti.util;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.time.DurationFormatUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created on 9/5/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class DateTimes {

    public static String getDuration(Date startDate,
                                     Date endDate,
                                     String format) {
        String duration = "";
        if (startDate != null && endDate != null && !startDate.after(endDate)) {
            duration = DurationFormatUtils.formatPeriod(
                    startDate.getTime(),
                    endDate.getTime(),
                    format);
        }
        return duration;
    }

    /**
     * Enleve les données des heures (hour, minute, second, milli = 0).
     *
     * @param date la date a modifier
     * @return la date d'un jour
     */
    public static Date getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        date = calendar.getTime();
        return date;
    }

}
