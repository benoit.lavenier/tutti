package fr.ifremer.tutti.report;

/*
 * #%L
 * Tutti :: Report Generator
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Created on 3/10/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class ReportConfig {

    private final File reportFile;

    private final File dataDirectory;

    private final File outputFile;

    private final String stationNumber;

    private final int fishingOperationNumber;

    public ReportConfig(File reportFile, File dataDirectory, File outputFile, String stationNumber, int fishingOperationNumber) {
        this.reportFile = reportFile;
        this.dataDirectory = dataDirectory;
        this.outputFile = outputFile;
        this.stationNumber = stationNumber;
        this.fishingOperationNumber = fishingOperationNumber;
    }

    public File getReportFile() {
        return reportFile;
    }

    public File getDataDirectory() {
        return dataDirectory;
    }

    public String getStationNumber() {
        return stationNumber;
    }

    public int getFishingOperationNumber() {
        return fishingOperationNumber;
    }

    public File getOutputFile() {
        return outputFile;
    }
}
